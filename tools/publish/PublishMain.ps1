$publisherCmd = Join-Path (Get-ItemProperty "HKCU:\Software\Bohemia Interactive\Arma 3 Tools").path "Publisher\PublisherCmd.exe"

$path = "$PSScriptRoot\..\build\$((Get-Content "$PSScriptRoot\..\build\config.ini" | Select-String "Key =" | ConvertFrom-StringData).Key)"
if (-Not (Test-Path $path)){Write-Error "MONKE ERROR: Cannot publish to main without key"; exit 1 }

& $publisherCmd update /id:2173635424 /changenote:"See: https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod" /path:"$((Resolve-Path "..\..\@501st Community Aux Mod").Path)"
