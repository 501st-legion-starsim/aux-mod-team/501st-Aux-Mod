param(
    [Parameter(Mandatory=$False)]
    [int]$Loaders = 4
)

$fres = Get-WmiObject -query "select * from Win32_OptionalFeature where name = 'Client-ProjFS' and installstate = 1"

if ($null -eq $fres) {
    Write-Output "Enabling ProjFS"

    if (-Not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] 'Administrator')) 
    {
        $CommandLine = "-File `"" + $MyInvocation.MyCommand.Path + "`" " + $MyInvocation.UnboundArguments
        Start-Process -Wait -FilePath PowerShell.exe -Verb Runas -ArgumentList $CommandLine
    } else {
        Enable-WindowsOptionalFeature -Online -FeatureName Client-ProjFS -NoRestart
        Write-Output "Enabled ProjFS. Re-run build once done."
        exit 0
    }
}

$noAPI = $False
if ($null -eq (Get-Process -Name "VirtualPDrive.API" -ErrorAction SilentlyContinue)) {
    Set-Item "Env:VPD_VIRTUAL_INSTANCE_ID" ""
    $noAPI = $True
}

if ($noAPI) {
    $path = "$PSScriptRoot\..\vpd-api\VirtualPDrive.API.exe"
    $apiBooterPath = (Resolve-Path "$PSScriptRoot\..\shared\api-boot.ps1").Path

    & $apiBooterPath $path
}

& .\load-booter.ps1 $Loaders
