class cfgPatches
{
    class Aux501_Patch_Drexl
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_Drexl"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_shotgun_scatter20",
            "Aux501_Weapons_Mags_shotgun_he10",
            "Aux501_Weapons_Mags_shotgun_slug24"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_shotgun_scatter",
            "Aux501_Weapons_Ammo_shotgun_scatter_sub",

            "Aux501_Weapons_Ammo_shotgun_HE",
            "Aux501_Weapons_Ammo_shotgun_HE_sub"
        };
    };
};

class CowsSlot;
class PointerSlot;
class MuzzleSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class FullAuto;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Drexl: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] SX-17 'Drexl'";
        baseWeapon = "Aux501_Weaps_Drexl";
        picture = "\Aux501\Weapons\Republic\Drexl\data\textures\UI\drexl_ui_ca.paa";
        model = "\OPTRE_Weapons\bulldog\bulldog_auto.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\OPTRE_Weapons\bulldog\data\anim\bulldog.rtm"};
        hiddenSelections[] = {"camoBody","camoDecal"};
		hiddenSelectionsTextures[] = 
        {
            "\OPTRE_Weapons\bulldog\data\Body_Black.paa",
            "\Aux501\Weapons\Republic\Drexl\data\textures\drexl_2\drexl_DecalSheet_co.paa"
        };
		hiddenSelectionsMaterials[] = {"\OPTRE_Weapons\bulldog\data\body.rvmat",""};
        magazines[] =
        {
            "Aux501_Weapons_Mags_shotgun_slug24",
            "Aux501_Weapons_Mags_shotgun_scatter20",
            "Aux501_Weapons_Mags_shotgun_he10"
        };
        recoil = "recoil_MSBS65_ubs";
        reloadAction = "WBK_Commando_Reload";
        modes[] = {"FullAuto","Single","aiclose","aimedium"};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class StandardSound
            {
                soundSetShot[] = {"Msbs65_01_Shotgun_Shot_SoundSet","Msbs65_01_Shotgun_Tail_SoundSet"};
            };
            reloadTime = 0.35;
            dispersion = "5*0.00087";
            minRange = 2;
            minRangeProbab = 0.5;
            midRange = 400;
            midRangeProbab = 0.7;
            maxRange = 800;
            maxRangeProbab = 0.3;
        };
        class FullAuto: FullAuto
        {
            sounds[] = {"StandardSound"};
            class StandardSound
            {
                soundSetShot[] = {"Msbs65_01_Shotgun_Shot_SoundSet","Msbs65_01_Shotgun_Tail_SoundSet"};
            };
            reloadTime = 0.275;
            dispersion = "5*0.00087";
            minRange = 2;
            minRangeProbab = 0.5;
            midRange = 25;
            midRangeProbab = 0.7;
            maxRange = 50;
            maxRangeProbab = 0.3;
            aiRateOfFire = 2;
            aiRateOfFireDistance = 500;
        };
        class aiclose: FullAuto
        {
            showToPlayer = 0;
            minRange = 50;
            minRangeProbab = 0.1;
            midRange = 130;
            midRangeProbab = 0.8;
            maxRange = 200;
            maxRangeProbab = 0.15;
            aiRateOfFire = 2;
            aiRateOfFireDistance = 200;
        };
        class aimedium: aiclose
        {
            minRange = 150;
            minRangeProbab = 0.1;
            midRange = 200;
            midRangeProbab = 0.7;
            maxRange = 300;
            maxRangeProbab = 0.15;
            aiRateOfFire = 4;
            aiRateOfFireDistance = 300;
        };
        inertia = 1.4;
        dexterity = 1.7;
        initSpeed = 300;
        maxRecoilSway = 0.0125;
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.45};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = 
                {
                    "Aux501_cows_Holosight",
                    "Aux501_cows_Holosight_2",
                    "Aux501_cows_Holosight_3",
                    "SWLW_Westar35S_scope"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash"
                };
                linkProxy = "\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName = "$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint = "Center";
                iconPosition[] = {0.24,0.35};
                iconScale = 0.2;
            };
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_Weapons_Drexl_shield";
            shield_status = "noshield";
        };

    };
    class Aux501_Weaps_Drexl_shield: Aux501_Weaps_Drexl
    {
        displayName = "[501st] SX-17 'Drexl' (Shield)";
        baseWeapon = "Aux501_Weaps_Drexl";
        model = "\OPTRE_Weapons\RiotShield\riotshield_Bulldog.p3d";
		hiddenSelections[] = {"camoShield","camoWindshield","camoBody","camoDecal"};
		hiddenSelectionsTextures[] = 
        {
            "\Aux501\Weapons\Republic\Drexl\data\textures\drexl_2\drexl_shield_co.paa",
            "\OPTRE_Weapons\riotshield\data\body_ca.paa",
            "\OPTRE_Weapons\bulldog\data\chihuahua\Body_Black.paa",
            "\Aux501\Weapons\Republic\Drexl\data\textures\drexl_2\drexl_DecalSheet_co.paa"
        };
		hiddenSelectionsMaterials[] = 
        {
            "OPTRE_Weapons\RiotShield\data\body.rvmat",
            "OPTRE_Weapons\riotshield\data\glass.rvmat",
            "\OPTRE_Weapons\bulldog\data\chihuahua\body.rvmat",
            ""
        };
		handAnim[] = {"OFP2_ManSkeleton","\OPTRE_Weapons\RiotShield\data\anim\bulldog_human.rtm"};
		type = 1;
		inertia = 0.5;
		dexterity = 1.25;
		class OpticsModes
		{
			class ironsight
			{
				cameraDir = "eye_dir";
				discreteDistance[] = {100};
				discreteDistanceCameraPoint[] = {"eye"};
				discreteDistanceInitIndex = 0;
				discretefov[] = {};
				discreteInitIndex = 0;
				distancezoommax = 100;
				distancezoommin = 100;
				maxZeroing = 100;
				memorypointcamera = "eye";
				opticsDisablePeripherialVision = 0;
				opticsFlare = 0;
				opticsID = 0;
				opticsPPEffects[] = {"",""};
				opticsZoomMin = 0.375;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.75;
				useModelOptics = 0;
				visionMode[] = {};
			};
			class shieldView
			{
				cameraDir = "eye2_dir";
				discreteDistance[] = {100};
				discreteDistanceCameraPoint[] = {"eye2"};
				discreteDistanceInitIndex = 0;
				discretefov[] = {};
				discreteInitIndex = 0;
				distancezoommax = 100;
				distancezoommin = 100;
				maxZeroing = 100;
				memorypointcamera = "eye2";
				opticsDisablePeripherialVision = 0;
				opticsFlare = 0;
				opticsID = 1;
				opticsPPEffects[] = {"",""};
				opticsZoomMin = 0.375;
				opticsZoomMax = 1.1;
				opticsZoomInit = 0.75;
				useModelOptics = 0;
				visionMode[] = {};
			};
		};
		class FlashLight
		{
			color[] = {180,180,180};
			ambient[] = {0.9,0.9,0.9};
			intensity = 750;
			size = 1;
			innerAngle = 5;
			outerAngle = 45;
			coneFadeCoef = 10;
			position = "flash_dir";
			direction = "flash";
			useFlare = 1;
			flareSize = 1.5;
			flareMaxDistance = 150;
			dayLight = 0;
			class Attenuation
			{
				start = 0;
				constant = 0.5;
				linear = 0.1;
				quadratic = 0.2;
				hardLimitStart = 100;
				hardLimitEnd = 170;
			};
			scale[] = {0};
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_Weapons_Drexl_shield";
            shield_status = "shielded";
        };
    };
};

class CfgMagazines
{
    class 2Rnd_12Gauge_Pellets;

    class Aux501_Weapons_Mags_shotgun_scatter20: 2Rnd_12Gauge_Pellets
    {
        displayName = "[501st] 20Rnd SP Magazine";
        displayNameShort = "20Rnd SP Mag";
        author = "501st Aux Team";
        picture = "\a3\Weapons_F_Enoch\MagazineProxies\data\UI\icon_2rnd_12gauge_slugs_CA.paa";
        count = 20;
        ammo = "Aux501_Weapons_Ammo_shotgun_scatter";
        mass = 8;
        initSpeed = 600;
        descriptionShort = "Scatter Plasma Magazine";
        tracersEvery = 1;
        model = "\OPTRE_Weapons\bulldog\drum.p3d";
		modelSpecial = "\OPTRE_Weapons\bulldog\drum.p3d";
		modelSpecialIsProxy = 1;
        hiddenSelections[] = {"camoBody"};
		hiddenSelectionsTextures[] = {"\OPTRE_Weapons\bulldog\data\Body_Black.paa"};	
    };
    
    class Aux501_Weapons_Mags_shotgun_he10: Aux501_Weapons_Mags_shotgun_scatter20
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] 10Rnd UP Magazine";
        displayNameShort = "10Rnd UP Mag";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_shotgun_up_ca.paa";
        count = 10;
        ammo = "Aux501_Weapons_Ammo_shotgun_he";
        descriptionShort = "Unstable Plasma Magazine";
        hiddenSelectionsTextures[] = {"\OPTRE_Weapons\bulldog\data\mag_he.paa"};
    };
    
    class Aux501_Weapons_Mags_shotgun_slug24: Aux501_Weapons_Mags_shotgun_scatter20
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] 24Rnd Slug Magazine";
        displayNameShort = "24Rnd Slug Mag";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_shotgun_slug_ca.paa";
        initSpeed = 800;
        count = 24;
        ammo = "Aux501_Weapons_Ammo_30mw";
        descriptionShort = "30MW Slug Magazine";
    };
};

class CfgAmmo
{
    class B_12Gauge_Pellets_Submunition;
    class B_12Gauge_Pellets_Submunition_Deploy;
    
    class Aux501_Weapons_Ammo_shotgun_scatter: B_12Gauge_Pellets_Submunition
	{
		cartridge = "";
		submunitionConeType[] = {"poissondisc",24};
        submunitionAmmo = "Aux501_Weapons_Ammo_shotgun_scatter_sub";
		caliber = 2.5;
		hit = 20;
		thrustTime = 4.5;
		thrust = 410;
		timeToLive = 10;
	};
    class Aux501_Weapons_Ammo_shotgun_scatter_sub: B_12Gauge_Pellets_Submunition_Deploy
    {
        hit = 12;
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Small_Blue.p3d";
		tracerScale = 1;
		tracerStartTime = 0;
		tracerEndTime = 10;
    };
    class Aux501_Weapons_Ammo_shotgun_HE: Aux501_Weapons_Ammo_shotgun_scatter
    {
        submunitionConeAngle = 4;
        submunitionAmmo = "Aux501_Weapons_Ammo_shotgun_HE_sub";
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Small_Green.p3d";
    };
    class Aux501_Weapons_Ammo_shotgun_HE_sub: Aux501_Weapons_Ammo_shotgun_scatter_sub
    {
        hit = 8;
        indirectHit = 4;
        indirectHitRange = 0.9;
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Small_Green.p3d";
    };
};

class XtdGearModels
{
	class CfgWeapons
	{
		class Aux501_ACEX_Gear_Republic_Weapons_Drexl_shield
		{
			label = "";
			author = "501st Aux Team";
			options[] = 
            {
                "shield_status"
            };
			class shield_status
			{
				label = "Shield";
				changeingame = 1;
				changedelay = 2;
				values[] = 
                {
                    "noshield",
                    "shielded"
                };
				class noshield
				{
					label = "No Shield";
					actionLabel = "Store Shield";

				};
				class shielded
				{
					label = "Shield";
                    actionLabel = "Equip Shield";
					icon = "\MRC\JLTS\weapons\Shield\data\ui\shield_ui_ca.paa";
                    itemingame = "JLTS_riot_shield_501_item";
				};
			};
		};
	};
};