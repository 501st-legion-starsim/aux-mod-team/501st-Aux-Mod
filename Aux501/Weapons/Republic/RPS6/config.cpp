class cfgPatches
{
    class Aux501_Patch_RPS6
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_RPS6"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_rps6_at",
            "Aux501_Weapons_Mags_rps6_aa"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_rps6_at",
            "Aux501_Weapons_Ammo_rps6_aa"
        };
    };
};

class cfgWeapons
{
    class Launcher_Base_F;
    class Aux501_launcher_base: Launcher_Base_F
    {
        class Single;
    };

    class Aux501_Weaps_RPS6: Aux501_launcher_base
    {
        scope = 2;
        canLock = 2;
        airLock = 2;
        displayName = "[501st] RPS-6 Rocket Launcher";
        baseWeapon = "Aux501_Weaps_RPS6";
        picture = "\Aux501\Weapons\Republic\RPS6\data\textures\UI\rps6_ui.paa";
        model = "\3AS\3AS_Weapons\RPS6\3AS_RPS6_F.p3d";
        weaponInfoType = "RscOptics_titan";
        nameSound = "aalauncher";
        handAnim[] = 
        {
            "OFP2_ManSkeleton",
            "3as\3as_weapons\data\anim\rps6_hold.rtm"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_rps6_at",
            "Aux501_Weapons_Mags_rps6_aa"
        };
        lockedTargetSound[] = {"A3\Sounds_F\arsenal\weapons\Launchers\NLAW\locked_NLAW",0.316228,2.5};
        lockingTargetSound[] = {"A3\Sounds_F\arsenal\weapons\Launchers\NLAW\locking_NLAW",0.316228,1};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"A3\Sounds_F\arsenal\weapons\Launchers\NLAW\nlaw",1.99526,1,1800};
                soundBegin[] = {"begin1",1};
                soundSetShot[] = {"Launcher_RPG7_Shot_SoundSet","Launcher_RPG7_Tail_SoundSet"};
            };
        };
        modelOptics = "\A3\Weapons_F_Beta\acc\reticle_titan.p3d";
        class OpticsModes
        {
            class StepScope
            {
                opticsID = 1;
                useModelOptics = 1;
                opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
                opticsFlare = 0;
                opticsZoomInit = "0.25/1";
                opticsZoomMax = "0.25/1";
                opticsZoomMin = "0.25/6";
                distanceZoomMin = 300;
                distanceZoomMax = 300;
                memoryPointCamera = "eye";
                cameraDir = "look";
                visionMode[] = {"Normal","NVG","Ti"};
                thermalMode[] = {0};
                opticsDisablePeripherialVision = 1;
                discretefov[] = {"0.25/1","0.25/3","0.25/6"};
                discreteInitIndex = 0;
            };
        };
    };
    class Aux501_Weaps_Launchers_Hob: Aux501_Weaps_RPS6
    {
        displayName = "[IND] H0B-1 Kirbo Launcher";
        baseWeapon = "Aux501_Weaps_Launchers_Hob";
        model = "ls_weapons\tertiary\plx1\ls_weapon_plx1";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Weapons\Republic\RPS6\data\textures\H0B1_co.paa"};
        hiddenSelectionsMaterials[] = {"\Aux501\Weapons\Republic\RPS6\data\textures\H0B1.rvmat"};
        handAnim[] = {"OFP2_ManSkeleton","\ls_animation\weapons\launcher\plx\plx1_handanim.rtm"};
        cameraDir = "look";
        magazines[] = 
        {
            "Aux501_Weapons_Mags_hob_at",
            "Aux501_Weapons_Mags_hob_aa",
            "Aux501_Weapons_Mags_rps6_at",
            "Aux501_Weapons_Mags_rps6_aa"
        };
        class OpticsModes: OpticsModes
        {
            class StepScope
            {
                opticsID = 1;
                useModelOptics = 1;
                opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
                opticsFlare = 0;
                opticsZoomInit = "0.25/1";
                opticsZoomMax = "0.25/1";
                opticsZoomMin = "0.25/6";
                distanceZoomMin = 300;
                distanceZoomMax = 300;
                memoryPointCamera = "eye";
                cameraDir = "look";
                visionMode[] = {"Normal","NVG","Ti"};
                thermalMode[] = {0};
                opticsDisablePeripherialVision = 1;
                discretefov[] = {"0.25/1","0.25/3","0.25/6"};
                discreteInitIndex = 0;
            };
            class kirby: StepScope
            {
                opticsID = 1;
                useModelOptics = 0;
            };
        };
        class Single: Single
        {
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"Aux501\Weapons\Republic\RPS6\data\sounds\kirby_shot2.wss",10,1,2200};
                begin2[] = {"Aux501\Weapons\Republic\RPS6\data\sounds\kirby_shot1.wss",10,1,2200};
                soundBegin[] =
                {
                    "begin1",1,
                    "begin2",1
                };
            };
        };
    };
};

class CfgMagazines
{
    class Titan_AT;
    class Titan_AA;

    class Aux501_Weapons_Mags_rps6_at: Titan_AT
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] RPS-6 AT Rocket";
        author = "501st Aux Team";
        ammo = "Aux501_Weapons_Ammo_rps6_at";
        picture = "\a3\Weapons_F_Tank\Launchers\MRAWS\Data\UI\icon_rocket_MRAWS_HEAT_F_ca.paa";
        model = "\a3\Weapons_F_Tank\Launchers\MRAWS\rocket_MRAWS_HEAT_F_item.p3d";
        type = "2 *   256";
        count = 1;
        initSpeed = 60;
        maxLeadSpeed = 60;
        mass = 70;
    };
    class Aux501_Weapons_Mags_rps6_aa: Titan_AA
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] RPS-6 AA Rocket";
        displayNameShort = "AA";
        author = "501st Aux Team";
        ammo = "Aux501_Weapons_Ammo_rps6_aa";
        type = "6 *		256";
        picture = "\A3\Weapons_F_beta\Launchers\titan\Data\UI\gear_titan_missile_atl_CA.paa";
        model = "\A3\Weapons_F_beta\Launchers\titan\titan_missile_atl";
        initSpeed = 18;
        maxLeadSpeed = 277.778;
        mass = 100;
    };
    
    class Aux501_Weapons_Mags_hob_at: Aux501_Weapons_Mags_rps6_at
    {
        displayName = "[IND] H0B-1 AT Rocket";
        ammo = "Aux501_Weapons_Ammo_hob_at";
    };
    class Aux501_Weapons_Mags_hob_aa: Aux501_Weapons_Mags_rps6_aa
    {
        displayName = "[IND] H0B-1 AA Rocket";
        ammo = "Aux501_Weapons_Ammo_hob_aa";
    };
};

class CfgAmmo
{
    class M_Titan_AT;
    class M_Titan_AA;

    class Aux501_Weapons_Ammo_rps6_at: M_Titan_AT
    {
        aiAmmoUsageFlags = "128 + 256 +512";
        hit = 95;
        thrust = 130;
        thrustTime = 3.5;
        timeToLive = 20;
        indirectHit = 20;
        indirectHitRange = 2;
        soundFly[] = {"swlw_rework\sounds\launcher\plx_fly.wss",6,1.5,700};
    };
    class Aux501_Weapons_Ammo_rps6_aa: M_Titan_AA
    {
        hit = 900;
        indirectHit = 400;
        indirectHitRange = 1;
        soundFly[] = {"swlw_rework\sounds\launcher\plx_fly.wss",6,1.5,700};
        airLock = 2;
    };

    class Aux501_Weapons_Ammo_hob_at: Aux501_Weapons_Ammo_rps6_at
    {
        soundFly[] = {"\Aux501\Weapons\Republic\RPS6\data\sounds\Hobnob_missile_flyby2.wss",15,1,2200};
        effectsMissile = "Aux501_particle_effect_HOB1_fly";
    };
    class Aux501_Weapons_Ammo_hob_aa: Aux501_Weapons_Ammo_rps6_aa
    {
        soundFly[] = {"\Aux501\Weapons\Republic\RPS6\data\sounds\Hobnob_missile_flyby1.wss",15,1,2200};
        effectsMissile = "Aux501_particle_effect_HOB1_fly";
    };
};

class Aux501_particle_effect_HOB1_fly
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_hob1";
        position[] = {0,0,0};
    };
    class Smoke
    {
        simulation = "particles";
        type = "Aux501_cloudlet_E60R_smoke";
        position[] = {0,0,0};
        qualityLevel = -1;
    };
    class Sparks
    {
        simulation = "particles";
        type = "Aux501_cloudlet_E60R_sparks";
        position[] = {0,0,0};
        qualityLevel = 2;
    };
};

class CfgLights
{
    class RocketLight;

    class Aux501_light_hob1: RocketLight
    {
        color[]={255,102,178};
        intensity = 1e8;
        dayLight = 1;
        useFlare = 1;
        flareSize = 6;
        flareMaxDistance = 10000;
    };
};