class cfgPatches
{
    class Aux501_Patch_81mm_mortar_launcher
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "A3_Weapons_F",
            "Aux501_Patch_Vehicles_Mortars_Republic_81mw"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_81mm_plasma_Mortar_Carry"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_81mm_mortar_HE_csw",
            "Aux501_Weapons_Mags_81mm_mortar_smoke_csw",
            "Aux501_Weapons_Mags_81mm_mortar_illum_csw"
        };
    };
};

class Mode_SemiAuto;
class Mode_Burst;

class cfgWeapons
{
    class Launcher_Base_F;
    
    class Aux501_Weaps_81mm_plasma_Mortar_Carry: Launcher_Base_F
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] 81mw Plasma Mortar";
        picture = "\Aux501\Weapons\Republic\Mortar_Launcher\data\textures\UI\mortarlauncher_ui.paa";
        author = "501st Aux Team";
        model = "A3\Weapons_F_Tank\Launchers\Vorona\Vorona_launcher_F.p3d";
        hiddenSelections[] = {"camo_launcher","camo_tube"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Weapons\Republic\Mortar_Launcher\data\textures\Aux501_mortar_launcher_co.paa",
            "\Aux501\Weapons\Republic\Mortar_Launcher\data\textures\Aux501_mortar_launcher_co.paa"
        };
        mass = 450;
        class ACE_CSW
        {
            type = "mount";
            deployTime = 1;
            pickupTime = 1;
            deploy = "Aux501_Vehicles_Mortars_81mw_plasma_mortar";
        };
    };
};

class ACE_CSW_Groups
{
    class Aux501_Weapons_Mags_81mm_mortar_HE_csw
    {
        Aux501_Vehicle_Mags_81mw_mortar_HE = 1;
    };
    class Aux501_Weapons_Mags_81mm_mortar_smoke_csw
    {
        Aux501_Vehicle_Mags_81mw_mortar_smoke = 1;
    };
    class Aux501_Weapons_Mags_81mm_mortar_illum_csw
    {
        Aux501_Vehicle_Mags_81mw_mortar_illum = 1;
    };
};

class CfgMagazines
{
    class Aux501_Vehicle_Mags_81mw_mortar_HE;
    class Aux501_Vehicle_Mags_81mw_mortar_smoke;
    class Aux501_Vehicle_Mags_81mw_mortar_illum;

    class Aux501_Weapons_Mags_81mm_mortar_HE_csw: Aux501_Vehicle_Mags_81mw_mortar_HE
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] 81mw HE Rounds";
        picture = "\z\ace\addons\mk6mortar\UI\w_l16_ammo_he_ca.paa";
        model = "\z\ace\addons\mk6mortar\data\l16_ammo_he.p3d";
        ACE_isBelt = 1;
        type = 256;
        mass = 50;
        
    };
    class Aux501_Weapons_Mags_81mm_mortar_smoke_csw: Aux501_Vehicle_Mags_81mw_mortar_smoke
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] 81mw Smoke Rounds";
        picture = "\z\ace\addons\mk6mortar\UI\w_l16_ammo_smk_white_ca.paa";
        model = "\z\ace\addons\mk6mortar\data\l16_ammo_smk_white.p3d";
        ACE_isBelt = 1;
        type = 256;
        mass = 50;
    };
    class Aux501_Weapons_Mags_81mm_mortar_illum_csw: Aux501_Vehicle_Mags_81mw_mortar_illum
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] 81mw Illum Rounds";
        picture = "\z\ace\addons\mk6mortar\UI\w_l16_ammo_illum_ca.paa";
        model = "\z\ace\addons\mk6mortar\data\l16_ammo_illum.p3d";
        ACE_isBelt = 1;
        type = 256;
        mass = 50;
    };
};