class cfgPatches
{
    class Aux501_Patch_DC15L
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_DC15L"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mw240"
        };
    };
};

class CfgSoundShaders
{
	class Aux501_SoundShader_DC15L_closeShot
	{
		samples[] = 
        {
            {"Aux501\Weapons\Republic\DC15L\sounds\DC15LFire.wss",1}
        };
		volume = "1";
		range = 150;
		rangeCurve = "closeShotCurve";
	};
	class Aux501_SoundShader_DC15L_distShot
	{
		samples[] = 
        {
            {"Aux501\Weapons\Republic\DC15L\sounds\DC15LFire.wss",1}
        };
		volume = "1";
		range = 2200;
		rangeCurve[] = {{0,0},{50,0},{300,1},{1800,1}};
	};
};

class CfgSoundSets
{
	class Aux501_SoundSet_Weapon_DC15L
	{
		soundShaders[] = 
        {
            "Aux501_SoundShader_DC15L_closeShot",
            "Aux501_SoundShader_DC15L_distShot"
        };
		volumeFactor = 1.6;
		spatial = 1;
		doppler = 0;
		loop = 0;
	};
};

class CowsSlot;
class MuzzleSlot;
class PointerSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class ItemCore;
    class optic_DMS: ItemCore
    {
        class ItemInfo;
    };

    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class FullAuto;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DC15L: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] DC-15L";
        baseWeapon = "Aux501_Weaps_DC15L";
        picture = "\Aux501\Weapons\Republic\DC15L\data\textures\UI\dc15l_ui_ca.paa";
        model = "3AS\3AS_Weapons\DC15L\3AS_DC15L_f.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\3AS\3AS_Weapons\DC15L\Data\Anim\DC15L_handanim.rtm"};
        recoil = "recoil_SMG_03";
        reloadAction = "ReloadOverheat_DLT";
        reloadMagazineSound[] = {"WarMantle\WM_Imperial_Weapons\data\sfx\overheat",5,1,30};
        magazines[]=
        {
            "Aux501_Weapons_Mags_20mw240",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwup30"
        };
        modes[] = {"FullAuto","aiclose","aimedium","aifar"};
        class FullAuto: FullAuto
        {
            reloadTime = 0.1;		
            dispersion = 0.00075;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_DC15L"};
            };
        };
        class aiclose: FullAuto
        {
            burst = 10;
            aiRateOfFire = 1e-06;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.05;
            midRange = 70;
            midRangeProbab = 0.7;
            maxRange = 150;
            maxRangeProbab = 0.04;
            showToPlayer = 0;
        };
        class aimedium: aiclose
        {
            burst = 10;
            aiRateOfFireDistance = 300;
            minRange = 75;
            minRangeProbab = 0.05;
            midRange = 250;
            midRangeProbab = 0.7;
            maxRange = 500;
            maxRangeProbab = 0.04;
        };
        class aifar: aiclose
        {
            burst = 10;
            aiRateOfFireDistance = 600;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 700;
            midRangeProbab = 0.6;
            maxRange = 1000;
            maxRangeProbab = 0.1;
        };
        class OpticsModes
        {
            class Ironsights
            {
                opticsID = 1;
                useModelOptics = 0;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.375;
                opticsZoomMax = 1.1;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                visionMode[] = {};
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                scope = 0;
                compatibleItems[] = 
                {
                    "Aux501_DC15L_Scope",
                    "Aux501_cows_Holosight",
                    "Aux501_cows_Holosight_2",
                    "Aux501_cows_Holosight_3",
                    "Aux501_cows_HoloScope",
                    "Aux501_cows_HoloScope_2",
                    "Aux501_cows_HoloScope_3",
                    "Aux501_cows_HoloScope_4",
                    "Aux501_cows_HoloScope_5",
                    "Aux501_cows_HoloScope_6",
                    "Aux501_cows_RCO",
                    "Aux501_cows_RCO_2",
                    "Aux501_cows_RCO_3",
                    "Aux501_cows_MRCO",
                    "Aux501_cows_MRCO_2",
                    "Aux501_cows_MRCO_3",
                    "Aux501_cows_DMS",
                    "Aux501_cows_DMS_2",
                    "Aux501_cows_DMS_3",
                    "Aux501_cows_DMS_4",
                    "Aux501_cows_DMS_5",
                    "Aux501_cows_DMS_6",
                    "Aux501_cows_DMS_7",
                    "Aux501_cows_DMS_8"
                };
            };
            class MuzzleSlot: MuzzleSlot
            {
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash",
                    "Aux501_muzzle_flash_changer_blue"
                };
                linkProxy = "\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName = "$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint = "Center";
                iconPosition[] = {0.24,0.35};
                iconScale = 0.2;
            };
            class UnderBarrelSlot: UnderBarrelSlot
            {
                iconPicture="\A3\Weapons_F_Mark\Data\UI\attachment_under.paa";
                iconPinpoint="Bottom";
                linkProxy="\A3\Data_F_Mark\Proxies\Weapon_Slots\UNDERBARREL";
                compatibleItems[] = 
                {
                    "bipod_01_f_blk",
                    "3AS_Bipod_DC15L_f"
                };
            };
        };
        class LinkedItems
		{
			class LinkedItemsMuzzle
			{
				slot = "MuzzleSlot";
				item = "Aux501_muzzle_flash_changer_blue";
			};
		};
    };

    class Aux501_DC15L_Scope: optic_DMS
    {
        author = "501st Aux Team";
        scope = 2;
        scopearsenal = 2;
        displayName = "DC-15L 6-12x Scope";
        model = "3AS\3AS_Weapons\DC15L\3AS_DC15L_Scope_f.p3d";
        descriptionShort = "Long Range Scope";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style1";
        inertia = 0.2;
        class ItemInfo: ItemInfo
        {
            mass = 12;
            optics = 1;
            modelOptics = "\A3\Weapons_F\acc\reticle_sniper_F";
            class OpticsModes
            {
                class sight
                {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsPPEffects[] = {""};
                    opticsDisablePeripherialVision = 0;
                    opticsZoomMin = 0.25;
                    opticsZoomMax = 0.5;
                    opticsZoomInit = 0.75;
                    discreteInitIndex = 0;
                    distanceZoomMin = 200;
                    distanceZoomMax = 200;
                    memoryPointCamera = "eye";
                    visionMode[] = {};
                    opticsFlare = "false";
                    cameraDir = "";
                };
                class Scope
                {
                    opticsID = 2;
                    useModelOptics = 1;
                    opticsPPEffects[] = {"OpticsCHAbera2","OpticsBlur3"};
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_A.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_A.p3d"
                    };
                    opticsZoomMin = "0.25/6";
                    opticsZoomMax = "0.25/4";
                    opticsZoomInit = "0.25/4";
                    discreteinitIndex = 0;
                    discretefov[] = {"0.25/4","0.25/6"};
                    discreteDistanceInitIndex = 1;
                    distanceZoomMin = 300;
                    distanceZoomMax = 1200;
                    memoryPointCamera = "opticView";
                    visionMode[] = {};
                    thermalMode[] = {};
                    opticsFlare = 1;
                    opticsDisablePeripherialVision = 0;
                    cameraDir = "";
                };
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_20mw40;

    class Aux501_Weapons_Mags_20mw240: Aux501_Weapons_Mags_20mw40
    {
        displayName = "[501st] 240Rnd 20MW Cell";
        displayNameShort = "240Rnd 20MW";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_dc15l_ca.paa";
        count = 240;
        ammo = "Aux501_Weapons_Ammo_20mw";
        descriptionShort = "DC15L Box Magazine";
        model = "\MRC\JLTS\weapons\DC15S\DC15S_mag.p3d";
    };
};