class cfgPatches
{
    class Aux501_Patch_AAP4
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_AAP4_carry"
        };
        magazines[] = {};
    };
};

class cfgWeapons
{
    class Launcher_Base_F;

    class Aux501_Weaps_AAP4_carry: Launcher_Base_F
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] AAP4 'Striker'";
        author = "501st Aux Team";
        model = "\ls_weapons\tertiary\plx1\ls_weapon_plx1.p3d";
        picture = "\Aux501\Weapons\Republic\AAP4\data\textures\UI\striker_ui.paa";
        mass = 450;
        class ACE_CSW
        {
            type = "mount";
            deployTime = 1;
            pickupTime = 1;
            deploy = "Aux501_Vehicles_CE_Turret_Striker";
        };
    };
};