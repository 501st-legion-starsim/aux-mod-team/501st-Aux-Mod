class cfgPatches
{
    class Aux501_Patch_DP23
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_DP23"
        };
    };
};

class PointerSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class Aux501_stun_muzzle;
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_DP23: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] DP-23";
        baseWeapon = "Aux501_Weaps_DP23";
        picture = "\MRC\JLTS\weapons\DP23\data\ui\DP23_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DP23\DP23.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\DP23\anims\DP23_handanim.rtm"};
        reloadMagazineSound[] = {"swlw_rework\sounds\dc\15\DC15A_reload.wss",3,1,30};
        recoil = "JLTS_recoil_DP23";
        magazines[]=
        {
            "Aux501_Weapons_Mags_shotgun_scatter20"
        };
        modes[] = {"Single","aiclose","aimedium"};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"swlw_rework\sounds\shotgun\DP23_shot.wss",+3db,1,2200};
                begin2[] = {"swlw_rework\sounds\shotgun\DP23_shot.wss",+3db,1,2200};
                begin3[] = {"swlw_rework\sounds\shotgun\DP23_shot.wss",+3db,1,2200};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.34};
            };
            reloadTime = 0.5;
            dispersion = 0.00073;
            minRange = 1;
            minRangeProbab = 0.5;
            midRange = 30;
            midRangeProbab = 0.7;
            maxRange = 60;
            maxRangeProbab = 0.3;
        };
        class aiclose: Single
        {
            showToPlayer = 0;
            minRange = 50;
            minRangeProbab = 0.1;
            midRange = 130;
            midRangeProbab = 0.8;
            maxRange = 200;
            maxRangeProbab = 0.15;
            aiRateOfFire = 2;
            aiRateOfFireDistance = 200;
        };
        class aimedium: aiclose
        {
            minRange = 150;
            minRangeProbab = 0.1;
            midRange = 200;
            midRangeProbab = 0.7;
            maxRange = 300;
            maxRangeProbab = 0.15;
            aiRateOfFire = 4;
            aiRateOfFireDistance = 300;
        };
    };
};