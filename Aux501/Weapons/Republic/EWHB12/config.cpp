class cfgPatches
{
    class Aux501_Patch_EWHB12
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_EWHB12",
            "Aux501_Weaps_EWHB12_carry"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_EWHB12",
            "Aux501_Weapons_Mags_EWHB12_csw",

            "Aux501_Weapons_Mags_EWHB12_GL",
            "Aux501_Weapons_Mags_EWHB12_GL_csw"
        };
    };
};

class Mode_SemiAuto;

class cfgWeapons
{
    class Launcher_Base_F;
    class Aux501_Weaps_EWHB12_carry: Launcher_Base_F
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] EWHB-12 'Boomer'";
        author = "501st Aux Team";
        model = "SWLW_clones\machineguns\z7\z7.p3d";
        picture = "\Aux501\Weapons\Republic\EWHB12\data\textures\UI\Aux501_launcher_staticweapons_boomer_ca.paa";
        mass = 450;
        class ACE_CSW
        {
            type = "mount";
            deployTime = 1;
            pickupTime = 1;
            deploy = "Aux501_Vehicles_CE_Turret_Boomer";
        };
    };
};