class cfgPatches
{
    class Aux501_Patch_MAR1
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_MAR1_carry"
        };
        magazines[] = {};
        ammo[] = {};
    };
};

class cfgWeapons
{
    class Launcher_Base_F;
    class Aux501_Weaps_MAR1_carry: Launcher_Base_F
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] MAR1 'Driver'";
        author = "501st Aux Team";
        model = "ls_weapons\tertiary\rps6\ls_weapon_rps6.p3d";
        picture = "\Aux501\Weapons\Republic\MAR1\data\textures\UI\driver_ui.paa";
        mass = 450;
        class ACE_CSW
        {
            type = "mount";
            deployTime = 15;
            pickupTime = 1;
            deploy = "Aux501_Vehicles_CE_Turret_Driver";
        };
    };
};