class cfgPatches
{
    class Aux501_Patch_Z6
    {
        Author = "501st Aux Team";
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_Z6"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_10mw400"
        };
    };
};

class CfgSoundShaders
{
	class Aux501_SoundShader_Z6_closeShot
	{
		samples[] = 
        {
            {"3AS\3AS_Main\Sounds\Z6\Z61.ogg",1}
        };
		volume = "1";
		range = 150;
		rangeCurve = "closeShotCurve";
	};
	class Aux501_SoundShader_Z6_distShot
	{
		samples[] = 
        {
            {"3AS\3AS_Main\Sounds\Z6\Z61.ogg",1}
        };
		volume = "1";
		range = 2200;
		rangeCurve[] = {{0,0},{50,0},{300,1},{1800,1}};
	};
};

class CfgSoundSets
{
	class Aux501_SoundSet_Weapon_Z6
	{
		soundShaders[] = 
        {
            "Aux501_SoundShader_Z6_closeShot",
            "Aux501_SoundShader_Z6_distShot"
        };
		volumeFactor = 1.6;
		spatial = 1;
		doppler = 0;
		loop = 0;
	};
};

class Mode_FullAuto;
class MuzzleSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_Z6: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] Z-6 Rotary Blaster Cannon";
        baseWeapon = "Aux501_Weaps_Z6";
        picture = "\MRC\JLTS\weapons\Z6\data\ui\Z6_ui_ca.paa";
        model = "\MRC\JLTS\weapons\Z6\Z6.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\Z6\data\Z6_1_co.paa","\MRC\JLTS\weapons\Z6\data\Z6_2_co.paa"};
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\Z6\anims\Z6_handanim.rtm"};
        recoil = "recoil_lim";
        reloadAction = "ReloadOverheat_DLT";
        reloadMagazineSound[] = {"WarMantle\WM_Imperial_Weapons\data\sfx\overheat",5,1,30};
        magazines[] =
        {
            "Aux501_Weapons_Mags_10mw400"
        };
        modes[] = {"manual","Overcharge","close","short","medium","far_optic1","far_optic2"};
        class manual: Mode_FullAuto
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_Z6"};
            };
            reloadTime = 0.075;
            dispersion = 0.00102;
            soundContinuous = 0;
            soundBurst = 0;
            minRange = 0;
            minRangeProbab = 0.3;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.04;
            showToPlayer = 1;
        };
        class Overcharge: manual
        {
            dispersion = "0.00015*4";
            displayName = "Overcharge";
            reloadTime = "0.015";
            burst = 25;
            textureType = "fastAuto";
        };
        class close: manual
        {
            burst = 10;
            aiRateOfFire = 0.5;
            aiRateOfFireDistance = 50;
            minRange = 10;
            minRangeProbab = 0.05;
            midRange = 20;
            midRangeProbab = 0.7;
            maxRange = 50;
            maxRangeProbab = 0.04;
            showToPlayer = 0;
        };
        class short: close
        {
            burst = 8;
            aiRateOfFire = 2;
            aiRateOfFireDistance = 300;
            minRange = 50;
            minRangeProbab = 0.05;
            midRange = 150;
            midRangeProbab = 0.7;
            maxRange = 300;
            maxRangeProbab = 0.04;
        };
        class medium: close
        {
            burst = 7;
            aiRateOfFire = 4;
            aiRateOfFireDistance = 600;
            minRange = 200;
            minRangeProbab = 0.05;
            midRange = 300;
            midRangeProbab = 0.7;
            maxRange = 500;
            maxRangeProbab = 0.1;
        };
        class far_optic1: medium
        {
            requiredOpticType = 1;
            showToPlayer = 0;
            burst = 3;
            aiRateOfFire = 10;
            aiRateOfFireDistance = 1000;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 500;
            midRangeProbab = 0.4;
            maxRange = 650;
            maxRangeProbab = 0.01;
        };
        class far_optic2: far_optic1
        {
            burst = 3;
            requiredOpticType = 2;
            minRange = 400;
            minRangeProbab = 0.05;
            midRange = 750;
            midRangeProbab = 0.7;
            maxRange = 900;
            maxRangeProbab = 0.01;
            aiRateOfFire = 10;
            aiRateOfFireDistance = 900;
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_10mw50;

    class Aux501_Weapons_Mags_10mw400: Aux501_Weapons_Mags_10mw50
    {
        displayName = "[501st] 400Rnd 10MW Cell";
        displayNameShort = "400Rnd 10MW";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_z6_ca.paa";
        count = 400;
        ammo = "Aux501_Weapons_Ammo_10mw";
        descriptionShort = "Z6 Box Magazine";
        model = "\MRC\JLTS\weapons\z6\z6_mag.p3d";
    };
};