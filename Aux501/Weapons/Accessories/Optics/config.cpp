class CfgPatches
{
    class Aux501_Patch_Accessories
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] = 
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F_Acc",
            "A3_Ui_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_muzzle_flash",

            "Aux501_muzzle_surpressor",

            "Aux501_cows_rco",
            "Aux501_cows_rco_2",
            "Aux501_cows_rco_3",

            "Aux501_cows_mrco",
            "Aux501_cows_mrco_2",
            "Aux501_cows_mrco_3",

            "Aux501_cows_Holosight",
            "Aux501_cows_Holosight_2",
            "Aux501_cows_Holosight_3",

            "Aux501_cows_HoloScope",
            "Aux501_cows_HoloScope_2",
            "Aux501_cows_HoloScope_3",
            "Aux501_cows_HoloScope_4",
            "Aux501_cows_HoloScope_5",
            "Aux501_cows_HoloScope_6",

            "Aux501_cows_DMS",
            "Aux501_cows_DMS_2",
            "Aux501_cows_DMS_3",
            "Aux501_cows_DMS_4",

            "Aux501_cows_Holoscope_LR",
            "Aux501_cows_Holoscope_LR_2",
            "Aux501_cows_Holoscope_LR_3",
            "Aux501_cows_Holoscope_LR_4",
            "Aux501_cows_Holoscope_LR_5",
            "Aux501_cows_Holoscope_LR_6",
            "Aux501_cows_Holoscope_LR_7",

            "Aux501_cows_pistol",
            "Aux501_cows_pistol_2",

            "Aux501_cows_LRPS",

            "Aux501_cows_LEScope_DC15A",

            "Aux501_cows_reflex_optic"
        };
    };
};

class ScrollBar;
class RscControlsGroup
{
    class VScrollbar: ScrollBar{};
    class HScrollbar: ScrollBar{};
    class Controls{};
};

class RscText;

class RscInGameUI
{
    class RscUnitInfo;

    class Aux501_Weapon_RscOptics_Style1: RscUnitInfo
    {
        idd = 300;
        controls[] = {"Aux501_IGUI_elements_group"};
        class Aux501_IGUI_elements_group: RscControlsGroup
        {		
            idc = 170;
            class VScrollbar: VScrollbar
            {
                width = 0;
            };
            class HScrollbar: HScrollbar
            {
                height = 0;
            };
            x = "0 * 		(0.01875 * SafezoneH) + 		(SafezoneX + ((SafezoneW - SafezoneH) / 2))";
            y = "0 * 		(0.025 * SafezoneH) + 		(SafezoneY)";
            w = "53.5 * 		(0.01875 * SafezoneH)";
            h = "40 * 		(0.025 * SafezoneH)";
            class controls
            {
                class CA_Bracket: RscText
                {
                    idc = 181;
                    style = "0x30 + 0x800";
                    sizeEx = "0.035*SafezoneH";
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "IBL\weapons\E11\data\E11_reticle_indicator_green_ca";
                    x = "16.5 * 		(0.01875 * SafezoneH)";
                    y = "9.75 * 		(0.025 * SafezoneH)";
                    w = "20.5 * 		(0.01875 * SafezoneH)";
                    h = "20.5 * 		(0.025 * SafezoneH)";
                };
                class CA_Distance: RscText
                {
                    idc = 198;
                    style = 0;
                    sizeEx = "0.028*SafezoneH";
                    colorText[] = {0.95,0.9,0.45,1};
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "- - - -";
                    x = "35.5 * 		(0.01875 * SafezoneH)";
                    y = "19.1 * 		(0.025 * SafezoneH)";
                    w = "5.6 * 		(0.01875 * SafezoneH)";
                    h = "2 * 		(0.025 * SafezoneH)";
                };
                class CA_Heading: RscText
                {
                    idc = 156;
                    style = 2;
                    sizeEx = "0.028*SafezoneH";
                    colorText[] = {0.95,0.9,0.45,1};
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "-----";
                    x = "13.5 * 		(0.01875 * SafezoneH)";
                    y = "19.1 * 		(0.025 * SafezoneH)";
                    w = "5 * 		(0.01875 * SafezoneH)";
                    h = "2 * 		(0.025 * SafezoneH)";
                };
                class CA_OpticsZoom: RscText
                {
                    idc = 180;
                    style = 1;
                    sizeEx = "0.028*SafezoneH";
                    colorText[] = {0.95,0.9,0.45,1};
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "5.5";
                    x = "37.9 * 		(0.01875 * SafezoneH)";
                    y = "23.5 * 		(0.025 * SafezoneH)";
                    w = "4 * 		(0.01875 * SafezoneH)";
                    h = "1.5 * 		(0.025 * SafezoneH)";
                };
            };
        };
    };
    class Aux501_Weapon_RscOptics_Style2: Aux501_Weapon_RscOptics_Style1
    {
        idd = 300;
        controls[] = {"Aux501_IGUI_elements_group"};
        class Aux501_IGUI_elements_group: Aux501_IGUI_elements_group
        {		
            class controls: controls
            {
                class CA_Bracket: RscText
                {
                    idc = 181;
                    style = "0x30 + 0x800";
                    sizeEx = "0.035*SafezoneH";
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "IBL\weapons\E11\data\E11_reticle_indicator_green_ca";
                    x = "16.5 * 		(0.01875 * SafezoneH)";
                    y = "9.75 * 		(0.025 * SafezoneH)";
                    w = "20.5 * 		(0.01875 * SafezoneH)";
                    h = "20.5 * 		(0.025 * SafezoneH)";
                };
                class CA_Distance: RscText
                {
                    idc = 198;
                    style = 0;
                    sizeEx = "0.028*SafezoneH";
                    colorText[] = {0.95,0.9,0.45,1};
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "- - - -";
                    x = "35.4 * 		(0.01875 * SafezoneH)";
                    y = "18.5 * 		(0.025 * SafezoneH)";
                    w = "5.6 * 		(0.01875 * SafezoneH)";
                    h = "2 * 		(0.025 * SafezoneH)";
                };
                class CA_Heading: RscText
                {
                    idc = 156;
                    style = 2;
                    sizeEx = "0.028*SafezoneH";
                    colorText[] = {0.95,0.9,0.45,1};
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "-----";
                    x = "13.5 * 		(0.01875 * SafezoneH)";
                    y = "18.5 * 		(0.025 * SafezoneH)";
                    w = "5 * 		(0.01875 * SafezoneH)";
                    h = "2 * 		(0.025 * SafezoneH)";
                };
                class CA_OpticsZoom: RscText
                {
                    idc = 180;
                    style = 1;
                    sizeEx = "0.028*SafezoneH";
                    colorText[] = {0.95,0.9,0.45,1};
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "5.5";
                    x = "38.8 * 		(0.01875 * SafezoneH)";
                    y = "15.8 * 		(0.025 * SafezoneH)";
                    w = "4 * 		(0.01875 * SafezoneH)";
                    h = "1.5 * 		(0.025 * SafezoneH)";
                };
            };
        };
    };
    class Aux501_Weapon_RscOptics_Style3: RscUnitInfo
    {
        idd = 300;
        controls[] = {"Aux501_IGUI_elements_group"};
        class Aux501_IGUI_elements_group: RscControlsGroup
        {		
            idc = 170;
            class VScrollbar: VScrollbar
            {
                width = 0;
            };
            class HScrollbar: HScrollbar
            {
                height = 0;
            };
            x = "0 * 		(0.01875 * SafezoneH) + 		(SafezoneX + ((SafezoneW - SafezoneH) / 2))";
            y = "0 * 		(0.025 * SafezoneH) + 		(SafezoneY)";
            w = "53.5 * 		(0.01875 * SafezoneH)";
            h = "40 * 		(0.025 * SafezoneH)";
            class controls
            {
                class CA_Bracket: RscText
                {
                    idc = 181;
                    style = "0x30 + 0x800";
                    sizeEx = "0.035*SafezoneH";
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "IBL\weapons\E11\data\E11_reticle_indicator_green_ca";
                    x = "16.5 * 		(0.01875 * SafezoneH)";
                    y = "9.75 * 		(0.025 * SafezoneH)";
                    w = "20.5 * 		(0.01875 * SafezoneH)";
                    h = "20.5 * 		(0.025 * SafezoneH)";
                };
                class CA_Distance: RscText
                {
                    idc = 198;
                    style = 0;
                    sizeEx = "0.028*SafezoneH";
                    colorText[] = {0.95,0.9,0.45,1};
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "- - - -";
                    x = "37.5 * 		(0.01875 * SafezoneH)";
                    y = "19.1 * 		(0.025 * SafezoneH)";
                    w = "5.6 * 		(0.01875 * SafezoneH)";
                    h = "2 * 		(0.025 * SafezoneH)";
                };
                class CA_Heading: RscText
                {
                    idc = 156;
                    style = 2;
                    sizeEx = "0.028*SafezoneH";
                    colorText[] = {0.95,0.9,0.45,1};
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "-----";
                    x = "10.5 * 		(0.01875 * SafezoneH)";
                    y = "19.1 * 		(0.025 * SafezoneH)";
                    w = "5 * 		(0.01875 * SafezoneH)";
                    h = "2 * 		(0.025 * SafezoneH)";
                };
                class CA_OpticsZoom: RscText
                {
                    idc = 180;
                    style = 1;
                    sizeEx = "0.028*SafezoneH";
                    colorText[] = {0.95,0.9,0.45,1};
                    shadow = 0;
                    font = "JLTS_republic";
                    text = "5.5";
                    x = "40.0 * 		(0.01875 * SafezoneH)";
                    y = "23.5 * 		(0.025 * SafezoneH)";
                    w = "4 * 		(0.01875 * SafezoneH)";
                    h = "1.5 * 		(0.025 * SafezoneH)";
                };
            };
        };
    };
    
};

class CfgWeapons
{
    class InventoryFlashLightItem_Base_F;
    class InventoryMuzzleItem_Base_F;
    class InventoryOpticsItem_Base_F;
    class ItemCore;

    class optic_DMS: ItemCore
    {
        class ItemInfo;
    };
    class optic_Hamr: ItemCore
    {
        class ItemInfo;
    };
    class optic_mrco: ItemCore
    {
        class ItemInfo;
    };
    class optic_Holosight: ItemCore
    {
        class ItemInfo;
    };
    class optic_MRD: ItemCore
    {
        class ItemInfo;
    };

    class Aux501_cows_rco: optic_Hamr
    {
        author = "501st Aux Team";
        scope = 2;
        scopearsenal = 2;
        displayName = "Short Range Scope A";
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_sr_01_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_hamr_1.p3d";
        descriptionShort = "HAMR SR 2-4x Scope";
        weaponInfoType = "RscOptics_sos";
        class ItemInfo: ItemInfo
        {
            mass = 8;
            opticType = 1;
            optics = 1;
            modelOptics = "\Aux501\Weapons\Accessories\data\Aux501_hamr_scope_1";
            class OpticsModes
            {
                class Hamr2Collimator
                {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsPPEffects[] = {"Default"};
                    opticsFlare = 0;
                    opticsDisablePeripherialVision = 0;
                    opticsZoomMin = 0.25;
                    opticsZoomMax = 1.25;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "eye";
                    visionMode[] = {};
                    distanceZoomMin = 200;
                    distanceZoomMax = 200;
                };
                class Hamr2Scope
                {
                    opticsID = 2;
                    useModelOptics = 0;
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    opticsFlare = 1;
                    opticsDisablePeripherialVision = 1;
                    opticsZoomMin = "0.25/2";
                    opticsZoomMax = "0.25/4";
                    opticsZoomInit = "0.25/2";
                    discreteinitIndex = 0;
                    discretefov[] = {"0.25/2","0.25/4"};
                    discreteDistanceInitIndex = 1;
                    memoryPointCamera = "opticView";
                    visionMode[] = {};
                    distanceZoomMin = 300;
                    distanceZoomMax = 300;
                };
            };
        };
        inertia = 0.1;
    };
    class Aux501_cows_rco_2: Aux501_cows_rco
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_sr_02_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_hamr_2.p3d";
        displayName = "Short Range Scope B";
        class ItemInfo: ItemInfo
        {
            modelOptics = "\Aux501\Weapons\Accessories\data\Aux501_hamr_scope_2";
        };
    };
    class Aux501_cows_rco_3: Aux501_cows_rco
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_sr_03_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_hamr_3.p3d";
        displayName = "Short Range Scope C";
        class ItemInfo: ItemInfo
        {
            modelOptics = "\Aux501\Weapons\Accessories\data\Aux501_hamr_scope_3";
        };
    };
    
    class Aux501_cows_mrco: optic_mrco
    {
        author = "501st Aux Team";
        scope = 2;
        scopearsenal = 2;
        displayName = "Medium Range Scope A";
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_mr_01_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_mrco_1.p3d";
        descriptionShort = "MRCO MR 2-6x Scope";
        weaponInfoType = "RscOptics_sos";
        class ItemInfo: ItemInfo
        {
            mass = 8;
            opticType = 1;
            optics = 1;
            modelOptics = "\Aux501\Weapons\Accessories\data\Aux501_mrco_scope_1.p3d";
            class OpticsModes
            {
                class MRCOcq
                {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsPPEffects[] = {"Default"};
                    opticsFlare = 0;
                    opticsDisablePeripherialVision = 0;
                    opticsZoomMin = 0.25;
                    opticsZoomMax = 1.25;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "eye";
                    visionMode[] = {};
                    distanceZoomMin = 300;
                    distanceZoomMax = 300;
                };
                class MRCOscope
                {
                    opticsID = 2;
                    useModelOptics = 0;
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    opticsFlare = 1;
                    opticsDisablePeripherialVision = 1;
                    opticsZoomMin = "0.25/6";
                    opticsZoomMax = "0.25/2";
                    opticsZoomInit = "0.25/2";
                    discreteinitIndex = 0;
                    discretefov[] = {"0.25/2","0.25/6"};
                    discreteDistanceInitIndex = 1;
                    memoryPointCamera = "opticView";
                    visionMode[] = {};
                    distanceZoomMin = 300;
                    distanceZoomMax = 300;
                };
            };
        };
        inertia = 0.1;
    };
    class Aux501_cows_mrco_2: Aux501_cows_mrco
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_mr_02_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_mrco_2.p3d";
        displayName = "Medium Range Scope B";
        class ItemInfo: ItemInfo
        {
            modelOptics = "\Aux501\Weapons\Accessories\data\Aux501_MRCO_scope_2";
        };
    };
    class Aux501_cows_mrco_3: Aux501_cows_mrco
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_mr_03_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_mrco_3.p3d";
        displayName = "Medium Range Scope C";
        class ItemInfo: ItemInfo
        {
            modelOptics = "\Aux501\Weapons\Accessories\data\Aux501_MRCO_scope_3";
        };
    };
    
    class Aux501_cows_Holosight: optic_Holosight
    {
        author = "501st Aux Team";
        scope = 2;
        scopearsenal = 2;
        displayName = "Holo Sight A";
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_holo_01_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_holo_1.p3d";
        descriptionShort = "Holo 1x Scope";
        weaponInfoType = "RscWeaponZeroing";
        class ItemInfo: ItemInfo
        {
            mass = 6;
            modelOptics = "\A3\Weapons_F\empty";
            optics = 1;
            class OpticsModes
            {
                class aco
                {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsZoomMin = 0.25;
                    opticsZoomMax = 1.25;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "eye";
                    opticsFlare = 0;
                    opticsDisablePeripherialVision = 0;
                    distanceZoomMin = 200;
                    distanceZoomMax = 200;
                    cameraDir = "";
                    visionMode[] = {};
                    opticsPPEffects[] = {"OpticsBlur1"};
                };
            };
        };
        inertia = 0;
    };
    class Aux501_cows_Holosight_2: Aux501_cows_Holosight
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_holo_02_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_holo_2.p3d";
        displayName = "Holo Sight B";
    };
    class Aux501_cows_Holosight_3: Aux501_cows_Holosight
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_holo_03_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_holo_3.p3d";
        displayName = "Holo Sight C";
    };
    
    class Aux501_cows_HoloScope: Aux501_cows_Holosight
    {
        author = "501st Aux Team";
        scope = 2;
        scopearsenal = 2;
        displayName = "Holo Scope A";
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_holo_01_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_holo_1.p3d";
        descriptionShort = "Holo 1x Scope";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style1";
        class ItemInfo: ItemInfo
        {
            mass = 6;
            modelOptics = "\A3\Weapons_F\acc\reticle_sniper_F";
            optics = 1;
            class OpticsModes
            {
                class aco
                {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsZoomMin = 0.25;
                    opticsZoomMax = 1.25;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "eye";
                    opticsFlare = 0;
                    opticsDisablePeripherialVision = 0;
                    distanceZoomMin = 200;
                    distanceZoomMax = 200;
                    cameraDir = "";
                    visionMode[] = {};
                    opticsPPEffects[] = {"OpticsBlur1"};
                };
                class Scope: aco
                {
                    opticsID = 2;
                    useModelOptics = 1;
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    opticsDisablePeripherialVision = 0.67;
                    opticsZoomMin = 0.125;
                    opticsZoomMax = 0.125;
                    opticsZoomInit = 0.125;
                    memoryPointCamera = "opticView";
                    opticsFlare = "true";
                    distanceZoomMin = 100;
                    distanceZoomMax = 100;
                    cameraDir = "";
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_B.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_B.p3d"
                    };
                };
            };
            inertia = 0;
        };
    };
    class Aux501_cows_HoloScope_2: Aux501_cows_HoloScope
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_holo_02_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_holo_2.p3d";
        displayName = "Holo Scope B";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class aco: aco{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_C.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_C.p3d"
                    };
                };
            };
        };
    };
    class Aux501_cows_HoloScope_3: Aux501_cows_HoloScope
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_holo_03_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_holo_3.p3d";
        displayName = "Holo Scope C";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class aco: aco{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_D.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_D.p3d"
                    };
                };
            };
        };
    };
    class Aux501_cows_HoloScope_4: Aux501_cows_HoloScope
    {
        displayName = "Holo Scope D";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style2";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class aco: aco{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_F.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_F.p3d"
                    };
                };
            };
        };
    };
    class Aux501_cows_HoloScope_5: Aux501_cows_HoloScope_2
    {
        displayName = "Holo Scope E";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style2";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class aco: aco{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_G.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_G.p3d"
                    };
                };
            };
        };
    };
    class Aux501_cows_HoloScope_6: Aux501_cows_HoloScope_3
    {
        displayName = "Holo Scope F";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style2";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class aco: aco{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_H.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_H.p3d"
                    };
                };
            };
        };
    };
    
    class Aux501_cows_DMS: optic_DMS
    {
        author = "501st Aux Team";
        scope = 2;
        scopearsenal = 2;
        displayName = "DMS LR 4-6x Scope A";
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_lr_01_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_dms.p3d";
        descriptionShort = "Long Range Scope";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style1";
        class ItemInfo: ItemInfo
        {
            mass = 12;
            opticType = 2;
            optics = 1;
            modelOptics = "\A3\Weapons_F\acc\reticle_sniper_F";
            class OpticsModes
            {
                class Ironsights
                {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsFlare = "true";
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    opticsDisablePeripherialVision = 0.67;
                    opticsZoomMin = 0.25;
                    opticsZoomMax = 1.1;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "eye";
                    visionMode[] = {};
                    distanceZoomMin = 100;
                    distanceZoomMax = 100;
                };
                class Scope: Ironsights
                {
                    opticsID = 2;
                    useModelOptics = 1;
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_B.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_B.p3d"
                    };
                    opticsDisablePeripherialVision = 0.67;
                    opticsZoomMin = "0.25/6";
                    opticsZoomMax = "0.25/4";
                    opticsZoomInit = "0.25/4";
                    discreteinitIndex = 0;
                    discretefov[] = {"0.25/4","0.25/6"};
                    distanceZoomMin = 200;
                    distanceZoomMax = 200;
                    memoryPointCamera = "opticView";
                    visionMode[] = {};
                    opticsFlare = "true";
                    cameraDir = "";
                };
            };
        };
        inertia = 0.2;
    };
    class Aux501_cows_DMS_2: Aux501_cows_DMS
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_lr_02_ca.paa";
        displayName = "DMS LR 4-6x Scope B";
        model = "\Aux501\Weapons\Accessories\data\Aux501_dms_2.p3d";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class Ironsights: Ironsights{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_C.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_C.p3d"
                    };
                };
            };
        };
    };
    class Aux501_cows_DMS_3: Aux501_cows_DMS
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_lr_03_ca.paa";
        displayName = "DMS LR 4-6x Scope C";
        model = "\Aux501\Weapons\Accessories\data\Aux501_dms_3.p3d";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class Ironsights: Ironsights{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_D.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_D.p3d"
                    };
                };
            };
        };
    };
    class Aux501_cows_DMS_4: Aux501_cows_DMS
    {
        displayName = "DMS LR 4-6x Scope D";
        model = "\Aux501\Weapons\Accessories\data\Aux501_dms_4.p3d";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class Ironsights: Ironsights{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_A.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_A.p3d"
                    };
                };
            };
        };
    };
    class Aux501_cows_DMS_5: Aux501_cows_DMS
    {
        displayName = "DMS LR 4-6x Scope E";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style2";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class Ironsights: Ironsights{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_F.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_F.p3d"
                    };
                };
            };
        };
    };
    class Aux501_cows_DMS_6: Aux501_cows_DMS_2
    {
        displayName = "DMS LR 4-6x Scope F";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style2";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class Ironsights: Ironsights{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_G.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_G.p3d"
                    };
                };
            };
        };
    };
    class Aux501_cows_DMS_7: Aux501_cows_DMS_3
    {
        displayName = "DMS LR 4-6x Scope G";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style2";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class Ironsights: Ironsights{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_H.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_H.p3d"
                    };
                };
            };
        };
    };
    class Aux501_cows_DMS_8: Aux501_cows_DMS_4
    {
        displayName = "DMS LR 4-6x Scope H";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style2";
        class ItemInfo: ItemInfo
        {
            class OpticsModes: OpticsModes
            {
                class Ironsights: Ironsights{};
                class Scope: Scope
                {
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_E.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_Reticle_E.p3d"
                    };
                };
            };
        };
    };

    class Aux501_cows_pistol: optic_MRD
    {
        author = "501st Aux Team";
        scope = 2;
        scopearsenal = 2;
        displayName = "Pistol Holo A";
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_pistol_01_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_pistol_1.p3d";
        descriptionShort = "Pistol Scope";
        class ItemInfo: ItemInfo
        {
            mass = 2;
            modelOptics = "\A3\Weapons_F\empty";
            optics = 1;
            class OpticsModes
            {
                class aco
                {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsZoomMin = 0.25;
                    opticsZoomMax = 1.25;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "eye";
                    opticsFlare = 0;
                    opticsDisablePeripherialVision = 0;
                    distanceZoomMin = 50;
                    distanceZoomMax = 50;
                    cameraDir = "";
                    visionMode[] = {};
                    opticsPPEffects[] = {"Default"};
                };
            };
        };
        inertia = 0;
    };
    class Aux501_cows_pistol_2: Aux501_cows_pistol
    {
        picture = "\Aux501\Weapons\Accessories\data\Aux501_ico_pistol_03_ca.paa";
        model = "\Aux501\Weapons\Accessories\data\Aux501_pistol_3.p3d";
        displayName = "Pistol Holo B";
    };
    
    class Aux501_cows_LRPS: optic_DMS
    {
        author = "501st Aux Team";
        scope = 2;
        scopearsenal = 2;
        displayName = "LRPS LR 12-20x Scope";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style2";
        class ItemInfo: ItemInfo
        {
            mass = 16;
            opticType = 2;
            weaponInfoType = "RscWeaponRangeZeroingFOV";
            optics = 1;
            modelOptics = "\A3\Weapons_F\acc\reticle_sniper_F";
            class OpticsModes
            {
                class Snip
                {
                    opticsID = 1;
                    opticsDisplayName = "WFOV";
                    useModelOptics = 1;
                    opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
                    opticsZoomMin = 0.01;
                    opticsZoomMax = 0.042;
                    opticsZoomInit = 0.042;
                    discreteDistance[] = {300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400};
                    discreteDistanceInitIndex = 2;
                    distanceZoomMin = 300;
                    distanceZoomMax = 2400;
                    discretefov[] = {0.042,0.01};
                    discreteInitIndex = 0;
                    memoryPointCamera = "opticView";
                    modelOptics[] = {"\A3\Weapons_F\acc\reticle_lrps_F","\A3\Weapons_F\acc\reticle_lrps_z_F"};
                    visionMode[] = {};
                    thermalMode[] = {4};
                    opticsFlare = 1;
                    opticsDisablePeripherialVision = 1;
                    cameraDir = "";
                };
                class Iron: Snip
                {
                    opticsID = 2;
                    useModelOptics = 0;
                    opticsPPEffects[] = {"",""};
                    opticsFlare = 0;
                    opticsDisablePeripherialVision = 0;
                    opticsZoomMin = 0.25;
                    opticsZoomMax = 1.25;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "eye";
                    visionMode[] = {};
                    discretefov[] = {};
                    distanceZoomMin = 200;
                    distanceZoomMax = 200;
                    discreteDistance[] = {200};
                    discreteDistanceInitIndex = 0;
                };
            };
        };
    };
    
    class Aux501_cows_LEScope_DC15A: optic_DMS
    {
        author = "501st Aux Team";
        scope = 2;
        picture = "\MRC\JLTS\weapons\DC15X\data\ui\DC15X_scope_ui_ca.paa";
        displayName = "DC-15LE Scope";
        model = "\3AS\3AS_Weapons\DC15A\3AS_DC15A_LE_Scope.p3d";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style1";
        class ItemInfo: ItemInfo
        {
            mass = 16;
            opticType = 2;
            optics = 1;
            modelOptics = "\A3\Weapons_F\acc\reticle_sniper_F";
            class OpticsModes
            {
                class IronSights
                {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsFlare = "true";
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    opticsDisablePeripherialVision = 0.67;
                    opticsZoomMin = 0.375;
                    opticsZoomMax = 1.1;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "eye";
                    visionMode[] = {};
                    distanceZoomMin = 100;
                    distanceZoomMax = 100;
                };
                class Scope: IronSights
                {
                    opticsID = 2;
                    useModelOptics = 1;
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_A.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_A.p3d"
                    };
                    opticsDisablePeripherialVision = 0.67;
                    opticsZoomMin = 0.03125;
                    opticsZoomMax = 0.0625;
                    opticsZoomInit = 0.0625;
                    memoryPointCamera = "opticView";
                    opticsFlare = "true";
                    distanceZoomMin = 100;
                    distanceZoomMax = 100;
                    cameraDir = "";
                };
            };
        };
    };
    
    class Aux501_cows_reflex_optic: ItemCore
    {
        scope = 2;
        author = "501st Aux Team";
        displayName = "Reflex Optic";
        picture = "\A3\Weapons_F_EPA\Acc\data\UI\gear_acco_yorris_CA.paa";
        model = "\3AS\3AS_Weapons\DC15C\3AS_DC15C_Reflex_f.p3d";
        weaponInfoType = "Aux501_Weapon_RscOptics_Style1";
        class ItemInfo: InventoryOpticsItem_Base_F
        {
            mass = 6;
            modelOptics = "\A3\Weapons_F\acc\reticle_sniper_F";
            optics = 1;
            class OpticsModes
            {
                class aco
                {
                    opticsID = 1;
                    useModelOptics = 0;
                    opticsZoomMin = 0.25;
                    opticsZoomMax = 1.25;
                    opticsZoomInit = 0.75;
                    memoryPointCamera = "eye";
                    opticsFlare = 0;
                    opticsDisablePeripherialVision = 0;
                    distanceZoomMin = 200;
                    distanceZoomMax = 200;
                    cameraDir = "";
                    visionMode[] = {};
                    opticsPPEffects[] = {"OpticsBlur1"};
                };
                class Scope: aco
                {
                    opticsID = 2;
                    useModelOptics = 1;
                    opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                    opticsDisablePeripherialVision = 0.67;
                    opticsZoomMin = 0.125;
                    opticsZoomMax = 0.125;
                    opticsZoomInit = 0.125;
                    memoryPointCamera = "opticView";
                    modelOptics[] = 
                    {
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_A.p3d",
                        "Aux501\Weapons\Accessories\Optics\data\Aux501_SR_Reticle_A.p3d"
                    };
                    visionMode[] = {};
                    opticsFlare = "true";
                    distanceZoomMin = 100;
                    distanceZoomMax = 100;
                    cameraDir = "";
                };
            };
        };
    };
};