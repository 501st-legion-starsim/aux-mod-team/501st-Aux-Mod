class CfgPatches
{
    class Aux501_Patch_Accessories_Pointers
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] = 
        {
            "Aux501_Patch_Weapons"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_pointer_Flaslight",
            "Aux501_pointer_Laserpointer"
        };
    };
};

class CfgWeapons
{
    class InventoryFlashLightItem_Base_F;
    class ItemCore;
    class acc_flashlight;

    class Aux501_pointer_Flaslight: acc_flashlight
    {
        author = "501st Aux Team";
        scope = 2;
        displayName = "[501st] Blaster Flashlight";
        picture = "\MRC\JLTS\optionals\Glocko\data\ui\Glocko_flash_ui_ca.paa";
        model = "\MRC\JLTS\optionals\Glocko\Glocko_flash.p3d";
    };
    class Aux501_pointer_Laserpointer: ItemCore
    {
        author = "501st Aux Team";
        scope = 2;
        displayName = "[501st] Blaster Laser Pointer";
        model = "SWLW_merc_mando\pistols\westar35sa\westar35sa_laser.p3d";
        picture = "\SWLW_merc_mando\pistols\westar35sa\data\ui\SWLW_westar35sa_laser_ui.paa";
        class ItemInfo: InventoryFlashLightItem_Base_F
        {
            mass = 6;
            class Pointer
            {
                irLaserPos = "flash dir";
                irLaserEnd = "flash";
                irDistance = 5;
            };
            class FlashLight{};
        };
    };
};