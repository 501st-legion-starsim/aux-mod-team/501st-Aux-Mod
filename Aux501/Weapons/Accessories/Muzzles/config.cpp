class CfgPatches
{
    class Aux501_Patch_Accessories_Muzzles
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] = 
        {
            "Aux501_Patch_Weapons"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_muzzle_flash",
            "Aux501_muzzle_flash_changer_blue",
            "Aux501_muzzle_surpressor"
        };
    };
};

class CfgWeapons
{
    class InventoryMuzzleItem_Base_F;
    class ItemCore;

    class ACE_muzzle_mzls_H;

    class muzzle_snds_338_black;

    class acc_flashlight;

    class Aux501_Muzzleflash_Blue: ItemCore
	{
		scope = 1;
		model = "\MRC\JLTS\weapons\Core\effects\muzzleflash_pistol_blue.p3d";
	};
    class Aux501_Muzzleflash_Red: Aux501_Muzzleflash_Blue
	{
		scope = 1;
		model = "\MRC\JLTS\weapons\Core\effects\muzzleflash_pistol_red.p3d";
	};
    class Aux501_Muzzleflash_Yellow: Aux501_Muzzleflash_Blue
	{
		scope = 1;
		model = "\MRC\JLTS\weapons\Core\effects\muzzleflash_pistol_yellow.p3d";
	};
    class Aux501_Muzzleflash_Orange: Aux501_Muzzleflash_Blue
	{
		scope = 1;
		model = "\MRC\JLTS\weapons\Core\effects\muzzleflash_pistol_orange.p3d";
	};

    class Aux501_muzzle_flash: ACE_muzzle_mzls_H
    {
        scope = 2;
        scopeInArsenal = 2;
        author = "501st Aux Team";
        displayName = "Flash Hider Chip";
        model = "";
        picture = "\Aux501\Weapons\Accessories\data\icon_white_chip_64_ca.paa";
        class ItemInfo: InventoryMuzzleItem_Base_F
        {
            mass = 10;
            class MagazineCoef
            {
                initSpeed = 1.0;
            };
            class AmmoCoef
            {
                hit = 1;
                typicalSpeed = 1;
                airFriction = 1;
                visibleFire = 1;
                audibleFire = 1;
                visibleFireTime = 1;
                audibleFireTime = 1;
                cost = 1.0;
            };
            soundTypeIndex = 0;
            muzzleEnd = "zaslehPoint";
            alternativeFire = "Zasleh2";
            class MuzzleCoef
            {
                dispersionCoef = 1.0;
                artilleryDispersionCoef = 1.0;
                fireLightCoef = 0.1;
                recoilCoef = 1.0;
                recoilProneCoef = 1.0;
                minRangeCoef = 1.0;
                minRangeProbabCoef = 1.0;
                midRangeCoef = 1.0;
                midRangeProbabCoef = 1.0;
                maxRangeCoef = 1.0;
                maxRangeProbabCoef = 1.0;
            };
        };
        inertia = 0.1;
    };
    class Aux501_muzzle_flash_changer_blue: Aux501_muzzle_flash
    {
        displayName = "Flash Changer Chip";
        picture = "\Aux501\Weapons\Accessories\data\icon_blue_chip_64_ca.paa";
        class ItemInfo: ItemInfo
        {
            muzzleEnd = "zaslehPoint";
            alternativeFire = "Aux501_Muzzleflash_Blue";
            class MuzzleCoef: MuzzleCoef
            {
                fireLightCoef = 1.0;
            };
        };
    };
    class Aux501_muzzle_flash_changer_red: Aux501_muzzle_flash_changer_blue
    {
        picture = "\Aux501\Weapons\Accessories\data\icon_red_chip_64_ca.paa";
        class ItemInfo: ItemInfo
        {
            alternativeFire = "Aux501_Muzzleflash_Red";
        };
    };
    class Aux501_muzzle_flash_changer_yellow: Aux501_muzzle_flash_changer_blue
    {
        picture = "\Aux501\Weapons\Accessories\data\icon_yellow_chip_64_ca.paa";
        class ItemInfo: ItemInfo
        {
            alternativeFire = "Aux501_Muzzleflash_Yellow";
        };
    };
    class Aux501_muzzle_flash_changer_orange: Aux501_muzzle_flash_changer_blue
    {
        picture = "\Aux501\Weapons\Accessories\data\icon_orange_chip_64_ca.paa";
        class ItemInfo: ItemInfo
        {
            alternativeFire = "Aux501_Muzzleflash_Orange";
        };
    };

    class Aux501_muzzle_surpressor: muzzle_snds_338_black
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "Surpressor Chip";
        author = "501st Aux Team";
        picture = "\Aux501\Weapons\Accessories\data\icon_gray_chip_64_ca.paa";
        model = "";
        class ItemInfo: InventoryMuzzleItem_Base_F
        {
            soundTypeIndex = 1;
            class MagazineCoef
            {
                initSpeed = 0.67;
            };
            class AmmoCoef
            {
                hit = 1;
                visibleFire = 0.1;
                audibleFire = 0.1;
                visibleFireTime = 1;
                audibleFireTime = 1;
                cost = 1;
                typicalSpeed = 1;
                airFriction = 1;
            };
            muzzleEnd = "zaslehPoint";
            alternativeFire = "Zasleh2";
            class MuzzleCoef
            {
                dispersionCoef = 1.0f;
                artilleryDispersionCoef = 1.0f;
                fireLightCoef = 0.1f;
                recoilCoef = 1.0f;
                recoilProneCoef = 1.0f;
                minRangeCoef = 1.0f;
                minRangeProbabCoef = 1.0f;
                midRangeCoef = 1.0f;
                midRangeProbabCoef = 1.0f;
                maxRangeCoef = 1.0f;
                maxRangeProbabCoef = 1.0f;
            };
        };
    };
};