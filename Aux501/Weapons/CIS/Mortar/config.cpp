class cfgPatches
{
    class Aux501_Patch_60mm_mortar_cis
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_60mm_plasma_Mortar"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_60mm_mortar_HE",
            "Aux501_Weapons_Mags_60mm_mortar_smoke",
            "Aux501_Weapons_Mags_60mm_mortar_illum"
        };
    };
};

class Mode_Burst;

class cfgWeapons
{
    class Aux501_Weaps_81mm_plasma_Mortar;
    
    class Aux501_Weaps_60mm_plasma_Mortar: Aux501_Weaps_81mm_plasma_Mortar
    {
        displayname = "[CIS] 60mm Plasma Mortar";
        magazines[] =
        {
            "Aux501_Weapons_Mags_60mm_mortar_HE",
            "Aux501_Weapons_Mags_60mm_mortar_smoke",
            "Aux501_Weapons_Mags_60mm_mortar_illum"
        };
        class Burst1: Mode_Burst
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_82mm_Burst10";
            sounds[] = {"StandardSound"};
            class StandardSound
            {
                begin1[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot1.wss",db+5,1,2200};
                begin2[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot2.wss",db+5,1,2200};
                begin3[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot3.wss",db+5,1,2200};
                begin4[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot4.wss",db+5,1,2200};
                begin5[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot5.wss",db+5,1,2200};
                soundBegin[] = {"begin1",0.15,"begin2",0.25,"begin3",0.1,"begin4",0.3,"begin5",0.2};
            };
            reloadSound[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_reload.wss",1,1,20};
            soundServo[] = {"",0.0001,1};
            soundBurst = 0;
            burst = 4;
            aiBurstTerminable = 1;
            reloadTime = 3;
            dispersion = 0.02;
            artilleryDispersion = 2;
            artilleryCharge = 0.01;
            minRange = 15;
            minRangeProbab = 0.5;
            midRange = 290;
            midRangeProbab = 0.7;
            maxRange = 665;
            maxRangeProbab = 0.5;
        };
        class Burst2: Burst1
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_82mm_Burst20";
            artilleryCharge = 0.7;
            minRange = 15;
            minRangeProbab = 0.4;
            midRange = 1175;
            midRangeProbab = 0.6;
            maxRange = 2660;
            maxRangeProbab = 0.4;
        };
        class Burst3: Burst1
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_82mm_Burst30";
            artilleryCharge = 1;
            artilleryDispersion = 15;
            minRange = 540;
            minRangeProbab = 0.3;
            midRange = 2355;
            midRangeProbab = 0.4;
            maxRange = 5500;
            maxRangeProbab = 0.3;
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_81mm_mortar_HE;
    class Aux501_Weapons_Mags_81mm_mortar_smoke;
    class Aux501_Weapons_Mags_81mm_mortar_illum;

    class Aux501_Weapons_Mags_60mm_mortar_HE: Aux501_Weapons_Mags_81mm_mortar_HE
    {
        displayName = "[501st] 60mm HE Rounds";
        displayNameShort = "60mm HE";
        ammo = "Aux501_Weapons_Ammo_60mm_HE";
        count = 20;
    };
    class Aux501_Weapons_Mags_60mm_mortar_smoke: Aux501_Weapons_Mags_81mm_mortar_smoke
    {
        displayName = "[501st] 60mm Smoke Rounds";
        displayNameShort = "60mm Smoke";
        count = 20;
    };
    class Aux501_Weapons_Mags_60mm_mortar_illum: Aux501_Weapons_Mags_81mm_mortar_illum
    {
        displayName = "[501st] 60mm Illum Rounds";
        displayNameShort = "60mm Illum";
        ammo = "Aux501_Weapons_Ammo_60mm_flare";
        count = 20;
    };
};

class cfgAmmo
{
    class Aux501_Weapons_Ammo_81mm_HE;
    class Aux501_Weapons_Ammo_81mm_flare;
    
    class Aux501_Weapons_Ammo_60mm_HE: Aux501_Weapons_Ammo_81mm_HE
    {
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Red.p3d";
        lightcolor[] = {0.5,0.25,0.25};
        effectFly = "Aux501_particle_effect_artillery_fly_red";
        intensity = 10000;
        tracerScale = 5;
        hit = 100;
        indirectHit = 40;
        indirectHitRange = 15;
        cost = 100;
        dangerRadiusHit = 70;
        suppressionRadiusHit = 50;
        aiAmmoUsageFlags = "64 + 128 + 512";
        soundFly[] = {"\SWLB_core\data\sounds\vehicles\mortar\weapon\mortar_fly.wss",3,1,2000};
    };
    class Aux501_Weapons_Ammo_60mm_flare: Aux501_Weapons_Ammo_81mm_flare
    {
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Red.p3d";
        lightcolor[] = {0.5,0.25,0.25};
    };
};