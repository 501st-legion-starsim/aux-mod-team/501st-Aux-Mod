class cfgPatches
{
    class Aux501_Patch_E5
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_E5",
            "Aux501_Weaps_E5_shield",

            "Aux501_Weaps_E5_Special",
            "Aux501_Weaps_E5_Special_shield"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_E5_Blaster"
        };
    };
};

class CowsSlot;
class UnderBarrelSlot;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless : Aux501_rifle_base
    {
        class FullAuto;
        class Single;
        class single_medium; 
        class single_far;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_E5: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[CIS] E5";
        baseWeapon = "Aux501_Weaps_E5";
        picture = "\MRC\JLTS\weapons\E5\data\ui\E5_ui_ca.paa";
        JLTS_canHaveShield = 1;
        JLTS_shieldedWeapon = "Aux501_Weaps_E5_shield";
        model = "\MRC\JLTS\weapons\E5\E5.p3d";
        reloadAction = "ReloadOverheat_E11";
        handAnim[]=
        {
            "OFP2_ManSkeleton",
            "\MRC\JLTS\weapons\E5\anims\E5_handanim.rtm"
        };
        recoil = "recoil_mxm";
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        magazines[]=
        {
            "Aux501_Weapons_Mags_E5100"
        };
        reloadMagazineSound[] = {"swlw_rework\sounds\e-series\E5_reload.wss",3,1,30};
        modes[] = {"FullAuto","Single","single_medium","single_far"};
        class FullAuto: FullAuto
        {
            reloadTime = 0.096;
            dispersion = 0.002;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\E5\sounds\E5_fire_2",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\E5\sounds\E5_fire_2",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\E5\sounds\E5_fire_2",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\E5\sounds\E5_fire_2",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class single_medium: single_medium
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\E5\sounds\E5_fire_2",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\E5\sounds\E5_fire_2",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class single_far: single_far
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\E5\sounds\E5_fire_2",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\E5\sounds\E5_fire_2",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Weapons_E5";
            variant = "standard";
        };
    };
    class Aux501_Weaps_E5_shield: Aux501_Weaps_E5
    {
        displayName= "[CIS] E5 (Shield)";
        baseWeapon = "Aux501_Weaps_E5_shield";
        scope = 1;
        scopearsenal = 0;
        JLTS_isShielded = 1;
        JLTS_baseWeapon = "Aux501_Weaps_E5";
        model = "\MRC\JLTS\weapons\E5\E5_shielded.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\E5\data\E5_co.paa"};
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\E5\anims\E5_shielded_handanim.rtm"};
        inertia = 0.8;
        reloadAction = "";
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            mass = 110;
            class UnderBarrelSlot: UnderBarrelSlot
            {
                compatibleItems[] = 
                {
                    "JLTS_riot_shield_droid_attachment"
                };
            };
        };
        class XtdGearInfo{};
    };
    
    class Aux501_Weaps_E5_Special: Aux501_Weaps_E5
    {
        displayName = "[CIS] E5 Special";
        baseWeapon = "Aux501_Weaps_E5_Special";
        JLTS_canHaveShield = 1;
        JLTS_shieldedWeapon = "Aux501_Weaps_E5_Special_shield";
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\CIS\E5\data\sounds\e5_special_shot.wss",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\CIS\E5\data\sounds\e5_special_shot.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class single_medium: single_medium
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\CIS\E5\data\sounds\e5_special_shot.wss",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\CIS\E5\data\sounds\e5_special_shot.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class single_far: single_far
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\CIS\E5\data\sounds\e5_special_shot.wss",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\CIS\E5\data\sounds\e5_special_shot.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class FullAuto: FullAuto
        {
            reloadTime = 0.055;
            dispersion = 0.0010000001;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\CIS\E5\data\sounds\e5_special_shot.wss",+3db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\CIS\E5\data\sounds\e5_special_shot.wss",+3db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Weapons_E5";
            variant = "special";
        };
    };
    class Aux501_Weaps_E5_Special_shield: Aux501_Weaps_E5_Special
    {
        displayName= "[CIS] E5 Special (Shield)";
        baseWeapon = "Aux501_Weaps_E5_Special_shield";
        scope = 1;
        scopearsenal = 0;
        JLTS_isShielded = 1;
        JLTS_baseWeapon = "Aux501_Weaps_E5_Special";
        model = "\MRC\JLTS\weapons\E5\E5_shielded.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\weapons\E5\data\E5_co.paa"};
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\E5\anims\E5_shielded_handanim.rtm"};
        inertia = 0.8;
        reloadAction = "";
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            mass = 110;
            class UnderBarrelSlot: UnderBarrelSlot
            {
                compatibleItems[] = 
                {
                    "JLTS_riot_shield_droid_attachment"
                };
            };
        };
        class XtdGearInfo{};
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_CIS_Weapons_E5
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "variant" };
            class variant
            {
                label = "E5 Variants";
                values[] = 
                {
                    "standard",
                    "special"
                };
                class standard       { label = "Standard"; };
                class special        { label = "Special"; };
            };
        };
    };
};

class CfgMagazines
{
    class 30Rnd_65x39_caseless_mag;

    class Aux501_Weapons_Mags_E5100: 30Rnd_65x39_caseless_mag
    {
        displayName = "[501st] E5 Blaster Cell";
        displayNameShort = "E5 Cell";
        author = "501st Aux Team";
        picture = "\MRC\JLTS\weapons\E5\data\ui\E5_mag_ui_ca.paa";
        model = "\MRC\JLTS\weapons\E5\E5_mag.p3d";
        count = 100;
        ammo = "Aux501_Weapons_Ammo_E5_Blaster";
        descriptionShort = "E5 Medium Power Magazine";
        mass = 8;
        modelSpecial = "";
        modelSpecialIsProxy = 0;
        tracersEvery = 1;
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_red;

    class Aux501_Weapons_Ammo_E5_Blaster: Aux501_Weapons_Ammo_base_red
    {
        hit = 10;
        dangerRadiusBulletClose = 8;
        dangerRadiusHit = 12;
        typicalSpeed = 820;
        caliber = 1;
    };
};