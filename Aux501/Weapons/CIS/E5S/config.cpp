class cfgPatches
{
    class Aux501_Patch_E5S
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[] =
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] =
        {
            "Aux501_Weaps_E5S"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5S15"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_E5S_Blaster"
        };
    };
};

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless : Aux501_rifle_base
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_E5S : Aux501_rifle_base_stunless
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[CIS] E5S";
        baseWeapon = "Aux501_Weaps_E5S";
        picture = "\MRC\JLTS\weapons\E5S\data\ui\E5S_ui_ca.paa";
        model = "\MRC\JLTS\weapons\E5S\E5S.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\MRC\JLTS\weapons\E5S\anims\E5S_handanim.rtm"};
        recoil = "recoil_dmr_01";
        recoilProne = "recoil_single_prone_mx";
        magazines[] =
        {
            "Aux501_Weapons_Mags_E5S15"
        };
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        modes[] = {"Single","aicqb","aiclose","aimedium","aifar","aiopticmode1","aiopticmode2"};
        class Single : Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound : BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"MRC\JLTS\weapons\E5\sounds\E5_fire",+3db,1,2200};
                soundBegin[] = {"begin1", 1};
            };
            reloadTime = 0.2;		
            dispersion = 0.0006;
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            dispersion = 0.0006;
            minRange = 1;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 0.1;
            aiRateOfFireDistance = 50;
            requiredOpticType = 1;
        };
        class aiclose: aicqb
        {
            minRange = 50;
            minRangeProbab = 0.5;
            midRange = 150;
            midRangeProbab = 1;
            maxRange = 250;
            maxRangeProbably = 0.5;
            aiRateOfFireDistance = 150;
        };
        class aimedium: aicqb
        {
            minRange = 150;
            minRangeProbab = 0.5;
            midRange = 250;
            midRangeProbab = 1;
            maxRange = 350;
            maxRangeProbab = 0.1;
            aiRateOfFireDistance = 250;
            
        };
        class aifar: aicqb
        {
            minRange = 250;
            minRangeProbab = 0.5;
            midRange = 350;
            midRangeProbab = 1;
            maxRange = 600;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 350;
        };
        class aiopticmode1: aicqb
        {
            minRange = 400;
            minRangeProbab = 0.5;
            midRange = 500;
            midRangeProbab = 1;
            maxRange = 700;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 500;
        };
        class aiopticmode2: aicqb
        {
            minRange = 500;
            minRangeProbab = 0.5;
            midRange = 700;
            midRangeProbab = 1;
            maxRange = 900;
            maxRangeProbab = 0.5;
            aiRateOfFireDistance = 700;
        };
        modelOptics = "\IBL\weapons\DLT19\DLT19_reticle.p3d";
        class OpticsModes
        {
            class Snip
            {
                cameraDir = "";
                discreteDistance[] = {100};
                discreteDistanceInitIndex = 0;
                discretefov[] = {0.045,0.011};
                discreteInitIndex = 0;
                distanceZoomMax = 2400;
                distanceZoomMin = 300;
                memoryPointCamera = "opticView";
                opticsDisablePeripherialVision = 1;
                opticsDisplayName = "WFOV";
                opticsFlare = 1;
                opticsID = 1;
                opticsPPEffects[] = {"OpticsCHAbera1","OpticsBlur1"};
                opticsZoomInit = 0.045;
                opticsZoomMax = 0.045;
                opticsZoomMin = 0.011;
                useModelOptics = 1;
                visionMode[] = {};
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_30mw10;

    class Aux501_Weapons_Mags_E5S15: Aux501_Weapons_Mags_30mw10
    {
        displayName = "[501st] E5S Blaster Cell";
        displayNameShort = "15Rnd 30MW";
        picture = "\MRC\JLTS\weapons\E5S\data\ui\E5S_mag_ui_ca.paa";
        count = 15;
        ammo = "Aux501_Weapons_Ammo_E5S_Blaster";
        descriptionShort = "E5S High Power Magazine";
        model = "\MRC\JLTS\weapons\E5S\E5S_mag.p3d";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_red;

    class Aux501_Weapons_Ammo_E5S_Blaster: Aux501_Weapons_Ammo_base_red
    {
        hit = 45;
        typicalSpeed = 1500;
        caliber = 2.4;
        waterFriction = -0.009;
    };
};