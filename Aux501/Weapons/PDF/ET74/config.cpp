class cfgPatches
{
    class Aux501_Patch_ET74
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_ET74"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_ET74"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_ET74"
        };
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_ET74_closeShot
    {
        samples[] = 
        {
            {"\SFA_Weapons_R\BoltBlaster\data\SFX\boltblaster_p.wav",1}
        };
        volume = "1";
        range = 150;
        rangeCurve = "closeShotCurve";
    };
    class Aux501_SoundShader_ET74_FarShot
    {
        samples[] = 
        {
            {"\SFA_Weapons_R\BoltBlaster\data\SFX\boltblaster_p.wav",1}
        };
        volume = "1";
        range = 2200;
        rangeCurve[] = {{0,0},{50,0},{300,1},{2200,1}};
    };
};

class CfgSoundSets
{
    class Aux501_SoundSet_Weapon_ET74
    {
        soundShaders[] = 
        {
            "Aux501_SoundShader_ET74_closeShot",
            "Aux501_SoundShader_ET74_FarShot"
        };
        volumeFactor = 1.6;
        spatial = 1;
        doppler = 0;
        loop = 0;
    };
};

class CowsSlot;

class cfgWeapons
{
    class hgun_P07_F;
    class Aux501_pistol_base: hgun_P07_F
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_ET74: Aux501_pistol_base
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[PDF] ET-74 Mk.II";
        baseWeapon = "Aux501_Weaps_ET74";
        picture = "\Aux501\Weapons\PDF\ET74\Textures\UI\ET74_ui_ca.paa";
        model = "\SFA_Weapons_R\BoltBlaster\Boltblaster_mk2_P.p3d";
        selectionFireAnim = "zasleh";
        hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"SFA_Weapons_R\BoltBlaster\data\boltblaster_p_co.paa"};
        magazines[] =
        {
            "Aux501_Weapons_Mags_ET74"
        };
        muzzles[] = {"this"};
        fireLightDiffuse[] = {0,1,0};
        fireLightAmbient[] = {0,1,0};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_ET74"};
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = {""};
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_Westar35C100;

    class Aux501_Weapons_Mags_ET74: Aux501_Weapons_Mags_Westar35C100
    {
        displayName = "[501st] ET-74 Cell";
        displayNameShort = "ET-74 Cell";
        count = 30;
        ammo = "Aux501_Weapons_Ammo_ET74"; 
        descriptionShort = "Medium Power Pistol magazine";
        mass = 4;
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_green;

    class Aux501_Weapons_Ammo_ET74: Aux501_Weapons_Ammo_base_green
    {
        hit = 10;
        dangerRadiusBulletClose = 8;
        dangerRadiusHit = 12;
        typicalSpeed = 820;
        caliber = 1;
        waterFriction = -0.009;
    };
};