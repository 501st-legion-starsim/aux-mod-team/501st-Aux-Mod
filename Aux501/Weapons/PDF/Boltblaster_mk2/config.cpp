class cfgPatches
{
    class Aux501_Patch_EL4
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_EL4",
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_EL4"
        };
        ammo[] = {};
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_EL4_CloseShot
    {
        samples[] =
        {
            {"\Aux501\Weapons\PDF\Boltblaster_mk2\data\Sounds\el4_shot.wss",1}
        };
        volume = "0.5";
        range = 150;
        rangeCurve = "closeShotCurve";
    };
    class Aux501_SoundShader_EL4_FarShot
    {
        samples[] =
        {
            {"\Aux501\Weapons\PDF\Boltblaster_mk2\data\Sounds\el4_shot.wss",1}
        };
        volume = "0.2";
        range = 2200;
        rangeCurve[] = {{0,0},{50,0},{300,1},{2200,1}};
    };
};

class CfgSoundSets
{
    class Aux501_SoundSet_Weapon_EL4
    {
        soundShaders[] =
        {
            "Aux501_SoundShader_EL4_CloseShot",
            "Aux501_SoundShader_EL4_FarShot"
        };
        volumefactor = 1.6;
        spatial = 1;
        doppler = 0;
        loop = 0;
    };
};

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless : Aux501_rifle_base
    {
        class Single;
    };

    class Aux501_Weaps_EL4: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[PDF] EL-4 Boltblaster Mk.II";
        baseWeapon = "Aux501_Weaps_EL4";
        picture = "\SFA_Weapons_R\BoltBlaster\data\ui\BoltblasterMK2_ui_ca.paa";
        model = "\SFA_Weapons_R\BoltBlaster\Boltblaster_mk2_S.p3d";
        reloadAction = "ReloadMagazine";
        handAnim[] = 
        {
            "OFP2_ManSkeleton",
            "SFA_Weapons_R\Anim\Boltblaster\boltblaster4.rtm"    
        };
        recoil = "recoil_mx";
        magazines[]=
        {
            "Aux501_Weapons_Mags_EL4"
        };
        reloadMagazineSound[] = {"swlw_rework\sounds\e-series\E5_reload.wss",3,1,30};
        modelOptics = "\IBL\weapons\Galaar\galaar_21_reticle.p3d";
        class OpticsModes
        {
            class Ironsights
            {
                opticsID = 1;
                useModelOptics = 0;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.25;
                opticsZoomMax = 1.1;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
            class Scope: Ironsights
            {
                opticsID = 2;
                useModelOptics = 1;
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.125;
                opticsZoomMax = 0.125;
                opticsZoomInit = 0.125;
                memoryPointCamera = "opticView";
                opticsFlare = "true";
                distanceZoomMin = 100;
                distanceZoomMax = 100;
                cameraDir = "";
            };
        };
        modes[] = {"Single","AICQB","AIClose","AIMedium","AIFar"};
        class Single: Single
        {
            reloadTime = 0.1;
            dispersion = 0.00073;
            aiDispersionCoefY = 1.7;
            aiDispersionCoefX = 1.4;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_EL4"};
            };
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            burst = 1;
            burstRangeMax = -1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 100;
        };
        class aiclose: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 100;
        };
        class aimedium: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 100;
            minRangeProbab = 0.05;
            midRange = 200;
            midRangeProbab = 0.5;
            maxRange = 300;
            maxRangeProbab = 0.04;
            aiRateOfFire = 5;
            aiRateOfFireDistance = 300;
        };
        class aifar: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 400;
            midRangeProbab = 0.5;
            maxRange = 500;
            maxRangeProbab = 0.04;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 500;
        };
        class aiopticmode1: aicqb
        {
            minRange = 350;
            minRangeProbab = 0.5;
            midRange = 550;
            midRangeProbab = 1;
            maxRange = 650;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 500;
            requiredOpticType = 1;
        };
        class aiopticmode2: aicqb
        {
            minRange = 550;
            minRangeProbab = 0.5;
            midRange = 650;
            midRangeProbab = 1;
            maxRange = 800;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 700;
            requiredOpticType = 1;
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_10mw50;

    class Aux501_Weapons_Mags_EL4: Aux501_Weapons_Mags_10mw50
    {
        displayName = "[501st] EL-4 Blaster Cell";
        displayNameShort = "EL-4 Cell";
        picture = "\MRC\JLTS\weapons\DC15S\data\ui\DC15S_mag_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DC15S\DC15S_mag.p3d";
        count = 100;
        descriptionShort = "Medium Power Magazine";
    };
};