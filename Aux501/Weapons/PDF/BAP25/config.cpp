class cfgPatches
{
    class Aux501_Patch_bap25
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_BAP25_at",
            "Aux501_Weaps_BAP25_aa"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_BAP25_at",
            "Aux501_Weapons_Mags_BAP25_he",
            "Aux501_Weapons_Mags_BAP25_aa"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_BAP25_at_sub",
            "Aux501_Weapons_Ammo_BAP25_at",

            "Aux501_Weapons_Ammo_BAP25_aa"
        };
    };
};

class CfgWeapons
{
    class launch_RPG32_F;
    class Aux501_Weaps_e60r_at: launch_RPG32_F
    {
        class Single;
    };

    class launch_Titan_short_base;
    class Aux501_Weaps_e60r_aa: launch_Titan_short_base
    {
        class Single;
        class TopDown;
    };

    class Aux501_Weaps_BAP25_at: Aux501_Weaps_e60r_at
    {
        displayName = "[PDF] BAP-25 Unguided Launcher";
        descriptionShort = "Portable Anti-Tank Launcher";
        picture = "\Aux501\Weapons\PDF\BAP25\Textures\UI\bap25_ui.paa";
        uiPicture = "\Aux501\Weapons\PDF\BAP25\Textures\UI\bap25_ui.paa";
        model = "\SFA_Weapons_R\S7\S7.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\SWLW_clones\launchers\rps6\anims\RPS6_handanim.rtm"};
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\SFA_Weapons_R\S7\data\S7_co.paa"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_BAP25_at",
            "Aux501_Weapons_Mags_BAP25_he"
        };
        reloadAction = "GestureReloadRPG7";
        class Single: Single
        {
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"SWLW_merc\rifles\zh73\sounds\zh73",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        weaponInfoType = "RscOptics_HH12";
        class GunParticles
        {
            class effect1
            {
                positionName = "konec hlavne";
                directionName = "usti hlavne";
                effectName = "RocketBackEffectsRPGNT";
            };
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Weapons_BAP25";
            variant = "at";
        };
    };

    class Aux501_Weaps_BAP25_aa: Aux501_Weaps_e60r_aa
    {
        displayName = "[501st] BAP-25-AA Guided Launcher";
        descriptionShort = "Portable Anti-Air Launcher";
        picture = "\Aux501\Weapons\PDF\BAP25\Textures\UI\bap25_ui.paa";
        uiPicture = "\Aux501\Weapons\PDF\BAP25\Textures\UI\bap25_ui.paa";
        model = "\SFA_Weapons_R\S7\S7.p3d";
        handAnim[] = {"OFP2_ManSkeleton","\SWLW_clones\launchers\rps6\anims\RPS6_handanim.rtm"};
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\SFA_Weapons_R\S7\data\S7_co.paa"};
        magazines[] = {"Aux501_Weapons_Mags_BAP25_aa"};
        reloadAction = "GestureReloadRPG7";
        weaponInfoType = "RscOptics_BAP25";
        class Single: Single
        {
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"SWLW_merc\rifles\zh73\sounds\zh73",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        class TopDown: TopDown
        {
            class BaseSoundModeType{};
            class StandardSound: BaseSoundModeType
            {
                begin1[] = {"SWLW_merc\rifles\zh73\sounds\zh73",+3db,1,2200};
                soundBegin[] = {"begin1",1};
            };
        };
        shotPos = "usti hlavne";
        shotEnd = "konec hlavne";
        class GunParticles
        {
            class effect1
            {
                positionName = "muzzleEnd2";
                directionName = "muzzlePos2";
                effectName = "RocketBackEffectsNLAWNT";
            };
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Weapons_BAP25";
            variant = "aa";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_PDF_Weapons_BAP25
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "variant" };
            class variant
            {
                label = "BAP-25 Variants";
                values[] = 
                {
                    "at",
                    "aa"
                };
                class at  { label = "AT"; };
                class aa  { label = "AA"; };
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_e60r_at;
    class Aux501_Weapons_Mags_e60r_he;
    class Aux501_Weapons_Mags_e60r_aa;

    class Aux501_Weapons_Mags_BAP25_at: Aux501_Weapons_Mags_e60r_at
    {
        displayName = "[501st] BAP-25 AT Rocket";
        displayNameShort = "AT";
        descriptionShort = "AT Rocket";
        ammo = "Aux501_Weapons_Ammo_BAP25_at";
    };
    class Aux501_Weapons_Mags_BAP25_he: Aux501_Weapons_Mags_e60r_he
    {
        displayName = "[501st] BAP-25 HE Rocket";
        descriptionShort = "HE Rocket";
        ammo = "Aux501_Weapons_Ammo_BAP25_he";
    };
    class Aux501_Weapons_Mags_BAP25_aa: Aux501_Weapons_Mags_e60r_aa
    {
        displayName = "[501st] BAP-25 AA Rocket";
        descriptionShort = "AA Rocket";
        ammo = "Aux501_Weapons_Ammo_BAP25_aa";
    };
};

class CfgAmmo
{
    class ammo_Penetrator_RPG32V;
    class R_PG32V_F;
    class R_TBG32V_F;

    class M_Titan_AA;

    class Aux501_Weapons_Ammo_BAP25_at_sub: ammo_Penetrator_RPG32V
    {
        hit = 650;
        caliber = 43.3333;
        airFriction = -0.28;
        thrust = 210;
        thrustTime = 1.5;
        typicalSpeed = 1000;
    };
    class Aux501_Weapons_Ammo_BAP25_at: R_PG32V_F
    {
        soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
        effectsMissile = "Aux501_particle_effect_BAP25_fly";
        thrust = 100;
        thrustTime = 5;
        timeToLive = 5;
        submunitionAmmo = "Aux501_Weapons_Ammo_BAP25_at_sub";
    };	
    class Aux501_Weapons_Ammo_BAP25_he: R_TBG32V_F
    {
        soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
        effectsMissile = "Aux501_particle_effect_BAP25_fly";
        thrust = 100;
        thrustTime = 5;
        timeToLive = 5;
    };
    class Aux501_Weapons_Ammo_BAP25_aa: M_Titan_AA
    {
        cmImmunity = 0.5;
        soundFly[] = {"swlw_rework\sounds\launcher\E60R_fly.wss",6,1.5,700};
        effectsMissile = "Aux501_particle_effect_BAP25_fly";
    };
};

class Aux501_particle_effect_BAP25_fly
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_BAP25";
        position[] = {0,0,0};
    };
    class Smoke
    {
        simulation = "particles";
        type = "Aux501_cloudlet_E60R_smoke";
        position[] = {0,0,0};
        qualityLevel = -1;
    };
    class Sparks
    {
        simulation = "particles";
        type = "Aux501_cloudlet_E60R_sparks";
        position[] = {0,0,0};
        qualityLevel = 2;
    };
};

class CfgLights
{
    class RocketLight;

    class Aux501_light_BAP25: RocketLight
    {
        color[]={81,250,2};
        intensity = 1e8;
        dayLight = 1;
        useFlare = 1;
        flareSize = 1.5;
        flareMaxDistance = 6000;
    };
};