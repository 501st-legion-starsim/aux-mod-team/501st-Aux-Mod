class cfgPatches
{
    class Aux501_Patch_T20
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_T20",
            "Aux501_Weaps_T20_scoped"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_T20"
        };
    };
};

class Mode_FullAuto;

class CowsSlot;

class CfgSoundShaders
{
    class Aux501_SoundShader_T20_closeShot
    {
        samples[] = 
        {
            {"SFA_Weapons_N\T20\data\sfx\t20_sound1.wav",1},
            {"SFA_Weapons_N\T20\data\sfx\t20_sound2.wav",1},
            {"SFA_Weapons_N\T20\data\sfx\t20_sound3.wav",1},
            {"SFA_Weapons_N\T20\data\sfx\t20_sound4.wav",1},
            {"SFA_Weapons_N\T20\data\sfx\t20_sound5.wav",1}
        };
        volume = "1";
        range = 150;
        rangeCurve = "closeShotCurve";
    };
    class Aux501_SoundShader_T20_FarShot
    {
        samples[] = 
        {
            {"SFA_Weapons_N\T20\data\sfx\t20_sound1.wav",1},
            {"SFA_Weapons_N\T20\data\sfx\t20_sound2.wav",1},
            {"SFA_Weapons_N\T20\data\sfx\t20_sound3.wav",1},
            {"SFA_Weapons_N\T20\data\sfx\t20_sound4.wav",1},
            {"SFA_Weapons_N\T20\data\sfx\t20_sound5.wav",1}
        };
        volume = "1";
        range = 2200;
        rangeCurve[] = {{0,0},{50,0},{300,1},{2200,1}};
    };
};

class CfgSoundSets
{
    class Aux501_SoundSet_Weapon_T20
    {
        soundShaders[] = 
        {
            "Aux501_SoundShader_T20_closeShot",
            "Aux501_SoundShader_T20_FarShot"
        };
        volumeFactor = 1.6;
        spatial = 1;
        doppler = 0;
        loop = 0;
    };
};

class CfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_T20: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[PDF] T-20 Heavy Blaster";
        baseWeapon = "Aux501_Weaps_T20";
        picture = "\Aux501\Weapons\PDF\T20\textures\UI\t20_ui_ca.paa";
        model = "SFA_Weapons_N\T20\T20_Rifle.p3d";
        handAnim[] = {"OFP2_ManSkeleton","SFA_Weapons_R\Anim\Boltblaster\boltblaster4.rtm"};
        magazines[]=
        {
            "Aux501_Weapons_Mags_T20"
        };
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        reloadAction = "ReloadMagazine";
        modes[] = {"FullAutoSlow","FullAuto","AIClose","AIMedium","AIFar"};
        class FullAuto: Mode_FullAuto
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_T20"};
            };
            reloadTime = 0.1;
            dispersion = 0.00102;
            soundContinuous = 0;
            soundBurst = 0;
            minRange = 0;
            minRangeProbab = 0.3;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.04;
            showToPlayer = 1;
        };
        class FullAutoSlow: FullAuto
        {
            reloadTime = 0.16;
            textureType = "burst";
            dispersion = 0.002;
            soundBurst = 0;
            ffCount = 1;
            soundContinuous = 0;
            minRange = 0;
            minRangeProbab = 0.9;
            midRange = 15;
            midRangeProbab = 0.7;
            maxRange = 30;
            maxRangeProbab = 0.1;
            aiRateOfFire = 1e-06;
            aiRateOfFireDistance = 50;
            showToPlayer = 1;
        };
        class aiclose: FullAutoSlow
        {
            burst = 10;
            aiRateOfFire = 1e-06;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.05;
            midRange = 70;
            midRangeProbab = 0.7;
            maxRange = 150;
            maxRangeProbab = 0.04;

            showToPlayer = 0;
        };
        class aimedium: aiclose
        {
            burst = 10;
            aiRateOfFireDistance = 300;
            minRange = 75;
            minRangeProbab = 0.05;
            midRange = 250;
            midRangeProbab = 0.7;
            maxRange = 500;
            maxRangeProbab = 0.04;
        };
        class aifar: aiclose 
        {
            burst = 10;
            aiRateOfFireDistance = 600;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 700;
            midRangeProbab = 0.6;
            maxRange = 1000;
            maxRangeProbab = 0.1;
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = 
                {
                    "Aux501_cows_HoloScope"
                };
            };
        };
    };    
    class Aux501_Weaps_T20_scoped: Aux501_Weaps_T20
    {
        class LinkedItems
        {
            class LinkedItemsOptic
            {
                item = "Aux501_cows_HoloScope";
                slot = "CowsSlot";
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_IQA20;

    class Aux501_Weapons_Mags_T20: Aux501_Weapons_Mags_IQA20
    {
        displayName = "[501st] T-20 Cell";
        displayNameShort = "T-20 Cell";
        picture = "\MRC\JLTS\weapons\DC15S\data\ui\DC15S_mag_ui_ca.paa";
        model = "\MRC\JLTS\weapons\DC15S\DC15S_mag.p3d";
        count = 50;
    };
};