class cfgPatches
{
    class Aux501_Patch_DH17
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        ammo[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_DH17",
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_DH17"
        };
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_DH17_closeShot
    {
        samples[] = 
        {
            {"MRC\JLTS\weapons\DC17SA\sounds\dc17sa_fire",1}
        };
        volume = "1";
        range = 150;
        rangeCurve = "closeShotCurve";
    };
    class Aux501_SoundShader_DH17_farShot
    {
        samples[] = 
        {
            {"MRC\JLTS\weapons\DC17SA\sounds\dc17sa_fire",1}
        };
        volume = "1";
        range = 2200;
        rangeCurve[] = {{0,0},{50,0},{300,1},{2200,1}};
    };
};

class CfgSoundSets
{ 
    class Aux501_SoundSet_Weapon_DH17
    {
        soundShaders[] = 
        {
            "Aux501_SoundShader_DH17_closeShot",
            "Aux501_SoundShader_DH17_farShot"
        };
        volumeFactor = 1.6;
        spatial = 1;
        doppler = 0;
        loop = 0;
    };
};

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless : Aux501_rifle_base
    {
        class Single;
    };

    class Aux501_Weaps_DH17: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[PDF] DH-17";
        baseWeapon = "Aux501_Weaps_DH-17";
        picture = "\WarMantle\WM_Imperial_Weapons\data\ui\dh17_x_ca.paa";
        model = "WarMantle\WM_Imperial_Weapons\DH17\Dh17.p3d";
        reloadAction = "ReloadMagazine";
        handAnim[] = 
        {
            "OFP2_ManSkeleton",
            "\A3\Weapons_F_epa\LongRangeRifles\DMR_01\Data\Anim\dmr_01.rtm"
        };

        recoil = "recoil_mxm";
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        magazines[]=
        {
            "Aux501_Weapons_Mags_DH17"
        };
        modelOptics = "\IBL\weapons\Galaar\galaar_21_reticle.p3d";
        class OpticsModes
        {
            class Scope
            {
                opticsID = 2;
                useModelOptics = 1;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.125;
                opticsZoomMax = 0.125;
                opticsZoomInit = 0.125;
                memoryPointCamera = "opticView";
                visionMode[] = {};
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
        };
        modes[] = {"Single","AICQB","AIClose","AIMedium","AIFar"};
        class Single: Single
        {
            reloadTime = 0.1;
            dispersion = 0.00073;
            aiDispersionCoefY = 1.7;
            aiDispersionCoefX = 1.4;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_DH17"};
            };
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            burst = 1;
            burstRangeMax = -1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 100;
        };
        class aiclose: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 100;
        };
        class aimedium: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 100;
            minRangeProbab = 0.05;
            midRange = 200;
            midRangeProbab = 0.5;
            maxRange = 300;
            maxRangeProbab = 0.04;
            aiRateOfFire = 5;
            aiRateOfFireDistance = 300;
        };
        class aifar: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 400;
            midRangeProbab = 0.5;
            maxRange = 500;
            maxRangeProbab = 0.04;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 500;
        };
        class aiopticmode1: aicqb
        {
            minRange = 350;
            minRangeProbab = 0.5;
            midRange = 550;
            midRangeProbab = 1;
            maxRange = 650;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 500;
            requiredOpticType = 1;
        };
        class aiopticmode2: aicqb
        {
            minRange = 550;
            minRangeProbab = 0.5;
            midRange = 650;
            midRangeProbab = 1;
            maxRange = 800;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 700;
            requiredOpticType = 1;
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_E5100;

    class Aux501_Weapons_Mags_DH17: Aux501_Weapons_Mags_E5100
    {
        displayName = "[501st] DH-17 Blaster Cell";
        displayNameShort = "DH-17 Cell";
        descriptionShort = "Medium Power Magazine";
    };
};