class cfgPatches
{
    class Aux501_Patch_RK3
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_RK3"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_RK3"
        };
    };
};

class CfgSoundShaders
{
	class Aux501_SoundShader_RK3_closeShot
	{
		samples[] =
        {
			{"WarMantle\WM_Imperial_Weapons\SE14C\sfx\SE14C_close_01",1},
			{"WarMantle\WM_Imperial_Weapons\SE14C\sfx\SE14C_close_02",1},
			{"WarMantle\WM_Imperial_Weapons\SE14C\sfx\SE14C_close_03",1},
			{"WarMantle\WM_Imperial_Weapons\SE14C\sfx\SE14C_close_04",1},
			{"WarMantle\WM_Imperial_Weapons\SE14C\sfx\SE14C_close_05",1}
        };
		volume = "db0";
		range = 50;
		rangeCurve = "closeShotCurve";
	};
	class Aux501_SoundShader_RK3_distShot
	{
		samples[] =
        {
		    {"WarMantle\WM_Imperial_Weapons\SE14C\sfx\SE14C_far_01",1},
		    {"WarMantle\WM_Imperial_Weapons\SE14C\sfx\SE14C_far_02",1},
		    {"WarMantle\WM_Imperial_Weapons\SE14C\sfx\SE14C_far_03",1},
		    {"WarMantle\WM_Imperial_Weapons\SE14C\sfx\SE14C_far_04",1},
		    {"WarMantle\WM_Imperial_Weapons\SE14C\sfx\SE14C_far_05",1}
        };
		volume = "db0";
		range = 1800;
		rangeCurve[] = {{0,0},{50,0},{300,1},{1800,1}};
	};
};
class CfgSoundSets
{
	class Aux501_SoundSet_Weapon_RK3
	{
		soundShaders[] = 
        {
            "Aux501_SoundShader_RK3_closeShot",
            "Aux501_SoundShader_RK3_distShot"
        };
		volumeFactor = 1.6;
		spatial = 1;
		doppler = 0;
		loop = 0;
	};
};

class cfgWeapons
{
    class hgun_P07_F;
    class Aux501_pistol_base: hgun_P07_F
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_RK3: Aux501_pistol_base
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[PDF] RK-3";
        baseWeapon = "Aux501_Weaps_RK3";
        picture = "\WarMantle\WM_Imperial_Weapons\data\ui\RK3_x_CA.paa";
		model = "WarMantle\WM_Imperial_Weapons\RK3\RK3.p3d";
        magazines[]=
        {
            "Aux501_Weapons_Mags_RK3"
        };
        muzzles[] = {"this"};
        fireLightDiffuse[] = {0.1,0,0.0025};
        fireLightAmbient[] = {0,0,0};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_RK3"};
            };
        };
	};
};
class CfgMagazines
{
    class Aux501_Weapons_Mags_E5100;
    class Aux501_Weapons_Mags_RK3: Aux501_Weapons_Mags_E5100
    {
        displayName = "[501st] RK-3 Blaster Cell";
        displayNameShort = "RK-3 Cell";
        descriptionShort = "RK-3 Medium Power Pistol Magazine";
        picture = "\SWLW_clones\pistols\dc17\data\ui\DC17_mag_ui.paa";
        model = "SWLW_clones\pistols\dc17\DC17_mag.p3d";
        count = 25;
		mass = 4;
    };
};