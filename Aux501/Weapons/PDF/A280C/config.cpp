class cfgPatches
{
    class Aux501_Patch_A280C
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_A280C"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_A280C"
        };
    };
};

class CfgSoundShaders
{
	class Aux501_SoundShader_A280_closeShot
	{
		samples[] = 
        {
            {"WarMantle\WM_Imperial_Weapons\A280\data\sfx\a280_Close_01",1},
            {"WarMantle\WM_Imperial_Weapons\A280\data\sfx\a280_Close_02",1},
            {"WarMantle\WM_Imperial_Weapons\A280\data\sfx\a280_Close_03",1}
        };
		volume = "1";
		range = 150;
		rangeCurve = "closeShotCurve";
	};
	class Aux501_SoundShader_A280_distShot
	{
		samples[] = 
        {
            {"WarMantle\WM_Imperial_Weapons\A280\data\sfx\a280_Close_01",1},
            {"WarMantle\WM_Imperial_Weapons\A280\data\sfx\a280_Close_02",1},
            {"WarMantle\WM_Imperial_Weapons\A280\data\sfx\a280_Close_03",1}
        };
		volume = "1";
		range = 2200;
		rangeCurve[] = {{0,0},{50,0},{300,1},{1800,1}};
	};
};

class CfgSoundSets
{
	class Aux501_SoundSet_Weapon_A280
	{
		soundShaders[] = 
        {
            "Aux501_SoundShader_A280_closeShot",
            "Aux501_SoundShader_A280_distShot"
        };
		volumeFactor = 1.6;
		spatial = 1;
		doppler = 0;
		loop = 0;
	};
};

class Mode_SemiAuto;
class Mode_FullAuto;

class cfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class Single;
    };

    class Aux501_Weaps_A280C: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[PDF] A280C";
        baseWeapon = "Aux501_Weaps_A280C";
        picture = "\WarMantle\WM_Imperial_Weapons\data\ui\a280_x_ca.paa";
		model = "WarMantle\WM_Imperial_Weapons\A280\A280.p3d";
        handAnim[] = 
        {
            "OFP2_ManSkeleton",
            "\A3\Weapons_F_Mark\LongRangeRifles\DMR_02\data\Anim\DMR_02.rtm"
        };
        magazines[]=
        {
            "Aux501_Weapons_Mags_A280C"
        };
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        reloadAction = "ReloadMagazine";
        recoil = "recoil_pdw";
        modelOptics = "\IBL\weapons\Galaar\galaar_21_reticle.p3d";
        class OpticsModes
        {
            class Scope
            {
                opticsID = 1;
                useModelOptics = 1;
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.125;
                opticsZoomMax = 0.125;
                opticsZoomInit = 0.125;
                memoryPointCamera = "opticView";
                visionMode[] = {};
                opticsFlare = "true";
                distanceZoomMin = 100;
                distanceZoomMax = 100;
                cameraDir = "";
            };
        };
        modes[] = {"FullAuto","Single","aicqb","aiclose","aimedium","aifar"};
        class FullAuto: Mode_FullAuto
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_A280"};
            };
            reloadTime = 0.15;
            dispersion = 0.00102;
            soundContinuous = 0;
            soundBurst = 0;
            minRange = 0;
            minRangeProbab = 0.3;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.04;
            showToPlayer = 1;
        };
        class Single: Single
        {
            reloadTime = 0.1;    
            dispersion = 0.00073;
            aiDispersionCoefY = 1.7;
            aiDispersionCoefX = 1.4;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_A280"};
            };
        };
        class aicqb: Single
        {
            showToPlayer = 0;
            burst = 1;
            burstRangeMax = -1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 100;
        };
        class aiclose: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 0;
            minRangeProbab = 1;
            midRange = 50;
            midRangeProbab = 1;
            maxRange = 100;
            maxRangeProbab = 0.5;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 100;
        };
        class aimedium: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 100;
            minRangeProbab = 0.05;
            midRange = 200;
            midRangeProbab = 0.5;
            maxRange = 300;
            maxRangeProbab = 0.04;
            aiRateOfFire = 5;
            aiRateOfFireDistance = 300;
        };
        class aifar: aicqb
        {
            burst = 1;
            burstRangeMax = -1;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 400;
            midRangeProbab = 0.5;
            maxRange = 800;
            maxRangeProbab = 0.04;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 700;
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_E5100;

    class Aux501_Weapons_Mags_A280C: Aux501_Weapons_Mags_E5100
    {
        displayName = "[501st] A280C Blaster Cell";
        displayNameShort = "A280C Cell";
        descriptionShort = "A280C Medium Power Magazine";
    };
};