class cfgPatches
{
    class Aux501_Patch_RG4D
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_RG4D"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_RG4D30"
        };
    };
};

class CowsSlot;

class cfgWeapons
{
    class hgun_P07_F;
    class Aux501_pistol_base: hgun_P07_F
    {
        class Single;
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_RG4D: Aux501_pistol_base
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[PDF] RG-4D";
        baseWeapon = "Aux501_Weaps_RG4D";
        picture = "\MRC\JLTS\weapons\RG4D\data\ui\RG4D_ui_ca.paa";
        model = "SWLW_droids\pistols\rg4d\rg4d.p3d";
        magazines[]=
        {
            "Aux501_Weapons_Mags_RG4D30"
        };
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"IBL\weapons\EC17\sounds\ec17_fire_1",2,1,2200};
				begin2[] = {"IBL\weapons\EC17\sounds\ec17_fire_2",2,1,2200};
				begin3[] = {"IBL\weapons\EC17\sounds\ec17_fire_3",2,1,2200};
				begin4[] = {"IBL\weapons\EC17\sounds\ec17_fire_4",2,1,2200};
				begin5[] = {"IBL\weapons\EC17\sounds\ec17_fire_5",2,1,2200};
				soundBegin[] = {"begin1",0.2,"begin2",0.2,"begin3",0.2,"begin4",0.2,"begin5",0.2};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        modelOptics = "\IBL\weapons\EC17\EC17_reticle.p3d";
        class OpticModes
        {
            class Scope
            {
                opticsID = 1;
                useModelOptics = 1;
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.122173;
                opticsZoomMax = 0.122173;
                opticsZoomInit = 0.122173;
                memoryPointCamera = "opticView";
                visionMode[] = {};
                opticsFlare = "true";
                distanceZoomMin = 300;
                distanceZoomMax = 300;
                cameraDir = "";
            };
        };
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class CowsSlot: CowsSlot
            {
                displayName = "Optics Slot";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_top.paa";
                iconPinpoint = "Bottom";
                iconPosition[] = {0.5,0.35};
                iconScale = 0.2;
                linkProxy = "\a3\data_f\proxies\weapon_slots\TOP";
                compatibleItems[] = {""};
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_E5100;

    class Aux501_Weapons_Mags_RG4D30: Aux501_Weapons_Mags_E5100
    {
        displayName = "[501st] RG4D Cell";
        displayNameShort = "RG-4D Cell";
        picture = "\SWLW_clones\pistols\dc17\data\ui\DC17_mag_ui.paa";
        model = "SWLW_clones\pistols\dc17\DC17_mag.p3d";
        count = 30;
        descriptionShort = "Medium Power Pistol magazine";
        mass = 4;
    };
};