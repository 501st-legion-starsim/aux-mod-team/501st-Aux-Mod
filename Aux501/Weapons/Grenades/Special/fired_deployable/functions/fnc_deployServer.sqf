/*
 * Author: M3ales
 * Description: Deploys a server-defined object based on ammo type. Supports both static and attachable objects.
 * 
 * Input:
 * _unit - Unit to which the object will be attached if the attach flag is set (Object)
 * _ammo - Ammo type to be used for deployment (String)
 * _position - Position where the object will be deployed if the attach flag is not set (Array)
 * _direction - Direction the object will face if the attach flag is not set (Number)
 *
 * Usage:
 * [_unit, _ammo, _position, _direction] call fnc_deployServer;
 *
 * Example:
 * [player, "attachableObject_ammo", [100, 200, 0], 45] call fnc_deployServer;
 */

#include "function_macros.hpp"

params [
	["_unit", player],
	["_ammo", ""],
	["_position", [0,0,0]],
	["_direction", 0]
];

private _config = configFile >> "CfgAmmo" >> _ammo;
private _isValid = getNumber (_config >> QGVAR(enabled)) == 1;
private _deployable = getText (_config >> QGVAR(object));
private _timeToLive = getNumber (_config >> QGVAR(timeToLive));
private _attach = getNumber (_config >> QGVAR(attach)) == 1;
private _offset = getArray (_config >> QGVAR(attach_offset));
private _bone = getText (_config >> QGVAR(attach_bone));
private _rotate = getNumber (_config >> QGVAR(attach_rotate));

if (!_isValid) exitWith {};

if (isNil "_deployable" || _deployable isEqualTo "") exitWith {};

_config = configFile >> "CfgVehicles" >> _deployable;

private _setInvincible = 0;
if (isNumber(_config >> QGVAR(ignoreDamage))) then {
    _setInvincible = getNumber (_config >> QGVAR(ignoreDamage));
};

private _hasLoopSound = isText (_config >> QGVAR(loopSound));
private _loopSound = getText (_config >> QGVAR(loopSound));
private _loopDuration = getNumber (_config >> QGVAR(loopDuration));
private _hasEndSound = isText (_config >> QGVAR(endSound));
private _endSound = getText (_config >> QGVAR(endSound));
private _endDuration = getNumber (_config >> QGVAR(endDuration));
private _soundDistance = getNumber (_config >> QGVAR(soundDistance));

private _deployed = createVehicle [_deployable, _position, [], 0, "CAN_COLLIDE"];

if (!_attach) then {
    _deployed setPosATL _position;
    _deployed setDir _direction;
} else {
    _deployed attachTo [_unit, _offset, _bone, _rotate > 0];
};

if (_setInvincible == 1) then {
    _deployed allowDamage false;
};

if (_timeToLive > 0) then {
    [
        {
            params["_deployable", "_attach"];
            if (_attach) then {
                detach _deployable;
            };
            deleteVehicle _deployable;
        },
        [_deployed, _attach],
        _timeToLive
    ] call CBA_fnc_waitAndExecute;
};

if (_hasLoopSound && _loopSound != "") then {
    [QGVAR(soundLoop), [_deployed, _loopSound, _loopDuration, _timeToLive, _soundDistance]] call CBA_fnc_globalEvent;
};

if (_hasEndSound && _endSound != "") then {
    [
        {
            params["_deployed", "_endSound", "_endDuration", "_distance"];
            [QGVAR(soundEnd), [_deployed, _endSound, _endDuration, _distance]] call CBA_fnc_globalEvent;
        },
        [_deployed, _endSound, _endDuration, _soundDistance],
        (_timeToLive - _endDuration)
    ] call CBA_fnc_waitAndExecute;
};
