#include "config_macros.hpp"

class cfgPatches
{
    class Aux501_Medical
    {
        requiredAddons[] = {"ace_medical_treatment"};
        units[]={};
		weapons[]=
        {
            "Aux501_Carbonate",
			"Aux501_Carbonate_Item"
        };
    };
};
class CfgWeapons
{

};
class CfgMagazines
{
	class CA_Magazine;
	class Aux501_Carbonate: CA_Magazine
	{
		scope=2;
		displayName="[501st] Carbonate";
		model="\A3\Structures_F_EPA\Items\Medical\Painkillers_F.p3d";
		picture="\Aux501\Weapons\Medical\data\icon_Carbonate.paa";
		ammo="";
		count=10;
		initSpeed=0;
		tracersEvery=0;
		lastRoundsTracer=0;
		mass=1;
        ACE_isMedicalItem = 1;
        ACE_asItem = 1;
	};
};
class ACE_Medical_Treatment
{
    class Morphine;
    
    class Medication
    {
		class Aux501_wakedrug {
            painReduce = 0;
            hrIncreaseLow[] = {5, 10};
            hrIncreaseNormal[] = {10, 15};
            hrIncreaseHigh[] = {15, 20};
            timeInSystem = 30;
            timeTillMaxEffect = 2;
            maxDose = 10;
            incompatibleMedication[] = {};
            viscosityChange = 0;
		};
    };
};
class ACE_Medical_Treatment_Actions
{
    class Morphine;
	class Aux501_wakedrug: Morphine {
    displayName = "Take 501st Carbonate";
    displayNameProgress = "Using Carbonate";
    category = "medication";
    treatmentLocations = 0;
    allowedSelections[] = {"Head"};
    allowSelfTreatment = 1;
    medicRequired = 0;
    treatmentTime = 4;
    items[] = {"Aux501_Carbonate"};
    litter[] = {};
	callbackSuccess= "Aux501_Medical_fnc_WakeDrugLocal";
	};
};

class Extended_PreInit_EventHandlers {
    class ADDON {
        init = QUOTE(call COMPILE_FILE(XEH_preInit));
    };
};
class Extended_PostInit_EventHandlers
{
	class ADDON
	{
		init = QUOTE(call COMPILE_FILE(XEH_postInit));
	};
};

