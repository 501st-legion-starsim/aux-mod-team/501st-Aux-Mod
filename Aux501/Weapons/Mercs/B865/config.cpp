class cfgPatches
{
    class Aux501_Patch_B865
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_B865"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_B865"
        };
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_B865_closeShot
    {
        samples[] = 
        {
            {"\SFA_Weapons_R\Arkanian\data\SFX\arkanian_blaster.wav",1}
        };
        volume = "1";
        range = 150;
        rangeCurve = "closeShotCurve";
    };
    class Aux501_SoundShader_B865_FarShot
    {
        samples[] = 
        {
            {"\SFA_Weapons_R\Arkanian\data\SFX\arkanian_blaster.wav",1}
        };
        volume = "1";
        range = 2200;
        rangeCurve[] = {{0,0},{50,0},{300,1},{2200,1}};
    };
};

class CfgSoundSets
{
    class Aux501_SoundSet_Weapon_B865
    {
        soundShaders[] = 
        {
            "Aux501_SoundShader_B865_closeShot",
            "Aux501_SoundShader_B865_FarShot"
        };
        volumeFactor = 1.6;

        spatial = 1;
        doppler = 0;
        loop = 0;
    };
};

class MuzzleSlot;

class Mode_Burst;

class CfgWeapons
{
    class Aux501_rifle_base;
    class Aux501_rifle_base_stunless: Aux501_rifle_base
    {
        class WeaponSlotsInfo;
    };

    class Aux501_Weaps_B865: Aux501_rifle_base_stunless
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[IND] B-865 Arkanian Blaster";
        baseWeapon = "Aux501_Weaps_B865";
        picture = "\Aux501\Weapons\Mercs\B865\textures\UI\B865_ui_ca.paa";
        model = "\SFA_Weapons_R\Arkanian\Arkanian_C.p3d";
		handAnim[] = {"OFP2_ManSkeleton","SFA_Weapons_R\Anim\Boltblaster\boltblaster4.rtm"};
        magazines[]=
        {
            "Aux501_Weapons_Mags_B865"
        };
        fireLightDiffuse[] = {0,1,0};
        fireLightAmbient[] = {0,1,0};
        reloadAction = "ReloadMagazine";
        modes[] = {"Burst","AIClose","AIMedium","AIFar"};
        muzzles[] = {"this"};
        class WeaponSlotsInfo: WeaponSlotsInfo
        {
            class PointerSlot{};
            class MuzzleSlot: MuzzleSlot
            {
                compatibleItems[]=
                {
                    "Aux501_muzzle_flash"
                };
                linkProxy = "\A3\data_f\proxies\weapon_slots\MUZZLE";
                displayName = "$str_a3_cfgweapons_abr_base_f_weaponslotsinfo_muzzleslot0";
                iconPicture = "\A3\Weapons_F\Data\UI\attachment_muzzle.paa";
                iconPinpoint = "Center";
                iconPosition[] = {0.24,0.35};
                iconScale = 0.2;
            };
        };
        class Burst: Mode_Burst
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_B865"};
            };
            reloadTime = 0.1;
            dispersion = 0.00102;
            soundContinuous = 0;
            soundBurst = 0;
            minRange = 0;
            minRangeProbab = 0.3;
            midRange = 5;
            midRangeProbab = 0.7;
            maxRange = 10;
            maxRangeProbab = 0.04;
            showToPlayer = 1;
        };
        class aiclose: Burst
        {
            burst = 3;
            aiRateOfFire = 1e-06;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.05;
            midRange = 70;
            midRangeProbab = 0.7;
            maxRange = 150;
            maxRangeProbab = 0.04;

            showToPlayer = 0;
        };
        class aimedium: aiclose
        {
            burst = 3;
            aiRateOfFireDistance = 300;
            minRange = 75;
            minRangeProbab = 0.05;
            midRange = 250;
            midRangeProbab = 0.7;
            maxRange = 500;
            maxRangeProbab = 0.04;
        };
        class aifar: aiclose 
        {
            burst = 3;
            aiRateOfFireDistance = 600;
            minRange = 300;
            minRangeProbab = 0.05;
            midRange = 700;
            midRangeProbab = 0.6;
            maxRange = 1000;
            maxRangeProbab = 0.1;
        };
        class LinkedItems
		{
			class LinkedItemsMuzzle
			{
				slot = "MuzzleSlot";
				item = "Aux501_muzzle_flash";
			};
		};
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_E5100;

    class Aux501_Weapons_Mags_B865: Aux501_Weapons_Mags_E5100
    {
        displayName = "[501st] B-865 Cell";
        displayNameShort = "B-865 Cell";
        ammo = "Aux501_Weapons_Ammo_B865";
        count = 60;
        descriptionShort = "Medium Power B865 magazine";
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_green;

    class Aux501_Weapons_Ammo_B865: Aux501_Weapons_Ammo_base_green
    {
        hit = 10;
        dangerRadiusBulletClose = 8;
        dangerRadiusHit = 12;
        typicalSpeed = 820;
        caliber = 1;
    };

};