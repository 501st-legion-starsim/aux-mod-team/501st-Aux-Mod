class cfgPatches
{
    class Aux501_Patch_DH45
    {
        addonRootClass = "Aux501_Patch_Weapons";
        requiredAddons[]=
        {
            "Aux501_Patch_Weapons",
            "A3_Weapons_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Weaps_DH45"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_DH4530"
        };
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_DH45_closeShot
    {
        samples[] = 
        {
            {"SFA_Weapons_N\Holepuncher\data\sfx\Holepuncher_shot1.wav",1},
            {"SFA_Weapons_N\Holepuncher\data\sfx\Holepuncher_shot2.wav",1}
        };
		volume = "db0";
		range = 50;
		rangeCurve = "closeShotCurve";
    };
    class Aux501_SoundShader_DH45_FarShot
    {
        samples[] = 
        {
            {"SFA_Weapons_N\Holepuncher\data\sfx\Holepuncher_shot1.wav",1},
            {"SFA_Weapons_N\Holepuncher\data\sfx\Holepuncher_shot2.wav",1}
        };
		volume = "db0";
		range = 1800;
		rangeCurve[] = {{0,0},{50,0},{300,1},{1800,1}};
    };
};

class CfgSoundSets
{
    class Aux501_SoundSet_Weapon_DH45
    {
        soundShaders[] = 
        {
            "Aux501_SoundShader_DH45_closeShot",
            "Aux501_SoundShader_DH45_FarShot"
        };
        volumeFactor = 1.6;
        spatial = 1;
        doppler = 0;
        loop = 0;
    };
};

class cfgWeapons
{
    class hgun_P07_F;
    class Aux501_pistol_base: hgun_P07_F
    {
        class Single;
    };

    class Aux501_Weaps_DH45: Aux501_pistol_base
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[IND] DH-45";
        baseWeapon = "Aux501_Weaps_DH45";
        picture = "\Aux501\Weapons\Mercs\DH45\textures\UI\DH45_ui_ca.paa";
        model = "SFA_Weapons_N\DH45\DH45.p3d";
        magazines[]=
        {
            "Aux501_Weapons_Mags_DH4530"
        };
        fireLightDiffuse[] = {0,1,0};
        fireLightAmbient[] = {0,1,0};
        modes[] = {"Single"};
        muzzles[] = {"this"};
        class WeaponSlotsInfo{};
        class Single: Single
        {
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                soundSetShot[] = {"Aux501_SoundSet_Weapon_DH45"};
            };
            aiDispersionCoefY = 1.7;
            aiDispersionCoefX = 1.4;
            minRange = 1;
            minRangeProbab = 0.3;
            midRange = 30;
            midRangeProbab = 0.6;
            maxRange = 75;
            maxRangeProbab = 0.1;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 30;
        };
        modelOptics = "\IBL\weapons\EC17\EC17_reticle.p3d";
        class OpticsModes
        {
             class Ironsights
            {
                opticsID = 1;
                useModelOptics = 0;
                opticsFlare = "true";
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.25;
                opticsZoomMax = 1.1;
                opticsZoomInit = 0.75;
                memoryPointCamera = "eye";
                visionMode[] = {};
                distanceZoomMin = 100;
                distanceZoomMax = 100;
            };
            class Scope: Ironsights
            {
                opticsID = 2;
                useModelOptics = 1;
                opticsPPEffects[] = {"OpticsCHAbera5","OpticsBlur5"};
                opticsDisablePeripherialVision = 0.67;
                opticsZoomMin = 0.122173;
                opticsZoomMax = 0.122173;
                opticsZoomInit = 0.122173;
                memoryPointCamera = "opticView";
                visionMode[] = {};
                opticsFlare = "true";
                distanceZoomMin = 300;
                distanceZoomMax = 300;
                cameraDir = "";
            };
        };
    };
};

class CfgMagazines
{
    class Aux501_Weapons_Mags_E5100;

    class Aux501_Weapons_Mags_DH4530: Aux501_Weapons_Mags_E5100
    {
        displayName = "[501st] DH-45 Cell";
        displayNameShort = "DH-45 Cell";
        picture = "\SWLW_clones\pistols\dc17\data\ui\DC17_mag_ui.paa";
        model = "SWLW_clones\pistols\dc17\DC17_mag.p3d";
        ammo = "Aux501_Weapons_Ammo_DH45_Blaster";
        count = 30;
        descriptionShort = "Medium Power Pistol magazine";
        mass = 4;
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_green;

    class Aux501_Weapons_Ammo_DH45_Blaster: Aux501_Weapons_Ammo_base_green
    {
        hit = 10;
        dangerRadiusBulletClose = 8;
        dangerRadiusHit = 12;
        typicalSpeed = 820;
        caliber = 1;
    };
};