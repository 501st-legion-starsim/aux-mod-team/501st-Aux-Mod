class CfgPatches
{
    class Aux501_Patch_Customs_Vests
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};