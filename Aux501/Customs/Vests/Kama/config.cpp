#define AUX501_CUSTOM_VEST_Kama(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_Kama\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF Kama VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            "",\
            \Aux501\Customs\Vests\Kama\Textures\##name##_Kama_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Kama";\
            vest = name;\
        };\
    }

#define AUX501_CUSTOM_VEST_Kama_Holsters(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF Kama VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Kama\Textures\##name##_Kama_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Kama";\
            vest = name;\
        };\
    }

#define AUX501_CUSTOM_VEST_Kama_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_Kama\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF Kama VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            "",\
            \Aux501\Customs\Vests\Kama\Textures\##name##_Kama_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            "",\
            \Aux501\Customs\Vests\Kama\Materials\##name##_Kama.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Kama";\
            vest = name;\
        };\
    }    

#define AUX501_CUSTOM_VEST_Kama_Holsters_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF Kama VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Kama\Textures\##name##_Kama_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Vests\Kama\Materials\##name##_Kama.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Kama";\
            vest = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Vests_Kamas
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_kamas"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_VEST_Eleck",

            "Aux501_Customs_VEST_Noodle"
        };
    };
};

class cfgWeapons
{
    //Kama Vest
    class Aux501_Units_Republic_501_Infantry_Vests_Kama;
    //Example: AUX501_CUSTOM_VEST_Kama(name)


    //Kama Holster
    class Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters;
    //Example: AUX501_CUSTOM_VEST_Kama_Holsters(name)
    AUX501_CUSTOM_VEST_Kama_Holsters(Eleck);

    AUX501_CUSTOM_VEST_Kama_Holsters(Noodle);

    //Kama Vest - RVMAT
    //Example: AUX501_CUSTOM_VEST_Kama_RVMAT(name)


    //Kama Holster - RVMAT
    //Example: AUX501_CUSTOM_VEST_Kama_Holsters_RVMAT(name)
    
};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_501st_Vest_Customs_Kama
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "vest" };
            class vest
            {
                label = "Vest";
                values[] = 
                {
                    "Eleck",

                    "Noodle"
                };
                class Eleck    { label = "Eleck"; };
                
                class Noodle   { label = "Noodle"; };
            };
        };
    };
};