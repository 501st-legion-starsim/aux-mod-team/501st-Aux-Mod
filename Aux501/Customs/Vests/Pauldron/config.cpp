#define AUX501_CUSTOM_VEST_Pauldron(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM Pauldron VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Pauldron\textures\##name##_Pauldron_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Pauldron";\
            vest = name;\
        };\
    }

#define AUX501_CUSTOM_VEST_Pauldron_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM Pauldron VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Pauldron\Textures\##name##_Pauldron_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Vests\Pauldron\Materials\##name##_Pauldron.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Pauldron";\
            vest = name;\
        };\
    }    

class cfgPatches
{
    class Aux501_Patch_Customs_Vests_Pauldron
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Pauldrons"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_VEST_Bandit",

            "Aux501_Customs_VEST_Dakota",
            "Aux501_Customs_VEST_Dusty",

            "Aux501_Customs_VEST_Kestrel", 

            "Aux501_Customs_VEST_Train" 
        };
    };
};

class cfgWeapons
{
    //Standard Pauldron
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon;
    //Example: AUX501_CUSTOM_VEST_Pauldron(name)
    AUX501_CUSTOM_VEST_Pauldron(Dusty);
    AUX501_CUSTOM_VEST_Pauldron(Train);

    //Standard Pauldron - RVMAT
    //Example: AUX501_CUSTOM_VEST_Pauldron_RVMAT(name)
    AUX501_CUSTOM_VEST_Pauldron_RVMAT(Andromeda);

    AUX501_CUSTOM_VEST_Pauldron_RVMAT(Bandit);

    AUX501_CUSTOM_VEST_Pauldron_RVMAT(Dakota);

    AUX501_CUSTOM_VEST_Pauldron_RVMAT(Kestrel);
};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_501st_Vest_Customs_Pauldron
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "vest" };
            class vest
            {
                label = "Vest";
                values[] = 
                {
                    "Andromeda",
                    
                    "Bandit",
                    "Bridger",

                    "Conqueror",

                    "Dakota",
                    "Drammon",
                    "Dusty",

                    "Great",

                    "Hobnob",
                    
                    "Kestrel",

                    "Shatter",

                    "Train"
                };
                class Andromeda  { label = "Andromeda"; };
                class Bandit     { label = "Bandit"; };
                class Bridger    { label = "Bridger"; };
                
                class Conqueror  { label = "Conqueror"; };
                
                class Dakota     { label = "Dakota"; };
                class Drammon    { label = "Drammon"; };
                class Dusty      { label = "Dusty"; };

                class Hobnob     { label = "Hobnob"; };

                class Great      { label = "Great"; };

                class Kestrel    { label = "Kestrel"; };

                class Shatter    { label = "Shatter"; };

                class Train      { label = "Train"; };
            };
        };
    };
};