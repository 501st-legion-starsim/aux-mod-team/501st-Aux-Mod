#define AUX501_CUSTOM_VEST_Pauldron_Pouch(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM Pauldron VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Pauldron\textures\##name##_Pauldron_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Pauldron";\
            vest = name;\
        };\
    }

#define AUX501_CUSTOM_VEST_Pauldron_Pouch_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM Pauldron VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Pauldron\textures\##name##_Pauldron_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Vests\Pauldron\Materials\##name##_Pauldron.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Pauldron";\
            vest = name;\
        };\
    }    

class cfgPatches
{
    class Aux501_Patch_Customs_Vests_Pauldron_Pouch
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Pauldrons"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_VEST_Bridger",

            "Aux501_Customs_VEST_Drammon",

            "Aux501_Customs_VEST_Great"
        };
    };
};

class cfgWeapons
{
    //Pouch Pauldron
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2;
    //Example: AUX501_CUSTOM_VEST_Pauldron_Pouch(name)


    //Pouch Pauldron - RVMAT
    //Example: AUX501_CUSTOM_VEST_Pauldron_Pouch_RVMAT(name)
    AUX501_CUSTOM_VEST_Pauldron_Pouch_RVMAT(Bridger);

    AUX501_CUSTOM_VEST_Pauldron_Pouch_RVMAT(Drammon);

    AUX501_CUSTOM_VEST_Pauldron_Pouch_RVMAT(Great);
};