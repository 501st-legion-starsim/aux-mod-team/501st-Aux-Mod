#define AUX501_CUSTOM_VEST_ARC(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_ARC_Vest_01_A\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM ARC VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\ARC\Textures\##name##_ARC_Vest_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_ARC";\
            vest = name;\
        };\
    }

#define AUX501_CUSTOM_VEST_ARC_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_ARC_Vest_01_A\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM ARC VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\ARC\Textures\##name##_ARC_Vest_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Vests\ARC\Materials\##name##_ARC_Vest.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_ARC";\
            vest = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Vests_ARC
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_ARC_Vests"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_VEST_Blade",
            
            "Aux501_Customs_VEST_Deytow",
            "Aux501_Customs_VEST_Dragony",

            "Aux501_Customs_VEST_Grogg",

            "Aux501_Customs_VEST_Inferno",
            "Aux501_Customs_VEST_Dragony",

            "Aux501_Customs_VEST_Pash",

            "Aux501_Customs_VEST_Tgon",

            "Aux501_Customs_VEST_Widyen"
        };
    };
};

class cfgWeapons
{
    //ARC Vest
    class Aux501_Units_Republic_501_ARC_Vest_01_A;
    //Example: AUX501_CUSTOM_VEST_ARC(name)
    AUX501_CUSTOM_VEST_ARC(Blade);

    AUX501_CUSTOM_VEST_ARC(Deytow);

    AUX501_CUSTOM_VEST_ARC(Dragony);

    AUX501_CUSTOM_VEST_ARC(Grogg);

    AUX501_CUSTOM_VEST_ARC(Inferno);

    AUX501_CUSTOM_VEST_ARC(Pash);

    AUX501_CUSTOM_VEST_ARC(Tgon);

    AUX501_CUSTOM_VEST_ARC(Widyen); 

    //ARC Vest RVMAT
    //Example: AUX501_CUSTOM_VEST_ARC_RVMAT(name)

};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_501st_Vest_Customs_ARC
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "vest" };
            class vest
            {
                label = "Vest";
                values[] = 
                {
                    "Blade",

                    "Deytow",

                    "Dragony",

                    "Grogg",

                    "Inferno",

                    "Pash",

                    "Tgon",

                    "Widyen"
                };
                class Blade      { label = "Blade"; };
                
                class Deytow     { label = "Deytow"; };

                class Dragony    { label = "Dragony"; };

                class Grogg      { label = "Grogg"; };

                class Inferno    { label = "Inferno"; };

                class Pash       { label = "Pash"; };

                class Tgon       { label = "Tgon"; };

                class Widyen     { label = "Widyen"; };
            };
        };
    };
};