//CP Seatbelt
#define AUX501_CUSTOM_VEST_JrNCO_AB_CP(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Airborne_Corporal_Vest_03\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AB JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO_Airborne";\
            vest = name;\
        };\
    }

//CP Seatbelt - RVMAT
#define AUX501_CUSTOM_VEST_JrNCO_AB_CP_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Airborne_Corporal_Vest_03\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AB JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\Materials\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\Materials\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO_Airborne";\
            vest = name;\
        };\
    }

class cfgPatches
{
    class Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO_Airborne_CP
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_VEST_Viperz",
            
            "Aux501_Customs_VEST_Wocky"
        };
    };
};

class cfgWeapons
{   
    //CP Seatbelt
    class Aux501_Units_Republic_501_Airborne_Corporal_Vest_03;
    //Example: AUX501_CUSTOM_VEST_JrNCO_AB_CP(name)
    AUX501_CUSTOM_VEST_JrNCO_AB_CP(Wocky);

    AUX501_CUSTOM_VEST_JrNCO_AB_CP(Viperz);

    //CP Seatbelt - RVMAT
    //Example: AUX501_CUSTOM_VEST_JrNCO_AB_CP_RVMAT(name)
    
};