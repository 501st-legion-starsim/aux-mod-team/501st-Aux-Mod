#define AUX501_CUSTOM_VEST_JrNCO_AB_CS(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO_Airborne";\
            vest = name;\
        };\
    }

#define AUX501_CUSTOM_VEST_JrNCO_AB_CS_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\Materials\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\Materials\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO_Airborne";\
            vest = name;\
        };\
    }

class cfgPatches
{
    class Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO_Airborne_CS
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = {};
    };
};

class cfgWeapons
{  
    //Jr.NCO CS
    class Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04;
    //Example: AUX501_CUSTOM_VEST_JrNCO_AB_CS(name)


    //Jr.NCO CS - RVMAT
    //Example: AUX501_CUSTOM_VEST_JrNCO_AB_CS_RVMAT(name)

};