//Vet Seatbelt
#define AUX501_CUSTOM_VEST_JrNCO_AB_Vet(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Airborne_Veteran_Vest_02\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AB JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO_Airborne";\
            vest = name;\
        };\
    }

//Vet Seatbelt - RVMAT
#define AUX501_CUSTOM_VEST_JrNCO_AB_Vet_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Airborne_Veteran_Vest_02\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AB JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\Materials\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO_Airborne\Materials\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO_Airborne";\
            vest = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Vests_Jr_NCO_Airborne
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_VEST_Calkas",

            "Aux501_Customs_VEST_Tater",

            "Aux501_Customs_VEST_Wocky"
        };
    };
};

class cfgWeapons
{
    //Airborne Vet Seatbelt
    class Aux501_Units_Republic_501_Airborne_Veteran_Vest_02;
    //Example: AUX501_CUSTOM_VEST_JrNCO_AB_Vet(name)
    AUX501_CUSTOM_VEST_JrNCO_AB_Vet(Calkas);

    AUX501_CUSTOM_VEST_JrNCO_AB_Vet(Tater);

    AUX501_CUSTOM_VEST_JrNCO_AB_Vet(Wocky);

    ///Airborne Vet Seatbelt - RVMAT
    //Example: AUX501_CUSTOM_VEST_JrNCO_AB_Vet_Bag_RVMAT(name)

};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO_Airborne
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "vest" };
            class vest
            {
                label = "Vests";
                values[] = 
                {
                    "Calkas",

                    "Tater",

                    "Viperz",

                    "Wocky"
                };
                class Calkas   { label = "Calkas"; };

                class Tater    { label = "Tater"; };

                class Viperz   { label = "Viperz"; };

                class Wocky    { label = "Wocky"; };
            };
        };
    };
};