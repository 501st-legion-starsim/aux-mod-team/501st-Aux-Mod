//CP Seatbelt
#define AUX501_CUSTOM_VEST_JrNCO_CP(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_CP\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }
#define AUX501_CUSTOM_VEST_JrNCO_CP_Bag(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_CP_bag\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            "",\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }

//CP Seatbelt - RVMAT
#define AUX501_CUSTOM_VEST_JrNCO_CP_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_CP\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            "",\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            "",\
            \Aux501\Customs\Vests\Jr_NCO\Materials\seatbelt\##name##_seatbelt.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }

#define AUX501_CUSTOM_VEST_JrNCO_CP_Bag_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_CP_bag\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            "",\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            "",\
            \Aux501\Customs\Vests\Jr_NCO\Materials\seatbelt\##name##_seatbelt.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Vests_Jr_NCO_CP
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_VEST_Ahmanni",
            "Aux501_Customs_VEST_Silver"
        };
    };
};

class cfgWeapons
{   
    //CP Seatbelt
    class Aux501_Units_Republic_501_Infantry_Vests_CP;
    //Example: AUX501_CUSTOM_VEST_JrNCO_CP(name)

    //CP Seatbelt Bag
    class Aux501_Units_Republic_501_Infantry_Vests_CP_bag;
    //Example: AUX501_CUSTOM_VEST_JrNCO_CP_Bag(name)
    AUX501_CUSTOM_VEST_JrNCO_CP_Bag(Ahmanni);
    
    AUX501_CUSTOM_VEST_JrNCO_CP_Bag(Silver);

    //CP Seatbelt - RVMAT
    //Example: AUX501_CUSTOM_VEST_JrNCO_CP_RVMAT(name)
    

    //CP Seatbelt Bag - RVMAT
    //Example: AUX501_CUSTOM_VEST_JrNCO_CP_Bag_RVMAT(name)

};