//Vet Seatbelt
#define AUX501_CUSTOM_VEST_JrNCO_Vet(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }
#define AUX501_CUSTOM_VEST_JrNCO_Vet_Bag(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper_Bag\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            "",\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }

//Vet Seatbelt - RVMAT
#define AUX501_CUSTOM_VEST_JrNCO_Vet_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Vests\Jr_NCO\Materials\seatbelt\##name##_seatbelt.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }

#define AUX501_CUSTOM_VEST_JrNCO_Vet_Bag_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper_Bag\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            "",\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            "",\
            \Aux501\Customs\Vests\Jr_NCO\Materials\seatbelt\##name##_seatbelt.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Vests_Jr_NCO
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Jr_NCOs",
            "Aux501_Patch_Units_Republic_501_Airborne_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = {};
    };
};

class cfgWeapons
{
    //Vet Seatbelt
    class Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper;
    //Example: AUX501_CUSTOM_VEST_JrNCO_Vet(name)

    //Vet Seatbelt Bag
    class Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper_Bag;
    //Example: AUX501_CUSTOM_VEST_JrNCO_Vet_Bag(name)


    //Vet Seatbelt - RVMAT
    //Example: AUX501_CUSTOM_VEST_JrNCO_Vet_RVMAT(name)
    

    ///Vet Seatbelt - RVMAT
    //Example: AUX501_CUSTOM_VEST_JrNCO_Vet_Bag_RVMAT(name)

};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "vest" };
            class vest
            {
                label = "Vests";
                values[] = 
                {
                    "Ahmanni",
                    "Klinger",
                    "September",
                    "Silver",
                    "Tatum",
                    "Testing"
                };
                class Ahmanni    { label = "Ahmanni"; };
                class Klinger    { label = "Klinger"; };
                class September  { label = "September"; };
                class Silver     { label = "Silver"; };
                class Tatum     { label = "Tatum"; };
                class Testing    { label = "Testing"; };
            };
        };
    };
};