#define AUX501_CUSTOM_VEST_JrNCO_CS(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_CS\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO\textures\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }
#define AUX501_CUSTOM_VEST_JrNCO_CS_BAG(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO\textures\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }

#define AUX501_CUSTOM_VEST_JrNCO_CS_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Infantry_Vests_CS\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO\textures\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Vests\Jr_NCO\Materials\kama\##name##_kama.rvmat,\
            \Aux501\Customs\Vests\Jr_NCO\Materials\seatbelt\##name##_seatbelt.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }
#define AUX501_CUSTOM_VEST_JrNCO_CS_BAG_RVMAT(name)\
    class Aux501_Customs_VEST_##name : Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF JrNCO VEST - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Vests\Jr_NCO\textures\kama\##name##_kama_co.paa,\
            \Aux501\Customs\Vests\Jr_NCO\textures\seatbelt\##name##_seatbelt_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Vests\Jr_NCO\Materials\kama\##name##_kama.rvmat,\
            \Aux501\Customs\Vests\Jr_NCO\Materials\seatbelt\##name##_seatbelt.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Jr_NCO";\
            vest = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Vests_Jr_CS
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_VEST_Klinger",
            "Aux501_Customs_VEST_September",
            "Aux501_Customs_VEST_Tatum",
            "Aux501_Customs_VEST_Testing"
        };
    };
};

class cfgWeapons
{  
    //Jr.NCO CS
    class Aux501_Units_Republic_501_Infantry_Vests_CS;
    //Example: AUX501_CUSTOM_VEST_JrNCO_CS(name)
    AUX501_CUSTOM_VEST_JrNCO_CS(September);

    AUX501_CUSTOM_VEST_JrNCO_CS(Tatum);


    //Jr.NCO CS Bag
    class Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04;
    //Example: AUX501_CUSTOM_VEST_JrNCO_CS_BAG(name)
    

    //Jr.NCO CS - RVMAT
    //Example: AUX501_CUSTOM_VEST_JrNCO_CS_RVMAT(name)
    
    AUX501_CUSTOM_VEST_JrNCO_CS_RVMAT(Testing);

    //Jr.NCO CS CS Bag - RVMAT
    //Example: AUX501_CUSTOM_VEST_JrNCO_CS_BAG_RVMAT(name)
    AUX501_CUSTOM_VEST_JrNCO_CS_BAG_RVMAT(Klinger);
};