#define AUX501_CUSTOM_Uniform_Aviation(name)\
    class Aux501_Customs_Uniform_##name : Aux501_Units_Republic_501st_Aviation_Flight_Cadet_Uniform\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AVI ARMR - name;\
        class ItemInfo : ItemInfo\
        {\
            uniformClass = Aux501_Customs_Uniform_Vehicle_##name;\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Custom_Uniforms_Aviation";\
            uniform = name;\
        };\
    }

//Uniform Vehicle
#define AUX501_CUSTOM_Uniform_Vehicle_Aviation(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Unit_Base\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Aviation\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Aviation\Textures\##name##_lower_co.paa\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

//Custom Rvmat
#define AUX501_CUSTOM_Uniform_Vehicle_Aviation_RVMAT(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Unit_Base\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Aviation\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Aviation\Textures\##name##_lower_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Uniforms\Aviation\Materials\##name##_upper.rvmat,\
            \Aux501\Customs\Uniforms\Aviation\Materials\##name##_lower.rvmat\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Uniforms_Aviation
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Razor_Uniforms"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_Uniform_Argo",
            
            "Aux501_Customs_Uniform_Bacon"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_Trooper_Uniform;

    class Aux501_Units_Republic_501st_Aviation_Flight_Cadet_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        class ItemInfo;
    };

    //Aviation Phase 2 Uniform
    //Example: AUX501_CUSTOM_Uniform_Aviation(name)

    AUX501_CUSTOM_Uniform_Aviation(Argo);
    
    AUX501_CUSTOM_Uniform_Aviation(Bacon);

};

class CfgVehicles
{
    //Aviation Phase 2 Uniform Vehicle
    class Aux501_Units_Republic_501st_Unit_Base;
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Aviation(name)

    AUX501_CUSTOM_Uniform_Vehicle_Aviation(Argo);

    AUX501_CUSTOM_Uniform_Vehicle_Aviation(Bacon);

    //Aviation Phase 2 Uniform Vehicle - RVMAT
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Aviation_RVMAT(name)


};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_501st_Custom_Uniforms_Aviation
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "uniform" };
            class uniform
            {
                label = "Uniform";
                values[] = 
                {
                    
                    "Argo",

                    "Bacon"
                };
                class Argo        { label = "Argo"; };
                class Bacon        { label = "Bacon"; };
            };
        };
    };
};