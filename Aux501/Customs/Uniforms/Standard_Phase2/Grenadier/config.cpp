#define AUX501_CUSTOM_Uniform_Phase2_Grenadier(name)\
    class Aux501_Customs_Uniform_##name : Aux501_Units_Republic_501st_Trooper_Uniform\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF ARMR - name;\
        class ItemInfo : ItemInfo\
        {\
            uniformClass = Aux501_Customs_Uniform_Vehicle_##name;\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Custom_Uniforms_Standard_Phase2";\
            uniform = name;\
            toggle = "off";\
        };\
    }

//Uniform Vehicle
#define AUX501_CUSTOM_Uniform_Vehicle_Phase2_Grenadier(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Trooper_Gren_Uniform_Vehicle\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_lower_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_attachments_co.paa\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

//Custom Rvmat
#define AUX501_CUSTOM_Uniform_Vehicle_Phase2_Grenadier_RVMAT(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Trooper_Gren_Uniform_Vehicle\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_lower_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_attachments_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Uniforms\Standard_Phase2\Materials\##name##_upper.rvmat,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Materials\##name##_lower.rvmat,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Materials\##name##_attachments.rvmat\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Uniforms_Phase2_Grenadier
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2_grenadier"
        };
        units[] = 
        {
            "Aux501_Customs_Uniform_Vehicle_Death"
        };
        weapons[] = 
        {
            "Aux501_Customs_Uniform_Death"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_Standard_Uniform;

    class Aux501_Units_Republic_501st_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        class ItemInfo;
    };

    //Grenadier Phase 2 Uniform
    //Example: AUX501_CUSTOM_Uniform_Phase2_Grenadier(name)
    AUX501_CUSTOM_Uniform_Phase2_Grenadier(Death);

};

class CfgVehicles
{
    //Grenadier Phase 2 Uniform Vehicle
    class Aux501_Units_Republic_501st_Trooper_Gren_Uniform_Vehicle;
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Phase2_Grenadier(name)
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Grenadier(Death);


    //Grenadier Phase 2 Uniform Vehicle - RVMAT
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Phase2_Grenadier_RVMAT(name)
    

};