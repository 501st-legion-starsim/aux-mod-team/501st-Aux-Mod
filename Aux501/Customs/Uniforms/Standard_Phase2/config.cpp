#define AUX501_CUSTOM_Uniform_Phase2(name)\
    class Aux501_Customs_Uniform_##name : Aux501_Units_Republic_501st_Trooper_Uniform\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF ARMR - name;\
        class ItemInfo : ItemInfo\
        {\
            uniformClass = Aux501_Customs_Uniform_Vehicle_##name;\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Custom_Uniforms_Standard_Phase2";\
            uniform = name;\
            toggle = "off";\
        };\
    }

#define AUX501_CUSTOM_Uniform_Phase2_Funni(name)\
    class Aux501_Customs_Uniform_Funni_##name : Aux501_Units_Republic_501st_Trooper_Uniform\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF ARMR - name;\
        class ItemInfo : ItemInfo\
        {\
            uniformClass = Aux501_Customs_Uniform_Vehicle_Funni_##name;\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Custom_Uniforms_Standard_Phase2";\
            uniform = name;\
            toggle = "on";\
        };\
    }

//Uniform Vehicle
#define AUX501_CUSTOM_Uniform_Vehicle_Phase2(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Unit_Base\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_lower_co.paa\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

//Custom Rvmat
#define AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Unit_Base\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_lower_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Uniforms\Standard_Phase2\Materials\##name##_upper.rvmat,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Materials\##name##_lower.rvmat\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }
#define AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT_Funni(name)\
    class Aux501_Customs_Uniform_Vehicle_Funni_##name : Aux501_Units_Republic_501st_Unit_Base\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_lower_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Helmets\Phase2\Materials\##name##_Visor.rvmat,\
            \Aux501\Customs\Helmets\Phase2\Materials\##name##_Visor.rvmat\
        };\
        uniformClass = AUX501_CUSTOM_Uniform_Phase2_Funni_##name;\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Uniforms_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2",
            "Aux501_Patch_Units_Republic_501_Infantry"
        };
        units[] = 
        {
            "Aux501_Customs_Uniform_Vehicle_Boneish",
            "Aux501_Customs_Uniform_Vehicle_Bridger",

            "Aux501_Customs_Uniform_Vehicle_Crinkcase",

            "Aux501_Customs_Uniform_Vehicle_Driverge",
            "Aux501_Customs_Uniform_Vehicle_Dusty",

            "Aux501_Customs_Uniform_Vehicle_Great",

            "Aux501_Customs_Uniform_Vehicle_Grey",

            "Aux501_Customs_Uniform_Vehicle_Jatt",
            "Aux501_Customs_Uniform_Vehicle_Jimmy",

            "Aux501_Customs_Uniform_Vehicle_Kestrel",

            "Aux501_Customs_Uniform_Vehicle_Osiris",

            "Aux501_Customs_Uniform_Vehicle_Shadow",
            "Aux501_Customs_Uniform_Vehicle_Shatter",
            "Aux501_Customs_Uniform_Vehicle_Silver",
            "Aux501_Customs_Uniform_Vehicle_Swanny",

            "Aux501_Customs_Uniform_Vehicle_Train",

            "Aux501_Customs_Uniform_Vehicle_Unity",

            "Aux501_Customs_Uniform_Vehicle_Worgan"
        };
        weapons[] = 
        {
            "Aux501_Customs_Uniform_Boneish",
            "Aux501_Customs_Uniform_Bridger",

            "Aux501_Customs_Uniform_Crinkcase",

            "Aux501_Customs_Uniform_Dakota",
            "Aux501_Customs_Uniform_Diverge",
            "Aux501_Customs_Uniform_Dusty",

            "Aux501_Customs_Uniform_Great",
            "Aux501_Customs_Uniform_Grey",
            
            "Aux501_Customs_Uniform_Jatt",
            "Aux501_Customs_Uniform_Jimmy",
            
            "Aux501_Customs_Uniform_Kestrel",

            "Aux501_Customs_Uniform_Marshmellow",

            "Aux501_Customs_Uniform_Osiris",

            "Aux501_Customs_Uniform_Shadow",
            "Aux501_Customs_Uniform_Shatter",
            "Aux501_Customs_Uniform_Silver",
            "Aux501_Customs_Uniform_Swanny",

            "Aux501_Customs_Uniform_Train",

            "Aux501_Customs_Uniform_Unity",

            "Aux501_Customs_Uniform_Worgan"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_Standard_Uniform;

    class Aux501_Units_Republic_501st_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        class ItemInfo;
    };

    //Standard Phase 2 Uniform
    //Example: AUX501_CUSTOM_Uniform_Phase2(name)

    AUX501_CUSTOM_Uniform_Phase2(Boneish);
    AUX501_CUSTOM_Uniform_Phase2(Bridger);

    AUX501_CUSTOM_Uniform_Phase2(Crinkcase);

    AUX501_CUSTOM_Uniform_Phase2(Dakota);
    AUX501_CUSTOM_Uniform_Phase2(Diverge);
    AUX501_CUSTOM_Uniform_Phase2(Dusty);

    AUX501_CUSTOM_Uniform_Phase2(Great);
    AUX501_CUSTOM_Uniform_Phase2(Grey);

    AUX501_CUSTOM_Uniform_Phase2(Hobnob);
    AUX501_CUSTOM_Uniform_Phase2_Funni(Hobnob);
    
    AUX501_CUSTOM_Uniform_Phase2(Jatt);
    AUX501_CUSTOM_Uniform_Phase2(Jimmy);
    
    AUX501_CUSTOM_Uniform_Phase2(Kestrel);

    AUX501_CUSTOM_Uniform_Phase2(Marshmellow);
    AUX501_CUSTOM_Uniform_Phase2(Morris);

    AUX501_CUSTOM_Uniform_Phase2(Noodle);

    AUX501_CUSTOM_Uniform_Phase2(Osiris);

    AUX501_CUSTOM_Uniform_Phase2(Shadow);
    AUX501_CUSTOM_Uniform_Phase2(Shatter);
    AUX501_CUSTOM_Uniform_Phase2(Silver);
    AUX501_CUSTOM_Uniform_Phase2(Swanny);

    AUX501_CUSTOM_Uniform_Phase2(Train);

    AUX501_CUSTOM_Uniform_Phase2(Unity);

    AUX501_CUSTOM_Uniform_Phase2(Worgan);
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    //Standard Phase 2 Uniform Vehicle
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Phase2(name)
    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Boneish);
    
    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Crinkcase);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Diverge);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Grey);
    
    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Jatt);
    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Jimmy);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Marshmellow);
    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Morris);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Noodle);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Osiris);
    
    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Shadow);
    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Silver);
    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Swanny);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Train);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Unity);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2(Worgan);

    //Standard Phase 2 Uniform Vehicle - RVMAT
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT(name)

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT(Bridger);
    
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT(Dakota);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT(Dusty);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT(Great);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT(Hobnob);
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT_Funni(Hobnob);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT(Kestrel);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_RVMAT(Shatter);
};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_501st_Custom_Uniforms_Standard_Phase2
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            { 
                "uniform",
                "toggle"
            };
            class toggle
            {
                label = "Toggle";
                changeingame = 1;
                changedelay = 0.5;
                values[] = 
                {
                    "off",
                    "on"
                };
                class off  
                { 
                    label = "Off"; 
                    actionLabel = "Normal Kirbo";
                };
                class on   
                { 
                    label = "On"; 
                    actionLabel = "Battle Kirbo";
                };
            };
            class uniform
            {
                label = "Uniform";
                values[] = 
                {
                    "Ahmanni",
                    "Andromeda",
                    "Ascanius",
                    
                    "Boneish",
                    "Bridger",

                    "Chaser",
                    "Crinkcase",

                    "Dakota",
                    "Death",
                    "Diverge",
                    "Donut",
                    "Drifter",
                    "Dusty",

                    "Hobnob",
                    
                    "Great",
                    "Grey",

                    "Jatt",
                    "Jimmy",

                    "Kestrel",
                    
                    "Maple",
                    "Marshmellow",
                    "Morris",
                    
                    "Nimroch",
                    "Noodle",
                    
                    "Osiris",

                    "Revenant",

                    "Shadow",
                    "Shape",
                    "Shatter",
                    "Shredded",
                    "Silver", 
                    "Swanny",

                    "Taka",
                    "Talker",
                    "TEST",
                    "Train",
                    "Trip",

                    "Unity",

                    "Worgan"
                };
                class Ahmanni           { label = "Ahmanni"; };
                class Andromeda         { label = "Andromeda"; };
                class Ascanius          { label = "Ascanius"; };
                
                class Boneish           { label = "Boneish"; };
                class Bridger           { label = "Bridger"; };
                
                class Chaser            { label = "Chaser"; };
                class Crinkcase         { label = "Crinkcase"; };

                class Dakota            { label = "Dakota"; };
                class Death             { label = "Death"; };
                class Diverge           { label = "Diverge"; };
                class Donut             { label = "Donut"; };
                class Drifter           { label = "Drifter"; };
                class Dusty             { label = "Dusty"; };

                class Great             { label = "Great"; };
                class Grey              { label = "Grey"; };

                class Hobnob            { label = "Hobnob"; };
                
                class Jatt              { label = "Jatt"; };
                class Jimmy             { label = "Jimmy"; };
                
                class Kestrel           { label = "Kestrel"; };

                class Maple             { label = "Maple"; };
                class Marshmellow       { label = "Marshmellow"; };
                class Morris            { label = "Morris"; };

                class Nimroch           { label = "Nimroch"; };
                class Noodle            { label = "Noodle"; };
                
                class Osiris            { label = "Osiris"; };

                class Revenant          { label = "Revenant"; };

                class Shadow            { label = "Shadow"; };
                class Shape             { label = "Shape"; };
                class Shatter           { label = "Shatter"; };
                class Shredded          { label = "Shredded"; };
                class Silver            { label = "Silver"; };
                class Swanny            { label = "Swanny"; };
                
                class Taka              { label = "Taka"; };
                class Talker            { label = "Talker"; };
                class TEST              { label = "TEST"; };
                class Train             { label = "Train"; };
                class Trip              { label = "Trip"; };

                class Unity             { label = "Unity"; };

                class Worgan            { label = "Worgan"; };
            };
        };
    };
};