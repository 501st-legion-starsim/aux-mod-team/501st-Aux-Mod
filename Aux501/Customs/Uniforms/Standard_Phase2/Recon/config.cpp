#define AUX501_CUSTOM_Uniform_Phase2_Recon(name)\
    class Aux501_Customs_Uniform_##name : Aux501_Units_Republic_501st_Trooper_Uniform\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF ARMR - name;\
        class ItemInfo : ItemInfo\
        {\
            uniformClass = Aux501_Customs_Uniform_Vehicle_##name;\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Custom_Uniforms_Standard_Phase2";\
            uniform = name;\
            toggle = "off";\
        };\
    }

//Uniform Vehicle
#define AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Trooper_Recon_Uniform_Vehicle\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_lower_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_attachments_co.paa\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

//Custom Rvmat
#define AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon_RVMAT(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Trooper_Recon_Uniform_Vehicle\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_lower_co.paa,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Textures\##name##_attachments_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Uniforms\Standard_Phase2\Materials\##name##_upper.rvmat,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Materials\##name##_lower.rvmat,\
            \Aux501\Customs\Uniforms\Standard_Phase2\Materials\##name##_attachments.rvmat\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Uniforms_Phase2_Recon
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2_Recon"
        };
        units[] = 
        {
            "Aux501_Customs_Uniform_Vehicle_Ahmanni",
            "Aux501_Customs_Uniform_Vehicle_Ascanius",

            "Aux501_Customs_Uniform_Vehicle_Chaser",
            
            "Aux501_Customs_Uniform_Vehicle_Donut",
            "Aux501_Customs_Uniform_Vehicle_Drifter",
            
            "Aux501_Customs_Uniform_Vehicle_Nimroch",

            "Aux501_Customs_Uniform_Vehicle_Revenant",
            
            "Aux501_Customs_Uniform_Vehicle_Shape",
            "Aux501_Customs_Uniform_Vehicle_Shredded",

            "Aux501_Customs_Uniform_Vehicle_Taka",
            "Aux501_Customs_Uniform_Vehicle_Talker",
            "Aux501_Customs_Uniform_Vehicle_Testing",
            "Aux501_Customs_Uniform_Vehicle_Trip"
        };
        weapons[] = 
        {
            "Aux501_Customs_Uniform_Ahmanni",
            "Aux501_Customs_Uniform_Ascanius",

            "Aux501_Customs_Uniform_Chaser",
            
            "Aux501_Customs_Uniform_Donut",
            "Aux501_Customs_Uniform_Drifter",

            "Aux501_Customs_Uniform_Nimroch",

            "Aux501_Customs_Uniform_Revenant",

            "Aux501_Customs_Uniform_Shape",
            "Aux501_Customs_Uniform_Shredded",
            
            "Aux501_Customs_Uniform_Taka",
            "Aux501_Customs_Uniform_Talker",
            "Aux501_Customs_Uniform_Testing",
            "Aux501_Customs_Uniform_Trip"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_Standard_Uniform;

    class Aux501_Units_Republic_501st_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        class ItemInfo;
    };

    //Recon Phase 2 Uniform
    //Example: AUX501_CUSTOM_Uniform_Phase2_Recon(name)
    AUX501_CUSTOM_Uniform_Phase2_Recon(Ahmanni);
    AUX501_CUSTOM_Uniform_Phase2_Recon(Ascanius);

    AUX501_CUSTOM_Uniform_Phase2_Recon(Chaser);
    
    AUX501_CUSTOM_Uniform_Phase2_Recon(Donut);
    AUX501_CUSTOM_Uniform_Phase2_Recon(Drifter);
    
    AUX501_CUSTOM_Uniform_Phase2_Recon(Maple);

    AUX501_CUSTOM_Uniform_Phase2_Recon(Nimroch);

    AUX501_CUSTOM_Uniform_Phase2_Recon(Revenant);

    AUX501_CUSTOM_Uniform_Phase2_Recon(Shape);
    AUX501_CUSTOM_Uniform_Phase2_Recon(Shredded);
    
    AUX501_CUSTOM_Uniform_Phase2_Recon(Taka);
    AUX501_CUSTOM_Uniform_Phase2_Recon(Talker);
    AUX501_CUSTOM_Uniform_Phase2_Recon(TEST);
    AUX501_CUSTOM_Uniform_Phase2_Recon(Trip);
};

class CfgVehicles
{
    //Recon Phase 2 Uniform Vehicle
    class Aux501_Units_Republic_501st_Trooper_Recon_Uniform_Vehicle;
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(name)
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Ahmanni);
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Ascanius);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Chaser);
    
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Donut);
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Drifter);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Maple);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Nimroch);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Revenant);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Shape);
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Shredded);

    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Taka);
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Talker);
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon(Trip);

    //Recon Phase 2 Uniform Vehicle - RVMAT
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon_RVMAT(name)
    
    AUX501_CUSTOM_Uniform_Vehicle_Phase2_Recon_RVMAT(TEST);
};