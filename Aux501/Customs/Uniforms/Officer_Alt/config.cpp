#define AUX501_CUSTOM_Uniform_Officer_Alt(name)\
    class Aux501_Customs_Uniform_##name : Aux501_Units_Republic_501st_2LT_Alt_Uniform\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM CMDR ARMR - name;\
        class ItemInfo : ItemInfo\
        {\
            uniformClass = Aux501_Customs_Uniform_Vehicle_##name;\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Custom_Uniforms_Officer_Alt";\
            uniform = name;\
        };\
    }

//Uniform Vehicle
#define AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Officer_Alt\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Officer_Alt\Textures\##name##_lower_co.paa,\
            \Aux501\Customs\Uniforms\Officer_Alt\Textures\##name##_rankplate_co.paa\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }
#define AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt_NoRankPlate(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Officer_Alt\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Officer_Alt\Textures\##name##_lower_co.paa,\
            ""\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

//Custom Rvmat
#define AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt_RVMAT(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Officer_Alt\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Officer_Alt\Textures\##name##_lower_co.paa,\
            \Aux501\Customs\Uniforms\Officer_Alt\Textures\##name##_rankplate_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Uniforms\Officer_Alt\Materials\##name##_upper.rvmat,\
            \Aux501\Customs\Uniforms\Officer_Alt\Materials\##name##_lower.rvmat,\
            \Aux501\Customs\Uniforms\Officer_Alt\Materials\##name##_rankplate.rvmat\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }
#define AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt_NoRankPlate_RVMAT(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Officer_Alt\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Officer_Alt\Textures\##name##_lower_co.paa,\
            ""\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Uniforms\Officer_Alt\Materials\##name##_upper.rvmat,\
            \Aux501\Customs\Uniforms\Officer_Alt\Materials\##name##_lower.rvmat,\
            ""\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Uniforms_Officer_Alt
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2_Officer_Alt"
        };
        units[] = 
        {
            "Aux501_Customs_Uniform_Vehicle_Andromeda",

            "Aux501_Customs_Uniform_Vehicle_Drammon",

            "Aux501_Customs_Uniform_Vehicle_Klinger"
        };
        weapons[] = 
        {
            "Aux501_Customs_Uniform_Andromeda",

            "Aux501_Customs_Uniform_Drammon",

            "Aux501_Customs_Uniform_Klinger"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_2LT_Uniform;

    class Aux501_Units_Republic_501st_2LT_Alt_Uniform: Aux501_Units_Republic_501st_2LT_Uniform
    {
        class ItemInfo;
    };

    //Officer Alt Uniform
    //Example: AUX501_CUSTOM_Uniform_Officer_Alt(name)

    AUX501_CUSTOM_Uniform_Officer_Alt(Andromeda);
    
    AUX501_CUSTOM_Uniform_Officer_Alt(Drammon);
    
    AUX501_CUSTOM_Uniform_Officer_Alt(Klinger);
};

class CfgVehicles
{
    //Officer Alt Uniform Vehicle
    class Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle;
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt(name)

    //Officer Alt Uniform Vehicle - No Rank Plate
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt_NoRankPlate(name)
    
    AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt_NoRankPlate(Klinger);

    //Officer Alt Uniform Vehicle - RVMAT
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt_RVMAT(name)


    
    //Officer Alt Uniform Vehicle - No Rank Plate - RVMAT
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt_NoRankPlate_RVMAT(name)

    AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt_NoRankPlate_RVMAT(Andromeda);
   
    AUX501_CUSTOM_Uniform_Vehicle_Officer_Alt_NoRankPlate_RVMAT(Drammon);
};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_501st_Custom_Uniforms_Officer_Alt
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "uniform" };
            class uniform
            {
                label = "Uniform";
                values[] = 
                {
                    "Andromeda",
                    
                    "Drammon",

                    "Klinger"
                };
            };
            class Andromeda   { label = "Andromeda"; };
            
            class Drammon    { label = "Drammon"; };
            
            class Klinger    { label = "Klinger"; };
        };
    };
};