#define AUX501_CUSTOM_Uniform_ARC(name)\
    class Aux501_Customs_Uniform_##name : Aux501_Units_Republic_501st_ARC_Trooper_Uniform\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM ARC ARMR - name;\
        class ItemInfo : ItemInfo\
        {\
            uniformClass = Aux501_Customs_Uniform_Vehicle_##name;\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Custom_Uniforms_ARC";\
            uniform = name;\
        };\
    }

//Uniform Vehicle
#define AUX501_CUSTOM_Uniform_Vehicle_ARC(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Unit_Base\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\ARC\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\ARC\Textures\##name##_lower_co.paa\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

//Custom Rvmat
#define AUX501_CUSTOM_Uniform_Vehicle_ARC_RVMAT(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Unit_Base\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\ARC\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\ARC\Textures\##name##_lower_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Uniforms\ARC\Materials\##name##_upper.rvmat,\
            \Aux501\Customs\Uniforms\ARC\Materials\##name##_lower.rvmat\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Uniforms_ARC
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_ARC_Uniforms"
        };
        units[] = 
        {
            "Aux501_Customs_Uniform_Vehicle_Inferno",

            "Aux501_Customs_Uniform_Vehicle_Mane",
            
            "Aux501_Customs_Uniform_Vehicle_Sabre"
        };
        weapons[] = 
        {
            "Aux501_Customs_Uniform_Inferno",

            "Aux501_Customs_Uniform_Mane",
            
            "Aux501_Customs_Uniform_Sabre"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_Trooper_Uniform;

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        class ItemInfo;
    };

    //ARC Phase 2 Uniform
    //Example: AUX501_CUSTOM_Uniform_ARC(name)
    AUX501_CUSTOM_Uniform_ARC(Inferno);

    AUX501_CUSTOM_Uniform_ARC(Mane);
    
    AUX501_CUSTOM_Uniform_ARC(Sabre);

};

class CfgVehicles
{
    //ARC Phase 2 Uniform Vehicle
    class Aux501_Units_Republic_501st_Unit_Base;
    
    //Example: AUX501_CUSTOM_Uniform_Vehicle_ARC(name);
    AUX501_CUSTOM_Uniform_Vehicle_ARC(Inferno);

    AUX501_CUSTOM_Uniform_Vehicle_ARC(Mane);
    
    AUX501_CUSTOM_Uniform_Vehicle_ARC(Sabre);

    //ARC Phase 2 Uniform Vehicle - RVMAT
    //Example: AUX501_CUSTOM_Uniform_Vehicle_ARC_RVMAT(name);
    

};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_501st_Custom_Uniforms_ARC
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "uniform" };
            class uniform
            {
                label = "Uniform";
                values[] = 
                {
                    "Inferno",

                    "Mane",
                    
                    "Sabre"
                };
                class Inferno   { label = "Inferno"; };

                class Mane      { label = "Mane"; };
                
                class Sabre     { label = "Sabre"; };
            };
        };
    };
};