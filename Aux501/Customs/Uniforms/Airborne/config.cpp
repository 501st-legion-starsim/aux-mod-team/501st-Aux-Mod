#define AUX501_CUSTOM_Uniform_Airborne(name)\
    class Aux501_Customs_Uniform_##name : Aux501_Units_Republic_501st_Airborne_Trooper_Uniform\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AB ARMR - name;\
        class ItemInfo : ItemInfo\
        {\
            uniformClass = Aux501_Customs_Uniform_Vehicle_##name;\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Custom_Uniforms_Airborne";\
            uniform = name;\
        };\
    }

//Uniform Vehicle
#define AUX501_CUSTOM_Uniform_Vehicle_Airborne(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Unit_Base\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Airborne\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Airborne\Textures\##name##_lower_co.paa\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

//Custom Rvmat
#define AUX501_CUSTOM_Uniform_Vehicle_Airborne_RVMAT(name)\
    class Aux501_Customs_Uniform_Vehicle_##name : Aux501_Units_Republic_501st_Unit_Base\
    {\
        scope = 1;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Uniforms\Airborne\Textures\##name##_upper_co.paa,\
            \Aux501\Customs\Uniforms\Airborne\Textures\##name##_lower_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Uniforms\Airborne\Materials\##name##_upper.rvmat,\
            \Aux501\Customs\Uniforms\Airborne\Materials\##name##_lower.rvmat\
        };\
        uniformClass = Aux501_Customs_Uniform_##name;\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Uniforms_Airborne
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Uniforms"
        };
        units[] = 
        {
            "Aux501_Customs_Uniform_Vehicle_Bandit",

            "Aux501_Customs_Uniform_Vehicle_Juggernaut",
            
            "Aux501_Customs_Uniform_Vehicle_Stomerone",

            "Aux501_Customs_Uniform_Vehicle_Tater",

            "Aux501_Customs_Uniform_Vehicle_Venom"
        };
        weapons[] = 
        {
            "Aux501_Customs_Uniform_Bandit",

            "Aux501_Customs_Uniform_Juggernaut",
            
            "Aux501_Customs_Uniform_Stomerone",

            "Aux501_Customs_Uniform_Tater",

            "Aux501_Customs_Uniform_Venom"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_Airborne_Cadet_Uniform;

    class Aux501_Units_Republic_501st_Airborne_Trooper_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        class ItemInfo;
    };

    //Airborne Phase 2 Uniform
    //Example: AUX501_CUSTOM_Uniform_Airborne(name);
    AUX501_CUSTOM_Uniform_Airborne(Bandit);
    
    AUX501_CUSTOM_Uniform_Airborne(Exodus);

    AUX501_CUSTOM_Uniform_Airborne(Juggernaut);

    AUX501_CUSTOM_Uniform_Airborne(Stomerone);

    AUX501_CUSTOM_Uniform_Airborne(Tater);

    AUX501_CUSTOM_Uniform_Airborne(Venom);

};

class CfgVehicles
{
    //Airborne Phase 2 Uniform Vehicle
    class Aux501_Units_Republic_501st_Unit_Base;
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Airborne(name);
    AUX501_CUSTOM_Uniform_Vehicle_Airborne(Juggernaut);
    
    AUX501_CUSTOM_Uniform_Vehicle_Airborne(Stomerone);

    AUX501_CUSTOM_Uniform_Vehicle_Airborne(Tater);

    AUX501_CUSTOM_Uniform_Vehicle_Airborne(Venom);


    //Airborne Phase 2 Uniform Vehicle - RVMAT
    //Example: AUX501_CUSTOM_Uniform_Vehicle_Airborne_RVMAT(name);
    
    AUX501_CUSTOM_Uniform_Vehicle_Airborne_RVMAT(Bandit);

    AUX501_CUSTOM_Uniform_Vehicle_Airborne_RVMAT(Exodus);
};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_501st_Custom_Uniforms_Airborne
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "uniform" };
            class uniform
            {
                label = "Uniform";
                values[] = 
                {
                    "Bandit",

                    "Exodus",

                    "Juggernaut",

                    "Stomerone",

                    "Tater",

                    "Venom"
                };
                class Bandit   { label = "Bandit"; };

                class Exodus   { label = "Exodus"; };

                class Juggernaut   { label = "Juggernaut"; };
                
                class Stomerone    { label = "Stomerone"; };

                class Tater    { label = "Tater"; };

                class Venom    { label = "Venom"; };
            };
        };
    };
};