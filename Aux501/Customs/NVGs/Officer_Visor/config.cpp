class cfgPatches
{
    class Aux501_Patch_Customs_NVGs_Officer_Visor
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "trooper" };
            class trooper
            {
                label = "NVG";
                values[] = 
                {
                    "Bandit",

                    "Drammon",

                    "Hobnob",

                    "Klinger",
                    
                    "Testing2"
                };
                class Bandit      { label = "Bandit"; };

                class Drammon     { label = "Drammon"; };

                class Hobnob      { label = "Hobnob"; };

                class Klinger     { label = "Klinger"; };

                class Testing2    { label = "Testing"; };
            };
        };
    };
};