//LowLight Only
#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_white_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_white_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_white_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_light_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_light_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_dark_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_dark_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_light_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_light_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_dark_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_dark_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_yellow_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_yellow_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_brightest_color_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_brightest_color_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

//LowLight Only - RVMAT
#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_white_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_white_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_white_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_light_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_light_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_dark_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_dark_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_light_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_light_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_dark_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_dark_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_yellow_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_yellow_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Commander_Visor_lowlight_brightest_color_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_brightest_color_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_NVGs_Commander_Visor_Lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor"
        };
        units[] = {};
        weapons[] = {};
    };
};

class cfgWeapons
{  
    //Commander Lowlight - White Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_white_hot(name)


    //Commander Lowlight - Black Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_black_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_black_hot(name)

    //Commander Lowlight - White/Black Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_white_black_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_white_black_hot(name)


    //Commander Lowlight - Light Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_light_green_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_light_green_hot(name)


    //Commander Lowlight - Dark Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_dark_green_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_dark_green_hot(name)


    //Commander Lowlight - Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_green_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_green_hot(name)


    //Commander Lowlight - Light Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_light_orange_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_light_orange_hot(name)


    //Commander Lowlight - Dark Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_dark_orange_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_dark_orange_hot(name)


    //Commander Lowlight - Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_orange_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_orange_hot(name)


    //Commander Lowlight - Yellow Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_yellow_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_yellow_hot(name)


    //Commander Lowlight - Brightest Color Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_brightest_color_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_brightest_color_hot(name)

    

    //RVMAT//

    //Commander Lowlight - RVMAT - White Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_white_hot_RVMAT(name)


    //Commander Lowlight - RVMAT - Black Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_black_hot_RVMAT(name)


    //Commander Lowlight - RVMAT - White/Black Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_white_black_hot_RVMAT(name)


    //Commander Lowlight - RVMAT - Light Green Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_light_green_hot_RVMAT(name)


    //Commander Lowlight - RVMAT - Dark Green Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_dark_green_hot_RVMAT(name)


    //Commander Lowlight - RVMAT - Green Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_green_hot_RVMAT(name)


    //Commander Lowlight - RVMAT - Light Orange Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_light_orange_hot_RVMAT(name)


    //Commander Lowlight - RVMAT - Dark Orange Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_dark_orange_hot_RVMAT(name)


    //Commander Lowlight - RVMAT - Orange Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_orange_hot_RVMAT(name)


    //Commander Lowlight - RVMAT - Yellow Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_yellow_hot_RVMAT(name)


    //Commander Lowlight - RVMAT - Brightest Color Hot
    //Example: AUX501_CUSTOM_NVG_Commander_Visor_lowlight_brightest_color_hot_RVMAT(name)


};