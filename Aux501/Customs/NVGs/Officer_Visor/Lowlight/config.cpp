//LowLight Only
#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_white_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_white_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_white_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_light_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_dark_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_light_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_dark_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_yellow_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_yellow_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_brightest_color_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

//LowLight Only - RVMAT
#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_white_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_white_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_white_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_light_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_dark_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_light_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_dark_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_yellow_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_yellow_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_lowlight_brightest_color_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_NVGs_Officer_Visor_Lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_black_hot_lowlight",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_white_black_hot_lowlight",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_light_green_hot_lowlight",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_dark_green_hot_lowlight",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_green_hot_lowlight",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_light_orange_hot_lowlight",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_dark_orange_hot_lowlight",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_orange_hot_lowlight",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_yellow_hot_lowlight",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_brightest_color_hot_lowlight"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_NVG_Bandit",

            "Aux501_Customs_NVG_Hobnob",

            "Aux501_Customs_NVG_Klinger",

            "Aux501_Customs_NVG_Testing2"
        };
    };
};

class cfgWeapons
{
    //Officer Lowlight - White Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_white_hot(name)

    
    //Officer Lowlight - Black Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_black_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_black_hot(name)



    //Officer Lowlight - White/Black Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_white_black_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_white_black_hot(name)



    //Officer Lowlight - Light Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_green_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_light_green_hot(name)



    //Officer Lowlight - Dark Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_green_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_dark_green_hot(name)



    //Officer Lowlight - Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_green_hot(name)



    //Officer Lowlight - Light Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_orange_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_light_orange_hot(name)



    //Officer Lowlight - Dark Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_orange_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_dark_orange_hot(name)



    //Officer Lowlight - Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_orange_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_orange_hot(name)



    //Officer Lowlight - Yellow Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_yellow_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_yellow_hot(name)

    AUX501_CUSTOM_NVG_Officer_Visor_lowlight_yellow_hot(Klinger);

    //Officer Lowlight - Brightest Color Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_brightest_color_hot(name)


    //RVMAT//

    //Officer Lowlight - RVMAT - White Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_white_hot_RVMAT(name)

    AUX501_CUSTOM_NVG_Officer_Visor_lowlight_white_hot_RVMAT(Bandit);

    AUX501_CUSTOM_NVG_Officer_Visor_lowlight_white_hot_RVMAT(Hobnob);

    //Officer Lowlight - RVMAT - Black Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_black_hot_RVMAT(name)

    AUX501_CUSTOM_NVG_Officer_Visor_lowlight_black_hot_RVMAT(Testing2);

    //Officer Lowlight - RVMAT - White/Black Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_white_black_hot_RVMAT(name)

    //Officer Lowlight - RVMAT - Light Green Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_light_green_hot_RVMAT(name)

    //Officer Lowlight - RVMAT - Dark Green Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_dark_green_hot_RVMAT(name)

    //Officer Lowlight - RVMAT - Green Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_green_hot_RVMAT(name)

    //Officer Lowlight - RVMAT - Light Orange Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_light_orange_hot_RVMAT(name)

    //Officer Lowlight - RVMAT - Dark Orange Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_dark_orange_hot_RVMAT(name)

    //Officer Lowlight - RVMAT - Orange Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_orange_hot_RVMAT(name)

    //Officer Lowlight - RVMAT - Yellow Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_yellow_hot_RVMAT(name)

    //Officer Lowlight - RVMAT - Brightest Color Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_lowlight_brightest_color_hot_RVMAT(name)
};