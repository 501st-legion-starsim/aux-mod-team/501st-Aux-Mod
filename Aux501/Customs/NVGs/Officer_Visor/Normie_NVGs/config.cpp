//LowLight Only
#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_white_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_white_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_black_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_white_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01_white_black_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_light_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_dark_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_light_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_dark_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_yellow_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_yellow_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_brightest_color_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

//LowLight Only - RVMAT
#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_white_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_white_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_black_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_white_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_white_black_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_light_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_dark_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_light_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_dark_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_yellow_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_yellow_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_brightest_color_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Off. - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Textures\##name##_Officer_visor_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Officer_Visor\Materials\##name##_Officer_visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Officer_Visor";\
            trooper = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_NVGs_Officer_Visor_Normie_NVGs
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_white_hot",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_black_hot",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_white_black_hot",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_light_green_hot",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_dark_green_hot",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_green_hot",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_light_orange_hot",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_dark_orange_hot",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_orange_hot",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_yellow_hot",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_brightest_color_hot"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_NVG_Drammon"
        };
    };
};

class cfgWeapons
{
    //Officer Lowlight & ARMA NVGs - White Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_white_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_white_hot(name)


    //Officer Lowlight & ARMA NVGs - Black Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_black_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_black_hot(name)

    //Officer Lowlight & ARMA NVGs - White/Black Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_white_black_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_white_black_hot(name)


    //Officer Lowlight & ARMA NVGs - Light Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_green_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_light_green_hot(name)


    //Officer Lowlight & ARMA NVGs - Dark Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_green_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_dark_green_hot(name)


    //Officer Lowlight & ARMA NVGs - Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_green_hot(name)


    //Officer Lowlight & ARMA NVGs - Light Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_light_orange_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_light_orange_hot(name)


    //Officer Lowlight & ARMA NVGs - Dark Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_dark_orange_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_dark_orange_hot(name)


    //Officer Lowlight & ARMA NVGs - Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_orange_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_orange_hot(name)


    //Officer Lowlight & ARMA NVGs - Yellow Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_yellow_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_yellow_hot(name)


    //Officer Lowlight & ARMA NVGs - Brightest Color Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot;
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_brightest_color_hot(name)

    //RVMAT//

    //Officer Lowlight & ARMA NVGs - RVMAT - White Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_white_hot_RVMAT(name)


    //Officer Lowlight & ARMA NVGs - RVMAT - Black Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_black_hot_RVMAT(name)


    //Officer Lowlight & ARMA NVGs - RVMAT - White/Black Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_white_black_hot_RVMAT(name)
    AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_white_black_hot_RVMAT(Drammon);


    //Officer Lowlight & ARMA NVGs - RVMAT - Light Green Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_light_green_hot_RVMAT(name)


    //Officer Lowlight & ARMA NVGs - RVMAT - Dark Green Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_dark_green_hot_RVMAT(name)


    //Officer Lowlight & ARMA NVGs - RVMAT - Green Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_green_hot_RVMAT(name)


    //Officer Lowlight & ARMA NVGs - RVMAT - Light Orange Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_light_orange_hot_RVMAT(name)


    //Officer Lowlight & ARMA NVGs - RVMAT - Dark Orange Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_dark_orange_hot_RVMAT(name)


    //Officer Lowlight & ARMA NVGs - RVMAT - Orange Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_orange_hot_RVMAT(name)


    //Officer Lowlight & ARMA NVGs - RVMAT - Yellow Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_yellow_hot_RVMAT(name)


    //Officer Lowlight & ARMA NVGs - RVMAT - Brightest Color Hot
    //Example: AUX501_CUSTOM_NVG_Officer_Visor_ARMA_NVGs_brightest_color_hot_RVMAT(name)
    
};