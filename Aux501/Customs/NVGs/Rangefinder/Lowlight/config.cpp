//LowLight Only
#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_white_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_white_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_light_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_dark_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_dark_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_light_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_dark_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_dark_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_yellow_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_yellow_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_brightest_color_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_brightest_color_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

//LowLight Only - RVMAT
#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_white_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_white_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_light_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_dark_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_dark_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_light_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_dark_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_dark_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_yellow_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_yellow_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_Rangefinder_lowlight_brightest_color_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_brightest_color_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG Rangefinder - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Textures\##name##_Rangefinder_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Rangefinder\Materials\##name##_Rangefinder.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder";\
            trooper = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_NVGs_Rangefinder_Lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Rangefinder"
        };
        units[] = {};
        weapons[] = {};
    };
};

class cfgWeapons
{
    //Rangefinder - Lowlight - White Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_white_hot(name)


    //Rangefinder - Lowlight - Black Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_black_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_black_hot(name)


    //Rangefinder - Lowlight - White/Black Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white_black_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_white_black_hot(name)


    //Rangefinder - Lowlight - Light Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_green_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_light_green_hot(name)


    //Rangefinder - Lowlight - Dark Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_dark_green_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_dark_green_hot(name)


    //Rangefinder - Lowlight - Green Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_green_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_green_hot(name)


    //Rangefinder - Lowlight - Light Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_orange_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_light_orange_hot(name)


    //Rangefinder - Lowlight - Dark Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_dark_orange_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_dark_orange_hot(name)


    //Rangefinder - Lowlight - Orange Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_orange_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_orange_hot(name)


    //Rangefinder - Lowlight - Yellow Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_yellow_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_yellow_hot(name)


    //Rangefinder - Lowlight - Brightest Color Hot
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_brightest_color_hot_lowlight;
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_brightest_color_hot(name)



    //RVMAT//

    //Rangefinder - Lowlight - RVMAT - White Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_white_hot_RVMAT(name)


    //Rangefinder - Lowlight - RVMAT - Black Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_black_hot_RVMAT(name)


    //Rangefinder - Lowlight - RVMAT - White/Black Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_white_black_hot_RVMAT(name)


    //Rangefinder - Lowlight - RVMAT - Light Green Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_light_green_hot_RVMAT(name)


    //Rangefinder - Lowlight - RVMAT - Dark Green Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_dark_green_hot_RVMAT(name)


    //Rangefinder - Lowlight - RVMAT - Green Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_green_hot_RVMAT(name)


    //Rangefinder - Lowlight - RVMAT - Light Orange Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_light_orange_hot_RVMAT(name)


    //Rangefinder - Lowlight - RVMAT - Dark Orange Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_dark_orange_hot_RVMAT(name)


    //Rangefinder - Lowlight - RVMAT - Orange Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_orange_hot_RVMAT(name)


    //Rangefinder - Lowlight - RVMAT - Yellow Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_yellow_hot_RVMAT(name)


    //Rangefinder - Lowlight - RVMAT - Brightest Color Hot
    //Example: AUX501_CUSTOM_NVG_Rangefinder_lowlight_brightest_color_hot_RVMAT(name)
};