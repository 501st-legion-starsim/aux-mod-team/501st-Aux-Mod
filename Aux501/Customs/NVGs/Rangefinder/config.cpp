class cfgPatches
{
    class Aux501_Patch_Customs_NVGs_Rangefinder
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Rangefinder
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "trooper" };
            class trooper
            {
                label = "NVG";
                values[] = 
                {
                    
                };
            };
        };
    };
};