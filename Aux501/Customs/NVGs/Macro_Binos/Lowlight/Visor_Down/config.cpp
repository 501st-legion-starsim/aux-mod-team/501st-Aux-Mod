//LowLight Only
#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_white_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_white_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_white_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_light_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_light_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_dark_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_dark_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_light_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_light_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_dark_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_dark_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_yellow_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_yellow_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_brightest_color_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_brightest_color_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

//LowLight Only - RVMAT
#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_white_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_white_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_white_black_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_light_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_light_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_dark_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_dark_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_green_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_light_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_light_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_dark_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_dark_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_orange_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_yellow_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_yellow_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_brightest_color_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_brightest_color_hot_lowlight\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_NVGs_MacroBinos_Visor_Down_Lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Macro_Binos"
        };
        units[] = {};
        weapons[] = {};
    };
};

class cfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_black_hot_lowlight;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_white_black_hot_lowlight;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_light_green_hot_lowlight;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_dark_green_hot_lowlight;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_green_hot_lowlight;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_light_orange_hot_lowlight;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_dark_orange_hot_lowlight;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_orange_hot_lowlight;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_yellow_hot_lowlight;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_down_brightest_color_hot_lowlight;

    //MacroBino Visor Down - Lowlight - White Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_white_hot(name)

    //MacroBino Visor Down - Lowlight - Black Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_black_hot(name)

    //MacroBino Visor Down - Lowlight - White/Black Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_white_black_hot(name)

    //MacroBino Visor Down - Lowlight - Light Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_light_green_hot(name)

    //MacroBino Visor Down - Lowlight - Dark Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_dark_green_hot(name)

    //MacroBino Visor Down - Lowlight - Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_green_hot(name)

    //MacroBino Visor Down - Lowlight - Light Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_light_orange_hot(name)

    //MacroBino Visor Down - Lowlight - Dark Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_dark_orange_hot(name)

    //MacroBino Visor Down - Lowlight - Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_orange_hot(name)

    //MacroBino Visor Down - Lowlight - Yellow Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_yellow_hot(name)

    //MacroBino Visor Down - Lowlight - Brightest Color Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_brightest_color_hot(name)


    //RVMAT//

    //MacroBino Visor Down - Lowlight - RVMAT - White Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_white_hot_RVMAT(name)

    //MacroBino Visor Down - Lowlight - RVMAT - Black Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_black_hot_RVMAT(name)

    //MacroBino Visor Down - Lowlight - RVMAT - White/Black Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_white_black_hot_RVMAT(name)

    //MacroBino Visor Down - Lowlight - RVMAT - Light Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_light_green_hot_RVMAT(name)

    //MacroBino Visor Down - Lowlight - RVMAT - Dark Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_dark_green_hot_RVMAT(name)

    //MacroBino Visor Down - Lowlight - RVMAT - Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_green_hot_RVMAT(name)

    //MacroBino Visor Down - Lowlight - RVMAT - Light Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_light_orange_hot_RVMAT(name)

    //MacroBino Visor Down - Lowlight - RVMAT - Dark Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_dark_orange_hot_RVMAT(name)

    //MacroBino Visor Down - Lowlight - RVMAT - Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_orange_hot_RVMAT(name)

    //MacroBino Visor Down - Lowlight - RVMAT - Yellow Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_yellow_hot_RVMAT(name)

    //MacroBino Visor Down - Lowlight - RVMAT - Brightest Color Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_Down_lowlight_brightest_color_hot_RVMAT(name)
};