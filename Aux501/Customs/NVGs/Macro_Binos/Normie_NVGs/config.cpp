//LowLight Only
#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_white_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_white_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_black_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_white_black_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_white_black_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_light_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_dark_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_dark_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_green_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_light_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_dark_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_dark_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_orange_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_yellow_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_yellow_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_brightest_color_hot(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_brightest_color_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

//LowLight Only - RVMAT
#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_white_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_white_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_black_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_white_black_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_white_black_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_light_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_dark_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_dark_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_green_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_green_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_light_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_dark_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_dark_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_orange_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_orange_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_yellow_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_yellow_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

#define AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_brightest_color_hot_RVMAT(name)\
    class Aux501_Customs_NVG_##name : Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_brightest_color_hot\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM NVG MacroBino - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Textures\##name##_MacroBino_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\NVGs\Macro_Binos\Materials\##name##_MacroBino.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Customs_NVG_Macrobino";\
            trooper = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_NVGs_MacroBinos_Normie_NVGs
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Macro_Binos_white_hot"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_NVG_Silver"
        };
    };
};

class cfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_white_hot;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_black_hot;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_white_black_hot;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_dark_green_hot;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_green_hot;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_orange_hot;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_dark_orange_hot;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_orange_hot;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_yellow_hot;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_brightest_color_hot;

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - White Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_white_hot(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - Black Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_black_hot(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - White/Black Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_white_black_hot(name)
    AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_white_black_hot(Silver);

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - Light Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_light_green_hot(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - Dark Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_dark_green_hot(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_green_hot(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - Light Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_light_orange_hot(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - Dark Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_dark_orange_hot(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_orange_hot(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - Yellow Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_yellow_hot(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - Brightest Color Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_brightest_color_hot(name)


    //RVMAT//

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - White Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_white_hot_RVMAT(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - Black Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_black_hot_RVMAT(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - White/Black Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_white_black_hot_RVMAT(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - Light Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_light_green_hot_RVMAT(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - Dark Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_dark_green_hot_RVMAT(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - Green Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_green_hot_RVMAT(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - Light Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_light_orange_hot_RVMAT(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - Dark Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_dark_orange_hot_RVMAT(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - Orange Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_orange_hot_RVMAT(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - Yellow Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_yellow_hot_RVMAT(name)

    //MacroBino - Visor Down - Lowlight & ARMA NVGs & ARMA NVGs - RVMAT - Brightest Color Hot
    //Example: AUX501_CUSTOM_NVG_MacroBino_ARMA_NVGs_brightest_color_hot_RVMAT(name)
};