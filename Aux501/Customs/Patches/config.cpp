class CfgPatches
{
    class Aux501_Patch_Customs_Patches
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgUnitInsignia
{	
    class Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 1";
        author = "501st Aux Team";
        texture = "\Aux501\Customs\Patches\Textures\ava1_ca.paa";
        material = "\MRC\JLTS\Core_mod\data\insignias\insignia_CloneArmor.rvmat";
    };
    class Aux501_Customs_Patches_ava1_1: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 1-1";
        texture = "\Aux501\Customs\Patches\Textures\ava1_1_ca.paa";
    };
    class Aux501_Customs_Patches_ava1_2: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 1-2";
        texture = "\Aux501\Customs\Patches\Textures\ava1_2_ca.paa";
    };
    class Aux501_Customs_Patches_ava1_3: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 1-3";
        texture = "\Aux501\Customs\Patches\Textures\ava1_3_ca.paa";
    };
    
    class Aux501_Customs_Patches_ava2: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 2";
        texture = "\Aux501\Customs\Patches\Textures\ava2_ca.paa";
    };
    class Aux501_Customs_Patches_ava2_1: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 2-1";
        texture = "\Aux501\Customs\Patches\Textures\ava2_1_ca.paa";
    };
    class Aux501_Customs_Patches_ava2_2: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 2-2";
        texture = "\Aux501\Customs\Patches\Textures\ava2_2_ca.paa";
    };
    class Aux501_Customs_Patches_ava2_3: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 2-3";
        texture = "\Aux501\Customs\Patches\Textures\ava2_3_ca.paa";
    };
    
    class Aux501_Customs_Patches_ava3: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 3";
        texture = "\Aux501\Customs\Patches\Textures\ava3_ca.paa";
    };
    class Aux501_Customs_Patches_ava3_1: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 3-1";
        texture = "\Aux501\Customs\Patches\Textures\ava3_1_ca.paa";
    };
    class Aux501_Customs_Patches_ava3_2: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 3-2";
        texture = "\Aux501\Customs\Patches\Textures\ava3_2_ca.paa";
    };
    class Aux501_Customs_Patches_ava3_3: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche 3-3";
        texture = "\Aux501\Customs\Patches\Textures\ava3_3_ca.paa";
    };
    
    class Aux501_Customs_Patches_krayt: Aux501_Customs_Patches_ava1
    {
        displayName = "Krayt";
        texture = "\Aux501\Customs\Patches\Textures\krayt_ca.paa";
    };
    class Aux501_Customs_Patches_krayt1: Aux501_Customs_Patches_ava1
    {
        displayName = "Krayt 1";
        texture = "\Aux501\Customs\Patches\Textures\krayt1_ca.paa";
    };
    class Aux501_Customs_Patches_krayt2: Aux501_Customs_Patches_ava1
    {
        displayName = "Krayt 2";
        texture = "\Aux501\Customs\Patches\Textures\krayt2_ca.paa";
    };

    class Aux501_Customs_Patches_ava_hq: Aux501_Customs_Patches_ava1
    {
        displayName = "Avalanche HQ";
        texture = "\Aux501\Customs\Patches\Textures\ava_HQ_ca.paa";
    };
    
    class Aux501_Customs_Patches_cyc1: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 1";
        texture = "\Aux501\Customs\Patches\Textures\cyc1_ca.paa";
    };
    class Aux501_Customs_Patches_cyc1_1: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 1-1";
        texture = "\Aux501\Customs\Patches\Textures\cyc1_1_ca.paa";
    };
    class Aux501_Customs_Patches_cyc1_2: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 1-2";
        texture = "\Aux501\Customs\Patches\Textures\cyc1_2_ca.paa";
    };
    class Aux501_Customs_Patches_cyc1_3: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 1-3";
        texture = "\Aux501\Customs\Patches\Textures\cyc1_3_ca.paa";
    };
    
    class Aux501_Customs_Patches_cyc2: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 2";
        texture = "\Aux501\Customs\Patches\Textures\cyc2_ca.paa";
    };
    class Aux501_Customs_Patches_cyc2_1: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 2-1";
        texture = "\Aux501\Customs\Patches\Textures\cyc2_1_ca.paa";
    };
    class Aux501_Customs_Patches_cyc2_2: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 2-2";
        texture = "\Aux501\Customs\Patches\Textures\cyc2_2_ca.paa";
    };
    class Aux501_Customs_Patches_cyc2_3: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 2-3";
        texture = "\Aux501\Customs\Patches\Textures\cyc2_3_ca.paa";
    };
    
    class Aux501_Customs_Patches_cyc3: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 3";
        texture = "\Aux501\Customs\Patches\Textures\cyc3_ca.paa";
    };
    class Aux501_Customs_Patches_cyc3_1: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 3-1";
        texture = "\Aux501\Customs\Patches\Textures\cyc3_1_ca.paa";
    };
    class Aux501_Customs_Patches_cyc3_3: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone 3-3";
        texture = "\Aux501\Customs\Patches\Textures\cyc3_3_ca.paa";
    };
    
    class Aux501_Customs_Patches_nexu: Aux501_Customs_Patches_ava1
    {
        displayName = "Nexu";
        texture = "\Aux501\Customs\Patches\Textures\nexu_ca.paa";
    };
    class Aux501_Customs_Patches_nexu1: Aux501_Customs_Patches_ava1
    {
        displayName = "Nexu 1";
        texture = "\Aux501\Customs\Patches\Textures\nexu1_ca.paa";
    };
    class Aux501_Customs_Patches_nexu2: Aux501_Customs_Patches_ava1
    {
        displayName = "Nexu 2";
        texture = "\Aux501\Customs\Patches\Textures\nexu2_ca.paa";
    };
    
    class Aux501_Customs_Patches_cyc_hq: Aux501_Customs_Patches_ava1
    {
        displayName = "Cyclone HQ";
        texture = "\Aux501\Customs\Patches\Textures\cyc_hq_ca.paa";
    };

    class Aux501_Customs_Patches_ack1: Aux501_Customs_Patches_ava1
    {
        displayName = "Acklay 1";
        texture = "\Aux501\Customs\Patches\Textures\ack1_ca.paa";
    };
    class Aux501_Customs_Patches_ack1_1: Aux501_Customs_Patches_ava1
    {
        displayName = "Acklay 1-1";
        texture = "\Aux501\Customs\Patches\Textures\ack1_1_ca.paa";
    };
    class Aux501_Customs_Patches_ack1_2: Aux501_Customs_Patches_ava1
    {
        displayName = "Acklay 1-2";
        texture = "\Aux501\Customs\Patches\Textures\ack1_2_ca.paa";
    };
    class Aux501_Customs_Patches_ack1_3: Aux501_Customs_Patches_ava1
    {
        displayName = "Acklay 1-3";
        texture = "\Aux501\Customs\Patches\Textures\ack1_3_ca.paa";
    };
    class Aux501_Customs_Patches_ack_hq: Aux501_Customs_Patches_ava1
    {
        displayName = "Acklay HQ";
        texture = "\Aux501\Customs\Patches\Textures\ack_hq_ca.paa";
    };
    class Aux501_Customs_Patches_ack_arc: Aux501_Customs_Patches_ava1
    {
        displayName = "Acklay Phoenix";
        texture = "\Aux501\Customs\Patches\Textures\ack_arc_ca.paa";
    };
    class Aux501_Customs_Patches_ack_med: Aux501_Customs_Patches_ava1
    {
        displayName = "Acklay Medic";
        texture = "\Aux501\Customs\Patches\Textures\ack_med_ca.paa";
    };
    class Aux501_Customs_Patches_ack_rto: Aux501_Customs_Patches_ava1
    {
        displayName = "Acklay RTO";
        texture = "\Aux501\Customs\Patches\Textures\ack_rto_ca.paa";
    };
    
    class Aux501_Customs_Patches_Razor: Aux501_Customs_Patches_ava1
    {
        displayName = "Razor";
        texture = "\Aux501\Customs\Patches\Textures\Razor_ca.paa";
    };
    class Aux501_Customs_Patches_Razor_Student: Aux501_Customs_Patches_ava1
    {
        displayName = "Razor Student";
        texture = "\Aux501\Customs\Patches\Textures\Razor_Student_ca.paa";
    };

    class Aux501_Customs_Patches_knife: Aux501_Customs_Patches_ava1
    {
        displayName = "Knife";
        texture = "\Aux501\Customs\Patches\Textures\knife_ca.paa";
    };

    class Aux501_Customs_Patches_Noble: Aux501_Customs_Patches_ava1
    {
        displayName = "Noble";
        texture = "\Aux501\Customs\Patches\Textures\Noble_ca.paa";
    };
    class Aux501_Customs_Patches_Noble1: Aux501_Customs_Patches_ava1
    {
        displayName = "Noble 1";
        texture = "\Aux501\Customs\Patches\Textures\Noble1_ca.paa";
    };
    class Aux501_Customs_Patches_Noble4: Aux501_Customs_Patches_ava1
    {
        displayName = "Noble 4";
        texture = "\Aux501\Customs\Patches\Textures\Noble4_ca.paa";
    };
    
    class Aux501_Customs_Patches_hydra: Aux501_Customs_Patches_ava1
    {
        displayName = "Hydra";
        texture = "\Aux501\Customs\Patches\Textures\Hydra_ca.paa";
    };
    class Aux501_Customs_Patches_hydra2: Aux501_Customs_Patches_ava1
    {
        displayName = "Hydra 2";
        texture = "\Aux501\Customs\Patches\Textures\Hydra2_ca.paa";
    };
    class Aux501_Customs_Patches_hydra4: Aux501_Customs_Patches_ava1
    {
        displayName = "Hydra 4";
        texture = "\Aux501\Customs\Patches\Textures\Hydra4_ca.paa";
    };

    class Aux501_Customs_Patches_lostplatoon_Frozen: Aux501_Customs_Patches_ava1
    {
        displayName = "Lost Platoon - Frozen";
        texture = "\Aux501\Customs\Patches\Textures\LostPlatoon_Frozen_ca.paa";
    };
    class Aux501_Customs_Patches_lostplatoon_Archangel: Aux501_Customs_Patches_ava1
    {
        displayName = "Lost Platoon - Archangel";
        texture = "\Aux501\Customs\Patches\Textures\LostPlatoon_Archangel_ca.paa";
    };
    class Aux501_Customs_Patches_lostplatoon_Phoenix: Aux501_Customs_Patches_ava1
    {
        displayName = "Lost Platoon - Phoenix";
        texture = "\Aux501\Customs\Patches\Textures\LostPlatoon_Phoenix_ca.paa";
    };
    class Aux501_Customs_Patches_lostplatoon_Redline: Aux501_Customs_Patches_ava1
    {
        displayName = "Lost Platoon - Redline";
        texture = "\Aux501\Customs\Patches\Textures\LostPlatoon_Redline_ca.paa";
    };
    class Aux501_Customs_Patches_lostplatoon_Scorched: Aux501_Customs_Patches_ava1
    {
        displayName = "Lost Platoon - Scorched";
        texture = "\Aux501\Customs\Patches\Textures\LostPlatoon_Scorched_ca.paa";
    };
    class Aux501_Customs_Patches_lostplatoon_Starfall: Aux501_Customs_Patches_ava1
    {
        displayName = "Lost Platoon - Starfall";
        texture = "\Aux501\Customs\Patches\Textures\LostPlatoon_Starfall_ca.paa";
    };
    class Aux501_Customs_Patches_lostplatoon_Templar: Aux501_Customs_Patches_ava1
    {
        displayName = "Lost Platoon - Templar";
        texture = "\Aux501\Customs\Patches\Textures\LostPlatoon_Templar_ca.paa";
    };

    class Aux501_Customs_Patches_Zeta: Aux501_Customs_Patches_ava1
    {
        displayName = "Zeta HQ";
        texture = "\Aux501\Customs\Patches\Textures\ZetaHQ_ca.paa";
    };
    class Aux501_Customs_Patches_Zeta1: Aux501_Customs_Patches_ava1
    {
        displayName = "Zeta 1";
        texture = "\Aux501\Customs\Patches\Textures\Zeta1_ca.paa";
    };
    class Aux501_Customs_Patches_Zeta2: Aux501_Customs_Patches_ava1
    {
        displayName = "Zeta 2";
        texture = "\Aux501\Customs\Patches\Textures\Zeta2_ca.paa";
    };

    class Aux501_Customs_Patches_medical: Aux501_Customs_Patches_ava1
    {
        displayName = "Medical";
        texture = "\Aux501\Customs\Patches\Textures\Medical_ca.paa";
    };

    class Aux501_Customs_Patches_Kirbo: Aux501_Customs_Patches_ava1
    {
        displayName = "Battle Kirbo";
        texture = "\Aux501\Customs\Patches\Textures\Battle_Kirbo_ca.paa";
    };
};