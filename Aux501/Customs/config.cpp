class cfgPatches
{
    class Aux501_Patch_Customs
    {
        Name = "501st Aux Mod - Customs";
        Author = "501st Aux Team";
        url = "https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};