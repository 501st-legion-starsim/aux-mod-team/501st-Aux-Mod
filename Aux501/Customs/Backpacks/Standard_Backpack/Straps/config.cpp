#define AUX501_CUSTOM_Backpack_Standard_Straps(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501_Infantry_Backpacks_Standard_straps\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF Backpack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\Standard_Backpack\Textures\##name##_Backpack_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Standard_Backpack";\
            backpack = name;\
        };\
    }

#define AUX501_CUSTOM_Backpack_Standard_Straps_RVMAT(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501_Infantry_Backpacks_Standard_straps\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF Backpack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\Standard_Backpack\Textures\##name##_Backpack_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Backpacks\Standard_Backpack\Materials\##name##_Backpack.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Standard_Backpack";\
            backpack = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Backpacks_Standard_Straps
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Standard"
        };
        units[] = 
        {
            "Aux501_Customs_Backpack_Maple"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    //Standard Backpack
    class Aux501_Units_Republic_501_Infantry_Backpacks_Standard_straps;
    //Example: AUX501_CUSTOM_Backpack_Standard_Straps(name);

    AUX501_CUSTOM_Backpack_Standard_Straps(Maple);

    //Standard Backpack - RVMAT
    //Example: AUX501_CUSTOM_Backpack_Standard_Straps_RVMAT(name);
    
};