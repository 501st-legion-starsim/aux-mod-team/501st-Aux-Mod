#define AUX501_CUSTOM_Backpack_Standard(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501_Infantry_Backpacks_Standard\
    {\
        scope = 2;\
        author = "501st Aux Team";\
        displayName = [501st] CUSTOM INF Backpack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\Standard_Backpack\Textures\##name##_Backpack_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Standard_Backpack";\
            backpack = name;\
        };\
    }

#define AUX501_CUSTOM_Backpack_Standard_RVMAT(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501_Infantry_Backpacks_Standard\
    {\
        scope = 2;\
        author = "501st Aux Team";\
        displayName = [501st] CUSTOM INF Backpack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\Standard_Backpack\Textures\##name##_Backpack_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Backpacks\Standard_Backpack\Materials\##name##_Backpack.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Standard_Backpack";\
            backpack = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Backpacks_Standard
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Standard"
        };
        units[] = 
        {
            "Aux501_Customs_Backpack_Morris",

            "Aux501_Customs_Backpack_Testing",

            "Aux501_Customs_Backpack_Worgan"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    //Standard Backpack
    class Aux501_Units_Republic_501_Infantry_Backpacks_Standard;
    //Example: AUX501_CUSTOM_Backpack_Standard(name)
    AUX501_CUSTOM_Backpack_Standard(Morris);

    AUX501_CUSTOM_Backpack_Standard(Worgan);

    //Standard Backpack - RVMAT
    //Example: AUX501_CUSTOM_Backpack_Standard_RVMAT(name)

    AUX501_CUSTOM_Backpack_Standard_RVMAT(Testing);
    
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_501st_Vest_Customs_Standard_Backpack
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "backpack" };
            class backpack
            {
                label = "Backpack";
                values[] = 
                {
                    "Maple",
                    "Morris",

                    "Testing",

                    "Worgan"
                };
                class Maple      { label = "Maple"; };
                class Morris     { label = "Morris"; };

                class Testing    { label = "Testing"; };

                class Worgan     { label = "Worgan"; };
            };
        };
    };
};