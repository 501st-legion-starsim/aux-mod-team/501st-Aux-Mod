#define AUX501_CUSTOM_Jumppack_JT12(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AB Jumppack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\Jumppacks\JT12\Textures\##name##_Jumppack_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Backpack_Customs_JT12";\
            backpack = name;\
        };\
    }

#define AUX501_CUSTOM_Jumppack_JT12_RVMAT(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AB Jumppack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\Jumppacks\JT12\Textures\##name##_Jumppack_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Backpacks\Jumppacks\JT12\Materials\##name##_Jumppack.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Backpack_Customs_JT12";\
            backpack = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Jumppacks_JT12
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Backpacks"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgVehicles
{
    //Standard Backpack
    class Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12;
    //Example: AUX501_CUSTOM_Jumppack_JT12(name);


    //Standard Backpack - RVMAT
    //Example: AUX501_CUSTOM_Jumppack_JT12_RVMAT(name);
    
    AUX501_CUSTOM_Jumppack_JT12_RVMAT(Bandit);
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_501st_Backpack_Customs_JT12
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "backpack" };
            class backpack
            {
                label = "Backpack";
                values[] = 
                {
                    "Bandit"
                };
                class Bandit    { label = "Bandit"; };
            };
        };
    };
};