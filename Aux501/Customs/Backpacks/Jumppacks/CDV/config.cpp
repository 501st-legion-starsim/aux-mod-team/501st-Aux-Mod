#define AUX501_CUSTOM_Jumppack_CDV(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_BN\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM CDV Jumppack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\Jumppacks\CDV\Textures\##name##_CDV_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Backpack_Customs_CDV";\
            backpack = name;\
        };\
    }

#define AUX501_CUSTOM_Jumppack_CDV_RVMAT(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_BN\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM CDV Jumppack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\Jumppacks\CDV\Textures\##name##_CDV_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Backpacks\Jumppacks\CDV\Materials\##name##_CDV.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Backpack_Customs_CDV";\
            backpack = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Backpacks_CDV
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501st_Infantry_Backpacks_Jumppack_CDV"
        };
        units[] = 
        {
            "Aux501_Customs_Backpack_Drammon",

            "Aux501_Customs_Backpack_Hobnob",

            "Aux501_Customs_Backpack_Kestrel"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    //CDV Jumppack
    class Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_BN;
    //Example: AUX501_CUSTOM_Jumppack_CDV(name);

    AUX501_CUSTOM_Jumppack_CDV(Hobnob);

    //CDV Jumppack - RVMAT
    //Example: AUX501_CUSTOM_Jumppack_CDV_RVMAT(name);

    AUX501_CUSTOM_Jumppack_CDV_RVMAT(Drammon);

    AUX501_CUSTOM_Jumppack_CDV_RVMAT(Kestrel);
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_501st_Backpack_Customs_CDV
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "backpack" };
            class backpack
            {
                label = "Backpack";
                values[] = 
                {
                    "Drammon",

                    "Hobnob",

                    "Kestrel"
                };
                class Drammon    { label = "Drammon"; };

                class Hobnob     { label = "Hobnob"; };

                class Kestrel    { label = "Kestrel"; };
            };
        };
    };
};