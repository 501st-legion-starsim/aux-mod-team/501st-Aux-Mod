#define AUX501_CUSTOM_Backpack_Standard_Medic_Straps(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501_Infantry_Backpacks_Medic_straps\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM MED Backpack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\Medics\Textures\##name##_Backpack_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Backpack_Medic";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_Backpack_Standard_Medic_Straps_RVMAT(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501_Infantry_Backpacks_Medic_straps\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM MED Backpack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\Medics\Textures\##name##_Backpack_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Backpacks\Medics\Materials\##name##_Backpack.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Backpack_Medic";\
            vest = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Backpacks_Standard_Medic_Straps
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Standard"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgVehicles
{
    //Standard Backpack
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic_straps;
    //Example: AUX501_CUSTOM_Backpack_Standard_Medic(name)


    //Standard Backpack - RVMAT
    //Example: AUX501_CUSTOM_Backpack_Standard_Medic(name)
    
};