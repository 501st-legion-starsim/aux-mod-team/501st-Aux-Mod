#define AUX501_CUSTOM_Backpack_Standard_RTO(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM RTO Backpack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\RTO\Textures\##name##_RTOpack_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Backpack_RTO";\
            backpack = name;\
        };\
    }

#define AUX501_CUSTOM_Backpack_Standard_RTO_RVMAT(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM RTO Backpack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\RTO\Textures\##name##_RTOpack_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Backpacks\RTO\Materials\##name##_RTOpack.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Backpack_RTO";\
            backpack = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Backpacks_RTO
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Large"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgVehicles
{
    //Standard Backpack
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large;
    //Example: AUX501_CUSTOM_Backpack_Standard_RTO(name)


    //Standard Backpack - RVMAT
    //Example: AUX501_CUSTOM_Backpack_Standard_RTO_RVMAT(name)
    
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_501st_Vest_Customs_Backpack_RTO
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "backpack" };
            class backpack
            {
                label = "Backpack";
                values[] = 
                {
                    //"NAME"
                };
                //class NAME    { label = "NAME"; };
            };
        };
    };
};