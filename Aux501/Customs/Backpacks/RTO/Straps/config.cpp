#define AUX501_CUSTOM_Backpack_Standard_RTO_Straps(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large_Straps\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM RTO Backpack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\RTO\Textures\##name##_Backpack_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Backpack_RTO";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_Backpack_Standard_RTO_Straps_RVMAT(name)\
    class Aux501_Customs_Backpack_##name : Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large_Straps\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM RTO Backpack - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Backpacks\RTO\Textures\##name##_Backpack_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Backpacks\RTO\Materials\##name##_Backpack.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Vest_Customs_Backpack_RTO";\
            vest = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Backpacks_RTO_Straps
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Large"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgVehicles
{
    //LR Backpack
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large_Straps;
    //Example: AUX501_CUSTOM_Backpack_Standard_RTO_Straps(name)


    //LR Backpack - RVMAT
    //Example: AUX501_CUSTOM_Backpack_Standard_RTO_Straps_RVMAT(name)
    
};