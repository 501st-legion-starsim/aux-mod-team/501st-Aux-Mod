#define AUX501_CUSTOM_Poncho_NoHUD(name)\
    class Aux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_nohud\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_NoHUD_NoBlue(name)\
    class Aux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue_nohud\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_NoHUD_NoRed(name)\
    class Aux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered_nohud\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_NoHUD_NoGreen(name)\
    class Aux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen_nohud\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_NoHUD_RVMAT(name)\
    class Aux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_nohud\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Facewear\Poncho\Materials\##name##_Poncho.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_NoHUD_NoBlue_RVMAT(name)\
    class Aux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue_nohud\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Facewear\Poncho\Materials\##name##_Poncho.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_NoHUD_NoRed_RVMAT(name)\
    class Aux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered_nohud\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Facewear\Poncho\Materials\##name##_Poncho.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_NoHUD_NoGreen_RVMAT(name)\
    class Aux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen_nohud\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Facewear\Poncho\Materials\##name##_Poncho.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Facewear_Poncho_nohud
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Poncho_Black"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_Facewear_Shatter"
        };
    };
};

class CfgGlasses
{
    //Poncho - NoHUD
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_nohud;
    //Example: AUX501_CUSTOM_Poncho_NoHUD(name)
    AUX501_CUSTOM_Poncho_NoHUD(Shatter);

    //Poncho - NoHUD - No Blue
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue_nohud;
    //Example: AUX501_CUSTOM_Poncho_NoHUD_NoBlue(name)

    //Poncho - NoHUD - No Red
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered_nohud;
    //Example: AUX501_CUSTOM_Poncho_NoHUD_NoRed(name)

    //Poncho - NoHUD - No Green
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen_nohud;
    //Example: AUX501_CUSTOM_Poncho_NoHUD_NoGreen(name)


    //Poncho - NoHUD - RVMAT
    //Example: AUX501_CUSTOM_Poncho_NoHUD_RVMAT(name)

    //Poncho - NoHUD - No Blue - RVMAT
    //Example: AUX501_CUSTOM_Poncho_NoHUD_NoBlue_RVMAT(name)

    //Poncho - NoHUD - No Red - RVMAT
    //Example: AUX501_CUSTOM_Poncho_NoHUD_NoRed_RVMAT(name)

    //Poncho - NoHUD - No Green - RVMAT
    //Example: AUX501_CUSTOM_Poncho_NoHUD_NoGreen_RVMAT(name)
};