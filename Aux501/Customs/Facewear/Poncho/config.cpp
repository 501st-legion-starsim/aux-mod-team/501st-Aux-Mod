#define AUX501_CUSTOM_Poncho_HUD(name)\
    class ux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_HUD_NoBlue(name)\
    class ux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_HUD_NoRed(name)\
    class ux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_HUD_NoGreen(name)\
    class ux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_HUD_RVMAT(name)\
    class ux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Facewear\Poncho\Materials\##name##_Poncho.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_HUD_NoBlue_RVMAT(name)\
    class ux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Facewear\Poncho\Materials\##name##_Poncho.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_HUD_NoRed_RVMAT(name)\
    class ux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Facewear\Poncho\Materials\##name##_Poncho.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

#define AUX501_CUSTOM_Poncho_HUD_NoGreen_RVMAT(name)\
    class Aux501_Customs_Facewear_##name : Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM FW Poncho - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Facewear\Poncho\Textures\##name##_Poncho_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Facewear\Poncho\Materials\##name##_Poncho.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_501st_Customs_Facewear";\
            poncho = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Facewear_Poncho
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Poncho_Black"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_Facewear_TEST"
        };
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen_nohud;

    //Poncho - HUD
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black;
    //Example: AUX501_CUSTOM_Poncho_HUD(name)

    //Poncho - HUD - No Blue
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removeblue;
    //Example: AUX501_CUSTOM_Poncho_HUD_NoBlue(name)

    //Poncho - HUD - No Red
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removered;
    //Example: AUX501_CUSTOM_Poncho_HUD_NoRed(name)

    //Poncho - HUD - No Green
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Poncho_Black_removegreen;
    //Example: AUX501_CUSTOM_Poncho_HUD_NoGreen(name)


    //Poncho - HUD - RVMAT
    //Example: AUX501_CUSTOM_Poncho_HUD_RVMAT(name);

    //Poncho - HUD - No Blue - RVMAT
    //Example: AUX501_CUSTOM_Poncho_HUD_NoBlue_RVMAT(name);

    AUX501_CUSTOM_Poncho_HUD_NoBlue_RVMAT(TEST);

    //Poncho - HUD - No Red - RVMAT
    //Example: AUX501_CUSTOM_Poncho_HUD_NoRed_RVMAT(name);

    //Poncho - HUD - No Green - RVMAT
    //Example: AUX501_CUSTOM_Poncho_HUD_NoGreen_RVMAT(name);
};