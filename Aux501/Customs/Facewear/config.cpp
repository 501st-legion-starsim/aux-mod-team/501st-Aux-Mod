class CfgPatches
{
    class Aux501_Patch_Customs_Facewear
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class XtdGearModels
{
    class CfgGlasses
    {
        class Aux501_ACEX_Gear_501st_Customs_Facewear
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "poncho" };
            class poncho
            {
                label = "Poncho";
                values[] = 
                {
                    "Shatter",
                    "TEST"
                };
                class Shatter { label = "Shatter"; };
                class TEST    { label = "TEST"; };
            };
        };
    };
};