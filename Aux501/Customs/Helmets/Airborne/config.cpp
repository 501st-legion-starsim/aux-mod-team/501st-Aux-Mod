#define AUX501_CUSTOM_AB(name)\
    class Aux501_Customs_501st_Airborne_Helmet_##name : Aux501_Units_Republic_501st_Airborne_Trooper_Helmet\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AB HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\Airborne\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Airborne\Textures\##name##_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_AB_Helmet_Customs";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_AB_VISOR(name)\
    class Aux501_Customs_501st_Airborne_Helmet_##name : Aux501_Units_Republic_501st_Airborne_Trooper_Helmet\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AB HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\Airborne\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Airborne\Textures\##name##_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\AB_Helmet.rvmat,\
            \Aux501\Customs\Helmets\Airborne\Materials\##name##_Visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_AB_Helmet_Customs";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_AB_RVMAT(name)\
    class Aux501_Customs_501st_Airborne_Helmet_##name : Aux501_Units_Republic_501st_Airborne_Trooper_Helmet\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AB HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\Airborne\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Airborne\Textures\##name##_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Helmets\Airborne\Materials\##name##.rvmat,\
            \Aux501\Customs\Helmets\Airborne\Materials\##name##_Visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_AB_Helmet_Customs";\
            helmet = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Helmets_Airborne
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Helmets"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_501st_AB_Helmet_Aedan",
            "Aux501_Customs_501st_AB_Helmet_Akira",
            "Aux501_Customs_501st_AB_Helmet_Anubis",

            "Aux501_Customs_501st_AB_Helmet_Bandit",
            "Aux501_Customs_501st_AB_Helmet_Blood",
            "Aux501_Customs_501st_AB_Helmet_Brogie",
            "Aux501_Customs_501st_AB_Helmet_Buzzard",

            "Aux501_Customs_501st_AB_Helmet_Calkas",

            "Aux501_Customs_501st_AB_Helmet_Exodus",

            "Aux501_Customs_501st_AB_Helmet_Giga",

            "Aux501_Customs_501st_AB_Helmet_Holliday",

            "Aux501_Customs_501st_AB_Helmet_Juggernaut",

            "Aux501_Customs_501st_AB_Helmet_Killer",

            "Aux501_Customs_501st_AB_Helmet_Marilynn",
            "Aux501_Customs_501st_AB_Helmet_Maurice",
            "Aux501_Customs_501st_AB_Helmet_Mitsu",
            "Aux501_Customs_501st_AB_Helmet_Mogar",

            "Aux501_Customs_501st_AB_Helmet_Omen",

            "Aux501_Customs_501st_AB_Helmet_Pash",
            "Aux501_Customs_501st_AB_Helmet_Power",

            "Aux501_Customs_501st_AB_Helmet_Scarab",
            "Aux501_Customs_501st_AB_Helmet_Sidetrack",
            "Aux501_Customs_501st_AB_Helmet_Soap",
            "Aux501_Customs_501st_AB_Helmet_Sparrow",
            "Aux501_Customs_501st_AB_Helmet_Spilberge",
            "Aux501_Customs_501st_AB_Helmet_Spire",
            "Aux501_Customs_501st_AB_Helmet_Sponge",
            "Aux501_Customs_501st_AB_Helmet_Stoic",
            "Aux501_Customs_501st_AB_Helmet_Stomerone",
            "Aux501_Customs_501st_AB_Helmet_Storm",

            "Aux501_Customs_501st_AB_Helmet_Tater",

            "Aux501_Customs_501st_AB_Helmet_Uni",

            "Aux501_Customs_501st_AB_Helmet_Venom",
            "Aux501_Customs_501st_AB_Helmet_Viperz",

            "Aux501_Customs_501st_AB_Helmet_Wocky"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_Airborne_Trooper_Helmet;
    
    //Normal Helmet
    AUX501_CUSTOM_AB(Aedan);

    AUX501_CUSTOM_AB(Giga);

    AUX501_CUSTOM_AB(Holliday);

    AUX501_CUSTOM_AB(Maurice);

    AUX501_CUSTOM_AB(Omen);

    AUX501_CUSTOM_AB(Pash);

    AUX501_CUSTOM_AB(Scarab);
    AUX501_CUSTOM_AB(Sidetrack);
    AUX501_CUSTOM_AB(Soap);
    AUX501_CUSTOM_AB(Sparrow);
    AUX501_CUSTOM_AB(Spilberge);
    AUX501_CUSTOM_AB(Sponge);
    AUX501_CUSTOM_AB(Stoic);
    AUX501_CUSTOM_AB(Stomerone);

    AUX501_CUSTOM_AB(Venom);

    //Glowing Visor
    AUX501_CUSTOM_AB_VISOR(Akira);
    AUX501_CUSTOM_AB_VISOR(Anubis);

    AUX501_CUSTOM_AB_VISOR(Bandit);
    AUX501_CUSTOM_AB_VISOR(Blood);
    AUX501_CUSTOM_AB_VISOR(Brogie);
    AUX501_CUSTOM_AB_VISOR(Buzzard);

    AUX501_CUSTOM_AB_VISOR(Calkas);

    AUX501_CUSTOM_AB_VISOR(Exodus);

    AUX501_CUSTOM_AB_VISOR(Juggernaut);

    AUX501_CUSTOM_AB_VISOR(Killer);

    AUX501_CUSTOM_AB_VISOR(Marilynn);
    AUX501_CUSTOM_AB_VISOR(Mitsu);
    AUX501_CUSTOM_AB_VISOR(Mogar);

    AUX501_CUSTOM_AB_VISOR(Power);
    
    AUX501_CUSTOM_AB_VISOR(Saitan);
    AUX501_CUSTOM_AB_VISOR(Spire);
    AUX501_CUSTOM_AB_VISOR(Storm);

    AUX501_CUSTOM_AB_VISOR(Tater);

    AUX501_CUSTOM_AB_VISOR(Uni);

    AUX501_CUSTOM_AB_VISOR(Viperz);

    //Special Materials
    //AUX501_CUSTOM_AB_RVMAT
    AUX501_CUSTOM_AB_RVMAT(Wocky);
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_AB_Helmet_Customs
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "helmet" };
            class helmet
            {
                label = "Helmet";
                values[] = 
                {
                    "Aedan",
                    "Akira",
                    "Anubis",

                    "Bandit",
                    "Blood",
                    "Brogie",
                    "Buzzard",

                    "Calkas",

                    "Exodus",

                    "Giga",

                    "Holliday",

                    "Juggernaut",

                    "Killer",

                    "Marilynn",
                    "Maurice",
                    "Mitsu",
                    "Mogar",

                    "Omen",

                    "Pash",
                    "Power",
                    
                    "Saitan",
                    "Scarab",
                    "Sidetrack",
                    "Soap",
                    "Spilberge",
                    "Spire",
                    "Sponge",
                    "Stoic",
                    "Stomerone",
                    "Storm",

                    "Tater",

                    "Uni",

                    "Venom",
                    "Viperz",

                    "Wocky"
                };
                class Aedan      { label = "Aedan"; };
                class Akira      { label = "Akira"; };
                class Anubis     { label = "Anubis"; };
                
                class Bandit     { label = "Bandit"; };
                class Blood      { label = "Blood"; };
                class Brogie     { label = "Brogie"; };
                class Buzzard    { label = "Buzzard"; };

                class Calkas     { label = "Calkas"; };
                
                class Exodus     { label = "Exodus"; };

                class Giga       { label = "Giga"; };
                
                class Holliday   { label = "Holliday"; };
                
                class Juggernaut { label = "Juggernaut"; };

                class Killer     { label = "Killer"; };
                
                class Marilynn   { label = "Marilynn"; };
                class Maurice    { label = "Maurice"; };
                class Mitsu      { label = "Mitsu"; };
                class Mogar      { label = "Mogar"; };
                
                class Omen       { label = "Omen"; };

                class Pash       { label = "Pash"; };
                class Power      { label = "Power"; };
                
                class Saitan     { label = "Saitan"; };
                class Scarab     { label = "Scarab"; };
                class Sidetrack  { label = "Sidetrack"; };
                class Soap       { label = "Soap"; };
                class Spilberge  { label = "Spilberge"; };
                class Spire      { label = "Spire"; };
                class Sponge     { label = "Sponge"; };
                class Stoic      { label = "Stoic"; };
                class Stomerone  { label = "Stomerone"; };
                class Storm      { label = "Storm"; };

                class Tater        { label = "Tater"; };

                class Uni        { label = "Uni"; };

                class Venom      { label = "Venom"; };
                class Viperz     { label = "Viperz"; };

                class Wocky      { label = "Wocky"; };
            };
        };
    };
};