#define AUX501_CUSTOM_RTO(name)\
    class Aux501_Customs_501st_RTO_Helmet_##name : Aux501_Units_Republic_501_RTO_Helmet\
    {\
        scope = 2;\
       displayName = [501st] CUSTOM RTO HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\RTO\Textures\##name##_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_RTO_Helmet_Customs";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_RTO_RVMAT(name)\
    class Aux501_Customs_501st_Infantry_Helmet_##name : Aux501_Units_Republic_501_Infantry_Helmet_Trooper\
    {\
        scope = 2;\
       displayName = [501st] CUSTOM RTO HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\RTO\Textures\##name##_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Helmets\RTO\Materials\##name##.rvmat,\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_RTO_Helmet_Customs";\
            helmet = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Helmets_RTO
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_RTO"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_501st_RTO_Helmet_Achilles",

            "Aux501_Customs_501st_RTO_Helmet_Dunpar",

            "Aux501_Customs_501st_RTO_Helmet_King",

            "Aux501_Customs_501st_RTO_Helmet_Mark",

            "Aux501_Customs_501st_RTO_Helmet_Nimroch",

            "Aux501_Customs_501st_RTO_Helmet_Peter",

            "Aux501_Customs_501st_RTO_Helmet_Ranger"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501_RTO_Helmet;

    //Normal Helmet
    AUX501_CUSTOM_RTO(Anarchy);

    AUX501_CUSTOM_RTO(Dunpar);

    AUX501_CUSTOM_RTO(King);

    AUX501_CUSTOM_RTO(Mark);

    AUX501_CUSTOM_RTO(Nimroch);

    AUX501_CUSTOM_RTO(Peter);

    AUX501_CUSTOM_RTO(Ranger);
    
    //Special Materials
    //AUX501_CUSTOM_RTO_RVMAT(name)
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_RTO_Helmet_Customs
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "helmet" };
            class helmet
            {
                label = "Helmet";
                values[] = 
                {
                    "Anarchy",

                    "Dunpar",

                    "King",

                    "Mark",

                    "Nimroch",

                    "Peter",

                    "Ranger"
                };

                class Anarchy    { label = "Anarchy"; };
                
                class Dunpar    { label = "Dunpar"; };
                
                class King      { label = "King"; };
                
                class Mark      { label = "Mark"; };
                
                class Nimroch   { label = "Nimroch"; };
                
                class Peter     { label = "Peter"; };

                class Ranger    { label = "Ranger"; };
            };
        };
    };
};