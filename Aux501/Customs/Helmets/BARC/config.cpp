#define AUX501_CUSTOM_BARC(name)\
    class Aux501_Customs_501st_Medic_Helmet_##name : Aux501_Units_Republic_501_Medic_Barc_CM\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM MED HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\BARC\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\BARC\Textures\##name##_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_BARC_Helmet_Customs";\
            helmet = name;\
        };\
    }
#define AUX501_CUSTOM_BARC_VISOR(name)\
    class Aux501_Customs_501st_Medic_Helmet_##name : Aux501_Units_Republic_501_Medic_Barc_CM\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM MED HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\BARC\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\BARC\Textures\##name##_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Units\Republic\501st\Infantry\Helmets\Medic\data\BARC_Helmet.rvmat,\
            \Aux501\Customs\Helmets\BARC\Materials\##name##_Visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_BARC_Helmet_Customs";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_BARC_RVMAT(name)\
    class Aux501_Customs_501st_Medic_Helmet_##name : Aux501_Units_Republic_501_Medic_Barc_CM\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM MED HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\BARC\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\BARC\Textures\##name##_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Helmets\BARC\Materials\##name##.rvmat,\
            \Aux501\Customs\Helmets\BARC\Materials\##name##_Visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_BARC_Helmet_Customs";\
            helmet = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Helmets_BARC
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Medic"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_501st_BARC_Helmet_Arrow",
            "Aux501_Customs_501st_BARC_Helmet_Ascanius",

            "Aux501_Customs_501st_BARC_Helmet_Bark",
            "Aux501_Customs_501st_BARC_Helmet_Bigfoot",

            "Aux501_Customs_501st_BARC_Helmet_Chris",
            "Aux501_Customs_501st_BARC_Helmet_Cold",

            "Aux501_Customs_501st_BARC_Helmet_Death",
            "Aux501_Customs_501st_BARC_Helmet_Desk",

            "Aux501_Customs_501st_BARC_Helmet_Ender",

            "Aux501_Customs_501st_BARC_Helmet_Jester",
            
            "Aux501_Customs_501st_BARC_Helmet_Law",

            "Aux501_Customs_501st_BARC_Helmet_Osiris",

            "Aux501_Customs_501st_BARC_Helmet_Spark",

            "Aux501_Customs_501st_BARC_Helmet_Trip"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Medic_Barc_CM;
    
    //Normal Helmet
    AUX501_CUSTOM_BARC(Arrow);

    AUX501_CUSTOM_BARC(Bark);

    AUX501_CUSTOM_BARC(Cold);

    AUX501_CUSTOM_BARC(Ender);

    AUX501_CUSTOM_BARC(Jester);

    AUX501_CUSTOM_BARC(Law);

    AUX501_CUSTOM_BARC(Spark);

    AUX501_CUSTOM_BARC(Trip);

    //Glowing Visor
    AUX501_CUSTOM_BARC_VISOR(Ascanius);

    AUX501_CUSTOM_BARC_VISOR(Bigfoot);

    AUX501_CUSTOM_BARC_VISOR(Chris);

    AUX501_CUSTOM_BARC_VISOR(Death);

    AUX501_CUSTOM_BARC_VISOR(Desk);
    
    AUX501_CUSTOM_BARC_VISOR(Osiris);

    //Special Materials
    //AUX501_CUSTOM_BARC_RVMAT(name)
};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_BARC_Helmet_Customs
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "helmet" };
            class helmet
            {
                label = "Helmet";
                values[] = 
                {
                    "Arrow",
                    "Ascanius",

                    "Bark",
                    "Bigfoot",

                    "Chris",
                    "Cold",

                    "Death",
                    
                    "Desk",

                    "Ender",

                    "Jester",

                    "Law",

                    "Osiris",

                    "Spark",

                    "Trip"
                };
                class Arrow     { label = "Arrow"; };
                class Ascanius  { label = "Ascanius"; };
                
                class Bark      { label = "Bark"; };
                class Bigfoot   { label = "Bigfoot"; };
                
                class Chris     { label = "Chris"; };
                class Cold      { label = "Cold"; };

                class Death      { label = "Death"; };
                
                class Desk      { label = "Desk"; };
                
                class Ender     { label = "Ender"; };
                
                class Jester    { label = "Jester"; };
                
                class Law       { label = "Law"; };
                
                class Osiris    { label = "Osiris"; };
                
                class Spark     { label = "Spark"; };
                
                class Trip      { label = "Trip"; };
            };
        };
    };
};