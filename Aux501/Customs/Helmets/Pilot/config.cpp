#define AUX501_CUSTOM_PILOT(name)\
    class Aux501_Customs_501st_Pilot_Helmet_##name : Aux501_Units_Republic_501st_Aviation_P2_Helmet\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AVI HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\Pilot\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Pilot\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Pilot\Textures\##name##_box_co.paa,\
            \3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Pilot_Helmet_Customs";\
            helmet = name;\
        };\
    }


#define AUX501_CUSTOM_PILOT_VISOR(name)\
    class Aux501_Customs_501st_Pilot_Helmet_##name : Aux501_Units_Republic_501st_Aviation_P2_Helmet\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AVI HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\Pilot\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Pilot\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Pilot\Textures\##name##_box_co.paa,\
            \3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            "",\
            \Aux501\Customs\Helmets\Pilot\Materials\##name##_Visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Pilot_Helmet_Customs";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_PILOT_RVMAT(name)\
    class Aux501_Customs_501st_Pilot_Helmet_##name : Aux501_Units_Republic_501st_Aviation_P2_Helmet\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM AVI HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\Pilot\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Pilot\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Pilot\Textures\##name##_box_co.paa,\
            \3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Helmets\Pilot\Materials\##name##.rvmat,\
            \Aux501\Customs\Helmets\Pilot\Materials\##name##_Visor.rvmat,\
            \Aux501\Customs\Helmets\Pilot\Materials\##name##_box.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_Pilot_Helmet_Customs";\
            helmet = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Helmets_Pilot
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Razor_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_501st_Pilot_Helmet_Amp",
            "Aux501_Customs_501st_Pilot_Helmet_Argo",
            "Aux501_Customs_501st_Pilot_Helmet_Atom",

            "Aux501_Customs_501st_Pilot_Helmet_Bacon",
            "Aux501_Customs_501st_Pilot_Helmet_Bastil",
            "Aux501_Customs_501st_Pilot_Helmet_Bonewheel",

            "Aux501_Customs_501st_Pilot_Helmet_Casskun",
            "Aux501_Customs_501st_Pilot_Helmet_Conqueror",

            "Aux501_Customs_501st_Pilot_Helmet_Dagger",
            "Aux501_Customs_501st_Pilot_Helmet_Diablo",
            "Aux501_Customs_501st_Pilot_Helmet_Dylan",

            "Aux501_Customs_501st_Pilot_Helmet_Ethan",
            "Aux501_Customs_501st_Pilot_Helmet_Exile",

            "Aux501_Customs_501st_Pilot_Helmet_Jaisus",
            "Aux501_Customs_501st_Pilot_Helmet_Jewel",

            "Aux501_Customs_501st_Pilot_Helmet_Lutom",

            "Aux501_Customs_501st_Pilot_Helmet_Mulkot",

            "Aux501_Customs_501st_Pilot_Helmet_Neb",

            "Aux501_Customs_501st_Pilot_Helmet_Orange",

            "Aux501_Customs_501st_Pilot_Helmet_Phantom",

            "Aux501_Customs_501st_Pilot_Helmet_Shock",
            "Aux501_Customs_501st_Pilot_Helmet_Sig",

            "Aux501_Customs_501st_Pilot_Helmet_Xavier",

            "Aux501_Customs_501st_Pilot_Helmet_Zulu"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_Aviation_P2_Helmet;
    
    //Normal Helmet
    AUX501_CUSTOM_PILOT(Amp);
    AUX501_CUSTOM_PILOT(Argo);
    AUX501_CUSTOM_PILOT(Atom);

    AUX501_CUSTOM_PILOT(Bastil);

    AUX501_CUSTOM_PILOT(Casskun);
    AUX501_CUSTOM_PILOT(Conqueror);

    AUX501_CUSTOM_PILOT(Dagger);
    AUX501_CUSTOM_PILOT(Dylan);

    AUX501_CUSTOM_PILOT(Jaisus);

    AUX501_CUSTOM_PILOT(Lutom);

    AUX501_CUSTOM_PILOT(Mulkot);

    AUX501_CUSTOM_PILOT(Neb);

    AUX501_CUSTOM_PILOT(Phantom);
    
    AUX501_CUSTOM_PILOT(Shock);
    AUX501_CUSTOM_PILOT(Sig);

    AUX501_CUSTOM_PILOT(Xavier);

    //Glowing Visor
    AUX501_CUSTOM_PILOT_VISOR(Bacon);

    AUX501_CUSTOM_PILOT_VISOR(Bonewheel);
    
    AUX501_CUSTOM_PILOT_VISOR(Diablo);
    
    AUX501_CUSTOM_PILOT_VISOR(Ethan);
    AUX501_CUSTOM_PILOT_VISOR(Exile);

    AUX501_CUSTOM_PILOT_VISOR(Jewel);

    AUX501_CUSTOM_PILOT_VISOR(Orange);

    AUX501_CUSTOM_PILOT_VISOR(Zulu);

    //Special Materials
    //AUX501_CUSTOM_PILOT_RVMAT(name)
};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_Pilot_Helmet_Customs
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "helmet" };
            class helmet
            {
                label = "Helmet";
                values[] = 
                {
                    "Amp",
                    "Argo",
                    "Atom",

                    "Bacon",
                    "Bastil",
                    "Bonewheel",

                    "Casskun",
                    "Conqueror",

                    "Dagger",
                    "Diablo",
                    "Dylan",

                    "Ethan",
                    "Exile",

                    "Jaisus",
                    "Jewel",

                    "Lutom",

                    "Mulkot",

                    "Neb",

                    "Orange",

                    "Phantom",

                    "Shock",
                    "Sig",

                    "Xavier",

                    "Zulu"
                };
                class Amp       { label = "Amp"; };
                class Argo      { label = "Argo"; };
                class Atom      { label = "Atom"; };
                
                class Bacon     { label = "Bacon"; };
                class Bastil    { label = "Bastil"; };
                class Bonewheel    { label = "Bonewheel"; };

                class Casskun   { label = "Casskun"; };
                class Conqueror { label = "Conqueror"; };

                class Dagger    { label = "Dagger"; };
                class Diablo    { label = "Diablo"; };
                class Dylan     { label = "Dylan"; };
                
                class Ethan     { label = "Ethan"; };
                class Exile     { label = "Exile"; };

                class Jaisus    { label = "Jaisus"; };
                class Jewel     { label = "Jewel"; };

                class Lutom     { label = "Lutom"; };

                class Mulkot    { label = "Mulkot"; };

                class Neb       { label = "Neb"; };

                class Orange    { label = "Orange"; };

                class Phantom   { label = "Phantom"; };

                class Shock     { label = "Shock"; };
                class Sig       { label = "Sig"; };

                class Xavier    { label = "Xavier"; };

                class Zulu      { label = "Zulu"; };
            };
        };
    };
};