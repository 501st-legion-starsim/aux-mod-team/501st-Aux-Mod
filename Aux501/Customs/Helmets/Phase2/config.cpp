#define AUX501_CUSTOM_P2(name)\
    class Aux501_Customs_501st_Infantry_Helmet_##name : Aux501_Units_Republic_501_Infantry_Helmet_Trooper\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\Phase2\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Phase2\Textures\##name##_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_GI_Helmet_Customs";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_P2_VISOR(name)\
    class Aux501_Customs_501st_Infantry_Helmet_##name : Aux501_Units_Republic_501_Infantry_Helmet_Trooper\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\Phase2\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Phase2\Textures\##name##_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\Phase2_Helmet.rvmat,\
            \Aux501\Customs\Helmets\Phase2\Materials\##name##_Visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_GI_Helmet_Customs";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_P2_RVMAT(name)\
    class Aux501_Customs_501st_Infantry_Helmet_##name : Aux501_Units_Republic_501_Infantry_Helmet_Trooper\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM INF HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\Phase2\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\Phase2\Textures\##name##_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Helmets\Phase2\Materials\##name##.rvmat,\
            \Aux501\Customs\Helmets\Phase2\Materials\##name##_Visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_GI_Helmet_Customs";\
            helmet = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Helmets_Phase2
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_501st_Infantry_Helmet_Ace",
            "Aux501_Customs_501st_Infantry_Helmet_Achilles",
            "Aux501_Customs_501st_Infantry_Helmet_Ahmanni",
            "Aux501_Customs_501st_Infantry_Helmet_AJ",
            "Aux501_Customs_501st_Infantry_Helmet_Alex",
            "Aux501_Customs_501st_Infantry_Helmet_Alistair",
            "Aux501_Customs_501st_Infantry_Helmet_Andromeda",
            "Aux501_Customs_501st_Infantry_Helmet_Anheiser",
            "Aux501_Customs_501st_Infantry_Helmet_Anselm",
            "Aux501_Customs_501st_Infantry_Helmet_Anta",
            "Aux501_Customs_501st_Infantry_Helmet_Asher",

            "Aux501_Customs_501st_Infantry_Helmet_Bambo",
            "Aux501_Customs_501st_Infantry_Helmet_Bevel",
            "Aux501_Customs_501st_Infantry_Helmet_Biscuit",
            "Aux501_Customs_501st_Infantry_Helmet_Bishop",
            "Aux501_Customs_501st_Infantry_Helmet_Bjorn",
            "Aux501_Customs_501st_Infantry_Helmet_Black",
            "Aux501_Customs_501st_Infantry_Helmet_Blood",
            "Aux501_Customs_501st_Infantry_Helmet_Boneish",
            "Aux501_Customs_501st_Infantry_Helmet_Bones",
            "Aux501_Customs_501st_Infantry_Helmet_Boozy",
            "Aux501_Customs_501st_Infantry_Helmet_Box",
            "Aux501_Customs_501st_Infantry_Helmet_Bradley",
            "Aux501_Customs_501st_Infantry_Helmet_Breach",
            "Aux501_Customs_501st_Infantry_Helmet_Bridger",
            "Aux501_Customs_501st_Infantry_Helmet_Broad",
            "Aux501_Customs_501st_Infantry_Helmet_Bruce",
            "Aux501_Customs_501st_Infantry_Helmet_Buddy",
            "Aux501_Customs_501st_Infantry_Helmet_Buffalo",
            "Aux501_Customs_501st_Infantry_Helmet_Buggs",
            "Aux501_Customs_501st_Infantry_Helmet_Burrito",

            "Aux501_Customs_501st_Infantry_Helmet_Cactus",
            "Aux501_Customs_501st_Infantry_Helmet_Casual",
            "Aux501_Customs_501st_Infantry_Helmet_Chan",
            "Aux501_Customs_501st_Infantry_Helmet_Charl",
            "Aux501_Customs_501st_Infantry_Helmet_Chaser",
            "Aux501_Customs_501st_Infantry_Helmet_Checkers",
            "Aux501_Customs_501st_Infantry_Helmet_Cherokee",
            "Aux501_Customs_501st_Infantry_Helmet_Chosen1",
            "Aux501_Customs_501st_Infantry_Helmet_Chimera",
            "Aux501_Customs_501st_Infantry_Helmet_Cito",
            "Aux501_Customs_501st_Infantry_Helmet_Clock",
            "Aux501_Customs_501st_Infantry_Helmet_Clover",
            "Aux501_Customs_501st_Infantry_Helmet_Compo",
            "Aux501_Customs_501st_Infantry_Helmet_Concerned",
            "Aux501_Customs_501st_Infantry_Helmet_Craz",
            "Aux501_Customs_501st_Infantry_Helmet_Crebar",
            "Aux501_Customs_501st_Infantry_Helmet_Crinkcase",
            "Aux501_Customs_501st_Infantry_Helmet_Crisis",
            "Aux501_Customs_501st_Infantry_Helmet_Cruisie",
            "Aux501_Customs_501st_Infantry_Helmet_Crush",
            "Aux501_Customs_501st_Infantry_Helmet_Cupid",
            "Aux501_Customs_501st_Infantry_Helmet_Cursed",
            
            "Aux501_Customs_501st_Infantry_Helmet_Daan",
            "Aux501_Customs_501st_Infantry_Helmet_Dadecoy",
            "Aux501_Customs_501st_Infantry_Helmet_Dakota",
            "Aux501_Customs_501st_Infantry_Helmet_Dasand",
            "Aux501_Customs_501st_Infantry_Helmet_Defender",
            "Aux501_Customs_501st_Infantry_Helmet_Demo",
            "Aux501_Customs_501st_Infantry_Helmet_Dimitri",
            "Aux501_Customs_501st_Infantry_Helmet_Diverge",
            "Aux501_Customs_501st_Infantry_Helmet_Dobby",
            "Aux501_Customs_501st_Infantry_Helmet_Donut",
            "Aux501_Customs_501st_Infantry_Helmet_Drammon",
            "Aux501_Customs_501st_Infantry_Helmet_Dredge",
            "Aux501_Customs_501st_Infantry_Helmet_Drifter",
            "Aux501_Customs_501st_Infantry_Helmet_Dusty",

            "Aux501_Customs_501st_Infantry_Helmet_Effort",
            "Aux501_Customs_501st_Infantry_Helmet_Eleck",

            "Aux501_Customs_501st_Infantry_Helmet_Fenrir",
            "Aux501_Customs_501st_Infantry_Helmet_Fixit",
            "Aux501_Customs_501st_Infantry_Helmet_Fluorite",
            "Aux501_Customs_501st_Infantry_Helmet_Fly",
            "Aux501_Customs_501st_Infantry_Helmet_Ford",

            "Aux501_Customs_501st_Infantry_Helmet_Gallagher",
            "Aux501_Customs_501st_Infantry_Helmet_Garviel",
            "Aux501_Customs_501st_Infantry_Helmet_Gary",
            "Aux501_Customs_501st_Infantry_Helmet_Ghost",
            "Aux501_Customs_501st_Infantry_Helmet_Goddest",
            "Aux501_Customs_501st_Infantry_Helmet_Goldarp",
            "Aux501_Customs_501st_Infantry_Helmet_Great",
            "Aux501_Customs_501st_Infantry_Helmet_Greene",
            "Aux501_Customs_501st_Infantry_Helmet_Grey",
            "Aux501_Customs_501st_Infantry_Helmet_Guardian",

            "Aux501_Customs_501st_Infantry_Helmet_Heat",
            "Aux501_Customs_501st_Infantry_Helmet_Hobnob",
            "Aux501_Customs_501st_Infantry_Helmet_Hoodoo",
            "Aux501_Customs_501st_Infantry_Helmet_Hosed",
            "Aux501_Customs_501st_Infantry_Helmet_Hungarian",

            "Aux501_Customs_501st_Infantry_Helmet_Ichigo",
            "Aux501_Customs_501st_Infantry_Helmet_Iron",

            "Aux501_Customs_501st_Infantry_Helmet_Jamenson",
            "Aux501_Customs_501st_Infantry_Helmet_Jatt",
            "Aux501_Customs_501st_Infantry_Helmet_Jay",
            "Aux501_Customs_501st_Infantry_Helmet_Jimmy",
            "Aux501_Customs_501st_Infantry_Helmet_Jub",
            "Aux501_Customs_501st_Infantry_Helmet_Juggernaut",
            "Aux501_Customs_501st_Infantry_Helmet_June",

            "Aux501_Customs_501st_Infantry_Helmet_Kahn",
            "Aux501_Customs_501st_Infantry_Helmet_Kenny",
            "Aux501_Customs_501st_Infantry_Helmet_Kestrel",
            "Aux501_Customs_501st_Infantry_Helmet_Kitti",
            "Aux501_Customs_501st_Infantry_Helmet_Klinger",
            "Aux501_Customs_501st_Infantry_Helmet_Knight",
            "Aux501_Customs_501st_Infantry_Helmet_Koda",
            "Aux501_Customs_501st_Infantry_Helmet_Krios",
            "Aux501_Customs_501st_Infantry_Helmet_Kyrie",

            "Aux501_Customs_501st_Infantry_Helmet_Law",
            "Aux501_Customs_501st_Infantry_Helmet_Legia",
            "Aux501_Customs_501st_Infantry_Helmet_Len",
            "Aux501_Customs_501st_Infantry_Helmet_Leon",
            "Aux501_Customs_501st_Infantry_Helmet_Liberty",
            "Aux501_Customs_501st_Infantry_Helmet_Loki",

            "Aux501_Customs_501st_Infantry_Helmet_Mada",
            "Aux501_Customs_501st_Infantry_Helmet_Maelstrom",
            "Aux501_Customs_501st_Infantry_Helmet_Maple",
            "Aux501_Customs_501st_Infantry_Helmet_Marshmellow",
            "Aux501_Customs_501st_Infantry_Helmet_Maverick",
            "Aux501_Customs_501st_Infantry_Helmet_Mayhal",
            "Aux501_Customs_501st_Infantry_Helmet_Midnight",
            "Aux501_Customs_501st_Infantry_Helmet_Mills",
            "Aux501_Customs_501st_Infantry_Helmet_Mikado",
            "Aux501_Customs_501st_Infantry_Helmet_Mirror",
            "Aux501_Customs_501st_Infantry_Helmet_Money",
            "Aux501_Customs_501st_Infantry_Helmet_Morris",
            "Aux501_Customs_501st_Infantry_Helmet_Mungus",
            "Aux501_Customs_501st_Infantry_Helmet_Mustang",

            "Aux501_Customs_501st_Infantry_Helmet_Napkin",
            "Aux501_Customs_501st_Infantry_Helmet_Narrator",
            "Aux501_Customs_501st_Infantry_Helmet_Neta",
            "Aux501_Customs_501st_Infantry_Helmet_Nico",
            "Aux501_Customs_501st_Infantry_Helmet_Noodle",
            
            "Aux501_Customs_501st_Infantry_Helmet_Odin",
            "Aux501_Customs_501st_Infantry_Helmet_Oktapius",

            "Aux501_Customs_501st_Infantry_Helmet_Patriot",
            "Aux501_Customs_501st_Infantry_Helmet_Peterson",
            "Aux501_Customs_501st_Infantry_Helmet_Pollyon",
            "Aux501_Customs_501st_Infantry_Helmet_Popeye",
            "Aux501_Customs_501st_Infantry_Helmet_Prime",

            "Aux501_Customs_501st_Infantry_Helmet_Rage",
            "Aux501_Customs_501st_Infantry_Helmet_Raktharg",
            "Aux501_Customs_501st_Infantry_Helmet_Raloris",
            "Aux501_Customs_501st_Infantry_Helmet_Ranque",
            "Aux501_Customs_501st_Infantry_Helmet_Rapteh",
            "Aux501_Customs_501st_Infantry_Helmet_Rebellion",
            "Aux501_Customs_501st_Infantry_Helmet_Regicide",
            "Aux501_Customs_501st_Infantry_Helmet_Revenant",
            "Aux501_Customs_501st_Infantry_Helmet_Rias",
            "Aux501_Customs_501st_Infantry_Helmet_Ripjaw",
            "Aux501_Customs_501st_Infantry_Helmet_Roe",
            "Aux501_Customs_501st_Infantry_Helmet_Roster",
            "Aux501_Customs_501st_Infantry_Helmet_Rupert",
            "Aux501_Customs_501st_Infantry_Helmet_Rythian",

            "Aux501_Customs_501st_Infantry_Helmet_Sazi",
            "Aux501_Customs_501st_Infantry_Helmet_Scanlon",
            "Aux501_Customs_501st_Infantry_Helmet_Schames",
            "Aux501_Customs_501st_Infantry_Helmet_September",
            "Aux501_Customs_501st_Infantry_Helmet_Shadow",
            "Aux501_Customs_501st_Infantry_Helmet_Shape",
            "Aux501_Customs_501st_Infantry_Helmet_Shark",
            "Aux501_Customs_501st_Infantry_Helmet_Shatter",
            "Aux501_Customs_501st_Infantry_Helmet_Shredded",
            "Aux501_Customs_501st_Infantry_Helmet_Sig",
            "Aux501_Customs_501st_Infantry_Helmet_Silver",
            "Aux501_Customs_501st_Infantry_Helmet_Simon",
            "Aux501_Customs_501st_Infantry_Helmet_Singed",
            "Aux501_Customs_501st_Infantry_Helmet_Ski",
            "Aux501_Customs_501st_Infantry_Helmet_Sledge",
            "Aux501_Customs_501st_Infantry_Helmet_Slyder",
            "Aux501_Customs_501st_Infantry_Helmet_Smiley",
            "Aux501_Customs_501st_Infantry_Helmet_Snoopy",
            "Aux501_Customs_501st_Infantry_Helmet_Solus",
            "Aux501_Customs_501st_Infantry_Helmet_Sosa",
            "Aux501_Customs_501st_Infantry_Helmet_Sour",
            "Aux501_Customs_501st_Infantry_Helmet_Source",
            "Aux501_Customs_501st_Infantry_Helmet_Soyvolon",
            "Aux501_Customs_501st_Infantry_Helmet_Sparticus",
            "Aux501_Customs_501st_Infantry_Helmet_Spud",
            "Aux501_Customs_501st_Infantry_Helmet_Squeaks",
            "Aux501_Customs_501st_Infantry_Helmet_Stitches",
            "Aux501_Customs_501st_Infantry_Helmet_Stoanes",
            "Aux501_Customs_501st_Infantry_Helmet_Storm",
            "Aux501_Customs_501st_Infantry_Helmet_Sunshine",
            "Aux501_Customs_501st_Infantry_Helmet_Super",
            "Aux501_Customs_501st_Infantry_Helmet_Swanny",
            "Aux501_Customs_501st_Infantry_Helmet_Surtr",
            
            "Aux501_Customs_501st_Infantry_Helmet_Taka",
            "Aux501_Customs_501st_Infantry_Helmet_Taken",
            "Aux501_Customs_501st_Infantry_Helmet_Talker",
            "Aux501_Customs_501st_Infantry_Helmet_Target",
            "Aux501_Customs_501st_Infantry_Helmet_Tatum",
            "Aux501_Customs_501st_Infantry_Helmet_Tee",
            "Aux501_Customs_501st_Infantry_Helmet_Tideend",
            "Aux501_Customs_501st_Infantry_Helmet_Tik",
            "Aux501_Customs_501st_Infantry_Helmet_Tim",
            "Aux501_Customs_501st_Infantry_Helmet_Train",
            "Aux501_Customs_501st_Infantry_Helmet_Triage",
            "Aux501_Customs_501st_Infantry_Helmet_Tuch",
            "Aux501_Customs_501st_Infantry_Helmet_Tupiks",
            "Aux501_Customs_501st_Infantry_Helmet_Twine",
            "Aux501_Customs_501st_Infantry_Helmet_Twisted",
            "Aux501_Customs_501st_Infantry_Helmet_Typhon",

            "Aux501_Customs_501st_Infantry_Helmet_Unity",

            "Aux501_Customs_501st_Infantry_Helmet_Vengeance",
            "Aux501_Customs_501st_Infantry_Helmet_Versa",
            "Aux501_Customs_501st_Infantry_Helmet_Volley",
            "Aux501_Customs_501st_Infantry_Helmet_Vulpes",

            "Aux501_Customs_501st_Infantry_Helmet_Wag",
            "Aux501_Customs_501st_Infantry_Helmet_Walsh",
            "Aux501_Customs_501st_Infantry_Helmet_Wasp",
            "Aux501_Customs_501st_Infantry_Helmet_Waylander",
            "Aux501_Customs_501st_Infantry_Helmet_Werdplay",
            "Aux501_Customs_501st_Infantry_Helmet_Winter",
            "Aux501_Customs_501st_Infantry_Helmet_Wirtimus",
            "Aux501_Customs_501st_Infantry_Helmet_Wizard",
            "Aux501_Customs_501st_Infantry_Helmet_Worgan",

            "Aux501_Customs_501st_Infantry_Helmet_Yokai",
            "Aux501_Customs_501st_Infantry_Helmet_Young",

            "Aux501_Customs_501st_Infantry_Helmet_Zeros"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Helmet_Trooper;

    AUX501_CUSTOM_P2(Achilles);
    AUX501_CUSTOM_P2(Ahmanni);
    AUX501_CUSTOM_P2(AJ);
    AUX501_CUSTOM_P2(Alex);
    AUX501_CUSTOM_P2(Alistair);
    AUX501_CUSTOM_P2(Anheiser);
    AUX501_CUSTOM_P2(Asher);

    AUX501_CUSTOM_P2(Bambo);
    AUX501_CUSTOM_P2(Base);
    AUX501_CUSTOM_P2(Bevel);
    AUX501_CUSTOM_P2(Biscuit);
    AUX501_CUSTOM_P2(Bishop);
    AUX501_CUSTOM_P2(Bjorn);
    AUX501_CUSTOM_P2(Black);
    AUX501_CUSTOM_P2(Boneish);
    AUX501_CUSTOM_P2(Boozy);
    AUX501_CUSTOM_P2(Box);
    AUX501_CUSTOM_P2(Bradley);
    AUX501_CUSTOM_P2(Breach);
    AUX501_CUSTOM_P2(Broad);
    AUX501_CUSTOM_P2(Bruce);
    AUX501_CUSTOM_P2(Buddy);
    AUX501_CUSTOM_P2(Buggs);
    AUX501_CUSTOM_P2(Burrito);

    AUX501_CUSTOM_P2(Cactus);
    AUX501_CUSTOM_P2(Casual);
    AUX501_CUSTOM_P2(Chan);
    AUX501_CUSTOM_P2(Charl);
    AUX501_CUSTOM_P2(Cherokee);
    AUX501_CUSTOM_P2(Chosen1);
    AUX501_CUSTOM_P2(Clock);
    AUX501_CUSTOM_P2(Clover);
    AUX501_CUSTOM_P2(Compo);
    AUX501_CUSTOM_P2(Craz);
    AUX501_CUSTOM_P2(Crebar);
    AUX501_CUSTOM_P2(Crisis);
    AUX501_CUSTOM_P2(Cruisie);
    AUX501_CUSTOM_P2(Crush);
    AUX501_CUSTOM_P2(Cupid);
    AUX501_CUSTOM_P2(Cursed);

    AUX501_CUSTOM_P2(Dadecoy);
    AUX501_CUSTOM_P2(Dasand);
    AUX501_CUSTOM_P2(Defender);
    AUX501_CUSTOM_P2(Demo);
    AUX501_CUSTOM_P2(Dimitri);
    AUX501_CUSTOM_P2(Diverge);
    AUX501_CUSTOM_P2(Dobby);
    AUX501_CUSTOM_P2(Donut);
    AUX501_CUSTOM_P2(Dredge);
    AUX501_CUSTOM_P2(Drifter);

    AUX501_CUSTOM_P2(Effort);

    AUX501_CUSTOM_P2(Fenrir);
    AUX501_CUSTOM_P2(Fixit);
    AUX501_CUSTOM_P2(Fluorite);
    AUX501_CUSTOM_P2(Fly);
    AUX501_CUSTOM_P2(Ford);

    AUX501_CUSTOM_P2(Gallagher);
    AUX501_CUSTOM_P2(Garviel);
    AUX501_CUSTOM_P2(Ghost);
    AUX501_CUSTOM_P2(Goddest);
    AUX501_CUSTOM_P2(Goldarp);
    AUX501_CUSTOM_P2(Great);
    AUX501_CUSTOM_P2(Greene);
    AUX501_CUSTOM_P2(Guardian);

    AUX501_CUSTOM_P2(Heat);

    AUX501_CUSTOM_P2(Ichigo);
    AUX501_CUSTOM_P2(Iron);

    AUX501_CUSTOM_P2(Jamenson);
    AUX501_CUSTOM_P2(Jay);
    AUX501_CUSTOM_P2(Jimmy);
    AUX501_CUSTOM_P2(June);

    AUX501_CUSTOM_P2(Kahn);
    AUX501_CUSTOM_P2(Kenny);
    AUX501_CUSTOM_P2(Kitti);
    AUX501_CUSTOM_P2(Knight);
    AUX501_CUSTOM_P2(Koda);
    AUX501_CUSTOM_P2(Krios);
    AUX501_CUSTOM_P2(Kyrie);

    AUX501_CUSTOM_P2(Law);
    AUX501_CUSTOM_P2(Legia);
    AUX501_CUSTOM_P2(Len);
    AUX501_CUSTOM_P2(Leon);
    AUX501_CUSTOM_P2(Liberty);
    AUX501_CUSTOM_P2(Loki);

    AUX501_CUSTOM_P2(Mada);
    AUX501_CUSTOM_P2(Maelstrom);
    AUX501_CUSTOM_P2(Marshmellow);
    AUX501_CUSTOM_P2(Maverick);
    AUX501_CUSTOM_P2(Mayhal);
    AUX501_CUSTOM_P2(Midnight);
    AUX501_CUSTOM_P2(Mikado);
    AUX501_CUSTOM_P2(Mills);
    AUX501_CUSTOM_P2(Mirror);
    AUX501_CUSTOM_P2(Money);
    AUX501_CUSTOM_P2(Morris);
    AUX501_CUSTOM_P2(Mungus);
    AUX501_CUSTOM_P2(Mustang);

    AUX501_CUSTOM_P2(Neta);

    AUX501_CUSTOM_P2(Odin);
    AUX501_CUSTOM_P2(Oktapius);

    AUX501_CUSTOM_P2(Peterson);
    AUX501_CUSTOM_P2(Pollyon);
    AUX501_CUSTOM_P2(Popeye);
    AUX501_CUSTOM_P2(Prime);

    AUX501_CUSTOM_P2(Rage);
    AUX501_CUSTOM_P2(Raktharg);
    AUX501_CUSTOM_P2(Raloris);
    AUX501_CUSTOM_P2(Ranque);
    AUX501_CUSTOM_P2(Rapteh);
    AUX501_CUSTOM_P2(Raptor);
    AUX501_CUSTOM_P2(Rebellion);
    AUX501_CUSTOM_P2(Revenant);
    AUX501_CUSTOM_P2(Ripjaw);
    AUX501_CUSTOM_P2(Roe);
    AUX501_CUSTOM_P2(Roster);
    AUX501_CUSTOM_P2(Rupert);

    AUX501_CUSTOM_P2(Sazi);
    AUX501_CUSTOM_P2(Scanlon);
    AUX501_CUSTOM_P2(Schames);
    AUX501_CUSTOM_P2(Shadow);
    AUX501_CUSTOM_P2(Shape);
    AUX501_CUSTOM_P2(Shark);
    AUX501_CUSTOM_P2(Shredded);
    AUX501_CUSTOM_P2(Sig);
    AUX501_CUSTOM_P2(Silver);
    AUX501_CUSTOM_P2(Simon);
    AUX501_CUSTOM_P2(Singed);
    AUX501_CUSTOM_P2(Ski);
    AUX501_CUSTOM_P2(Sledge);
    AUX501_CUSTOM_P2(Slyder);
    AUX501_CUSTOM_P2(Snoopy);
    AUX501_CUSTOM_P2(Solus);
    AUX501_CUSTOM_P2(Sosa);
    AUX501_CUSTOM_P2(Sour);
    AUX501_CUSTOM_P2(Soyvolon);
    AUX501_CUSTOM_P2(Sparticus);
    AUX501_CUSTOM_P2(Spud);
    AUX501_CUSTOM_P2(Squeaks);
    AUX501_CUSTOM_P2(Stoanes);
    AUX501_CUSTOM_P2(Storm);
    AUX501_CUSTOM_P2(Sunshine);
    AUX501_CUSTOM_P2(Super);
    AUX501_CUSTOM_P2(Surtr);

    AUX501_CUSTOM_P2(Taka);
    AUX501_CUSTOM_P2(Taken);
    AUX501_CUSTOM_P2(Target);
    AUX501_CUSTOM_P2(Tatum);
    AUX501_CUSTOM_P2(Tee);
    AUX501_CUSTOM_P2(Tideend);
    AUX501_CUSTOM_P2(Tik);
    AUX501_CUSTOM_P2(Tim);
    AUX501_CUSTOM_P2(Train);
    AUX501_CUSTOM_P2(Tuch);
    AUX501_CUSTOM_P2(Tupiks);
    AUX501_CUSTOM_P2(Twine);
    AUX501_CUSTOM_P2(Twisted);
    AUX501_CUSTOM_P2(Typhon);

    AUX501_CUSTOM_P2(Unity);

    AUX501_CUSTOM_P2(Vengeance);
    AUX501_CUSTOM_P2(Versa);
    AUX501_CUSTOM_P2(Volley);

    AUX501_CUSTOM_P2(Wag);
    AUX501_CUSTOM_P2(Walsh);
    AUX501_CUSTOM_P2(Wasp);
    AUX501_CUSTOM_P2(Waylander);
    AUX501_CUSTOM_P2(Winter);
    AUX501_CUSTOM_P2(Wizard);

    AUX501_CUSTOM_P2(Yokai);
    AUX501_CUSTOM_P2(Young);

    AUX501_CUSTOM_P2(Zeros);
    
    //Glowing
    AUX501_CUSTOM_P2_VISOR(Ace);
    AUX501_CUSTOM_P2_VISOR(Anselm);
    AUX501_CUSTOM_P2_VISOR(Anta);

    AUX501_CUSTOM_P2_VISOR(Blood);
    AUX501_CUSTOM_P2_VISOR(Bones);
    AUX501_CUSTOM_P2_VISOR(Buffalo);

    AUX501_CUSTOM_P2_VISOR(Chaser);
    AUX501_CUSTOM_P2_VISOR(Checkers);
    AUX501_CUSTOM_P2_VISOR(Chimera);
    AUX501_CUSTOM_P2_VISOR(Concerned);
    AUX501_CUSTOM_P2_VISOR(Crinkcase);

    AUX501_CUSTOM_P2_VISOR(Daan);
    AUX501_CUSTOM_P2_VISOR(Dakota);
    AUX501_CUSTOM_P2_VISOR(Dusty);

    AUX501_CUSTOM_P2_VISOR(Gary);
    AUX501_CUSTOM_P2_VISOR(Grey);

    AUX501_CUSTOM_P2_VISOR(Eleck);

    AUX501_CUSTOM_P2_VISOR(Hosed);
    AUX501_CUSTOM_P2_VISOR(Hungarian);

    AUX501_CUSTOM_P2_VISOR(Jub);
    AUX501_CUSTOM_P2_VISOR(Juggernaut);

    AUX501_CUSTOM_P2_VISOR(Maple);

    AUX501_CUSTOM_P2_VISOR(Napkin);
    AUX501_CUSTOM_P2_VISOR(Narrator);
    AUX501_CUSTOM_P2_VISOR(Nico);
    AUX501_CUSTOM_P2_VISOR(Noodle);

    AUX501_CUSTOM_P2_VISOR(Patriot);

    AUX501_CUSTOM_P2_VISOR(Regicide);
    AUX501_CUSTOM_P2_VISOR(Rias);
    AUX501_CUSTOM_P2_VISOR(Rythian);

    AUX501_CUSTOM_P2_VISOR(September);
    AUX501_CUSTOM_P2_VISOR(Smiley);
    AUX501_CUSTOM_P2_VISOR(Source);
    AUX501_CUSTOM_P2_VISOR(Stitches);
    AUX501_CUSTOM_P2_VISOR(Swanny);

    AUX501_CUSTOM_P2_VISOR(Talker);
    AUX501_CUSTOM_P2_VISOR(Triage);

    AUX501_CUSTOM_P2_VISOR(Vegas);
    AUX501_CUSTOM_P2_VISOR(Vulpes);

    AUX501_CUSTOM_P2_VISOR(Werdplay);
    AUX501_CUSTOM_P2_VISOR(Wirtimus);
    AUX501_CUSTOM_P2_VISOR(Worgan);

    //Special Materials
    AUX501_CUSTOM_P2_RVMAT(Andromeda);

    AUX501_CUSTOM_P2_RVMAT(Bridger);

    AUX501_CUSTOM_P2_RVMAT(Drammon);

    AUX501_CUSTOM_P2_RVMAT(Hobnob);

    AUX501_CUSTOM_P2_RVMAT(Jatt);

    AUX501_CUSTOM_P2_RVMAT(Kestrel);
    AUX501_CUSTOM_P2_RVMAT(Klinger);

    AUX501_CUSTOM_P2_RVMAT(Shatter);
};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_GI_Helmet_Customs
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "helmet" };
            class helmet
            {
                label = "Helmet";
                values[] = 
                {
                    "Ace",
                    "Achilles",
                    "Ahmanni",
                    "AJ",
                    "Alex",
                    "Alistair",
                    "Andromeda",
                    "Anheiser",
                    "Anselm",
                    "Anta",
                    "Asher",
                    
                    "Bambo",
                    "Base",
                    "Bevel",
                    "Biscuit",
                    "Bishop",
                    "Bjorn",
                    "Black",
                    "Blood",
                    "Boneish",
                    "Bones",
                    "Boozy",
                    "Box",
                    "Bradley",
                    "Breach",
                    "Bridger",
                    "Broad",
                    "Bruce",
                    "Buddy",
                    "Buffalo",
                    "Buggs",
                    "Burrito",

                    "Cactus",
                    "Casual",
                    "Chan",
                    "Charl",
                    "Chaser",
                    "Checkers",
                    "Cherokee",
                    "Chimera",
                    "Chosen1",
                    "Clock",
                    "Clover",
                    "Compo",
                    "Concerned",
                    "Craz",
                    "Crebar",
                    "Crinkcase",
                    "Crisis",
                    "Cruisie",
                    "Crush",
                    "Cupid",
                    "Cursed",
                    
                    "Daan",
                    "Dadecoy",
                    "Dakota",
                    "Dasand",
                    "Defender",
                    "Demo",
                    "Dimitri",
                    "Diverge",
                    "Dobby",
                    "Donut",
                    "Drammon",
                    "Dredge",
                    "Drifter",
                    "Dusty",

                    "Effort",
                    "Eleck",

                    "Fenrir",
                    "Fixit",
                    "Fluorite",
                    "Fly",
                    "Ford",

                    "Gallagher",
                    "Garviel",
                    "Gary",
                    "Ghost",
                    "Goddest",
                    "Goldarp",
                    "Great",
                    "Greene",
                    "Grey",
                    "Guardian",

                    "Heat",
                    "Hobnob",
                    "Hoodoo",
                    "Hosed",
                    "Hungarian",

                    "Ichigo",
                    "Iron",

                    "Jamenson",
                    "Jatt",
                    "Jay",
                    "Jimmy",
                    "Jub",
                    "Juggernaut",
                    "June",

                    "Kahn",
                    "Kenny",
                    "Kestrel",
                    "Kitti",
                    "Klinger",
                    "Knight",
                    "Koda",
                    "Krios",
                    "Kyrie",

                    "Law",
                    "Legia",
                    "Len",
                    "Leon",
                    "Liberty",
                    "Loki",

                    "Mada",
                    "Maelstrom",
                    "Maple",
                    "Marshmellow",
                    "Maverick",
                    "Mayhal",
                    "Midnight",
                    "Mikado",
                    "Mills",
                    "Mirror",
                    "Money",
                    "Morris",
                    "Mungus",
                    "Mustang",

                    "Napkin",
                    "Narrator",
                    "Neta",
                    "Nico",
                    "Noodle",
                    
                    "Odin",
                    "Oktapius",

                    "Patriot",
                    "Peterson",
                    "Pollyon",
                    "Popeye",
                    "Prime",

                    "Rage",
                    "Raktharg",
                    "Raloris",
                    "Ranque",
                    "Rapteh",
                    "Raptor",
                    "Rebellion",
                    "Regicide",
                    "Revenant",
                    "Rias",
                    "Ripjaw",
                    "Roe",
                    "Roster",
                    "Rupert",
                    "Rythian",

                    "Sazi",
                    "Scanlon",
                    "Schames",
                    "September",
                    "Shadow",
                    "Shape",
                    "Shark",
                    "Shatter",
                    "Shredded",
                    "Sig",
                    "Silver",
                    "Simon",
                    "Singed",
                    "Ski",
                    "Sledge",
                    "Slyder",
                    "Smiley",
                    "Snoopy",
                    "Solus",
                    "Sosa",
                    "Sour",
                    "Source",
                    "Soyvolon",
                    "Sparticus",
                    "Spud",
                    "Squeaks",
                    "Stitches",
                    "Stoanes",
                    "Storm",
                    "Sunshine",
                    "Super",
                    "Surtr",
                    "Swanny",

                    "Taka",
                    "Taken",
                    "Talker",
                    "Target",
                    "Tatum",
                    "Tee",
                    "Tideend",
                    "Tik",
                    "Tim",
                    "Train",
                    "Triage",
                    "Tuch",
                    "Tupiks",
                    "Twine",
                    "Twisted",
                    "Typhon",

                    "Unity",

                    "Vegas",
                    "Vengeance",
                    "Versa",
                    "Volley",
                    "Vulpes",

                    "Wag",
                    "Walsh",
                    "Wasp",
                    "Waylander",
                    "Werdplay",
                    "Winter",
                    "Wirtimus",
                    "Wizard",
                    "Worgan",

                    "Yokai",
                    "Young",

                    "Zeros"
                };
                class Ace        { label = "Ace"; };
                class Achilles   { label = "Achilles"; };
                class Ahmanni    { label = "Ahmanni"; };
                class AJ         { label = "AJ"; };
                class Alex       { label = "Alex"; };
                class Alistair   { label = "Alistair"; };
                class Andromeda  { label = "Andromeda"; };
                class Anheiser   { label = "Anheiser"; };
                class Anselm     { label = "Anselm"; };
                class Anta       { label = "Anta"; };
                class Asher      { label = "Asher"; };
                
                class Bambo      { label = "Bambo"; };
                class Base       { label = "Base"; };
                class Bevel      { label = "Bevel"; };
                class Biscuit    { label = "Biscuit"; };
                class Bishop     { label = "Bishop"; };
                class Bjorn      { label = "Bjorn"; };
                class Black      { label = "Black"; };
                class Blood      { label = "Blood"; };
                class Boneish    { label = "Boneish"; };
                class Bones      { label = "Bones"; };
                class Boozy      { label = "Boozy"; };
                class Box        { label = "Box"; };
                class Bradley    { label = "Bradley"; };
                class Breach     { label = "Breach"; };
                class Bridger    { label = "Bridger"; };
                class Broad      { label = "Broad"; };
                class Bruce      { label = "Bruce"; };
                class Buddy      { label = "Buddy"; };
                class Buffalo    { label = "Buffalo"; };
                class Buggs      { label = "Buggs"; };
                class Burrito    { label = "Burrito"; };
                 
                class Cactus     { label = "Cactus"; };
                class Casual     { label = "Casual"; };
                class Chan       { label = "Chan"; };
                class Charl      { label = "Charl"; };
                class Chaser     { label = "Chaser"; };
                class Checkers   { label = "Checkers"; };
                class Cherokee   { label = "Cherokee"; };
                class Chimera    { label = "Chimera"; };
                class Chosen1    { label = "Chosen1"; };
                class Clock      { label = "Clock"; };
                class Clover     { label = "Clover"; };
                class Compo      { label = "Compo"; };
                class Concerned  { label = "Concerned"; };
                class Craz       { label = "Craz"; };
                class Crebar     { label = "Crebar"; };
                class Crinkcase  { label = "Crinkcase"; };
                class Crisis     { label = "Crisis"; };
                class Cruisie    { label = "Cruisie"; };
                class Crush      { label = "Crush"; };
                class Cupid      { label = "Cupid"; };
                class Cursed     { label = "Cursed"; };
 
                class Daan       { label = "Daan"; };
                class Dadecoy    { label = "Dadecoy"; };
                class Dakota     { label = "Dakota"; };
                class Dasand     { label = "Dasand"; };
                class Defender   { label = "Defender"; };
                class Demo       { label = "Demo"; };
                class Dimitri    { label = "Dimitri"; };
                class Diverge    { label = "Diverge"; };
                class Dobby      { label = "Dobby"; };
                class Donut      { label = "Donut"; };
                class Drammon    { label = "Drammon"; };
                class Dredge     { label = "Dredge"; };
                class Drifter    { label = "Drifter"; };
                class Dusty      { label = "Dusty"; };

                class Effort     { label = "Effort"; };
                class Eleck      { label = "Eleck"; };

                class Fenrir     { label = "Fenrir"; };
                class Fixit      { label = "Fixit"; };
                class Fluorite   { label = "Fluorite"; };
                class Fly        { label = "Fly"; };
                class Ford       { label = "Ford"; };
                
                class Gallagher  { label = "Gallagher"; };
                class Garviel    { label = "Garviel"; };
                class Gary       { label = "Gary"; };
                class Ghost      { label = "Ghost"; };
                class Goddest    { label = "Goddest"; };
                class Goldarp    { label = "Goldarp"; };
                class Great      { label = "Great"; };
                class Greene     { label = "Greene"; };
                class Grey       { label = "Grey"; };
                class Guardian   { label = "Guardian"; };
                
                class Heat       { label = "Heat"; };
                class Hobnob     { label = "Hobnob"; };
                class Hoodoo     { label = "Hoodoo"; };
                class Hosed      { label = "Hosed"; };
                class Hungarian  { label = "Hungarian"; };
                
                class Ichigo     { label = "Ichigo"; };
                class Iron       { label = "Iron"; };
                
                class Jamenson   { label = "Jamenson"; };
                class Jatt       { label = "Jatt"; };
                class Jay        { label = "Jay"; };
                class Jimmy      { label = "Jimmy"; };
                class Jub        { label = "Jub"; };
                class Juggernaut { label = "Juggernaut"; };
                class June       { label = "June"; };

                class Kahn       { label = "Kahn"; };
                class Kenny      { label = "Kenny"; };
                class Kestrel    { label = "Kestrel"; };
                class Kitti      { label = "Kitti"; };
                class Klinger    { label = "Klinger"; };
                class Knight     { label = "Knight"; };
                class Koda       { label = "Koda"; };
                class Krios      { label = "Krios"; };
                class Kyrie      { label = "Kyrie"; };
                
                class Law        { label = "Law"; };
                class Legia      { label = "Legia"; };
                class Len        { label = "Len"; };
                class Leon       { label = "Leon"; };
                class Liberty    { label = "Liberty"; };
                class Loki       { label = "Loki"; };
                
                class Mada       { label = "Mada"; };
                class Maelstrom  { label = "Maelstrom"; };
                class Maple      { label = "Maple"; };
                class Marshmellow   { label = "Marshmellow"; };
                class Maverick   { label = "Maverick"; };
                class Mayhal     { label = "Mayhal"; };
                class Midnight   { label = "Midnight"; };
                class Mills      { label = "Mills"; };
                class Mirror     { label = "Mirror"; };
                class Money      { label = "Money"; };
                class Morris     { label = "Morris"; };
                class Mungus     { label = "Mungus"; };
                class Mustang    { label = "Mustang"; };
                
                class Napkin     { label = "Napkin"; };
                class Narrator   { label = "Narrator"; };
                class Neta       { label = "Neta"; };
                class Nico       { label = "Nico"; };
                class Noodle     { label = "Noodle"; };
                  
                class Odin       { label = "Odin"; };
                class Oktapius   { label = "Oktapius"; };
                
                class Patriot    { label = "Patriot"; };
                class Peterson   { label = "Peterson"; };
                class Pollyon    { label = "Pollyon"; };
                class Popeye     { label = "Popeye"; };
                class Prime      { label = "Prime"; };

                class Rage       { label = "Rage"; };
                class Raktharg   { label = "Raktharg"; };
                class Raloris    { label = "Raloris"; };
                class Ranque     { label = "Ranque"; };
                class Rapteh     { label = "Rapteh"; };
                class Raptor     { label = "Raptor"; };
                class Rebellion  { label = "Rebellion"; };
                class Regicide   { label = "Regicide"; };
                class Revenant   { label = "Revenant"; };
                class Rias       { label = "Rias"; };
                class Ripjaw     { label = "Ripjaw"; };
                class Roe        { label = "Roe"; };
                class Roster     { label = "Roster"; };
                class Rupert     { label = "Rupert"; };
                class Rythian    { label = "Rythian"; };

                class Sazi       { label = "Sazi"; };
                class Scanlon    { label = "Scanlon"; };
                class Schames    { label = "Schames"; };
                class September  { label = "September"; };
                class Shadow     { label = "Shadow"; };
                class Shape      { label = "Shape"; };
                class Shark      { label = "Shark"; };
                class Shatter    { label = "Shatter"; };
                class Shredded   { label = "Shredded"; };
                class Sig        { label = "Sig"; };
                class Silver     { label = "Silver"; };
                class Simon      { label = "Simon"; };
                class Singed     { label = "Singed"; };
                class Ski        { label = "Ski"; };
                class Sledge     { label = "Sledge"; };
                class Slyder     { label = "Slyder"; };
                class Smiley     { label = "Smiley"; };
                class Snoopy     { label = "Snoopy"; };
                class Solus      { label = "Solus"; };
                class Sosa       { label = "Sosa"; };
                class Sour       { label = "Sour"; };
                class Source     { label = "Source"; };
                class Soyvolon   { label = "Soyvolon"; };
                class Sparticus  { label = "Sparticus"; };
                class Spud       { label = "Spud"; };
                class Squeaks    { label = "Squeaks"; };
                class Stitches   { label = "Stitches"; };
                class Storm      { label = "Storm"; };
                class Sunshine   { label = "Sunshine"; };
                class Super      { label = "Super"; };
                class Surtr      { label = "Surtr"; };
                class Swanny     { label = "Swanny"; };

                class Taka       { label = "Taka"; };
                class Taken      { label = "Taken"; };
                class Talker     { label = "Talker"; };
                class Target     { label = "Target"; };
                class Tatum      { label = "Tatum"; };
                class Tee        { label = "Tee"; };
                class Tideend    { label = "Tideend"; };
                class Tik        { label = "Tik"; };
                class Tim        { label = "Tim"; };
                class Train      { label = "Train"; };
                class Triage     { label = "Triage"; };
                class Tuch       { label = "Tuch"; };
                class Tupiks     { label = "Tupiks"; };
                class Twine      { label = "Twine"; };
                class Twisted    { label = "Twisted"; };
                class Typhon     { label = "Typhon"; };

                class Unity      { label = "Unity"; };

                class Vegas      { label = "Vegas"; };
                class Vengeance  { label = "Vengeance"; };
                class Versa      { label = "Versa"; };
                class Volley     { label = "Volley"; };
                class Vulpes     { label = "Vulpes"; };
                
                class Wag        { label = "Wag"; };
                class Walsh      { label = "Walsh"; };
                class Wasp       { label = "Wasp"; };
                class Waylander  { label = "Waylander"; };
                class Werderplay { label = "Werderplay"; };
                class Winter     { label = "Winter"; };
                class Wirtimus   { label = "Wirtimus"; };
                class Wizard     { label = "Wizard"; };
                class Worgan     { label = "Worgan"; };

                class Yokai      { label = "Yokai"; };
                class Young      { label = "Young"; };

                class Zeros      { label = "Zeros"; };
            };
        };
    };
};