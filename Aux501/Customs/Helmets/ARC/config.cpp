#define AUX501_CUSTOM_ARC(name)\
    class Aux501_Customs_501st_ARC_Helmet_##name : Aux501_Units_Republic_501st_ARC_Trooper_Helmet\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM ARC HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\ARC\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\ARC\Textures\##name##_co.paa\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Helmet_Customs";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_ARC_VISOR(name)\
    class Aux501_Customs_501st_ARC_Helmet_##name : Aux501_Units_Republic_501st_ARC_Trooper_Helmet\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM ARC HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\ARC\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\ARC\Textures\##name##_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Units\Republic\501st\ARC\Helmets\Phase1.5\data\textures\Clone_helmet_ARC.rvmat,\
            \Aux501\Customs\Helmets\ARC\Materials\##name##_Visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Helmet_Customs";\
            helmet = name;\
        };\
    }

#define AUX501_CUSTOM_ARC_RVMAT(name)\
    class Aux501_Customs_501st_ARC_Helmet_##name : Aux501_Units_Republic_501st_ARC_Trooper_Helmet\
    {\
        scope = 2;\
        displayName = [501st] CUSTOM ARC HELM - name;\
        hiddenSelectionsTextures[] = \
        {\
            \Aux501\Customs\Helmets\ARC\Textures\##name##_co.paa,\
            \Aux501\Customs\Helmets\ARC\Textures\##name##_co.paa\
        };\
        hiddenSelectionsMaterials[]= \
        {\
            \Aux501\Customs\Helmets\ARC\Materials\##name##.rvmat,\
            \Aux501\Customs\Helmets\ARC\Materials\##name##_Visor.rvmat\
        };\
        class XtdGearInfo\
        {\
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Helmet_Customs";\
            helmet = name;\
        };\
    }

class cfgPatches
{
    class Aux501_Patch_Customs_Helmets_ARC
    {
        addonRootClass = "Aux501_Patch_Customs";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_ARC_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Customs_501st_ARC_Helmet_Alya",
            "Aux501_Customs_501st_ARC_Helmet_Athely",

            "Aux501_Customs_501st_ARC_Helmet_Bemount",
            "Aux501_Customs_501st_ARC_Helmet_Blade",
            "Aux501_Customs_501st_ARC_Helmet_Blaze",
            "Aux501_Customs_501st_ARC_Helmet_Brad",

            "Aux501_Customs_501st_ARC_Helmet_Cito",

            "Aux501_Customs_501st_ARC_Helmet_Deytow",
            "Aux501_Customs_501st_ARC_Helmet_Doug",
            "Aux501_Customs_501st_ARC_Helmet_Dragony",
            "Aux501_Customs_501st_ARC_Helmet_Duke",

            "Aux501_Customs_501st_ARC_Helmet_Grogg",
            "Aux501_Customs_501st_ARC_Helmet_Guts",

            "Aux501_Customs_501st_ARC_Helmet_Halo",

            "Aux501_Customs_501st_ARC_Helmet_Jink",

            "Aux501_Customs_501st_ARC_Helmet_Inferno",

            "Aux501_Customs_501st_ARC_Helmet_Luck",

            "Aux501_Customs_501st_ARC_Helmet_Mane",
            "Aux501_Customs_501st_ARC_Helmet_Merlin",

            "Aux501_Customs_501st_ARC_Helmet_Nightingale",

            "Aux501_Customs_501st_ARC_Helmet_Odd",

            "Aux501_Customs_501st_ARC_Helmet_Panther",
            "Aux501_Customs_501st_ARC_Helmet_Parkes",
            "Aux501_Customs_501st_ARC_Helmet_Pebbles",

            "Aux501_Customs_501st_ARC_Helmet_Rinzler",

            "Aux501_Customs_501st_ARC_Helmet_Sabre",
            "Aux501_Customs_501st_ARC_Helmet_Sandman",
            "Aux501_Customs_501st_ARC_Helmet_Signus",
            "Aux501_Customs_501st_ARC_Helmet_Smoke",
            "Aux501_Customs_501st_ARC_Helmet_Starpup",

            "Aux501_Customs_501st_ARC_Helmet_Tal",

            "Aux501_Customs_501st_ARC_Helmet_Weapon",

            "Aux501_Customs_501st_ARC_Helmet_Zareth"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_ARC_Trooper_Helmet;

    //Normal Helmet
    AUX501_CUSTOM_ARC(Athely);

    AUX501_CUSTOM_ARC(Bemount);
    AUX501_CUSTOM_ARC(Blade);
    AUX501_CUSTOM_ARC(Blaze);
    AUX501_CUSTOM_ARC(Brad);

    AUX501_CUSTOM_ARC(Cito);

    AUX501_CUSTOM_ARC(Deytow);
    AUX501_CUSTOM_ARC(Doug);
    AUX501_CUSTOM_ARC(Dragony);
    AUX501_CUSTOM_ARC(Duke);

    AUX501_CUSTOM_ARC(Guts);

    AUX501_CUSTOM_ARC(Jink);

    AUX501_CUSTOM_ARC(Mane);

    AUX501_CUSTOM_ARC(Nightingale);

    AUX501_CUSTOM_ARC(Odd);

    AUX501_CUSTOM_ARC(Parkes);
    AUX501_CUSTOM_ARC(Pebbles);

    AUX501_CUSTOM_ARC(Rinzler);

    AUX501_CUSTOM_ARC(Sandman);
    AUX501_CUSTOM_ARC(Smoke);
    AUX501_CUSTOM_ARC(Starpup);

    AUX501_CUSTOM_ARC(Tal);
    
    AUX501_CUSTOM_ARC(Weapon);

    //Glowing Visor
    AUX501_CUSTOM_ARC_VISOR(Alya);

    AUX501_CUSTOM_ARC_VISOR(Halo);

    AUX501_CUSTOM_ARC_VISOR(Luck);

    AUX501_CUSTOM_ARC_VISOR(Merlin);

    AUX501_CUSTOM_ARC_VISOR(Panther);

    AUX501_CUSTOM_ARC_VISOR(Sabre);
    AUX501_CUSTOM_ARC_VISOR(Signus);

    AUX501_CUSTOM_ARC_VISOR(Widyen);

    AUX501_CUSTOM_ARC_VISOR(Zareth);

    //Special Materials
    //AUX501_CUSTOM_ARC_RVMAT(name)
    
    AUX501_CUSTOM_ARC_RVMAT(Grogg);

    AUX501_CUSTOM_ARC_RVMAT(Inferno);
    
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_ARC_Helmet_Customs
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "helmet" };
            class helmet
            {
                label = "Helmet";
                values[] = 
                {
                    "Alya",
                    "Athely",

                    "Bemount",
                    "Blade",
                    "Blaze",
                    "Brad",

                    "Cito",

                    "Deytow",
                    "Doug",
                    "Dragony",
                    "Duke",

                    "Grogg",
                    "Guts",

                    "Halo",

                    "Inferno",

                    "Jink",

                    "Luck",

                    "Merlin",
                    "Mane",

                    "Nightingale",

                    "Odd",

                    "Panther",
                    "Parkes",
                    "Pebbles",

                    "Rinzler",

                    "Sabre",
                    "Sandman",
                    "Signus",
                    "Smoke",
                    "Starpup",

                    "Tal",

                    "Weapon",
                    "Widyen",

                    "Zareth"
                };
                class Alya        { label = "Alya"; };
                class Athely      { label = "Athely"; };

                class Bemount     { label = "Bemount"; };
                class Blade       { label = "Blade"; };
                class Blaze       { label = "Blaze"; };
                class Brad        { label = "Brad"; };

                class Cito        { label = "Cito"; };

                class Deytow      { label = "Deytow"; };
                class Doug        { label = "Doug"; };
                class Dragony     { label = "Dragony"; };
                class Duke        { label = "Duke"; };

                class Grogg       { label = "Grogg"; };
                class Guts        { label = "Guts"; };

                class Halo        { label = "Halo"; };

                class Inferno     { label = "Inferno"; };
                
                class Jink        { label = "Jink"; };
                
                class Luck        { label = "Luck"; };

                class Mane        { label = "Mane"; };
                class Merlin      { label = "Merlin"; };

                class Nightingale { label = "Nightingale"; };

                class Odd         { label = "Odd"; };
                
                class Panther     { label = "Panther"; };
                class Parkes      { label = "Parkes"; };
                class Pebbles     { label = "Pebbles"; };

                class Rinzler     { label = "Rinzler"; };

                class Sabre       { label = "Sabre"; };
                class Sandman     { label = "Sandman"; };
                class Smoke       { label = "Smoke"; };
                class Starpup     { label = "Starpup"; };
                
                class Tal         { label = "Tal"; };

                class Weapon      { label = "Weapon"; };
                class Widyen      { label = "Widyen"; };

                class Zareth      { label = "Zareth"; };
            };
        };
    };
};