class cfgPatches
{
    class Aux501_Patch_Units_Republic_101_Backpack
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Standard",
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Large"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_Standard",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic",
            "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_Standard;
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic;
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large;

    class Aux501_Units_Republic_101_Backpack: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "{101st} Backpack 01";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_backpack_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_101st_Backpacks";
            variant = "standard";
        };
    };

    class Aux501_Units_Republic_101_Medical_Backpack: Aux501_Units_Republic_501_Infantry_Backpacks_Medic
    {
        displayName = "{101st} Medic Backpack 01";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCMedic_backpack_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_101st_Backpacks";
            variant = "medic";
        };
    };
    class Aux501_Units_Republic_101_RTO_Backpack: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large
    {
        displayName = "{101st} RTO Backpack 01";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_DCTrooper_backpack_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_101st_Backpacks";
            variant = "rto";
        };
    };
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_Republic_101st_Backpacks
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "medic",
                    "rto"
                };
                class standard
                {
                    label = "Standard";
                };
                class medic
                {
                    label = "Medic";
                };
                class rto
                {
                    label = "RTO";
                };
            };
        };
    };
};