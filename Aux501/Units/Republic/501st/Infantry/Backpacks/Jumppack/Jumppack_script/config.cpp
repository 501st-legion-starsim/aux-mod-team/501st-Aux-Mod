class CfgPatches
{
    class Aux501_jumppack
    {
        addonRootClass = "Aux501_Patch_Units";
        version[] = {1,0,0,0};
        requiredAddons[] = 
        {
            "ace_medical",
            "cba_settings",
            "A3_UI_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class Extended_PreInit_EventHandlers
{
    class Aux501_jumppack
    {
        init = "call compileFinal preprocessFileLineNumbers 'Aux501\Units\Republic\501st\Infantry\Backpacks\Jumppack\Jumppack_script\XEH_preInit.sqf'";
    };
};

class Extended_PostInit_EventHandlers
{
    class Aux501_jumppack
    {
        init = "call compileFinal preprocessFileLineNumbers 'Aux501\Units\Republic\501st\Infantry\Backpacks\Jumppack\Jumppack_script\XEH_postInit.sqf'";
    };
};

class ACE_Medical_Injuries
{
    class damageTypes
    {
        class woundHandlers;
        class falling
        {
            class woundHandlers: woundHandlers
            {
                Aux501_jumppack = "{ _this call Aux501_jumppack_fnc_fallDamageHandler }";
            };
        };
        class collision
        {
            class woundHandlers: woundHandlers
            {
                Aux501_jumppack = "{ _this call Aux501_jumppack_fnc_fallDamageHandler }";
            };
        };
    };
};

class RscControlsGroup;
class RscFrame;
class RscStructuredText;

class RscTitles
{
    class Aux501_jumppack_hud
    {
        idd = 722730;
        duration = 1e+11;
        name = "Aux501_jumppack_hud";
        enableDisplay = 1;
        enableSimulation = 1;
        movingEnable = 0;
        onLoad = "with uiNamespace do { Aux501_jumppack_hud = _this select 0 }";
        onUnload = "with uiNamespace do { Aux501_jumppack_hud = displayNull }";
        fadeIn = 0;
        fadeOut = 0;
        class Controls
        {
            class Aux501_jumppack_details: RscControlsGroup
            {
                idc = 722731;
                x = "0.845 * safezoneW + safezoneX";
                y = "0.405 * safezoneH + safezoneY";
                w = "0.2 * safezoneW";
                h = "0.143 * safezoneH";
                type = 15;
                onLoad = "with uiNamespace do { Aux501_jumppack_hudDetails = _this select 0 }";
                class Controls
                {
                    class displayNameFrame: RscFrame
                    {
                        idc = 722732;
                        text = "Jumppack Name Goes Here";
                        x = "0 * (((safezoneW / safezoneH) min 1.2) / 40)";
                        y = "0 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                        w = "20 * (((safezoneW / safezoneH) min 1.2) / 40)";
                        h = "5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                        colorText[] = {1,1,1,1};
                        colorBackground[] = {0,0,0,0};
                        sizeEx = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                        type = 0;
                    };
                    class jumpTypeText: RscStructuredText
                    {
                        idc = 722733;
                        text = "Selected Jump Cycle";
                        x = "1 * (((safezoneW / safezoneH) min 1.2) / 40)";
                        y = "1.5 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                        w = "9 * (((safezoneW / safezoneH) min 1.2) / 40)";
                        h = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                        colorBackground[] = {0,0,0,0};
                        sizeEx = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                        type = 13;
                    };
                    class energyText: RscStructuredText
                    {
                        idc = 722734;
                        text = "Energy: 100/200";
                        x = "1 * (((safezoneW / safezoneH) min 1.2) / 40)";
                        y = "3 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                        w = "9 * (((safezoneW / safezoneH) min 1.2) / 40)";
                        h = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                        colorText[] = {1,1,1,1};
                        colorBackground[] = {0,0,0,0};
                        sizeEx = "1 * ((((safezoneW / safezoneH) min 1.2) / 1.2) / 25)";
                        type = 13;
                    };
                };
            };
        };
    };
};

class JumpTypes
{
    rechargeIntervalSeconds = 0.625;
    class Forward
    {
        displayName = "Forward Jump";
        forward = 12;
        vertical = 20;
        cost = 50;
        generateExecuteKeybind = 1;
    };
    class Short
    {
        displayName = "Short Jump";
        forward = 12;
        vertical = 5;
        relativeToCamera = 1;
        cost = 30;
        generateExecuteKeybind = 1;
    };
    class Dash
    {
        displayName = "Dash";
        forward = 20;
        vertical = 2;
        cost = 20;
        hideFromCycle = 1;
        generateExecuteKeybind = 1;
    };
    class Cancel
    {
        displayName = "Stop";
        forward = 0;
        vertical = 0;
        cost = 5;
        hideFromCycle = 1;
        generateExecuteKeybind = 1;
    };
};