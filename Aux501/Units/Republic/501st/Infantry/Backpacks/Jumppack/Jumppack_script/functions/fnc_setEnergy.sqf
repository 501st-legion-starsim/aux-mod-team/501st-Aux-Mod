/*
 * Author: M3ales
 * This function sets the energy of a Aux501_jumppack for a given unit to a defined value.
 * If the value passed is -1, it will be replaced with the max capacity.
 *
 * Arguments:
 * 0: Unit who is using the jumppack <OBJECT>
 * 1: The amount to set the energy to <NUMBER>
 *
 * Return Value:
 * None
 *
 * Example:
 * [player, 10] call Aux501_jumppack_fnc_setEnergy
 * [player, -1] call Aux501_jumppack_fnc_setEnergy
 *
 * Public: Yes
 */

#include "function_macros.hpp"

params["_unit", "_value"];

private _jumppack = [_unit] call FUNC(getJumppack);
if (_jumppack isEqualTo false) exitWith {};

_jumppack params ["_class", "_capacity", "_rechargeRate"];

// Set the energy value to the capacity if -1 is provided
if (_value isEqualTo -1) then {
	_value = _capacity;
};

_value = [_value, 0, _capacity] call BIS_fnc_clamp;

(unitBackpack _unit) setVariable [QGVAR(energy), _value, true];

[_unit] call FUNC(showState);
