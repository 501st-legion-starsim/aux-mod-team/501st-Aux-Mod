#define macro_isJumppack Aux501_jumppack_is_jumppack
#define macro_spamDelay  Aux501_jumppack_spam_delay
#define macro_energyCap Aux501_jumppack_energy_capacity
#define macro_rechargeRate Aux501_jumppack_recharge
#define macro_effectScript Aux501_jumppack_jump_effect_script
#define macro_effectPoints Aux501_jumppack_effect_points[]

#define macro_igniteSound Aux501_jumppack_sound_ignite[]
#define macro_landSound Aux501_jumppack_sound_land[]
#define macro_idleSound Aux501_jumppack_sound_idle[]

#define macro_textureCatagory Aux501_jumppack_skin_group[]

#define macro_types_of_jumps Aux501_jumppack_jump_types[]//update script

#define macro_default_texture_group "default"

#define macro_jumppackClass(group,name) Aux501_jumppack_##group##_##name




