/*
 * Author: M3ales
 * Handles the fall damage event for units equipped with a jumppack.
 *
 * Arguments:
 * 0: Unit <OBJECT>
 * 1: damage Array <ARRAY>
 * 2: damage type <STRING>
 *
 * Return Value:
 * Modified damage array or original input array if not applicable <ARRAY>
 *
 * Example:
 * [player, [5, "head", "falling"], "falling"] call Aux501_jumppack_fnc_fallDamageHandler;
 *
 * Public: No
 */

#include "function_macros.hpp"

params ["_unit", "_damageArr", "_type"];

if (_type isNotEqualTo "falling" && _type isNotEqualTo "collision") exitWith {
	_this
};

private _jumppack = [_unit] call FUNC(getJumppack);
if (_jumppack isEqualTo false) exitWith {
	_this
};

LOG("Absorbed impact with ground, ignored damage");

[_unit, [], _type]
