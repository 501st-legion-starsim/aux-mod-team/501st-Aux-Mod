/*
 * Author: M3ales
 * This function loads jump types from the configuration file and returns them as an array.
 *
 * Arguments:
 * 0: Config Path <CONFIG>
 *
 * Return Value:
 * Array of jump types <ARRAY>
 *
 * Example:
 * [configFile] call Aux501_jumppack_fnc_loadJumpTypesFromConfig
 *
 * Public: Yes
 */

#include "function_macros.hpp"

LOG("Searching for valid JumpTypes");

private _store = [];

// Retrieve all jump type classes from the config file
private _jumpTypeClasses = configProperties [    configFile >> "JumpTypes",    "isClass _x",    true];

LOGF_1("Found %1 jump classes defined", count _jumpTypeClasses);

// Iterate through the jump type classes and extract their properties
{
    // Retrieve jump type properties from the config
    private _name = [_x, "displayName", configName _x] call BIS_fnc_returnConfigEntry;
    private _forward = [_x, "forward", false] call BIS_fnc_returnConfigEntry;
    private _vertical = [_x, "vertical", false] call BIS_fnc_returnConfigEntry;
    private _cost = [_x, "cost", 0] call BIS_fnc_returnConfigEntry;
    private _relativeToCamera = [_x, "relativeToCamera", 0] call BIS_fnc_returnConfigEntry > 0;
    private _hideFromCycle = [_x, "hideFromCycle", 0] call BIS_fnc_returnConfigEntry > 0;
    private _generateSelectKeybind = [_x, "generateSelectKeybind", 0] call BIS_fnc_returnConfigEntry > 0;
    private _generateExecuteKeybind = [_x, "generateExecuteKeybind", 0] call BIS_fnc_returnConfigEntry > 0;

    // Check if the jump type has a forward or vertical component
    if (_forward isEqualType false && _vertical isEqualType false) then {
        LOG_WARNF_1("Jump Type %1 does not specify a forward or vertical component to it's jump arc, this effectively does nothing.", _name);
    };

    // Add the jump type to the storage array
    _store pushBack [configName _x, _name, _forward, _vertical, _cost, _relativeToCamera, _hideFromCycle, _generateSelectKeybind, _generateExecuteKeybind];
    // Log the addition of the jump type
    LOGF_1("Adding jump type '%1'", configName _x);
} forEach(_jumpTypeClasses);

LOGF_1("Read %1 jump types from config", count _store);

_store
