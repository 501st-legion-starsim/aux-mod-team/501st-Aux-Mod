#define PREP(func_name) [QUOTE(ADDON_PATH\functions\CONCAT(fnc_,func_name).sqf), QUOTE(FUNC(func_name))] call cba_fnc_compileFunction; diag_log format["PREP %1", QUOTE(FUNC(func_name))]

PREP(addDispenserSelfActionsToClass);
PREP(addRechargePFH);
PREP(canJump);
PREP(cycleJumpType);
PREP(cycleWrap);
PREP(displayHud);
PREP(executeJump);
PREP(fallDamageHandler);
PREP(getJumppack);
PREP(getJumppackFromDispenser);
PREP(handleHideSettingsChanged);
PREP(inheritsFromConfig);
PREP(isHiddenFromCycle);
PREP(jump);
PREP(loadJumpDispenserVehiclesFromConfig);
PREP(loadJumppacksFromConfig);
PREP(loadJumpTypesFromConfig);
PREP(playIgniteSound);
PREP(rechargeEnergy);
PREP(setEnergy);
PREP(setJumpType);
PREP(showState);
PREP(trySetJumpType);
PREP(tryUseEnergy);