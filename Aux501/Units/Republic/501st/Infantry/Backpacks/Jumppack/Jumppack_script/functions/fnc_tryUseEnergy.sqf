/*
 * Author: M3ales
 * This function attempts to subtract a specified amount of energy from the unit's jumppack.
 * It returns true if the energy was successfully subtracted or false if not.
 *
 * Arguments:
 * 0: Unit to subtract energy from <OBJECT>
 * 1: Amount of energy to subtract <NUMBER>
 *
 * Return Value:
 * A boolean indicating whether the energy was subtracted successfully or not <BOOLEAN>
 *
 * Example:
 * [player, 10] call Aux501_jumppack_fnc_tryUseEnergy;
 *
 * Public: Yes
 */

#include "function_macros.hpp"

params["_unit", "_amount"];

private _backpack = (unitBackpack _unit);
private _currentEnergy = _backpack getVariable [QGVAR(energy), 0];

if (_currentEnergy >= _amount) exitWith {
	_backpack setVariable [QGVAR(energy), _currentEnergy - _amount, true];
	true
};

false
