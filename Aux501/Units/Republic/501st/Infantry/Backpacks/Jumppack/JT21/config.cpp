class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Jumppack_JT21
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501st_Infantry_Backpacks_Jumppack_CDV"
        };
        units[] = 
        {            
            "Aux501_Units_Republic_501st_Infantry_Droppack_JT21"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_White;

    //Airborne Squads
    class Aux501_Units_Republic_501st_Infantry_Droppack_JT21: Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_White
    {
        displayName = "[501st] JT-21 Droppack 01";
        picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\Clone_jumppack_chicken_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor2\CloneJumppackChicken.p3d";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stTrooper_jumppack_chicken_co.paa"};
        hiddenSelectionsMaterials[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Jumppack\JT21\Materials\Aux501_JT21_droppack.rvmat"
        };
        class XtdGearInfo{};
        class Aux501_jumppack 
        {
            igniteSound = "\Aux501\Units\Republic\501st\Infantry\Backpacks\Jumppack\sounds\cdv21Start.ogg";
            rechargeRateSecond = 0;
            capacity = 0;
            allowedJumpTypes[] = {};
        };
    };
};