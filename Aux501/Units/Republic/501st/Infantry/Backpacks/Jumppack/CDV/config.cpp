class cfgPatches
{
    class Aux501_Patch_Units_Republic_501st_Infantry_Backpacks_Jumppack_CDV
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Large"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_White",
            "Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_BN"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large;

    class Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_White: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large
    {
        displayName = "[501st] CDV Jumppack 01 - White";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_jumppack_mc_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneJumppackMC.p3d";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_jumppack_mc_co.paa"};
        hiddenSelectionsMaterials[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Jumppack\CDV\Materials\cdv_jumppack.rvmat"
        };
        class Aux501_jumppack 
        {
            igniteSound = "\Aux501\Units\Republic\501st\Infantry\Backpacks\Jumppack\sounds\cdv21Start.ogg";
            rechargeRateSecond = 8;
            capacity = 150;
            allowedJumpTypes[] = 
            {
                "Forward",
                "Short",
                "Dash",
                "Cancel"
            };
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_CDV_Jumppack";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_BN: Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_White
    {
        displayName = "[501st] CDV Jumppack 02 - BN";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Jumppack\CDV\textures\inf_jumppack_cdv_bn_co.paa"
        };
        maximumLoad = 1000;
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_CDV_Jumppack";
            variant = "battalion";
        };
    };
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_Republic_501st_CDV_Jumppack
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "variant" };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "battalion",
                    "razor",
                    "lightning"
                };
                class standard       { label = "Standard"; };
                class battalion      { label = "BN"; };
                class razor          { label = "Razor"; };
                class lightning      { label = "Lightning"; };
            };
        };
    };
};