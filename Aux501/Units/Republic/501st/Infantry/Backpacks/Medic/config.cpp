class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Medics
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Standard"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic_cm",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic_cmv",

            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic_cm_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic_cmv_straps"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_Standard;

    //Backpacks
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] MED Backpack 01 - CM-C";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Medic\textures\Aux501_backpack_medic_CMC.paa"
        };
        maximumload = 1000;
        tf_dialog = "JLTS_clone_rto_radio_dialog";
        tf_dialogUpdate = "call TFAR_fnc_updateLRDialogToChannel;";
        tf_encryptionCode = "tf_west_radio_code";
        tf_hasLRradio = 1;
        tf_range = 35000;
        tf_subtype = "digital_lr";
        class Attributes
        {
            class staticRadioFrequency
            {
                displayName = "$STR_tfar_static_radios_moduleStaticRadio_FreqTitle";
                tooltip = "$STR_tfar_static_radios_moduleStaticRadio_ATT_Frequency_tooltip";
                property = "staticRadioFrequency";
                control = "Edit";
                expression = "if (isMultiplayer) then {[_this,call compile _value] call TFAR_static_radios_fnc_setFrequencies}";
                defaultValue = "str (_this call TFAR_static_radios_fnc_generateFrequencies)";
                validate = "none";
                condition = "objectHasInventoryCargo";
                typeName = "STRING";
            };
            class staticRadioChannel
            {
                displayName = "$STR_tfar_static_radios_moduleStaticRadio_ChannelTitle";
                tooltip = "$STR_tfar_static_radios_moduleStaticRadio_ATT_Channel_tooltip";
                property = "staticRadioChannel";
                control = "Edit";
                expression = "if (isMultiplayer) then {[_this,_value] call TFAR_static_radios_fnc_setActiveChannel}";
                defaultValue = "'1'";
                validate = "none";
                condition = "objectHasInventoryCargo";
                typeName = "NUMBER";
            };
            class staticRadioSpeaker
            {
                displayName = "$STR_tfar_static_radios_moduleStaticRadio_ATT_SpeakerEnabled";
                tooltip = "$STR_tfar_static_radios_moduleStaticRadio_ATT_SpeakerEnabled_tooltip";
                property = "staticRadioSpeaker";
                control = "Checkbox";
                expression = "if (isMultiplayer) then {[_this,_value] call TFAR_static_radios_fnc_setSpeakers}";
                defaultValue = "false";
                validate = "none";
                condition = "objectHasInventoryCargo";
                typeName = "NUMBER";
            };
            class staticRadioVolume
            {
                displayName = "$STR_tfar_static_radios_moduleStaticRadio_ATT_RadioVolume";
                tooltip = "$STR_tfar_static_radios_moduleStaticRadio_ATT_RadioVolume_tooltip";
                property = "staticRadioVolume";
                control = "tfar_static_radios_volumeSlider";
                expression = "if (isMultiplayer) then {[_this,_value] call TFAR_static_radios_fnc_setVolume}";
                defaultValue = 7;
                validate = "none";
                condition = "objectHasInventoryCargo";
                typeName = "NUMBER";
            };
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Medics";
            rank = "cmc";
            strap = "no_strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic_cm: Aux501_Units_Republic_501_Infantry_Backpacks_Medic
    {
        displayName = "[501st] MED Backpack 02 - CM";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Medic\textures\Aux501_backpack_medic_CM.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Medics";
            rank = "cm";
            strap = "no_strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic_cmv: Aux501_Units_Republic_501_Infantry_Backpacks_Medic
    {
        displayName = "[501st] MED Backpack 03 - CM-V";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Medic\textures\Aux501_backpack_medic_CMV.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Medics";
            rank = "cmv";
            strap = "no_strap";
        };
    };

    //Strappacks
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Medic
    {
        displayName = "[501st] MED Strappack 01 - CM-C";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Medics";
            rank = "cmc";
            strap = "strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic_cm_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Medic_cm
    {
        displayName = "[501st] MED Strappack 02 - CM";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Medics";
            rank = "cm";
            strap = "strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic_cmv_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Medic_cmv
    {
        displayName = "[501st] MED Strappack 03 - CM-V";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Medics";
            rank = "cmv";
            strap = "strap";
        };
    };
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_Republic_501st_Backpack_Medics
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "rank",
                "strap"
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "cmc",
                    "cm",
                    "cmv"
                };
                class cmc
                {
                    label = "CM-C";
                };
                class cm
                {
                    label = "CM";
                };
                class cmv
                {
                    label = "CM-V";
                };
            };
            class strap
            {
                label = "Straps";
                changeingame = 1;
                changedelay = 1;
                values[] = 
                {
                    "no_strap",
                    "strap"
                };
                class no_strap
                {
                    label = "Strapless";
                    actionLabel = "Remove Straps";
                };
                class strap
                {
                    label = "Strap";
                    actionLabel = "Attach Straps";
                };
            };
        };
    };
};