class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Standard
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_Standard",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Assault",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Grenadier",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Engineer",
            "Aux501_Units_Republic_501_Infantry_Backpacks_EOD",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic",

            "Aux501_Units_Republic_501_Infantry_Backpacks_Medic_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Standard_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_AT_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Grenadier_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Engineer_straps",
            "Aux501_Units_Republic_501_Infantry_Backpacks_EOD_straps"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;

    class Aux501_Units_Republic_501_Infantry_Backpacks_Standard: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] INF Backpack 03 - Standard";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "standard";
            strap = "no_strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_AT: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] INF Backpack 04 - AT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Standard\data\textures\Aux501_backpack_staticweapons_rocket_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "at";
            strap = "no_strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Grenadier: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] INF Backpack 05 - Grenadier";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Standard\data\textures\Aux501_backpack_staticweapons_grenade_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "gren";
            strap = "no_strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Engineer: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] INF Backpack 06 - Engineer";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Backpacks\Standard\data\textures\Aux501_backpack_engineer_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "engi";
            strap = "no_strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_EOD: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] INF Backpack 07 - EOD";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneArmor\data\Clone_backpack_eod_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "eod";
            strap = "no_strap";
        };
    };

    //Medical Backpacks
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] MED Backpack 01";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneArmor\data\Clone_backpack_medic_co.paa"
        };
        maximumload = 1000;
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "medic";
            strap = "no_strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Medic_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Medic
    {
        displayName = "[501st] MED Strappack 01";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "medic";
            strap = "strap";
        };
    };

    //Strappacks
    class Aux501_Units_Republic_501_Infantry_Backpacks_Standard_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Standard
    {
        displayName = "[501st] INF Strappack 01 - Standard";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "standard";
            strap = "strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_AT_straps: Aux501_Units_Republic_501_Infantry_Backpacks_AT
    {
        displayName = "[501st] INF Strappack 02 - AT";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "at";
            strap = "strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Grenadier_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Grenadier
    {
        displayName = "[501st] INF Strappack 03 - Grenadier";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "gren";
            strap = "strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Engineer_straps: Aux501_Units_Republic_501_Infantry_Backpacks_Engineer
    {
        displayName = "[501st] INF Strappack 04 - Engineer";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "engi";
            strap = "strap";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_EOD_straps: Aux501_Units_Republic_501_Infantry_Backpacks_EOD
    {
        displayName = "[501st] INF Strappack 05 - EOD";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackStraps.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Standard";
            variant = "eod";
            strap = "strap";
        };
    };
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_Republic_501st_Backpack_Standard
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant",
                "strap"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "at",
                    "gren",
                    "engi",
                    "eod",
                    "medic",
                    "rto"
                };
                class standard
                {
                    label = "Standard";
                };
                class at
                {
                    label = "AT";
                };
                class gren
                {
                    label = "Grenadier";
                };
                class engi
                {
                    label = "Engineer";
                };
                class eod
                {
                    label = "EOD";
                };
                class medic
                {
                    label = "Medic";
                };
                class rto
                {
                    label = "RTO";
                };
            };
            class strap
            {
                label = "Straps";
                changeingame = 1;
                changedelay = 1;
                values[] = 
                {
                    "no_strap",
                    "strap"
                };
                class no_strap
                {
                    label = "Strapless";
                    actionLabel = "Remove Straps";
                };
                class strap
                {
                    label = "Strap";
                    actionLabel = "Attach Straps";
                };
            };
        };
    };
};