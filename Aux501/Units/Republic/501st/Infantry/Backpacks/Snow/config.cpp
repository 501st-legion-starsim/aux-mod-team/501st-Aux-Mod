class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Snow
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_Snow",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Snow_LR"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_Standard;
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large;
    
    class Aux501_Units_Republic_501_Infantry_Backpacks_Snow: Aux501_Units_Republic_501_Infantry_Backpacks_Standard 
    {
        displayName = "[501st] SNOW Backpack 01";
        model = "WarMantle\WM_Imperial_Core\SnowTrooper_backpack.p3d";
        hiddenSelections[] = {};
        hiddenSelectionsTextures[] = {};
        class XtdGearInfo
        {
        model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Snow";
        variant = "standard";
        };
    };

    class Aux501_Units_Republic_501_Infantry_Backpacks_Snow_LR: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large 
    {
        displayName = "[501st] SNOW Backpack 02 - LR";
        model = "WarMantle\WM_Imperial_Core\SnowTrooper_backpack.p3d";
        hiddenSelections[] = {};
        hiddenSelectionsTextures[] = {};
        class XtdGearInfo
        {
        model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Snow";
        variant = "lr";
        };
    };

    class Aux501_Units_Republic_501_Infantry_Backpacks_Snow_RTO: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large 
    {
        displayName = "[501st] SNOW Backpack 03 - RTO";
        model = "\JLTS_AE_GM\SEA_Backpack_GM_RTO.p3d";
        hiddenselections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\JLTS_AE_GM\data\BaseTextures\Backpack\RTO\Backpack_Base_ca.paa",
            "\JLTS_AE_GM\data\BaseTextures\Backpack\RTO\Backpack_RTO_ca.paa"
        };
        class XtdGearInfo
        {
        model = "Aux501_ACEX_Gear_Republic_501st_Backpack_Snow";
        variant = "rto";
        };
    };
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_Republic_501st_Backpack_Snow
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "lr",
                    "rto"
                };
                class standard
                {
                    label = "Standard";
                };
                class lr
                {
                    label = "LR";
                };
                class rto
                {
                    label = "RTO";
                };
            };
        };
    };
};