class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Backpacks_Invisible
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks",
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Small"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Backpacks_Invisible",
            "Aux501_Units_Republic_501_Infantry_Backpacks_Invisible_LR"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Small;

    class Aux501_Units_Republic_501_Infantry_Backpacks_Invisible: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[501st] INF Backpack 00 - Standard";
        picture = "\Aux501\Units\Republic\501st\Infantry\Backpacks\Invisible\data\textures\UI\501st_CloneTrooper_Backpack_empty_UI_ca.paa";
        model = "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\Empty.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpacks_Basic";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Backpacks_Invisible_LR: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Small
    {
        displayname = "[501st] INF Backpack 01 - LR";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Shoulder_Antenna\data\textures\UI\Aux501_FW_shoulder_antenna_ui_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Wolffe_Vest.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "swlb_clones\data\mc_camo1_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Backpacks_Basic";
            variant = "lr";
        };
    };
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_Republic_501st_Backpacks_Basic
        
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "lr",
                    "beltbag",
                    "lr_small"
                };
                class standard
                {
                    label = "Standard";
                };
                class beltbag
                {
                    label = "Beltbag";
                };
                class lr
                {
                    label = "LR";
                };
                class lr_small
                {
                    label = "LR Small";
                };
            };
        };
    };
};