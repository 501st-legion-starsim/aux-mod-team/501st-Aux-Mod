class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_green_hot_lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_green_hot_lowlight"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_02_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_03_green_hot_lowlight",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_01_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_02_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_03_green_hot_lowlight"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        class ItemInfo;
    };

    //Normal Officer Visor
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_green_hot_lowlight
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 06 - Officer - Black";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_cc_visor_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_visor_co.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "black";
            nvg_type = "lowlight";
            ti_color = "green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 06 - Officer - Dark Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_CGFox_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "dark_gray";
            nvg_type = "lowlight";
            ti_color = "green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_02_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 06 - Officer - Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_KSCommander_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "gray";
            nvg_type = "lowlight";
            ti_color = "green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_03_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 06 - Officer - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Officer_Visor\inf_officer_visor_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "blue";
            nvg_type = "lowlight";
            ti_color = "green_hot";
        };
    };

    //Cody Style
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 06 - CMDR - Black";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_mc_visor_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGMC.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGMC.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGMC.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "black";
            nvg_type = "lowlight";
            ti_color = "green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_01_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 06 - CMDR - Dark Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_CGFox_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "dark_gray";
            nvg_type = "lowlight";
            ti_color = "green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_02_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 06 - CMDR - Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_KSCommander_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "gray";
            nvg_type = "lowlight";
            ti_color = "green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_03_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 06 - CMDR - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Officer_Visor\inf_officer_visor_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "blue";
            nvg_type = "lowlight";
            ti_color = "green_hot";
        };
    };
};