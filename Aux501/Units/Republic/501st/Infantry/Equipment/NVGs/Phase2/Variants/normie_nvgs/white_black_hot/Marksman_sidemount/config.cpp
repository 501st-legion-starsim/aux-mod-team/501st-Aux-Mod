class cfgPatches
{
    class Aux501_Patch_Units_Republic_Equipment_NVGs_Phase2_Marksman_white_black_hot
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_white_black_hot"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_Equipment_NVGs_Marksman_white_black_hot"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_hot;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_black_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_hot
    {
        class ItemInfo;
    };

    class Aux501_Republic_Equipment_NVGs_Marksman_white_black_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_black_hot
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 03 - Marksman";
        picture = "\IBL\characters\death\data\ui\icon_nvg_ca.paa";
        model = "\IBL\characters\death\death_nvg_matte.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\IBL\characters\death\data\death_helmet_black_co.paa",
            "\IBL\characters\death\data\death_helmet_black_co.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "",
            "\IBL\core\data\helmet_visor_emissive.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1","camo2"};
            uniformModel = "\IBL\characters\death\death_nvg_matte.p3d";
            modelOff = "\IBL\characters\death\death_nvg_matte.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Marksman";
            nvg_type = "standard_nv";
            ti_color = "white_black_hot";
        };
    };
};