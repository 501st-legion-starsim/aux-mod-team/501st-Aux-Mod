class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_02",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_03",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_01",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_02",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_03"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_base;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI: Aux501_Republic_501_Infantry_Equipment_NVGs_base
    {
        class ItemInfo;
    };

    //Normal Officer Visor
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 06 - Officer - Black";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_cc_visor_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_visor_co.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "black";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor
    {
        displayName = "[501st] INF NVG 06 - Officer - Dark Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_CGFox_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "dark_gray";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_02: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor
    {
        displayName = "[501st] INF NVG 06 - Officer - Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_KSCommander_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "gray";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_03: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor
    {
        displayName = "[501st] INF NVG 06 - Officer - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Officer_Visor\inf_officer_visor_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "blue";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };

    //Cody Style
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor
    {
        displayName = "[501st] INF NVG 06 - CMDR - Black";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_mc_visor_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGMC.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGMC.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGMC.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "black";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_01: Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor
    {
        displayName = "[501st] INF NVG 06 - CMDR - Dark Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_CGFox_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "dark_gray";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_02: Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor
    {
        displayName = "[501st] INF NVG 06 - CMDR - Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_KSCommander_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "gray";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_03: Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor
    {
        displayName = "[501st] INF NVG 06 - CMDR - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Officer_Visor\inf_officer_visor_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "blue";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "style",
                "variant",
                "nvg_type",
                "ti_color"
            };
            class style
            {
                label = "Style";
                values[] = 
                {
                    "officer",
                    "commander",
                    "phase1"
                };
                class officer     {label = "Officer";};
                class commander   {label = "Commander";};
                class phase1      {label = "Phase 1";};
            };
            class variant
            {
                label = "Variant";
                values[] =
                {
                    "black",
                    "dark_gray",
                    "gray",
                    "blue"
                };
                class black       {label = "Black";};
                class dark_gray   {label = "Dark Gray";};
                class gray        {label = "Gray";};
                class blue        {label = "Blue";};
            };
            class nvg_type
            {
                label = "NVG Type";
                values[] = 
                {
                    "lowlight",
                    "standard_nv"
                };
                class lowlight       {label = "Lowlight";};
                class standard_nv    {label = "Lowlight/NV";};
            };
            class ti_color
            {
                label = "Thermal Style";
                values[] = 
                {
                    "white_hot",
                    "black_hot",
                    "white_black_hot",
                    "light_green_hot",
                    "dark_green_hot",
                    "green_hot",
                    "light_orange_hot",
                    "dark_orange_hot",
                    "orange_hot",
                    "yellow_hot",
                    "brightest_color_hot"
                };
                class white_hot             {label = "White";};
                class black_hot             {label = "Black";};
                class white_black_hot       {label = "White/Black";};
                class light_green_hot       {label = "Light Green";};
                class dark_green_hot        {label = "Dark Green";};
                class green_hot             {label = "Green";};
                class light_orange_hot      {label = "Light Orange";};
                class dark_orange_hot       {label = "Dark Orange";};
                class orange_hot            {label = "Orange";};
                class yellow_hot            {label = "Yellow";};
                class brightest_color_hot   {label = "Bright Color";};
            };
        };
    };
};