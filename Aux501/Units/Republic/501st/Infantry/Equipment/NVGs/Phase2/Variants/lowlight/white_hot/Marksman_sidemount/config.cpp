class cfgPatches
{
    class Aux501_Patch_Units_Republic_Equipment_NVGs_Phase2_Marksman_white_hot_lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_Equipment_NVGs_Marksman_white_hot_lowlight"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_base;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI: Aux501_Republic_501_Infantry_Equipment_NVGs_base
    {
        class ItemInfo;
    };

    class Aux501_Republic_Equipment_NVGs_Marksman_white_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 03 - Marksman";
        picture = "\IBL\characters\death\data\ui\icon_nvg_ca.paa";
        model = "\IBL\characters\death\death_nvg_matte.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\IBL\characters\death\data\death_helmet_black_co.paa",
            "\IBL\characters\death\data\death_helmet_black_co.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "",
            "\IBL\core\data\helmet_visor_emissive.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1","camo2"};
            uniformModel = "\IBL\characters\death\death_nvg_matte.p3d";
            modelOff = "\IBL\characters\death\death_nvg_matte.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Marksman";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_NVGs_Marksman
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "nvg_type",
                "ti_color"
            };
            class nvg_type
            {
                label = "NVG Type";
                values[] = 
                {
                    "lowlight",
                    "standard_nv"
                };
                class lowlight       {label = "Lowlight";};
                class standard_nv    {label = "Lowlight/NV";};
            };
            class ti_color
            {
                label = "Thermal Style";
                values[] = 
                {
                    "white_hot",
                    "black_hot",
                    "white_black_hot",
                    "light_green_hot",
                    "dark_green_hot",
                    "green_hot",
                    "light_orange_hot",
                    "dark_orange_hot",
                    "orange_hot",
                    "yellow_hot",
                    "brightest_color_hot"
                };
                class white_hot             {label = "White";};
                class black_hot             {label = "Black";};
                class white_black_hot       {label = "White/Black";};
                class light_green_hot       {label = "Light Green";};
                class dark_green_hot        {label = "Dark Green";};
                class green_hot             {label = "Green";};
                class light_orange_hot      {label = "Light Orange";};
                class dark_orange_hot       {label = "Dark Orange";};
                class orange_hot            {label = "Orange";};
                class yellow_hot            {label = "Yellow";};
                class brightest_color_hot   {label = "Bright Color";};
            };
        };
    };
};