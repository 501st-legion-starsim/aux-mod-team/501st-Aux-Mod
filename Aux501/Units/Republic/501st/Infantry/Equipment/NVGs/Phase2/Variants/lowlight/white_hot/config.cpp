class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_base;
   
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV: Aux501_Republic_501_Infantry_Equipment_NVGs_base
    {
        scope = 2;
        scopearsenal = 2;
        picture = "\MRC\JLTS\Core_mod\data\ui\nvg_chip_1_ui_ca.paa";
        displayName = "[501st] INF NVG 01 - NVG";
        model = "\MRC\JLTS\weapons\E5S\E5S_mag.p3d";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips";
            nvg_type = "lowlight";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 02 - TI";
        picture = "\MRC\JLTS\Core_mod\data\ui\nvg_chip_2_ui_ca.paa";
        visionMode[] = {"Normal","TI"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips_TI";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "nvg_type"
            };
            class nvg_type
            {
                label = "NVG Type";
                values[] = 
                {
                    "lowlight",
                    "standard_nv"
                };
                class lowlight       {label = "Lowlight";};
                class standard_nv    {label = "Lowlight/NV";};
            };
        };
        class Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips_TI
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "nvg_type",
                "ti_color"
            };
            class nvg_type
            {
                label = "NVG Type";
                values[] = 
                {
                    "lowlight",
                    "standard_nv"
                };
                class lowlight       {label = "Lowlight";};
                class standard_nv    {label = "Lowlight/NV";};
            };
            class ti_color
            {
                label = "Thermal Style";
                values[] = 
                {
                    "white_hot",
                    "black_hot",
                    "white_black_hot",
                    "light_green_hot",
                    "dark_green_hot",
                    "green_hot",
                    "light_orange_hot",
                    "dark_orange_hot",
                    "orange_hot",
                    "yellow_hot",
                    "brightest_color_hot"
                };
                class white_hot             {label = "White";};
                class black_hot             {label = "Black";};
                class white_black_hot       {label = "White/Black";};
                class light_green_hot       {label = "Light Green";};
                class dark_green_hot        {label = "Dark Green";};
                class green_hot             {label = "Green";};
                class light_orange_hot      {label = "Light Orange";};
                class dark_orange_hot       {label = "Dark Orange";};
                class orange_hot            {label = "Orange";};
                class yellow_hot            {label = "Yellow";};
                class brightest_color_hot   {label = "Bright Color";};
            };
        };
    };
};