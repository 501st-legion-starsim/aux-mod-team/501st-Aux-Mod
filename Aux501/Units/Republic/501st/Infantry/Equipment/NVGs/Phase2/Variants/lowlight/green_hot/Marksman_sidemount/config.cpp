class cfgPatches
{
    class Aux501_Patch_Units_Republic_Equipment_NVGs_Phase2_Marksman_green_hot_lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_green_hot_lowlight"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_Equipment_NVGs_Marksman_green_hot_lowlight"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        class ItemInfo;
    };

    class Aux501_Republic_Equipment_NVGs_Marksman_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_green_hot_lowlight
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 03 - Marksman";
        picture = "\IBL\characters\death\data\ui\icon_nvg_ca.paa";
        model = "\IBL\characters\death\death_nvg_matte.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\IBL\characters\death\data\death_helmet_black_co.paa",
            "\IBL\characters\death\data\death_helmet_black_co.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "",
            "\IBL\core\data\helmet_visor_emissive.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1","camo2"};
            uniformModel = "\IBL\characters\death\death_nvg_matte.p3d";
            modelOff = "\IBL\characters\death\death_nvg_matte.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Marksman";
            nvg_type = "lowlight";
            ti_color = "green_hot";
        };
    };
};