class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Macro_Binos_light_green_hot_lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_light_green_hot_lowlight"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down_light_green_hot_lowlight",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501_light_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_501_down_light_green_hot_lowlight",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_blue_light_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_blue_down_light_green_hot_lowlight",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red_light_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_down_light_green_hot_lowlight",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_gray_light_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_gray_down_light_green_hot_lowlight",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_blue_full_light_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_blue_full_down_light_green_hot_lowlight",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red_full_light_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_full_down_light_green_hot_lowlight",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_gray_full_light_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_gray_full_down_light_green_hot_lowlight",
            
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_black_full_light_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_black_full_down_light_green_hot_lowlight"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        class ItemInfo;
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_light_green_hot_lowlight
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 04 - MacroBino";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVG_off.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_co.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVG_on.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVG_off.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "white";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Down";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVG_on.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVG_on.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVG_on.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "white";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - 501st";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_501st_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "FiveOhFirst";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_501_down_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - 501st";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_501st_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "FiveOhFirst";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_blue_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_blue_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "blue";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_blue_down_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_blue_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "blue";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_blue_full_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Full Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_full_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "full_blue";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_blue_full_down_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Full Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_full_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "full_blue";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Red";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_red_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "red";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_down_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Red";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_red_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "red";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red_full_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Full Red";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_full_red_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "full_red";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_red_full_down_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Full Red";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_full_red_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "full_red";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_gray_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Gray";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_gray_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "gray";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_gray_down_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Gray";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_gray_on_white_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "gray";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_gray_full_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Full Gray";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_full_gray_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "full_gray";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_gray_full_down_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Full Gray";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Macro_Binos\inf_macrobino_full_gray_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "full_gray";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_black_full_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Full Black";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor2\data\Clone_PurgeTrooper_nvg_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_toggle";
            color = "black";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_black_full_down_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Microbino_down_light_green_hot_lowlight
    {
        displayName = "[501st] INF NVG 04 - MacroBino - Full Black";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor2\data\Clone_PurgeTrooper_nvg_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVGs_Macrobinos";
            variant = "visor_down";
            color = "black";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
};