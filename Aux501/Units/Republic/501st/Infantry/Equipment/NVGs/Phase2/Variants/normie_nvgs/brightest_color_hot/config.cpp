class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_brightest_color_hot
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_white_hot"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_brightest_color_hot"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_hot;
   
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_brightest_color_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_hot
    {
        scope = 2;
        scopearsenal = 2;
        thermalMode[] = {7};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips_TI";
            nvg_type = "standard_nv";
            ti_color = "brightest_color_hot";
        };
    };
};