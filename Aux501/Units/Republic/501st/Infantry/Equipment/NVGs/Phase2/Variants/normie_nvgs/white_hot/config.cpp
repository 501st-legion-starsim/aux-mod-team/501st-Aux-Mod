class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_white_hot
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_standard_nv",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_hot"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV;
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI;
   
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_standard_nv: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV
    {
        scope = 2;
        scopearsenal = 2;
        visionMode[] = {"Normal","NVG"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips";
            variant = "nvg";
            nvg_type = "standard_nv";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        scope = 2;
        scopearsenal = 2;
        visionMode[] = {"Normal","NVG","Ti"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips_TI";
            nvg_type = "standard_nv";
            ti_color = "white_hot";
        };
    };
};