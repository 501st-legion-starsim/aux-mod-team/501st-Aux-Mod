class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_dark_green_hot_lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_dark_green_hot_lowlight"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI;
   
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_dark_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        scope = 2;
        scopearsenal = 2;
        thermalMode[] = {3};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_NVG_chips_TI";
            nvg_type = "lowlight";
            ti_color = "dark_green_hot";
        };
    };
};