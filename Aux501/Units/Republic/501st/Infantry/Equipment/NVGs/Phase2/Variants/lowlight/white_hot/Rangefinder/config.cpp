class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Rangefinder
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs",
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_blue",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_red",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_yellow"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_base;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI: Aux501_Republic_501_Infantry_Equipment_NVGs_base
    {
        class ItemInfo;
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 05 - Rangefinder - Black";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_range_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_off.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Rangefinder\inf_rangefinder_black_co.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_on.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_off.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase2";
            color = "black";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder
    {
        displayName = "[501st] INF NVG 05 - Rangefinder - White";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_range_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase2";
            color = "white";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_blue: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder
    {
        displayName = "[501st] INF NVG 05 - Rangefinder - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Rangefinder\inf_rangefinder_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase2";
            color = "blue";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_red: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder
    {
        displayName = "[501st] INF NVG 05 - Rangefinder - Red";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_ARCHammer_rangefinder_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase2";
            color = "red";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_yellow: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder
    {
        displayName = "[501st] INF NVG 05 - Rangefinder - Yellow";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_ARCBlitz_rangefinder_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase2";
            color = "yellow";
            nvg_type = "lowlight";
            ti_color = "white_hot";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant",
                "color",
                "nvg_type",
                "ti_color"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "phase2",
                    "phase1",
                    "snow"
                };
                class phase2     {label = "P2";};
                class phase1     {label = "P1";};
                class snow       {label = "Snow";};
            };
            class color
            {
                label = "Color";
                values[] = 
                {
                    "black",
                    "white",
                    "blue",
                    "red",
                    "yellow"
                };
                class black     {label = "Black";};
                class white     {label = "White";};
                class blue      {label = "Blue";};
                class red       {label = "Red";};
                class yellow    {label = "Yellow";};
            };
            class nvg_type
            {
                label = "NVG Type";
                values[] = 
                {
                    "lowlight",
                    "standard_nv"
                };
                class lowlight       {label = "Lowlight";};
                class standard_nv    {label = "Lowlight/NV";};
            };
            class ti_color
            {
                label = "Thermal Style";
                values[] = 
                {
                    "white_hot",
                    "black_hot",
                    "white_black_hot",
                    "light_green_hot",
                    "dark_green_hot",
                    "green_hot",
                    "light_orange_hot",
                    "dark_orange_hot",
                    "orange_hot",
                    "yellow_hot",
                    "brightest_color_hot"
                };
                class white_hot             {label = "White";};
                class black_hot             {label = "Black";};
                class white_black_hot       {label = "White/Black";};
                class light_green_hot       {label = "Light Green";};
                class dark_green_hot        {label = "Dark Green";};
                class green_hot             {label = "Green";};
                class light_orange_hot      {label = "Light Orange";};
                class dark_orange_hot       {label = "Dark Orange";};
                class orange_hot            {label = "Orange";};
                class yellow_hot            {label = "Yellow";};
                class brightest_color_hot   {label = "Bright Color";};
            };
        };
    };
};