class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Rangefinder_light_orange_hot_lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_light_orange_hot_lowlight"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_orange_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white_light_orange_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_blue_light_orange_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_red_light_orange_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_yellow_light_orange_hot_lowlight"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_light_orange_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        class ItemInfo;
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_orange_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_light_orange_hot_lowlight
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 05 - Rangefinder - Black";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_range_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_off.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Rangefinder\inf_rangefinder_black_co.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_on.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGRange_off.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase2";
            color = "black";
            nvg_type = "lowlight";
            ti_color = "light_orange_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white_light_orange_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_orange_hot_lowlight
    {
        displayName = "[501st] INF NVG 05 - Rangefinder - White";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_range_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase2";
            color = "white";
            nvg_type = "lowlight";
            ti_color = "light_orange_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_blue_light_orange_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_orange_hot_lowlight
    {
        displayName = "[501st] INF NVG 05 - Rangefinder - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Rangefinder\inf_rangefinder_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase2";
            color = "blue";
            nvg_type = "lowlight";
            ti_color = "light_orange_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_red_light_orange_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_orange_hot_lowlight
    {
        displayName = "[501st] INF NVG 05 - Rangefinder - Red";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_ARCHammer_rangefinder_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase2";
            color = "red";
            nvg_type = "lowlight";
            ti_color = "light_orange_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_yellow_light_orange_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_light_orange_hot_lowlight
    {
        displayName = "[501st] INF NVG 05 - Rangefinder - Yellow";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_ARCBlitz_rangefinder_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase2";
            color = "yellow";
            nvg_type = "lowlight";
            ti_color = "light_orange_hot";
        };
    };
};