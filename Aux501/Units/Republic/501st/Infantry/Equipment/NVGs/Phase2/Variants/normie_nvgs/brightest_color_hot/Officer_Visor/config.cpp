class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_Officer_Visor_brightest_color_hot
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_brightest_color_hot"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01_brightest_color_hot",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_02_brightest_color_hot",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_03_brightest_color_hot",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_brightest_color_hot",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_01_brightest_color_hot",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_02_brightest_color_hot",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_03_brightest_color_hot"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_hot;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_brightest_color_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_hot
    {
        class ItemInfo;
    };

    //Normal Officer Visor
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_brightest_color_hot
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 06 - Officer - Black";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_cc_visor_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_nvg_visor_co.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGCC.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "black";
            nvg_type = "standard_nv";
            ti_color = "brightest_color_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_01_brightest_color_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot
    {
        displayName = "[501st] INF NVG 06 - Officer - Dark Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_CGFox_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "dark_gray";
            nvg_type = "standard_nv";
            ti_color = "brightest_color_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_02_brightest_color_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot
    {
        displayName = "[501st] INF NVG 06 - Officer - Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_KSCommander_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "gray";
            nvg_type = "standard_nv";
            ti_color = "brightest_color_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_03_brightest_color_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot
    {
        displayName = "[501st] INF NVG 06 - Officer - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Officer_Visor\inf_officer_visor_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "officer";
            variant = "blue";
            nvg_type = "standard_nv";
            ti_color = "brightest_color_hot";
        };
    };

    //Cody Style
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_brightest_color_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor_brightest_color_hot
    {
        displayName = "[501st] INF NVG 06 - CMDR - Black";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_mc_visor_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneNVGMC.p3d";
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneNVGMC.p3d";
            modelOff = "\MRC\JLTS\characters\CloneArmor\CloneNVGMC.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "black";
            nvg_type = "standard_nv";
            ti_color = "brightest_color_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_01_brightest_color_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_brightest_color_hot
    {
        displayName = "[501st] INF NVG 06 - CMDR - Dark Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_CGFox_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "dark_gray";
            nvg_type = "standard_nv";
            ti_color = "brightest_color_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_02_brightest_color_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_brightest_color_hot
    {
        displayName = "[501st] INF NVG 06 - CMDR - Gray";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_KSCommander_visor_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "gray";
            nvg_type = "standard_nv";
            ti_color = "brightest_color_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_03_brightest_color_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Commander_visor_brightest_color_hot
    {
        displayName = "[501st] INF NVG 06 - CMDR - Blue";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\NVGs\Phase2\textures\Officer_Visor\inf_officer_visor_blue_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "commander";
            variant = "blue";
            nvg_type = "standard_nv";
            ti_color = "brightest_color_hot";
        };
    };
};