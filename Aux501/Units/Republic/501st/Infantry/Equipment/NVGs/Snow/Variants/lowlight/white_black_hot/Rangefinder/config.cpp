class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Snow_Rangefinder_white_black_hot_lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_white_black_hot_lowlight"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Snow_Rangefinder_black_white_black_hot_lowlight"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_black_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        class ItemInfo;
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Snow_Rangefinder_black_white_black_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_black_hot_lowlight
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] SNOW Rangefinder 01 - Black ";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_range_ui_ca.paa";
        model = "JLTS_AE_GM\SEA_Equipment_Rangefinder_Off.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\JLTS_AE_GM\data\BaseTextures\RangeFinder\Rangefinder_ca.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "JLTS_AE_GM\SEA_Equipment_Rangefinder_On.p3d";
            modelOff = "JLTS_AE_GM\SEA_Equipment_Rangefinder_Off.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "snow";
            color = "black";
            nvg_type = "lowlight";
            ti_color = "white_black_hot";
        };
    };
};