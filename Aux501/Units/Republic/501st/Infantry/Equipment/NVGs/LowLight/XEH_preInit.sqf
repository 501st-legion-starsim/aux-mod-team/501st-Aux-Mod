#include "\a3\editor_f\Data\Scripts\dikCodes.h"
#include "functions\function_macros.hpp"
#define DIK_MOUSEUP         0xF8
#define DIK_MOUSEDOWN       0xF9

LOG("PreInit Begin");
private _addonConfig = configFile >> "CfgPatches" >> QUOTE(ADDON);
private _version = getArray(_addonConfig >> "version");
LOG(format["Version: %1",_verison]);

LOG("PREP Begin");
#include "XEH_PREP.sqf"
LOG("PREP Complete");

["loadout", FUNC(gearChanged)] call CBA_fnc_addPlayerEventHandler;

[       ["Aux501 - Lowlight", ADDON_NAME]
,       "LowLightToggle"
,       [       "Toggle Low Light"
        ,       "Toggles Low Light on/off"
        ]
,       { [player] call FUNC(toggle); }
,       ""
,       [DIK_N,[false, true, false]]
,       false
] call cba_fnc_addKeybind;
[       ["Aux501 - Lowlight", ADDON_NAME]
,       QGVAR(GainUp)
,       [       "Increase Gain"
        ,       "Increases Gain / Brightness."
        ]
,       { [player] call FUNC(gainUp); }
,       ""
,       [DIK_MOUSEUP,[true, false, false]]
,       false
] call cba_fnc_addKeybind;
[       ["Aux501 - Lowlight", ADDON_NAME]
,       QGVAR(GainDown)
,       [       "Decrease Gain"
        ,       "Decreases Gain / Brightness."
        ]
,       { [player] call FUNC(gainDown); }
,       ""
,       [DIK_MOUSEDOWN,[true, false, false]]
,       false
] call cba_fnc_addKeybind;

[       QGVAR(DefaultGain)
,       "SLIDER"
,       ["Default Gain", "The Default Gain / Brightness"]
,       ["Aux501", ADDON_NAME]
,       [0.02,1,500, 2]
,       2
//,       { [player] call FUNC(switchOn); } this needs to check if its off
] call cba_fnc_addSetting;

LOG("PreInit Complete");
