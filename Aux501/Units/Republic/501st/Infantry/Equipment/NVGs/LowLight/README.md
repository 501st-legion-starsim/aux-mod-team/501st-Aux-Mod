# LowLight

Adds lowlight to NVG pieces with adjustable gain.

## Add LowLight to NVG's

Any NVG can be registered to support LowLight:

```cpp
class CfgWeapons {
    class Aux501_SomeNVG 
    {
        Aux501_LowLight = 1;
    };
};
```
This will add Low Light to the NVG, Keybinds and Default Gain / Brightness are configurable in addon settings under Aux501 > LowLight.

Smaller number = brighter. 