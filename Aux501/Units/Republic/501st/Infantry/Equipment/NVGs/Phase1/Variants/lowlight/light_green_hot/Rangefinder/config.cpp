class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_light_green_hot_lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_light_green_hot_lowlight"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_light_green_hot_lowlight",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_white_light_green_hot_lowlight"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        class ItemInfo;
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_light_green_hot_lowlight
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 05 - Rangefinder - P1";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_range_ui_ca.paa";
        model = "\JLTS_AE\SEA_Rangefinder_Off.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\JLTS_AE\Data\BaseTextures\Rangefinder\Rangefinder_black.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\JLTS_AE\SEA_Rangefinder_On.p3d";
            modelOff = "\JLTS_AE\SEA_Rangefinder_Off.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase1";
            color = "black";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_white_light_green_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_light_green_hot_lowlight
    {
        displayName = "[501st] INF P1 Rangefinder 02 - White";
        hiddenSelectionsTextures[] = {"\JLTS_AE\Data\BaseTextures\Rangefinder\Rangefinder_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase1";
            color = "white";
            nvg_type = "lowlight";
            ti_color = "light_green_hot";
        };
    };
};