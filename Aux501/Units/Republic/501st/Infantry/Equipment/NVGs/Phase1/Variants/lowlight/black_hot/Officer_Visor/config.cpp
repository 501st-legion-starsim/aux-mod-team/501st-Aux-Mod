class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase1_Officer_Visor_black_hot_lowlight
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_black_hot_lowlight"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_P1_Off_visor_black_hot_lowlight"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_black_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI
    {
        class ItemInfo;
    };

    //Normal Officer Visor
    class Aux501_Republic_501_Infantry_Equipment_NVGs_P1_Off_visor_black_hot_lowlight: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_black_hot_lowlight
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 06 - Officer - P1";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_cc_visor_ui_ca.paa";
        model = "\SWLB_clones\SWLB_clone_ccVisor.p3d";
        hiddenSelections[] = {"camo1","camo2"};
		hiddenSelectionsTextures[] = {"\SWLB_clones\data\SWLB_clone_nvg_co.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1","camo2"};
            uniformmodel = "\SWLB_clones\SWLB_clone_ccVisor.p3d";
            modelOff = "\SWLB_clones\SWLB_clone_ccVisor.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Officer_Visor";
            style = "phase1";
            variant = "black";
            nvg_type = "lowlight";
            ti_color = "black_hot";
        };
    };
};