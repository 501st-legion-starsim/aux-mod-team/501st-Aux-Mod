class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_green_hot
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_NVGs_Phase2_green_hot"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_green_hot",
            "Aux501_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_white_green_hot"
        };
    };
};

class CfgWeapons
{
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_hot;

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_green_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_white_hot
    {
        class ItemInfo;
    };

    class Aux501_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_green_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_green_hot
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[501st] INF NVG 05 - Rangefinder - P1";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_nvg_range_ui_ca.paa";
        model = "\JLTS_AE\SEA_Rangefinder_Off.p3d";
        hiddenSelections[] = {"Camo1"};
        hiddenSelectionsTextures[] = {"\JLTS_AE\Data\BaseTextures\Rangefinder\Rangefinder_black.paa"};
        class ItemInfo: ItemInfo
        {
            hiddenSelections[] = {"camo1"};
            uniformModel = "\JLTS_AE\SEA_Rangefinder_On.p3d";
            modelOff = "\JLTS_AE\SEA_Rangefinder_Off.p3d";
            mass = 20;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase1";
            color = "black";
            nvg_type = "standard_nv";
            ti_color = "green_hot";
        };
    };
    class Aux501_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_white_green_hot: Aux501_Republic_501_Infantry_Equipment_NVGs_Phase1_Rangefinder_green_hot
    {
        displayName = "[501st] INF P1 Rangefinder 02 - White";
        hiddenSelectionsTextures[] = {"\JLTS_AE\Data\BaseTextures\Rangefinder\Rangefinder_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_NVGs_Rangefinder";
            variant = "phase1";
            color = "white";
            nvg_type = "standard_nv";
            ti_color = "green_hot";
        };
    };
};