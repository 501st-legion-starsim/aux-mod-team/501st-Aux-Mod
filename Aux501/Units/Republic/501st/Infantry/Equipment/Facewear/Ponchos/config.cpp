class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Ponchos
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = {};
        weapons[] = {};
    };
};

class XtdGearModels
{
    class CfgGlasses
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_FW_Ponchos
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "hud",
                "colorblind",
                "variant"
            };
            class hud
            {
                label = "HUD";
                values[] = 
                {
                    "hud_on",
                    "hud_off"
                };
                class hud_on
                {
                    label = "Hud On";
                };
                class hud_off
                {
                    label = "Hud Off";
                };
            };
            class colorblind
            {
                label = "Filter";
                values[] = 
                {
                    "clear",
                    "no_blue",
                    "no_red",
                    "no_green"
                };
                class clear
                {
                    label = "Default";
                };
                class no_blue
                {
                    label = "No Blue";
                };
                class no_red
                {
                    label = "No Red";
                };
                class no_green
                {
                    label = "No Green";
                };
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "black",
                    "gray",
                    "gray_yellow",
                    "blue",
                    "blue_gray",
                    "red",
                    "red_gray",
                    "green",
                    "brown"
                };
                class black
                {
                    label = "Black";
                };
                class gray
                {
                    label = "Gray";
                };
                class gray_yellow
                {
                    label = "Gray/Yellow";
                };
                class blue
                {
                    label = "Blue";
                };
                class blue_gray
                {
                    label = "Blue/Gray";
                };
                class red
                {
                    label = "Red";
                };
                class red_gray
                {
                    label = "Red/Gray";
                };
                class green
                {
                    label = "Green";
                };
                class brown
                {
                    label = "Brown";
                };                
            };
        };
    };
};