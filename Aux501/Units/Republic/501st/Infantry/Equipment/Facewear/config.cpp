class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class G_Diving;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display: G_Diving
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        displayname = "[501st] FW HUD P2 00";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_interior_ui_ca.paa";
        model = "\A3\Weapons_f\DummyNVG";
        ACE_Overlay = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\phase2_facewear_ca.paa";
        ACE_OverlayCracked = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\phase2_facewear_ca.paa";
        ACE_OverlayDirt = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\phase2_facewear_ca.paa";
        ACE_Resistance = 2;
        ACE_Protection = 1;
        mass = 2;
        identityTypes[] = {};
        mode = 4;
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW";
            hud = "hud_on";
            colorblind = "clear";
        };
    };
    
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW HUD P2 01 - Blue";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\UI\no_blue_ca.paa";
        ace_color[] = {0.0,0.0,-200.0};
		ace_tintAmount = 1;
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW";
            hud = "hud_on";
            colorblind = "no_blue";
        };

    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUD P2 02 - Red";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\UI\no_red_ca.paa";
        ace_color[] = {-150.0,0.0,0.0};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW";
            hud = "hud_on";
            colorblind = "no_red";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUD P2 03 - Green";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\UI\no_green_ca.paa";
        ace_color[] = {0.0,-100.0,0.0};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW";
            hud = "hud_on";
            colorblind = "no_green";
        };
    };

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW HUDless Display P2 00";
        ACE_Overlay = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        ACE_OverlayCracked = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        ACE_OverlayDirt = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW";
            hud = "hud_off";
            colorblind = "clear";
        };
    };

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUDless Display P2 01 - Blue";
        ACE_Overlay = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        ACE_OverlayCracked = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        ACE_OverlayDirt = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW";
            hud = "hud_off";
            colorblind = "no_blue";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] FW HUDless Display P2 02 - Red";
        ACE_Overlay = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        ACE_OverlayCracked = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        ACE_OverlayDirt = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW";
            hud = "hud_off";
            colorblind = "no_red";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] FW HUDless Display P2 03 - Green";
        ACE_Overlay = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        ACE_OverlayCracked = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        ACE_OverlayDirt = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\empty_facewear_ca.paa";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW";
            hud = "hud_off";
            colorblind = "no_green";
        };
    };
};

class XtdGearModels
{
    class CfgGlasses
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_FW
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "hud",
                "colorblind",
            };
            class hud
            {
                label = "Hud";
                values[] = 
                {
                    "hud_on",
                    "hud_off"
                };
                class hud_on
                {
                    label = "Hud On";
                };
                class hud_off
                {
                    label = "Hud Off";
                };
            };
            class colorblind
            {
                label = "Filter";
                values[] = 
                {
                    "clear",
                    "no_blue",
                    "no_red",
                    "no_green"
                };
                class clear
                {
                    label = "Default";
                };
                class no_blue
                {
                    label = "No Blue";
                };
                class no_red
                {
                    label = "No Red";
                };
                class no_green
                {
                    label = "No Green";
                };
            };
        };
    };
};