class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Kamas
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = {};
        weapons[] = {};
    };
};

class XtdGearModels
{
    class CfgGlasses
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Kamas
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "hud",
                "colorblind",     
                "mos",
                "rank"
            };
            class hud
            {
                label = "HUD";
                values[] = 
                {
                    "hud_on",
                    "hud_off"
                };
                class hud_on
                {
                    label = "Hud On";
                };
                class hud_off
                {
                    label = "Hud Off";
                };
            };
            class colorblind
            {
                label = "Filter";
                values[] = 
                {
                    "clear",
                    "no_blue",
                    "no_red",
                    "no_green"
                };
                class clear
                {
                    label = "Default";
                };
                class no_blue
                {
                    label = "No Blue";
                };
                class no_red
                {
                    label = "No Red";
                };
                class no_green
                {
                    label = "No Green";
                };
            };
            class mos
            {
                label = "MOS";
                values[] = 
                {
                    "infantry",
                    "medic",
                    "rto"
                };
                class infantry
                {
                    label = "GI";
                };
                class medic
                {
                    label = "Medic";
                };
                class rto
                {
                    label = "RTO";
                };
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "cs",
                    "sr_cs",
                    "sr_csm"
                };
                class cs
                {
                    label = "CS+";
                };
                class sr_cs
                {
                    label = "Sr. CS+";
                };
                class sr_csm
                {
                    label = "CO CS-M+";
                };
            };
        };
    };
};