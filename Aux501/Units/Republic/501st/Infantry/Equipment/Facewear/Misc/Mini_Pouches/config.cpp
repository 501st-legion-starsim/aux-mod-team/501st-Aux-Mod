class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Mini_Pouches
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removegreen",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud;

    //Hud//

    //Suspenders
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW HUD Pouch 01";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Mini_Pouches\data\textures\UI\Aux501_FW_mini_pouch_ui_ca.paa";
        model = "kobra\442_equipment\vests\model\clone\scout_pouches.p3d";
        hiddenselections[] = {"pouch1","pouch2","pouch3"};
        hiddenselectionstextures[] = {"kobra\442_equipment\vests\data\scout\pouches_co.paa","kobra\442_equipment\vests\data\scout\pouches_co.paa",""};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "clear";
            variant = "mini";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUD Pouch 01 - Blue";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Mini_Pouches\data\textures\UI\Aux501_FW_mini_pouch_ui_ca.paa";
        model = "kobra\442_equipment\vests\model\clone\scout_pouches.p3d";
        hiddenselections[] = {"pouch1","pouch2","pouch3"};
        hiddenselectionstextures[] = {"kobra\442_equipment\vests\data\scout\pouches_co.paa","kobra\442_equipment\vests\data\scout\pouches_co.paa",""};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "no_blue";
            variant = "mini";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] FW HUD Pouch 01 - Red";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Mini_Pouches\data\textures\UI\Aux501_FW_mini_pouch_ui_ca.paa";
        model = "kobra\442_equipment\vests\model\clone\scout_pouches.p3d";
        hiddenselections[] = {"pouch1","pouch2","pouch3"};
        hiddenselectionstextures[] = {"kobra\442_equipment\vests\data\scout\pouches_co.paa","kobra\442_equipment\vests\data\scout\pouches_co.paa",""};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "no_red";
            variant = "mini";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] FW HUD Pouch 01 - Green";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Mini_Pouches\data\textures\UI\Aux501_FW_mini_pouch_ui_ca.paa";
        model = "kobra\442_equipment\vests\model\clone\scout_pouches.p3d";
        hiddenselections[] = {"pouch1","pouch2","pouch3"};
        hiddenselectionstextures[] = {"kobra\442_equipment\vests\data\scout\pouches_co.paa","kobra\442_equipment\vests\data\scout\pouches_co.paa",""};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "no_green";
            variant = "mini";
        };
    };
    
    //No Hud//

    //Suspenders
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud
    {
        displayname = "[501st] FW HUDless POUCH 01";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Mini_Pouches\data\textures\UI\Aux501_FW_mini_pouch_ui_ca.paa";
        model = "kobra\442_equipment\vests\model\clone\scout_pouches.p3d";
        hiddenselections[] = {"pouch1","pouch2","pouch3"};
        hiddenselectionstextures[] = {"kobra\442_equipment\vests\data\scout\pouches_co.paa","kobra\442_equipment\vests\data\scout\pouches_co.paa",""};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "clear";
            variant = "mini";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud
    {
        displayname = "[501st] FW HUDless POUCH 01 - Blue";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Mini_Pouches\data\textures\UI\Aux501_FW_mini_pouch_ui_ca.paa";
        model = "kobra\442_equipment\vests\model\clone\scout_pouches.p3d";
        hiddenselections[] = {"pouch1","pouch2","pouch3"};
        hiddenselectionstextures[] = {"kobra\442_equipment\vests\data\scout\pouches_co.paa","kobra\442_equipment\vests\data\scout\pouches_co.paa",""};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "no_blue";
            variant = "mini";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless POUCH 01 - Red";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Mini_Pouches\data\textures\UI\Aux501_FW_mini_pouch_ui_ca.paa";
        model = "kobra\442_equipment\vests\model\clone\scout_pouches.p3d";
        hiddenselections[] = {"pouch1","pouch2","pouch3"};
        hiddenselectionstextures[] = {"kobra\442_equipment\vests\data\scout\pouches_co.paa","kobra\442_equipment\vests\data\scout\pouches_co.paa",""};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "no_red";
            variant = "mini";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Mini_Pouches_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless POUCH 01 - Green";
        picture = "\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\Misc\Mini_Pouches\data\textures\UI\Aux501_FW_mini_pouch_ui_ca.paa";
        model = "kobra\442_equipment\vests\model\clone\scout_pouches.p3d";
        hiddenselections[] = {"pouch1","pouch2","pouch3"};
        hiddenselectionstextures[] = {"kobra\442_equipment\vests\data\scout\pouches_co.paa","kobra\442_equipment\vests\data\scout\pouches_co.paa",""};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "no_green";
            variant = "mini";
        };
    };
};