class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Misc
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = {};
        weapons[] = {};
    };
};

class XtdGearModels
{
    class CfgGlasses
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "hud",
                "colorblind",     
                "variant"
            };
            class hud
            {
                label = "HUD";
                values[] = 
                {
                    "hud_on",
                    "hud_off"
                };
                class hud_on
                {
                    label = "Hud On";
                };
                class hud_off
                {
                    label = "Hud Off";
                };
            };
            class colorblind
            {
                label = "Filter";
                values[] = 
                {
                    "clear",
                    "no_blue",
                    "no_red",
                    "no_green"
                };
                class clear
                {
                    label = "Default";
                };
                class no_blue
                {
                    label = "No Blue";
                };
                class no_red
                {
                    label = "No Red";
                };
                class no_green
                {
                    label = "No Green";
                };
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "mini",
                    "front",
                    "sash",
                    "antenna"
                };
                class mini
                {
                    label = "Mini";
                };
                class front
                {
                    label = "Front";
                };
                class sash
                {
                    label = "Sash";
                };
                class antenna
                {
                    label = "Antenna";
                };
            };
        };
    };
};