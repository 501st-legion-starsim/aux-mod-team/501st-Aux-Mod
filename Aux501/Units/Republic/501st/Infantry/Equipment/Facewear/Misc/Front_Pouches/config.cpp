class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Front_Pouches
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removegreen",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud;

    //Hud//

    //Suspenders
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW HUD Pouch 02";
        picture = "\ls_armor_bluefor\vest\_ui\icon_lsd_orsf_trooper_vest_ca.paa";
        model = "ls_armor_bluefor\vest\orsf\trooper\ls_orsf_trooper_vest.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\front_pouches_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "clear";
            variant = "front";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUD Pouch 02 - Blue";
        picture = "\ls_armor_bluefor\vest\_ui\icon_lsd_orsf_trooper_vest_ca.paa";
        model = "ls_armor_bluefor\vest\orsf\trooper\ls_orsf_trooper_vest.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\front_pouches_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "no_blue";
            variant = "front";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] FW HUD Pouch 02 - Red";
        picture = "\ls_armor_bluefor\vest\_ui\icon_lsd_orsf_trooper_vest_ca.paa";
        model = "ls_armor_bluefor\vest\orsf\trooper\ls_orsf_trooper_vest.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\front_pouches_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "no_red";
            variant = "front";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] FW HUD Pouch 02 - Green";
        picture = "\ls_armor_bluefor\vest\_ui\icon_lsd_orsf_trooper_vest_ca.paa";
        model = "ls_armor_bluefor\vest\orsf\trooper\ls_orsf_trooper_vest.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\front_pouches_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_on";
            colorblind = "no_green";
            variant = "front";
        };
    };
    
    //No Hud//

    //Suspenders
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud
    {
        displayname = "[501st] FW HUDless POUCH 02";
        picture = "\ls_armor_bluefor\vest\_ui\icon_lsd_orsf_trooper_vest_ca.paa";
        model = "ls_armor_bluefor\vest\orsf\trooper\ls_orsf_trooper_vest.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\front_pouches_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "clear";
            variant = "front";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud
    {
        displayname = "[501st] FW HUDless POUCH 02 - Blue";
        picture = "\ls_armor_bluefor\vest\_ui\icon_lsd_orsf_trooper_vest_ca.paa";
        model = "ls_armor_bluefor\vest\orsf\trooper\ls_orsf_trooper_vest.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\front_pouches_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "no_blue";
            variant = "front";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless POUCH 02 - Red";
        picture = "\ls_armor_bluefor\vest\_ui\icon_lsd_orsf_trooper_vest_ca.paa";
        model = "ls_armor_bluefor\vest\orsf\trooper\ls_orsf_trooper_vest.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\front_pouches_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "no_red";
            variant = "front";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless POUCH 02 - Green";
        picture = "\ls_armor_bluefor\vest\_ui\icon_lsd_orsf_trooper_vest_ca.paa";
        model = "ls_armor_bluefor\vest\orsf\trooper\ls_orsf_trooper_vest.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Equipment\Facewear\data\textures\front_pouches_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Misc";
            hud = "hud_off";
            colorblind = "no_green";
            variant = "front";
        };
    };
};