class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Med_Equipment_Facewear_Jr_NCOs_Sr_CP
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removeblue",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removered",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removegreen",

            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removeblue",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removered",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removegreen",

            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_nohud",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removegreen_nohud",

            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_nohud",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removegreen_nohud;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removegreen_nohud;

    //Hud//

    //Sr. CP
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "medic";
            rank = "sr_cp";
            variant = "standard";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removeblue
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "medic";
            rank = "sr_cp";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removered
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "medic";
            rank = "sr_cp";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removegreen
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "medic";
            rank = "sr_cp";
            variant = "standard";
        };
    };

    //Sr. CP - Bag
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "medic";
            rank = "sr_cp";
            variant = "bag";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removeblue
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "medic";
            rank = "sr_cp";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removered
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "medic";
            rank = "sr_cp";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removegreen
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "medic";
            rank = "sr_cp";
            variant = "bag";
        };
    };
    
    //No Hud//

    //Sr. CP
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_nohud
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "medic";
            rank = "sr_cp";
            variant = "standard";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removeblue_nohud
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "medic";
            rank = "sr_cp";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removered_nohud
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "medic";
            rank = "sr_cp";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_removegreen_nohud
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "medic";
            rank = "sr_cp";
            variant = "standard";
        };
    };

    //Sr. CP - Bag
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "medic";
            rank = "sr_cp";
            variant = "bag";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removeblue_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "medic";
            rank = "sr_cp";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removered_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "medic";
            rank = "sr_cp";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Sr_CP_bag_removegreen_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "medic";
            rank = "sr_cp";
            variant = "bag";
        };
    };
};