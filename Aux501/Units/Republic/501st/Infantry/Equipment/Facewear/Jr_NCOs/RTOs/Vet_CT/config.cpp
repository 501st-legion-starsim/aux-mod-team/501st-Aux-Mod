class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Jr_NCOs_Vet_Trooper
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_removeblue",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_removered",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_removegreen",

            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removeblue",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removered",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removegreen",

            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_removegreen_nohud",

            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removegreen_nohud;

    //Hud//

    //Veteran Trooper - Bag
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "rto";
            rank = "vet_ct";
            variant = "bag";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removeblue
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "rto";
            rank = "vet_ct";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removered
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "rto";
            rank = "vet_ct";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removegreen
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "rto";
            rank = "vet_ct";
            variant = "bag";
        };
    };
    
    //No Hud//

    //Veteran Trooper - Bag
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "rto";
            rank = "vet_ct";
            variant = "bag";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removeblue_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "rto";
            rank = "vet_ct";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removered_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "rto";
            rank = "vet_ct";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Vet_Trooper_bag_removegreen_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "rto";
            rank = "vet_ct";
            variant = "bag";
        };
    };
};