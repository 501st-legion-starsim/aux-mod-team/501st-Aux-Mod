class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Jr_NCOs_CLC
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removegreen",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removegreen_nohud",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud;

    //Hud//

    //Veteran Trooper
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW HUD VEST 05 - CLC";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "infantry";
            rank = "clc";
            variant = "standard";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUD VEST 05 - CLC - Blue";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "infantry";
            rank = "clc";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] FW HUD VEST 05 - CLC - Red";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "infantry";
            rank = "clc";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] FW HUD VEST 05 - CLC - Green";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "infantry";
            rank = "clc";
            variant = "standard";
        };
    };

    //Veteran Trooper - Bag
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC
    {
        displayname = "[501st] FW HUD VEST 05 - CLC";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "infantry";
            rank = "clc";
            variant = "bag";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removeblue
    {
        displayname = "[501st] FW HUD VEST 05 - CLC - Blue";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "infantry";
            rank = "clc";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removered
    {
        displayname = "[501st] FW HUD VEST 05 - CLC - Red";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "infantry";
            rank = "clc";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removegreen
    {
        displayname = "[501st] FW HUD VEST 05 - CLC - Green";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "infantry";
            rank = "clc";
            variant = "bag";
        };
    };
    
    //No Hud//

    //Veteran Trooper
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud
    {
        displayname = "[501st] FW HUDless VEST 05 - CLC";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "infantry";
            rank = "clc";
            variant = "standard";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud
    {
        displayname = "[501st] FW HUDless VEST 05 - CLC - Blue";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "infantry";
            rank = "clc";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless VEST 05 - CLC - Red";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "infantry";
            rank = "clc";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless VEST 05 - CLC - Green";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestRecon.p3d";
        hiddenSelections[] = {"camo2"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "infantry";
            rank = "clc";
            variant = "standard";
        };
    };

    //Veteran Trooper - Bag
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_nohud
    {
        displayname = "[501st] FW HUDless VEST 05 - CLC";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "infantry";
            rank = "clc";
            variant = "bag";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removeblue_nohud
    {
        displayname = "[501st] FW HUDless VEST 05 - CLC - Blue";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "infantry";
            rank = "clc";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removered_nohud
    {
        displayname = "[501st] FW HUDless VEST 05 - CLC - Red";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "infantry";
            rank = "clc";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removegreen_nohud
    {
        displayname = "[501st] FW HUDless VEST 05 - CLC - Green";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "infantry";
            rank = "clc";
            variant = "bag";
        };
    };
};