class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Jr_NCOs_CLC
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removeblue",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removered",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removegreen",

            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removered",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen",

            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removegreen_nohud",

            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removegreen_nohud;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen_nohud;

    //Hud//

    //CLC
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "rto";
            rank = "clc";
            variant = "standard";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removeblue
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "rto";
            rank = "clc";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removered
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "rto";
            rank = "clc";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removegreen
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "rto";
            rank = "clc";
            variant = "standard";
        };
    };

    //CLC - Bag
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "rto";
            rank = "clc";
            variant = "bag";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "rto";
            rank = "clc";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removered
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "rto";
            rank = "clc";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "rto";
            rank = "clc";
            variant = "bag";
        };
    };
    
    //No Hud//

    //CLC
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_nohud
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "rto";
            rank = "clc";
            variant = "standard";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removeblue_nohud
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "rto";
            rank = "clc";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removered_nohud
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "rto";
            rank = "clc";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_removegreen_nohud
    {
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "rto";
            rank = "clc";
            variant = "standard";
        };
    };

    //CLC - Bag
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "rto";
            rank = "clc";
            variant = "bag";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removeblue_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "rto";
            rank = "clc";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removered_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "rto";
            rank = "clc";
            variant = "bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_RTO_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_CLC_bag_removegreen_nohud
    {
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "rto";
            rank = "clc";
            variant = "bag";
        };
    };
};