class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Heavy_Bag_Standard
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removeblue",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removered",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removegreen",

            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removeblue_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removered_nohud",
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud;

    //Hud//

    //Veteran Trooper
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] FW HUD Bag 01 - Standard";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_medic_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_airborne_armor.p3d";
        hiddenSelections[] = {"camo1","camo2","ammo","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_on";
            colorblind = "clear";
            variant = "standard";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] FW HUD Bag 01 - Blue";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_medic_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_airborne_armor.p3d";
        hiddenSelections[] = {"camo1","camo2","ammo","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_on";
            colorblind = "no_blue";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] FW HUD Bag 01 - Red";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_medic_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_airborne_armor.p3d";
        hiddenSelections[] = {"camo1","camo2","ammo","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_on";
            colorblind = "no_red";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] FW HUD Bag 01 - Green";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_medic_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_airborne_armor.p3d";
        hiddenSelections[] = {"camo1","camo2","ammo","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_on";
            colorblind = "no_green";
            variant = "standard";
        };
    };
    
    //No Hud//

    //Veteran Trooper
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud
    {
        displayname = "[501st] FW HUDless Bag 01 -  Standard";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_medic_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_airborne_armor.p3d";
        hiddenSelections[] = {"camo1","camo2","ammo","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_off";
            colorblind = "clear";
            variant = "standard";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud
    {
        displayname = "[501st] FW HUDless Bag 01 - Blue";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_medic_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_airborne_armor.p3d";
        hiddenSelections[] = {"camo1","camo2","ammo","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_off";
            colorblind = "no_blue";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless Bag 01 - Red";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_medic_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_airborne_armor.p3d";
        hiddenSelections[] = {"camo1","camo2","ammo","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_off";
            colorblind = "no_red";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Heavy_Bag_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] FW HUDless Bag 01 - Green";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_medic_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_airborne_armor.p3d";
        hiddenSelections[] = {"camo1","camo2","ammo","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "\SWLB_clones\data\heavy_accessories_co.paa",
            "",
            "",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Heavy_Bag";
            hud = "hud_off";
            colorblind = "no_green";
            variant = "standard";
        };
    };
};