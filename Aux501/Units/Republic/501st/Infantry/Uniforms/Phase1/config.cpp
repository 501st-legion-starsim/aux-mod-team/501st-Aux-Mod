class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase1
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2",
            "Aux501_Patch_Units_Republic_501_Infantry"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_P1_Blue_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_P1_sgt_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_P1_Lt_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_P1_captain_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_P1_commander_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_P1_Blue_Uniform",
            "Aux501_Units_Republic_501st_P1_sgt_Uniform",
            "Aux501_Units_Republic_501st_P1_Lt_Uniform",
            "Aux501_Units_Republic_501st_P1_captain_Uniform",
            "Aux501_Units_Republic_501st_P1_commander_Uniform"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_P1_Blue_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] INF P1 ARMR 01 - 501st";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_P1_Blue_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Uniforms";
            variant = "FiveOhFirst";
        };
    };
    class Aux501_Units_Republic_501st_P1_sgt_Uniform: Aux501_Units_Republic_501st_P1_Blue_Uniform
    {
        displayName = "[501st] INF P1 ARMR 02 - Sgt.";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_P1_sgt_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Uniforms";
            variant = "sgt";
        };
    };
    class Aux501_Units_Republic_501st_P1_Lt_Uniform: Aux501_Units_Republic_501st_P1_Blue_Uniform
    {
        displayName = "[501st] INF P1 ARMR 03 - Lt.";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_P1_Lt_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Uniforms";
            variant = "lt";
        };
    };
    class Aux501_Units_Republic_501st_P1_captain_Uniform: Aux501_Units_Republic_501st_P1_Blue_Uniform
    {
        displayName = "[501st] INF P1 ARMR 04 - Captain";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_P1_captain_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Uniforms";
            variant = "captain";
        };
    };
    class Aux501_Units_Republic_501st_P1_commander_Uniform: Aux501_Units_Republic_501st_P1_Blue_Uniform
    {
        displayName = "[501st] INF P1 ARMR 05 - Commander";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_P1_commander_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Uniforms";
            variant = "commander";
        };
    };
};

class cfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    class Aux501_Units_Republic_501st_P1_Blue_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase1\data\textures\inf_p1_501st_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase1\data\textures\inf_p1_501st_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_P1_Blue_Uniform";
    };

    class Aux501_Units_Republic_501st_P1_sgt_Uniform_Vehicle: Aux501_Units_Republic_501st_P1_Blue_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_sergeant_armor1_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cr_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_P1_sgt_Uniform";
    };

    class Aux501_Units_Republic_501st_P1_Lt_Uniform_Vehicle: Aux501_Units_Republic_501st_P1_Blue_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase1\data\textures\inf_p1_LT_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cr_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_P1_Lt_Uniform";
    };

    class Aux501_Units_Republic_501st_P1_captain_Uniform_Vehicle: Aux501_Units_Republic_501st_P1_Blue_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_captain_armor1_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cr_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_P1_captain_Uniform";
    };

    class Aux501_Units_Republic_501st_P1_commander_Uniform_Vehicle: Aux501_Units_Republic_501st_P1_Blue_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_commander_armor1_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cr_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_P1_commander_Uniform";
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P1_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "FiveOhFirst",
                    "sgt",
                    "lt",
                    "captain",
                    "commander"
                };
                class FiveOhFirst
                {
                    label = "501st";
                };
                class sgt
                {
                    label = "Sgt.";
                };
                class lt
                {
                    label = "Lt.";
                };
                class captain
                {
                    label = "Captain";
                };
                class commander
                {
                    label = "Commander";
                };
            };
        };
    };
};