class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_RTO_Recon
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Lance_CP_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_CP_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_CP_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_CS_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_CS_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Battalion_CSM_Recon_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_Recon_Uniform",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_Recon_Uniform",
            "Aux501_Units_Republic_501st_RTO_Lance_CP_Recon_Uniform",

            "Aux501_Units_Republic_501st_RTO_CP_Recon_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_CP_Recon_Uniform",
            "Aux501_Units_Republic_501st_RTO_CS_Recon_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_CS_Recon_Uniform",
            
            "Aux501_Units_Republic_501st_RTO_Battalion_CSM_Recon_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Trooper_Uniform;

    class Aux501_Units_Republic_501st_RTO_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 02 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "recon";
            rank = "ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_Recon_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 03 - Sr. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "recon";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_Recon_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 04 - Vet. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "recon";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Lance_CP_Recon_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 05 - CLC";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Lance_CP_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "recon";
            rank = "clc";
        };
    };
    
    //NCOs
    class Aux501_Units_Republic_501st_RTO_CP_Recon_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 06 - CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_CP_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "recon";
            rank = "cp";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_CP_Recon_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 07 - Sr. CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_CP_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "recon";
            rank = "sr_cp";
        };
    };
    class Aux501_Units_Republic_501st_RTO_CS_Recon_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 08 - CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_CS_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "recon";
            rank = "cs";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_CS_Recon_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 09 - Sr. CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_CS_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "recon";
            rank = "sr_cs";
        };
    };
    
    //CS-Ms
    class Aux501_Units_Republic_501st_RTO_Battalion_CSM_Recon_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 12 - BN CS-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Battalion_CSM_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "recon";
            rank = "b_csm";
        };
    };
    
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Trooper_Recon_Uniform_Vehicle;

    class Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Lance_CP_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_clc_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_clc_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Lance_CP_Recon_Uniform";
    };
    
    //NCOs
    class Aux501_Units_Republic_501st_RTO_CP_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_cp_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cp_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_CP_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_CP_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_cp_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_cp_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_CP_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_CS_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_cs_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cp_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_CS_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_CS_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_cs_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_cs_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_CS_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Battalion_CSM_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Trooper_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_bn_csm_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_bn_csm_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Battalion_CSM_Recon_Uniform";
    };
};