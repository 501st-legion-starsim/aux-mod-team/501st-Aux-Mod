class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Recon_Uniforms_Phase2_SquadNumbers
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_1_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_2_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_3_Support_Uniform_Vehicle",

            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_1_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_2_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_3_Support_Uniform_Vehicle",

            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_1_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_2_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_3_Support_Uniform_Vehicle",

            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_1_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_2_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_3_Support_Uniform_Vehicle",

            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_1_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_2_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_3_Support_Uniform_Vehicle",

            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_1_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_2_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_3_Support_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_1_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_2_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_3_Support_Uniform",

            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_1_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_2_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_3_Support_Uniform",

            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_1_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_2_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_3_Support_Uniform",

            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_1_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_2_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_3_Support_Uniform",

            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_1_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_2_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_3_Support_Uniform",

            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_1_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_2_Support_Uniform",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_3_Support_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_RTO_Trooper_Support_Uniform;

    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Support_Uniform
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform: Aux501_Units_Republic_501st_RTO_Trooper_Support_Uniform
    {
        class ItemInfo;
    };

    //Senior Trooper - 1st Platoon
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_1_Support_Uniform: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_1_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_1";
            mos = "rto";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_2_Support_Uniform: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_2_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_2";
            mos = "rto";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_3_Support_Uniform: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_3_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_3";
            mos = "rto";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };
    
    //Senior Trooper - 2nd Platoon
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_1_Support_Uniform: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_1_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_1";
            mos = "rto";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_2_Support_Uniform: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_2_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_2";
            mos = "rto";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_3_Support_Uniform: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_3_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_3";
            mos = "rto";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };

    //Senior Trooper - 3rd Platoon
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_1_Support_Uniform: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_1_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_1";
            mos = "rto";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_2_Support_Uniform: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_2_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_2";
            mos = "rto";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_3_Support_Uniform: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_3_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_3";
            mos = "rto";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };
    
    //Veteran Trooper - 1st Platoon
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_1_Support_Uniform: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_1_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_1";
            mos = "rto";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_2_Support_Uniform: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_2_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_2";
            mos = "rto";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_3_Support_Uniform: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_3_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_3";
            mos = "rto";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };

    //Veteran Trooper - 2nd Platoon
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_1_Support_Uniform: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_1_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_1";
            mos = "rto";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_2_Support_Uniform: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_2_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_2";
            mos = "rto";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_3_Support_Uniform: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_3_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_3";
            mos = "rto";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };

    //Veteran Trooper - 3rd Platoon
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_1_Support_Uniform: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_1_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_1";
            mos = "rto";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_2_Support_Uniform: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_2_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_2";
            mos = "rto";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_3_Support_Uniform: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_3_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_3";
            mos = "rto";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform_Vehicle;
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform_Vehicle;

    //Senior Trooper - 1st Platoon
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_1_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_1_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_1_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_2_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_1_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_2_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_3_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_1_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_1_3_Support_Uniform";
    };

    //Senior Trooper - 2nd Platoon
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_1_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_2_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_1_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_2_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_2_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_2_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_3_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_2_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_2_3_Support_Uniform";
    };

    //Senior Trooper - 3rd Platoon
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_1_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_3_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_1_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_2_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_3_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_2_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_3_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Snr_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_3_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_3_3_Support_Uniform";
    };

    //Veteran Trooper - 1st Platoon
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_1_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_1_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_1_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_2_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_1_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_2_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_3_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_1_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_1_3_Support_Uniform";
    };

    //Veteran Trooper - 2nd Platoon
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_1_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_2_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_1_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_2_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_2_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_2_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_3_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_2_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_2_3_Support_Uniform";
    };

    //Veteran Trooper - 3rd Platoon
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_1_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_3_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_1_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_2_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_3_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_2_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_3_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_RTO_Vet_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_3_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_3_3_Support_Uniform";
    };
};