class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_RTO
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_RTO_Cadet_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Lance_CP_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_CP_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_RTO_Snr_CS_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_RTO_Cadet_Uniform",
            "Aux501_Units_Republic_501st_RTO_Trooper_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_Trooper_Uniform",
            "Aux501_Units_Republic_501st_RTO_Vet_Trooper_Uniform",
            "Aux501_Units_Republic_501st_RTO_Lance_CP_Uniform",
            "Aux501_Units_Republic_501st_RTO_CP_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_CP_Uniform",
            "Aux501_Units_Republic_501st_RTO_CS_Uniform",
            "Aux501_Units_Republic_501st_RTO_Snr_CS_Uniform",
            "Aux501_Units_Republic_501st_RTO_CSM_Uniform"
        };
    };
};

class CfgWeapons
{    
    class Aux501_Units_Republic_501st_Uniform_Base;

    class Aux501_Units_Republic_501st_Standard_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        class itemInfo;
    };
    class Aux501_Units_Republic_501st_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_RTO_Cadet_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] RTO P2 ARMR 01 - CR-C";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Cadet_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "standard";
            rank = "cr_c";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 02 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "standard";
            rank = "ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 03 - Sr. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 04 - Vet. CT ";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "standard";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Lance_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 05 - CI-L";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Lance_CP_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "standard";
            rank = "clc";
        };
    }; 
    
    //NCOs
    class Aux501_Units_Republic_501st_RTO_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 06 - CI-P";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_CP_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "standard";
            rank = "cp";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 07 - Sr. CI-P";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_CP_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "standard";
            rank = "sr_cp";
        };
    };
    class Aux501_Units_Republic_501st_RTO_CS_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 08 - CI-S";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_CS_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "standard";
            rank = "cs";
        };
    };
    class Aux501_Units_Republic_501st_RTO_Snr_CS_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 09 - Sr. CI-S";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_CS_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "standard";
            rank = "sr_cs";
        };
    };
    
    //CSM
    class Aux501_Units_Republic_501st_RTO_Battalion_CSM_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] RTO P2 ARMR 10 - CI-O";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_RTO_Battalion_CSM_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "rto";
            style = "standard";
            rank = "b_csm";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    //Trooper
    class Aux501_Units_Republic_501st_RTO_Cadet_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_crc_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_crc_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Cadet_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_Trooper_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_Trooper_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Vet_Trooper_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_vet_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Vet_Trooper_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Lance_CP_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] =
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_clc_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_clc_armor_lower_co.paa"
        }; 
        uniformClass = "Aux501_Units_Republic_501st_RTO_Lance_CP_Uniform";
    };

    //NCO's
    class Aux501_Units_Republic_501st_RTO_Snr_CP_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_cp_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_cp_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_CP_Uniform";
    };
    class Aux501_Units_Republic_501st_RTO_Snr_CS_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_cs_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\RTO\data\textures\inf_rto_snr_cs_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_RTO_Snr_CS_Uniform";
    };
};