class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2_SquadNumbers
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Snr_Trooper_1_1_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Snr_Trooper_1_2_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Snr_Trooper_1_3_Uniform_Vehicle",

            "Aux501_Units_Republic_501st_Snr_Trooper_2_1_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Snr_Trooper_2_2_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Snr_Trooper_2_3_Uniform_Vehicle",

            "Aux501_Units_Republic_501st_Snr_Trooper_3_1_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Snr_Trooper_3_2_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Snr_Trooper_3_3_Uniform_Vehicle",

            "Aux501_Units_Republic_501st_Vet_Trooper_1_1_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Vet_Trooper_1_2_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Vet_Trooper_1_3_Uniform_Vehicle",

            "Aux501_Units_Republic_501st_Vet_Trooper_2_1_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Vet_Trooper_2_2_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Vet_Trooper_2_3_Uniform_Vehicle",

            "Aux501_Units_Republic_501st_Vet_Trooper_3_1_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Vet_Trooper_3_2_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Vet_Trooper_3_3_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Snr_Trooper_1_1_Uniform",
            "Aux501_Units_Republic_501st_Snr_Trooper_1_2_Uniform",
            "Aux501_Units_Republic_501st_Snr_Trooper_1_3_Uniform",

            "Aux501_Units_Republic_501st_Snr_Trooper_2_1_Uniform",
            "Aux501_Units_Republic_501st_Snr_Trooper_2_2_Uniform",
            "Aux501_Units_Republic_501st_Snr_Trooper_2_3_Uniform",

            "Aux501_Units_Republic_501st_Snr_Trooper_3_1_Uniform",
            "Aux501_Units_Republic_501st_Snr_Trooper_3_2_Uniform",
            "Aux501_Units_Republic_501st_Snr_Trooper_3_3_Uniform",

            "Aux501_Units_Republic_501st_Vet_Trooper_1_1_Uniform",
            "Aux501_Units_Republic_501st_Vet_Trooper_1_2_Uniform",
            "Aux501_Units_Republic_501st_Vet_Trooper_1_3_Uniform",

            "Aux501_Units_Republic_501st_Vet_Trooper_2_1_Uniform",
            "Aux501_Units_Republic_501st_Vet_Trooper_2_2_Uniform",
            "Aux501_Units_Republic_501st_Vet_Trooper_2_3_Uniform",

            "Aux501_Units_Republic_501st_Vet_Trooper_3_1_Uniform",
            "Aux501_Units_Republic_501st_Vet_Trooper_3_2_Uniform",
            "Aux501_Units_Republic_501st_Vet_Trooper_3_3_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Trooper_Uniform;

    class Aux501_Units_Republic_501st_Snr_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_Vet_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        class ItemInfo;
    };

    //Senior Trooper - 1st Platoon
    class Aux501_Units_Republic_501st_Snr_Trooper_1_1_Uniform: Aux501_Units_Republic_501st_Snr_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_1_1_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_1";
            mos = "infantry";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_1_2_Uniform: Aux501_Units_Republic_501st_Snr_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_1_2_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_2";
            mos = "infantry";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_1_3_Uniform: Aux501_Units_Republic_501st_Snr_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_1_3_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_3";
            mos = "infantry";
            style = "standard";
            rank = "sr_ct";
        };
    };
    
    //Senior Trooper - 2nd Platoon
    class Aux501_Units_Republic_501st_Snr_Trooper_2_1_Uniform: Aux501_Units_Republic_501st_Snr_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_2_1_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_1";
            mos = "infantry";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_2_2_Uniform: Aux501_Units_Republic_501st_Snr_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_2_2_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_2";
            mos = "infantry";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_2_3_Uniform: Aux501_Units_Republic_501st_Snr_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_2_3_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_3";
            mos = "infantry";
            style = "standard";
            rank = "sr_ct";
        };
    };

    //Senior Trooper - 3rd Platoon
    class Aux501_Units_Republic_501st_Snr_Trooper_3_1_Uniform: Aux501_Units_Republic_501st_Snr_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_3_1_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_1";
            mos = "infantry";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_3_2_Uniform: Aux501_Units_Republic_501st_Snr_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_3_2_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_2";
            mos = "infantry";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_3_3_Uniform: Aux501_Units_Republic_501st_Snr_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_3_3_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_3";
            mos = "infantry";
            style = "standard";
            rank = "sr_ct";
        };
    };
    
    //Veteran Trooper - 1st Platoon
    class Aux501_Units_Republic_501st_Vet_Trooper_1_1_Uniform: Aux501_Units_Republic_501st_Vet_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_1_1_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_1";
            mos = "infantry";
            style = "standard";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_1_2_Uniform: Aux501_Units_Republic_501st_Vet_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_1_2_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_2";
            mos = "infantry";
            style = "standard";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_1_3_Uniform: Aux501_Units_Republic_501st_Vet_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_1_3_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_3";
            mos = "infantry";
            style = "standard";
            rank = "vet_ct";
        };
    };

    //Veteran Trooper - 2nd Platoon
    class Aux501_Units_Republic_501st_Vet_Trooper_2_1_Uniform: Aux501_Units_Republic_501st_Vet_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_2_1_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_1";
            mos = "infantry";
            style = "standard";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_2_2_Uniform: Aux501_Units_Republic_501st_Vet_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_2_2_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_2";
            mos = "infantry";
            style = "standard";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_2_3_Uniform: Aux501_Units_Republic_501st_Vet_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_2_3_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_3";
            mos = "infantry";
            style = "standard";
            rank = "vet_ct";
        };
    };

    //Veteran Trooper - 3rd Platoon
    class Aux501_Units_Republic_501st_Vet_Trooper_3_1_Uniform: Aux501_Units_Republic_501st_Vet_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_3_1_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_1";
            mos = "infantry";
            style = "standard";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_3_2_Uniform: Aux501_Units_Republic_501st_Vet_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_3_2_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_2";
            mos = "infantry";
            style = "standard";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_3_3_Uniform: Aux501_Units_Republic_501st_Vet_Trooper_Uniform
    {
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_3_3_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_3";
            mos = "infantry";
            style = "standard";
            rank = "vet_ct";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    //Senior Trooper - 1st Platoon
    class Aux501_Units_Republic_501st_Snr_Trooper_1_1_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_1_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_1_1_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_1_2_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_1_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_1_2_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_1_3_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_1_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_1_3_Uniform";
    };

    //Senior Trooper - 2nd Platoon
    class Aux501_Units_Republic_501st_Snr_Trooper_2_1_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_2_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_2_1_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_2_2_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_2_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_2_2_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_2_3_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_2_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_2_3_Uniform";
    };

    //Senior Trooper - 3rd Platoon
    class Aux501_Units_Republic_501st_Snr_Trooper_3_1_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_3_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_3_1_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_3_2_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_3_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_3_2_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_3_3_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_3_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_3_3_Uniform";
    };

    //Veteran Trooper - 1st Platoon
    class Aux501_Units_Republic_501st_Vet_Trooper_1_1_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_1_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_1_1_Uniform";
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_1_2_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_1_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_1_2_Uniform";
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_1_3_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_1_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_1_3_Uniform";
    };

    //Veteran Trooper - 2nd Platoon
    class Aux501_Units_Republic_501st_Vet_Trooper_2_1_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_2_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_2_1_Uniform";
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_2_2_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_2_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_2_2_Uniform";
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_2_3_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_2_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_2_3_Uniform";
    };

    //Veteran Trooper - 3rd Platoon
    class Aux501_Units_Republic_501st_Vet_Trooper_3_1_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_3_1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_3_1_Uniform";
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_3_2_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_3_2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_3_2_Uniform";
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_3_3_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_3_3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_3_3_Uniform";
    };
};