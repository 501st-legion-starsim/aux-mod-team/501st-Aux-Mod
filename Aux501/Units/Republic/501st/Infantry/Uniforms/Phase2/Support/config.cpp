class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2_Support
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Snr_Trooper_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Vet_Trooper_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Lance_CP_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_CP_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Snr_CP_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_CS_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Snr_CS_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Platoon_CSM_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Company_CSM_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Battalion_CSM_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_2LT_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_1LT_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Captain_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Major_Support_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Trooper_Support_Uniform",
            "Aux501_Units_Republic_501st_Snr_Trooper_Support_Uniform",
            "Aux501_Units_Republic_501st_Vet_Trooper_Support_Uniform",
            "Aux501_Units_Republic_501st_Lance_CP_Support_Uniform",

            "Aux501_Units_Republic_501st_CP_Support_Uniform",
            "Aux501_Units_Republic_501st_Snr_CP_Support_Uniform",
            "Aux501_Units_Republic_501st_CS_Support_Uniform",
            "Aux501_Units_Republic_501st_Snr_CS_Support_Uniform",

            "Aux501_Units_Republic_501st_Platoon_CSM_Support_Uniform",
            "Aux501_Units_Republic_501st_Company_CSM_Support_Uniform",
            "Aux501_Units_Republic_501st_Battalion_CSM_Support_Uniform",

            "Aux501_Units_Republic_501st_2LT_Support_Uniform",
            "Aux501_Units_Republic_501st_1LT_Support_Uniform",
            "Aux501_Units_Republic_501st_Captain_Support_Uniform",
            "Aux501_Units_Republic_501st_Major_Support_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Standard_Uniform;

    class Aux501_Units_Republic_501st_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_Trooper_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] INF P2 ARMR 02 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "ct";
        };
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 03 - Sr. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 04 - Vet. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Lance_CP_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 05 - CLC";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Lance_CP_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "clc";
        };
    };
    
    //NCOs
    class Aux501_Units_Republic_501st_CP_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 06 - CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_CP_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "cp";
        };
    };
    class Aux501_Units_Republic_501st_Snr_CP_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 07 - Sr. CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_CP_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "sr_cp";
        };
    };
    class Aux501_Units_Republic_501st_CS_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 08 - CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_CS_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "cs";
        };
    };
    class Aux501_Units_Republic_501st_Snr_CS_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 09 - Sr. CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Snr_CS_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "sr_cs";
        };
    };
    
    //CS-Ms
    class Aux501_Units_Republic_501st_Platoon_CSM_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 10 - CS-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Platoon_CSM_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "csm";
        };
    };
    class Aux501_Units_Republic_501st_Company_CSM_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 11 - CO CS-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Company_CSM_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "c_csm";
        };
    };
    class Aux501_Units_Republic_501st_Battalion_CSM_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 12 - BN CS-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Battalion_CSM_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "b_csm";
        };
    };
    
    //Officers
    class Aux501_Units_Republic_501st_2LT_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 13 - 2nd Lt";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_2LT_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "second_Lt";
        };
    };
    class Aux501_Units_Republic_501st_1LT_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 14 - 1st Lt";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_1LT_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "first_Lt";
        };
    };
    class Aux501_Units_Republic_501st_Captain_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 15 - Captain";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Captain_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "captain";
        };
    };
    class Aux501_Units_Republic_501st_Major_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] INF P2 ARMR 16 - Major";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Major_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "grenade_1x";
            rank = "major";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    class Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        model = "\MRC\JLTS\characters\CloneArmor\CloneArmorSupport.p3d";
        hiddenSelections[] = {"camo1","camo2","camo3","insignia"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "MRC\JLTS\characters\CloneArmor\data\clone_armor1_clean.rvmat",
            "MRC\JLTS\characters\CloneArmor\data\clone_armor2_clean.rvmat",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\Clone_back_attachments.rvmat",
            "MRC\JLTS\Core_mod\data\insignias\insignia_CloneArmor.rvmat"
        };
        uniformClass = "Aux501_Units_Republic_501st_Trooper_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_Trooper_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_Trooper_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Vet_Trooper_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Vet_Trooper_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Lance_CP_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_clc_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_clc_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Lance_CP_Support_Uniform";
    };
    
    //NCOs
    class Aux501_Units_Republic_501st_CP_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cp_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cp_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_CP_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_CP_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_cp_nco_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_cp_nco_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_CP_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_CS_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cs_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cp_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_CS_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Snr_CS_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_cs_nco_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_cs_nco_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Snr_CS_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Platoon_CSM_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_plt_csm_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_plt_csm_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Platoon_CSM_Support_Uniform";
    };

    class Aux501_Units_Republic_501st_Company_CSM_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_comp_csm_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_comp_csm_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Company_CSM_Support_Uniform";
    };

    class Aux501_Units_Republic_501st_Battalion_CSM_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_bn_csm_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_bn_csm_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Battalion_CSM_Support_Uniform";
    };

    //Officers
    class Aux501_Units_Republic_501st_2LT_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_2LT_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_2LT_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_2LT_Support_Uniform";
    };

    class Aux501_Units_Republic_501st_1LT_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_1LT_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_1LT_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_1LT_Support_Uniform";
    };

    class Aux501_Units_Republic_501st_Captain_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cpt_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cpt_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Captain_Support_Uniform";
    };
    
    class Aux501_Units_Republic_501st_Major_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_maj_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_maj_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Major_Support_Uniform";
    };
};