class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2_Officer_Alt
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_1LT_Alt_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Captain_Alt_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Major_Alt_Uniform_Vehicle"
        };
        weapons[] = 
        {

            "Aux501_Units_Republic_501st_2LT_Alt_Uniform",
            "Aux501_Units_Republic_501st_1LT_Alt_Uniform",
            "Aux501_Units_Republic_501st_Captain_Alt_Uniform",
            "Aux501_Units_Republic_501st_Major_Alt_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Trooper_Uniform;

    class Aux501_Units_Republic_501st_2LT_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        class ItemInfo;
    };

    //Officers
    class Aux501_Units_Republic_501st_2LT_Alt_Uniform: Aux501_Units_Republic_501st_2LT_Uniform
    {
        displayName = "[501st] INF P2 CMDR ARMR 01 - 2nd LT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "officer_alt";
            rank = "second_Lt";
        };
    };
    class Aux501_Units_Republic_501st_1LT_Alt_Uniform: Aux501_Units_Republic_501st_2LT_Alt_Uniform
    {
        displayName = "[501st] INF P2 CMDR ARMR 02 - 1st Lt";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_1LT_Alt_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "officer_alt";
            rank = "first_Lt";
        };
    };
    class Aux501_Units_Republic_501st_Captain_Alt_Uniform: Aux501_Units_Republic_501st_2LT_Alt_Uniform
    {
        displayName = "[501st] INF P2 CMDR ARMR 03 - Captain";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Captain_Alt_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "officer_alt";
            rank = "captain";
        };
    };
    class Aux501_Units_Republic_501st_Major_Alt_Uniform: Aux501_Units_Republic_501st_2LT_Alt_Uniform
    {
        displayName = "[501st] INF P2 CMDR ARMR 04 - Major";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Major_Alt_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "infantry";
            style = "officer_alt";
            rank = "major";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    //Officers
    class Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        model = "\MRC\JLTS\characters\CloneArmor\CloneArmorMC.p3d";
        hiddenSelections[] = {"camo1","camo2","camo3"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\mc_armor\inf_2LT_alt_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_2LT_armor_lower_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\mc_armor\inf_2LT_chest_rank_co.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\clone_officer_alt_armor.rvmat",
            "MRC\JLTS\characters\CloneArmor\data\clone_armor2_clean.rvmat"
        };
        class Wounds
        {
            tex[] = {};
            mat[] = 
            {
                "Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\clone_officer_alt_armor.rvmat",
                "MRC\JLTS\Characters\CloneArmor\data\Clone_armor1_mc_injury.rvmat",
                "MRC\JLTS\Characters\CloneArmor\data\Clone_armor1_mc_injury.rvmat",
                "MRC\JLTS\Characters\CloneArmor\data\Clone_armor2_clean.rvmat",
                "MRC\JLTS\Characters\CloneArmor\data\Clone_armor2_clean_injury.rvmat",
                "MRC\JLTS\Characters\CloneArmor\data\Clone_armor2_clean_injury.rvmat",
                "A3\Characters_F\Common\Data\basicbody.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "A3\Characters_F\Common\Data\basicbody_injury.rvmat",
                "a3\characters_f\heads\data\hl_white.rvmat",
                "a3\characters_f\heads\data\hl_white_injury.rvmat",
                "a3\characters_f\heads\data\hl_white_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_02_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_02_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_02_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_muscular.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_muscular_injury.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_muscular_injury.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_asian_02_bald_muscular.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_asian_02_bald_muscular_injury.rvmat",
                "A3\Characters_F_Exp\Heads\Data\hl_asian_02_bald_muscular_injury.rvmat"
            };
        };
        uniformClass = "Aux501_Units_Republic_501st_2LT_Alt_Uniform";
    };
    class Aux501_Units_Republic_501st_1LT_Alt_Uniform_Vehicle: Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\mc_armor\inf_1LT_alt_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_1LT_armor_lower_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\mc_armor\inf_1LT_chest_rank_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_1LT_Alt_Uniform";
    };
    class Aux501_Units_Republic_501st_Captain_Alt_Uniform_Vehicle: Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\mc_armor\inf_cpt_alt_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_cpt_armor_lower_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\mc_armor\inf_cpt_chest_rank_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Captain_Alt_Uniform";
    };
    class Aux501_Units_Republic_501st_Major_Alt_Uniform_Vehicle: Aux501_Units_Republic_501st_2LT_Alt_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\mc_armor\inf_maj_alt_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_maj_armor_lower_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\mc_armor\inf_maj_chest_rank_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Major_Alt_Uniform";
    };
};