class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Medic
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Medic_Cadet_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Snr_Trooper_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Vet_Trooper_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Lance_CP_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Snr_CP_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Snr_CS_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Medic_Cadet_Uniform",
            "Aux501_Units_Republic_501st_Medic_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Medic_Snr_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Medic_Vet_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Medic_Lance_CP_Uniform",

            "Aux501_Units_Republic_501st_Medic_CP_Uniform",
            "Aux501_Units_Republic_501st_Medic_Snr_CP_Uniform",
            "Aux501_Units_Republic_501st_Medic_CS_Uniform",
            "Aux501_Units_Republic_501st_Medic_Snr_CS_Uniform",

            "Aux501_Units_Republic_501st_Medic_Battalion_CSM_Uniform"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;
    class Aux501_Units_Republic_501st_Standard_Uniform: Uniform_Base
    {
        class ItemInfo;
    };
    class Aux501_Units_Republic_501st_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        class ItemInfo;
    };

    //CTs
    class Aux501_Units_Republic_501st_Medic_Cadet_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        displayName = "[501st] MED P2 ARMR 01 - CR-C";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Cadet_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "standard";
            rank = "cr_c";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] MED P2 ARMR 02 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "standard";
            rank = "ct";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Snr_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] MED P2 ARMR 03 - Sr. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_Trooper_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Vet_Trooper_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] MED P2 ARMR 04 - Vet. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Vet_Trooper_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "standard";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Lance_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] MED P2 ARMR 05 - CM-L";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Lance_CP_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "standard";
            rank = "clc";
        };
    };
    
    //NCOs
    class Aux501_Units_Republic_501st_Medic_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] MED P2 ARMR 06 - CM-P";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_CP_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "standard";
            rank = "cp";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Snr_CP_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] MED P2 ARMR 07 - Sr. CM-P";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_CP_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "standard";
            rank = "sr_cp";
        };
    };
    class Aux501_Units_Republic_501st_Medic_CS_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] MED P2 ARMR 08 - CM-S";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_CS_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "standard";
            rank = "cs";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Snr_CS_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] MED P2 ARMR 09 - Sr. CM-S";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_CS_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "standard";
            rank = "sr_cs";
        };
    };
    
    //CS-M
    class Aux501_Units_Republic_501st_Medic_Battalion_CSM_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] MED P2 ARMR 10 - CM-O";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Battalion_CSM_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "standard";
            rank = "b_csm";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    class Aux501_Units_Republic_501st_Medic_Cadet_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501st_Medic_Cadet_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_crc_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_crc_armor_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Medic_Snr_Trooper_Uniform_Vehicle: Aux501_Units_Republic_501st_Medic_Cadet_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_Trooper_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Medic_Vet_Trooper_Uniform_Vehicle: Aux501_Units_Republic_501st_Medic_Cadet_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Medic_Vet_Trooper_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_vet_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Medic_Lance_CP_Uniform_Vehicle: Aux501_Units_Republic_501st_Medic_Cadet_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Medic_Lance_CP_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_clc_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_clc_armor_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Medic_Snr_CP_Uniform_Vehicle: Aux501_Units_Republic_501st_Medic_Cadet_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_CP_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_cp_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_cp_armor_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Medic_Snr_CS_Uniform_Vehicle: Aux501_Units_Republic_501st_Medic_Cadet_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_CS_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_cs_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_cs_armor_lower_co.paa"
        };
    };
};