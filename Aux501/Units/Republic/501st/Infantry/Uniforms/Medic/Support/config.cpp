class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2_Support_Medic
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Medic_Trooper_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Snr_Trooper_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Vet_Trooper_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Lance_CP_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_CP_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Snr_CP_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_CS_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Snr_CS_Support_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Medic_Battalion_CSM_Support_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Medic_Trooper_Support_Uniform",
            "Aux501_Units_Republic_501st_Medic_Snr_Trooper_Support_Uniform",
            "Aux501_Units_Republic_501st_Medic_Vet_Trooper_Support_Uniform",
            "Aux501_Units_Republic_501st_Medic_Lance_CP_Support_Uniform",

            "Aux501_Units_Republic_501st_Medic_CP_Support_Uniform",
            "Aux501_Units_Republic_501st_Medic_Snr_CP_Support_Uniform",
            "Aux501_Units_Republic_501st_Medic_CS_Support_Uniform",
            "Aux501_Units_Republic_501st_Medic_Snr_CS_Support_Uniform",

            "Aux501_Units_Republic_501st_Medic_Battalion_CSM_Support_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Trooper_Uniform;

    class Aux501_Units_Republic_501st_Trooper_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_Medic_Trooper_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] MED P2 ARMR 02 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Trooper_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "grenade_1x";
            rank = "ct";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Snr_Trooper_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] MED P2 ARMR 03 - Sr. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_Trooper_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "grenade_1x";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Vet_Trooper_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] MED P2 ARMR 04 - Vet. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Vet_Trooper_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "grenade_1x";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Lance_CP_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] MED P2 ARMR 05 - CLC";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Lance_CP_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "grenade_1x";
            rank = "clc";
        };
    };
    
    //NCOs
    class Aux501_Units_Republic_501st_Medic_CP_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] MED P2 ARMR 06 - CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_CP_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "grenade_1x";
            rank = "cp";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Snr_CP_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] MED P2 ARMR 07 - Sr. CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_CP_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "grenade_1x";
            rank = "sr_cp";
        };
    };
    class Aux501_Units_Republic_501st_Medic_CS_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] MED P2 ARMR 08 - CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_CS_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "grenade_1x";
            rank = "cs";
        };
    };
    class Aux501_Units_Republic_501st_Medic_Snr_CS_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] MED P2 ARMR 09 - Sr. CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_CS_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "grenade_1x";
            rank = "sr_cs";
        };
    };
    
    //CS-M
    class Aux501_Units_Republic_501st_Medic_Battalion_CSM_Support_Uniform: Aux501_Units_Republic_501st_Trooper_Support_Uniform
    {
        displayName = "[501st] MED P2 ARMR 12 - BN CS-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Medic_Battalion_CSM_Support_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "default";
            mos = "medic";
            style = "grenade_1x";
            rank = "b_csm";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle;

    class Aux501_Units_Republic_501st_Medic_Trooper_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Medic_Trooper_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Medic_Snr_Trooper_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_snr_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_Trooper_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Medic_Vet_Trooper_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_vet_ct_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Medic_Vet_Trooper_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Medic_Lance_CP_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_clc_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_clc_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Medic_Lance_CP_Support_Uniform";
    };
    
    //NCOs
    class Aux501_Units_Republic_501st_Medic_CP_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_cp_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_cp_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Medic_CP_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Medic_Snr_CP_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_cp_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_cp_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_CP_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Medic_CS_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_cs_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_cp_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Medic_CS_Support_Uniform";
    };
    class Aux501_Units_Republic_501st_Medic_Snr_CS_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_cs_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_snr_cs_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Medic_Snr_CS_Support_Uniform";
    };

    //CS-M
    class Aux501_Units_Republic_501st_Medic_Battalion_CSM_Support_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Support_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_bn_csm_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Medic\data\textures\inf_med_bn_csm_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Medic_Battalion_CSM_Support_Uniform";
    };
};