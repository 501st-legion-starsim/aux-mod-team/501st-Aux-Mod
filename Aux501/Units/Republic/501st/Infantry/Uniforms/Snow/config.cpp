class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Snow
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Snow_Trooper_Uniform_Vehicle",
            "Aux501_Units_Republic_501_Snow_NCO_Uniform_Vehicle",
            "Aux501_Units_Republic_501_Snow_RTO_Uniform_Vehicle",
            "Aux501_Units_Republic_501_Snow_RTO_NCO_Uniform_Vehicle",
            "Aux501_Units_Republic_501_Snow_MED_Uniform_Vehicle",
            "Aux501_Units_Republic_501_Snow_MED_NCO_Uniform_Vehicle",

            "Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform_Vehicle",
            "Aux501_Units_Republic_501_Snow_Plastic_NCO_Uniform_Vehicle",
            "Aux501_Units_Republic_501_Snow_Plastic_RTO_Uniform_Vehicle",
            "Aux501_Units_Republic_501_Snow_Plastic_RTO_NCO_Uniform_Vehicle",
            "Aux501_Units_Republic_501_Snow_Plastic_MED_Uniform_Vehicle",
            "Aux501_Units_Republic_501_Snow_Plastic_MED_NCO_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Snow_Trooper_Uniform",
            "Aux501_Units_Republic_501_Snow_NCO_Uniform",
            "Aux501_Units_Republic_501_Snow_RTO_Uniform",
            "Aux501_Units_Republic_501_Snow_RTO_NCO_Uniform",
            "Aux501_Units_Republic_501_Snow_MED_Uniform",
            "Aux501_Units_Republic_501_Snow_MED_NCO_Uniform",

            "Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform",
            "Aux501_Units_Republic_501_Snow_Plastic_NCO_Uniform",
            "Aux501_Units_Republic_501_Snow_Plastic_RTO_Uniform",
            "Aux501_Units_Republic_501_Snow_Plastic_RTO_NCO_Uniform",
            "Aux501_Units_Republic_501_Snow_Plastic_MED_Uniform",
            "Aux501_Units_Republic_501_Snow_Plastic_MED_NCO_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Uniform_Base;
    class Aux501_Units_Republic_501st_Standard_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501_Snow_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[501st] SNOW P2 Uniform 01 - CT";
        picture = "\Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\UI\Aux501_Snow_Uniform_ui_ca.paa";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_Trooper_Uniform_Vehicle";
		};
        
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "trooper";
            mos = "gi";
            style = "cloth";
        };
    }; 
    class Aux501_Units_Republic_501_Snow_NCO_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 02 - NCO";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_NCO_Uniform_Vehicle";
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "nco";
            mos = "gi";
            style = "cloth";
        };
    };
    class Aux501_Units_Republic_501_Snow_RTO_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 03 - RTO";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_RTO_Uniform_Vehicle";
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "trooper";
            mos = "rto";
            style = "cloth";
        };
    };
    class Aux501_Units_Republic_501_Snow_RTO_NCO_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 04 - RTO NCO";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_RTO_NCO_Uniform_Vehicle";
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "nco";
            mos = "rto";
            style = "cloth";
        };
    };
    class Aux501_Units_Republic_501_Snow_MED_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 05 - Medic";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_MED_Uniform_Vehicle";
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "trooper";
            mos = "med";
            style = "cloth";
        };
    };
    class Aux501_Units_Republic_501_Snow_MED_NCO_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 06 - Medic NCO";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_MED_NCO_Uniform_Vehicle";
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "nco";
            mos = "med";
            style = "cloth";
        };
    };
   
    //Plasteel Time Baby
    class Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 01 - CT";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform_Vehicle";
		};
        
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "trooper";
            mos = "gi";
            style = "plasteel";
        };
    }; 
    class Aux501_Units_Republic_501_Snow_Plastic_NCO_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 02 - NCO";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_NCO_Uniform_Vehicle";
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "nco";
            mos = "gi";
            style = "plasteel";
        };
    };
    class Aux501_Units_Republic_501_Snow_Plastic_RTO_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 03 - RTO";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_RTO_Uniform_Vehicle";
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "trooper";
            mos = "rto";
            style = "plasteel";
        };
    };
    class Aux501_Units_Republic_501_Snow_Plastic_RTO_NCO_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 04 - RTO NCO";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_RTO_NCO_Uniform_Vehicle";
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "nco";
            mos = "rto";
            style = "plasteel";
        };
    };
    class Aux501_Units_Republic_501_Snow_Plastic_MED_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 05 - Medic";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_MED_Uniform_Vehicle";
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "trooper";
            mos = "med";
            style = "plasteel";
        };
    };
    class Aux501_Units_Republic_501_Snow_Plastic_MED_NCO_Uniform: Aux501_Units_Republic_501_Snow_Trooper_Uniform
    {
        displayname = "[501st] SNOW P2 Uniform 06 - Medic NCO";
        class ItemInfo: ItemInfo
		{
            uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_MED_NCO_Uniform_Vehicle";
		};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms";
            rank = "nco";
            mos = "med";
            style = "plasteel";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    //Cloth Time GI Vehicles Uniform Stuff
    class Aux501_Units_Republic_501_Snow_Trooper_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_Trooper_Uniform";
        model = "JLTS_AE_GM\SEA_Uniform_GM_Fabric.p3d";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_co.paa"
        };
        hiddenSelectionsMaterials[] = {};
    };
    class Aux501_Units_Republic_501_Snow_NCO_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_NCO_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_nco_co.paa"
        };
    };
    //RTO Vehicles Uniform Stuff
    class Aux501_Units_Republic_501_Snow_RTO_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_RTO_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\RTO_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501_Snow_RTO_NCO_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_RTO_NCO_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\RTO_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_nco_co.paa"
        };
    };
    //Medical Vehicles Uniform Stuff
    class Aux501_Units_Republic_501_Snow_MED_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_MED_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\MED_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501_Snow_MED_NCO_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_MED_NCO_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\MED_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_nco_co.paa"
        };
    };
    
    //Plasteel Time GI Vehicles Uniform Stuff
    class Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform";
        model = "JLTS_AE_GM\SEA_Uniform_GM_Plastic.p3d";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_plastic_co.paa"
        };
    };
    class Aux501_Units_Republic_501_Snow_Plastic_NCO_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_NCO_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_plastic_nco_co.paa"
        };
    };
    //RTO Vehicles Uniform Stuff
    class Aux501_Units_Republic_501_Snow_Plastic_RTO_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_RTO_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\RTO_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_plastic_co.paa"
        };
    };
    class Aux501_Units_Republic_501_Snow_Plastic_RTO_NCO_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_RTO_NCO_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\RTO_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_plastic_nco_co.paa"
        };
    };
    //Medical Vehicles Uniform Stuff
    class Aux501_Units_Republic_501_Snow_Plastic_MED_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_MED_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\MED_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_plastic_co.paa"
        };
    };
    class Aux501_Units_Republic_501_Snow_Plastic_MED_NCO_Uniform_Vehicle: Aux501_Units_Republic_501_Snow_Plastic_Trooper_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501_Snow_Plastic_MED_NCO_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\MED_snow_uniform_upper_co.paa",
            "Aux501\Units\Republic\501st\Infantry\Uniforms\Snow\data\textures\INF_snow_uniform_lower_plastic_nco_co.paa"
        };
    };
};
class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_Snow_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "rank",
                "mos",
                "style"
            };

            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "trooper",
                    "nco"
                };
                class trooper
                {
                    label = "Trooper";
                };
                class nco
                {
                    label = "NCO";
                };
            };
            class mos
            {
                label = "MOS";
                values[] = 
                {
                    "gi",
                    "rto",
                    "med"
                };
                class gi
                {
                    label = "GI";
                };
                class rto
                {
                    label = "RTO";
                };
                class med 
                {
                    label = "Medic";
                };
            };
            class style
            {
                label = "Style";
                values[] = 
                {
                    "cloth",
                    "plasteel"
                };
                class cloth
                {
                    label = "Cloth";
                };
                class plasteel
                {
                    label = "Plasteel";
                };
            };
        };
    };
};