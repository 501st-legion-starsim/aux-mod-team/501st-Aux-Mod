class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_RTO_Vests_Phase2_kamas
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_kamas"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_RTO_Vests_Kama_CSM_Battalion",
            "Aux501_Units_Republic_501_RTO_Vests_Kama_holsters_CSM_Battalion"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Kama_Gray;
    class Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters_Gray;


    //Kamas
    class Aux501_Units_Republic_501_Rto_Vests_Kama_CSM_Battalion: Aux501_Units_Republic_501_Infantry_Vests_Kama_Gray
    {
        displayname = "[501st] RTO VEST 12 - Kama - CS-M (C)";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_bn_csm_vest_co.paa",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Kama_Vests";
            mos = "rto";
            rank = "sr_csm";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Rto_Vests_Kama_holsters_CSM_Battalion: Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters_Gray
    {
        displayname = "[501st] RTO VEST 13 - Kama - CS-M (C)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\RTO\data\textures\inf_rto_bn_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Kama_Vests";
            mos = "rto";
            rank = "sr_csm";
            variant = "holsters";
        };
    };
};