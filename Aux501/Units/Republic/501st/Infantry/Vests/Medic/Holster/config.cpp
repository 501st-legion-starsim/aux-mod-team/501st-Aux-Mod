class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Medic_Vests_Phase2_holsters
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_holsters"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Medic_Vests_Holster_bag"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Holster_bag;

    class Aux501_Units_Republic_501_Medic_Vests_Holster_bag: Aux501_Units_Republic_501_Infantry_Vests_Holster_bag
    {
        displayname = "[501st] MED VEST 01 - Holster - Sr. CT";
        hiddenSelectionsTextures[] = 
        {
            "SWLB_clones\data\heavy_accessories_medic_co.paa",
            "SWLB_clones\data\heavy_accessories_medic_co.paa",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "medic_bag";
        };
    };
};