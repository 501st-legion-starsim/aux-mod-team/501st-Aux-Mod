class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Medic_Vests_Phase2_Jr_NCOs
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Medic_Vests_Vet_Trooper",
            "Aux501_Units_Republic_501_Medic_Vests_Vet_Trooper_Bag",

            "Aux501_Units_Republic_501_Medic_Vests_Lance_CP",
            "Aux501_Units_Republic_501_Medic_Vests_Lance_CP_bag",

            "Aux501_Units_Republic_501_Medic_Vests_snr_CP",
            "Aux501_Units_Republic_501_Medic_Vests_snr_CP_bag",

            "Aux501_Units_Republic_501_Medic_Vests_snr_CS"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper_Bag;
    class Aux501_Units_Republic_501_Infantry_Vests_Lance_CP;
    class Aux501_Units_Republic_501_Infantry_Vests_Lance_CP_bag;
    class Aux501_Units_Republic_501_Infantry_Vests_CP;
    class Aux501_Units_Republic_501_Infantry_Vests_CP_bag;
    class Aux501_Units_Republic_501_Infantry_Vests_CS;

    //Vet Trooper
    class Aux501_Units_Republic_501_Medic_Vests_Vet_Trooper_Bag: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper_Bag
    {
        displayname = "[501st] MED VEST 02 - Vet. CT";
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "medic";
            rank = "vet_ct";
            variant = "bag";
        };
    };

    //CLC
    class Aux501_Units_Republic_501_Medic_Vests_Lance_CP: Aux501_Units_Republic_501_Infantry_Vests_Lance_CP
    {
        displayname = "[501st] MED VEST 03 - CLC";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_lance_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "medic";
            rank = "clc";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Medic_Vests_Lance_CP_bag: Aux501_Units_Republic_501_Infantry_Vests_Lance_CP_bag
    {
        displayname = "[501st] MED VEST 04 - CLC";
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_lance_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "medic";
            rank = "clc";
            variant = "bag";
        };
    };

    //Sr. CP    
    class Aux501_Units_Republic_501_Medic_Vests_snr_CP: Aux501_Units_Republic_501_Infantry_Vests_CP
    {
        displayname = "[501st] MED VEST 05 - Sr. CP";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "medic";
            rank = "sr_cp";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Medic_Vests_snr_CP_bag: Aux501_Units_Republic_501_Infantry_Vests_CP_bag
    {
        displayname = "[501st] MED VEST 06 - Sr. CP";
        hiddenSelectionsTextures[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "medic";
            rank = "sr_cp";
            variant = "bag";
        };
    };

    //Sr. CS
    class Aux501_Units_Republic_501_Medic_Vests_snr_CS: Aux501_Units_Republic_501_Infantry_Vests_CS
    {
        displayname = "[501st] MED VEST 07 - Sr. CS";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_bn_csm_vest_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Vests\Medic\data\textures\inf_med_snr_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Jr_NCO_Vests";
            mos = "medic";
            rank = "sr_cs";
            variant = "standard";
        };
    };
};