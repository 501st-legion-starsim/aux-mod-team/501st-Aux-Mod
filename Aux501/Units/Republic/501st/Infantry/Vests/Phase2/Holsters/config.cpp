class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_holsters
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Vests_Standard_Holster",
            "Aux501_Units_Republic_501_Infantry_Vests_Holster_Pouches",
            "Aux501_Units_Republic_501_Infantry_Vests_Holster_Pad",
            "Aux501_Units_Republic_501_Infantry_Vests_Holster_leg",
            "Aux501_Units_Republic_501_Infantry_Vests_Holster_chest",
            "Aux501_Units_Republic_501_Infantry_Vests_Holster_bag",
            "Aux501_Units_Republic_501_Infantry_Vests_Holster_bag_nco",
            "Aux501_Units_Republic_501_Infantry_Vests_Holster_blackbeard"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Base;
    
    class Aux501_Units_Republic_501_Infantry_Vests_Nanoweave: Aux501_Units_Republic_501_Infantry_Vests_Base
    {
        class ItemInfo;
    };

    //Holster
    class Aux501_Units_Republic_501_Infantry_Vests_Standard_Holster: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 01 - Holster";
        picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\CloneVestHolster_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor2\CloneVestHolster.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor2\CloneVestHolster.p3d";
            hiddenSelections[] = {"camo1"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Holster_Pouches: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 02 - Holster - Sr. CT";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_airborne_armor_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_ARF_Vest.p3d";
        hiddenSelections[] = {"camo1","camo2","camo3"};
        hiddenSelectionsTextures[] = 
        {
            "SWLB_clones\data\light_accessories_co.paa",
            "SWLB_clones\data\heavy_accessories_co.paa",
            "SWLB_clones\data\officer_accessories_co.paa"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_CEE\data\SWLB_CEE_ARF_Vest.p3d";
            hiddenSelections[] = {"camo1","camo2","camo3"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Holster_Pad: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 03 - Holster - Sr. CT";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_cfr_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_cfr_armor.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\SWLB_clones\data\heavy_accessories_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_clones\SWLB_clone_cfr_armor.p3d";
            hiddenSelections[] = {"camo1"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "pad";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Holster_leg: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 04 - Holster - Sr. CT";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_recon_nco_armor_ca.paa";
        model = "\SWLB_clones\SWLB_clone_recon_armor.p3d";
        hiddenSelections[] = {"camo1","holster","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "",
            "\SWLB_clones\data\heavy_accessories_co.paa",
            ""
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_clones\SWLB_clone_recon_armor.p3d";
            hiddenSelections[] = {"camo1","holster","pauldron"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "leg";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Holster_chest: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 04 - Holster - Sr. CT";
        picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\CloneVestHolster_ui_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Recon_Force_Officer.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "SWLB_clones\data\light_accessories_co.paa",
            ""
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_CEE\data\SWLB_CEE_Recon_Force_Officer.p3d";
            hiddenSelections[] = {"camo1","camo2"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "chest";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Holster_chest_dual: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 05 - Holster - CS";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_light_armor_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Recon_Force_Commander.p3d";
		hiddenSelections[] = {"camo1","camo2","camo4"};
		hiddenSelectionsTextures[] = 
        {
            "SWLB_clones\data\light_accessories_co.paa",
            "",
            ""
        };
		
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_CEE\data\SWLB_CEE_Recon_Force_Commander.p3d";
            hiddenSelections[] = {"camo1","camo2","camo4"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "chest_dual";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Holster_bag: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 06 - Holster - Sr. CT";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_light_armor_ca.paa";
        model = "\SWLB_CEE\data\SWLB_CEE_Heavy_Vest.p3d";
        hiddenSelections[] = {"camo1","camo2","pauldron"};
        hiddenSelectionsTextures[] = 
        {
            "SWLB_clones\data\heavy_accessories_co.paa",
            "SWLB_clones\data\heavy_accessories_co.paa",
            ""
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_CEE\data\SWLB_CEE_Heavy_Vest.p3d";
            hiddenSelections[] = {"camo1","camo2","pauldron"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "heavy_bag";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Holster_blackbeard: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 09 - Holster - Pirate";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_light_armor_ca.paa";
        model = "ls_meme_misc\vest\blackbeard\ls_memefor_blackbeard_vest";
        hiddenSelections[] = {};
        hiddenSelectionsTextures[] = {};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "ls_meme_misc\vest\blackbeard\ls_memefor_blackbeard_vest";
            hiddenSelections[] = {};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "pirate";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "pouch",
                    "pad",
                    "leg",
                    "chest",
                    "chest_dual",
                    "heavy_bag",
                    "medic_bag",
                    "pirate",
                    "razorblade",
                    "razorblade2"
                };
                class standard
                {
                    label = "Holster";
                };
                class pouch
                {
                    label = "Pouch";
                };
                class pad
                {
                    label = "Pad";
                };
                class leg
                {
                    label = "Leg";
                };
                class chest
                {
                    label = "Chest";
                };
                class chest_dual
                {
                    label = "Chest/Dual";
                };
                class heavy_bag
                {
                    label = "Bag";
                };
                class medic_bag
                {
                    label = "MedBag";
                };
                class pirate
                {
                    label = "Pirate";
                };
                class razorblade
                {
                    label = "Razorblade 1";
                };
                class razorblade2
                {
                    label = "Razorblade 2";
                };
            };
        };
    };
};