class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Vests_Base",
            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave"
        };
    };
};

class cfgWeapons
{
    class V_RebreatherB;
    
    class Aux501_Units_Republic_501_Infantry_Vests_Base: V_RebreatherB
    {
        class ItemInfo;
    };
    
    class Aux501_Units_Republic_501_Infantry_Vests_Nanoweave: Aux501_Units_Republic_501_Infantry_Vests_Base
    {
        scope = 2;
        scopeArsenal = 2;
        author = "501st Aux Team";
        displayname = "[501st] INF VEST 00 - Nanoweave";
        picture = "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\UI\CloneUndersuit_ca.paa";
    };
};