class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_kamas
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Vests_Kama",
            "Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters",

            "Aux501_Units_Republic_501_Infantry_Vests_Kama_CSM_Platoon",
            "Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters_CSM_Platoon",

            "Aux501_Units_Republic_501_Infantry_Vests_Kama_Gray",
            "Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters_Gray",

            "Aux501_Units_Republic_501_Infantry_Vests_Kama_CSM_Battalion",
            "Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters_CSM_Battalion"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_CP_bag;
    class Aux501_Units_Republic_501_Infantry_Vests_CS;

    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon: Aux501_Units_Republic_501_Infantry_Vests_CS
    {
        class ItemInfo;
    };

    //Kamas
    class Aux501_Units_Republic_501_Infantry_Vests_Kama: Aux501_Units_Republic_501_Infantry_Vests_CP_bag
    {
        displayname = "[501st] INF VEST 23 - Kama - CS";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        hiddenSelectionsTextures[] = 
        {
            "MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Kama_Vests";
            mos = "infantry";
            rank = "cs";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF VEST 24 - Kama - CS";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestKama.p3d";
        hiddenSelectionsTextures[] = {"MRC\JLTS\characters\CloneArmor\data\Clone_vest_officer_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestKama.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Kama_Vests";
            mos = "infantry";
            rank = "cs";
            variant = "holsters";
        };
    };

    class Aux501_Units_Republic_501_Infantry_Vests_Kama_Blue: Aux501_Units_Republic_501_Infantry_Vests_Kama
    {
        displayname = "[501st] INF VEST 25 - Kama - Sr. CS";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_plt_csm_vest_co.paa",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Kama_Vests";
            mos = "infantry";
            rank = "sr_cs";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters_Blue: Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters
    {
        displayname = "[501st] INF VEST 26 - Kama - Sr. CS";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_plt_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Kama_Vests";
            mos = "infantry";
            rank = "sr_cs";
            variant = "holsters";
        };
    };

    class Aux501_Units_Republic_501_Infantry_Vests_Kama_Gray: Aux501_Units_Republic_501_Infantry_Vests_Kama
    {
        displayname = "[501st] INF VEST 55 - Kama - CS-M (C)";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest_co.paa",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Kama_Vests";
            mos = "infantry";
            rank = "sr_csm";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters_Gray: Aux501_Units_Republic_501_Infantry_Vests_Kama_holsters
    {
        displayname = "[501st] INF VEST 56 - Kama - CS-M (C)";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_comp_csm_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Kama_Vests";
            mos = "infantry";
            rank = "sr_csm";
            variant = "holsters";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {        
        class Aux501_ACEX_Gear_Republic_501st_P2_Kama_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "mos",
                "rank",
                "variant"
            };
            class mos
            {
                label = "MOS";
                values[] = 
                {
                    "infantry",
                    "medic",
                    "rto"
                };
                class infantry
                {
                    label = "GI";
                };
                class medic
                {
                    label = "Medic";
                };
                class rto
                {
                    label = "RTO";
                };
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "cs",
                    "sr_cs",
                    "sr_csm"
                };
                class cs
                {
                    label = "CS+";
                };
                class sr_cs
                {
                    label = "Sr. CS+";
                };
                class sr_csm
                {
                    label = "CO CS-M+";
                };
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "holsters"
                };
                class standard
                {
                    label = "Standard";
                };
                class holsters
                {
                    label = "Holsters";
                };
            };
        };
    };
};