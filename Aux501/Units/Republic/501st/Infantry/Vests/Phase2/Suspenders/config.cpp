class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_suspenders
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Vests_Suspenders",
            "Aux501_Units_Republic_501_Infantry_Vests_Suspenders_gray",
            "Aux501_Units_Republic_501_Infantry_Vests_Suspenders_white",
            "Aux501_Units_Republic_501_Infantry_Vests_Suspenders_pouches",
            "Aux501_Units_Republic_501_Infantry_Vests_Suspenders_nades"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Base;
    
    class Aux501_Units_Republic_501_Infantry_Vests_Nanoweave: Aux501_Units_Republic_501_Infantry_Vests_Base
    {
        class ItemInfo;
    };   

    //Vet Trooper - Suspenders
    class Aux501_Units_Republic_501_Infantry_Vests_Suspenders: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] INF VEST 09 - Suspenders - Vet. CT";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestSuspender_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor\data\Clone_vest_suspender_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestSuspender.p3d";
            hiddenSelections[] = {"camo1"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_Suspenders";
            variant = "black";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Suspenders_gray: Aux501_Units_Republic_501_Infantry_Vests_Suspenders
    {
        displayname = "[501st] INF VEST 10 - Suspenders - Vet. CT";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_41stGree_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_Suspenders";
            variant = "gray";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Suspenders_white: Aux501_Units_Republic_501_Infantry_Vests_Suspenders
    {
        displayname = "[501st] INF VEST 11 - Suspenders - Vet. CT";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stJet_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_Suspenders";
            variant = "white";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Suspenders_pouches: Aux501_Units_Republic_501_Infantry_Vests_Suspenders
    {
        displayname = "[501st] INF VEST 12 - Suspenders - Vet. CT";
        model = "\SWLB_clones\SWLB_clone_specialist_armor.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"\SWLB_clones\data\light_accessories_co.paa","\SWLB_clones\data\heavy_accessories_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_clones\SWLB_clone_specialist_armor.p3d";
            hiddenSelections[] = {"camo1","camo2"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_Suspenders";
            variant = "holster_suspenders";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Vests_Suspenders_nades: Aux501_Units_Republic_501_Infantry_Vests_Suspenders
    {
        displayname = "[501st] INF VEST 13 - Suspenders - Vet. CT";
        model = "\SWLB_clones\SWLB_clone_grenadier_armor.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"\SWLB_clones\data\light_accessories_co.paa","\SWLB_clones\data\heavy_accessories_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_clones\SWLB_clone_grenadier_armor.p3d";
            hiddenSelections[] = {"camo1","camo2"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_Suspenders";
            variant = "holster_suspenders_nades";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {       
        class Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_Suspenders
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "black",
                    "gray",
                    "white",
                    "holster_suspenders",
                    "holster_suspenders_nades"
                };
                class black
                {
                    label = "Black";
                };
                class gray
                {
                    label = "Gray";
                };
                class white
                {
                    label = "White";
                };
                class holster_suspenders
                {
                    label = "Holster 1";
                };
                class holster_suspenders_nades
                {
                    label = "Holster 2";
                };
            };
        };
    };
};