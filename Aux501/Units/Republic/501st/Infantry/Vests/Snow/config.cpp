class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Vests_Snow
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Snow_Vests_Trooper",
            "Aux501_Units_Republic_501_Snow_Vests_NCO"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Base;
    
    class Aux501_Units_Republic_501_Infantry_Vests_Nanoweave: Aux501_Units_Republic_501_Infantry_Vests_Base
    {
        class ItemInfo;
    };
    
    class Aux501_Units_Republic_501_Snow_Vests_Trooper: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[501st] SNOW P2 VEST 01 - CT";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\JLTS_AE_GM\SEA_Vest_GM_Base.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\JLTS_AE_GM\data\BaseTextures\Vest\Vest_co.paa"};
        class ItemInfo: ItemInfo
		{
			uniformModel = "JLTS_AE_GM\SEA_Vest_GM_Base.p3d";
			hiddenSelections[] = {"camo1"};
		};
        
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Vests";
            rank = "trooper";
        };
    };

    class Aux501_Units_Republic_501_Snow_Vests_NCO: Aux501_Units_Republic_501_Snow_Vests_Trooper
    {
        displayname = "[501st] SNOW P2 VEST 02 - NCO";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Snow\data\textures\INF_snow_vest_nco_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Vests";
            rank = "nco";
        };
    };
};

class XtdGearModels
{
    class cfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_Snow_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "rank"
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "trooper",
                    "nco"
                };
                class trooper
                {
                    label = "Trooper";
                };
                class nco
                {
                    label = "NCO";
                };
            };
        };
    };
};