class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase1
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Pauldrons"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Infantry_P1_Vests_LT",
            "Aux501_Units_Republic_501_Infantry_P1_Vests_LT_2",
            "Aux501_Units_Republic_501_Infantry_P1_Vests_LT_3",
            "Aux501_Units_Republic_501_Infantry_P1_Vests_LT_4",

            "Aux501_Units_Republic_501_Infantry_P1_Vests_Captain",
            "Aux501_Units_Republic_501_Infantry_P1_Vests_Captain_2",
            "Aux501_Units_Republic_501_Infantry_P1_Vests_Captain_3",
            "Aux501_Units_Republic_501_Infantry_P1_Vests_Captain_4"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon;
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2;
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3;
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4;

    class Aux501_Units_Republic_501_Infantry_P1_Vests_LT: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF P1 VEST 01 - Lt.";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase1\textures\p1_lieutenant_pauldron_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Pauldron_Vests";
            rank = "Lt";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_P1_Vests_LT_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayname = "[501st] INF P1 VEST 02 - Lt.";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase1\textures\p1_lieutenant_pauldron_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Pauldron_Vests";
            rank = "Lt";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Infantry_P1_Vests_LT_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3
    {
        displayname = "[501st] INF P1 VEST 03 - Lt.";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase1\textures\p1_lieutenant_pauldron_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Pauldron_Vests";
            rank = "Lt";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Infantr_P1_Vests_LT_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayname = "[501st] INF P1 VEST 04 - Lt.";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase1\textures\p1_lieutenant_pauldron_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Pauldron_Vests";
            rank = "Lt";
            variant = "reverse";
        };
    };

    class Aux501_Units_Republic_501_Infantry_P1_Vests_Captain: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] INF P1 VEST 05 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase1\textures\p1_captain_pauldron_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Pauldron_Vests";
            rank = "captain";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Infantry_P1_Vests_Captain_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayname = "[501st] INF P1 VEST 06 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase1\textures\p1_captain_pauldron_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Pauldron_Vests";
            rank = "captain";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Infantry_P1_Vests_Captain_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3
    {
        displayname = "[501st] INF P1 VEST 07 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase1\textures\p1_captain_pauldron_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Pauldron_Vests";
            rank = "captain";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Infantry_P1_Vests_Captain_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayname = "[501st] INF P1 VEST 08 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase1\textures\p1_captain_pauldron_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_Pauldron_Vests";
            rank = "captain";
            variant = "reverse";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P1_Pauldron_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "rank",      
                "variant"
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "Lt",
                    "captain"
                };
                class Lt
                {
                    label = "Lt";
                };
                class captain
                {
                    label = "Captain";
                };
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "pouch",
                    "nokama",
                    "reverse",
                    "Nopauldron"
                };
                class standard
                {
                    label = "Standard";
                };
                class pouch
                {
                    label = "Pouch";
                };
                class nokama
                {
                    label = "No Kama";
                };
                class reverse
                {
                    label = "Reverse";
                };
            };
        };
    };
};