class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Infantry_Helmet_Base",
            "Aux501_Units_Republic_501_Infantry_Blank_Helmet",
            "Aux501_Units_Republic_501_Infantry_Helmet_Trooper",
            "Aux501_Units_Republic_501_Infantry_Helmet_Sergeant"
        };
    };
};

class CfgWeapons
{
    class H_HelmetB;
    class HeadgearItem;
    
    class Aux501_Units_Republic_501_Infantry_Helmet_Base: H_HelmetB
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        model = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\textures\cadet_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\textures\cadet_helmet_co.paa"
        };
        hiddenSelectionsMaterials[]= 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\Phase2_Helmet.rvmat",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\Standard_Visor.rvmat"
        };
        ace_hearing_protection = 0.85; 		
        ace_hearing_lowerVolume = 0;
        subItems[] = {"G_B_Diving"};
        class ItemInfo: HeadgearItem
        {
            mass = 30;
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetP2.p3d";
            modelSides[] = {6};
            hiddenSelections[] = {"Camo1","Camo2"};
            material = -1;
            explosionShielding = 2.2;
            minimalHit = 0.01;
            passThrough = 0.01;
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointName = "HitHead";
                    armor = 50;
                    passThrough = 0.6;
                };
            };
        };
    };

    class Aux501_Units_Republic_501_Infantry_Blank_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] INF P2 HELM 00 - Blank";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_ui_ca.paa";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Helmet";
            variant = "blank";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Helmet_Trooper: Aux501_Units_Republic_501_Infantry_Blank_Helmet
    {
        displayName = "[501st] INF P2 HELM 01 - CT";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_helmet_ca.paa";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\textures\trooper_helmet_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Helmet";
            variant = "ct";
        };
    };
    class Aux501_Units_Republic_501_Infantry_Helmet_Sergeant: Aux501_Units_Republic_501_Infantry_Helmet_Trooper
    {
        displayName = "[501st] INF P2 HELM 02 - CS";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\textures\sgt_helmet_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Helmet";
            variant = "cs";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_GI_Helmet
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "blank",
                    "ct",
                    "cs"
                };
                class blank
                {
                    label = "Blank";
                };
                class ct
                {
                    label = "CT";
                };
                class cs
                {
                    label = "CS";
                };
            };
        };
    };
};