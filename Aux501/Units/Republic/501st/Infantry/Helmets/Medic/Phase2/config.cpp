class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Helmets_Medic
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Medic_Helmet_CMC",
            "Aux501_Units_Republic_501_Medic_Helmet_CM"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Helmet_Trooper;

    class Aux501_Units_Republic_501_Medic_Helmet_CMC: Aux501_Units_Republic_501_Infantry_Helmet_Trooper
    {
        displayName = "[501st] MED P2 HELM O1 - CM-C";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Medic\data\textures\cm_c_trooper_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_MED_Helmet";
            variant = "p2";
            rank = "cmc";
        };
    };
    class Aux501_Units_Republic_501_Medic_Helmet_CM: Aux501_Units_Republic_501_Medic_Helmet_CMC
    {
        displayName = "[501st] MED P2 HELM O2 - CM";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Medic\data\textures\cm_trooper_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_MED_Helmet";
            variant = "p2";
            rank = "cm";
        };
    };
};