class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Helmets_Medic
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets"
        };
        units[] = {};
        weapons[] = {};
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_MED_Helmet
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant",
                "rank"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "p2",
                    "barc"
                };
                class p2
                {
                    label = "P2";
                };
                class barc
                {
                    label = "BARC";
                };
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "cmc",
                    "cm"
                };
                class cmc
                {
                    label = "CM-C";
                };
                class cm
                {
                    label = "CM";
                };
            };
        };
    };
};