class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Helmets_RTO
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_RTO_Helmet"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Blank_Helmet;
    
    class Aux501_Units_Republic_501_Infantry_Helmet_Trooper: Aux501_Units_Republic_501_Infantry_Blank_Helmet
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501_RTO_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Trooper
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] RTO P2 HELM 01";
        model = "SWLB_CEE\data\SWLB_P2_SpecOps_Helmet.p3d";
        hiddenSelections[]= {"camo1"};
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Infantry\Helmets\RTO\data\textures\rto_trooper_helmet_co.paa"};
        hiddenSelectionsMaterials[]= {};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_CEE\data\SWLB_P2_SpecOps_Helmet.p3d";
            hiddenSelections[] = {"camo1"};
        };
        class XtdGearInfo{};
    };
};