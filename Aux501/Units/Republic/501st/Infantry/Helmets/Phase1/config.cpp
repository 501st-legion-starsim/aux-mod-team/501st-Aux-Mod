class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase1
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_P1_Blank_Helmet",
            "Aux501_Units_Republic_501_P1_501st_Helmet",
            "Aux501_Units_Republic_501_P1_Sgt_Helmet",
            "Aux501_Units_Republic_501_P1_Lt_Helmet",
            "Aux501_Units_Republic_501_P1_Captain_Helmet",
            "Aux501_Units_Republic_501_P1_Commander_Helmet"
        };
    };
};

class CfgWeapons
{
    class H_HelmetB;
    
    class Aux501_Units_Republic_501_Infantry_Helmet_Base: H_HelmetB
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501_P1_Blank_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] INF P1 HELM 01 - Blank";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_helmet_p1_ca.paa";
        model = "JLTS_AE\SEA_Helmet_P1.p3d";
		hiddenSelections[] = {"Camo1","Visor"};
		hiddenSelectionsTextures[] = {"\JLTS_AE\Data\BaseTextures\P1\P1_Helmet_CO.paa","\JLTS_AE\Data\BaseTextures\P1\P1_Helmet_CO.paa"};
		hiddenSelectionsMaterials[] = {"","\a3\characters_f_bootcamp\common\data\vrarmoremmisive.rvmat"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "JLTS_AE\SEA_Helmet_P1.p3d";
            hiddenSelections[] = {"Camo1","Visor"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_GI_Helmet";
            variant = "blank";
        };
    };
    class Aux501_Units_Republic_501_P1_501st_Helmet: Aux501_Units_Republic_501_P1_Blank_Helmet
    {
        displayName = "[501st] INF P1 HELM 02 - 501st";
		hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase1\data\textures\p1_501st_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase1\data\textures\p1_501st_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_GI_Helmet";
            variant = "FiveOhFirst";
        };
    };
    class Aux501_Units_Republic_501_P1_Sgt_Helmet: Aux501_Units_Republic_501_P1_Blank_Helmet
    {
        displayName = "[501st] INF P1 HELM 03 - Sgt.";
		hiddenSelectionsTextures[] = 
        {
            "\JLTS_EA_Units\data\Phase1\Helmet_Sergeant.paa",
            "\JLTS_EA_Units\data\Phase1\Helmet_Sergeant.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_GI_Helmet";
            variant = "sgt";
        };
    };
    class Aux501_Units_Republic_501_P1_Lt_Helmet: Aux501_Units_Republic_501_P1_Blank_Helmet
    {
        displayName = "[501st] INF P1 HELM 04 - Lt.";
		hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase1\data\textures\p1_lieutenant_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase1\data\textures\p1_lieutenant_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_GI_Helmet";
            variant = "lt";
        };
    };
    class Aux501_Units_Republic_501_P1_Captain_Helmet: Aux501_Units_Republic_501_P1_Blank_Helmet
    {
        displayName = "[501st] INF P1 HELM 05 - Captain";
		hiddenSelectionsTextures[] = 
        {
            "\JLTS_EA_Units\data\Phase1\Helmet_Captain.paa",
            "\JLTS_EA_Units\data\Phase1\Helmet_Captain.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_GI_Helmet";
            variant = "captain";
        };
    };
    class Aux501_Units_Republic_501_P1_Commander_Helmet: Aux501_Units_Republic_501_P1_Blank_Helmet
    {
        displayName = "[501st] INF P1 HELM 06 - Commander";
		hiddenSelectionsTextures[] = 
        {
            "\JLTS_EA_Units\data\Phase1\Helmet_Commander.paa",
            "\JLTS_EA_Units\data\Phase1\Helmet_Commander.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P1_GI_Helmet";
            variant = "commander";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P1_GI_Helmet
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "blank",
                    "FiveOhFirst",
                    "sgt",
                    "lt",
                    "captain",
                    "commander"
                };
                class blank
                {
                    label = "Blank";
                };
                class FiveOhFirst
                {
                    label = "501st";
                };
                class sgt
                {
                    label = "Sgt.";
                };
                class lt
                {
                    label = "Lt.";
                };
                class captain
                {
                    label = "Captain";
                };
                class commander
                {
                    label = "Commander";
                };
            };
        };
    };
};