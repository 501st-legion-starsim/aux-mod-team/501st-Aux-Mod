class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Helmets_Snow
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Snow_Helmet",
            "Aux501_Units_Republic_501_Snow_Helmet_NCO",
            "Aux501_Units_Republic_501_Snow_Helmet_RTO",
            "Aux501_Units_Republic_501_Snow_Helmet_RTO_NCO",
            "Aux501_Units_Republic_501_Snow_Helmet_ARC",
            "Aux501_Units_Republic_501_Snow_Helmet_ARC_NCO"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Blank_Helmet;
    
    class Aux501_Units_Republic_501_Infantry_Helmet_Trooper: Aux501_Units_Republic_501_Infantry_Blank_Helmet
    {
        class ItemInfo;
    };

    //Base
    class Aux501_Units_Republic_501_Snow_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Trooper
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] INF SNOW HELM 01 - CT";
        picture = "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\UI\snow_helm_01_ui_ca.paa";
        model = "JLTS_AE_GM\SEA_Helmet_GM_Base.p3d";
        hiddenSelections[] = {"Camo1","Visor"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\INF_snow_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\INF_snow_helmet_co.paa"
        };
        hiddenSelectionsMaterials[]= 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\Standard_Visor.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "JLTS_AE_GM\SEA_Helmet_GM_Base.p3d";
            hiddenSelections[] = {"Camo1","Visor"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Helmet";
            mos = "infantry";
            rank = "trooper";
        };
    };
    class Aux501_Units_Republic_501_Snow_Helmet_NCO: Aux501_Units_Republic_501_Snow_Helmet
    {
        displayName = "[501st] INF SNOW HELM 02 - NCO";
        picture = "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\UI\snow_helm_02_ui_ca.paa";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\INF_snow_helmet_nco_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\INF_snow_helmet_nco_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Helmet";
            mos = "infantry";
            rank = "nco";
        };
    };
    
    //RTO
    class Aux501_Units_Republic_501_Snow_Helmet_RTO: Aux501_Units_Republic_501_Snow_Helmet
    {
        displayName = "[501st] RTO SNOW HELM 01 - CT";
        picture = "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\UI\snow_helm_01_ui_ca.paa";
        model = "JLTS_AE_GM\SEA_Helmet_GM_RTO.p3d";
        hiddenSelectionsTextures[] = 
            {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\RTO_snow_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\RTO_snow_helmet_co.paa"
            };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "JLTS_AE_GM\SEA_Helmet_GM_RTO.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Helmet";
            mos = "rto";
            rank = "trooper";
        };
    };
    class Aux501_Units_Republic_501_Snow_Helmet_RTO_NCO: Aux501_Units_Republic_501_Snow_Helmet_RTO
    {
        displayName = "[501st] RTO SNOW HELM 02 - NCO";
        picture = "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\UI\snow_helm_02_ui_ca.paa";
        model = "JLTS_AE_GM\SEA_Helmet_GM_RTO.p3d";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\RTO_snow_helmet_nco_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\RTO_snow_helmet_nco_co.paa"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "JLTS_AE_GM\SEA_Helmet_GM_RTO.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Helmet";
            mos = "rto";
            rank = "nco";
        };
    };
    
    //ARC
    class Aux501_Units_Republic_501_Snow_Helmet_ARC: Aux501_Units_Republic_501_Snow_Helmet
    {
        displayName = "[501st] ARC SNOW HELM 01 - CT";
        picture = "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\UI\snow_helm_01_ui_ca.paa";
        model = "JLTS_AE_GM\SEA_Helmet_GM_Modular.p3d";
        hiddenSelectionsTextures[] = 
            {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\ARC_snow_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\ARC_snow_helmet_co.paa"
            };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "JLTS_AE_GM\SEA_Helmet_GM_Modular.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Helmet";
            mos = "arc";
            rank = "trooper";
        };
    };
    class Aux501_Units_Republic_501_Snow_Helmet_ARC_NCO: Aux501_Units_Republic_501_Snow_Helmet_ARC
    {
        displayName = "[501st] ARC SNOW HELM 02 - NCO";
        picture = "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\UI\snow_helm_02_ui_ca.paa";
        model = "JLTS_AE_GM\SEA_Helmet_GM_Modular.p3d";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\ARC_snow_helmet_nco_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Snow\data\textures\ARC_snow_helmet_nco_co.paa"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "JLTS_AE_GM\SEA_Helmet_GM_Modular.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Snow_Helmet";
            mos = "arc";
            rank = "nco";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_Snow_Helmet
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "mos",
                "rank"
            };
            class mos
            {
                label = "MOS";
                values[] = 
                {
                    "infantry",
                    "rto",
                    "arc"
                };
                class infantry
                {
                    label = "GI";
                };
                class rto
                {
                    label = "RTO";
                };
                class arc
                {
                    label = "ARC";
                };
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "trooper",
                    "nco"
                };
                class trooper
                {
                    label = "Trooper";
                };
                class nco
                {
                    label = "NCO";
                };
            };
        };
    };
};