class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_UTC_Helmets
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_UTC_Helmet"
        };
    };
};

class CfgWeapons
{
    class H_HelmetB;
 
    class Aux501_Units_Republic_501_Infantry_Helmet_Base: H_HelmetB
    {
        class ItemInfo; 
    };

    class Aux501_Units_Republic_501_UTC_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;   
        displayName = "[501st] UTC HELM 01";
        picture = "\Aux501\Units\Republic\501st\UTC\Helmets\data\UI\utc_cadet_ui_ca.paa";
        model = "kobra\442_equipment\helmets\model\clone\k_cadet_helmet.p3d";
        hiddenSelections[] = {"helmet"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\UTC\Helmets\data\textures\utc_cadet_helmet_co.paa"
        };
        hiddenSelectionsMaterials[]= {};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "kobra\442_equipment\helmets\model\clone\k_cadet_helmet.p3d";
            modelSides[] = {0,1,2,3};
            hiddenSelections[] = {"helmet"};
        };
    };
};