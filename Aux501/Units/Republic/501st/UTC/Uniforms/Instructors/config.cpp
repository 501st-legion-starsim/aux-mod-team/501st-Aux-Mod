class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_UTC_Instructors_Uniform
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_UTC_Instructors_Uniform"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_UTC_Instructors_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] UTC Instructor Uniform";
        picture = "\IBL\characters\officer\data\ui\icon_uniform_black_ca.paa";
        
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_UTC_Instructors_Uniform_Vehicle";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    class Aux501_Units_Republic_501st_UTC_Instructors_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501st_UTC_Instructors_Uniform";
        model = "\IBL\characters\agent\agent_uniform.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\IBL\characters\agent\data\agent_uniform_co.paa"};
        hiddenSelectionsMaterials[] = {};
    };
};
