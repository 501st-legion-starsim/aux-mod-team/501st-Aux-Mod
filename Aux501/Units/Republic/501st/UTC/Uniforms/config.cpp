class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_UTC_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_UTC_Uniform_Blue: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] UTC P2 ARMR 01 - Blue";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Blue";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_UTC_Uniforms";
            color = "blue";
        };
    };
    class Aux501_Units_Republic_501st_UTC_Uniform_Red: Aux501_Units_Republic_501st_UTC_Uniform_Blue
    {
        displayName = "[501st] UTC P2 ARMR 02 - Red";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Red";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_UTC_Uniforms";
            color = "red";
        };
    };
    class Aux501_Units_Republic_501st_UTC_Uniform_Green: Aux501_Units_Republic_501st_UTC_Uniform_Blue
    {
        displayName = "[501st] UTC P2 ARMR 03 - Green";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Green";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_UTC_Uniforms";
            color = "green";
        };
    };
    class Aux501_Units_Republic_501st_UTC_Uniform_Teal: Aux501_Units_Republic_501st_UTC_Uniform_Blue
    {
        displayName = "[501st] UTC P2 ARMR 04 - Teal";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Teal";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_UTC_Uniforms";
            color = "teal";
        };
    };
    class Aux501_Units_Republic_501st_UTC_Uniform_Orange: Aux501_Units_Republic_501st_UTC_Uniform_Blue
    {
        displayName = "[501st] UTC P2 ARMR 05 - Orange";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Orange";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_UTC_Uniforms";
            color = "orange";
        };
    };
    class Aux501_Units_Republic_501st_UTC_Uniform_Pink: Aux501_Units_Republic_501st_UTC_Uniform_Blue
    {
        displayName = "[501st] UTC P2 ARMR 06 - Pink";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Pink";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_UTC_Uniforms";
            color = "pink";
        };
    };
    class Aux501_Units_Republic_501st_UTC_Uniform_Violet: Aux501_Units_Republic_501st_UTC_Uniform_Blue
    {
        displayName = "[501st] UTC P2 ARMR 07 - Violet";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Violet";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_UTC_Uniforms";
            color = "violet";
        };
    };
    class Aux501_Units_Republic_501st_UTC_Uniform_Yellow: Aux501_Units_Republic_501st_UTC_Uniform_Blue
    {
        displayName = "[501st] UTC P2 ARMR 08 - Yellow";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Yellow";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_UTC_Uniforms";
            color = "yellow";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    class Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Blue: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Blue";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_blue_6_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_armor_lower_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Red: Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Blue
    {
        uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Red";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_red_6_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_armor_lower_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Green: Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Blue
    {
        uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Green";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_green_6_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_armor_lower_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Teal: Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Blue
    {
        uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Teal";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_teal_6_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_armor_lower_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Orange: Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Blue
    {
        uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Orange";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_orange_6_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_armor_lower_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Pink: Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Blue
    {
        uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Pink";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_pink_6_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_armor_lower_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Violet: Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Blue
    {
        uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Violet";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_violet_6_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_armor_lower_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Yellow: Aux501_Units_Republic_501st_UTC_Uniform_Vehicle_Blue
    {
        uniformClass = "Aux501_Units_Republic_501st_UTC_Uniform_Yellow";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_yellow_6_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\UTC\Uniforms\data\textures\utc_armor_lower_co.paa"
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_UTC_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "color"
            };
            class color
            {
                label = "Color";
                values[] = 
                {
                    "blue",
                    "red",
                    "green",
                    "teal",
                    "orange",
                    "pink",
                    "violet",
                    "yellow"
                };
                class blue
                {
                    label = "Blue";
                };
                class red
                {
                    label = "Red";
                };
                class green
                {
                    label = "Green";
                };
                class teal
                {
                    label = "Teal";
                };
                class orange
                {
                    label = "Orange";
                };
                class pink
                {
                    label = "Pink";
                };
                class violet
                {
                    label = "Violet";
                };
                class yellow
                {
                    label = "Yellow";
                };
            };
        };
    };
};