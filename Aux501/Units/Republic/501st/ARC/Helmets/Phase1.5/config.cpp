class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_ARC_Helmets_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_ARC_Trooper_Helmet",
            "Aux501_Units_Republic_501st_ARC_Sergeant_Helmet"
        };
    };
};

class cfgWeapons
{
    class H_HelmetB;
    
    class Aux501_Units_Republic_501_Infantry_Helmet_Base: H_HelmetB
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_ARC_Trooper_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;   
        displayName = "[501st] ARC HELM 01 - CT";
        picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\CloneHelmetARC_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Helmets\Phase1.5\data\textures\arc_ct_helmet_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Helmets\Phase1.5\data\textures\arc_ct_helmet_co.paa"
        };
        hiddenSelectionsMaterials[]= 
        {
            "\Aux501\Units\Republic\501st\ARC\Helmets\Phase1.5\data\textures\Clone_helmet_ARC.rvmat",
            "\Aux501\Units\Republic\501st\ARC\Helmets\Phase1.5\data\textures\Clone_helmet_ARC.rvmat"
        };
		class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor2\CloneHelmetARC.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Helmet";
            variant = "ct";
        };
    };

    class Aux501_Units_Republic_501st_ARC_Sergeant_Helmet: Aux501_Units_Republic_501st_ARC_Trooper_Helmet
    {
        displayName = "[501st] ARC HELM 02 - CS";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Helmets\Phase1.5\data\textures\arc_cs_helmet_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Helmets\Phase1.5\data\textures\arc_cs_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Helmet";
            variant = "cs";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_ARC_Helmet
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "ct",
                    "cs"
                };
                class ct
                {
                    label = "CT";
                };
                class cs
                {
                    label = "CS";
                };
            };
        };
    };
};