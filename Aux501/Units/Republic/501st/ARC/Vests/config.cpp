class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_ARC_Vests
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Vests"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_ARC_Vest_00",
            "Aux501_Units_Republic_501_ARC_Vest_01_A",
            "Aux501_Units_Republic_501_ARC_Vest_01_B",
            "Aux501_Units_Republic_501_ARC_Vest_01_C",
            "Aux501_Units_Republic_501_ARC_Vest_01_D",
            "Aux501_Units_Republic_501_ARC_Vest_01_E",

            "Aux501_Units_Republic_501_ARC_Vest_02_A",
            "Aux501_Units_Republic_501_ARC_Vest_02_B",
            "Aux501_Units_Republic_501_ARC_Vest_02_C",
            "Aux501_Units_Republic_501_ARC_Vest_02_D",
            "Aux501_Units_Republic_501_ARC_Vest_02_E",

            "Aux501_Units_Republic_501_ARC_Vest_03_A",
            "Aux501_Units_Republic_501_ARC_Vest_03_B",
            "Aux501_Units_Republic_501_ARC_Vest_03_C",
            "Aux501_Units_Republic_501_ARC_Vest_03_D",
            "Aux501_Units_Republic_501_ARC_Vest_03_E",

            "Aux501_Units_Republic_501_ARC_Vest_04_A",
            "Aux501_Units_Republic_501_ARC_Vest_04_B",
            "Aux501_Units_Republic_501_ARC_Vest_04_C",
            "Aux501_Units_Republic_501_ARC_Vest_04_D",
            "Aux501_Units_Republic_501_ARC_Vest_04_E",

            "Aux501_Units_Republic_501_ARC_Vest_05_A",
            "Aux501_Units_Republic_501_ARC_Vest_05_B",
            "Aux501_Units_Republic_501_ARC_Vest_05_C",
            "Aux501_Units_Republic_501_ARC_Vest_05_D",
            "Aux501_Units_Republic_501_ARC_Vest_05_E"
        };
    };
};

class cfgWeapons
{

    class V_RebreatherB;
    
    class Aux501_Units_Republic_501_Infantry_Vests_Base: V_RebreatherB
    {
        class ItemInfo;
    };

    //ARC Candidate
    class Aux501_Units_Republic_501_ARC_Vest_00: Aux501_Units_Republic_501_Infantry_Vests_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[501st] ARC VEST 00 - Candidate";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestARC_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestARCCadet.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_00_candidate_co.paa"};
        hiddenSelectionsMaterials[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\clone_vest_arc.rvmat"};
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestARCCadet.p3d";
            containerClass = "Supply100";
            hiddenSelections[] = {"camo1"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "candidate";
            kama = "kama_a";
        };
    };
    
    //ARC Trooper
    class Aux501_Units_Republic_501_ARC_Vest_01_A: Aux501_Units_Republic_501_ARC_Vest_00
    {
        displayname = "[501st] ARC VEST 01 - Kama A";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestARC.p3d";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_01_kama_A_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneVestARC.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "ct";
            kama = "kama_a";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_01_B: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 01 - Kama B";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_01_kama_B_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "ct";
            kama = "kama_b";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_01_C: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 01 - Kama C";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_01_kama_C_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "ct";
            kama = "kama_c";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_01_D: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 01 - Kama D";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_01_kama_D_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "ct";
            kama = "kama_d";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_01_E: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 01 - Kama E";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_01_kama_E_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "ct";
            kama = "kama_e";
        };
    };

    //ARC Lance Corporal
    class Aux501_Units_Republic_501_ARC_Vest_02_A: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 02 - Kama A";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_02_kama_A_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "clc";
            kama = "kama_a";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_02_B: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 02 - Kama B";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_02_kama_B_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "clc";
            kama = "kama_b";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_02_C: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 02 - Kama C";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_02_kama_C_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "clc";
            kama = "kama_c";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_02_D: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 02 - Kama D";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_02_kama_D_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "clc";
            kama = "kama_d";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_02_E: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 02 - Kama E";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_02_kama_E_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "clc";
            kama = "kama_e";
        };
    };

    //Platoon ARC
    class Aux501_Units_Republic_501_ARC_Vest_03_A: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 03 - Kama A";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_03_kama_A_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "cp";
            kama = "kama_a";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_03_B: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 03 - Kama B";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_03_kama_B_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "cp";
            kama = "kama_b";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_03_C: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 03 - Kama C";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_03_kama_C_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "cp";
            kama = "kama_c";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_03_D: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 03 - Kama D";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_03_kama_D_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "cp";
            kama = "kama_d";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_03_E: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 03 - Kama E";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_03_kama_E_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "cp";
            kama = "kama_e";
        };
    };

    //Company ARC
    class Aux501_Units_Republic_501_ARC_Vest_04_A: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 04 - Kama A";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_04_kama_A_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "cs";
            kama = "kama_a";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_04_B: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 04 - Kama B";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_04_kama_B_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "cs";
            kama = "kama_b";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_04_C: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 04 - Kama C";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_04_kama_C_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "cs";
            kama = "kama_c";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_04_D: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 04 - Kama D";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_04_kama_D_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "cs";
            kama = "kama_d";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_04_E: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 04 - Kama E";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_04_kama_E_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "cs";
            kama = "kama_e";
        };
    };

    //Battalion ARC
    class Aux501_Units_Republic_501_ARC_Vest_05_A: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 05 - Kama A";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_05_kama_A_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "csm";
            kama = "kama_a";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_05_B: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 05 - Kama B";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_05_kama_B_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "csm";
            kama = "kama_b";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_05_C: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 05 - Kama C";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_05_kama_C_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "csm";
            kama = "kama_c";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_05_D: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 05 - Kama D";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_05_kama_D_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "csm";
            kama = "kama_d";
        };
    };

    class Aux501_Units_Republic_501_ARC_Vest_05_E: Aux501_Units_Republic_501_ARC_Vest_01_A
    {
        displayname = "[501st] ARC VEST 05 - Kama E";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\ARC\Vests\data\ARC_vest_05_kama_E_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_ARC_Vests";
            rank = "csm";
            kama = "kama_e";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_ARC_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "rank",      
                "kama"
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "candidate",
                    "ct",
                    "clc",
                    "cp",
                    "cs",
                    "csm"
                };
                class candidate
                {
                    label = "Candidate";
                };
                class ct
                {
                    label = "CT";
                };
                class clc
                {
                    label = "CLC";
                };
                class cp
                {
                    label = "CP";
                };
                class cs
                {
                    label = "CS";
                };
                class csm
                {
                    label = "CS-M";
                };
            };
            class kama
            {
                label = "Kama";
                values[] = 
                {
                    "kama_a",
                    "kama_b",
                    "kama_c",
                    "kama_d",
                    "kama_e"
                };
                class kama_a
                {
                    label = "A";
                };
                class kama_b
                {
                    label = "B";
                };
                class kama_c
                {
                    label = "C";
                };
                class kama_d
                {
                    label = "D";
                };
                class kama_e
                {
                    label = "E";
                };
            };
        };
    };
};