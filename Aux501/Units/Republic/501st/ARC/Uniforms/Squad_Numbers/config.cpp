class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_ARC_Uniforms_Squad_Numbers
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_ARC_Uniforms"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_1",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_2",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_3",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_2_1",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_2_2",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_2_3",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_3_1",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_3_2",
            "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_3_3"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Uniform_Base;

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        class ItemInfo;
    };

    //ARC Squad Uniforms
    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_1: Aux501_Units_Republic_501st_ARC_Trooper_Uniform
    {
        displayName = "[501st] ARC P2 ARMR 01 - 1-1";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_1";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_1";
            mos = "arc";
            style = "standard";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_2: Aux501_Units_Republic_501st_ARC_Trooper_Uniform
    {
        displayName = "[501st] ARC P2 ARMR 01 - 1-2"; 
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_2";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_2";
            mos = "arc";
            style = "standard";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501st_ARC_Uniform_Trooper_1_3: Aux501_Units_Republic_501st_ARC_Trooper_Uniform
    {
        displayName = "[501st] ARC P2 ARMR 01 - 1-3";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_3";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_1_3";
            mos = "arc";
            style = "standard";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501st_ARC_Uniform_Trooper_2_1: Aux501_Units_Republic_501st_ARC_Trooper_Uniform
    {
        displayName = "[501st] ARC P2 ARMR 01 - 2-1";
       class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_2_1";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_1";
            mos = "arc";
            style = "standard";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501st_ARC_Uniform_Trooper_2_2: Aux501_Units_Republic_501st_ARC_Trooper_Uniform
    {
        displayName = "[501st] ARC P2 ARMR 01 - 2-2";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_2_2";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_2";
            mos = "arc";
            style = "standard";
            rank = "ct";
        };
    };
    
    class Aux501_Units_Republic_501st_ARC_Uniform_Trooper_2_3: Aux501_Units_Republic_501st_ARC_Trooper_Uniform
    {
        displayName = "[501st] ARC P2 ARMR 01 - 2-3";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_2_3";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_2_3";
            mos = "arc";
            style = "standard";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501st_ARC_Uniform_Trooper_3_1: Aux501_Units_Republic_501st_ARC_Trooper_Uniform
    {
        displayName = "[501st] ARC P2 ARMR 01 - 3-1";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_3_1";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_1";
            mos = "arc";
            style = "standard";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501st_ARC_Uniform_Trooper_3_2: Aux501_Units_Republic_501st_ARC_Trooper_Uniform
    {
        displayName = "[501st] ARC P2 ARMR 01 - 3-2";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_3_2";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_2";
            mos = "arc";
            style = "standard";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501st_ARC_Uniform_Trooper_3_3: Aux501_Units_Republic_501st_ARC_Trooper_Uniform
    {
        displayName = "[501st] ARC P2 ARMR 01 - 3-3";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_3_3";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Uniforms";
            unit = "squad_3_3";
            mos = "arc";
            style = "standard";
            rank = "ct";
        };
    };
};

class cfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_1: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_1";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_1_1_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_01_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_2: Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_1
    {
        uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_2";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_1_2_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_01_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_3: Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_1
    {
        uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_1_3";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_1_3_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_01_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_2_1: Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_1
    {
        uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_2_1";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_2_1_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_01_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_2_2: Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_1
    {
        uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_2_2";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_2_2_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_01_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_2_3: Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_1
    {
        uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_2_3";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_2_3_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_01_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_3_1: Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_1
    {
        uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_3_1";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_3_1_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_01_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_3_2: Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_1
    {
        uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_3_2";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_3_2_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_01_co.paa"
        };
    };

    class Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_3_3: Aux501_Units_Republic_501st_ARC_Trooper_Uniform_Vehicle_1_1
    {
        uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform_3_3";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_3_3_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_01_co.paa"
        };
    };
};