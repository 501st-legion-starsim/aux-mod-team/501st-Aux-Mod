class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_ARC
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_ARC_Trooper_Unit",
            "Aux501_Units_Republic_501st_ARC_CP_Unit",
            "Aux501_Units_Republic_501st_ARC_CS_Unit",
            "Aux501_Units_Republic_501st_ARC_CSM_Unit"
        };
        weapons[] = {};
    };
};

class cfgVehicles
{
    class Aux501_Units_Republic_501st_Trooper_Unit;
    class Aux501_Units_Republic_501st_CP_Unit;
    class Aux501_Units_Republic_501st_CS_Unit;
    class Aux501_Units_Republic_501st_Platoon_CSM_Unit;

    class Aux501_Units_Republic_501st_ARC_Trooper_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        displayname = "ARC Trooper";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_MOS";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_01_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_01_co.paa"
        };
        weapons[] = {"Aux501_Weaps_WestarM5_scoped","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_WestarM5_scoped","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_flashnade"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_flashnade"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_ARC_Trooper_Helmet",
            
            "Aux501_Units_Republic_501_ARC_Vest_01_A",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_ARC_Trooper_Helmet",

            "Aux501_Units_Republic_501_ARC_Vest_01_A",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_ARC_Trooper_Uniform";
        backpack = "Aux501_Units_Republic_501_ARC_Backpack";
    };

    class Aux501_Units_Republic_501st_ARC_CP_Unit: Aux501_Units_Republic_501st_CP_Unit
    {
        displayname = "Platoon ARC";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_MOS";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_02_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_02_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15A","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15A","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_ARC_Trooper_Helmet",
            
            "Aux501_Units_Republic_501_ARC_Vest_03_B",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_ARC_Trooper_Helmet",

            "Aux501_Units_Republic_501_ARC_Vest_03_B",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder_white",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_ARC_NCO_Uniform";
        backpack = "Aux501_Units_Republic_501_ARC_Backpack";
    };

    class Aux501_Units_Republic_501st_ARC_CS_Unit: Aux501_Units_Republic_501st_CS_Unit
    {
        displayname = "Company ARC";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_MOS";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_02_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_02_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15C","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15C","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            
            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",
            
            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_ARC_Sergeant_Helmet",
            
            "Aux501_Units_Republic_501_ARC_Vest_04_D",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_ARC_Sergeant_Helmet",

            "Aux501_Units_Republic_501_ARC_Vest_04_D",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_ARC_NCO_Uniform";
        backpack = "Aux501_Units_Republic_501_ARC_Backpack";
    };

    class Aux501_Units_Republic_501st_ARC_CSM_Unit:  Aux501_Units_Republic_501st_Platoon_CSM_Unit
    {
        displayname = "Battalion ARC";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_MOS";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_upper_02_co.paa",
            "\Aux501\Units\Republic\501st\ARC\Uniforms\data\textures\ARC_armor_lower_02_co.paa"
        };
        weapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_DC17","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_DC17","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",

            "Aux501_Weapons_Mags_20mwdp20",
            "Aux501_Weapons_Mags_20mwdp20",

            "Aux501_Weapons_Mags_Thermal_Detonator"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_ARC_Sergeant_Helmet",
            
            "Aux501_Units_Republic_501_ARC_Vest_05_E",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_ARC_CS_Helmet",

            "Aux501_Units_Republic_501_ARC_Vest_05_E",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_ARC_NCO_Uniform";
        backpack = "Aux501_Units_Republic_501_ARC_Backpack";
    };
};