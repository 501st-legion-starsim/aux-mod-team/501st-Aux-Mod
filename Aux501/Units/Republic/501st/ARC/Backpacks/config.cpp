class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_ARC_Backpacks
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks_LR_Large"
        };
        units[] =
        {
            "Aux501_Units_Republic_501_ARC_Backpack"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large;

    class Aux501_Units_Republic_501_ARC_Backpack: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] ARC Backpack 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\Clone_backpack_arc_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneBackpackARC.p3d";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneArmor\data\Clone_backpack_arc_co.paa"
        };
        tf_dialog = "JLTS_clone_lr_programmer_radio_dialog";
        class XtdGearInfo{};
    };
};