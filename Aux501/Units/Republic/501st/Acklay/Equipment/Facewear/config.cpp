class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Equipment_Facewear
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Airborne_Facewear_Display",
            "Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue",
            "Aux501_Units_Republic_501_Airborne_Facewear_Display_removered",
            "Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen",

            "Aux501_Units_Republic_501_Airborne_Facewear_Display_nohud",
            "Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue_nohud",
            "Aux501_Units_Republic_501_Airborne_Facewear_Display_removered_nohud",
            "Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud;

    class Aux501_Units_Republic_501_Airborne_Facewear_Display: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] AB FW HUD 01";
        picture = "\Aux501\Units\Republic\501st\Acklay\Equipment\Facewear\data\CloneHelmetAB_interior_ui_ca.paa";
        ACE_Overlay = "\Aux501\Units\Republic\501st\Acklay\Equipment\Facewear\data\Airborne_facewear_ca.paa";
        ACE_OverlayCracked = "";
        ACE_OverlayDirt = "";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW";
            hud = "hud_on";
            colorblind = "clear";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] AB FW HUD 02 - Blue";
        picture = "\Aux501\Units\Republic\501st\Acklay\Equipment\Facewear\data\CloneHelmetAB_interior_ui_ca.paa";
        ACE_Overlay = "\Aux501\Units\Republic\501st\Acklay\Equipment\Facewear\data\Airborne_facewear_ca.paa";
        ACE_OverlayCracked = "";
        ACE_OverlayDirt = "";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW";
            hud = "hud_on";
            colorblind = "no_blue";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] AB FW HUD 03 - Red";
        picture = "\Aux501\Units\Republic\501st\Acklay\Equipment\Facewear\data\CloneHelmetAB_interior_ui_ca.paa";
        ACE_Overlay = "\Aux501\Units\Republic\501st\Acklay\Equipment\Facewear\data\Airborne_facewear_ca.paa";
        ACE_OverlayCracked = "";
        ACE_OverlayDirt = "";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW";
            hud = "hud_on";
            colorblind = "no_red";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] AB FW HUD 04 - Green";
        picture = "\Aux501\Units\Republic\501st\Acklay\Equipment\Facewear\data\CloneHelmetAB_interior_ui_ca.paa";
        ACE_Overlay = "\Aux501\Units\Republic\501st\Acklay\Equipment\Facewear\data\Airborne_facewear_ca.paa";
        ACE_OverlayCracked = "";
        ACE_OverlayDirt = "";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW";
            hud = "hud_on";
            colorblind = "no_green";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Facewear_Display_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud
    {
        displayname = "[501st] AB FW HUDless Display 01";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW";
            hud = "hud_off";
            colorblind = "clear";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud
    {
        displayname = "[501st] AB FW HUDless Display 02 - Blue";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW";
            hud = "hud_off";
            colorblind = "no_blue";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud
    {
        displayname = "[501st] AB FW HUDless Display 03 - Red";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW";
            hud = "hud_off";
            colorblind = "no_red";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] AB FW HUDless Display 04 - Green";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW";
            hud = "hud_off";
            colorblind = "no_green";
        };
    };
};

class XtdGearModels
{
    class CfgGlasses
    {
        class Aux501_ACEX_Gear_Republic_501st_Airborne_FW
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "hud",
                "colorblind",
            };
            class hud
            {
                label = "Hud";
                values[] = 
                {
                    "hud_on",
                    "hud_off"
                };
                class hud_on
                {
                    label = "Hud On";
                };
                class hud_off
                {
                    label = "Hud Off";
                };
            };
            class colorblind
            {
                label = "Filter";
                values[] = 
                {
                    "clear",
                    "no_blue",
                    "no_red",
                    "no_green"
                };
                class clear
                {
                    label = "Default";
                };
                class no_blue
                {
                    label = "No Blue";
                };
                class no_red
                {
                    label = "No Red";
                };
                class no_green
                {
                    label = "No Green";
                };
            };
        };
    };
};