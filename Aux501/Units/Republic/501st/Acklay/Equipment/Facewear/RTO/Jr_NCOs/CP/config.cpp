class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Jr_NCOs_CP
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Equipment_Facewear_Jr_NCOs_CP"
        };
        units[] =
        {
            "Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP",
            "Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removeblue",
            "Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removered",
            "Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removegreen",

            "Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_nohud",
            "Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removeblue_nohud",
            "Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removered_nohud",
            "Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
        class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP;
        class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removeblue;
        class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removered;
        class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removegreen;

        class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_nohud;
        class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removeblue_nohud;
        class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removered_nohud;
        class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removegreen_nohud;

    //Hud
    //Airborne Corporal Vest
    class Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP
    {
        displayname = "[501st] AB RTO FW HUD VEST 04 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "rto";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removeblue: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removeblue
    {
        displayname = "[501st] AB RTO FW HUD VEST 04 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "rto";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removered: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removered
    {
        displayname = "[501st] AB RTO FW HUD VEST 04 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "rto";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removegreen: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removegreen
    {
        displayname = "[501st] AB RTO FW HUD VEST 04 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "rto";
            rank = "cp";
        };
    };

    //HUDless
    //Airborne Corporal Vest
    class Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_nohud
    {
        displayname = "[501st] AB RTO FW HUDless VEST 04 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "rto";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removeblue_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removeblue_nohud
    {
        displayname = "[501st] AB RTO FW HUDless VEST 04 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "rto";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removered_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removered_nohud
    {
        displayname = "[501st] AB RTO FW HUDless VEST 04 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "rto";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_RTO_Equipment_Facewear_Display_CP_removegreen_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removegreen_nohud
    {
        displayname = "[501st] AB RTO FW HUDless VEST 04 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "rto";
            rank = "cp";
        };
    };
};