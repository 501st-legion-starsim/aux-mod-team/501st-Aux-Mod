class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_MED_Equipment_Facewear_Jr_NCOs_Trooper
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Equipment_Facewear_Jr_NCOs_Standard"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper",
            "Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removeblue",
            "Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removered",
            "Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removegreen",
            
            "Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_nohud",
            "Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removeblue",
            "Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removered",
            "Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removegreen"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen;

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_nohud;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue_nohud;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered_nohud;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen_nohud;

    //HUD
    //Airborne Veteran Trooper Vest
    class Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard
    {
        displayname = "[501st] AB MED FW HUD VEST 02 - CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "medic";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removeblue: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue
    {
        displayname = "[501st] AB MED FW HUD VEST 02 - CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "medic";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removered: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered
    {
        displayname = "[501st] AB MED FW HUD VEST 02 - CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "medic";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removegreen: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen
    {
        displayname = "[501st] AB MED FW HUD VEST 02 - CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "medic";
            rank = "ct";
        };
    };

    //HUDless
    //Airborne Veteran Trooper Vest
    class Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_nohud
    {
        displayname = "[501st] AB MED FW HUDless VEST 02 - CT ";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "medic";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removeblue_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue_nohud
    {
        displayname = "[501st] AB MED FW HUDless VEST 02 - CT ";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "medic";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removered_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered_nohud
    {
        displayname = "[501st] AB MED FW HUDless VEST 02 - CT ";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "medic";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_MED_Equipment_Facewear_Display_Trooper_removegreen_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen_nohud
    {
        displayname = "[501st] AB MED FW HUDless VEST 02 - CT ";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "medic";
            rank = "ct";
        };
    };
};