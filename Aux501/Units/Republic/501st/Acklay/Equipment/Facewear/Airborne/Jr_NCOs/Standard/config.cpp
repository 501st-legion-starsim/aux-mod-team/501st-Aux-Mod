class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Equipment_Facewear_Jr_NCOs_Standard
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen",

            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_nohud",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue_nohud",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered_nohud",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Airborne_Facewear_Display;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removered;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen;
    
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_nohud;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removered_nohud;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen_nohud;

    //Hud
    //Airborne Standard Vest
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard: Aux501_Units_Republic_501_Airborne_Facewear_Display
    {
        displayname = "[501st] AB FW HUD VEST 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "airborne";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue: Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue
    {
        displayname = "[501st] AB FW HUD VEST 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "airborne";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered: Aux501_Units_Republic_501_Airborne_Facewear_Display_removered
    {
        displayname = "[501st] AB FW HUD VEST 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "airborne";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen: Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen
    {
        displayname = "[501st] AB FW HUD VEST 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "airborne";
            rank = "ct";
        };
    };

    //HUDless
    //Airborne Standard Vest
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_nohud: Aux501_Units_Republic_501_Airborne_Facewear_Display_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "airborne";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue_nohud: Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "airborne";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered_nohud: Aux501_Units_Republic_501_Airborne_Facewear_Display_removered_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "airborne";
            rank = "ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen_nohud: Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 01";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "airborne";
            rank = "ct";
        };
    };
};