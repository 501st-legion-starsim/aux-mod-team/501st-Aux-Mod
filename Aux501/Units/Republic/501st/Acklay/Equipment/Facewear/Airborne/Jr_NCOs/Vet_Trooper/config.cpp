class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Equipment_Facewear_Jr_NCOs_Vet_Trooper
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Equipment_Facewear_Jr_NCOs_Standard"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removeblue",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removered",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removegreen",
            
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_nohud",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removeblue",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removered",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removegreen"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen;

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_nohud;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue_nohud;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered_nohud;
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen_nohud;

    //HUD
    //Airborne Veteran Trooper Vest
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard
    {
        displayname = "[501st] AB FW HUD VEST 02 - Vet. CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "airborne";
            rank = "vet_ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removeblue: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue
    {
        displayname = "[501st] AB FW HUD VEST 02 - Vet. CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "airborne";
            rank = "vet_ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removered: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered
    {
        displayname = "[501st] AB FW HUD VEST 02 - Vet. CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "airborne";
            rank = "vet_ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removegreen: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen
    {
        displayname = "[501st] AB FW HUD VEST 02 - Vet. CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "airborne";
            rank = "vet_ct";
        };
    };

    //HUDless
    //Airborne Veteran Trooper Vest
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 02 - Vet. CT ";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "airborne";
            rank = "vet_ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removeblue_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removeblue_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 02 - Vet. CT ";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "airborne";
            rank = "vet_ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removered_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removered_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 02 - Vet. CT ";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "airborne";
            rank = "vet_ct";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Vet_CT_removegreen_nohud: Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_Standard_removegreen_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 02 - Vet. CT ";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "airborne";
            rank = "vet_ct";
        };
    };
};