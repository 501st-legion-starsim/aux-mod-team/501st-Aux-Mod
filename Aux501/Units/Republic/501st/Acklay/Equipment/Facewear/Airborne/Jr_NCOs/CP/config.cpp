class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Equipment_Facewear_Jr_NCOs_CP
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Equipment_Facewear"
        };
        units[] =
        {
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removeblue",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removered",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removegreen",

            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_nohud",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removeblue_nohud",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removered_nohud",
            "Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Airborne_Facewear_Display;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removered;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen;

    class Aux501_Units_Republic_501_Airborne_Facewear_Display_nohud;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removered_nohud;
    class Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen_nohud;

    //Hud
    //Airborne Corporal Vest
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP: Aux501_Units_Republic_501_Airborne_Facewear_Display
    {
        displayname = "[501st] AB FW HUD VEST 03 - CP"; 
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cpl_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "clear";
            mos = "airborne";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removeblue: Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue
    {
        displayname = "[501st] AB FW HUD VEST 03 - CP";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cpl_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_blue";
            mos = "airborne";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removered: Aux501_Units_Republic_501_Airborne_Facewear_Display_removered
    {
        displayname = "[501st] AB FW HUD VEST 03 - CP";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cpl_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_red";
            mos = "airborne";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removegreen: Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen
    {
        displayname = "[501st] AB FW HUD VEST 03 - CP";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cpl_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_on";
            colorblind = "no_green";
            mos = "airborne";
            rank = "cp";
        };
    };

    //HUDless
    //Airborne Corporal Vest
    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_nohud: Aux501_Units_Republic_501_Airborne_Facewear_Display_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 03 - CP";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cpl_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "clear";
            mos = "airborne";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removeblue_nohud: Aux501_Units_Republic_501_Airborne_Facewear_Display_removeblue_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 03 - CP";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cpl_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_blue";
            mos = "airborne";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removered_nohud: Aux501_Units_Republic_501_Airborne_Facewear_Display_removered_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 03 - CP";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cpl_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_red";
            mos = "airborne";
            rank = "cp";
        };
    };

    class Aux501_Units_Republic_501_Airborne_Equipment_Facewear_Display_CP_removegreen_nohud: Aux501_Units_Republic_501_Airborne_Facewear_Display_removegreen_nohud
    {
        displayname = "[501st] AB FW HUDless VEST 03 - CP";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestHeavy_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborneNCO.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cpl_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_FW_Vest_Jr_NCO";
            hud = "hud_off";
            colorblind = "no_green";
            mos = "airborne";
            rank = "cp";
        };
    };
};