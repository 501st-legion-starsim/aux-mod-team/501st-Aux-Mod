class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Vests_Phase2_Jr_NCOs_RTO
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Airborne_RTO_Cadet_Vest",
            "Aux501_Units_Republic_501_Airborne_RTO_Trooper_Vest",
            "Aux501_Units_Republic_501_Airborne_RTO_Vet_CT_Vest",
            "Aux501_Units_Republic_501_Airborne_RTO_CP_Vest"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Airborne_Standard_Vest_01;
    class Aux501_Units_Republic_501_Airborne_Corporal_Vest_03;

    class Aux501_Units_Republic_501_Airborne_RTO_Cadet_Vest: Aux501_Units_Republic_501_Airborne_Standard_Vest_01
    {
        displayName = "[501st] AB RTO VEST 01 - CR-C";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_crc_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "rto";
            rank = "crc";
           
        };
    };
    class Aux501_Units_Republic_501_Airborne_RTO_Trooper_Vest: Aux501_Units_Republic_501_Airborne_RTO_Cadet_Vest
    {
        displayName = "[501st] AB RTO VEST 02 - CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "rto";
            rank = "ct";
        };
    };
    class Aux501_Units_Republic_501_Airborne_RTO_Vet_CT_Vest: Aux501_Units_Republic_501_Airborne_RTO_Cadet_Vest
    {
        displayName = "[501st] AB RTO VEST 03 - Vet. CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "rto";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501_Airborne_RTO_CP_Vest: Aux501_Units_Republic_501_Airborne_Corporal_Vest_03
    {
        displayName = "[501st] AB RTO VEST 04 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\RTO\ab_rto_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "rto";
            rank = "cp";
        };
    };
};