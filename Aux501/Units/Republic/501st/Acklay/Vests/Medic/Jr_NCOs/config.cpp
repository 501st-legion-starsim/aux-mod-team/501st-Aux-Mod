class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Vests_Phase2_Jr_NCOs_Medics
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Airborne_Medic_Cadet_Vest_01",
            "Aux501_Units_Republic_501_Airborne_Medic_CT_Vest_02",
            "Aux501_Units_Republic_501_Airborne_Medic_Vet_CT_Vest_03",
            "Aux501_Units_Republic_501_Airborne_Medic_Corporal_Vest_04"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Airborne_Standard_Vest_01;
    class Aux501_Units_Republic_501_Airborne_Corporal_Vest_03;

    class Aux501_Units_Republic_501_Airborne_Medic_Cadet_Vest_01: Aux501_Units_Republic_501_Airborne_Standard_Vest_01
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] AB MED VEST 01 - CR-C";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_crc_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "medic";
            rank = "crc";
           
        };
    };
    class Aux501_Units_Republic_501_Airborne_Medic_CT_Vest_02: Aux501_Units_Republic_501_Airborne_Standard_Vest_01
    {
        displayName = "[501st] AB MED VEST 02 - CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "medic";
            rank = "ct";
        };
    };
    class Aux501_Units_Republic_501_Airborne_Medic_Vet_CT_Vest_03: Aux501_Units_Republic_501_Airborne_Standard_Vest_01
    {
        displayName = "[501st] AB MED VEST 03 - CM-T";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_vet_ct_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "medic";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501_Airborne_Medic_Corporal_Vest_04: Aux501_Units_Republic_501_Airborne_Corporal_Vest_03
    {
        displayName = "[501st] AB MED VEST 04 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Medic\ab_med_cp_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "medic";
            rank = "cp";
        };
    };
};
