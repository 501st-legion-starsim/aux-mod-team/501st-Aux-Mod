class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Vests_Phase2_Pauldrons
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Pauldrons"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Airborne_CSM_Vest",
            "Aux501_Units_Republic_501_Airborne_CSM_Vest_2",
            "Aux501_Units_Republic_501_Airborne_CSM_Vest_3",

            "Aux501_Units_Republic_501_Airborne_2ndLT_Vest",
            "Aux501_Units_Republic_501_Airborne_2ndLT_Vest_2",
            "Aux501_Units_Republic_501_Airborne_2ndLT_Vest_3",

            "Aux501_Units_Republic_501_Airborne_1stLT_Vest",
            "Aux501_Units_Republic_501_Airborne_1stLT_Vest_2",
            "Aux501_Units_Republic_501_Airborne_1stLT_Vest_3"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon;
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2;
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4;

    class Aux501_Units_Republic_501_Infantry_Vests_2LT;
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_2;
    class Aux501_Units_Republic_501_Infantry_Vests_2LT_4;

    class Aux501_Units_Republic_501_Infantry_Vests_1LT;
    class Aux501_Units_Republic_501_Infantry_Vests_1LT_2;
    class Aux501_Units_Republic_501_Infantry_Vests_1LT_4;

    //Airborne CS-M
    class Aux501_Units_Republic_501_Airborne_CSM_Vest: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayName = "[501st] AB VEST 05 - CS-M";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_csm_kama_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Pauldron_Vests";
            rank = "csm";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Airborne_CSM_Vest_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayName = "[501st] AB VEST 06 - CS-M";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_csm_kama_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Pauldron_Vests";
            rank = "csm";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Airborne_CSM_Vest_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayName = "[501st] AB VEST 07 - CS-M";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_csm_kama_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Pauldron_Vests";
            rank = "csm";
            variant = "reverse";
        };
    };

    //Airborne Officer
    class Aux501_Units_Republic_501_Airborne_2ndLT_Vest: Aux501_Units_Republic_501_Infantry_Vests_2LT
    {
        displayName = "[501st] AB VEST 08 - 2nd LT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_2LT_kama_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Pauldron_Vests";
            rank = "second_Lt";
            variant = "standard";
        };
    };

    class Aux501_Units_Republic_501_Airborne_2ndLT_Vest_2: Aux501_Units_Republic_501_Infantry_Vests_2LT_2
    {
        displayName = "[501st] AB VEST 09 - 2nd LT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_2LT_kama_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Pauldron_Vests";
            rank = "second_Lt";
            variant = "pouch";
        };
    };

    class Aux501_Units_Republic_501_Airborne_2ndLT_Vest_3: Aux501_Units_Republic_501_Infantry_Vests_2LT_4
    {
        displayName = "[501st] AB VEST 10 - 2nd LT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_2LT_kama_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Pauldron_Vests";
            rank = "second_Lt";
            variant = "reverse";
        };
    };

    class Aux501_Units_Republic_501_Airborne_1stLT_Vest: Aux501_Units_Republic_501_Infantry_Vests_1LT
    {
        displayName = "[501st] AB VEST 11 - 1st LT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_1LT_kama_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Pauldron_Vests";
            rank = "first_Lt";
            variant = "standard";
        };
    };

    class Aux501_Units_Republic_501_Airborne_1stLT_Vest_2: Aux501_Units_Republic_501_Infantry_Vests_1LT_2
    {
        displayName = "[501st] AB VEST 12 - 1st LT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_1LT_kama_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Pauldron_Vests";
            rank = "first_Lt";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Airborne_1stLT_Vest_3: Aux501_Units_Republic_501_Infantry_Vests_1LT_4
    {
        displayName = "[501st] AB VEST 13 - 1st LT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_1LT_kama_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Pauldron_Vests";
            rank = "first_Lt";
            variant = "reverse";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_Airborne_Pauldron_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "rank",      
                "variant"
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "csm",

                    "second_Lt",
                    "first_Lt"
                };
                class csm
                {
                    label = "CS-M";
                };
                class second_Lt
                {
                    label = "2nd Lt";
                };
                class first_Lt
                {
                    label = "1st Lt";
                };
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "pouch",
                    "reverse"
                };
                class standard
                {
                    label = "Standard";
                };
                class pouch
                {
                    label = "Pouch";
                };
                class reverse
                {
                    label = "Reverse";
                };
            };
        };
    };
};
    