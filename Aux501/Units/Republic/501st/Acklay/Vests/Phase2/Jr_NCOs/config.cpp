class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Vests_Phase2_Jr_NCOs
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Airborne_Standard_Vest_01",
            "Aux501_Units_Republic_501_Airborne_Veteran_Vest_02",
            "Aux501_Units_Republic_501_Airborne_Corporal_Vest_03",
            "Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper_Bag;
    class Aux501_Units_Republic_501_Infantry_Vests_CP_bag;

    class Aux501_Units_Republic_501_Airborne_Standard_Vest_01: Aux501_Units_Republic_501_Infantry_Vests_Vet_Trooper_Bag
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] AB VEST 01 - CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_ct_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "airborne";
            rank = "ct";
           
        };
    };
    class Aux501_Units_Republic_501_Airborne_Veteran_Vest_02: Aux501_Units_Republic_501_Airborne_Standard_Vest_01
    {
        displayName = "[501st] AB VEST 02 - Vet. CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "airborne";
            rank = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501_Airborne_Corporal_Vest_03: Aux501_Units_Republic_501_Infantry_Vests_CP_bag
    {
        displayName = "[501st] AB VEST 03 - CP";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_vt_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cpl_vest_co.paa"
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "airborne";
            rank = "cp";
        };
    };
    class Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04: Aux501_Units_Republic_501_Airborne_Corporal_Vest_03
    {
        displayName = "[501st] AB VEST 04 - CS";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cs_kama_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Vests\data\textures\Airborne\ab_cpl_vest_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs";
            mos = "airborne";
            rank = "cs";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_Airborne_Vest_Jr_NCOs
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "mos",
                "rank"
            };
            class mos
            {
                label = "MOS";
                hiddenselection = "camo1";
                values[] = 
                {
                    "airborne",
                    "medic",
                    "rto"
                };
                class airborne
                {
                    label = "AB";
                };
                class medic
                {
                    label = "Medic";
                };
                class rto
                {
                    label = "RTO";
                };
            };
            class rank
            {
                label = "Rank";
                hiddenselection = "camo2";
                values[] = 
                {
                    "crc",
                    "ct",
                    "vet_ct",

                    "cp",
                    "sr_cp",
                    "cs",
                    "sr_cs"
                };
                class crc
                {
                    label = "CR-C";
                };
                class ct
                {
                    label = "CT";
                };
                class vet_ct
                {
                    label = "Vet. CT";
                };
                class clc
                {
                    label = "CLC";
                };
                class cp
                {
                    label = "CP";
                };
                class sr_cp
                {
                    label = "Sr. CP";
                };
                class cs
                {
                    label = "CS";
                };
                class sr_cs
                {
                    label = "Sr. CS";
                };
            };
        };
    };
};