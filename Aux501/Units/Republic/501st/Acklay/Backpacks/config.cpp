class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Backpacks
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501st_Infantry_Backpacks_Jumppack_CDV"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12",
            "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Blue",

            "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_1_1",
            "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_1_2",
            "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_1_3",
            "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_1_HQ",

            "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Medic",
            "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_RTO",
            
            "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Instructor"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_White;

    //Airborne Squads
    class Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12: Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_White
    {
        displayName = "[501st] JT-12 Jumppack 01 - Gray";
        picture = "\MRC\JLTS\characters\CloneArmor2\data\ui\Clone_jumppack_jt12_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor2\CloneJumppackJT12.p3d";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneArmor2\data\Clone_jumppack_jt12_co.paa"};
        hiddenSelectionsMaterials[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Backpacks\JT12\Materials\Aux501_JT12_jumppack.rvmat"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_AB_JT12_Jumppack";
            variant = "gray";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Blue: Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12
    {
        displayName = "[501st] JT-12 Jumppack 02 - Blue";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\CloneLegions\data\Clone_501stTrooper_jumppack_JT12_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_AB_JT12_Jumppack";
            variant = "blue";
        };
    };
    
    //Airborne Squads
    class Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_1_1: Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12
    {
        displayName = "[501st] JT-12 Jumppack 03 - 1-1";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Backpacks\JT12\textures\Airborne\Aux501_JT12_jumpack_AB_1_1_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_AB_JT12_Jumppack";
            variant = "squad_1_1";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_1_2: Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12
    {
        displayName = "[501st] JT-12 Jumppack 04 - 1-2";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Backpacks\JT12\textures\Airborne\Aux501_JT12_jumpack_AB_1_2_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_AB_JT12_Jumppack";
            variant = "squad_1_2";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_1_3: Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12
    {
        displayName = "[501st] JT-12 Jumppack 05 - 1-3";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Backpacks\JT12\textures\Airborne\Aux501_JT12_jumpack_AB_1_3_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_AB_JT12_Jumppack";
            variant = "squad_1_3";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_1_HQ: Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12
    {
        displayName = "[501st] JT-12 Jumppack 06 - 1 HQ";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Backpacks\JT12\textures\Airborne\Aux501_JT12_jumpack_AB_1_HQ_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_AB_JT12_Jumppack";
            variant = "hq_1";
        };
    };

    //MOS
    class Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Medic: Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12
    {
        displayName = "[501st] JT-12 Jumppack 07 - Medic";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Backpacks\JT12\textures\Medic\Aux501_JT12_jumpack_Medic_co.paa"
        };
        maximumLoad = 1000;
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_AB_JT12_Jumppack";
            variant = "medic";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_RTO: Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12
    {
        displayName = "[501st] JT-12 Jumppack 08 - RTO";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Backpacks\JT12\textures\RTO\Aux501_JT12_jumpack_RTO_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_AB_JT12_Jumppack";
            variant = "rto";
        };
    };
    
    //Instructor
    class Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Instructor: Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12
    {
        displayName = "[501st] JT-12 Jumppack 09 - Instructor";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Backpacks\JT12\textures\Airborne\Aux501_JT12_jumpack_Pink_co.paa"
        };
        class Aux501_jumppack 
        {
            igniteSound = "\Aux501\Units\Republic\501st\Infantry\Backpacks\Jumppack\sounds\cdv21Start.ogg";
            rechargeRateSecond = 100;
            capacity = 300;
            allowedJumpTypes[] = 
            {
                "Forward",
                "Short",
                "Dash",
                "Cancel"
            };
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_AB_JT12_Jumppack";
            variant = "instructor";
        };
    };
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_Republic_501st_AB_JT12_Jumppack
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "variant" };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "gray",
                    "blue",
                    "squad_1_1",
                    "squad_1_2",
                    "squad_1_3",
                    "hq_1",
                    "medic",
                    "rto",
                    "instructor"
                };
                class gray        { label = "Gray"; };
                class blue        { label = "Blue"; };
                class squad_1_1   { label = "1-1"; };
                class squad_1_2   { label = "1-2"; };
                class squad_1_3   { label = "1-3"; };
                class hq_1        { label = "1 HQ"; };
                class medic       { label = "Medic"; };
                class rto         { label = "RTO"; };
                class instructor  { label = "Instructor"; };
            };
        };
    };
};