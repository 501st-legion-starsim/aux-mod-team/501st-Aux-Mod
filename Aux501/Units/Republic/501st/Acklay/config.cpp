class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Cadet_Unit",
            "Aux501_Units_Republic_501st_Airborne_Trooper_Unit",
            "Aux501_Units_Republic_501st_Airborne_Snr_Trooper_Unit",
            "Aux501_Units_Republic_501st_Airborne_Vet_Trooper_Unit",
            "Aux501_Units_Republic_501st_Airborne_Lance_CP_Unit",

            "Aux501_Units_Republic_501st_Airborne_CP_Unit",
            "Aux501_Units_Republic_501st_Airborne_Snr_CP_Unit",
            "Aux501_Units_Republic_501st_Airborne_CS_Unit",
            "Aux501_Units_Republic_501st_Airborne_Snr_CS_Unit",
            
            "Aux501_Units_Republic_501st_Airborne_CSM_Unit",
            "Aux501_Units_Republic_501st_Airborne_2ndLT_Unit",

            //RTO
            "Aux501_Units_Republic_501st_Airborne_RTO_Trooper_Unit",
            "Aux501_Units_Republic_501st_Airborne_RTO_CP_Unit",
            
            //Medics
            "Aux501_Units_Republic_501st_Airborne_Medic_Trooper_Unit",
            "Aux501_Units_Republic_501st_Airborne_Medic_CP_Unit"
        };
        weapons[] = {};
    };
};

class cfgVehicles
{
    class Aux501_Units_Republic_501st_Cadet_Unit;
    class Aux501_Units_Republic_501st_Trooper_Unit;
    class Aux501_Units_Republic_501st_Snr_Trooper_Unit;
    class Aux501_Units_Republic_501st_Vet_Trooper_Unit;
    class Aux501_Units_Republic_501st_Lance_CP_Unit;
    class Aux501_Units_Republic_501st_CP_Unit;
    class Aux501_Units_Republic_501st_Snr_CP_Unit;
    class Aux501_Units_Republic_501st_CS_Unit;
    class Aux501_Units_Republic_501st_Snr_CS_Unit;
    class Aux501_Units_Republic_501st_Platoon_CSM_Unit;
    class Aux501_Units_Republic_501st_2LT_Unit;

    class Aux501_Units_Republic_501st_RTO_Trooper_Unit;
    class Aux501_Units_Republic_501st_RTO_CP_Unit;

    class Aux501_Units_Republic_501st_Medic_Trooper_Unit;
    class Aux501_Units_Republic_501st_Medic_CP_Unit;

    class Aux501_Units_Republic_501st_Airborne_Cadet_Unit: Aux501_Units_Republic_501st_Cadet_Unit
    {
        displayName = "Cadet";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_crc_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_crc_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Cadet_Helmet",

            "Aux501_Units_Republic_501_Airborne_Standard_Vest_01",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Cadet_Helmet",

            "Aux501_Units_Republic_501_Airborne_Standard_Vest_01",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Cadet_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12";
    };
    class Aux501_Units_Republic_501st_Airborne_Trooper_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        displayName = "Trooper";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_ct_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_ct_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Standard_Vest_01",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Standard_Vest_01",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Trooper_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Blue";
    };
    class Aux501_Units_Republic_501st_Airborne_Snr_Trooper_Unit: Aux501_Units_Republic_501st_Snr_Trooper_Unit
    {
        displayName = "Sr. Trooper";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_snr_ct_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_snr_ct_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Standard_Vest_01",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Standard_Vest_01",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Snr_Trooper_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Blue";
    };
    class Aux501_Units_Republic_501st_Airborne_Vet_Trooper_Unit: Aux501_Units_Republic_501st_Vet_Trooper_Unit
    {
        displayName = "Vet. Trooper";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_vet_ct_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_vet_ct_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Veteran_Vest_02",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Veteran_Vest_02",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Vet_Trooper_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Blue";
    };
    class Aux501_Units_Republic_501st_Airborne_Lance_CP_Unit: Aux501_Units_Republic_501st_Lance_CP_Unit
    {
        displayName = "Lance Corporal";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_cp_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_cp_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Veteran_Vest_02",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Veteran_Vest_02",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_CP_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Blue";
    };

    class Aux501_Units_Republic_501st_Airborne_CP_Unit: Aux501_Units_Republic_501st_CP_Unit
    {
        displayName = "Corporal";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_cp_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_cp_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Corporal_Vest_03",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Corporal_Vest_03"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_CP_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Blue";
    };
    class Aux501_Units_Republic_501st_Airborne_Snr_CP_Unit: Aux501_Units_Republic_501st_Snr_CP_Unit
    {
        displayName = "Sr. Corporal";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_snr_cp_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_snr_cp_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Corporal_Vest_03",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Corporal_Vest_03",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Snr_CP_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Blue";
    };
    class Aux501_Units_Republic_501st_Airborne_CS_Unit: Aux501_Units_Republic_501st_CS_Unit
    {
        displayName = "Sergeant";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_cs_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_cs_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_CS_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Blue";
    };
    class Aux501_Units_Republic_501st_Airborne_Snr_CS_Unit: Aux501_Units_Republic_501st_Snr_CS_Unit
    {
        displayName = "Sr. Sergeant";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_cs_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_snr_cs_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Sergeant_Vest_04",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_501"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Snr_CS_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Blue";
    };
    class Aux501_Units_Republic_501st_Airborne_CSM_Unit: Aux501_Units_Republic_501st_Platoon_CSM_Unit
    {
        displayName = "Sergeant Major";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_csm_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_csm_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_CSM_Vest_2",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_CSM_Vest_2",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Rangefinder"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_CSM_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_1_HQ";
    };
    class Aux501_Units_Republic_501st_Airborne_2ndLT_Unit: Aux501_Units_Republic_501st_2LT_Unit
    {
        displayName = "Officer";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_2ndlt_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_2ndlt_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_2ndLT_Vest_2",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_2ndLT_Vest_2",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Off_visor"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_2ndLT_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_1_HQ";
    };

    //RTOs
    class Aux501_Units_Republic_501st_Airborne_RTO_Trooper_Unit: Aux501_Units_Republic_501st_RTO_Trooper_Unit
    {
        displayName = "RTO";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_ct_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_ct_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_RTO_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Standard_Vest_01",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_RTO_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Standard_Vest_01",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_Trooper_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_RTO";
    };
    class Aux501_Units_Republic_501st_Airborne_RTO_CP_Unit: Aux501_Units_Republic_501st_RTO_CP_Unit
    {
        displayName = "Platoon RTO";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_cp_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_cp_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_RTO_Vet_CT_Helmet",

            "Aux501_Units_Republic_501_Airborne_RTO_CP_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_gray_white_hot"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_RTO_Vet_CT_Helmet",

            "Aux501_Units_Republic_501_Airborne_RTO_CP_Vest",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_gray_white_hot"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_CP_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_RTO";
    };

    //Medics
    class Aux501_Units_Republic_501st_Airborne_Medic_Trooper_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        displayName = "Medic";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_ct_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_ct_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Medic_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Medic_CT_Vest_02",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Medic_Trooper_Helmet",

            "Aux501_Units_Republic_501_Airborne_Medic_CT_Vest_02",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_Trooper_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Medic";
    };
    class Aux501_Units_Republic_501st_Airborne_Medic_CP_Unit: Aux501_Units_Republic_501st_CP_Unit
    {
        displayName = "Platoon Medic";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Airborne";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_cp_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_cp_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Medic_Vet_CT_Helmet",

            "Aux501_Units_Republic_501_Airborne_Medic_Corporal_Vest_04",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red_white_hot"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Airborne_Medic_Vet_CT_Helmet",

            "Aux501_Units_Republic_501_Airborne_Medic_Corporal_Vest_04",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Macrobino_red_white_hot"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_CP_Uniform";
        backpack = "Aux501_Units_Republic_501st_Airborne_Jumppacks_JT12_Medic";
    };
};