class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Medic_Cadet_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Airborne_Medic_Snr_CT_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Airborne_Medic_Vet_CT_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Airborne_Medic_Snr_CP_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Medic_CRC_Uniform",
            "Aux501_Units_Republic_501st_Airborne_Medic_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Airborne_Medic_Snr_CT_Uniform",
            "Aux501_Units_Republic_501st_Airborne_Medic_Vet_CT_Uniform",

            "Aux501_Units_Republic_501st_Airborne_Medic_CP_Uniform",
            "Aux501_Units_Republic_501st_Airborne_Medic_Snr_CP_Uniform"
        };
    };
};

class CfgWeapons

{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    //Airborne Troopers
    class Aux501_Units_Republic_501st_Airborne_Medic_CRC_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] AB MED ARMR 01 - CR-C";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_Cadet_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "medic";
            style = "standard";
            rank = "cr_c";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Medic_Trooper_Uniform: Aux501_Units_Republic_501st_Airborne_Medic_CRC_Uniform
    {
        displayName = "[501st] AB MED ARMR 02 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "medic";
            style = "standard";
            rank = "ct";
        };
    };
    
    class Aux501_Units_Republic_501st_Airborne_Medic_Snr_CT_Uniform: Aux501_Units_Republic_501st_Airborne_Medic_CRC_Uniform
    {
        displayName = "[501st] AB MED ARMR 03 - Sr. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_Snr_CT_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "medic";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Medic_Vet_CT_Uniform: Aux501_Units_Republic_501st_Airborne_Medic_CRC_Uniform
    {
        displayName = "[501st] AB MED ARMR 04 - Vet. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_Vet_CT_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "medic";
            style = "standard";
            rank = "vet_ct";
        };
    };

    //Airborne NCOs
    class Aux501_Units_Republic_501st_Airborne_Medic_CP_Uniform: Aux501_Units_Republic_501st_Airborne_Medic_CRC_Uniform
    {
        displayName = "[501st] AB MED ARMR 05 - CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_CP_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "medic";
            style = "standard";
            rank = "cp";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Medic_Snr_CP_Uniform: Aux501_Units_Republic_501st_Airborne_Medic_CRC_Uniform
    {
        displayName = "[501st] AB MED ARMR 06 - Sr. CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_Snr_CP_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "medic";
            style = "standard";
            rank = "sr_cp";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    class Aux501_Units_Republic_501st_Airborne_Medic_Cadet_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_Cadet_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_crc_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_crc_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Medic_Snr_CT_Uniform_Vehicle: Aux501_Units_Republic_501st_Airborne_Medic_Cadet_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_Snr_CT_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_snr_ct_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_snr_ct_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Medic_Vet_CT_Uniform_Vehicle: Aux501_Units_Republic_501st_Airborne_Medic_Cadet_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_Vet_CT_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_vet_ct_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_vet_ct_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Medic_Snr_CP_Uniform_Vehicle: Aux501_Units_Republic_501st_Airborne_Medic_Cadet_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Airborne_Medic_Snr_CP_Uniform";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_snr_cp_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Medic\ab_med_snr_cp_lower_co.paa"
        };
    };
};