class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_RTO_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_RTO_Cadet_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Airborne_RTO_Snr_CT_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Airborne_RTO_Vet_CT_Uniform_Vehicle",

            //RTO NCOs
            "Aux501_Units_Republic_501st_Airborne_RTO_Snr_CP_Uniform"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_RTO_Cadet_Uniform",
            "Aux501_Units_Republic_501st_Airborne_RTO_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Airborne_RTO_Snr_CT_Uniform",
            "Aux501_Units_Republic_501st_Airborne_RTO_Vet_CT_Uniform",

            //RTO NCOs
            "Aux501_Units_Republic_501st_Airborne_RTO_CP_Uniform",
            "Aux501_Units_Republic_501st_Airborne_RTO_Snr_CP_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Airborne_Cadet_Uniform;

    class Aux501_Units_Republic_501st_Airborne_Trooper_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        class ItemInfo;
    };

    //Airborne RTOs
    class Aux501_Units_Republic_501st_Airborne_RTO_Cadet_Uniform: Aux501_Units_Republic_501st_Airborne_Trooper_Uniform
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] AB RTO ARMR 01 - CR-C";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_Cadet_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "rto";
            rank = "cr_c";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_RTO_Trooper_Uniform: Aux501_Units_Republic_501st_Airborne_Trooper_Uniform
    {
        displayName = "[501st] AB RTO ARMR 02 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "rto";
            rank = "ct";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_RTO_Snr_CT_Uniform: Aux501_Units_Republic_501st_Airborne_Trooper_Uniform
    {
        displayName = "[501st] AB RTO ARMR 03 - Sr. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_Snr_CT_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "rto";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_RTO_Vet_CT_Uniform: Aux501_Units_Republic_501st_Airborne_Trooper_Uniform
    {
        displayName = "[501st] AB RTO ARMR 04 - Vet. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_Vet_CT_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "rto";
            rank = "vet_ct";
        };
    };

    //Airborne NCOs
    class Aux501_Units_Republic_501st_Airborne_RTO_CP_Uniform: Aux501_Units_Republic_501st_Airborne_Trooper_Uniform
    {
        displayName = "[501st] AB RTO ARMR 05 - CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_CP_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "rto";
            rank = "cp";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_RTO_Snr_CP_Uniform: Aux501_Units_Republic_501st_Airborne_Trooper_Uniform

    {
        displayName = "[501st] AB RTO ARMR 06 - Sr. CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_Snr_CP_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "rto";
            rank = "sr_cp";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    //Trooper
    class Aux501_Units_Republic_501st_Airborne_RTO_Cadet_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_crc_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\Airborne\ab_crc_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_Cadet_Uniform";
    };
    class Aux501_Units_Republic_501st_Airborne_RTO_Snr_CT_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_snr_ct_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_snr_ct_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_Snr_CT_Uniform";
    };
    class Aux501_Units_Republic_501st_Airborne_RTO_Vet_CT_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_vet_ct_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_vet_ct_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_Vet_CT_Uniform";
    };

    //NCOs
    class Aux501_Units_Republic_501st_Airborne_RTO_Snr_CP_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_snr_cp_upper_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Uniforms\data\textures\RTO\ab_rto_snr_cp_lower_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Airborne_RTO_Snr_CP_Uniform";
    };
};