class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Cadet_Uniform",
            "Aux501_Units_Republic_501st_Airborne_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Airborne_Snr_Trooper_Uniform",
            "Aux501_Units_Republic_501st_Airborne_Vet_Trooper_Uniform",

            "Aux501_Units_Republic_501st_Airborne_CP_Uniform",
            "Aux501_Units_Republic_501st_Airborne_Snr_CP_Uniform",
            "Aux501_Units_Republic_501st_Airborne_CS_Uniform",
            "Aux501_Units_Republic_501st_Airborne_Snr_CS_Uniform",
            "Aux501_Units_Republic_501st_Airborne_CSM_Uniform",

            "Aux501_Units_Republic_501st_Airborne_2ndLT_Uniform",
            "Aux501_Units_Republic_501st_Airborne_1stLT_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Standard_Uniform;

    class Aux501_Units_Republic_501st_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        class ItemInfo;
    };

    //Airborne Troopers
    class Aux501_Units_Republic_501st_Airborne_Cadet_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] AB ARMR 01 - CR-C";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Cadet_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "airborne";
            style = "standard";
            rank = "cr_c";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Trooper_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        displayName = "[501st] AB ARMR 02 - CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "airborne";
            style = "standard";
            rank = "ct";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Snr_Trooper_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        displayName = "[501st] AB ARMR 03 - Sr. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Snr_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "airborne";
            style = "standard";
            rank = "sr_ct";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Vet_Trooper_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        displayName = "[501st] AB ARMR 04 - Vet. CT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Vet_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "airborne";
            style = "standard";
            rank = "vet_ct";
        };
    };

    //Airborne NCOs
    class Aux501_Units_Republic_501st_Airborne_CP_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        displayName = "[501st] AB ARMR 05 - CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_CP_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "airborne";
            style = "standard";
            rank = "cp";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Snr_CP_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        displayName = "[501st] AB ARMR 06 - Sr. CP";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Snr_CP_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "airborne";
            style = "standard";
            rank = "sr_cp";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_CS_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        displayName = "[501st] AB ARMR 07 - CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_CS_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "airborne";
            style = "standard";
            rank = "cs";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Snr_CS_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        displayName = "[501st] AB ARMR 08 - Sr. CS";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_Snr_CS_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "airborne";
            style = "standard";
            rank = "sr_cs";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_CSM_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        displayName = "[501st] AB ARMR 09 - CS-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_CSM_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "airborne";
            style = "standard";
            rank = "csm";
        };
    };

     //Airborne Officer
    class Aux501_Units_Republic_501st_Airborne_2ndLT_Uniform: Aux501_Units_Republic_501st_Airborne_Cadet_Uniform
    {
        displayName = "[501st] AB ARMR 10 - Officer";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Airborne_2ndLT_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms";
            mos = "airborne";
            style = "standard";
            rank = "second_Lt";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_Airborne_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "mos",
                "rank"
            };
            class mos
            {
                label = "MOS";
                hiddenselection = "camo1";
                values[] = 
                {
                    "airborne",
                    "medic",
                    "rto",
                    "arc"
                };
                class airborne
                {
                    label = "AB";
                };
                class medic
                {
                    label = "Medic";
                };
                class rto
                {
                    label = "RTO";
                };
                class arc
                {
                    label = "ARC";
                };
            };
            class rank
            {
                label = "Rank";
                hiddenselection = "camo2";
                values[] = 
                {
                    "cr_c",
                    "ct",
                    "sr_ct",
                    "vet_ct",
                    "clc",

                    "cp",
                    "sr_cp",
                    "cs",
                    "sr_cs",
                    "csm",
            
                    "second_Lt"
                };
                class cr_c
                {
                    label = "Cadet";
                };
                class ct
                {
                    label = "CT";
                };
                class sr_ct
                {
                    label = "Sr. CT";
                };
                class vet_ct
                {
                    label = "Vet. CT";
                };
                class clc
                {
                    label = "CLC";
                };
                class cp
                {
                    label = "CP";
                };
                class sr_cp
                {
                    label = "Sr. CP";
                };
                class cs
                {
                    label = "CS";
                };
                class sr_cs
                {
                    label = "Sr. CS";
                };
                class csm
                {
                    label = "CS-M";
                };
                class second_Lt
                {
                    label = "Officer";
                };
            };
        };
    };
};