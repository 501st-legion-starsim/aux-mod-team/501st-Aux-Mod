class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Helmets
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Cadet_Helmet",
            "Aux501_Units_Republic_501st_Airborne_Trooper_Helmet",
            "Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet",
            "Aux501_Units_Republic_501st_Airborne_Sergeant_Helmet",
            "Aux501_Units_Republic_501st_Airborne_ARC_Helmet"
        };
    };
};

class cfgWeapons
{
    class H_HelmetB;

    class Aux501_Units_Republic_501_Infantry_Helmet_Base: H_HelmetB
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_Airborne_Cadet_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] AB HELM 01 - CR-C";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetAB_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneArmor\data\Clone_Helmet_AB_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_Helmet_AB_co.paa"
        };
        hiddenSelectionsMaterials[]= 
        {
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\AB_Helmet.rvmat",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\Standard_Visor.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformModel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetAB.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Helmet";
            mos = "airborne";
            variant = "cr_c";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Trooper_Helmet: Aux501_Units_Republic_501st_Airborne_Cadet_Helmet
    {
        displayName = "[501st] AB HELM 02 - CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Airborne\ab_trooper_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Airborne\ab_trooper_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Helmet";
            mos = "airborne";
            variant = "ct";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Veteran_Trooper_Helmet: Aux501_Units_Republic_501st_Airborne_Cadet_Helmet
    {
        displayName = "[501st] AB HELM 03 - Vet. CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Airborne\ab_vet_trooper_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Airborne\ab_vet_trooper_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Helmet";
            mos = "airborne";
            variant = "vet_ct";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_Sergeant_Helmet: Aux501_Units_Republic_501st_Airborne_Cadet_Helmet
    {
        displayName = "[501st] AB HELM 04 - CS";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Airborne\ab_sergeant_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Airborne\ab_sergeant_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Helmet";
            mos = "airborne";
            variant = "cs";
        };
    };
    class Aux501_Units_Republic_501st_Airborne_ARC_Helmet: Aux501_Units_Republic_501st_Airborne_Cadet_Helmet
    {
        displayName = "[501st] AB HELM 05 - ARC";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Airborne\ab_arc_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Airborne\ab_arc_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Helmet";
            mos = "arc";
            variant = "ct";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_Airborne_Helmet
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "mos",
                "variant"
            };
            class mos
            {
                label = "MOS";
                hiddenselection = "camo1";
                values[] = 
                {
                    "airborne",
                    "medic",
                    "rto",
                    "arc"
                };
                class airborne
                {
                    label = "AB";
                };
                class medic
                {
                    label = "Medic";
                };
                class rto
                {
                    label = "RTO";
                };
                class arc
                {
                    label = "ARC";
                };
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "cr_c",
                    "ct",
                    "vet_ct",
                    "cs"
                };
                class cr_c
                {
                    label = "Cadet";
                };
                class ct
                {
                    label = "CT";
                };
                class vet_ct
                {
                    label = "Vet. CT";
                };
                class cs
                {
                    label = "CS";
                };
            };
        };
    };
};