class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Helmets
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Airborne_Helmets"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_RTO_Trooper_Helmet",
            "Aux501_Units_Republic_501st_Airborne_RTO_Vet_CT_Helmet"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501st_Airborne_Cadet_Helmet;

    class Aux501_Units_Republic_501st_Airborne_Trooper_Helmet: Aux501_Units_Republic_501st_Airborne_Cadet_Helmet
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_Airborne_RTO_Trooper_Helmet: Aux501_Units_Republic_501st_Airborne_Trooper_Helmet
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] AB RTO HELM 01 - CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\RTO\ab_rto_ct_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\RTO\ab_rto_ct_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Helmet";
            mos = "rto";
            variant = "ct";
        };
    };

    class Aux501_Units_Republic_501st_Airborne_RTO_Vet_CT_Helmet: Aux501_Units_Republic_501st_Airborne_Trooper_Helmet
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[501st] AB RTO HELM 02 - Vet. CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\RTO\ab_rto_vet_ct_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\RTO\ab_rto_vet_ct_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Helmet";
            mos = "rto";
            variant = "vet_ct";
        };
    };
};