class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Airborne_Helmets_Medic
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Airborne_Medic_Trooper_Helmet",
            "Aux501_Units_Republic_501st_Airborne_Medic_Vet_CT_Helmet"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Helmet_Base;

    class Aux501_Units_Republic_501st_Airborne_Cadet_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_Airborne_Medic_Trooper_Helmet: Aux501_Units_Republic_501st_Airborne_Cadet_Helmet
    {
        displayName = "[501st] AB MED HELM 01 - CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Medic\ab_med_ct_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Medic\ab_med_ct_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Helmet";
            mos = "medic";
            variant = "ct";
        };
    };

    class Aux501_Units_Republic_501st_Airborne_Medic_Vet_CT_Helmet: Aux501_Units_Republic_501st_Airborne_Cadet_Helmet
    {
        displayName = "[501st] AB MED HELM 02 - Vet. CT";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Medic\ab_med_vet_ct_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Acklay\Helmets\data\textures\Medic\ab_med_vet_ct_helmet_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Airborne_Helmet";
            mos = "medic";
            variant = "vet_ct";
        };
    };
};