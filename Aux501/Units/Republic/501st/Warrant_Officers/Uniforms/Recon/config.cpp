class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_WO_Uniforms_Phase2_Recon
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2_Recon",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Warrant_WO2_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Warrant_WO3_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Warrant_WO4_Recon_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Warrant_WO5_Recon_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform",
            "Aux501_Units_Republic_501st_Warrant_WO2_Recon_Uniform",
            "Aux501_Units_Republic_501st_Warrant_WO3_Recon_Uniform",
            "Aux501_Units_Republic_501st_Warrant_WO4_Recon_Uniform",
            "Aux501_Units_Republic_501st_Warrant_WO5_Recon_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Trooper_Uniform;

    class Aux501_Units_Republic_501st_Trooper_Recon_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform: Aux501_Units_Republic_501st_Trooper_Recon_Uniform
    {
        displayName = "[501st] WO P2 ARMR 01 - WO1";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo1";
            style = "recon";
        };
    };
    class Aux501_Units_Republic_501st_Warrant_WO2_Recon_Uniform: Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform
    {
        displayName = "[501st] WO P2 ARMR 02 - WO2";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Warrant_WO2_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo2";
            style = "recon";
        };
    }; 
    class Aux501_Units_Republic_501st_Warrant_WO3_Recon_Uniform: Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform
    {
        displayName = "[501st] WO P2 ARMR 03 - WO3";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Warrant_WO3_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo3";
            style = "recon";
        };
    };
    class Aux501_Units_Republic_501st_Warrant_WO4_Recon_Uniform: Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform
    {
        displayName = "[501st] WO P2 ARMR 04 - WO4";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Warrant_WO4_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo4";
            style = "recon";
        };
    };
    class Aux501_Units_Republic_501st_Warrant_WO5_Recon_Uniform: Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform
    {
        displayName = "[501st] WO P2 ARMR 05 - WO5";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Warrant_WO5_Recon_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo5";
            style = "recon";
        };
    };   
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Trooper_Recon_Uniform_Vehicle;

    class Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_Warrant_WO2_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Warrant_WO2_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_Warrant_WO3_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Warrant_WO3_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_Warrant_WO4_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo4_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Warrant_WO4_Recon_Uniform";
    };
    class Aux501_Units_Republic_501st_Warrant_WO5_Recon_Uniform_Vehicle: Aux501_Units_Republic_501st_Warrant_WO1_Recon_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo5_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Warrant_WO5_Recon_Uniform";
    };
};