class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_WO_Uniforms_Phase2_grenadier
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2_grenadier",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Warrant_WO2_Gren_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Warrant_WO3_Gren_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Warrant_WO4_Gren_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Warrant_WO5_Gren_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform",
            "Aux501_Units_Republic_501st_Warrant_WO2_Gren_Uniform",
            "Aux501_Units_Republic_501st_Warrant_WO3_Gren_Uniform",
            "Aux501_Units_Republic_501st_Warrant_WO4_Gren_Uniform",
            "Aux501_Units_Republic_501st_Warrant_WO5_Gren_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Trooper_Uniform;

    class Aux501_Units_Republic_501st_Trooper_Gren_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform: Aux501_Units_Republic_501st_Trooper_Gren_Uniform
    {
        displayName = "[501st] WO P2 ARMR 01 - WO1";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo1";
            style = "grenade_2x";
        };
    };
    class Aux501_Units_Republic_501st_Warrant_WO2_Gren_Uniform: Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform
    {
        displayName = "[501st] WO P2 ARMR 02 - WO2";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Warrant_WO2_Gren_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo2";
            style = "grenade_2x";
        };
    }; 
    class Aux501_Units_Republic_501st_Warrant_WO3_Gren_Uniform: Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform
    {
        displayName = "[501st] WO P2 ARMR 03 - WO3";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Warrant_WO3_Gren_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo3";
            style = "grenade_2x";
        };
    };
    class Aux501_Units_Republic_501st_Warrant_WO4_Gren_Uniform: Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform
    {
        displayName = "[501st] WO P2 ARMR 04 - WO4";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Warrant_WO4_Gren_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo4";
            style = "grenade_2x";
        };
    };
    class Aux501_Units_Republic_501st_Warrant_WO5_Gren_Uniform: Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform
    {
        displayName = "[501st] WO P2 ARMR 05 - WO5";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Warrant_WO5_Gren_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_WO_Uniforms";
            rank = "wo5";
            style = "grenade_2x";
        };
    };   
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Trooper_Gren_Uniform_Vehicle;

    class Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform_Vehicle: Aux501_Units_Republic_501st_Trooper_Gren_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo1_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform";
    };
    class Aux501_Units_Republic_501st_Warrant_WO2_Gren_Uniform_Vehicle: Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo2_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Infantry\Uniforms\Phase2\data\textures\inf_vet_ct_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Warrant_WO2_Gren_Uniform";
    };
    class Aux501_Units_Republic_501st_Warrant_WO3_Gren_Uniform_Vehicle: Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Warrant_WO3_Gren_Uniform";
    };
    class Aux501_Units_Republic_501st_Warrant_WO4_Gren_Uniform_Vehicle: Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo4_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Warrant_WO4_Gren_Uniform";
    };
    class Aux501_Units_Republic_501st_Warrant_WO5_Gren_Uniform_Vehicle: Aux501_Units_Republic_501st_Warrant_WO1_Gren_Uniform_Vehicle
    {
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo5_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Warrant_Officers\Uniforms\data\textures\wo3_armor_lower_co.paa",
            "\MRC\JLTS\characters\CloneArmor\data\Clone_armor_recon_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_501st_Warrant_WO5_Gren_Uniform";
    };
};