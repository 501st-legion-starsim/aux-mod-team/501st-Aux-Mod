class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Warrant_Officers_Helmets
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Warrant_Officers_Helmet"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Helmet_Base;

    class Aux501_Units_Republic_501_Warrant_Officers_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;   
        displayName = "[501st] WO P2 HELM 01";
        picture = "\SWLB_units\data\ui\icon_SWLB_clone_501stTrooper_helmet_ca.paa";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Warrant_Officers\Helmets\data\textures\warrant_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Warrant_Officers\Helmets\data\textures\warrant_helmet_co.paa"
        };
    };
};