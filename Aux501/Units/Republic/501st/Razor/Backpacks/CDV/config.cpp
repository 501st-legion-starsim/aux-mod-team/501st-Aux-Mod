class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Razor_Backpacks_Jumppack_CDV
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501st_Infantry_Backpacks_Jumppack_CDV"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_Razor",
            "Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_Lightning"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_White;

    class Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_Razor: Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_White
    {
        displayName = "[501st] CDV Jumppack 03 - Razor";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Backpacks\CDV\textures\avi_backpack_cdv_razor_co.paa"
        };
        maximumLoad = 1000;
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_CDV_Jumppack";
            variant = "razor";
        };
    };
    class Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_Lightning: Aux501_Units_Republic_501st_Infantry_Jumppacks_CDV_Razor
    {
        displayName = "[501st] CDV Jumppack 04 - Lightning";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Backpacks\CDV\textures\avi_backpack_cdv_lightning_co.paa"
        };
        maximumLoad = 1000;
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_CDV_Jumppack";
            variant = "lightning";
        };
    };
};