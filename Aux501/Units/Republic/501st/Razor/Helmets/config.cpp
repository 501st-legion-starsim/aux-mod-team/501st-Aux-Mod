class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Razor_Helmets
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_Avi_Helmet
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "p1",
                    "p2",
                    "knife",
                    "lightning",
                    "ground"
                };
                class p1
                {
                    label = "P1";
                };
                class p2
                {
                    label = "P2";
                };
                class knife
                {
                    label = "Knife";
                };
                class lightning
                {
                    label = "Lightning";
                };
                class ground
                {
                    label = "Ground";
                };
            };
        };
    };
};