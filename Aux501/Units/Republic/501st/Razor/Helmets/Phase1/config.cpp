class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Razor_Helmets_Phase1
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Aviation_P1_Helmet"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Blank_Helmet;
    class Aux501_Units_Republic_501_Infantry_Helmet_Trooper: Aux501_Units_Republic_501_Infantry_Blank_Helmet
    {
        class ItemInfo;
    };
    class Aux501_Units_Republic_501st_Aviation_P1_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Trooper
    {
        displayName = "[501st] AVI P1 HELM 01";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_pilot_P2_helmet_ca.paa";
        model="SWLB_CEE\data\SWLB_P1_Pilot_Helmet.p3d";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Helmets\Phase1\data\textures\AVI_standard_p1_helmet_co.paa",
            "swlb_cee\data\swlb_p1_pilot_lifesupport_co.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "swlb_cee\data\swlb_p1_pilot_helmet.rvmat",
            "swlb_cee\data\swlb_p1_pilot_lifesupport.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\SWLB_CEE\data\SWLB_P1_Pilot_Helmet.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Helmet";
            variant = "p1";
        };
    };
};