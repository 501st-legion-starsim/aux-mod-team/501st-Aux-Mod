class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Razor_Helmets_Phase2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet",
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet_Knife",
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet_Lightning",
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet_Ground"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Blank_Helmet;
    class Aux501_Units_Republic_501_Infantry_Helmet_Trooper: Aux501_Units_Republic_501_Infantry_Blank_Helmet
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_501st_Aviation_P2_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Trooper
    {
        displayName = "[501st] AVI P2 HELM 01 - Standard";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_pilot_P2_helmet_ca.paa";
        model="\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P2.p3d";
        hiddenSelections[] = 
        {
            "Camo",
            "Camo1",
            "Camo5",
            "Camo3",

            "Camo2",
            "Camo4"
        };
        hiddenSelectionsTextures[] = 
        {
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase2_Pilot_Razor_co.paa",
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase2_Pilot_Razor_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Helmets\Phase2\data\textures\box\AVI_Standard_helmet_box_co.paa",
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\Standard_Visor.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\3AS\3AS_Characters\Clones\Headgear\3AS_Clone_Pilot_P2.p3d";
            hiddenSelections[] = 
            {
                "Camo",
                "Camo1",
                "Camo5",
                "Camo3",

                "Camo2",
                "Camo4"
            };
            modelSides[]={3,1};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Helmet";
            variant = "p2";
        };
    };
    class Aux501_Units_Republic_501st_Aviation_P2_Helmet_Knife: Aux501_Units_Republic_501st_Aviation_P2_Helmet
    {
        displayName = "[501st] AVI P2 HELM 02 - Knife";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Helmets\Phase2\data\textures\helmet\avi_knife_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Helmets\Phase2\data\textures\helmet\avi_knife_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Helmets\Phase2\data\textures\box\AVI_Standard_helmet_box_co.paa",
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Helmet";
            variant = "knife";
        };
    };
    class Aux501_Units_Republic_501st_Aviation_P2_Helmet_Lightning: Aux501_Units_Republic_501st_Aviation_P2_Helmet
    {
        displayName = "[501st] AVI P2 HELM 03 - Lightning";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Helmets\Phase2\data\textures\helmet\avi_lightning_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Helmets\Phase2\data\textures\helmet\avi_lightning_helmet_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Helmets\Phase2\data\textures\box\AVI_Lightning_helmet_box_co.paa",
            "3AS\3AS_Characters\Clones\Headgear\Textures\PilotP2\Phase_2_Pilot_Tubes_co.paa"
        };
        hiddenSelectionsMaterial[]=
        {
            "",
            "\Aux501\Units\Republic\501st\Razor\Helmets\Phase2\data\textures\Lightning.rvmat"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Helmet";
            variant = "lightning";
        };
    };
    class Aux501_Units_Republic_501st_Aviation_P2_Helmet_Ground: Aux501_Units_Republic_501_Infantry_Blank_Helmet
    {
        displayName = "[501st] AVI P2 HELM 04 - Ground";
        picture = "\SWLB_clones\data\ui\icon_SWLB_clone_pilot_P2_helmet_ca.paa";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Helmets\Phase2\data\textures\helmet\avi_ground_helmet_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Helmet";
            variant = "ground";
        };
    };
};