class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Razor_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Aviation_Midshipman_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Aviation_Flight_Officer_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Aviation_Lieutenant_Junior_Grade_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Aviation_Knife_Uniform_Vehicle",
            "Aux501_Units_Republic_501st_Aviation_Lightning_Uniform_Vehicle"
        };
        weapons[] = 
        {
            "Aux501_Units_Republic_501st_Aviation_Flight_Cadet_Uniform",
            "Aux501_Units_Republic_501st_Aviation_Midshipman_Uniform",
            "Aux501_Units_Republic_501st_Aviation_Flight_Officer_Uniform",

            "Aux501_Units_Republic_501st_Aviation_Ensign_Uniform",
            "Aux501_Units_Republic_501st_Aviation_Lieutenant_Junior_Grade_Uniform",

            "Aux501_Units_Republic_501st_Aviation_Lieutenant_Uniform",
    
            "Aux501_Units_Republic_501st_Aviation_Commander_Uniform",

            "Aux501_Units_Republic_501st_Aviation_Knife_Uniform",
            "Aux501_Units_Republic_501st_Aviation_Lightning_Uniform"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501st_Standard_Uniform;
    class Aux501_Units_Republic_501st_Trooper_Uniform: Aux501_Units_Republic_501st_Standard_Uniform
    {
        class ItemInfo;
    };
    
    //Pilots
    class Aux501_Units_Republic_501st_Aviation_Flight_Cadet_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] AVI P2 ARMR 01 - CX-C";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Aviation_Pilot_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Uniforms";
            rank = "cx_c";
        };
    };
    class Aux501_Units_Republic_501st_Aviation_Midshipman_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] AVI P2 ARMR 02 - CX-E";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Aviation_Midshipman_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Uniforms";
            rank = "cx_e";
        };
    };
    class Aux501_Units_Republic_501st_Aviation_Flight_Officer_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] AVI P2 ARMR 03 - CX-E";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Aviation_Flight_Officer_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Uniforms";
            rank = "cx_x";
        };
    };

    //Pilot NCos
    class Aux501_Units_Republic_501st_Aviation_Ensign_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] AVI P2 ARMR 04 - CX-P";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Aviation_Pilot_NCO_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Uniforms";
            rank = "cx_p";
        };
    };
    class Aux501_Units_Republic_501st_Aviation_Lieutenant_Junior_Grade_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] AVI P2 ARMR 05 - CX-S";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Aviation_Lieutenant_Junior_Grade_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Uniforms";
            rank = "cx_s";
        };
    };

    //Pilot XO
    class Aux501_Units_Republic_501st_Aviation_Lieutenant_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] AVI P2 ARMR 06 - CX-M";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Aviation_Pilot_XO_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Uniforms";
            rank = "cx_m";
        };
    };

    //Pilot Officers
    class Aux501_Units_Republic_501st_Aviation_Commander_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] AVI P2 ARMR 07 - Commander";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Aviation_Pilot_CO_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Uniforms";
            rank = "commander";
        };
    };

    //Knife
    class Aux501_Units_Republic_501st_Aviation_Knife_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] AVI P2 ARMR 09 - Knife";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Aviation_Knife_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Uniforms";
            rank = "knife";
        };
    };

    //Lightning
    class Aux501_Units_Republic_501st_Aviation_Lightning_Uniform: Aux501_Units_Republic_501st_Trooper_Uniform
    {
        displayName = "[501st] AVI P2 ARMR 08 - Lightning";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_501st_Aviation_Lightning_Uniform_Vehicle";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Uniforms";
            rank = "lightning";
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Unit_Base;

    class Aux501_Units_Republic_501st_1LT_Alt_Uniform_Vehicle;

    class Aux501_Units_Republic_501st_Aviation_Midshipman_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501st_Aviation_Midshipman_Uniform";
        hiddenSelectionsTextures[]=
        {
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxe_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxe_armor_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Aviation_Flight_Officer_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501st_Aviation_Flight_Officer_Uniform";
        hiddenSelectionsTextures[]=
        {
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxx_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxe_armor_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Aviation_Lieutenant_Junior_Grade_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501st_Aviation_Lieutenant_Junior_Grade_Uniform";
        hiddenSelectionsTextures[]=
        {
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxs_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxs_armor_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Aviation_Knife_Uniform_Vehicle: Aux501_Units_Republic_501st_Unit_Base
    {
        uniformClass = "Aux501_Units_Republic_501st_Aviation_Knife_Uniform";
        hiddenSelectionsTextures[]=
        {
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_knife_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_knife_armor_lower_co.paa"
        };
    };
    class Aux501_Units_Republic_501st_Aviation_Lightning_Uniform_Vehicle: Aux501_Units_Republic_501st_1LT_Alt_Uniform_Vehicle
    {
        uniformClass = "Aux501_Units_Republic_501st_Aviation_Lightning_Uniform";
        hiddenSelectionsTextures[]=
        {
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_lightning_Armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_lightning_Armor_lower_co.paa"
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_Avi_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "rank"
            };
            class rank
            {
                label = "Rank";
                hiddenselection = "camo2";
                values[] = 
                {
                    "cx_c",
                    "cx_e",
                    "cx_x",

                    "cx_p",
                    "cx_s",
                    "cx_m",

                    "commander",

                    "lightning",
                    "knife"
                };
                class cx_c
                {
                    label = "CX-C";
                };
                class cx_e
                {
                    label = "CX-E";
                };
                class cx_x
                {
                    label = "CX-X";
                };
                class cx_p
                {
                    label = "CX-P";
                };
                class cx_s
                {
                    label = "CX-S";
                };
                class cx_m
                {
                    label = "CX-M";
                };
                class commander
                {
                    label = "Commander";
                };
                class lightning
                {
                    label = "Lightning";
                };
                class knife
                {
                    label = "Knife";
                };
            };
        };
    };
};