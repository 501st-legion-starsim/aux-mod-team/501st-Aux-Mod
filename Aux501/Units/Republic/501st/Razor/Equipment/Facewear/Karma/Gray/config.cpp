class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear_Gray_Kama
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Equipment_Facewear"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama",
            "Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removeblue",
            "Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removered",
            "Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removegreen",

            "Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_nohud",
            "Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removeblue_nohud",
            "Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removered_nohud",
            "Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removegreen_nohud"
        };
        weapons[] = {};
    };
};

class CfgGlasses
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen;

    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered_nohud;
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud;

    //Hud//

    class Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        displayname = "[501st] AVI FW HUD VEST 01 - Kama";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Aviation_Kamas";
            hud = "hud_on";
            colorblind = "clear";
            rank = "cx_m";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removeblue: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue
    {
        displayname = "[501st] AVI FW HUD VEST 01 - Kama - Blue";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Aviation_Kamas";
            hud = "hud_on";
            colorblind = "no_blue";
            rank = "cx_m";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removered: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removered
    {
        displayname = "[501st] AVI FW HUD VEST 01 - Kama - Red";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Aviation_Kamas";
            hud = "hud_on";
            colorblind = "no_red";
            rank = "cx_m";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removegreen: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen
    {
        displayname = "[501st] AVI FW HUD VEST 01 - Kama - Green";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Aviation_Kamas";
            hud = "hud_on";
            colorblind = "no_green";
            rank = "cx_m";
        };
    };
    
    //No Hud//

    class Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_nohud
    {
        displayname = "[501st] AVI FW HUDless VEST 01 - Kama";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Aviation_Kamas";
            hud = "hud_off";
            colorblind = "clear";
            rank = "cx_m";
        };
    };
    //Color Blind Variants
    class Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removeblue_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removeblue_nohud
    {
        displayname = "[501st] AVI FW HUDless VEST 01 - Kama - Blue";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Aviation_Kamas";
            hud = "hud_off";
            colorblind = "no_blue";
            rank = "cx_m";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removered_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] AVI FW HUDless VEST 01 - Kama - Red";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Aviation_Kamas";
            hud = "hud_off";
            colorblind = "no_red";
            rank = "cx_m";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Equipment_Facewear_Phase_2_Display_Gray_Kama_removegreen_nohud: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_removegreen_nohud
    {
        displayname = "[501st] AVI FW HUDless VEST 01 - Kama - Green";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestAirborne.p3d";
        hiddenSelections[]= {"camo1","camo2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_FW_Vest_Aviation_Kamas";
            hud = "hud_off";
            colorblind = "no_green";
            rank = "cx_m";
        };
    };
};