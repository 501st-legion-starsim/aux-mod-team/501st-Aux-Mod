class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Razor
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501st_Aviation_Pilot_Unit",
            "Aux501_Units_Republic_501st_Aviation_Pilot_NCO_Unit",
            "Aux501_Units_Republic_501st_Aviation_Pilot_XO_Unit",
            "Aux501_Units_Republic_501st_Aviation_Pilot_CO_Unit"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501st_Trooper_Unit;

    class Aux501_Units_Republic_501st_Aviation_Pilot_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        displayname = "Pilot";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Pilots";
        hiddenSelectionsTextures[]=
        {
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxc_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxc_armor_lower_co.paa"
        };
        weapons[] = {"Aux501_Weaps_Razorblade","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_Razorblade","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7",
            "Aux501_Weapons_Mags_30mw7"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet",

            "Aux501_Units_Republic_501_Aviation_Vests_Standard_Holster",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet",

            "Aux501_Units_Republic_501_Aviation_Vests_Standard_Holster",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Aviation_Flight_Cadet_Uniform";
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium_Blue";
    };
    class Aux501_Units_Republic_501st_Aviation_Pilot_NCO_Unit: Aux501_Units_Republic_501st_Aviation_Pilot_Unit
    {
        displayname="Pilot NCO";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Pilots";
        hiddenSelectionsTextures[]=
        {
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxp_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxp_armor_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_CP",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_CP",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Aviation_Ensign_Uniform";
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium_Blue";
    };
    class Aux501_Units_Republic_501st_Aviation_Pilot_XO_Unit: Aux501_Units_Republic_501st_Aviation_Pilot_Unit
    {
        displayname="Pilot NCOIC";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Pilots";
        hiddenSelectionsTextures[]=
        {
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxm_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cxm_armor_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet",

            "Aux501_Units_Republic_501_Aviation_Vests_LT_Commander",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet",

            "Aux501_Units_Republic_501_Aviation_Vests_LT_Commander",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Aviation_Lieutenant_Uniform";
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium_Blue";
    };
    class Aux501_Units_Republic_501st_Aviation_Pilot_CO_Unit: Aux501_Units_Republic_501st_Aviation_Pilot_Unit
    {
        displayname="Pilot CO";
        editorSubcategory = "Aux501_Editor_Subcategory_501st_Infantry_Pilots";
        hiddenSelectionsTextures[]=
        {
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cx_armor_upper_co.paa",
            "\Aux501\Units\Republic\501st\Razor\Uniforms\data\textures\AVI_cx_armor_lower_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet",

            "Aux501_Units_Republic_501_Aviation_Vests_Commander_2",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_501st_Aviation_P2_Helmet",

            "Aux501_Units_Republic_501_Aviation_Vests_Commander_2",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_TI",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        uniformClass = "Aux501_Units_Republic_501st_Aviation_Commander_Uniform";
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_LR_Medium_Blue";
    };
};