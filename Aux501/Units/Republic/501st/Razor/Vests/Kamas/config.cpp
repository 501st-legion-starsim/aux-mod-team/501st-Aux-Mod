class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Razor_Vests_Phase2_kamas
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Jr_NCOs"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Aviation_Vests_Kama_White",
            "Aux501_Units_Republic_501_Aviation_Vests_Kama_holsters_White",

            "Aux501_Units_Republic_501_Aviation_Vests_Kama_Gray",
            "Aux501_Units_Republic_501_Aviation_Vests_Kama_holsters_Gray"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_CP_bag;
    class Aux501_Units_Republic_501_Infantry_Vests_CS;

    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon: Aux501_Units_Republic_501_Infantry_Vests_CS
    {
        class ItemInfo;
    };

    //Kamas
    class Aux501_Units_Republic_501_Aviation_Vests_Kama_White: Aux501_Units_Republic_501_Infantry_Vests_CP_bag
    {
        displayname = "[501st] AVI VEST 19 - Karma - CX-M+";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa",
            ""
        };
        hiddenSelectionsMaterials[]= {"\Aux501\Units\Republic\501st\Infantry\Vests\Phase2\data\textures\inf_officer_pauldron.rvmat"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Avi_P2_Kama_Vests";
            rank = "cx_m";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_Kama_holsters_White: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] AVI VEST 20 - Karma - CX-M+";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneVestKama_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneVestKama.p3d";
        hiddenSelectionsTextures[] = {"Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneVestKama.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Avi_P2_Kama_Vests";
            rank = "cx_m";
            variant = "holsters";
        };
    };

    class Aux501_Units_Republic_501_Aviation_Vests_Kama_Gray: Aux501_Units_Republic_501_Aviation_Vests_Kama_White
    {
        displayname = "[501st] AVI VEST 21 - Karma - CX+";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_cmdr_vest_co.paa",
            ""
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Avi_P2_Kama_Vests";
            rank = "cx";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_Kama_holsters_Gray: Aux501_Units_Republic_501_Aviation_Vests_Kama_holsters_White
    {
        displayname = "[501st] AVI VEST 22 - Karma - CX+";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_cmdr_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_Avi_P2_Kama_Vests";
            rank = "cx";
            variant = "holsters";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {        
        class Aux501_ACEX_Gear_Republic_501st_Avi_P2_Kama_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "rank",
                "variant"
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "cx_m",
                    "cx"
                };
                class cx_m
                {
                    label = "CX-M";
                };
                class cx
                {
                    label = "CX";
                };
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "holsters"
                };
                class standard
                {
                    label = "Standard";
                };
                class holsters
                {
                    label = "Holsters";
                };
            };
        };
    };
};