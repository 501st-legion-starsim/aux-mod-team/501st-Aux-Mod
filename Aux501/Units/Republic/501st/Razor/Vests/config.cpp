class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Razor_Vests
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Jr_NCOs",
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2_Pauldrons"
        };
        units[] = 
        {
            "Aux501_Units_Republic_501_Aviation_Vests_CXM",
            "Aux501_Units_Republic_501_Aviation_Vests_CXM_2",
            "Aux501_Units_Republic_501_Aviation_Vests_CXM_3",
            "Aux501_Units_Republic_501_Aviation_Vests_CXM_4",

            "Aux501_Units_Republic_501_Aviation_Vests_LT_Commander",
            "Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_2",
            "Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_3",
            "Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_4",

            "Aux501_Units_Republic_501_Aviation_Vests_Commander",
            "Aux501_Units_Republic_501_Aviation_Vests_Commander_2",
            "Aux501_Units_Republic_501_Aviation_Vests_Commander_3",
            "Aux501_Units_Republic_501_Aviation_Vests_Commander_4",

            "Aux501_Units_Republic_501_Aviation_Vests_Captain",
            "Aux501_Units_Republic_501_Aviation_Vests_Captain_2",
            "Aux501_Units_Republic_501_Aviation_Vests_Captain_3",
            "Aux501_Units_Republic_501_Aviation_Vests_Captain_4"
        };
        weapons[] = {};
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_CS;
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon: Aux501_Units_Republic_501_Infantry_Vests_CS
    {
        class ItemInfo;
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        class ItemInfo;
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        class ItemInfo;
    };
    class Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        class ItemInfo;
    };



    //Razor CX-M
    class Aux501_Units_Republic_501_Aviation_Vests_CXM: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] AVI VEST 03 - CX-M";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            mos = "infantry";
            rank = "cx_m";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_CXM_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayname = "[501st] AVI VEST 04 - CX-M";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            mos = "infantry";
            rank = "cx_m";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_CXM_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3
    {
        displayname = "[501st] AVI VEST 05 - CX-M";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            mos = "infantry";
            rank = "cx_m";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_CXM_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayname = "[501st] AVI VEST 06 - CX-M";
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "cx_m";
            variant = "reverse";
        };
    };

    //Officers

    //Lieutenant Commander
    class Aux501_Units_Republic_501_Aviation_Vests_LT_Commander: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon
    {
        displayname = "[501st] AVI VEST 07 - Lt Cmdr";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "lt_commander";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_2: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_2
    {
        displayname = "[501st] AVI VEST 08 - Lt Cmdr";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "lt_commander";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_3: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_3
    {
        displayname = "[501st] AVI VEST 09 - Lt Commander";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "lt_commander";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_4: Aux501_Units_Republic_501_Infantry_Vests_CSM_Platoon_4
    {
        displayname = "[501st] AVI VEST 10 - Lt Commander";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_LT_cmdr_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "lt_commander";
            variant = "reverse";
        };
    };

    //Commander
    class Aux501_Units_Republic_501_Aviation_Vests_Commander: Aux501_Units_Republic_501_Aviation_Vests_LT_Commander
    {
        displayname = "[501st] AVI VEST 11 - Commander";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_cmdr_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "commander";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_Commander_2: Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_2
    {
        displayname = "[501st] AVI VEST 12 - Commander";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_cmdr_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "commander";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_Commander_3: Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_3
    {
        displayname = "[501st] AVI VEST 13 - Commander";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_cmdr_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "commander";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_Commander_4: Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_4
    {
        displayname = "[501st] AVI VEST 14 - Commander";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_cmdr_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "commander";
            variant = "reverse";
        };
    };

    //Captain
    class Aux501_Units_Republic_501_Aviation_Vests_Captain: Aux501_Units_Republic_501_Aviation_Vests_LT_Commander
    {
        displayname = "[501st] AVI VEST 15 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_cpt_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "captain";
            variant = "standard";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_Captain_2: Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_2
    {
        displayname = "[501st] AVI VEST 16 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_cpt_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "captain";
            variant = "pouch";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_Captain_3: Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_3
    {
        displayname = "[501st] AVI VEST 17 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_cpt_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "captain";
            variant = "nokama";
        };
    };
    class Aux501_Units_Republic_501_Aviation_Vests_Captain_4: Aux501_Units_Republic_501_Aviation_Vests_LT_Commander_4
    {
        displayname = "[501st] AVI VEST 18 - Captain";
        hiddenSelectionsTextures[] = {"\Aux501\Units\Republic\501st\Razor\Vests\data\textures\avi_cpt_vest_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests";
            rank = "captain";
            variant = "reverse";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_P2_Avi_Pauldron_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "rank",      
                "variant"
            };
            class rank
            {
                label = "Rank";
                values[] = 
                {
                    "cx_m",
                    "lt_commander",
                    "commander",
                    "captain"
                };
                class cx_m
                {
                    label = "CX-M";
                };
                class lt_commander
                {
                    label = "LT. Cmmdr";
                };
                class commander
                {
                    label = "Commander";
                };
                class captain
                {
                    label = "Captain";
                };
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "pouch",
                    "nokama",
                    "reverse"
                };
                class standard
                {
                    label = "Standard";
                };
                class pouch
                {
                    label = "Pouch";
                };
                class nokama
                {
                    label = "No Kama";
                };
                class reverse
                {
                    label = "Reverse";
                };
            };
        };
    };
};