class cfgPatches
{
    class Aux501_Patch_Units_Republic_501_Razor_Vests_Phase2_holsters
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_501_Aviation_Vests_Standard_Holster",
            "Aux501_Units_Republic_501_Aviation_Vests_Standard_Holster_black"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Base;
    
    class Aux501_Units_Republic_501_Infantry_Vests_Nanoweave: Aux501_Units_Republic_501_Infantry_Vests_Base
    {
        class ItemInfo;
    };

    //Holster
    class Aux501_Units_Republic_501_Aviation_Vests_Standard_Holster: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        displayname = "[501st] AVI VEST 01 - Razorblade";
        picture = "\Aux501\Units\Republic\501st\Razor\Vests\data\UI\razorblade_holster_ui_ca.paa";
        model = "\Aux501\Units\Republic\501st\Razor\Vests\data\models\Aux501_razorblade_holster.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\razorblade_holster\Aux501_razorblade_holster_co.paa"
        };
        hiddenSelectionsMaterials[]=
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\razorblade_holster\Aux501_razorblade_holster.rvmat"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\Aux501\Units\Republic\501st\Razor\Vests\data\models\Aux501_razorblade_holster.p3d";
            hiddenSelections[] = {"camo1"};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "razorblade";
        };
    };
      class Aux501_Units_Republic_501_Aviation_Vests_Standard_Holster_black: Aux501_Units_Republic_501_Aviation_Vests_Standard_Holster
    {
        displayname = "[501st] AVI VEST 02 - Razorblade";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\Republic\501st\Razor\Vests\data\textures\razorblade_holster\Aux501_razorblade_holster_black_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_P2_GI_Vests_holsters";
            variant = "razorblade2";
        };
    };
};