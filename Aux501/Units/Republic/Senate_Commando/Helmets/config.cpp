class cfgPatches
{
    class Aux501_Patch_Units_Republic_Senate_Commando_Helmets
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Helmets_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_Senate_Commando_Helmet",
            "Aux501_Units_Republic_Senate_Commando_Helmet_NCO",
            "Aux501_Units_Republic_Senate_Commando_Helmet_Commander"
        };
    };
};

class CfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Blank_Helmet;
    
    class Aux501_Units_Republic_501_Infantry_Helmet_Trooper: Aux501_Units_Republic_501_Infantry_Blank_Helmet
    {
        class ItemInfo;
    };

    class Aux501_Units_Republic_Senate_Commando_Helmet: Aux501_Units_Republic_501_Infantry_Helmet_Trooper
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[SC] HELM 01 - Trooper";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetSC_ui_ca.paa";
        model = "\MRC\JLTS\characters\CloneArmor\CloneHelmetSC.p3d";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCTrooper_helmet_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCTrooper_helmet_co.paa"
        };
        hiddenSelectionsMaterials[]= 
        {
            "",
            "\Aux501\Units\Republic\501st\Infantry\Helmets\Phase2\data\Standard_Visor.rvmat"
        };

        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetSC.p3d";
        };
        
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_Senate_Commando_Helmet";
            variant = "trooper";
        };
    };

    class Aux501_Units_Republic_Senate_Commando_Helmet_NCO: Aux501_Units_Republic_Senate_Commando_Helmet
    {
        displayName = "[SC] HELM 02 - NCO";
        model = "\MRC\JLTS\characters\CloneArmor\CloneHelmetSCC.p3d";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCOfficer_helmet_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCOfficer_helmet_co.paa"
        };
        class ItemInfo: ItemInfo
        {
            uniformmodel = "\MRC\JLTS\characters\CloneArmor\CloneHelmetSCC.p3d";
        };

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_Senate_Commando_Helmet";
            variant = "nco";
        };
    };

    class Aux501_Units_Republic_Senate_Commando_Helmet_Commander: Aux501_Units_Republic_Senate_Commando_Helmet_NCO
    {
        displayName = "[SC] HELM 03 - Officer";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCCommander_helmet_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCCommander_helmet_co.paa"
        };

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_Senate_Commando_Helmet";
            variant = "officer";
        };
    };

};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_Senate_Commando_Helmet
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "variant" };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "trooper",
                    "nco",
                    "officer"
                };
                class trooper   { label = "Trooper"; };
                class nco       { label = "NCO"; };
                class officer   { label = "Officer"; };
            };
        };
    };
};