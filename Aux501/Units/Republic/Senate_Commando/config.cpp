class cfgPatches
{
    class Aux501_Patch_Units_Republic_Senate_Commando
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_Republic_Senate_Commando_Trooper_Unit",
            "Aux501_Units_Republic_Senate_Commando_Medic_Unit",
            "Aux501_Units_Republic_Senate_Commando_NCO_Unit",
            "Aux501_Units_Republic_Senate_Commando_Officer_Unit"
        };
        weapons[] = {};
    };
};


class CfgVehicles
{
    class Aux501_Units_Republic_501st_Trooper_Unit;

    class Aux501_Units_Republic_Senate_Commando_Trooper_Unit: Aux501_Units_Republic_501st_Trooper_Unit
    {
        scope = 2;
        scopecurator = 2;
        displayName = "Senate Commando";
        model = "\MRC\JLTS\characters\CloneArmor\CloneArmorSC.p3d";
        editorSubcategory = "Aux501_Editor_Subcategory_Senate_Guard_Clones";
        genericNames = "NATOMen";
        editorPreview = "";
        hiddenSelections[] = {"camo1","camo2","camo3"};
        hiddenSelectionsTextures[] = 
		{
			"\MRC\JLTS\characters\CloneLegions\data\Clone_SCTrooper_armor1_co.paa",
			"\MRC\JLTS\characters\CloneLegions\data\Clone_SCTrooper_armor2_co.paa",
			"\MRC\JLTS\characters\CloneLegions\data\Clone_SCTrooper_shoulder_co.paa"
		};
        hiddenSelectionsMaterials[] = {};
        uniformClass = "Aux501_Units_Republic_Senate_Commando_Trooper_Uniform";
        backpack = "Aux501_Units_Republic_501_Infantry_Backpacks_Invisible";
        weapons[] = {"Aux501_Weaps_DC15A_Wood","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30"
        };
        respawnWeapons[] = {"Aux501_Weaps_DC15A_Wood","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_Senate_Commando_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_Nanoweave",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        Items[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_ids_police"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_Senate_Commando_Helmet",

            "Aux501_Units_Republic_501_Infantry_Vests_Standard_Holster",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnItems[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_ids_police"
        };

    };
    class Aux501_Units_Republic_Senate_Commando_Medic_Unit: Aux501_Units_Republic_Senate_Commando_Trooper_Unit
    {
        displayName = "Senate Commando Medic";
        icon = "iconManMedic";
        hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCMedic_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCTrooper_armor2_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCTrooper_shoulder_co.paa"
        };
        uniformClass = "Aux501_Units_Republic_Senate_Commando_Medic_Uniform";
        Items[] =
        {
            "ACE_quikclot",
            "ACE_quikclot",
            "ACE_quikclot",
            "ACE_packingBandage",
            "ACE_packingBandage",
            "ACE_packingBandage",
            "ACE_elasticBandage",
            "ACE_elasticBandage",
            "ACE_elasticBandage",
            "ACE_tourniquet",
            "ACE_tourniquet",

            "JLTS_intel_holoProjector",
            "JLTS_ids_police"
        };
        respawnItems[] =
        {
            "ACE_quikclot",
            "ACE_quikclot",
            "ACE_quikclot",
            "ACE_packingBandage",
            "ACE_packingBandage",
            "ACE_packingBandage",
            "ACE_elasticBandage",
            "ACE_elasticBandage",
            "ACE_elasticBandage",
            "ACE_tourniquet",
            "ACE_tourniquet",

            "JLTS_intel_holoProjector",
            "JLTS_ids_police"
        };
    };

    class Aux501_Units_Republic_Senate_Commando_NCO_Unit: Aux501_Units_Republic_Senate_Commando_Trooper_Unit
    {
        displayName = "Senate Commando NCO";
        icon = "iconManLeader";
        weapons[] = {"Aux501_Weaps_DC15A_Wood","Aux501_Weaps_DC17A","Throw","Put"};
        respawnWeapons[] = {"Aux501_Weaps_DC15A_Wood","Aux501_Weaps_DC17A","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30",
            "Aux501_Weapons_Mags_20mw30"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_Senate_Commando_Helmet_NCO",

            "Aux501_Units_Republic_501_Infantry_Vests_Holster_leg",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_black_hot",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_Senate_Commando_Helmet_NCO",

            "Aux501_Units_Republic_501_Infantry_Vests_Holster_leg",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_black_hot",

            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
    };
    class Aux501_Units_Republic_Senate_Commando_Officer_Unit: Aux501_Units_Republic_Senate_Commando_NCO_Unit
    {
        displayName = "Senate Commando Officer";
        icon = "iconManOfficer";
        uniformClass = "Aux501_Units_Republic_Senate_Commando_Officer_Uniform";
		hiddenSelectionsTextures[] = 
        {
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCCommander_armor1_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCCommander_armor2_co.paa",
            "\MRC\JLTS\characters\CloneLegions\data\Clone_SCCommander_shoulder_co.paa"
        };
        linkedItems[] = 
        {
            "Aux501_Units_Republic_Senate_Commando_Helmet_Commander",

            "Aux501_Units_Republic_501_Infantry_Vests_Holster_leg",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_black_hot",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnLinkedItems[] =
        {
            "Aux501_Units_Republic_Senate_Commando_Helmet_Commander",

            "Aux501_Units_Republic_501_Infantry_Vests_Holster_leg",

            "Aux501_Republic_501_Infantry_Equipment_NVGs_Integrated_NV_TI_black_hot",
            
            "JLTS_clone_comlink",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
    };
};

class CfgGroups
{
    class West
    {
        class Aux501_FactionClasses_Republic
        {
            name = "[501st] Republic";
            class Aux501_Group_Editor_Category_Senate_Commando_Groups
            {
                name = "Senate Commandos";
                class Aux501_republic_senate_commando_team
                {
                    name = "Fireteam";
                    side = 1;
                    faction = "Aux501_FactionClasses_Republic";
                    icon = "\A3\ui_f\data\map\markers\nato\b_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "Corporal";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {5,-5,0};
                        rank = "PRIVATE";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Trooper_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-5,-5,0};
                        rank = "PRIVATE";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Trooper_Unit";
                    };
                    class Unit3
                    {
                        position[] = {10,-10,0};
                        rank = "PRIVATE";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Trooper_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-10,-10,0};
                        rank = "PRIVATE";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Medic_Unit";
                    };
                };

                class Aux501_republic_senate_commando_squad
                {
                    name = "Rifle Squad";
                    side = 1;
                    faction = "Aux501_FactionClasses_Republic";
                    icon = "\A3\ui_f\data\map\markers\nato\b_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Officer_Unit";
                    };
                    class Unit1
                    {
                        position[] = {5,-5,0};
                        rank = "CORPORAL";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_NCO_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-5,-5,0};
                        rank = "PRIVATE";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_NCO_Unit";
                    };
                    class Unit3
                    {
                        position[] = {10,-10,0};
                        rank = "PRIVATE";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Trooper_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-10,-10,0};
                        rank = "PRIVATE";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Trooper_Unit";
                    };

                    class Unit5
                    {
                        position[] = {15,-15,0};
                        rank = "CORPORAL";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Trooper_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-15,-15,0};
                        rank = "PRIVATE";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Trooper_Unit";
                    };
                    class Unit7
                    {
                        position[] = {20,-20,0};
                        rank = "PRIVATE";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Medic_Unit";
                    };
                    class Unit8
                    {
                        position[] = {-20,-20,0};
                        rank = "PRIVATE";
                        side = 1;
                        vehicle = "Aux501_Units_Republic_Senate_Commando_Medic_Unit";
                    };
                };
            };
        };
    };
};