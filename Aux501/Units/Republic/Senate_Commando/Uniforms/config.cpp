class cfgPatches
{
    class Aux501_Patch_Units_Republic_Senate_Commando_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Republic_Senate_Commando_Trooper_Uniform",
            "Aux501_Units_Republic_Senate_Commando_Medic_Uniform",
            "Aux501_Units_Republic_Senate_Commando_Officer_Uniform"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class itemInfo;
    };

    class Aux501_Units_Republic_Senate_Commando_Trooper_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[SC] ARMR 01 - Trooper";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_Senate_Commando_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_Senate_Commando_Uniforms";
            variant = "trooper";
        };
    };

    class Aux501_Units_Republic_Senate_Commando_Medic_Uniform: Aux501_Units_Republic_Senate_Commando_Trooper_Uniform
    {
        displayName = "[SC] ARMR 02 - Medic";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_Senate_Commando_Medic_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_Senate_Commando_Uniforms";
            variant = "medic";
        };
    };

    class Aux501_Units_Republic_Senate_Commando_Officer_Uniform: Aux501_Units_Republic_Senate_Commando_Trooper_Uniform
    {
        displayName = "[SC] ARMR 03 - Officer";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Republic_Senate_Commando_Officer_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_Senate_Commando_Uniforms";
            variant = "officer";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_Senate_Commando_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "variant" };
            class variant
            {
                label = "Variant";
                values[] = 
                { 
                    "trooper",
                    "medic",
                    "officer" 
                };
                class trooper   { label = "Trooper"; };
                class medic     { label = "Medic"; };
                class officer   { label = "Officer"; };
            };
        };
    };
};