class cfgPatches
{
    class Aux501_Patch_Units_Civilians_Republic_Engineer
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgVehicles
{
    class C_Man_ConstructionWorker_01_Blue_F;

    class Aux501_Units_Civilians_Republic_Engineer: C_Man_ConstructionWorker_01_Blue_F
    {
        scope = 2;
        scopecurator = 2;
        author = "501st Aux Team";
        displayname = "Engineer";
        editorpreivew = "";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"Aux501\Units\Civilians\Gear\Uniforms\data\textures\republic_engineer_uniform.paa"}; 
        hiddenSelectionsMaterials[] = {"Aux501\Units\Civilians\Gear\Uniforms\data\materials\republic_engineer_coverall.rvmat"};
        icon = "iconManEngineer";
        faction = "Aux501_FactionClasses_Civilians";
        editorCategory = "Aux501_Editor_Category_Civilians";
        editorSubcategory = "Aux501_Editor_Subcategory_Republic";
        uniformClass = "Aux501_Units_Civilians_Uniforms_Republic_Engineer";
        identityTypes[] = 
        {
            "LanguageENG_F",
            "Head_EURO",
            "Head_Asian",
            "Head_Greek",
            "Head_m_mirialan",
            "Head_m_zelosian",
            "Head_m_zeltron"
        };
        headgearList[] = {"H_Construction_earprot_orange_F",1};
        linkedItems[] =  
        {
            "H_Construction_earprot_orange_F",
            "Aux501_Units_Civilians_Vests_Republic_Engineer",
            "G_Lowprofile",
            "ItemWatch"
        };
        respawnlinkedItems[] =  
        {
            "H_Construction_earprot_orange_F",
            "Aux501_Units_Civilians_Vests_Republic_Engineer",
            "G_Lowprofile",
            "ItemWatch"
        };
        Items[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ",
            "JLTS_ids_rep_civ",
            "JLTS_credit_card"
        };
        RespawnItems[] =
        {
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ",
            "JLTS_ids_rep_civ",
            "JLTS_credit_card"
        };
        class Wounds
        {
            tex[] = {};
            mat[] = 
            {
                "a3\Characters_F_Orange\Uniforms\Data\c_constructioncoverall.rvmat",
                "a3\Characters_F_Orange\Uniforms\Data\c_constructioncoverall_injury.rvmat",
                "a3\Characters_F_Orange\Uniforms\Data\c_constructioncoverall_injury.rvmat"
            };
        };
    };
};