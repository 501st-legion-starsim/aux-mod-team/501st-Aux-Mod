class cfgPatches
{
    class Aux501_Patch_Units_Civilians_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_Civilians_Uniforms_Republic_Engineer",
            "Aux501_Units_Civilians_Uniforms_Republic_Press",
            "Aux501_Units_Civilians_Uniforms_Republic_Factory_Worker",
            "Aux501_Units_Civilians_Uniforms_Republic_Scientist",
            
            "Aux501_Units_Civilians_Uniforms_Separatist_Scientist"
        };
    };
};

class cfgWeapons
{
    class UniformItem;

    class U_C_ConstructionCoverall_Blue_F;

    class Aux501_Units_Civilians_Uniforms_Republic_Engineer: U_C_ConstructionCoverall_Blue_F
    {
        author = "501st Aux Team";
        displayname = "[REP] Engineer Uniform";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"Aux501\Units\Civilians\Gear\Uniforms\data\textures\republic_engineer_uniform.paa"};
        hiddenSelectionsMaterials[]= {"Aux501\Units\Civilians\Gear\Uniforms\data\materials\republic_engineer_coverall.rvmat"};
        class ItemInfo: UniformItem
        {
            uniformModel = "-";
            uniformClass = "Aux501_Units_Civilians_Republic_Engineer";
            containerClass = "Supply40";
            mass = 50;
            modelSides[] = {6};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Civilian_Uniforms_Republic";
            variant = "engineer";
        };
    };

    class Aux501_Units_Civilians_Uniforms_Republic_Press: Aux501_Units_Civilians_Uniforms_Republic_Engineer
    {
        displayname = "[REP] HoloNet Reporter Uniform";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Civilians_Republic_Press";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Civilian_Uniforms_Republic";
            variant = "reporter";
        };
    };
    
    class Aux501_Units_Civilians_Uniforms_Republic_Factory_Worker: Aux501_Units_Civilians_Uniforms_Republic_Engineer
    {
        displayname = "[REP] Factory Worker Uniform";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Civilians_Republic_Factory_Worker";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Civilian_Uniforms_Republic";
            variant = "factory_worker";
        };
    };
    class Aux501_Units_Civilians_Uniforms_Republic_Scientist: Aux501_Units_Civilians_Uniforms_Republic_Engineer
    {
        displayname = "[REP] Scientist Uniform";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Civilians_Republic_Scientist";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Civilian_Uniforms_Republic";
            variant = "scientist";
        };
    };
    class Aux501_Units_Civilians_Uniforms_Separatist_Scientist: Aux501_Units_Civilians_Uniforms_Republic_Engineer
    {
        displayname = "[CIS] Scientist Uniform";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_Civilians_Separatist_Scientist";
        };
        class XtdGearInfo{};
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Civilian_Uniforms_Republic
        {
            label = "";
            author = "501st Aux Team";
            options[] = { "variant" };
            class variant
            {
                label = "Variants";
                values[] = 
                {
                    "scientist",
                    "factory_worker",
                    "engineer",
                    "reporter"
                };
                class scientist        { label = "Scientist"; };
                class factory_worker   { label = "Factory Worker"; };
                class engineer         { label = "Engineer"; };
                class reporter         { label = "Reporter"; };
            };
        };
    };
};