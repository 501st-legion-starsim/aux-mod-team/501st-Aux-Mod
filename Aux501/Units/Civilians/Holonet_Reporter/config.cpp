class cfgPatches
{
    class Aux501_Patch_Units_Civilians_Holonet_Reporter
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Civilians_Republic_Engineer;

    class Aux501_Units_Civilians_Republic_Press: Aux501_Units_Civilians_Republic_Engineer
    {
        scope = 2;
        scopeCurator = 2;
        displayname = "HoloNet Reporter";
        model = "\optre_unsc_units\oni\research_l.p3d";
        hiddenSelections[] = {"camo"};
        hiddenSelectionsTextures[] = {"optre_unsc_units\oni\data\research_uniform_co.paa"}; 
        icon = "iconMan";
        uniformClass = "Aux501_Units_Civilians_Uniforms_Republic_Press";
        headgearList[] = {"H_PASGT_neckprot_blue_press_F",1};
        linkedItems[] =  
        {
            "H_PASGT_neckprot_blue_press_F",
            "V_Press_F",
            "OPTRE_HUD_w_Glasses",
            "ItemWatch"
        };
        respawnlinkedItems[] =  
        {
            "H_PASGT_neckprot_blue_press_F",
            "V_Press_F",
            "OPTRE_HUD_w_Glasses",
            "ItemWatch"
        };
    };
};