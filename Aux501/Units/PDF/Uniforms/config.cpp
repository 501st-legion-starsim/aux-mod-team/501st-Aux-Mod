class cfgPatches
{
    class Aux501_Patch_Units_PDF_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_PDF_Trooper_Uniform",
            "Aux501_Units_PDF_Medic_Uniform",
            "Aux501_Units_PDF_Marksman_Uniform",
            "Aux501_Units_PDF_Heavy_Uniform",
            "Aux501_Units_PDF_Crew_Uniform",
            "Aux501_Units_PDF_Guard_Uniform",
            "Aux501_Units_PDF_Officer_Uniform"
        };
    };
};

class CfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };


    class Aux501_Units_PDF_Trooper_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[PDF] Uniform 01 - Trooper";
        picture = "\ls_armor_bluefor\uniform\_ui\lsd_orsf_trooper_uniform.paa";
		model = "\A3\Characters_F\Common\Suitpacks\suitpack_blufor_diver";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_PDF_Trooper_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Uniforms";
            variant = "trooper";
        };
    };

    class Aux501_Units_PDF_Medic_Uniform: Aux501_Units_PDF_Trooper_Uniform
    {
        displayName = "[PDF] Uniform 02 - Medic";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_PDF_Medic_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Uniforms";
            variant = "medic";
        };
    };

    class Aux501_Units_PDF_Marksman_Uniform: Aux501_Units_PDF_Trooper_Uniform
    {
        displayName = "[PDF] Uniform 03 - Marksman";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_PDF_Marksman_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Uniforms";
            variant = "marksman";
        };
    };

    class Aux501_Units_PDF_Heavy_Uniform: Aux501_Units_PDF_Trooper_Uniform
    {
        displayName = "[PDF] Uniform 04 - Heavy Weapons Guy";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_PDF_Heavy_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Uniforms";
            variant = "heavy";
        };
    };

    class Aux501_Units_PDF_Crew_Uniform: Aux501_Units_PDF_Trooper_Uniform
    {
        displayName = "[PDF] Uniform 05 - Crew";
        picture = "\SFA_Equipment_R\uniform\Navy\data\ui\SFA_Officer_Uniform_ca.paa";
		model = "SFA_Equipment_R\Uniform\Trooper\Combat_Engineer_uniform.p3d";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_PDF_Crewman_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Uniforms";
            variant = "crew";
        };
    };

    class Aux501_Units_PDF_Guard_Uniform: Aux501_Units_PDF_Trooper_Uniform
    {
        displayName = "[PDF] Uniform 06 - Honor Guard";
        picture = "\SFA_Equipment_R\uniform\Navy\data\ui\SFA_NavyTrooper_Uniform_ca.paa";
		model = "SFA_Equipment_R\Uniform\Navy\Navy_Trooper_uniform.p3d";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_PDF_Guard_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Uniforms";
            variant = "guard";
        };
    };

    class Aux501_Units_PDF_Officer_Uniform: Aux501_Units_PDF_Trooper_Uniform
    {
        displayName = "[PDF] Uniform 07 - Officer";
        picture = "\SFA_Equipment_R\uniform\Navy\data\ui\SFA_NavyHeavyTrooper_Uniform_ca.paa";
		model = "SFA_Equipment_R\Uniform\Navy\Navy_Trooper_uniform.p3d";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_PDF_Officer_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Uniforms";
            variant = "officer";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_PDF_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "trooper",
                    "medic",
                    "marksman",
                    "heavy",
                    "crew",
                    "guard",
                    "officer"
                };
                class trooper
                {
                    label = "Trooper";
                };
                class medic
                {
                    label = "Medic";
                };
                class marksman
                {
                    label = "Marksman";
                };
                class heavy
                {
                    label = "Heavy";
                };
                class crew
                {
                    label = "Crew";
                };
                class guard
                {
                    label = "Guard";
                };
                class officer
                {
                    label = "Officer";
                };
            };
        };
    };
};