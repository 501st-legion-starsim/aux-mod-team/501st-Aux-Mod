class cfgPatches
{
    class Aux501_Patch_Units_PDF_Helmets
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_PDF_Helmet_Trooper",
            "Aux501_Units_PDF_Helmet_Crewman",
            "Aux501_Units_PDF_Helmet_Guard",
            "Aux501_Units_PDF_Helmet_Officer",
            "Aux501_Units_PDF_Helmet_Commander"
        };
    };
};

class cfgWeapons
{
    class H_HelmetB;

    class Aux501_Units_Republic_501_Infantry_Helmet_Base: H_HelmetB
    {
        class Iteminfo;
    };

    class Aux501_Units_PDF_Helmet_Trooper: Aux501_Units_Republic_501_Infantry_Helmet_Base
    {
        scope = 2;
        scopeArsenal = 2;
        displayName = "[PDF] HELM 01 - Trooper";
        picture = "\ls_armor_bluefor\helmet\_ui\icon_orsf_standard_helmet_ca.paa";
		model = "ls_armor_bluefor\helmet\orsf\trooper\ls_orsf_trooper_helmet.p3d";
		hiddenSelections[] = {"camo","visor"};
		hiddenSelectionsTextures[] = 
        {
            "ls_armor_bluefor\helmet\orsf\trooper\Data\helmet_co.paa",
            "ls_armor_bluefor\helmet\orsf\trooper\Data\helmet_co.paa"
        };
        subItems[] = {};
        hiddenSelectionsMaterials[] = {};
        class Iteminfo: ItemInfo
        {
            mass = 10;
            uniformModel = "ls_armor_bluefor\helmet\orsf\trooper\ls_orsf_trooper_helmet.p3d";
            hiddenselections[] = {"camo","visor"};
            modelsides[] = {6};
            class HitpointsProtectionInfo
            {
                class Head
                {
                    hitpointname = "HitHead";
                    armor = 15;
                    passThrough = 0.1;
                };
            };
        };

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Headgear";
            variant = "trooper";
        };
    };

    class Aux501_Units_PDF_Helmet_Crewman: Aux501_Units_PDF_Helmet_Trooper
    {
        displayName = "[PDF] HELM 02 - Crew";
        picture = "\WarMantle\WM_Rebel_Units\Data\UI\Endor_Rebel_helm_ca.paa";
		model = "WarMantle\WM_Rebel_Units\helm.p3d";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"WarMantle\WM_Rebel_Units\Data\helmet_co.paa"};
        class Iteminfo: ItemInfo
        {
            uniformModel = "WarMantle\WM_Rebel_Units\helm.p3d";
            hiddenselections[] = {"camo"};
            modelsides[] = {3};
        };

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Headgear";
            variant = "crew";
        };
    };

    class Aux501_Units_PDF_Helmet_Guard: Aux501_Units_PDF_Helmet_Trooper
    {
        displayName = "[PDF] HELM 03 - Honor Guard";
        picture = "\SFA_Equipment_R\Helmet\Navy\data\ui\SFA_KOTR_Trooper_Helmet_ca.paa";
		model = "SFA_Equipment_R\Helmet\Navy\KOTOR_Trooper_Helmet.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"SFA_Equipment_R\Helmet\Navy\data\Kotor_Trooper_Helmet_co.paa"};
        class Iteminfo: ItemInfo
        {
            uniformModel = "SFA_Equipment_R\Helmet\Navy\KOTOR_Trooper_Helmet.p3d";
            hiddenselections[] = {"camo1"};
            modelsides[] = {3};
        };

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Headgear";
            variant = "guard";
        };
    };

    class Aux501_Units_PDF_Helmet_Officer: Aux501_Units_PDF_Helmet_Trooper
    {
        displayName = "[PDF] HELM 04 - Officer";
        picture = "\SFA_Equipment_R\Helmet\Navy\data\ui\SFA_KOTR_Trooper_Helmet_ca.paa";
		model = "SFA_Equipment_R\Helmet\Navy\KOTOR_Trooper_Helmet.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"SFA_Equipment_R\Helmet\Navy\data\Kotor_TrooperHeavy_Helmet_co.paa"};
        class Iteminfo: ItemInfo
        {
            uniformModel = "SFA_Equipment_R\Helmet\Navy\KOTOR_Trooper_Helmet.p3d";
			modelSides[] = {3,1};
			hiddenSelections[] = {"camo1"};
        };

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Headgear";
            variant = "officer";
        };
    };

    class Aux501_Units_PDF_Helmet_Commander: Aux501_Units_PDF_Helmet_Trooper
    {
        displayName = "[PDF] HELM 05 - Commander";
        picture = "\SFA_Equipment_R\Helmet\Navy\data\ui\SFA_KOTR_Officer_Helmet_ca.paa";
		model = "SFA_Equipment_R\Helmet\Navy\KOTOR_Officer_Hat.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"SFA_Equipment_R\Helmet\Navy\data\Kotor_OFC_Hat_co.paa"};
        class Iteminfo: ItemInfo
        {
            uniformModel = "SFA_Equipment_R\Helmet\Navy\KOTOR_Officer_Hat.p3d";
			modelSides[] = {3,1};
			hiddenSelections[] = {"camo1"};
        };

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Headgear";
            variant = "commander";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_PDF_Headgear
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "trooper",
                    "crew",
                    "guard",
                    "officer",
                    "commander"
                };
                class trooper
                {
                    label = "Trooper";
                };
                class crew
                {
                    label = "Crewman";
                };
                class guard
                {
                    label = "Honor Guard";
                };
                class officer
                {
                    label = "Officer";
                };
                class commander
                {
                    label = "Commander";
                };
            };
        };
    };
};