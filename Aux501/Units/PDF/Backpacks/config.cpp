class cfgPatches
{
    class Aux501_Patch_Units_PDF_Backpacks
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
        vehicles[] = 
        {
            "Aux501_Units_PDF_Backpack_standard",
            "Aux501_Units_PDF_Backpack_lr",
            "Aux501_Units_PDF_Backpack_AT",
            "Aux501_Units_PDF_Backpack_AA",
            "Aux501_Units_PDF_Backpack_LR",
            "Aux501_Units_PDF_Backpack_Crewman"
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;

    class Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large;

    class Aux501_Units_PDF_Backpack_standard: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[PDF] Backpack 01 - Standard";
        picture = "\ls_equipment_greenfor\backpack\mandalorian\ui\icon_light_backpack_ca.paa";
		model = "\ls_equipment_greenfor\backpack\mandalorian\light\ls_mandalorian_light_backpack.p3d";
		hiddenSelections[] = {"camo1","bacta","rto"};
		hiddenSelectionsTextures[] = {"\ls_equipment_greenfor\backpack\mandalorian\light\data\backpack_light_co.paa","",""};
        maximumLoad = 280;

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Backpacks";
            variant = "standard";
        };
    };

    class Aux501_Units_PDF_Backpack_Medic: Aux501_Units_PDF_Backpack_standard
    {
        displayName = "[PDF] Backpack 02 - Medic";
		hiddenSelectionsTextures[] = {"\ls_equipment_greenfor\backpack\mandalorian\light\data\backpack_light_medic_co.paa","\ls_equipment_greenfor\backpack\mandalorian\light\data\backpack_light_medic_co.paa",""};

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Backpacks";
            variant = "medic";
        };
    };

    class Aux501_Units_PDF_Backpack_Medic_Bandages: Aux501_Units_PDF_Backpack_Medic
    {
        scope = 1;
        scopearsenal = 0;
        class TransportItems
        {
            class _xx_FirstAidKit
            {
                count = 10;
                name = "FirstAidKit";
            };
        };
        class XtdGearInfo {};
    };

    class Aux501_Units_PDF_Backpack_AT: Aux501_Units_PDF_Backpack_standard
    {
        scope = 1;
        scopearsenal = 0;
        class TransportMagazines
        {
            class _xx_at_mag
            {
                count = 4;
                magazine = "Aux501_Weapons_Mags_BAP25_at";
            };
            class _xx_he_mag
            {
                count = 2;
                magazine = "Aux501_Weapons_Mags_BAP25_he";
            };
        };
        class XtdGearInfo {};
    };

    class Aux501_Units_PDF_Backpack_AA: Aux501_Units_PDF_Backpack_AT
    {
        class TransportMagazines
        {
            class _xx_aa_mag
            {
                count = 3;
                magazine = "Aux501_Weapons_Mags_BAP25_aa";
            };
        };
        class XtdGearInfo {};
    };

    class Aux501_Units_PDF_Backpack_LR: Aux501_Units_Republic_501_Infantry_Backpacks_LR_Large
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[PDF] Backpack 03 - LR";
        picture = "\ls_equipment_greenfor\backpack\mandalorian\ui\icon_light_backpack_ca.paa";
        model = "ls_equipment_bluefor\backpack\orsf\trooper\lsd_orsf_trooper_backpack.p3d";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"ls_equipment_bluefor\backpack\orsf\trooper\data\backpack_co.paa"};
        tf_dialog = "JLTS_clone_lr_programmer_radio_dialog";
        tf_encryptionCode = "tf_independent_radio_code";
        maximumLoad = 280;

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Backpacks";
            variant = "lr";
        };
    };

    class Aux501_Units_PDF_Backpack_Crewman: Aux501_Units_PDF_Backpack_standard
    {
        displayName = "[PDF] Backpack 04 - Crewman";
        picture = "\WarMantle\WM_Rebel_Units\Data\UI\Endor_Rebel_bag_ca.paa";
        model = "WarMantle\WM_Rebel_Units\bag.p3d";
		hiddenSelections[] = {"camo"};
		hiddenSelectionsTextures[] = {"WarMantle\WM_Rebel_Units\Data\backpack_blck_co.paa"};

        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_PDF_Backpacks";
            variant = "crewman";
        };
    };

    
};

class XtdGearModels
{
    class CfgVehicles
    {
        class Aux501_ACEX_Gear_PDF_Backpacks
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "lr",
                    "medic",
                    "crewman"
                };
                class standard
                {
                    label = "Standard";
                };
                class lr
                {
                    label = "LR";
                };
                class medic
                {
                    label = "Medic";
                };
                class crewman
                {
                    label = "Crewman";
                };
            };
        };
    };
};