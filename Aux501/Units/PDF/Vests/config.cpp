class cfgPatches
{
    class Aux501_Patch_Units_PDF_Vests
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_PDF_Vests_Standard",
            "Aux501_Units_PDF_Vests_Holster"
        };
    };
};

class CfgWeapons
{
    class V_SmershVest_01_radio_F;
    class Aux501_Units_Republic_501_Aviation_Vests_Standard_Holster_black;

    class Aux501_Units_PDF_Vests_Standard: V_SmershVest_01_radio_F
    {
        scope = 2;
        scopearsenal = 2;
        displayname = "[PDF] VEST 01";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\PDF\Vests\data\textures\pdf_vest_co.paa",
            "\Aux501\Units\PDF\Vests\data\textures\pdf_vest_miscellaneous_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_PDF_Vests";
            variant = "standard";
        };
    };

    class Aux501_Units_PDF_Vests_Holster: Aux501_Units_Republic_501_Aviation_Vests_Standard_Holster_black
    {
        displayname = "[PDF] VEST 02 - Holster";
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Units\PDF\Vests\data\textures\pdf_holster_co.paa"
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_Republic_501st_PDF_Vests";
            variant = "holster";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        class Aux501_ACEX_Gear_Republic_501st_PDF_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[] = 
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[] = 
                {
                    "standard",
                    "holster"
                };
                class standard
                {
                    label = "Standard";
                };
                class holster
                {
                    label = "Holster";
                };
            };
        };
    };
};