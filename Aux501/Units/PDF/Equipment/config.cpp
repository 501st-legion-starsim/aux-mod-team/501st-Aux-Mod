class cfgPatches
{
    class Aux501_Patch_Units_PDF_Equipment
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = {};
        weapons[] = {};
    };
};

class cfgGlasses 
{
    class Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display;

    class Aux501_PDF_Facewear_Goggles: Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display
    {
        scope = 2;
        scopeArsenal = 2;
        displayname = "[PDF] Goggles";
        picture = "\MRC\JLTS\characters\CloneArmor\data\ui\CloneHelmetP2_interior_ui_ca.paa";
        model = "ls_equipment_redfor\nvg\jn\trooper\ls_jn_goggles_down";
        hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\ls_equipment_redfor\nvg\jn\trooper\data\NVG_CO.paa"};
        ACE_Overlay = "";
        ACE_OverlayCracked = "";
        ACE_OverlayDirt = "A3\Ui_f\data\igui\rsctitles\HealthTextures\dust_upper_ca.paa";
        ACE_DustPath = "\z\ace\addons\goggles\textures\fx\dust\%1.paa";
        mass = 2;
        identityTypes[] = {};
        mode = 4;

        class XtdGearInfo {};
    };
};

class XtdGearModels 
{
    
};