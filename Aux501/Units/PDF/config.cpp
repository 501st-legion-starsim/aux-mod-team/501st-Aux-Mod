class cfgPatches
{
    class Aux501_Patch_Units_PDF
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_PDF_Base",

            "Aux501_Units_PDF_Trooper_Unit",
            "Aux501_Units_PDF_Trooper2_Unit",
            "Aux501_Units_PDF_Trooper3_Unit",
            "Aux501_Units_PDF_Trooper4_Unit",

            "Aux501_Units_PDF_Heavy_Unit",
            "Aux501_Units_PDF_Heavy2_Unit",

            "Aux501_Units_PDF_Marksman_Unit",

            "Aux501_Units_PDF_AT_Unit",
            "Aux501_Units_PDF_AA_Unit",

            "Aux501_Units_PDF_RTO_Unit",
            "Aux501_Units_PDF_Medic_Unit",

            "Aux501_Units_PDF_NCO_Unit",

            "Aux501_Units_PDF_Crewman_Unit",

            "Aux501_Units_PDF_Guard_Unit",
            "Aux501_Units_PDF_Officer_Unit",
            "Aux501_Units_PDF_Commander_Unit"
        };

        weapons[] = {};
        
    };
};

class cfgVehicles
{

    class I_Soldier_base_F;
    class I_Soldier_F: I_Soldier_base_F
    {
        class HitPoints;
    };

    class Aux501_Units_PDF_Base: I_Soldier_F
    {
        scope = 0;
        scopeCurator = 0;
        author = "501st Aux Team";
        identityTypes[] = 
        {
            "LanguagePRF_F",
            "Head_TK",
            "Head_African",
            "Head_Asian",
            "Head_Greek",
            "Head_m_mirialan",
            "Head_m_zelosian",
            "Head_m_zeltron"
        };
        faceType = "Man_A3";
        side = 2;
        genericNames = "TakistaniMen";
        faction = "Aux501_FactionClasses_PDF";
        editorCategory = "Aux501_Editor_Category_PDFs";
        editorSubcategory = "Aux501_Editor_Subcategory_Infantry";
        vehicleClass = "Men";
        portrait = "";
        picture = "";
        icon = "iconMan";
        accuracy = 2.3;
        camouflage = 1;
		sensitivity = 1.75;
		threat[] = {0.8,0.3,0.3};
		armor = 6;
		armorStructural = 4;
		explosionShielding = 0.3;
		minTotalDamageThreshold = 0.001;
		impactDamageMultiplier = 0.3;
        minFireTime = 7;
        canCarryBackPack = 1;
        impactEffectsBlood = "ImpactMetal";
		impactEffectsNoBlood = "ImpactPlastic";
        canBleed = 0;
        role = "Rifleman";
        cost = 100000;
        canHideBodies = 0;
        detectSkill = 12;
        canDeactivateMines = 0;
        class SpeechVariants
        {
            class Default
            {
                speechSingular[] = {"veh_infantry_s"};
                speechPlural[] = {"veh_infantry_p"};
            };
        };
        textSingular = "$STR_A3_namesound_veh_infantry_s";
        textPlural = "$STR_A3_namesound_veh_infantry_p";
        nameSound = "veh_infantry_s";
        maxSpeed = 24;
        maxTurnAngularVelocity = 3;
        lyingLimitSpeedHiding = 0.8;
        crouchProbabilityHiding = 0.8;
        lyingLimitSpeedCombat = 1.8;
        crouchProbabilityCombat = 0.4;
        crouchProbabilityEngage = 0.75;
        lyingLimitSpeedStealth = 2;
        items[] = {"FirstAidKit"};
        respawnItems[] = {"FirstAidKit"};
    };

    class Aux501_Units_PDF_Trooper_Unit: Aux501_Units_PDF_Base
    {
        scope = 2;
        scopeCurator = 2;
        displayName = "Trooper - EL-4";
        model = "ls_armor_bluefor\uniform\orsf\trooper\ls_orsf_trooper_uniform.p3d";
        hiddenSelections[] = {"camo1","camo2","camo3","insignia"};
		hiddenSelectionsTextures[] = 
        {
            "ls_armor_bluefor\uniform\orsf\trooper\data\upper_co.paa",
            "ls_armor_bluefor\uniform\orsf\trooper\data\Lower_CO.paa",
            "ls_armor_bluefor\uniform\orsf\trooper\data\TacRig_CO.paa",
            "swlb_core\data\insignia\orsf.paa"
        };
        uniformClass = "Aux501_Units_PDF_Trooper_Uniform";
        backpack = "Aux501_Units_PDF_Backpack_standard";
        editorPreview = "";
        weapons[] = {"Aux501_Weaps_EL4","Aux501_Weaps_RK3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_EL4","Aux501_Weaps_RK3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_EL4",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        linkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Trooper",

            "Aux501_Units_PDF_Vests_Standard",
            
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_nohud",
            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Trooper",

            "Aux501_Units_PDF_Vests_Standard",
            
            "Aux501_Units_Republic_501_Infantry_Equipment_Facewear_Phase_2_Display_Front_Pouches_nohud",
            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        items[] = 
        {
            "FirstAidKit",
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ"
        };
        respawnItems[] = 
        {
            "FirstAidKit",
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ"
        };
    };
    class Aux501_Units_PDF_Trooper2_Unit: Aux501_Units_PDF_Trooper_Unit
    {
        displayName = "Trooper - DC-15S";
        weapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_RK3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_RK3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };
    class Aux501_Units_PDF_Trooper3_Unit: Aux501_Units_PDF_Trooper_Unit
    {
        displayName = "Trooper - ZH-73";
        weapons[] = {"Aux501_Weaps_ZH73_scoped","Aux501_Weaps_RK3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_ZH73_scoped","Aux501_Weaps_RK3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_ZH7315",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };
    class Aux501_Units_PDF_Trooper4_Unit: Aux501_Units_PDF_Trooper_Unit
    {
        displayName = "Trooper - A280";
        weapons[] = {"Aux501_Weaps_A280C","Aux501_Weaps_RK3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_A280C","Aux501_Weaps_RK3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_A280C",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };

    class Aux501_Units_PDF_Heavy_Unit: Aux501_Units_PDF_Trooper_Unit
    {
        displayName = "Heavy Trooper - T-20";
        icon = "JLTS_iconManSupportGunner";
		hiddenSelectionsTextures[] = 
        {
            "ls_armor_bluefor\uniform\orsf\trooper\data\upper_co.paa",
            "ls_armor_bluefor\uniform\orsf\trooper\data\Lower_CO.paa",
            "ls_armor_bluefor\uniform\orsf\trooper\data\TacRig_CO.paa",
            "\OPTRE_Emblems\data\cartridges.paa"
        };
        uniformClass = "Aux501_Units_PDF_Heavy_Uniform";
        weapons[] = {"Aux501_Weaps_T20_scoped","Aux501_Weaps_RK3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_T20_scoped","Aux501_Weaps_RK3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_T20",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };
    class Aux501_Units_PDF_Heavy2_Unit: Aux501_Units_PDF_Heavy_Unit
    {
        displayName = "Heavy Trooper - E5C";
        weapons[] = {"Aux501_Weaps_E5C","Aux501_Weaps_RK3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5C","Aux501_Weaps_RK3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };

    class Aux501_Units_PDF_Marksman_Unit: Aux501_Units_PDF_Trooper_Unit
    {
        displayName = "Marksman";
        icon = "JLTS_iconManSniper";
		hiddenSelectionsTextures[] = 
        {
            "ls_armor_bluefor\uniform\orsf\trooper\data\upper_co.paa",
            "ls_armor_bluefor\uniform\orsf\trooper\data\Lower_CO.paa",
            "ls_armor_bluefor\uniform\orsf\trooper\data\TacRig_CO.paa",
            "\OPTRE_Emblems\data\crosshairs.paa"
        };
        uniformClass = "Aux501_Units_PDF_Marksman_Uniform";
        weapons[] = {"Aux501_Weaps_IQA11_scoped","Aux501_Weaps_RK3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_IQA11_scoped", "Aux501_Weaps_RK3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_IQA20",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        linkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Trooper",

            "Aux501_Units_PDF_Vests_Standard",
            
            "OPTRE_EyePiece",
            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Trooper",

            "Aux501_Units_PDF_Vests_Standard",
            
            "OPTRE_EyePiece",
            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
    };

    class Aux501_Units_PDF_AT_Unit: Aux501_Units_PDF_Trooper_Unit
    {
        displayName = "AT Trooper";
        icon = "iconManAT";
        weapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_RK3","Aux501_Weaps_BAP25_at","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_BAP25_at",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_RK3","Aux501_Weaps_BAP25_at","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_BAP25_at",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        backpack = "Aux501_Units_PDF_Backpack_AT";
    };
    class Aux501_Units_PDF_AA_Unit: Aux501_Units_PDF_AT_Unit
    {
        displayName = "AA Trooper";
        weapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_RK3","Aux501_Weaps_BAP25_aa","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_BAP25_aa",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_DC15S","Aux501_Weaps_RK3","Aux501_Weaps_BAP25_aa","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_10mw50",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_BAP25_aa",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        backpack = "Aux501_Units_PDF_Backpack_AA";
    };

    class Aux501_Units_PDF_RTO_Unit: Aux501_Units_PDF_Trooper4_Unit
    {
        displayName = "Comms Trooper";
        icon = "JLTS_iconManMarshalCMDR";
        backpack = "Aux501_Units_PDF_Backpack_LR";
    };
    class Aux501_Units_PDF_Medic_Unit: Aux501_Units_PDF_Trooper_Unit
    {
        displayName = "Medic";
        icon = "iconManMedic";
        backpack = "Aux501_Units_PDF_Backpack_Medic_Bandages";
        hiddenSelectionsTextures[] = 
        {
            "ls_armor_bluefor\uniform\orsf\trooper\data\upper_co.paa",
            "ls_armor_bluefor\uniform\orsf\trooper\data\Lower_CO.paa",
            "ls_armor_bluefor\uniform\orsf\trooper\data\TacRig_CO.paa",
            "\MRC\JLTS\Core_mod\data\insignias\gar_medic_ca.paa"
        };
        uniformClass = "Aux501_Units_PDF_Medic_Uniform";
        weapons[] = {"Aux501_Weaps_DC15A_Wood","Aux501_Weaps_RK3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_DC15A_Wood","Aux501_Weaps_RK3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_20mwdp30",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };

    class Aux501_Units_PDF_NCO_Unit: Aux501_Units_PDF_Trooper_Unit
    {
        displayName = "Sergeant";
        icon = "iconManLeader";
        weapons[] = {"Aux501_Weaps_DC15C","Aux501_Weaps_RK3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_DC15C","Aux501_Weaps_RK3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_20mw40",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_RK3",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        linkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Trooper",

            "Aux501_Units_PDF_Vests_Standard",
            
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_nohud",
            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Trooper",

            "Aux501_Units_PDF_Vests_Standard",
            
            "Aux501_Units_Republic_501_Infantry_Med_Equipment_Facewear_Phase_2_Display_Sr_CP_nohud",
            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
    };

    class Aux501_Units_PDF_Crewman_Unit: Aux501_Units_PDF_Base
    {
        scope = 2;
        scopeCurator = 2;
        displayName = "Crewman";
		model = "SFA_Equipment_R\Uniform\Trooper\Combat_Engineer_uniform.p3d";
        uniformClass = "Aux501_Units_PDF_Crew_Uniform";
        backpack = "Aux501_Units_PDF_Backpack_Crewman";
        editorPreview = "";
        weapons[] = {"Aux501_Weaps_DH17","Aux501_Weaps_ET74","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_DH17","Aux501_Weaps_ET74","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        linkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Crewman",
            "Aux501_Units_PDF_Vests_Holster",
            "Aux501_PDF_Facewear_Goggles",
            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Crewman",
            "Aux501_Units_PDF_Vests_Holster",
            "Aux501_PDF_Facewear_Goggles",
            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        items[] = 
        {
            "FirstAidKit",
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ"
        };
        respawnItems[] = 
        {
            "FirstAidKit",
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ"
        };
    };
    
    class Aux501_Units_PDF_Guard_Unit: Aux501_Units_PDF_Base
    {
        scope = 2;
        scopeCurator = 2;
        displayName = "Honor Guard";
		model = "SFA_Equipment_R\Uniform\Navy\Navy_Trooper_uniform.p3d";
        uniformClass = "Aux501_Units_PDF_Guard_Uniform";
        editorPreview = "";
        weapons[] = {"Aux501_Weaps_DH17","Aux501_Weaps_ET74","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_DH17","Aux501_Weaps_ET74","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_DH17",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        linkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Guard",
            "Aux501_Units_PDF_Vests_Holster",

            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Guard",
            "Aux501_Units_PDF_Vests_Holster",

            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        items[] = 
        {
            "FirstAidKit",
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ"
        };
        respawnItems[] = 
        {
            "FirstAidKit",
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ"
        };
    };
    class Aux501_Units_PDF_Officer_Unit: Aux501_Units_PDF_Base
    {
        scope = 2;
        scopeCurator = 2;
        displayName = "Officer";
        icon = "iconManOfficer";
		model = "SFA_Equipment_R\Uniform\Navy\Navy_Trooper_uniform.p3d";
        hiddenSelections[] = {"camo1","camo2","insignia"};
        hiddenSelectionsTextures[] = 
        {
            "SFA_Equipment_R\Uniform\Navy\data\Kotor_TrooperHeavy_Body_co.paa",
            "SFA_Equipment_R\Uniform\Navy\data\Kotor_TrooperHeavy_Body_co.paa"
        };
        uniformClass = "Aux501_Units_PDF_Officer_Uniform";
        editorPreview = "";
        weapons[] = {"Aux501_Weaps_RG4D","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_RG4D","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_RG4D30",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        linkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Officer",
            "Aux501_Units_PDF_Vests_Holster",

            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Officer",
            "Aux501_Units_PDF_Vests_Holster",

            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        items[] = 
        {
            "FirstAidKit",
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ"
        };
        respawnItems[] = 
        {
            "FirstAidKit",
            "JLTS_intel_holoProjector",
            "JLTS_intel_datacard",
            "JLTS_intel_datapad_civ"
        };
    };
    class Aux501_Units_PDF_Commander_Unit: Aux501_Units_PDF_Officer_Unit
    {
        displayName = "Commander";
        editorPreview = "";
        weapons[] = {"Aux501_Weaps_ET74","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_ET74","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_ET74",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        linkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Commander",
            "Aux501_Units_PDF_Vests_Holster",

            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
        respawnlinkeditems[] = 
        {
            "Aux501_Units_PDF_Helmet_Commander",
            "Aux501_Units_PDF_Vests_Holster",

            "SWLB_comlink_hush98",
            "ACE_Altimeter",
            "ItemMap",
            "ItemCompass",
            "ItemMicroDAGR"
        };
    };
};

class CfgGroups
{
    class Indep
    {
        class Aux501_FactionClasses_PDF
        {
            name = "[501st] Planetary Defence Forces";
            class Aux501_Group_Editor_Category_PDF_Infantry
            {
                name = "Infantry";
                class Aux501_PDF_Infantry_Squad
                {
                    name = "Infantry Squad";
                    side = 2;
                    faction = "Aux501_FactionClasses_PDF";
                    rarityGroup = 1;
                    class Unit0
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AT_Unit";
                        rank = "CORPORAL";
                        position[] = {5,-5,0};
                    };
                    class Unit2
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Trooper_Unit";
                        rank = "PRIVATE";
                        position[] = {-5,-5,0};
                    };
                    class Unit3
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Trooper2_Unit";
                        rank = "PRIVATE";
                        position[] = {10,-10,0};
                    };
                    class Unit4
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Trooper3_Unit";
                        rank = "PRIVATE";
                        position[] = {-10,-10,0};
                    };
                    class Unit5
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Trooper4_Unit";
                        rank = "PRIVATE";
                        position[] = {15,-15,0};
                    };
                    class Unit6
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Medic_Unit";
                        rank = "PRIVATE";
                        position[] = {-15,-15,0};
                    };
                    class Unit7
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_RTO_Unit";
                        rank = "PRIVATE";
                        position[] = {20,-20,0};
                    };
                    class Unit8
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-20,-20,0};
                    };
                    class Unit9
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Heavy2_Unit";
                        rank = "PRIVATE";
                        position[] = {25,-25,0};
                    };
                };
                class Aux501_PDF_Weapons_Squad
                {
                    name = "Weapons Squad";
                    side = 2;
                    faction = "Aux501_FactionClasses_PDF";
                    rarityGroup = 1;
                    class Unit0
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AT_Unit";
                        rank = "CORPORAL";
                        position[] = {5,-5,0};
                    };
                    class Unit2
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AT_Unit";
                        rank = "PRIVATE";
                        position[] = {-5,-5,0};
                    };
                    class Unit3
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AA_Unit";
                        rank = "PRIVATE";
                        position[] = {10,-10,0};
                    };
                    class Unit4
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Trooper4_Unit";
                        rank = "PRIVATE";
                        position[] = {-10,-10,0};
                    };
                    class Unit5
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Medic_Unit";
                        rank = "PRIVATE";
                        position[] = {15,-15,0};
                    };
                    class Unit6
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_RTO_Unit";
                        rank = "PRIVATE";
                        position[] = {-15,-15,0};
                    };
                    class Unit7
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Heavy2_Unit";
                        rank = "PRIVATE";
                        position[] = {20,-20,0};
                    };
                    class Unit8
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-20,-20,0};
                    };
                    class Unit9
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Heavy2_Unit";
                        rank = "PRIVATE";
                        position[] = {25,-25,0};
                    };
                };
                class Aux501_PDF_Infantry_Team
                {
                    name = "Infantry Team";
                    side = 2;
                    faction = "Aux501_FactionClasses_PDF";
                    rarityGroup = 1;
                    class Unit0
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AT_Unit";
                        rank = "CORPORAL";
                        position[] = {5,-5,0};
                    };
                    class Unit2
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-5,-5,0};
                    };
                    class Unit3
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Trooper2_Unit";
                        rank = "PRIVATE";
                        position[] = {10,-10,0};
                    };
                    class Unit4
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Medic_Unit";
                        rank = "PRIVATE";
                        position[] = {-10,-10,0};
                    };
                };
                class Aux501_PDF_AT_Team
                {
                    name = "AT Team";
                    side = 2;
                    faction = "Aux501_FactionClasses_PDF";
                    rarityGroup = 1;
                    class Unit0
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AT_Unit";
                        rank = "CORPORAL";
                        position[] = {5,-5,0};
                    };
                    class Unit2
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AT_Unit";
                        rank = "PRIVATE";
                        position[] = {-5,-5,0};
                    };
                    class Unit3
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AT_Unit";
                        rank = "PRIVATE";
                        position[] = {10,-10,0};
                    };
                    class Unit4
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Medic_Unit";
                        rank = "PRIVATE";
                        position[] = {-10,-10,0};
                    };
                };
                class Aux501_PDF_AA_Team
                {
                    name = "AA Team";
                    side = 2;
                    faction = "Aux501_FactionClasses_PDF";
                    rarityGroup = 1;
                    class Unit0
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AA_Unit";
                        rank = "CORPORAL";
                        position[] = {5,-5,0};
                    };
                    class Unit2
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AA_Unit";
                        rank = "PRIVATE";
                        position[] = {-5,-5,0};
                    };
                    class Unit3
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_AA_Unit";
                        rank = "PRIVATE";
                        position[] = {10,-10,0};
                    };
                    class Unit4
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Medic_Unit";
                        rank = "PRIVATE";
                        position[] = {-10,-10,0};
                    };
                };
                class Aux501_PDF_Infantry_Sentry
                {
                    name = "Infantry Sentry";
                    side = 2;
                    faction = "Aux501_FactionClasses_PDF";
                    rarityGroup = 1;
                    class Unit0
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Trooper2_Unit";
                        rank = "PRIVATE";
                        position[] = {5,-5,0};
                    };
                };
                class Aux501_PDF_Sniper_Team
                {
                    name = "Sniper Team";
                    side = 2;
                    faction = "Aux501_FactionClasses_PDF";
                    rarityGroup = 1;
                    class Unit0
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Marksman_Unit";
                        rank = "CORPORAL";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Marksman_Unit";
                        rank = "PRIVATE";
                        position[] = {5,-5,0};
                    };
                };
                class Aux501_PDF_Command_Section
                {
                    name = "Command Section";
                    side = 2;
                    faction = "Aux501_FactionClasses_PDF";
                    rarityGroup = 1;
                    class Unit0
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Commander_Unit";
                        rank = "COLONEL";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Officer_Unit";
                        rank = "CAPTAIN";
                        position[] = {5,-5,0};
                    };
                    class Unit2
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Guard_Unit";
                        rank = "SERGEANT";
                        position[] = {-5,-5,0};
                    };
                    class Unit3
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Guard_Unit";
                        rank = "SERGEANT";
                        position[] = {10,-10,0};
                    };
                    class Unit4
                    {
                        side = 2;
                        vehicle = "Aux501_Units_PDF_Guard_Unit";
                        rank = "SERGEANT";
                        position[] = {-10,-10,0};
                    };
                };
            };
        };
    };
};