class cfgPatches
{
    class Aux501_Patch_Units_CIS_Humans_Vests
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Vests_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_CIS_Humans_Vests_Standard",
            "Aux501_Units_CIS_Humans_Vests_Medic",
            "Aux501_Units_CIS_Humans_Vests_Heavy",
            "Aux501_Units_CIS_Humans_Vests_NCO"
        };
    };
};

class cfgWeapons
{
    class Aux501_Units_Republic_501_Infantry_Vests_Base;
    
    class Aux501_Units_Republic_501_Infantry_Vests_Nanoweave: Aux501_Units_Republic_501_Infantry_Vests_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_CIS_Humans_Vests_Standard: Aux501_Units_Republic_501_Infantry_Vests_Nanoweave
    {
        scope = 2;
        scopearsenal = 2;
        displayname = "[CIS] HMN VESTS 01 - Trooper";
        picture = "\Aux501\Units\CIS\Human_Legion\Vests\data\textures\UI\CIS_Humans_Vest_Standard_UI_ca.paa";
        model = "SFA_Equipment_S\Vest\Grenadier_Vest.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        hiddenSelectionsTextures[] = {"SFA_Equipment_S\Vest\data\Grenades_co.paa","SFA_Equipment_S\Vest\data\Pouches_co.paa"};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "SFA_Equipment_S\Vest\Grenadier_Vest.p3d";
            hiddenSelections[] = {"camo1","camo2"};
            class HitpointsProtectionInfo{};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Human_Vests";
            variant = "standard";
        };
    };
    class Aux501_Units_CIS_Humans_Vests_Medic: Aux501_Units_CIS_Humans_Vests_Standard
    {
        displayname = "[CIS] HMN VESTS 03 - Medic";
        picture = "\Aux501\Units\CIS\Human_Legion\Vests\data\textures\UI\CIS_Humans_Vest_Medic_UI_ca.paa";
        hiddenSelectionsTextures[] = {"SFA_Equipment_S\Vest\data\Bacta_co.paa","SFA_Equipment_S\Vest\data\MedPouches_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Human_Vests";
            variant = "medic";
        };
    };
    class Aux501_Units_CIS_Humans_Vests_Heavy: Aux501_Units_CIS_Humans_Vests_Standard
    {
        displayname = "[CIS] HMN VESTS 02 - Heavy";
        picture = "\Aux501\Units\CIS\Human_Legion\Vests\data\textures\UI\CIS_Humans_Vest_Heavy_UI_ca.paa";
        model = "SFA_Equipment_S\Vest\Lead_Vest.p3d";
        hiddenSelectionsTextures[] = {"SFA_Equipment_S\Vest\data\Pouches_co.paa",""};
        class ItemInfo: ItemInfo
        {
            uniformmodel = "SFA_Equipment_S\Vest\Lead_Vest.p3d";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Human_Vests";
            variant = "heavy";
        };
    };
    class Aux501_Units_CIS_Humans_Vests_NCO: Aux501_Units_CIS_Humans_Vests_Heavy
    {
        displayname = "[CIS] HMN VESTS 04 - NCO";
        picture = "\Aux501\Units\CIS\Human_Legion\Vests\data\textures\UI\CIS_Humans_Vest_NCO_UI_ca.paa";
        model = "SFA_Equipment_S\Vest\Lead_Vest.p3d";
        hiddenSelectionsTextures[] = {"SFA_Equipment_S\Vest\data\Pouches_co.paa","SFA_Equipment_S\Vest\data\Knife_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Human_Vests";
            variant = "nco";
        };
    };

    class Aux501_Units_CIS_Humans_Vests_Crew: Aux501_Units_CIS_Humans_Vests_Standard
    {
        displayname = "[CIS] HMN VESTS 05 - Crew";
        picture = "\IBL\characters\agent\data\ui\icon_armor_ca.paa";
		model = "\IBL\characters\agent\agent_armor.p3d";
		hiddenSelections[] = {"camo1"};
		hiddenSelectionsTextures[] = {"\IBL\characters\agent\data\agent_armor_co.paa"};
		class ItemInfo: ItemInfo
		{
			uniformModel = "\IBL\characters\agent\agent_armor.p3d";
			hiddenSelections[] = {"camo1"};
			mass = 80;
			class HitpointsProtectionInfo
			{
				class Chest
				{
					HitpointName = "HitChest";
					armor = 16;
					PassThrough = 0.3;
				};
			};
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Human_Vests";
            variant = "crew";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons 
    {
        ////Human Legion Vests////
        class Aux501_ACEX_Gear_CIS_Human_Vests
        {
            label = "";
            author = "501st Aux Team";
            options[]=
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[]=
                {
                    "standard",
                    "heavy",
                    "medic",
                    "nco",
                    "crew"
                };
                class standard
                {
                    label = "Standard";
                };
                class heavy
                {
                    label = "Heavy";
                };
                class nco
                {
                    label = "NCO";
                };
                class medic
                {
                    label = "Medic";
                };
                class crew
                {
                    label = "Crew";
                };
            };
        };
    };
};