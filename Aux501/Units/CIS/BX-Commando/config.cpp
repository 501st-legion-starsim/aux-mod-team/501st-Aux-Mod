
class cfgPatches
{
    class Aux501_Patch_Units_CIS_BX
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_CIS_B1"
        };
        units[] = 
        {
            "Aux501_Units_CIS_BX_Standard_Unit",
            "Aux501_Units_CIS_BX_Captain_Unit",
            "Aux501_Units_CIS_BX_Diplomat_Unit",
            "Aux501_Units_CIS_BX_Security_Unit"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class O_Soldier_F;
    class Aux501_Units_CIS_B1_Base_Unit: O_Soldier_F
    {
        class HitPoints;
    };

    class Aux501_Units_CIS_BX_Standard_Unit: Aux501_Units_CIS_B1_Base_Unit
    {
        scope = 2;
        scopeCurator = 2;
        displayName = "BX Commando Droid";
        model = "\ls_armor_redfor\uniform\cis\bx\lsd_cis_bx_uniform.p3d";
        editorSubcategory = "Aux501_Editor_Subcategory_BX";
        genericNames = "Aux501_BX_Names";
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\BX-Commando\data\textures\BX_standard_co.paa"};
        armorStructural = 3;
        class HitPoints: HitPoints
		{
			class HitFace
			{
				armor = 10;
				material = -1;
				name = "face_hub";
				passThrough = 0.8;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 10;
				material = -1;
				name = "neck";
				passThrough = 0.8;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 10;
				material = -1;
				name = "head";
				passThrough = 0.8;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 6;
				material = -1;
				name = "pelvis";
				passThrough = 0.8;
				radius = 0.24;
				explosionShielding = 1;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 20;
				material = -1;
				name = "spine1";
				passThrough = 0.8;
				radius = 0.16;
				explosionShielding = 1;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 20;
				material = -1;
				name = "spine2";
				passThrough = 0.8;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 20;
				material = -1;
				name = "spine3";
				passThrough = 0.8;
				radius = 0.18;
				explosionShielding = 10;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitBody: HitChest
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 3;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 3;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 30;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 1;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 1;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 15;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
			};
			class HitLeftLeg
			{
				armor = 15;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
        weapons[] = {"Aux501_Weaps_E5_Special","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_flashnade",
            "Aux501_Weapons_Mags_flashnade"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5_Special","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_flashnade",
            "Aux501_Weapons_Mags_flashnade"
        };
        linkeditems[] = 
        {
            "ItemGPS",
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_2"
        };
        respawnlinkeditems[] = 
        {
            "ItemGPS",
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_2"
        };
        items[] = 
        {
            "ACE_CableTie",
            "ACE_CableTie",
            "ACE_CableTie"
        };
        respawnItems[] = 
        {
            "ACE_CableTie",
            "ACE_CableTie",
            "ACE_CableTie"
        };
        backpack = "";
        uniformClass = "Aux501_Units_CIS_BX_Standard_Uniform";
    };

    class Aux501_Units_CIS_BX_Captain_Unit: Aux501_Units_CIS_BX_Standard_Unit
    {
        displayName = "BX Captain Droid";
        icon = "LSiconLeader";
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\BX-Commando\data\textures\BX_captain_co.paa"};
        uniformClass = "Aux501_Units_CIS_BX_Captain_Uniform";
    };

    class Aux501_Units_CIS_BX_Diplomat_Unit: Aux501_Units_CIS_BX_Standard_Unit
    {
        displayName = "BX Diplomat Droid";
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\BX-Commando\data\textures\BX_diplomat_co.paa"};
        uniformClass = "Aux501_Units_CIS_BX_Diplomat_Uniform";
    };

    class Aux501_Units_CIS_BX_Security_Unit: Aux501_Units_CIS_BX_Standard_Unit
    {
        displayName = "BX Security Droid";
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\BX-Commando\data\textures\BX_security_co.paa"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White",
            "Aux501_Weapons_Mags_Smoke_White",
            "Aux501_Weapons_Mags_flashnade",
            "Aux501_Weapons_Mags_flashnade"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_Smoke_White",
            "Aux501_Weapons_Mags_Smoke_White",
            "Aux501_Weapons_Mags_flashnade",
            "Aux501_Weapons_Mags_flashnade"
        };
        items[] = 
        {
            "ACE_CableTie",
            "ACE_CableTie",
            "ACE_CableTie",
            "JLTS_riot_shield_droid_item"
        };
        respawnItems[] = 
        {
            "ACE_CableTie",
            "ACE_CableTie",
            "ACE_CableTie",
            "JLTS_riot_shield_droid_item"
        };
        uniformClass = "Aux501_Units_CIS_BX_Security_Uniform";
    };
};