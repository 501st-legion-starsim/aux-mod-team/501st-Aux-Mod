class cfgPatches
{
    class Aux501_Patch_Units_CIS_BX_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_CIS_BX_Standard_Uniform",
            "Aux501_Units_CIS_BX_Captain_Uniform",
            "Aux501_Units_CIS_BX_Diplomat_Uniform",
            "Aux501_Units_CIS_BX_Security_Uniform"
        };
    };
};

class cfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_CIS_BX_Standard_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        JLTS_isDroid = 1;
        displayName = "[CIS] BX ARMR 01 - Standard";
        picture = "\Aux501\Units\CIS\BX-Commando\data\UI\BX_armor_ui_ca.paa";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_BX_Standard_Unit";
            containerClass = "Supply150";
            mass = 40;
            modelSides[] = {6};
            uniformType = "";
            armor = 15;
            armorStructural = 4;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_BX_Uniforms";
            variant = "standard";
        };
    };

    class Aux501_Units_CIS_BX_Captain_Uniform: Aux501_Units_CIS_BX_Standard_Uniform
    {
        displayName = "[CIS] BX ARMR 02 - Captain";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_BX_Captain_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_BX_Uniforms";
            variant = "captain";
        };
    };

    class Aux501_Units_CIS_BX_Diplomat_Uniform: Aux501_Units_CIS_BX_Standard_Uniform
    {
        displayName = "[CIS] BX ARMR 03 - Diplomat";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_BX_Diplomat_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_BX_Uniforms";
            variant = "diplomat";
        };
    };

    class Aux501_Units_CIS_BX_Security_Uniform: Aux501_Units_CIS_BX_Standard_Uniform
    {
        displayName = "[CIS] BX ARMR 04 - Security";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_BX_Security_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_BX_Uniforms";
            variant = "security";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        ////BX Battledroid Uniform////
        class Aux501_ACEX_Gear_CIS_BX_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[]=
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[]=
                {
                    "standard",
                    "captain",
                    "diplomat",
                    "security"
                };
    
                class standard
                {
                    label = "Standard";
                };
                class captain
                {
                    label = "Captain";
                };
                class diplomat
                {
                    label = "Diplomat";
                };
                class security
                {
                    label = "Security";
                };
            };
        };
    };
};