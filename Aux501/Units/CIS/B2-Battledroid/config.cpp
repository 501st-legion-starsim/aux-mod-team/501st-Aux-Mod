

class cfgPatches
{
    class Aux501_Patch_Units_CIS_B2
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_CIS_B1"
        };
        units[] = 
        {
            "Aux501_Units_CIS_B2_Standard_Unit",
            "Aux501_Units_CIS_B2_Heavy_Unit",
            "Aux501_Units_CIS_B2_Grenadier_Unit",
            "Aux501_Units_CIS_B2_Rocket_Unit"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class O_Soldier_F;

    class Aux501_Units_CIS_B1_Base_Unit: O_Soldier_F
    {
        class HitPoints;
    };

    class Aux501_Units_CIS_B2_Standard_Unit: Aux501_Units_CIS_B1_Base_Unit
    {
        scope = 2;
        scopeCurator = 2;
        faction = "Aux501_FactionClasses_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_B2";
        editorPreview = "";
        displayName = "B2 Super Battle Droid";
        model = "\lsd_armor_redfor\uniform\cis\b2\lsd_cis_b2_uniform.p3d";
        genericNames = "Aux501_B2_Names";
        hiddenSelections[]= {"camo_arms", "legs", "torso"};
        hiddenSelectionsTextures[] =
        {
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\standard\B2_Arms_co.paa",
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\standard\B2_Legs_co.paa",
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\standard\B2_Torso_co.paa"
        };
        hiddenSelectionsMaterials[]=
        {
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\standard\B2_Arms.rvmat",
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\standard\B2_Legs.rvmat",
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\standard\B2_Torso.rvmat"
        };
        weapons[] =
        {
            "Aux501_Weaps_B2_blaster",
            "Put"
        };
        magazines[] =
        {
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_rocket3"
        };
        respawnWeapons[] =
        {
            "Aux501_Weaps_B2_blaster",
            "Put"
        };
        respawnMagazines[] =
        {
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_blaster60",
            "Aux501_Weapons_Mags_B2_rocket3"
        };
        linkedItems[] =
        {
            "ItemMap",
            "ItemGPS",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_2"
        };
        backpack = "";
        uniformClass = "Aux501_Units_CIS_B2_Uniform_Standard";
        armorStructural	= 0.1;
        class HitPoints: HitPoints
		{
			class HitFace
			{
				armor = 40;
				material = -1;
				name = "face_hub";
				passThrough = 0.1;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 40;
				material = -1;
				name = "neck";
				passThrough = 0.1;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 10;
				material = -1;
				name = "head";
				passThrough = 0.1;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 20;
				material = -1;
				name = "pelvis";
				passThrough = 0.1;
				radius = 0.24;
				explosionShielding = 1;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 30;
				material = -1;
				name = "spine1";
				passThrough = 0.1;
				radius = 0.2;
				explosionShielding = 1;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 30;
				material = -1;
				name = "spine2";
				passThrough = 0.1;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 40;
				material = -1;
				name = "spine3";
				passThrough = 0.1;
				radius = 0.2;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitSensor: HitChest
			{
				armor = 10;
				radius = 0.1;
				name = "sensor_hit";
			};
			class HitBody: HitChest
			{
				armor = 50;
				material = -1;
				name = "body";
				passThrough = 1;
				explosionShielding = 6;
				radius = 0;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 10;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.16;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 100;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 100;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 1;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 1;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 20;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
				armor = 20;
			};
			class HitLeftLeg
			{
				armor = 20;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
    };

    class Aux501_Units_CIS_B2_Heavy_Unit: Aux501_Units_CIS_B2_Standard_Unit
    {
        displayName = "B2-H Super Heavy Droid";
        hiddenSelectionsTextures[] =
        {
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\heavy\B2_Heavy_Torso.paa",
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\heavy\B2_Heavy_Legs_co.paa",
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\heavy\B2_Heavy_Arms_co.paa"
        };
        uniformClass = "Aux501_Units_CIS_B2_Uniform_Heavy";
        class HitPoints: HitPoints
		{
			class HitFace
			{
				armor = 40;
				material = -1;
				name = "face_hub";
				passThrough = 0.1;
				radius = 0.08;
				explosionShielding = 0.1;
				minimalHit = 0.01;
			};
			class HitNeck: HitFace
			{
				armor = 40;
				material = -1;
				name = "neck";
				passThrough = 0.1;
				radius = 0.1;
				explosionShielding = 0.5;
				minimalHit = 0.01;
			};
			class HitHead: HitNeck
			{
				armor = 10;
				material = -1;
				name = "head";
				passThrough = 0.1;
				radius = 0.2;
				explosionShielding = 0.5;
				minimalHit = 0.01;
				depends = "HitFace max HitNeck";
			};
			class HitPelvis: HitHead
			{
				armor = 20;
				material = -1;
				name = "pelvis";
				passThrough = 0.1;
				radius = 0.24;
				explosionShielding = 1;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitAbdomen: HitPelvis
			{
				armor = 30;
				material = -1;
				name = "spine1";
				passThrough = 0.1;
				radius = 0.2;
				explosionShielding = 1;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitDiaphragm: HitAbdomen
			{
				armor = 30;
				material = -1;
				name = "spine2";
				passThrough = 0.1;
				radius = 0.18;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitChest: HitDiaphragm
			{
				armor = 40;
				material = -1;
				name = "spine3";
				passThrough = 0.1;
				radius = 0.2;
				explosionShielding = 6;
				visual = "injury_body";
				minimalHit = 0.01;
			};
			class HitSensor: HitChest
			{
				armor = 10;
				radius = 0.1;
				name = "sensor_hit";
			};
			class HitBody: HitChest
			{
				armor = 50;
				material = -1;
				name = "body";
				passThrough = 1;
				explosionShielding = 6;
				radius = 0;
				visual = "injury_body";
				minimalHit = 0.01;
				depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
			};
			class HitArms: HitBody
			{
				armor = 10;
				material = -1;
				name = "arms";
				passThrough = 1;
				radius = 0.16;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "0";
			};
			class HitHands: HitArms
			{
				armor = 100;
				material = -1;
				name = "hands";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
				depends = "HitArms";
			};
			class HitLegs: HitHands
			{
				armor = 100;
				material = -1;
				name = "legs";
				passThrough = 1;
				radius = 0.14;
				explosionShielding = 1;
				visual = "injury_legs";
				minimalHit = 0.01;
				depends = "0";
			};
			class Incapacitated: HitLegs
			{
				armor = 1000;
				material = -1;
				name = "body";
				passThrough = 1;
				radius = 0;
				explosionShielding = 1;
				visual = "";
				minimalHit = 0;
				depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
			};
			class HitLeftArm
			{
				armor = 20;
				material = -1;
				name = "hand_l";
				passThrough = 1;
				radius = 0.08;
				explosionShielding = 1;
				visual = "injury_hands";
				minimalHit = 0.01;
			};
			class HitRightArm: HitLeftArm
			{
				name = "hand_r";
				armor = 20;
			};
			class HitLeftLeg
			{
				armor = 20;
				material = -1;
				name = "leg_l";
				passThrough = 1;
				radius = 0.1;
				explosionShielding = 1;
				visual = "injury_legs";
				minimalHit = 0.01;
			};
			class HitRightLeg: HitLeftLeg
			{
				name = "leg_r";
			};
		};
    };

    class Aux501_Units_CIS_B2_Grenadier_Unit: Aux501_Units_CIS_B2_Standard_Unit
    {
        displayName = "B2-G Super Grenadier Droid";
        explosionShielding = 0.40000001;
        hiddenSelectionsTextures[] =
        {
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\standard\B2_Arms_co.paa",
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\standard\B2_Legs_co.paa",
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\grenadier\B2_Gren_Torso_co.paa"
        };
        weapons[] =
        {
            "Aux501_Weaps_B2_GL",
            "Put"
        };
        magazines[] =
        {
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3"
        };
        respawnWeapons[] =
        {
            "Aux501_Weaps_B2_GL",
            "Put"
        };
        respawnMagazines[] =
        {
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3",
            "Aux501_Weapons_Mags_B2_GL3"
        };
        uniformClass = "Aux501_Units_CIS_B2_Uniform_Grenadier";
    };

    class Aux501_Units_CIS_B2_Rocket_Unit: Aux501_Units_CIS_B2_Heavy_Unit
    {
        displayName = "B2-HA Super Rocket Droid";
        explosionShielding = 0.40000001;
        hiddenSelectionsTextures[] =
        {
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\heavy\B2_Heavy_Torso.paa",
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\rocket\B2_Rocket_Legs_co.paa",
            "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\textures\rocket\B2_Rocket_Arms_co.paa"
        };
        weapons[]=
        {
            "Aux501_Weaps_B2_rocket_cannon",
            "Put"
        };
        magazines[]=
        {
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3"
        };
        respawnWeapons[]=
        {
            "Aux501_Weaps_B2_rocket_cannon",
            "Put"
        };
        respawnMagazines[]=
        {
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3",
            "Aux501_Weapons_Mags_B2_rocket3"
        };
        uniformClass = "Aux501_Units_CIS_B2_Uniform_Rocket";
    };
};