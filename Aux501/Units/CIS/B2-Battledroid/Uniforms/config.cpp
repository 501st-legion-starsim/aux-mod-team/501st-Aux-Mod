class cfgPatches
{
    class Aux501_Patch_Units_CIS_B2_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_CIS_B2_Uniform_Standard",
            "Aux501_Units_CIS_B2_Uniform_Grenadier",
            "Aux501_Units_CIS_B2_Uniform_Rocket",
            "Aux501_Units_CIS_B2_Uniform_Heavy"
        };
    };
};

class cfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_CIS_B2_Uniform_Standard: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        JLTS_isDroid = 1;
        displayName = "[CIS] B2 ARMR 01 - Standard";
        picture = "\Aux501\Units\CIS\B2-Battledroid\Uniforms\data\UI\B2_armor_ui_ca.paa";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B2_Standard_Unit";
            containerClass = "Supply100";
            mass = 80;
            modelSides[] = {6};
            uniformType = "";
            armor = 2;
            armorStructural = 4;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B2_Uniforms";
            variant = "standard";
        };
    };

    class Aux501_Units_CIS_B2_Uniform_Grenadier: Aux501_Units_CIS_B2_Uniform_Standard
    {
        displayName = "[CIS] B2 ARMR 02 - Grenadier";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B2_Grenadier_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B2_Uniforms";
            variant = "grenadier";
        };
    };

    class Aux501_Units_CIS_B2_Uniform_Rocket: Aux501_Units_CIS_B2_Uniform_Standard
    {
        displayName = "[CIS] B2 ARMR 03 - Rocket";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B2_Rocket_Unit";
            containerClass = "Supply200";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B2_Uniforms";
            variant = "rocket";
        };
    };

    class Aux501_Units_CIS_B2_Uniform_Heavy: Aux501_Units_CIS_B2_Uniform_Standard
    {
        displayName = "[CIS] B2 ARMR 04 - Heavy";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B2_Heavy_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B2_Uniforms";
            variant = "heavy";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        ////B2 Battledroid Uniform////
        class Aux501_ACEX_Gear_CIS_B2_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[]=
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[]=
                {
                    "standard",
                    "grenadier",
                    "rocket",
                    "heavy",
                };
                class standard
                {
                    label = "Standard";
                };
                class grenadier
                {
                    label = "Gren";
                };
                class rocket
                {
                    label = "Rocket";
                };
                class heavy
                {
                    label = "Heavy";
                };
            };
        };
    };
};