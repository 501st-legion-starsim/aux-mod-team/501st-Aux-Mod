class cfgPatches
{
    class Aux501_Patch_Units_CIS_Tseries_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_CIS_TSeries_Blue_Uniform",
            "Aux501_Units_CIS_TSeries_Green_Uniform",
            "Aux501_Units_CIS_TSeries_Red_Uniform"
        };
    };
};

class cfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_CIS_TSeries_Blue_Uniform: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        JLTS_isDroid = 1;
        displayName = "[CIS] T-Series ARMR 01 - Blue";
        picture = "\Aux501\Units\CIS\T-series\Head_Item\data\textures\prop_head\UI\Tseries_armor_ui_ca.paa";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_TSeries_Blue_Unit";
            containerClass = "Supply150";
            mass = 40;
            modelSides[] = {6};
            uniformType = "";
            armor = 2;
            armorStructural = 4;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Tseries_Uniforms";
            variant = "blue";
        };
    };

    class Aux501_Units_CIS_TSeries_Green_Uniform: Aux501_Units_CIS_TSeries_Blue_Uniform
    {
        displayName = "[CIS] T-Series ARMR 02 - Green";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_TSeries_Green_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_Tseries_Uniforms";
            variant = "green";
        };
    };

    class Aux501_Units_CIS_TSeries_Red_Uniform: Aux501_Units_CIS_TSeries_Blue_Uniform
    {
        displayName = "[CIS] T-Series ARMR 03 - Red";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_TSeries_Red_Unit";
        };
        class XtdGearInfo

        {
            model = "Aux501_ACEX_Gear_CIS_Tseries_Uniforms";
            variant = "red";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        ////T-Series Uniform////
        class Aux501_ACEX_Gear_CIS_Tseries_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[]=
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[]=
                {
                    "blue",
                    "green",
                    "red"
                };
    
                class blue
                {
                    label = "Blue";
                };
                class green
                {
                    label = "Green";
                };
                class red
                {
                    label = "Red";
                };
            };
        };
    };
};