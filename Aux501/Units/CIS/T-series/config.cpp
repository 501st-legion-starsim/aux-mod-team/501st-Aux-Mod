class cfgPatches
{
    class Aux501_Patch_Units_CIS_Tseries
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_CIS_B1"
        };
        units[] = 
        {
            "Aux501_Units_CIS_TSeries_Blue_Unit",
            "Aux501_Units_CIS_TSeries_Green_Unit",
            "Aux501_Units_CIS_TSeries_Red_Unit"
        };
        weapons[] = 
        {
            "Aux501_props_TSeries_head_blue",
            "Aux501_props_TSeries_head_red",
            "Aux501_props_TSeries_head_green"
        };
    };
};

class CfgVehicles
{
    class Aux501_Units_CIS_B1_Base_Unit;

    class Aux501_Units_CIS_BX_Standard_Unit: Aux501_Units_CIS_B1_Base_Unit
    {
        class HitPoints;
    };

    class Aux501_Units_CIS_TSeries_Blue_Unit: Aux501_Units_CIS_BX_Standard_Unit 
    {
        scope = 2;
        scopeCurator = 2;
        editorSubcategory = "Aux501_Editor_Subcategory_Tseries";
        displayName = "T-Series - Blue";
        model = "\3AS\3AS_Characters\Droids\TSeries\3AS_TS.p3d";
        icon = "iconManOfficer";
        threat[] = {1.2,0.1,0.1};
        genericNames = "Aux501_TSeries_Names";
        accuracy = 0.9;
        precision = 6;
        sensitivity = 1.5;
        sensitivityEar = 0.13;
        camouflage = 0;
        minFireTime = 20;
        AnimEffectShortExhaust = 0.005;
        AnimEffectShortRest = 0.05;
        oxygenCapacity = 10800;
        aimingAccuracy = 0.2;
        aimingSpeed = 0.6;
        commanding = 0.6;
        courage = 1;
        general = 0.8;
        reloadSpeed = 0.6;
        spotDistance = 0.4;
        spotTime = 0.4;
        aimingShake = 0.6;
        irTarget = 0.5;
        hiddenSelections[] = {"Camo"};
        hiddenSelectionsTextures[] = {"\3AS\3AS_Characters\Droids\TSeries\data\3AS_TS_CO.paa"};
        hideProxySelections[] = {"ghillie_hide"};
        backpack = "";
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        Items[] =
        {
            "Aux501_props_TSeries_head_blue"
        };
        respawnItems[] =
        {
            "Aux501_props_TSeries_head_blue"
        };
        cost = 600000;
        uniformClass = "Aux501_Units_CIS_TSeries_Blue_Uniform";
    };

    class Aux501_Units_CIS_TSeries_Green_Unit: Aux501_Units_CIS_TSeries_Blue_Unit
    {
        displayName = "T-Series - Green";
        hiddenSelectionsTextures[] = {"\3AS\3AS_Characters\Droids\TSeries\data\3AS_TS_Green_CO.paa"};
        Items[] =
        {
            "Aux501_props_TSeries_head_green"
        };
        respawnItems[] =
        {
            "Aux501_props_TSeries_head_green"
        };
        uniformClass = "Aux501_Units_CIS_TSeries_Green_Uniform";
    };
    
    class Aux501_Units_CIS_TSeries_Red_Unit: Aux501_Units_CIS_TSeries_Blue_Unit
    {
      displayName = "T-Series - Red";
      hiddenSelectionsTextures[] = {"\3AS\3AS_Characters\Droids\TSeries\data\3AS_TS_Red_CO.paa"};
      Items[] =
        {
            "Aux501_props_TSeries_head_red"
        };
        respawnItems[] =
        {
            "Aux501_props_TSeries_head_red"
        };
        uniformClass = "Aux501_Units_CIS_TSeries_Red_Uniform";
    };
};
