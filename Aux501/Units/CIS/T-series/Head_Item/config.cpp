class cfgPatches
{
    class Aux501_Patch_Units_CIS_Tseries_headitem
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Ground_Item_TSeries_head_blue",
            "Aux501_Ground_Item_TSeries_head_red",
            "Aux501_Units_CIS_TSeries_Red_Unit"
        };
        weapons[] = 
        {
            "Aux501_props_TSeries_head_blue",
            "Aux501_props_TSeries_head_red",
            "Aux501_Ground_Item_TSeries_head_green"
        };
    };
};

class CfgWeapons
{
    class ItemCore;
    class InventoryWeapon_Base_F;

    class Aux501_props_TSeries_head_blue: ItemCore
    {
        scope = 2;
        scopearsenal = 0;
        author = "501st Aux Team";
        displayName = "[501st] T-Series Head - Blue";
        descriptionShort = "T-Series Head";
        picture = "\Aux501\Units\CIS\T-series\Head_Item\data\textures\prop_head\UI\blue_head_ui_ca.paa";
        model = "\kobra\442_misc\t_series\t_series_head.p3d";
        hiddenselections[] = {"camo1"};
        hiddenselectionstextures[] = {"\Aux501\Units\CIS\T-series\Head_Item\data\textures\prop_head\blue_head_co.paa"};
        detectRange = -1;
        simulation = "ItemMineDetector";
        useAsBinocular = 0;
        type = 4096;
        class ItemInfo: InventoryWeapon_Base_F
        {
            mass = 1;
        };
    };
    class Aux501_props_TSeries_head_red: Aux501_props_TSeries_head_blue
    {
        displayName = "[501st] T-Series Head - Red";
        picture = "\Aux501\Units\CIS\T-series\Head_Item\data\textures\prop_head\UI\red_head_ui_ca.paa";
        hiddenselectionstextures[] = {"\Aux501\Units\CIS\T-series\Head_Item\data\textures\prop_head\red_head_co.paa"};
    };
    class Aux501_props_TSeries_head_green: Aux501_props_TSeries_head_blue
    {
        displayName = "[501st] T-Series Head - Green";
        picture = "\Aux501\Units\CIS\T-series\Head_Item\data\textures\prop_head\UI\green_head_ui_ca.paa";
        hiddenselectionstextures[] = {"\Aux501\Units\CIS\T-series\Head_Item\data\textures\prop_head\green_head_co.paa"};
    };
};

class CfgVehicles
{
    class Weapon_Base_F;

    class Aux501_Ground_Item_TSeries_head_blue: Weapon_Base_F
    {
        scope = 2;
        scopecurator = 2;
        author = "501st Aux Team";
        displayName = "T-Series Head - Blue";
        editorCategory = "RD501_Editor_Category_statics";
        editorSubcategory = "RD501_Editor_Category_static_msc";
        class TransportItems
        {
            class xx_Aux501_props_TSeries_head_blue
            {
                count = 1;
                name = "Aux501_props_TSeries_head_blue";
            };
        };
    };
    class Aux501_Ground_Item_TSeries_head_red: Aux501_Ground_Item_TSeries_head_blue
    {
        displayName = "T-Series Head - Red";
        class TransportItems
        {
            class xx_Aux501_props_TSeries_head_red
            {
                count = 1;
                name = "Aux501_props_TSeries_head_red";
            };
        };
    };
    class Aux501_Ground_Item_TSeries_head_green: Aux501_Ground_Item_TSeries_head_blue
    {
        displayName = "T-Series Head - Green";
        class TransportItems
        {
            class xx_Aux501_props_TSeries_head_green
            {
                count = 1;
                name = "Aux501_props_TSeries_head_green";
            };
        };
    };
};
