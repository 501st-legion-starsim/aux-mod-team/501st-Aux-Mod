class cfgPatches
{
    class Aux501_Patch_Units_CIS_B1
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F"
        };
        units[] = 
        {
            "Aux501_Units_CIS_B1_Base_Unit",
            "Aux501_Units_CIS_B1_Unit",
            "Aux501_Units_CIS_B1_Jammer_Unit",
            "Aux501_Units_CIS_B1_Pilot_Unit",
            "Aux501_Units_CIS_B1_Crew_Unit",
            "Aux501_Units_CIS_B1_Crew_Late_Unit",

            "Aux501_Units_CIS_B1_NCO_Unit",

            "Aux501_Units_CIS_B1_AT_Unit",
            "Aux501_Units_CIS_B1_AA_Unit",
            "Aux501_Units_CIS_B1_Marksman_Unit",
            "Aux501_Units_CIS_B1_Heavy_Unit",
            "Aux501_Units_CIS_B1_Breacher_Unit",

            "Aux501_Units_CIS_B1_Prototype_Unit"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class O_Soldier_base_F;
    class O_Soldier_F: O_Soldier_base_F
    {
        class HitPoints;
    };

    class Aux501_Units_CIS_B1_Base_Unit: O_Soldier_F
    {
        scope = 1;
        scopeCurator = 0;
        author = "501st Aux Team";
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_B1";
        editorPreview = "";
        identityTypes[] = {"lsd_voice_b1Droid"};
        genericNames = "ls_droid_b1";
        portrait = "";
        picture = "";
        icon = "iconMan";
        accuracy = 2.3;
        threat[] = {1,0.1,0.1};
        camouflage = 1.4;
        minFireTime = 7;
        canCarryBackPack = 1;
        impactEffectsBlood = "ImpactMetal";
        impactEffectsNoBlood = "ImpactPlastic";
        canBleed = 0;
        armor = 2;
        armorStructural = 4;
        explosionShielding = 0.4;
        minTotalDamageThreshold = 0.001;
        impactDamageMultiplier = 0.5;
        class HitPoints: HitPoints
        {
            class ACE_HDBracket
            {
                armor = 1;
                depends = "HitHead";
                explosionShielding = 1;
                material = -1;
                minimalHit = 0;
                name = "head";
                passThrough = 0;
                radius = 1;
                visual = "";
            };
            class HitFace
            {
                armor = 1;
                material = -1;
                name = "face_hub";
                passThrough = 0.8;
                radius = 0.08;
                explosionShielding = 0.1;
                minimalHit = 0.01;
            };
            class HitNeck: HitFace
            {
                armor = 1;
                material = -1;
                name = "neck";
                passThrough = 0.8;
                radius = 0.1;
                explosionShielding = 0.5;
                minimalHit = 0.01;
            };
            class HitHead: HitNeck
            {
                armor = 1;
                material = -1;
                name = "head";
                passThrough = 0.8;
                radius = 0.2;
                explosionShielding = 0.5;
                minimalHit = 0.01;
                depends = "HitFace max HitNeck";
            };
            class HitPelvis: HitHead
            {
                armor = 8;
                material = -1;
                name = "pelvis";
                passThrough = 0.8;
                radius = 0.24;
                explosionShielding = 3;
                visual = "injury_body";
                minimalHit = 0.01;
                depends = "";
            };
            class HitAbdomen: HitPelvis
            {
                armor = 6;
                material = -1;
                name = "spine1";
                passThrough = 0.8;
                radius = 0.16;
                explosionShielding = 3;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitDiaphragm: HitAbdomen
            {
                armor = 6;
                material = -1;
                name = "spine2";
                passThrough = 0.33;
                radius = 0.18;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitChest: HitDiaphragm
            {
                armor = 8;
                material = -1;
                name = "spine3";
                passThrough = 0.33;
                radius = 0.18;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
            };
            class HitBody: HitChest
            {
                armor = 1000;
                material = -1;
                name = "body";
                passThrough = 1;
                radius = 0;
                explosionShielding = 6;
                visual = "injury_body";
                minimalHit = 0.01;
                depends = "HitPelvis max HitAbdomen max HitDiaphragm max HitChest";
            };
            class HitArms: HitBody
            {
                armor = 6;
                material = -1;
                name = "arms";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 3;
                visual = "injury_hands";
                minimalHit = 0.01;
                depends = "0";
            };
            class HitHands: HitArms
            {
                armor = 6;
                material = -1;
                name = "hands";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 1;
                visual = "injury_hands";
                minimalHit = 0.01;
                depends = "HitArms";
            };
            class HitLegs: HitHands
            {
                armor = 6;
                material = -1;
                name = "legs";
                passThrough = 1;
                radius = 0.14;
                explosionShielding = 3;
                visual = "injury_legs";
                minimalHit = 0.01;
                depends = "0";
            };
            class Incapacitated: HitLegs
            {
                armor = 1000;
                material = -1;
                name = "body";
                passThrough = 1;
                radius = 0;
                explosionShielding = 3;
                visual = "";
                minimalHit = 0;
                depends = "(((Total - 0.25) max 0) + ((HitHead - 0.25) max 0) + ((HitBody - 0.25) max 0)) * 2";
            };
            class HitLeftArm
            {
                armor = 6;
                material = -1;
                name = "hand_l";
                passThrough = 1;
                radius = 0.08;
                explosionShielding = 3;
                visual = "injury_hands";
                minimalHit = 0.01;
            };
            class HitRightArm: HitLeftArm
            {
                name = "hand_r";
            };
            class HitLeftLeg
            {
                armor = 6;
                material = -1;
                name = "leg_l";
                passThrough = 1;
                radius = 0.1;
                explosionShielding = 3;
                visual = "injury_legs";
                minimalHit = 0.01;
            };
            class HitRightLeg: HitLeftLeg
            {
                name = "leg_r";
            };
        };
        
        class Wounds
        {
            tex[] = {};
            mat[] = {"MRC\JLTS\Characters\DroidArmor\data\b1.rvmat","MRC\JLTS\Characters\DroidArmor\data\b1_injury.rvmat","MRC\JLTS\Characters\DroidArmor\data\b1_injury.rvmat","A3\Characters_F\Common\Data\basicbody.rvmat","A3\Characters_F\Common\Data\basicbody_injury.rvmat","A3\Characters_F\Common\Data\basicbody_injury.rvmat","a3\characters_f\heads\data\hl_white.rvmat","a3\characters_f\heads\data\hl_white_injury.rvmat","a3\characters_f\heads\data\hl_white_injury.rvmat","A3\Characters_F\Heads\Data\hl_white_bald_muscular.rvmat","A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat","A3\Characters_F\Heads\Data\hl_white_bald_muscular_injury.rvmat","A3\Characters_F\Heads\Data\hl_white_02_bald_muscular.rvmat","A3\Characters_F\Heads\Data\hl_white_02_bald_muscular_injury.rvmat","A3\Characters_F\Heads\Data\hl_white_02_bald_muscular_injury.rvmat","A3\Characters_F\Heads\Data\hl_black_bald_muscular.rvmat","A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat","A3\Characters_F\Heads\Data\hl_black_bald_muscular_injury.rvmat","A3\Characters_F\Heads\Data\hl_white_hairy_muscular.rvmat","A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat","A3\Characters_F\Heads\Data\hl_white_hairy_muscular_injury.rvmat","A3\Characters_F\Heads\Data\hl_white_old.rvmat","A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat","A3\Characters_F\Heads\Data\hl_white_old_injury.rvmat","A3\Characters_F\Heads\Data\hl_asian_bald_muscular.rvmat","A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat","A3\Characters_F\Heads\Data\hl_asian_bald_muscular_injury.rvmat","A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_muscular.rvmat","A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_muscular_injury.rvmat","A3\Characters_F_Exp\Heads\Data\hl_tanoan_bald_muscular_injury.rvmat","A3\Characters_F_Exp\Heads\Data\hl_asian_02_bald_muscular.rvmat","A3\Characters_F_Exp\Heads\Data\hl_asian_02_bald_muscular_injury.rvmat","A3\Characters_F_Exp\Heads\Data\hl_asian_02_bald_muscular_injury.rvmat"};
        };
    };

    class Aux501_Units_CIS_B1_Unit: Aux501_Units_CIS_B1_Base_Unit
    {
        scope = 2;
        scopeCurator = 2;
        displayName = "B1 Battledroid";
        model = "\MRC\JLTS\characters\DroidArmor\DroidUniformB1.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidArmor\data\b1_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_Standard";
        weapons[] = {"Aux501_Weaps_E5","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        linkeditems[] = 
        {
            "ItemGPS",
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
        respawnlinkeditems[] = 
        {
            "ItemGPS",
            "ItemMap",
            "ItemCompass",
            "ItemWatch",
            "JLTS_droid_comlink",
            "JLTS_NVG_droid_chip_1"
        };
        items[] = {};
        respawnItems[] = {};
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Standard";
    };

    class Aux501_Units_CIS_B1_Jammer_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - Jammer";
        icon = "JLTS_iconManMarshalCMDR";
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\B1-Battledroid\Uniforms\data\textures\B1_jammer_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_Jammer";
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Jammer";
    };

    class Aux501_Units_CIS_B1_Pilot_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - Pilot";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidArmor\data\b1_pilot_co.paa"};
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Blue";
    };

    class Aux501_Units_CIS_B1_Crew_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - Crew";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidArmor\data\b1_crew_co.paa"};
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Camo";
    };

    class Aux501_Units_CIS_B1_Crew_Late_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - Crew/Late";
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\B1-Battledroid\Uniforms\data\textures\B1_tank_crew_late_war_co.paa"};
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Camo_Late";
    };

    class Aux501_Units_CIS_B1_NCO_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - NCO";
        icon = "iconManLeader";
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidArmor\data\b1_commander_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_NCO";
        uniformClass = "Aux501_Units_CIS_B1_Uniform_NCO";
    };

    class Aux501_Units_CIS_B1_AT_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - AT";
        icon = "iconManAT";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidArmor\data\b1_security_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_Standard_AT";
        weapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_at","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_at",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_at","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_at",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Red";
    };

    class Aux501_Units_CIS_B1_AA_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - AA";
        icon = "iconManAT";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidArmor\data\b1_marine_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_Standard_AA";
        weapons[] = {"Aux501_Weaps_E5","Aux501_Weaps_e60r_aa","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_aa",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_e60r_aa",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Green";
    };

    class Aux501_Units_CIS_B1_Marksman_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - Marksman";
        icon = "JLTS_iconManSniper";
        weapons[] = {"Aux501_Weaps_E5S","Throw","Put"};
        magazines[]=
        {
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5S","Throw","Put"};
        respawnMagazines[]=
        {
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_E5S15",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };

    class Aux501_Units_CIS_B1_Heavy_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - Heavy";
        icon = "JLTS_iconManSupportGunner";
        weapons[] = {"Aux501_Weaps_E5C","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5C","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150",
            "Aux501_Weapons_Mags_E5C150"
        };
    };

    class Aux501_Units_CIS_B1_Breacher_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - Breacher";
        weapons[] = {"Aux501_Weaps_SBB3","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator"
        };
        respawnWeapons[] = {"Aux501_Weaps_SBB3","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_SBB3_shotgun25",
            "Aux501_Weapons_Mags_CISDetonator",
            "Aux501_Weapons_Mags_CISDetonator"
        };
    };

    class Aux501_Units_CIS_B1_Prototype_Unit: Aux501_Units_CIS_B1_Unit
    {
        displayName = "B1 Battledroid - Spec-Ops";
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidArmor\data\b1_prototype_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_SpecOps";
        weapons[] = {"Aux501_Weaps_E5_Special","Throw","Put"};
        magazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_flashnade"
        };
        respawnWeapons[] = {"Aux501_Weaps_E5_Special","Throw","Put"};
        respawnMagazines[] = 
        {
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_E5100",
            "Aux501_Weapons_Mags_flashnade"
        };
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Prototype";
        cost = 3;
    };
};

class CfgGroups
{
    class EAST
    {
        class Aux501_FactionClasses_Confederacy
        {
            name = "[501st] Confederacy";
            class Aux501_Group_Editor_Category_Droid_Sections
            {
                name = "Droid Sections";
                class Aux501_cis_droid_infantry_heavy_section
                {
                    name = "Heavy Section";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit7
                    {
                        position[] = {2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                    class Unit8
                    {
                        position[] = {-2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit9
                    {
                        position[] = {2.5,-2.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit10
                    {
                        position[] = {-2.5,-2.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_light_section
                {
                    name = "Light Section";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit7
                    {
                        position[] = {2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit8
                    {
                        position[] = {-2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit9
                    {
                        position[] = {2.5,-2.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_weapons_section
                {
                    name = "Weapon Section";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit7
                    {
                        position[] = {2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Marksman_Unit";
                    };
                    class Unit8
                    {
                        position[] = {-2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Marksman_Unit";
                    };
                    class Unit9
                    {
                        position[] = {2.5,-2.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AA_Unit";
                    };
                    class Unit10
                    {
                        position[] = {-2.5,-2.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                    class Unit11
                    {
                        position[] = {3,-3,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                };
            };
            class Aux501_Group_Editor_Category_Droid_Squads
            {
                name = "Droid Squads";
                class Aux501_cis_droid_infantry_heavy_defense_squad
                {
                    name = "Heavy Defense Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Marksman_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_heavy_assault_squad
                {
                    name = "Heavy Assault Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";

                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Marksman_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit7
                    {
                        position[] = {2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_AT_squad
                {
                    name = "AT Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_AA_squad
                {
                    name = "AA Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AA_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AA_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_weapons_squad
                {
                    name = "Weapons Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Marksman_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_B1_squad
                {
                    name = "B1 Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit7
                    {
                        position[] = {2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_light_defense_squad
                {
                    name = "Light Defense Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Marksman_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_light_assault_squad
                {
                    name = "Light Assault Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit3
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_spec_ops_squad
                {
                    name = "Spec Ops Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Captain_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Standard_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Standard_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Prototype_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Prototype_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Prototype_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Prototype_Unit";
                    };
                    class Unit7
                    {
                        position[] = {2,-2,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Prototype_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_command_squad
                {
                    name = "Command Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "LIEUTENANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_TSeries_Blue_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Captain_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Security_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Security_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Security_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Security_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Standard_Unit";
                    };
                };
            };
            class Aux501_Group_Editor_Category_Droid_Teams
            {
                name = "Droid Teams";
                class Aux501_cis_droid_infantry_heavy_assault_team
                {
                    name = "Heavy Assault Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_light_assault_team
                {
                    name = "Light Assault Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_B1_team
                {
                    name = "B1 Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_cqc_team
                {
                    name = "CQC Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_AT_team
                {
                    name = "AT Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_AA_team
                {
                    name = "AA Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AA_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AA_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_weapons_team
                {
                    name = "Weapons Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_marksmen_team
                {
                    name = "Marksmen Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Marksman_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Marksman_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_defense_team
                {
                    name = "Defense Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_spec_ops_B1_team
                {
                    name = "Spec Ops B1 Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Prototype_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Prototype_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Prototype_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_spec_ops_BX_team
                {
                    name = "Spec Ops BX Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Captain_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Standard_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Standard_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_spec_ops_team
                {
                    name = "Spec Ops Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Captain_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Standard_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Prototype_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Prototype_Unit";
                    };
                };
                class Aux501_cis_droid_command_team
                {
                    name = "Command Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "LIEUTENANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_TSeries_Green_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "SERGEANT";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Captain_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Security_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_BX_Security_Unit";
                    };
                };
            };
            class Aux501_Group_Editor_Category_Droid_Fillers
            {
                name = "Vehicle Fillers";
                class Aux501_cis_droid_infantry_hmp_filler
                {
                    name = "HMP Filler";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit7
                    {
                        position[] = {2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit8
                    {
                        position[] = {-2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit9
                    {
                        position[] = {2.5,-2.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit10
                    {
                        position[] = {-2.5,-2.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
                class Aux501_cis_droid_infantry_b1_argon_filler
                {
                    name = "Argon B1 Filler";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0
                    {
                        position[] = {0,0,0};
                        rank = "SERGEANT";
						side = 0;
						vehicle = "Aux501_Units_CIS_B1_NCO_Unit";
                    };
                    class Unit1
                    {
                        position[] = {0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Heavy_Unit";
                    };
                    class Unit2
                    {
                        position[] = {-0.5,-0.5,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit3
                    {
                        position[] = {1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit4
                    {
                        position[] = {-1,-1,0};
                        rank = "CORPORAL";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_AT_Unit";
                    };
                    class Unit5
                    {
                        position[] = {1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Breacher_Unit";
                    };
                    class Unit6
                    {
                        position[] = {-1.5,-1.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit7
                    {
                        position[] = {2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit8
                    {
                        position[] = {-2,-2,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit9
                    {
                        position[] = {2.5,-2.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit10
                    {
                        position[] = {-2.5,-2.5,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit11
                    {
                        position[] = {3,-3,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                    class Unit12
                    {
                        position[] = {-3,-3,0};
                        rank = "PRIVATE";
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Unit";
                    };
                };
            };
        };
    };
};