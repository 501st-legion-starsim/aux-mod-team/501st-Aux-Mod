class cfgPatches
{
    class Aux501_Patch_Units_CIS_B1_Backpacks
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_Republic_501_Infantry_Backpacks"
        };
        units[] = 
        {
            "Aux501_Units_CIS_B1_Backpack_Standard",
            "Aux501_Units_CIS_B1_Backpack_Standard_AT",
            "Aux501_Units_CIS_B1_Backpack_Standard_AA",

            "Aux501_Units_CIS_B1_Backpack_NCO",
            "Aux501_Units_CIS_B1_Backpack_Jammer",
            "Aux501_Units_CIS_B1_Backpack_SpecOps",

            "Aux501_Units_CIS_B1_Backpack_Standard_Geonosis",
            "Aux501_Units_CIS_B1_Backpack_Standard_Geonosis_AT",
            "Aux501_Units_CIS_B1_Backpack_Standard_Geonosis_AA",
            "Aux501_Units_CIS_B1_Backpack_NCO_Geonosis"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_Republic_501_Infantry_Backpacks_base;

    class Aux501_Units_CIS_B1_Backpack_Standard: Aux501_Units_Republic_501_Infantry_Backpacks_base
    {
        scope = 2;
        scopearsenal = 2;
        displayName = "[CIS] B1 Backpack 01 - Standard";
        picture = "\MRC\JLTS\characters\DroidArmor\data\ui\b1_backpack_ui_ca.paa";
        model = "\MRC\JLTS\characters\DroidArmor\DroidBackpackB1.p3d";
        hiddenSelections[] = {"camo1"};
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidArmor\data\b1_backpack_co.paa"};
        maximumLoad = 280;
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Backpacks";
            variant = "standard";
        };
    };

    class Aux501_Units_CIS_B1_Backpack_Standard_AT: Aux501_Units_CIS_B1_Backpack_Standard
    {
        scope = 1;
        scopearsenal = 1;
        class TransportMagazines
        {
            class _xx_at_mag
            {
                count = 4;
                magazine = "Aux501_Weapons_Mags_e60r_at";
            };
            class _xx_he_mag
            {
                count = 2;
                magazine = "Aux501_Weapons_Mags_e60r_he";
            };
        };
    };

    class Aux501_Units_CIS_B1_Backpack_Standard_AA: Aux501_Units_CIS_B1_Backpack_Standard_AT
    {
        class TransportMagazines
        {
            class _xx_aa_mag
            {
                count = 3;
                magazine = "Aux501_Weapons_Mags_e60r_aa";
            };
        };
    };

    class Aux501_Units_CIS_B1_Backpack_NCO: Aux501_Units_CIS_B1_Backpack_Standard
    {
        displayName = "[CIS] B1 Backpack 02 - NCO";
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\B1-Battledroid\Backpacks\data\textures\B1_commander_backpack_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Backpacks";
            variant = "nco";
        };  
    };

    class Aux501_Units_CIS_B1_Backpack_Jammer: Aux501_Units_CIS_B1_Backpack_Standard
    {
        displayName = "[CIS] B1 Backpack 03 - Jammer";
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\B1-Battledroid\Backpacks\data\textures\B1_jammer_backpack_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Backpacks";
            variant = "jammer";
        };   
    };

    class Aux501_Units_CIS_B1_Backpack_SpecOps: Aux501_Units_CIS_B1_Backpack_Standard
    {
        displayName = "[CIS] B1 Backpack 04 - Spec-Ops";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidArmor\data\b1_backpack_prototype_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Backpacks";
            variant = "specops";
        };   
    };

    class Aux501_Units_CIS_B1_Backpack_Standard_Geonosis: Aux501_Units_CIS_B1_Backpack_Standard
    {
        displayName = "[CIS] B1 Backpack 05 - Geonosis";
        hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidUnits\data\Droid_B1Geonosis_backpack_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Backpacks";
            variant = "geonosis";
        };  
    };

    class Aux501_Units_CIS_B1_Backpack_Standard_Geonosis_AT: Aux501_Units_CIS_B1_Backpack_Standard_Geonosis
    {
        scope = 1;
        scopearsenal = 1;
        class TransportMagazines
        {
            class _xx_at_mag
            {
                count = 4;
                magazine = "Aux501_Weapons_Mags_e60r_at";
            };
            class _xx_he_mag
            {
                count = 2;
                magazine = "Aux501_Weapons_Ammo_e60r_he";
            };
        };
    };

    class Aux501_Units_CIS_B1_Backpack_Standard_Geonosis_AA: Aux501_Units_CIS_B1_Backpack_Standard_Geonosis_AT
    {
        class TransportMagazines
        {
            class _xx_aa_mag
            {
                count = 3;
                magazine = "Aux501_Weapons_Mags_e60r_aa";
            };
        };
    };

    class Aux501_Units_CIS_B1_Backpack_NCO_Geonosis: Aux501_Units_CIS_B1_Backpack_Standard
    {
        displayName = "[CIS] B1 Backpack 06 - Geo NCO";
        hiddenSelectionsTextures[] = {"\Aux501\Units\CIS\B1-Battledroid\Backpacks\data\textures\B1_commander_backpack_geo_co.paa"};
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Backpacks";
            variant = "ncogeo";
        };  
    };
};

class XtdGearModels
{
    class CfgVehicles
    {
        ////B1 Battledroid Backpack////
        class Aux501_ACEX_Gear_CIS_B1_Backpacks
        {
            label = "";
            author = "501st Aux Team";
            options[]=
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[]=
                {
                    "standard",
                    "nco",
                    "jammer",
                    "specops",
                    "geonosis",
                    "ncogeo"
                };
                class standard
                {
                    label = "Standard";
                };
                 class nco
                {
                    label = "NCO";
                };
                class jammer
                {
                    label = "Jammer";
                };
                class specops
                {
                    label = "Spec-Ops";
                };
                class geonosis
                {
                    label = "Geonosis";
                };
                class ncogeo
                {
                    label = "Geo/NCO";
                };
            };
        };
    };
};