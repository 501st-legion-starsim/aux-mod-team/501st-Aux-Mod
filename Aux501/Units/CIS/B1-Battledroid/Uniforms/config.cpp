class cfgPatches
{
    class Aux501_Patch_Units_CIS_B1_Uniforms
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units",
            "A3_data_F",
            "A3_anims_F",
            "A3_weapons_F",
            "A3_characters_F",
            "Aux501_Patch_Units_Republic_501_Infantry_Uniforms_Phase2"
        };
        units[] = {};
        weapons[] = 
        {
            "Aux501_Units_CIS_B1_Uniform_Standard",
            "Aux501_Units_CIS_B1_Uniform_Blue",
            "Aux501_Units_CIS_B1_Uniform_Red",
            "Aux501_Units_CIS_B1_Uniform_Green",
            "Aux501_Units_CIS_B1_Uniform_Camo",
            "Aux501_Units_CIS_B1_Uniform_Camo_Late",
            "Aux501_Units_CIS_B1_Uniform_NCO",
            "Aux501_Units_CIS_B1_Uniform_Jammer",
            "Aux501_Units_CIS_B1_Uniform_Prototype",
            "Aux501_Units_CIS_B1_Uniform_Standard_Geonosis",
            "Aux501_Units_CIS_B1_Uniform_NCO_Geonosis"
        };
    };
};

class cfgWeapons
{
    class Uniform_Base;

    class Aux501_Units_Republic_501st_Uniform_Base: Uniform_Base
    {
        class ItemInfo;
    };

    class Aux501_Units_CIS_B1_Uniform_Standard: Aux501_Units_Republic_501st_Uniform_Base
    {
        scope = 2;
        scopeArsenal = 2;
        JLTS_isDroid = 1;
        displayName = "[CIS] B1 ARMR 01 - Standard";
        picture = "\MRC\JLTS\characters\DroidArmor\data\ui\b1_uniform_ui_ca.paa";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_Unit";
            containerClass = "Supply150";
            mass = 40;
            modelSides[] = {6};
            uniformType = "";
            armor = 2;
            armorStructural = 4;
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "standard";
        };
    };

    class Aux501_Units_CIS_B1_Uniform_Blue: Aux501_Units_CIS_B1_Uniform_Standard
    {
        displayName = "[CIS] B1 ARMR 02 - Pilot";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_Pilot_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "pilot";
        };
    };

    class Aux501_Units_CIS_B1_Uniform_Red: Aux501_Units_CIS_B1_Uniform_Standard
    {
        displayName = "[CIS] B1 ARMR 03 - AT";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_AT_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "at";
        };
    };

    class Aux501_Units_CIS_B1_Uniform_Green: Aux501_Units_CIS_B1_Uniform_Standard
    {
        displayName = "[CIS] B1 ARMR 04 - AA";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_AA_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "aa";
        };
    };

    class Aux501_Units_CIS_B1_Uniform_Camo: Aux501_Units_CIS_B1_Uniform_Standard
    {
        displayName = "[CIS] B1 ARMR 05 - Crew";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_Crew_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "crew";
        };
    };

    class Aux501_Units_CIS_B1_Uniform_Camo_Late: Aux501_Units_CIS_B1_Uniform_Standard
    {
        displayName = "[CIS] B1 ARMR 06 - Crew/Late";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_Crew_Late_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "crewlate";
        };
    };

    class Aux501_Units_CIS_B1_Uniform_NCO: Aux501_Units_CIS_B1_Uniform_Standard
    {
        displayName = "[CIS] B1 ARMR 07 - NCO";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_NCO_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "nco";
        };
    };

    class Aux501_Units_CIS_B1_Uniform_Jammer: Aux501_Units_CIS_B1_Uniform_Standard
    {
        displayName = "[CIS] B1 ARMR 08 - Jammer";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_Jammer_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "jammer";
        };
    };

    class Aux501_Units_CIS_B1_Uniform_Prototype: Aux501_Units_CIS_B1_Uniform_Standard
    {
        displayName = "[CIS] B1 ARMR 09 - Spec-Ops";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_Prototype_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "specops";
        };
    };

    class Aux501_Units_CIS_B1_Uniform_Standard_Geonosis: Aux501_Units_CIS_B1_Uniform_Standard
    {
        displayName = "[CIS] B1 ARMR GEO 01 - Standard";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_Geo_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "geonosis";
        };
    };

    class Aux501_Units_CIS_B1_Uniform_NCO_Geonosis: Aux501_Units_CIS_B1_Uniform_Standard
    {
        displayName = "[CIS] B1 ARMR GEO 02 - NCO";
        class ItemInfo: ItemInfo
        {
            uniformClass = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
        };
        class XtdGearInfo
        {
            model = "Aux501_ACEX_Gear_CIS_B1_Uniforms";
            variant = "geonco";
        };
    };
};

class XtdGearModels
{
    class CfgWeapons
    {
        ////B1 Battledroid Uniform////
        class Aux501_ACEX_Gear_CIS_B1_Uniforms
        {
            label = "";
            author = "501st Aux Team";
            options[]=
            {
                "variant"
            };
            class variant
            {
                label = "Variant";
                values[]=
                {
                    "standard",
                    "pilot",
                    "at",
                    "aa",
                    "crew",
                    "crewlate",
                    "nco",
                    "jammer",
                    "specops",
                    "geonosis",
                    "geonco"
                };
    
                class standard
                {
                    label = "Standard";
                };
                class at
                {
                    label = "AT";
                };
                class aa
                {
                    label = "AA";
                };
                class crew
                {
                    label = "Crew";
                };
                class crewlate
                {
                    label = "Crew/Late";
                };
                class nco
                {
                    label = "NCO";
                };
                class jammer
                {
                    label = "Jammer";
                };
                class pilot
                {
                    label = "Pilot";
                };
                class specops
                {
                    label = "Spec-Ops";
                };
                class geonosis
                {
                    label = "Geonosis";
                };
                class geonco
                {
                    label = "Geo/NCO";
                };
            };
        };
    };
};