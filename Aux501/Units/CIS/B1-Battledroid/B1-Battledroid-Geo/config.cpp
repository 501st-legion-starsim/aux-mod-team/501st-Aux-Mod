class cfgPatches
{
    class Aux501_Patch_Units_CIS_B1_Geo
    {
        addonRootClass = "Aux501_Patch_Units";
        requiredAddons[] = 
        {
            "Aux501_Patch_Units_CIS_B1"
        };
        units[] = 
        {
            "Aux501_Units_CIS_B1_Geo_Unit",
            "Aux501_Units_CIS_B1_Geo_NCO_Unit",

            "Aux501_Units_CIS_B1_Geo_AT_Unit",
            "Aux501_Units_CIS_B1_Geo_AA_Unit",
            "Aux501_Units_CIS_B1_Geo_Marksman_Unit",
            "Aux501_Units_CIS_B1_Geo_Heavy_Unit",
            "Aux501_Units_CIS_B1_Geo_Breacher_Unit"
        };
        weapons[] = {};
    };
};

class CfgVehicles
{
    class Aux501_Units_CIS_B1_Unit;
    class Aux501_Units_CIS_B1_NCO_Unit;
    class Aux501_Units_CIS_B1_AT_Unit;
    class Aux501_Units_CIS_B1_AA_Unit;
    class Aux501_Units_CIS_B1_Marksman_Unit;
    class Aux501_Units_CIS_B1_Heavy_Unit;
    class Aux501_Units_CIS_B1_Breacher_Unit;


    class Aux501_Units_CIS_B1_Geo_Unit: Aux501_Units_CIS_B1_Unit
    {
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_B1_Geo";
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidUnits\data\Droid_B1Geonosis_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_Standard_Geonosis";
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Standard_Geonosis";
    };

    class Aux501_Units_CIS_B1_Geo_NCO_Unit: Aux501_Units_CIS_B1_NCO_Unit
    {
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_B1_Geo";
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidUnits\data\Droid_B1GeonosisCommander_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_NCO_Geonosis";
        uniformClass = "Aux501_Units_CIS_B1_Uniform_NCO_Geonosis";
    };

    class Aux501_Units_CIS_B1_Geo_AT_Unit: Aux501_Units_CIS_B1_AT_Unit
    {
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_B1_Geo";
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidUnits\data\Droid_B1Geonosis_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_Standard_Geonosis_AT";
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Standard_Geonosis";
    };

    class Aux501_Units_CIS_B1_Geo_AA_Unit: Aux501_Units_CIS_B1_AA_Unit
    {
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_B1_Geo";
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidUnits\data\Droid_B1Geonosis_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_Standard_Geonosis_AA";
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Standard_Geonosis";
    };

    class Aux501_Units_CIS_B1_Geo_Marksman_Unit: Aux501_Units_CIS_B1_Marksman_Unit
    {
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_B1_Geo";
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidUnits\data\Droid_B1Geonosis_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_Standard_Geonosis";
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Standard_Geonosis";
    };

    class Aux501_Units_CIS_B1_Geo_Heavy_Unit: Aux501_Units_CIS_B1_Heavy_Unit
    {
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_B1_Geo";
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidUnits\data\Droid_B1Geonosis_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_Standard_Geonosis";
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Standard_Geonosis";
    };

    class Aux501_Units_CIS_B1_Geo_Breacher_Unit: Aux501_Units_CIS_B1_Breacher_Unit
    {
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_B1_Geo";
		hiddenSelectionsTextures[] = {"\MRC\JLTS\characters\DroidUnits\data\Droid_B1Geonosis_co.paa"};
        backpack = "Aux501_Units_CIS_B1_Backpack_Standard_Geonosis";
        uniformClass = "Aux501_Units_CIS_B1_Uniform_Standard_Geonosis";
    };
};

class CfgGroups
{
    class East
    {
        class Aux501_FactionClasses_Confederacy 
        {
            name = "[501st] Confederacy";
            class Aux501_Group_Editor_Category_Droid_Sections
            {
                name = "Droid Sections";
                class Aux501_CIS_Droid_Infantry_B1_Heavy_Section_Geo
                {
                    name = "GEO Heavy Section";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                    class Unit6
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AT_Unit";
                        rank = "PRIVATE";
                        position[] = {-1.5,-1.5,0};
                    };
                    class Unit7
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {2,-2,0};
                    };
                    class Unit8
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-2,-2,0};
                    };
                    class Unit9
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {2.5,-2.5,0};
                    };
                    class Unit10
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-2.5,-2.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_Light_Section_Geo
                {
                    name = "GEO Light Section";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                    class Unit6
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-1.5,-1.5,0};
                    };
                    class Unit7
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {2,-2,0};
                    };
                    class Unit8
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-2,-2,0};
                    };
                    class Unit9
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {2.5,-2.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_Weapon_Section_Geo
                {
                    name = "GEO Weapon Section";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AT_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                    class Unit6
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AT_Unit";
                        rank = "PRIVATE";
                        position[] = {-1.5,-1.5,0};
                    };
                    class Unit7
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Marksman_Unit";
                        rank = "PRIVATE";
                        position[] = {2,-2,0};
                    };
                    class Unit8
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Marksman_Unit";
                        rank = "PRIVATE";
                        position[] = {-2,-2,0};
                    };
                    class Unit9
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AA_Unit";
                        rank = "PRIVATE";
                        position[] = {2.5,-2.5,0};
                    };
                    class Unit10
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {-2.5,-2.5,0};
                    };
                    class Unit11
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {3,-3,0};
                    };
                };
            };

            class Aux501_Group_Editor_Category_Droid_Squads
            {
                name = "Droid Squads";
                side = 0;
                faction = "Aux501_FactionClasses_Confederacy";
                rarityGroup = 1;
                class Aux501_CIS_Droid_Infantry_B1_AA_Squad_Geo
                {
                    name = "GEO AA Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AA_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AA_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                    class Unit6
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-1.5,-1.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_AT_Squad_Geo
                {
                    name = "GEO AT Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AT_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AT_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                    class Unit6
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-1.5,-1.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_Squad_Geo
                {
                    name = "GEO B1 Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                    class Unit6
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-1.5,-1.5,0};
                    };
                    class Unit7
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {2,-2,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_Heavy_Assualt_Squad_Geo
                {
                    name = "GEO Heavy Assault Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Marksman_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                    class Unit6
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-1.5,-1.5,0};
                    };
                    class Unit7
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {2,-2,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_Heavy_Defense_Squad_Geo
                {
                    name = "GEO Heavy Defense Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Marksman_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                    class Unit6
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-1.5,-1.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_Light_Assault_Squad_Geo
                {
                    name = "GEO Light Assault Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_Light_Defense_Squad_Geo
                {
                    name = "GEO Light Defense Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Marksman_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_Weapon_Squad_Geo
                {
                    name = "GEO Weapon Squad";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AT_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AT_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                    class Unit4
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Marksman_Unit";
                        rank = "PRIVATE";
                        position[] = {-1,-1,0};
                    };
                    class Unit5
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1.5,-1.5,0};
                    };
                    class Unit6
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {-1.5,-1.5,0};
                    };
                };
            };

            class Aux501_Group_Editor_Category_Droid_Teams
            {
                name = "Droid Teams";
                side = 0;
                faction = "Aux501_FactionClasses_Confederacy";
                rarityGroup = 1;
                class Aux501_CIS_Droid_Infantry_B1_AA_Team_Geo
                {
                    name = "GEO AA Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AA_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AA_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_AT_Team_Geo
                {
                    name = "GEO AT Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AT_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AT_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_Team_Geo
                {
                    name = "GEO B1 Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_CQC_Team_Geo
                {
                    name = "GEO CQC Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                };
                class Aux501_CIS_B1_Defense_Team_Geo
                {
                    name = "GEO Defense Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_Heavy_Assualt_Team_Geo
                {
                    name = "GEO Heavy Assault Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_Light_Assault_Team_Geo
                {
                    name = "GEO Light Assault Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_Marksman_Team_Geo
                {
                    name = "GEO Marksman Team ";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Marksman_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Marksman_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                };
                class Aux501_CIS_Droid_Infantry_B1_Weapons_Team_Geo
                {
                    name = "GEO Weapons Team";
                    side = 0;
                    faction = "Aux501_FactionClasses_Confederacy";
                    icon = "\A3\ui_f\data\map\markers\nato\o_inf.paa";
                    rarityGroup = 1;
                    class Unit0 
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_NCO_Unit";
                        rank = "SERGEANT";
                        position[] = {0,0,0};
                    };
                    class Unit1
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_AT_Unit";
                        rank = "CORPORAL";
                        position[] = {0.5,-0.5,0};
                    };
                    class Unit2
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Heavy_Unit";
                        rank = "PRIVATE";
                        position[] = {-0.5,-0.5,0};
                    };
                    class Unit3
                    {
                        side = 0;
                        vehicle = "Aux501_Units_CIS_B1_Geo_Breacher_Unit";
                        rank = "PRIVATE";
                        position[] = {1,-1,0};
                    };
                };
            };
        };
    };
};