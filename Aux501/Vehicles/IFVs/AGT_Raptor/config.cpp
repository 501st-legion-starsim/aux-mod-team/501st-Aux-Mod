class CfgPatches
{
    class Aux501_Patch_Vehicles_IFV_Raptor
    {
        Author = "501st Aux Team";
        addonRootClass = "Aux501_Patch_Vehicles";
        requiredAddons[] = 
        {
            "A3_Drones_F_Soft_F_Gamma_UGV_01"
        };
        requiredVersion = 0.1;
        units[] = 
        {
            "Aux501_Vehicles_IFV_agtRaptor_base",
            "Aux501_Vehicles_IFV_agtRaptor",
            "Aux501_Vehicles_IFV_agtRaptor_tan",
            "Aux501_Vehicles_IFV_agtRaptor_Snow",
            "Aux501_Vehicles_IFV_agtRaptor_Urban",
            "Aux501_Vehicles_IFV_agtRaptor_Woodland",
            "Aux501_Vehicles_IFV_agtRaptor_TradeFederation"
        };
        weapons[] = 
        {
            "Aux501_eweb_blaster_raptor"
        };
    };
};

class DefaultVehicleSystemsDisplayManagerLeft
{
    class components;
};
class DefaultVehicleSystemsDisplayManagerRight
{
    class components;
};

class Mode_SemiAuto;					// External Class Reference - For Inheriting information about firing modes
class Mode_FullAuto;					// External Class Reference - For Inheriting information about firing modes
class Optics_Armored;					// External Class Reference - For Inheriting information about optics

class Optics_Gunner_MBT_01: Optics_Armored
{
    class Wide;
    class Medium;
    class Narrow;
};

class Optics_Gunner_MBT_02 : Optics_Armored
{
    class Wide;
    class Medium;
    class Narrow;
};

class RCWSOptics;

class CfgVehicles
{
    class LandVehicle;						// External Class reference - For Inheritance of base classes

    // class Car;								// External class reference
    class House_F;							// External Class reference - For static vehicle objects
    class Items_base_F;						// External Class reference - So static decorative vehicles have gravity
    // class UGV_01_base_F;

    class Wreck_base_f;						// External Class reference - For wrecks

    class Car: LandVehicle
    {
        class Hitpoints;
    };

    // =================================
    // BASE CAR INHERITANCE
    // =================================
    class Car_F: Car
    {
        /// we want to use hitpoints predefined for all cars
        class HitPoints : Hitpoints
        {
            class HitLFWheel;
            class HitLF2Wheel;
            class HitRFWheel;
            class HitRF2Wheel;
            class HitBody;
            class HitGlass1;
            class HitGlass2;
            class HitGlass3;
            class HitGlass4;
        };
        class Turrets;
        class NewTurret;
        class EventHandlers;			//External class reference
        class AnimationSources;			//External class reference
    };

    // Drone Base
    class UGV_01_base_F : Car_F
    {
        class Turrets;
        class HitPoints;
    };

    // Class Drone + Turret Base
    class UGV_01_rcws_base_F : UGV_01_base_F
    {
        class Components;
        class AnimationSources;
        
        class Turrets : Turrets 
        {
            class MainTurret;
        };

        class HitPoints : HitPoints
        {
            class HitEngine;
            class HitFuel;
            class HitBody;
        };
    };
    class Aux501_Vehicles_IFV_agtRaptor_base: UGV_01_rcws_base_F
    {
        Author = "501st Aux Team";
        displayName = "AGT_BASE"; /// displayed in Editor
        editorpreview	= "";
        crew = "Aux501_Units_CIS_UAV_Crew_Unit";
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_IFVs";
        side = 0;
        icon = "\Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\ui\agt_icon_UI_ca.paa";
        model = "\Aux501\Vehicles\IFVs\AGT_Raptor\agt\Aux501_Vics_ifv_agtRaptor.p3d";  /// simple path to model
        hiddenSelections[] = {"Camo1","Camo2"};
        hiddenSelectionsTextures[] = 
        {
            "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo1_co.paa",
            "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo2_co.paa"
        };
        weapons[] = {"CarHorn"};
        magazines[] = {};
        memoryPointGun[] = {};
        uavCameraDriverPos = "PiP0_pos";
        uavCameraDriverDir = "PiP0_dir";
        armor = 70;
        armorStructural = 4;
        class EventHandlers: EventHandlers
        {
            postInit = "if (local (_this select 0)) then {[(_this select 0), """", [], true] call bis_fnc_initVehicle;};";
        };
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                isCopilot = 0;
                dontCreateAI = 0;
                body = "mainTurret";
                gun = "mainGun";
                memoryPointGunnerOptics = "PiP1_pos";
                memoryPointGun = "machinegun";
                gunnerForceOptics = 1;
                gunnerOpticsModel = "A3\drones_f\Weapons_F_Gamma\Reticle\UAV_Optics_Gunner_F.p3d";
                turretInfoType = "RscOptics_UGV_gunner";
                weapons[]=
                {
                    "Aux501_eweb_blaster_raptor"
                };
                magazines[]=
                {
                    "Aux501_Weapons_Mags_CIS_eweb",
                    "Aux501_Weapons_Mags_CIS_eweb",
                    "Aux501_Weapons_Mags_CIS_eweb",
                    "Aux501_Weapons_Mags_CIS_eweb",
                    "Aux501_Weapons_Mags_CIS_eweb",
                    "Aux501_Weapons_Mags_CIS_eweb",
                    "Aux501_Weapons_Mags_CIS_eweb",
                    "Aux501_Weapons_Mags_CIS_eweb",
                    "Aux501_Weapons_Mags_CIS_eweb"
                };
                soundServo[] = {"A3\Sounds_F\vehicles\soft\UGV_01\Servo_UGV_gunner",0.31622776,1,30};
                soundServoVertical[] = {"A3\Sounds_F\vehicles\soft\UGV_01\Servo_UGV_gunner_vertical",0.31622776,1,30};
                minElev = -10;
                maxElev = 60;
                forceHideGunner = 1;
                outGunnerMayFire = 1;
                discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500};
                discreteDistanceInitIndex = 2;
                stabilizedInAxes = 3;
                class HitPoints
                {
                    class HitTurret
                    {
                        armor = 0.5;
                        material = -1;
                        armorComponent = "hit_main_turret";
                        name = "hit_main_turret_point";
                        visual = "OtocVez";
                        passThrough = 0;
                        minimalHit = 0.03;
                        explosionShielding = 0.4;
                        radius = 0.25;
                        isTurret = 1;
                    };
                    class HitGun
                    {
                        armor = 0.5;
                        material = -1;
                        armorComponent = "hit_main_gun";
                        name = "hit_main_gun_point";
                        visual = "OtocHlaven";
                        passThrough = 0;
                        minimalHit = 0.03;
                        explosionShielding = 0.4;
                        radius = 0.2;
                        isGun = 1;
                    };
                };
                class ViewOptics: RCWSOptics
                {
                    thermalMode[] = {0,1};
                    visionMode[] = {"Normal","TI"};
                    initFov = 0.4667;
                    maxFov = 0.4667;
                    minFov = 0.035;
                    initAngleX = 0;
                    minAngleX = -30;
                    maxAngleX = 30;
                    minAngleY = -100;
                    maxAngleY = 100;
                    directionStabilized = 1;
                    initAngleY = 0;
                };
                class Components
                {
                    class VehicleSystemsDisplayManagerComponentLeft: DefaultVehicleSystemsDisplayManagerLeft
                    {
                        class components
                        {
                            class EmptyDisplay
                            {
                                componentType = "EmptyDisplayComponent";
                            };
                            class MinimapDisplay
                            {
                                componentType = "MinimapDisplayComponent";
                                resource = "RscCustomInfoMiniMap";
                            };
                            class UAVDisplay
                            {
                                componentType = "UAVFeedDisplayComponent";
                            };
                        };
                    };
                    class VehicleSystemsDisplayManagerComponentRight: DefaultVehicleSystemsDisplayManagerRight
                    {
                        class components
                        {
                            class EmptyDisplay
                            {
                                componentType = "EmptyDisplayComponent";
                            };
                            class MinimapDisplay
                            {
                                componentType = "MinimapDisplayComponent";
                                resource = "RscCustomInfoMiniMap";
                            };
                            class UAVDisplay
                            {
                                componentType = "UAVFeedDisplayComponent";
                            };
                        };
                    };
                };
            };
        };
        class HitPoints: HitPoints
        {
            class HitRFWheel
            {
                armor = 1;
                explosionShielding = 4;
                armorComponent = "hit_fr_wheel";
                material = -1;
                passThrough = 0.3;
                name = "wheel_fr_point";
                visual = "wheel_fr_destruct";
            };
            class HitLFWheel: HitRFWheel
            {
                armorComponent = "hit_fl_wheel";
                name = "wheel_fl_point";
                visual = "wheel_fl_destruct";
            };
            class HitLF2Wheel: HitRFWheel
            {
                armorComponent = "hit_bl_wheel";
                name = "wheel_bl_point";
                visual = "wheel_bl_destruct";
            };
            class HitRF2Wheel: HitRFWheel
            {
                armorComponent = "hit_br_wheel";
                name = "wheel_br_point";
                visual = "wheel_br_destruct";
            };
            class HitEngine: HitEngine
            {
                visual = "engine_destruct";
                name = "engine_point";
                armorComponent = "hit_engine";
                armor = 0.5;
                material = -1;
                passThrough = 0.3;
            };
            class HitFuel: HitFuel
            {
                visual = "fuel_destruct";
                name = "hit_fuel_point";
                armorComponent = "hit_fuel";
                armor = 1;
                explosionShielding = 0.6;
                material = -1;
                minimalHit = 0.1;
                passThrough = 0.3;
                radius = 0.3;
            };
            class HitBody: HitBody
            {
                name = "karoserie";
                visual = "zbytek";
                armor = 0.5;
                material = -1;
                passThrough = 0.3;
            };
        };
        class Components: Components
        {
            class TransportPylonsComponent
            {
                uiPicture = "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\ui\agt_icon_UI_ca.paa";
                class Pylons
                {
                    class PylonLeft
                    {
                        attachment = "";
                        priority = 5;
                        turret[] = {0};
                        UIposition[] = {0.15,0.2};
                        hardpoints[] = 
                        {
                            "SCALPEL_1RND",
                            "B_ASRAAM",
                            "DAR",
                            "DAGR",
                            "B_AMRAAM_D_DUAL_RAIL",
                            "B_SDB_QUAD_RAIL",
                            "B_GBU12",
                            "B_AGM65_RAIL",
                            "BRU_32_EJECTOR",
                            "B_A143_BUZZARD_CENTER_PYLON"
                        };
                    };
                    class PylonRight: PylonLeft
                    {
                        UIposition[] = {0.55,0.2};
                    };
                };
                class Presets
                {
                    class Empty
                    {
                        displayName = "Empty";
                        attachment[] = {};
                    };
                    class Test
                    {
                        displayname = "Tester";
                        attachment[] = {"PylonRack_12Rnd_PG_missiles","PylonRack_12Rnd_missiles"};
                    };
                };
            };
        };
        class AnimationSources: AnimationSources
        {
            class HitFRWheel
            {
                hitPoint = "HitRFWheel";
                raw = 1;
                source = "Hit";
            };
            class HitEngine
            {
                hitPoint = "HitEngine";
                raw = 1;
                source = "Hit";
            };
            class HitFLWheel: HitFRWheel
            {
                hitPoint = "HitLFWheel";
            };
            class HitBRWheel: HitFRWheel
            {
                hitPoint = "HitRF2Wheel";
            };
            class HitBLWheel: HitFRWheel
            {
                hitPoint = "HitLF2Wheel";
            };
        };
        class TransportItems{};
        attenuationEffectType = "CarAttenuation";
        soundGetIn[] = {"A3\sounds_f\vehicles\armor\noises\get_in_out",0.562341,1};
        soundGetOut[] = {"A3\sounds_f\vehicles\armor\noises\get_in_out",0.562341,1,20};
        soundDammage[] = {"",0.562341,1};
        soundEngineOnInt[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\Firebrand_on.wav",2,1};
        soundEngineOnExt[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\Firebrand_on.wav",4,1,200};
        soundEngineOffInt[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\Firebrand_off.wav",2,1};
        soundEngineOffExt[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\Firebrand_off.wav",4,1,200};
        buildCrash0[] = {"A3\sounds_f\Vehicles\crashes\crash_08",1,1,200};
        buildCrash1[] = {"A3\sounds_f\Vehicles\crashes\crash_09",1,1,200};
        buildCrash2[] = {"A3\sounds_f\Vehicles\crashes\crash_10",1,1,200};
        buildCrash3[] = {"A3\sounds_f\Vehicles\crashes\crash_11",1,1,200};
        soundBuildingCrash[] = {"buildCrash0",0.25,"buildCrash1",0.25,"buildCrash2",0.25,"buildCrash3",0.25};
        WoodCrash0[] = {"A3\sounds_f\Vehicles\crashes\crash_08",1,1,200};
        WoodCrash1[] = {"A3\sounds_f\Vehicles\crashes\crash_09",1,1,200};
        WoodCrash2[] = {"A3\sounds_f\Vehicles\crashes\crash_10",1,1,200};
        WoodCrash3[] = {"A3\sounds_f\Vehicles\crashes\crash_11",1,1,200};
        WoodCrash4[] = {"A3\sounds_f\Vehicles\crashes\crash_01",1,1,200};
        WoodCrash5[] = {"A3\sounds_f\Vehicles\crashes\crash_08",1,1,200};
        soundWoodCrash[] = {"woodCrash0",0.166,"woodCrash1",0.166,"woodCrash2",0.166,"woodCrash3",0.166,"woodCrash4",0.166,"woodCrash5",0.166};
        ArmorCrash0[] = {"A3\sounds_f\Vehicles\crashes\crash_08",1,1,200};
        ArmorCrash1[] = {"A3\sounds_f\Vehicles\crashes\crash_09",1,1,200};
        ArmorCrash2[] = {"A3\sounds_f\Vehicles\crashes\crash_10",1,1,200};
        ArmorCrash3[] = {"A3\sounds_f\Vehicles\crashes\crash_11",1,1,200};
        soundArmorCrash[] = {"ArmorCrash0",0.25,"ArmorCrash1",0.25,"ArmorCrash2",0.25,"ArmorCrash3",0.25};
        class Sounds
        {
            class Idle_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",4,1,200};
                frequency = "0.95 + ((rpm/ 2640) factor[(400/ 2640),(900/ 2640)])*0.15";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(100/ 2640),(200/ 2640)]) * ((rpm/ 2640) factor[(900/ 2640),(700/ 2640)]))";
            };
            class Engine
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",4.48808,1,240};
                frequency = "0.8 + ((rpm/ 2640) factor[(700/ 2640),(1100/ 2640)])*0.2";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(705/ 2640),(850/ 2640)]) * ((rpm/ 2640) factor[(1100 / 2640),(950/ 2640)]))";
            };
            class Engine1_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",5.65016,1,280};
                frequency = "0.8 + ((rpm/ 2640) factor[(950/ 2640),(1400/ 2640)])*0.2";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(900/ 2640),(1050/ 2640)]) * ((rpm/ 2640) factor[(1400/ 2640),(1200/ 2640)]))";
            };
            class Engine2_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",6.3396,1,320};
                frequency = "0.8 + ((rpm/ 2640) factor[(1200/ 2640),(1700/ 2640)])*0.2";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(1170/ 2640),(1380/ 2640)]) * ((rpm/ 2640) factor[(1700/ 2640),(1500/ 2640)]))";
            };
            class Engine3_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",7.11313,1,360};
                frequency = "0.8 + ((rpm/ 2640) factor[(1500/ 2640),(2100/ 2640)])*0.1";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(1500/ 2640),(1670/ 2640)]) * ((rpm/ 2640) factor[(2100/ 2640),(1800/ 2640)]))";
            };
            class Engine4_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",7.98104,1,400};
                frequency = "0.8 + ((rpm/ 2640) factor[(1800/ 2640),(2300/ 2640)])*0.1";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(1780/ 2640),(2060/ 2640)]) * ((rpm/ 2640) factor[(2450/ 2640),(2200/ 2640)]))";
            };
            class Engine5_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",8.95489,1,440};
                frequency = "0.8 + ((rpm/ 2640) factor[(2100/ 2640),(2640/ 2640)])*0.1";
                volume = "engineOn*camPos*((rpm/ 2640) factor[(2150/ 2640),(2500/ 2640)])";
            };
            class IdleThrust
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",5.65016,1,200};
                frequency = "0.8 + ((rpm/ 2640) factor[(400/ 2640),(900/ 2640)])*0.15";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(100/ 2640),(200/ 2640)]) * ((rpm/ 2640) factor[(900/ 2640),(700/ 2640)]))";
            };
            class EngineThrust
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",7.11313,1,200};
                frequency = "0.8 + ((rpm/ 2640) factor[(700/ 2640),(1100/ 2640)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(705/ 2640),(850/ 2640)]) * ((rpm/ 2640) factor[(1100 / 2640),(950/ 2640)]))";
            };
            class Engine1_Thrust_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",8.95489,1,230};
                frequency = "0.8 + ((rpm/ 2640) factor[(950/ 2640),(1400/ 2640)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(900/ 2640),(1050/ 2640)]) * ((rpm/ 2640) factor[(1400/ 2640),(1200/ 2640)]))";
            };
            class Engine2_Thrust_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",10.0475,1,290};
                frequency = "0.8 + ((rpm/ 2640) factor[(1200/ 2640),(1700/ 2640)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1170/ 2640),(1380/ 2640)]) * ((rpm/ 2640) factor[(1700/ 2640),(1500/ 2640)]))";
            };
            class Engine3_Thrust_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",8.95489,1,350};
                frequency = "0.8 + ((rpm/ 2640) factor[(1500/ 2640),(2100/ 2640)])*0.1";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1500/ 2640),(1670/ 2640)]) * ((rpm/ 2640) factor[(2100/ 2640),(1800/ 2640)]))";
            };
            class Engine4_Thrust_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",11.27353,1,400};
                frequency = "0.8 + ((rpm/ 2640) factor[(1800/ 2640),(2300/ 2640)])*0.1";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1780/ 2640),(2060/ 2640)]) * ((rpm/ 2640) factor[(2450/ 2640),(2200/ 2640)]))";
            };
            class Engine5_Thrust_ext
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",12.64913,1,450};
                frequency = "0.8 + ((rpm/ 2640) factor[(2100/ 2640),(2640/ 2640)])*0.1";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/ 2640) factor[(2150/ 2640),(2500/ 2640)])";
            };
            class Idle_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",2,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(400/ 2640),(900/ 2640)])*0.15";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(100/ 2640),(200/ 2640)]) * ((rpm/ 2640) factor[(900/ 2640),(700/ 2640)]))";
            };
            class Engine_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",1.41589,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(700/ 2640),(1100/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(705/ 2640),(850/ 2640)]) * ((rpm/ 2640) factor[(1100 / 2640),(950/ 2640)]))";
            };
            class Engine1_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",1.58866,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(950/ 2640),(1400/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(900/ 2640),(1050/ 2640)]) * ((rpm/ 2640) factor[(1400/ 2640),(1200/ 2640)]))";
            };
            class Engine2_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1200/ 2640),(1700/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(1170/ 2640),(1380/ 2640)]) * ((rpm/ 2640) factor[(1700/ 2640),(1500/ 2640)]))";
            };
            class Engine3_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",2,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1500/ 2640),(2100/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(1500/ 2640),(1670/ 2640)]) * ((rpm/ 2640) factor[(2100/ 2640),(1800/ 2640)]))";
            };
            class Engine4_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",2.24404,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1800/ 2640),(2300/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(1780/ 2640),(2060/ 2640)]) * ((rpm/ 2640) factor[(2450/ 2640),(2200/ 2640)]))";
            };
            class Engine5_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",2.51785,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(2100/ 2640),(2640/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*((rpm/ 2640) factor[(2150/ 2640),(2500/ 2640)])";
            };
            class IdleThrust_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",2.51785,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(400/ 2640),(900/ 2640)])*0.15";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(100/ 2640),(200/ 2640)]) * ((rpm/ 2640) factor[(900/ 2640),(700/ 2640)]))";
            };
            class EngineThrust_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",1.58866,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(700/ 2640),(1100/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(705/ 2640),(850/ 2640)]) * ((rpm/ 2640) factor[(1100 / 2640),(950/ 2640)]))";
            };
            class Engine1_Thrust_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",1.7825,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(950/ 2640),(1400/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(900/ 2640),(1050/ 2640)]) * ((rpm/ 2640) factor[(1400/ 2640),(1200/ 2640)]))";
            };
            class Engine2_Thrust_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",1.7825,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1200/ 2640),(1700/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1170/ 2640),(1380/ 2640)]) * ((rpm/ 2640) factor[(1700/ 2640),(1500/ 2640)]))";
            };
            class Engine3_Thrust_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",2,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1500/ 2640),(2100/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1500/ 2640),(1670/ 2640)]) * ((rpm/ 2640) factor[(2100/ 2640),(1800/ 2640)]))";
            };
            class Engine4_Thrust_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",2.24404,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1800/ 2640),(2300/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1780/ 2640),(2060/ 2640)]) * ((rpm/ 2640) factor[(2450/ 2640),(2200/ 2640)]))";
            };
            class Engine5_Thrust_int
            {
                sound[] = {"\SFA_Vehicles_R\data\Firebrand\SFX\firebrand_idle.wav",2.51785,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(2100/ 2640),(2640/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/ 2640) factor[(2150/ 2640),(2500/ 2640)])";
            };
            class NoiseInt
            {
                sound[] = {"A3\sounds_f\vehicles\armor\noises\noise_tank_int_1",0.501187,1};
                frequency = "1";
                volume = "(1-camPos)*(angVelocity max 0.04)*(speed factor[4, 15])";
            };
            class NoiseExt
            {
                sound[] = {"A3\sounds_f\vehicles\armor\noises\noise_tank_ext_1",0.891251,1,50};
                frequency = "1";
                volume = "camPos*(angVelocity max 0.04)*(speed factor[4, 15])";
            };
            class ThreadsOutH0
            {
                sound[] = {"",0.398107,1,140};
                frequency = "1";
                volume = "engineOn*camPos*(1-grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-0) max 0)/ 60),(((-5) max 5)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-15) max 15)/ 60),(((-10) max 10)/ 60)]))";
            };
            class ThreadsOutH1
            {
                sound[] = {"",0.446684,1,160};
                frequency = "1";
                volume = "engineOn*camPos*(1-grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-10) max 10)/ 60),(((-15) max 15)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-30) max 30)/ 60),(((-25) max 25)/ 60)]))";
            };
            class ThreadsOutH2
            {
                sound[] = {"",0.501187,1,180};
                frequency = "1";
                volume = "engineOn*camPos*(1-grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-25) max 25)/ 60),(((-30) max 30)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-45) max 45)/ 60),(((-40) max 40)/ 60)]))";
            };
            class ThreadsOutH3
            {
                sound[] = {"",0.562341,1,200};
                frequency = "1";
                volume = "engineOn*camPos*(1-grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-40) max 40)/ 60),(((-45) max 45)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-55) max 55)/ 60),(((-50) max 50)/ 60)]))";
            };
            class ThreadsOutH4
            {
                sound[] = {"",0.562341,1,220};
                frequency = "1";
                volume = "engineOn*camPos*(1-grass)*((((-speed*3.6) max speed*3.6)/ 60) factor[(((-49) max 49)/ 60),(((-53) max 53)/ 60)])";
            };
            class ThreadsOutS0
            {
                sound[] = {"",0.316228,1,120};
                frequency = "1";
                volume = "engineOn*(camPos)*(grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-0) max 0)/ 60),(((-5) max 5)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-15) max 15)/ 60),(((-10) max 10)/ 60)]))";
            };
            class ThreadsOutS1
            {
                sound[] = {"",0.354813,1,140};
                frequency = "1";
                volume = "engineOn*(camPos)*(grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-10) max 10)/ 60),(((-15) max 15)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-30) max 30)/ 60),(((-25) max 25)/ 60)]))";
            };
            class ThreadsOutS2
            {
                sound[] = {"",0.398107,1,160};
                frequency = "1";
                volume = "engineOn*(camPos)*(grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-25) max 25)/ 60),(((-30) max 30)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-45) max 45)/ 60),(((-40) max 40)/ 60)]))";
            };
            class ThreadsOutS3
            {
                sound[] = {"",0.446684,1,180};
                frequency = "1";
                volume = "engineOn*(camPos)*(grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-40) max 40)/ 60),(((-45) max 45)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-55) max 55)/ 60),(((-50) max 50)/ 60)]))";
            };
            class ThreadsOutS4
            {
                sound[] = {"",0.501187,1,200};
                frequency = "1";
                volume = "engineOn*(camPos)*(grass)*((((-speed*3.6) max speed*3.6)/ 60) factor[(((-49) max 49)/ 60),(((-53) max 53)/ 60)])";
            };
            class ThreadsInH0
            {
                sound[] = {"",0.251189,1};
                frequency = "1";
                volume = "engineOn*(1-camPos)*(1-grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-0) max 0)/ 60),(((-5) max 5)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-15) max 15)/ 60),(((-10) max 10)/ 60)]))";
            };
            class ThreadsInH1
            {
                sound[] = {"",0.281838,1};
                frequency = "1";
                volume = "engineOn*(1-camPos)*(1-grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-10) max 10)/ 60),(((-15) max 15)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-30) max 30)/ 60),(((-25) max 25)/ 60)]))";
            };
            class ThreadsInH2
            {
                sound[] = {"",0.316228,1};
                frequency = "1";
                volume = "engineOn*(1-camPos)*(1-grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-25) max 25)/ 60),(((-30) max 30)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-45) max 45)/ 60),(((-40) max 40)/ 60)]))";
            };
            class ThreadsInH3
            {
                sound[] = {"",0.354813,1};
                frequency = "1";
                volume = "engineOn*(1-camPos)*(1-grass)*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-40) max 40)/ 60),(((-45) max 45)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-55) max 55)/ 60),(((-50) max 50)/ 60)]))";
            };
            class ThreadsInH4
            {
                sound[] = {"",0.398107,1};
                frequency = "1";
                volume = "engineOn*(1-camPos)*(1-grass)*((((-speed*3.6) max speed*3.6)/ 60) factor[(((-49) max 49)/ 60),(((-53) max 53)/ 60)])";
            };
            class ThreadsInS0
            {
                sound[] = {"",0.316228,1};
                frequency = "1";
                volume = "engineOn*(1-camPos)*grass*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-0) max 0)/ 60),(((-5) max 5)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-15) max 15)/ 60),(((-10) max 10)/ 60)]))";
            };
            class ThreadsInS1
            {
                sound[] = {"",0.316228,1};
                frequency = "1";
                volume = "engineOn*(1-camPos)*grass*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-10) max 10)/ 60),(((-15) max 15)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-30) max 30)/ 60),(((-25) max 25)/ 60)]))";
            };
            class ThreadsInS2
            {
                sound[] = {"",0.354813,1};
                frequency = "1";
                volume = "engineOn*(1-camPos)*grass*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-25) max 25)/ 60),(((-30) max 30)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-45) max 45)/ 60),(((-40) max 40)/ 60)]))";
            };
            class ThreadsInS3
            {
                sound[] = {"",0.354813,1};
                frequency = "1";
                volume = "engineOn*(1-camPos)*grass*(((((-speed*3.6) max speed*3.6)/ 60) factor[(((-40) max 40)/ 60),(((-45) max 45)/ 60)]) * ((((-speed*3.6) max speed*3.6)/ 60) factor[(((-55) max 55)/ 60),(((-50) max 50)/ 60)]))";
            };
            class ThreadsInS4
            {
                sound[] = {"",0.398107,1};
                frequency = "1";
                volume = "engineOn*(1-camPos)*grass*((((-speed*3.6) max speed*3.6)/ 60) factor[(((-49) max 49)/ 60),(((-53) max 53)/ 60)])";
            };
        };
        canFloat = 1;
        waterLeakiness = 2.5;
        waterPPInVehicle = 0;
        waterAngularDampingCoef = 0.5;
        waterResistanceCoef = 0.005;
        waterSpeedFactor = 1;
        waterSpeedCoef = 3;
        accelAidForceCoef = 30;
        accelAidForceSpd = 20;
        waterLinearDampingCoefY = 10;
        maxSpeed = 250;
        wheelCircumference = 2.277;
        idleRpm = 1000;
        redRpm = 7000;
        differentialType = "all_limited";
        frontRearSplit = 0.45;
        minOmega = 104.72;
        maxOmega = 850;
        enginePower = 400;
        peakTorque = 3000;
        torqueCurve[] = {{0,0},{0.221759,0.883333},{0.369599,0.933333},{0.431199,0.95},{0.492799,0.933333},{0.739198,0.916667},{0.923998,0.866667},{0.985598,0.783333}};
        changeGearMinEffectivity[] = {0.95,0.15,0.9,0.9,0.9,0.9,0.9,0.8};
        switchTime = 0.31;
        latency = 1;
        class Wheels
        {
            class LF
            {
                boneName = "wheel_lf_damper";
                steering = 1;
                side = "left";
                center = "wheel_lf_axis";
                boundary = "wheel_lf_bound";
                width = 0.2;
                mass = 4000;
                MOI = 5.3;
                dampingRate = 0.5;
                maxBrakeTorque = 3000;
                maxHandBrakeTorque = 0;
                suspTravelDirection[] = {0,-1,0};
                suspForceAppPointOffset = "wheel_lf_axis";
                tireForceAppPointOffset = "wheel_lf_axis";
                maxCompression = 0.2;
                MaxDroop = 0.5;
                sprungMass = 2000;
                springStrength = 50000;
                springDamperRate = 6725;
                longitudinalStiffnessPerUnitGravity = 100000;
                latStiffX = 25;
                latStiffY = 18000;
                frictionVsSlipGraph[] = {{0,1},{0.5,1},{1,1}};
            };
            class LR: LF
            {
                boneName = "wheel_lb_damper";
                steering = 0;
                center = "wheel_lb_axis";
                boundary = "wheel_lb_bound";
                suspForceAppPointOffset = "wheel_lb_axis";
                tireForceAppPointOffset = "wheel_lb_axis";
                maxHandBrakeTorque = 3000;
            };
            class RF: LF
            {
                side = "right";
                boneName = "wheel_rf_damper";
                center = "wheel_rf_axis";
                boundary = "wheel_rf_bound";
                suspForceAppPointOffset = "wheel_rf_axis";
                tireForceAppPointOffset = "wheel_rf_axis";
                steering = 1;
            };
            class RR: RF
            {
                boneName = "wheel_rb_damper";
                steering = 0;
                center = "wheel_rb_axis";
                boundary = "wheel_rb_bound";
                suspForceAppPointOffset = "wheel_rb_axis";
                tireForceAppPointOffset = "wheel_rb_axis";
                maxHandBrakeTorque = 3000;
            };
        };
        /// damage changes material in specific places (visual in hitPoint)
        class Damage 
        {
            tex[]={};
            mat[]= 
            {
                "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo1.rvmat", 		/// material mapped in model
                "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo1_damage.rvmat", 	/// changes to this one once damage of the part reaches 0.5
                "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo1_destruct.rvmat",		/// changes to this one once damage of the part reaches 1

                "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo2.rvmat", 		/// material mapped in model
                "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo2_damage.rvmat", 	/// changes to this one once damage of the part reaches 0.5
                "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo2_destruct.rvmat"		/// changes to this one once damage of the part reaches 1
            };
        }; 
        class Reflectors
        {
            class Left
            {
                color[] = {1900,1800,1700};
                ambient[] = {5,5,5};
                intensity = 1;
                size = 1;
                innerAngle = 100;
                outerAngle = 179;
                coneFadeCoef = 10;
                position = "Light_L_Pos";
                direction = "Light_L_Dir";
                hitpoint = "Light_b_hitpoint";
                selection = "Light_L_Lamp";
                useFlare = 0;
                flareSize = 1;
                flareMaxDistance = 250;
                dayLight = 0;
                class Attenuation
                {
                    start = 1;
                    constant = 0;
                    linear = 0;
                    quadratic = 0.25;
                    hardLimitStart = 30;
                    hardLimitEnd = 60;
                };
            };
            class Right: Left
            {
                position = "Light_R_Pos";
                direction = "Light_R_Dir";
                hitpoint = "Light_b_hitpoint";
                selection = "Light_R_Lamp";
            };
        };
        class TextureSources
        {
            class CIS
            {
                Author = "501st Aux Team";
                displayName = "Separatist";
                textures[] = 
                {
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo1_co.paa",
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo2_co.paa"
                };
                factions[] = 
                {
                    "Aux501_FactionClasses_Confederacy"
                };
            };
            class Tan: CIS
            {
                displayName = "Tan";
                textures[] = 
                {
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo1_tan_co.paa",
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo2_tan_co.paa"
                };
            };
            class Olive: CIS
            {
                displayName = "Olive";
                textures[] = 
                {
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo1_olive_co.paa",
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\camo2_olive_co.paa"
                };
            };
            class Snow: CIS
            {
                displayName = "Snow";
                textures[] = 
                {
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\snow\camo1_co.paa",
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\snow\camo2_co.paa"
                };
            };
            class Urban: CIS
            {
                displayName = "Urban";
                textures[] = 
                {
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\urban\camo1_co.paa",
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\urban\camo2_co.paa"
                };
            };
            class Woodland: CIS
            {
                displayName = "Woodland";
                textures[] = 
                {
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\woodland\camo1_co.paa",
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\woodland\camo2_co.paa"
                };
            };
            class TradeFederation: CIS
            {
                displayName = "Trade Federation";
                textures[] = 
                {
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\tf\camo1_co.paa",
                    "Aux501\Vehicles\IFVs\AGT_Raptor\agt\data\tf\camo2_co.paa"
                };
            };
        };
    };
    
    class Aux501_Vehicles_IFV_agtRaptor: Aux501_Vehicles_IFV_agtRaptor_base
    {
        scope = 2;
        scopeCurator = 2;
        displayName = "AGT 'Raptor'";
        textureList[] = {"CIS",1};
    };
    class Aux501_Vehicles_IFV_agtRaptor_tan: Aux501_Vehicles_IFV_agtRaptor
    {
        displayName = "AGT 'Raptor' - Tan";
        textureList[] = {"Tan",1};
    };
    class Aux501_Vehicles_IFV_agtRaptor_Snow: Aux501_Vehicles_IFV_agtRaptor
    {
        displayName = "AGT 'Raptor' - Snow";
        textureList[] = {"Snow",1};
    };
    class Aux501_Vehicles_IFV_agtRaptor_Urban: Aux501_Vehicles_IFV_agtRaptor
    {
        displayName = "AGT 'Raptor' - Urban";
        textureList[] = {"Urban",1};
    };
    class Aux501_Vehicles_IFV_agtRaptor_Woodland: Aux501_Vehicles_IFV_agtRaptor
    {
        displayName = "AGT 'Raptor' - Woodland";
        textureList[] = {"Woodland",1};
    };
    class Aux501_Vehicles_IFV_agtRaptor_TradeFederation: Aux501_Vehicles_IFV_agtRaptor
    {
        displayName = "AGT 'Raptor' - TF";
        textureList[] = {"TradeFederation",1};
    };
};

class cfgWeapons
{
    class RD501_eweb_blaster;

    class Aux501_eweb_blaster_raptor: RD501_eweb_blaster
    {
        class GunParticles
        {
            class effect1
            {
                positionName = "usti hlavne";
                directionName = "konec_hlavne_1";
                effectName = "";
            };
            class effect2: effect1
            {
                positionName = "usti hlavne";
                directionName = "konec_hlavne_2";
            };
        };
    };
};