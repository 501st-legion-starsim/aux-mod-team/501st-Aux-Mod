class CfgPatches
{
    class Aux501_Patch_Vehicles_Republic_AA_Missle_Launchers
    {
        addonRootClass = "Aux501_Patch_Vehicles";
        requiredAddons[] = 
        {
            "A3_Weapons_F",
            "A3_Static_F_AA_01",
            "ace_csw",
            "ace_interaction"
        };
        units[] = 
        {
            "Aux501_Vehicles_AA_RPS48_Missle_Launcher"
        };
        weapons[] = 
        {
            "Aux501_Weaps_AAP4"
        };
        magazines[] = 
        {
            "Aux501_Vehicle_Weapons_Mags_rps48_aa",
            "Aux501_Vehicle_Weapons_Mags_rps48_aa_LAAT"
        };
        ammo[] = 
        {
            "Aux501_Vehicle_Weapons_Ammo_rps48_aa"
        };
    };
};

class SensorTemplatePassiveRadar;
class SensorTemplateAntiRadiation;
class SensorTemplateActiveRadar;
class SensorTemplateIR;
class SensorTemplateVisual;
class SensorTemplateMan;
class SensorTemplateLaser;
class SensorTemplateNV;
class SensorTemplateDataLink;
class DefaultVehicleSystemsDisplayManagerLeft
{
    class components;
};
class DefaultVehicleSystemsDisplayManagerRight
{
    class components;
};
class VehicleSystemsTemplateLeftPilot: DefaultVehicleSystemsDisplayManagerLeft
{
    class components;
};
class VehicleSystemsTemplateRightPilot: DefaultVehicleSystemsDisplayManagerRight
{
    class components;
};

class CfgSoundShaders
{
    class Aux501_RPS48_shot_SoundShader
	{
		samples[] = 
        {
            {"\Aux501\Vehicles\AA\Republic\Missle_launchers\Sounds\RPS48_shot_01.wss",1},
            {"\Aux501\Vehicles\AA\Republic\Missle_launchers\Sounds\RPS48_shot_01.wss",1},
            {"\Aux501\Vehicles\AA\Republic\Missle_launchers\Sounds\RPS48_shot_01.wss",1},
            {"\Aux501\Vehicles\AA\Republic\Missle_launchers\Sounds\RPS48_shot_01.wss",1}
        };
		volume = 1.0;
		range = 1600;
	};
};

class CfgSoundSets
{
    class Aux501_RPS48_Shot_SoundSet
	{
		SoundShaders[] = {"Aux501_RPS48_shot_SoundShader"};
		volumeFactor = 1.6;
		volumeCurve = "InverseSquare2Curve";
		spatial = 1;
		doppler = 0;
		loop = 0;
		sound3DProcessingType = "ExplosionLight3DProcessingType";
		distanceFilter = "explosionDistanceFreqAttenuationFilter";
		occlusionFactor = 0.3;
		obstructionFactor = 0;
	};
};

class CfgVehicles
{
    class NonStrategic;
    class StaticShip;
    class Building;
    class House_F;
    class FloatingStructure_F;
    class thingx;
    class Ship;
    class LandVehicle;
    class StaticWeapon: LandVehicle
    {
        class Turrets;
        class HitPoints;
    };
    class StaticMGWeapon: StaticWeapon
    {
        class Turrets: Turrets
        {
            class MainTurret;
        };
        class Components;
    };
    class AAA_System_01_base_F: StaticMGWeapon
    {
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                class Components: Components
                {
                    class VehicleSystemsDisplayManagerComponentLeft: DefaultVehicleSystemsDisplayManagerLeft
                    {
                        class Components;
                    };
                    class VehicleSystemsDisplayManagerComponentRight: DefaultVehicleSystemsDisplayManagerRight
                    {
                        class Components;
                    };
                };
            };
        };
    };
    
    class Aux501_Vehicles_AA_RPS48_Missle_Launcher: AAA_System_01_base_F
    {
        scope = 2;
        scopeCurator = 2;
        editorPreview = "\OPTRE_Misc\Image\OPTRE\Turrets\OPTRE_Lance.jpg";
        displayName = "RPS-48 Missile Launcher";
        author = "501st Aux Mod";
        faction = "Aux501_FactionClasses_Republic";
        editorCategory = "Aux501_Editor_Category_Republic";
        editorSubcategory = "EdSubcat_AAs";
        vehicleClass = "Static";
        side = 1;
        picture = "\A3\Static_F_Jets\SAM_system_02\Data\UI\SAM_system_02_picture_CA.paa";
        icon = "\A3\Static_F_Jets\SAM_system_02\Data\UI\SAM_system_02_icon_CA.paa";
        isUav = 1;
        hasDriver = 0;
        getInRadius = 0;
        model = "OPTRE_Weapons_Turrets\Lance\Lance.p3d";
        hiddenSelections[] = {"camo_turret","camo_base","camo_decal","camo_decal2"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Vehicles\AA\Republic\Missle_launchers\textures\rps48_body_co.paa",
            "\Aux501\Vehicles\AA\Republic\Missle_launchers\textures\rps48_base_co.paa",
            "\Aux501\Vehicles\AA\Republic\Missle_launchers\textures\rps48_decals_ca.paa",
            "\Aux501\Vehicles\AA\Republic\Missle_launchers\textures\rps48_decals_ca.paa"
        };
        crew = "B_UAV_AI";
        uavCameraGunnerPos = "gunnerview";
        uavCameraGunnerDir = "usti hlavne";
        destrType = "DestructWreck";
        mapSize = 2.74;
        armor = 600;
        armorStructural = 2;
        damageResistance = 0.004;
        damageEffect = "AirDestructionEffects";
        extCameraPosition[] = {0,5,-5};
        canFloat = 0;
        class Components: Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class IRSensorComponent: SensorTemplateIR
                    {
                        class AirTarget
                        {
                            minRange = 0;
                            maxRange = 3000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = 1;
                        };
                        class GroundTarget
                        {
                            minRange = 0;
                            maxRange = 3000;
                            objectDistanceLimitCoef = 1;
                            viewDistanceLimitCoef = 1;
                        };
                        typeRecognitionDistance = 4000;
                        maxTrackableSpeed = 600;
                        angleRangeHorizontal = 60;
                        angleRangeVertical = 40;
                        animDirection = "MainGun";
                        aimDown = -0.5;
                    };
                    class ActiveRadarSensorComponent: SensorTemplateActiveRadar
                    {
                        class AirTarget
                        {
                            minRange = 0;
                            maxRange = 3000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget
                        {
                            minRange = 0;
                            maxRange = 3000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        typeRecognitionDistance = 3000;
                        angleRangeHorizontal = 360;
                        angleRangeVertical = 100;
                        aimDown = -45;
                        maxTrackableSpeed = 1388.89;
                    };
                    class DataLinkSensorComponent: SensorTemplateDataLink{};
                };
            };
        };
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                class Components: Components
                {
                    class VehicleSystemsDisplayManagerComponentLeft: DefaultVehicleSystemsDisplayManagerLeft
                    {
                        class Components
                        {
                            class EmptyDisplay
                            {
                                componentType = "EmptyDisplayComponent";
                            };
                            class MinimapDisplay
                            {
                                componentType = "MinimapDisplayComponent";
                                resource = "RscCustomInfoMiniMap";
                            };
                            class UAVDisplay
                            {
                                componentType = "UAVFeedDisplayComponent";
                            };
                            class SensorDisplay
                            {
                                componentType = "SensorsDisplayComponent";
                                range[] = {16000,8000,4000,2000};
                                resource = "RscCustomInfoSensors";
                            };
                        };
                    };
                    class VehicleSystemsDisplayManagerComponentRight: DefaultVehicleSystemsDisplayManagerRight
                    {
                        defaultDisplay = "SensorDisplay";
                        class Components
                        {
                            class EmptyDisplay
                            {
                                componentType = "EmptyDisplayComponent";
                            };
                            class MinimapDisplay
                            {
                                componentType = "MinimapDisplayComponent";
                                resource = "RscCustomInfoMiniMap";
                            };
                            class UAVDisplay
                            {
                                componentType = "UAVFeedDisplayComponent";
                            };
                            class SensorDisplay
                            {
                                componentType = "SensorsDisplayComponent";
                                range[] = {16000,8000,4000,2000};
                                resource = "RscCustomInfoSensors";
                            };
                        };
                    };
                };
                selectionFireAnim = "zasleh";
                body = "MainTurret";
                gun = "MainGun";
                animationsourcebody = "MainTurret";
                animationSourceGun = "MainGun";
                gunAxis = "Osa Hlavne";
                gunBeg = "konec hlavne";
                gunEnd = "usti hlavne";
                minelev = -20;
                maxelev = 55;
                minturn = -360;
                maxturn = 360;
                initElev = 0;
                initTurn = 0;
                turretAxis = "OsaVeze";
                maxHorizontalRotSpeed = 2.7;
                maxVerticalRotSpeed = 2.7;
                turretInfoType = "RscWeaponZeroing";
                soundServo[] = {"A3\Sounds_F\vehicles\armor\noises\servo_best",1.4125376,1,40};
                hasGunner = 1;
                gunnerName = "$STR_A3_Phalanx_operator_displayName";
                primary = 1;
                primaryGunner = 1;
                startEngine = 0;
                enableManualFire = 0;
                optics = 1;
                gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_AAA_01_w_F";
                class OpticsIn
                {
                    class Wide
                    {
                        opticsDisplayName = "W";
                        initAngleX = 0;
                        minAngleX = -30;
                        maxAngleX = 30;
                        initAngleY = 0;
                        minAngleY = -100;
                        maxAngleY = 100;
                        initFov = 0.466;
                        minFov = 0.466;
                        maxFov = 0.466;
                        visionMode[] = {"Normal","NVG","Ti"};
                        thermalMode[] = {0,1};
                        gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_AAA_01_w_F";
                    };
                    class Medium: Wide
                    {
                        opticsDisplayName = "M";
                        initFov = 0.093;
                        minFov = 0.093;
                        maxFov = 0.093;
                        gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_AAA_01_m_F";
                    };
                    class Narrow: Wide
                    {
                        opticsDisplayName = "N";
                        gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Gunner_AAA_01_n_F";
                        initFov = 0.029;
                        minFov = 0.029;
                        maxFov = 0.029;
                    };
                };
                forceHideGunner = 1;
                gunnerforceoptics = 1;
                gunnerOutForceOptics = 1;
                viewgunnerinExternal = 0;
                outGunnerMayFire = 1;
                inGunnerMayFire = 1;
                castGunnerShadow = 0;
                memoryPointGunnerOptics = "gunnerview";
                weapons[] = {"Aux501_Vehicle_Weapon_RPS48"};
                magazines[] = 
                {
                    "Aux501_Vehicle_Weapons_Mags_rps48_aa",
                    "Aux501_Vehicle_Weapons_Mags_rps48_aa",
                    "Aux501_Vehicle_Weapons_Mags_rps48_aa",
                    "Aux501_Vehicle_Weapons_Mags_rps48_aa",
                    "Aux501_Vehicle_Weapons_Mags_rps48_aa"
                };
                enableGPS = 1;
                reportRemoteTargets = 1;
                reportOwnPosition = 1;
                lockDetectionSystem = 0;
            };
        };
    };
};

class CfgWeapons
{
    class missiles_ASRAAM;

    class Aux501_Vehicle_Weapon_RPS48: missiles_ASRAAM
    {
        displayName = "RPS-48 AA Missile";
        descriptionShort = "RPS-48";
        magazines[] = 
        {
            "Aux501_Vehicle_Weapons_Mags_rps48_aa",
            "Aux501_Vehicle_Weapons_Mags_rps48_aa_1rnd"
        };
        class StandardSound
		{
			soundsetshot[] = {"Aux501_RPS48_Shot_SoundSet"};
		};
        weaponLockDelay = 1.5;
        cursor = "EmptyCursor";
        cursorAim = "";
        lockAcquire = 0;
        magazineReloadTime = 6;
        reloadTime = 0.5;
        reloadMagazineSound[] = {"A3\Sounds_F\arsenal\weapons_static\Mortar\reload_magazine_Mortar",1,1,20};
        reloadSound[] = {"A3\Sounds_F\arsenal\weapons_static\Mortar\reload_mortar",1,1,20};
        maxRangeProbab = 0.2;
        midRangeProbab = 0.9;
        minRangeProbab = 0.3;
        maxRange = 3000;
        midRange = 1000;
        minRange = 300;
        class GunParticles
        {
            class FirstEffect
            {
                effectName = "MLRSFired";
                positionName = "Usti Hlavne";
                directionName = "Konec Hlavne";
            };
        };
    };
};

class CfgMagazines
{
    class 4Rnd_GAA_missiles;

    class Aux501_Vehicle_Weapons_Mags_rps48_aa: 4Rnd_GAA_missiles 
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_battery_ca.paa";
        displayName = "[501st] RPS-48 Lance";
        displayNameShort = "48Rnd AA";
        count = 48;
        ammo = "Aux501_Vehicle_Weapons_Ammo_rps48_aa";
        weaponpoolavailable = 1;
        mass = 50;	
    };

    class Aux501_Vehicle_Weapons_Mags_rps48_aa_1rnd: Aux501_Vehicle_Weapons_Mags_rps48_aa
	{
		displayName = "RPS-48 Lance AA";
		displayNameShort = "RPS-48 AA";
		count = 1;
		hardpoints[]=
		{
			"Aux501_Vehicle_AA_RPS48_pylon"
		};
		pylonWeapon = "Aux501_Vehicle_Weapon_RPS48";
		
	};
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_rps6_aa;

    class Aux501_Vehicle_Weapons_Ammo_rps48_aa: Aux501_Weapons_Ammo_rps6_aa
    {
        soundFly[] = {"swlw_rework\sounds\launcher\plx_fly.wss",6,1.5,700};
        effectsMissile = "Aux501_particle_effect_RPS48_rocket_fly";
        cmImmunity = 1;
    };
};

class Aux501_particle_effect_RPS48_rocket_fly
{
    class Light
    {
        simulation="light";
        type="Aux501_light_RPS48_rocket_light";
        position[]={0,0,0};
        qualityLevel = -1;
    };
    class Smoke
    {
        simulation="particles";
        type="Aux501_cloudlet_RPS48_rocket_smoke";
        position[]={0,0,0};
        qualityLevel = -1;
    };
};

class CfgCloudlets
{
    class Missile3;

    class Aux501_cloudlet_RPS48_rocket_smoke: Missile3
    {
        color[]={{0,0,0,0.5},{120,120,120,1},{120,120,120,1}};
        size[]={0.3,0.5,0.5};
        lifeTime = 1;
        interval = 0.0005;
    };
};

class CfgLights
{
    class RocketLight;

    class Aux501_light_RPS48_rocket_light : RocketLight
    {
        color[]={0,123,255};
        intensity = 1e6;
        dayLight = 1;
        useFlare = 1;
        flareSize = 7;
        flareMaxDistance = 6000;
    };
};

class Extended_Init_EventHandlers
{
	class Aux501_Vehicles_AA_RPS48_Missle_Launcher
	{
        class ERD501_LR_Radar_On
        {
            init = "(_this#0) setVehicleRadar 1;";
        };
	};
};