class CfgPatches
{
    class Aux501_Patch_Vehicles_CE_Turrets_Boomer
    {
        addonRootClass = "Aux501_Patch_Vehicles";
        requiredAddons[] = 
        {
            "A3_Weapons_F",
            "A3_Static_F_HMG_02",
            "ace_csw",
            "ace_interaction"
        };
        units[] = 
        {
            "Aux501_Vehicles_CE_Turret_boomer_base",
            "Aux501_Vehicles_CE_Turret_boomer"
        };
        weapons[] = 
        {
            "Aux501_Weaps_EWHB12"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_EWHB12_MG",
            "Aux501_Weapons_Mags_EWHB12_GL"
        };
        ammo[] = {};
    };
};

class Mode_SemiAuto;
class Mode_Burst;

class CfgVehicles
{
    class StaticWeapon;
    
    class StaticMGWeapon: StaticWeapon
    {
        class Turrets;
        class ACE_Actions;
    };
    class HMG_02_base_F: StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions;
        };
        class ACE_CSW;
	};

    class Aux501_Vehicles_CE_Turret_Boomer_base: HMG_02_base_F
    {
        scope = 1;
        scopeCurator = 1;
        author = "501st Aux Team";
        displayName = "EWHB-12 'Boomer'";
        side = 1;
        editorPreview = "";
        icon = "\A3\Static_F_Gamma\data\UI\map_StaticTurret_AA_CA.paa";
        class Armory
        {
            description = "Mark I Medium Repeating Blaster Cannon";
        };
        editorSubcategory = "EdSubcat_Turrets";
        vehicleClass = "Static";
        model = "ls_vehicles_turrets\mrbc\ls_turret_mrbc.p3d";
        picture = "ls_vehicles_turrets\mrbc\data\ui\ls_MRBC_mk1_editor_CA.paa";
        UiPicture = "ls_vehicles_turrets\mrbc\data\ui\ls_MRBC_mk1_editor_CA.paa";
        cost = 150000;
        getInAction = "";
        getOutAction = "";
        armor = 30;
        explosionShielding = 10;
        class Damage
        {
            tex[] = {};
            mat[] = 
            {
                "A3\Static_F_Gamma\data\StaticTurret_01.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_01_damage.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_01_destruct.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_02.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_02_damage.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_02_destruct.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_03.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_03_damage.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_03_destruct.rvmat"
            };
        };
        minTotalDamageThreshold = 0.5;
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                selectionFireAnim = "zasleh";
                optics = 1;
                discreteDistance[] = {100,200,300,400,600,800,1000,1200,1500,1600,1700,1800,1900,2000};
                discreteDistanceInitIndex = 2;
                turetInfoType = "RscOptics_crows";
                gunnerOpticsModel = "\a3\weapons_f_gamma\reticle\HMG_01_Optics_Gunner_F";
                minElev = -15;
                maxElev = 25;
                minTurn = -180;
                maxTurn = 180;
                weapons[] = {"Aux501_Weaps_EWHB12"};
                magazines[] = 
                {
                    "Aux501_Weapons_Mags_EWHB12_MG",
                    "Aux501_Weapons_Mags_EWHB12_MG",
                    "Aux501_Weapons_Mags_EWHB12_MG",
                    "Aux501_Weapons_Mags_EWHB12_MG",
                    "Aux501_Weapons_Mags_EWHB12_MG"
                };
                gunnerAction = "ls_mrbc_Gunner";
                memoryPointGunnerOptics = "gunnerview";
                gunnerLeftHandAnimName = "gun";
                gunnerRightHandAnimName = "gun";
                gunnerLeftLegAnimName = "";
                gunnerRightLegAnimName = "";
                gunnerInAction = "";
                gunnerGetOutAction = "";
                GunnerName = "Gunner";
                gunnerForceOptics = 0;
                disableSountAttenuation = 1;
                ejectDeadGunner = 1;
                memoryPointsGetInGunner = "pos_gunner";
                memoryPointsGetInGunnerDir = "pos_gunner_dir";
                class ViewOptics
                {
                    initAngleX = 0;
                    minAngleX = -30;
                    maxAngleX = 30;
                    initAngleY = 0;
                    minAngleY = -100;
                    maxAngleY = 100;
                    initFov = 0.5;
                    minFov = 0.25;
                    maxFov = 1.25;
                    visionMode[] = {"Normal","NVG"};
                    thermalMode[] = {0,1};
                };
            };
        };
        class AnimationSources
        {
            class Revolving
            {
                source = "revolving";
                weapon = "Aux501_Weaps_EWHB12";
            };
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions: ACE_MainActions
            {
                position = "";
                selection = "ace interact";
            };
        };
        class ACE_CSW: ACE_CSW
        {
            enabled = 1;
            proxyWeapon = "Aux501_Weaps_EWHB12";
            magazineLocation = "_target selectionPosition 'konec hlavne'";
            disassembleWeapon = "Aux501_Weaps_EWHB12_carry";
            disassembleTurret = "";
            ammoLoadTime = 1;
            ammoUnloadTime = 1;
            desiredAmmo = 500;
        };
        soundGetOut[] = {"A3\sounds_f\dummysound",0.0009999999,1,5};
        soundGetIn[] = {"A3\sounds_f\dummysound",0.00031622773,1,5};
        class assembleInfo
		{
			assembleTo = "";
			base = "";
			displayName = "";
			dissasembleTo[] = {""};
			primary = 0;
		};
    };
    class Aux501_Vehicles_CE_Turret_Boomer: Aux501_Vehicles_CE_Turret_Boomer_base
    {
        scope = 2;
        scopeCurator = 2;
        faction = "Aux501_FactionClasses_Republic";
        editorCategory = "Aux501_Editor_Category_Republic";
        crew = "Aux501_Units_Republic_501st_Trooper_Unit";       
    };
};

class CfgWeapons
{
    class MGun;
    class LMG_RCWS: MGun
    {
        class manual: Mgun{};
        class close: manual{};
        class short: close{};
        class medium: close{};
        class far: close{};
    };
    
    class Aux501_Weaps_EWHB12: LMG_RCWS
    {
        author = "501st Aux Team";
        displayName = "EWHB-12 'Boomer' Cannon";
        magazineReloadTime = 2;
        maxZeroing = 2000;
        class GunParticles{};
        showAimCursorInternal = 0;
        magazines[]=
        {
            "Aux501_Weapons_Mags_EWHB12_MG",
            "Aux501_Weapons_Mags_EWHB12_GL"
        };
        modes[] = 
        {
            "manual",
            "Single",
            "close",
            "short",
            "medium",
            "far"
        };
        class Single: Mode_SemiAuto
        {
            reloadTime = 0.09;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin2[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin3[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class manual: MGun
        {
            displayName = "EWHB-12 Cannon";
            dispersion = 0.011;
            reloadTime = 0.05;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin2[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                begin3[] = {"\SWLW_merc_trando\smgs\acpr\sounds\acpr",1,1,1800};
                soundBegin[] = {"begin1",0.33,"begin2",0.33,"begin3",0.33};
            };
        };
        class close: manual
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 8;
            burstRangeMax = 16;
            aiRateOfFire = 0.5;
            aiRateOfFireDispersion = 1.5;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.7;
            midRange = 100;
            midRangeProbab = 0.7;
            maxRange = 200;
            maxRangeProbab = 0.2;
        };
        class short: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 6;
            burstRangeMax = 16;
            aiRateOfFire = 1;
            aiRateOfFireDispersion = 2;
            aiRateOfFireDistance = 150;
            minRange = 100;
            minRangeProbab = 0.7;
            midRange = 400;
            midRangeProbab = 0.75;
            maxRange = 800;
            maxRangeProbab = 0.2;
        };
        class medium: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 4;
            burstRangeMax = 12;
            aiRateOfFire = 2;
            aiRateOfFireDispersion = 2;
            aiRateOfFireDistance = 400;
            minRange = 400;
            minRangeProbab = 0.75;
            midRange = 800;
            midRangeProbab = 0.7;
            maxRange = 1500;
            maxRangeProbab = 0.1;
        };
        class far: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 3;
            burstRangeMax = 12;
            aiRateOfFire = 4;
            aiRateOfFireDispersion = 4;
            aiRateOfFireDistance = 800;
            minRange = 800;
            minRangeProbab = 0.6;
            midRange = 1500;
            midRangeProbab = 0.25;
            maxRange = 2000;
            maxRangeProbab = 0.05;
        };
    };
};

class ACE_CSW_Groups
{
    class Aux501_Weapons_Mags_EWHB12_MG_csw
    {
        Aux501_Weapons_Mags_EWHB12_MG = 1;
    };
    class Aux501_Weapons_Mags_EWHB12_GL_csw
    {
        Aux501_Weapons_Mags_EWHB12_GL = 1;
    };
};

class CfgMagazines
{
    class VehicleMagazine;
    class 40Rnd_20mm_G_belt;

    class Aux501_Weapons_Mags_EWHB12_MG: VehicleMagazine
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        displayName = "[501st] EWHB-12 EWEB Charge";
        displayNameShort = "500Rnd 30MW";
        descriptionShort = "Heavy Republic Railgun Cell";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_staticeweb_ca.paa";
        count = 500;
        tracersEvery = 1;
        initSpeed = 750;
        maxLeadSpeed = 36.1111;
        nameSound = "mgun";
        ammo = "Aux501_Weapons_Ammo_30mw";
        model = "SWLW_merc_trando\machineguns\ls150\ls150_mag.p3d";
        weaponpoolavailable = 1;
        mass = 50;
    };
    class Aux501_Weapons_Mags_EWHB12_MG_csw: Aux501_Weapons_Mags_EWHB12_MG
    {
        scope = 2;
        scopeArsenal = 2;
        type = 256;
        ACE_isBelt = 1;
    };
    class Aux501_Weapons_Mags_EWHB12_GL: 40Rnd_20mm_G_belt
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        displayName= "[501st] EWHB-12 'Boomer' belt";
        displayNameShort = "40Rnd HE";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_staticgl_ca.paa";
        model = "\A3\Structures_F_EPB\Items\Military\Ammobox_rounds_F.p3d";
        muzzleImpulseFactor[] = {0,0};
        count = 40;
        ammo = "Aux501_Weapons_Ammo_GL_HE";
        weaponpoolavailable = 1;
        mass = 50;	
    };
    class Aux501_Weapons_Mags_EWHB12_GL_csw: Aux501_Weapons_Mags_EWHB12_GL
    {
        scope = 2;
        scopeArsenal = 2;
        type = 256;
        ACE_isBelt = 1;
    };
};