class CfgPatches
{
    class Aux501_Patch_Vehicles_CE_Turrets_Driver_Patch
    {
        addonRootClass = "Aux501_Patch_Vehicles";
        requiredAddons[] = 
        {
            "A3_Weapons_F",
            "A3_Static_F_AA_01",
            "ace_csw",
            "ace_interaction"
        };
        units[] = 
        {
            "Aux501_Vehicles_CE_Turret_Striker"
        };
        weapons[] = 
        {
            "Aux501_Weaps_AAP4"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_AAP4",
            "Aux501_Weapons_Mags_AAP4_csw"
        };
        ammo[] = {};
    };
};

class CfgVehicles
{
    class LandVehicle;
    class StaticWeapon: LandVehicle
	{
		class Turrets;
    };
    
    class StaticMGWeapon: StaticWeapon
    {
        class Turrets: Turrets
		{
			class MainTurret;
		};
        class ACE_Actions;
    };
	class AA_01_base_F: StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret: MainTurret
            {
                class ViewOptics;
            };
		};
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions;
        };
        class ACE_CSW;
        class AnimationSources;
	};

    class Aux501_Vehicles_CE_Turret_Striker: AA_01_base_F
    {
        scope = 2;
        scopeCurator = 2;
        side = 1;
        author = "501st Aux Mod";
        displayName = "AAP4 'Striker'";
        faction = "Aux501_FactionClasses_Republic";
        editorCategory = "Aux501_Editor_Category_Republic";
        editorSubcategory = "EdSubcat_Turrets";
        vehicleClass = "Static";
        ace_cargo_size = 1;
        crew = "Aux501_Units_Republic_501st_Trooper_Unit";
        armor = 150;
        nameSound = "veh_static_AA_s";
        getInAction = "GetInlow";
        getOutAction = "GetOutlow";
        radarType = 2;
        model = "\OPTRE_Weapons_Turrets\LAU65D\LAU65D_pod.p3d";
        picture = "\OPTRE_Weapons_Turrets\LAU65D\data\UI\LAU65D_ca.paa";
        icon = "\OPTRE_Weapons_Turrets\LAU65D\data\UI\map_LAU65D_ca.paa";
        editorPreview = "OPTRE_Misc\Image\OPTRE\Turrets\OPTRE_LAU65D_pod.jpg";
        mapSize = 4;
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                ejectDeadGunner = 1;
                proxyType = "CPGunner";
                proxyIndex = 1;
                gunnerType = "Aux501_Units_Republic_501st_Trooper_Unit";
                animationSourceBody = "Turret_rot";
                animationSourceCamElev = "camElev";
                animationSourceGun = "camElev";
                memoryPointsGetInGunner = "pos_gunner";
                memoryPointsGetInGunnerDir = "pos_gunner_dir";
                body = "Turret_rot";
                stabilizedInAxes = "StabilizedInAxesNone";
                gunnerAction = "Gunner_OPTRE_Lau";
                weapons[] = {"Aux501_Weaps_AAP4"};
                magazines[] = 
                {
                    "Aux501_Weapons_Mags_AAP4",
                    "Aux501_Weapons_Mags_AAP4",
                    "Aux501_Weapons_Mags_AAP4",
                    "Aux501_Weapons_Mags_AAP4"
                };
                turretInfoType = "ACE_RscOptics_javelin";
                gunnerOpticsColor[] = {0,0,0,1};
                gunnerOpticsEffect[] = {};
                gunnerOpticsModel = "\z\ace\addons\javelin\data\reticle_titan.p3d";
                gunnerOpticsShowCursor = 0;
                gun = "";
                gunBeg = "usti hlavne";
                gunEnd = "konec hlavne";
                memoryPointGun = "usti hlavne";
                selectionFireAnim = "zasleh";
                memoryPointGunnerOptics = "eye";
                cameraDir = "look";
                gunnerForceOptics = 1;
                gunnerInAction = "ManActTestDriver";
                gunnergetInAction = "";
                gunnergetOutAction = "";
                initElev = 0;
                minElev = -15;
                maxElev = 40;
                initCamElev = 0;
                minCamElev = -15;
                maxCamElev = 40;
                initTurn = 0;
                minTurn = -360;
                maxTurn = 360;
                discreteDistance[] = {1};
                discreteDistanceCameraPoint[] = {"eye"};
                discreteDistanceInitIndex = 0;
                class ViewOptics: ViewOptics
                {
                    initAngleX = 0;
                    minAngleX = -45;
                    maxAngleX = 75;
                    initAngleY = 0;
                    minAngleY = -120;
                    maxAngleY = 120;
                    initFov = 0.4;
                    minFov = 0.04167;
                    maxFov = 0.4;
                    thermalMode[] = {0,1};
                    visionMode[] = {"Normal","NVG","Ti"};
                };
                class ViewGunner
                {
                    initAngleX = 0;
                    minAngleX = -45;
                    maxAngleX = 75;
                    initAngleY = 0;
                    minAngleY = -120;
                    maxAngleY = 120;
                    initFov = 0.4;
                    minFov = 0.4;
                    maxFov = 0.4;
                };
            };
        };
        class Damage
        {
            tex[] = {};
            mat[] = 
            {
                "OPTRE_Weapons_Turrets\LAU65D\data\LAU65D.rvmat",
                "OPTRE_Weapons_Turrets\LAU65D\data\LAU65D_damage.rvmat",
                "OPTRE_Weapons_Turrets\LAU65D\data\LAU65D_destruct.rvmat",
                "OPTRE_Weapons_Turrets\LAU65D\data\LAU65D_tripod.rvmat",
                "OPTRE_Weapons_Turrets\LAU65D\data\LAU65D_tripod_damage.rvmat",
                "OPTRE_Weapons_Turrets\LAU65D\data\LAU65D_tripod_destruct.rvmat"
            };
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions: ACE_MainActions
            {
                position = "";
                selection = "osaveze";
            };
        };
        class ACE_CSW: ACE_CSW
        {
            enabled = 1;
            proxyWeapon = "Aux501_Weaps_AAP4";
            magazineLocation = "_target selectionPosition 'konec rakety'";
            disassembleWeapon = "Aux501_Weaps_AAP4_carry";
            disassembleTurret = "";
            ammoLoadTime = 1;
            ammoUnloadTime = 1;
            desiredAmmo = 6;
        };
    };
};

class CfgWeapons
{
    class ace_javelin_Titan_Static;

    class Aux501_Weaps_AAP4: ace_javelin_Titan_Static
    {
        author = "501st Aux Team";
        displayName = "AAP4 'Striker' Launcher";
        magazineReloadTime = 2;
        ace_javelin_enabled = 1;
        weaponInfoType = "ACE_RscOptics_javelin";
        modelOptics = "\z\ace\addons\javelin\data\reticle_titan.p3d";
        canLock = 0;  
        lockingTargetSound[] = {"", 0, 1};
        lockedTargetSound[] = {"", 0, 1};
        magazines[]=
        {
            "Aux501_Weapons_Mags_AAP4"
        };
    };
};

class ACE_CSW_Groups
{
    class Aux501_Weapons_Mags_AAP4_csw
    {
        Aux501_Weapons_Mags_AAP4 = 1;
    };
};

class CfgMagazines
{
    class Titan_AT;

    class Aux501_Weapons_Mags_AAP4: Titan_AT
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_battery_ca.paa";
        displayName = "[501st] AAP4 'Striker' Pod";
        displayNameShort = "6Rnd 'Striker' Pod";
        count = 6;
        ammo = "ACE_Javelin_FGM148_Static";
        model = "\MRC\JLTS\weapons\PLX1\PLX1_mag.p3d";
        weaponpoolavailable = 1;
        mass = 50;	
    };
    class Aux501_Weapons_Mags_AAP4_csw: Aux501_Weapons_Mags_AAP4
    {
        scope = 2;
        scopeArsenal = 2;
        type = 256;
        ACE_isBelt = 1;
    };
};