class CfgPatches
{
    class Aux501_Patch_Vehicles_CE_Turrets_Driver
    {
        addonRootClass = "Aux501_Patch_Vehicles";
        requiredAddons[] = 
        {
            "A3_Weapons_F",
            "A3_Static_F_HMG_02",
            "ace_csw",
            "ace_interaction"
        };
        units[] = 
        {
            "Aux501_Vehicles_CE_Turret_Driver"
        };
        weapons[] = 
        {
            "Aux501_Weaps_MAR1"
        };
        magazines[] = 
        {
            "Aux501_Weapons_Mags_mar1",
            "Aux501_Weapons_Mags_mar1_csw"
        };
        ammo[] = 
        {
            "Aux501_Weapons_Ammo_mar1"
        };
    };
};

class CfgVehicles
{
    class StaticWeapon;
    
    class StaticMGWeapon: StaticWeapon
    {
        class Turrets;
        class ACE_Actions;
    };
    class HMG_02_base_F: StaticMGWeapon
	{
		class Turrets: Turrets
		{
			class MainTurret;
		};
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions;
        };
        class ACE_CSW;
	};

    class Aux501_Vehicles_CE_Turret_Driver: HMG_02_base_F
    {
        scope = 2;
        scopeCurator = 2;
        author = "501st Aux Mod";
        displayName = "MAR1 'Driver'";
        model = "\OPTRE_Weapons_Turrets\static\StaticTurret\Static_Gauss.p3d";
        picture = "\OPTRE_Weapons_Turrets\LAU65D\data\UI\LAU65D_ca.paa";
        icon = "\OPTRE_Weapons_Turrets\LAU65D\data\UI\map_LAU65D_ca.paa";
        armor = 150;
        side = 1;
        ace_cargo_size = 1;
        faction = "Aux501_FactionClasses_Republic";
        editorCategory = "Aux501_Editor_Category_Republic";
        editorSubcategory = "EdSubcat_Turrets";
        vehicleClass = "Static";
        mapSize = 4;
        nameSound = "veh_static_MG_s";
        radarType = 2;
        crew = "Aux501_Units_Republic_501st_Trooper_Unit";
        hiddenSelections[] = {"Camo3","Camo4","camoBase"};
        hiddenSelectionsTextures[] = 
        {
            "\OPTRE_Vehicles\Warthog\data\night\m68_turret_night_co.paa",
            "\OPTRE_Vehicles\Warthog\data\night\m12_turret_night_co.paa",
            "\OPTRE_Weapons_Turrets\static\staticturret\data\staticturretbase_night_co.paa"
        };
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                selectionFireAnim = "zasleh";
                body = "mainturret";
                gun = "maingun";
                animationsourcebody = "mainturret";
                animationSourceGun = "maingun";
                gunAxis = "Osa Hlavne";
                gunBeg = "Usti hlavne";
                gunEnd = "konec hlavne";
                minElev = -15;
                maxElev = 45;
                minTurn = -360;
                maxTurn = 360;
                initTurn = 0;
                turretAxis = "OsaVeze";
                maxHorizontalRotSpeed = 1.75;
                maxVerticalRotSpeed = 1.5;
                gunnerAction = "OPTRE_Gunner_Gaus_Warthog";
                gunnerInAction = "OPTRE_Gunner_Gaus_Warthog";
                gunnerGetInAction = "GetInMRAP_01";
                gunnerGetOutAction = "GetOutMRAP_01";
                gunnerName = "Gunner";
                ejectDeadGunner = 1;
                hideWeaponsGunner = 0;
                stabilizedInAxes = 3;
                soundServo[] = {"",0.01,1};
                outGunnerMayFire = 1;
                inGunnerMayFire = 1;
                commanding = 1;
                primaryGunner = 1;
                memoryPointsGetInGunner = "Pos Gunner";
                memoryPointsGetInGunnerDir = "Pos Gunner dir";
                gunnerLeftHandAnimName = "OtocHlaven";
                gunnerRightHandAnimName = "OtocHlaven";
                memoryPointGun = "usti hlavne";
                weapons[] = {"Aux501_Weaps_MAR1"};
                magazines[] = 
                {
                    "Aux501_Weapons_Mags_mar1",
                    "Aux501_Weapons_Mags_mar1",
                    "Aux501_Weapons_Mags_mar1",
                    "Aux501_Weapons_Mags_mar1",
                    "Aux501_Weapons_Mags_mar1"
                };
                memoryPointGunnerOptics = "gunnerview";
                memoryPointGunneroutOptics = "gunneroutview";
                gunnerOpticsModel = "\A3\Weapons_F\Reticle\Optics_Gunner_MBT_01_w_F.p3d";
                gunnerOpticsShowCursor = 1;
                turretInfoType = "RscWeaponZeroing";
                castGunnerShadow = 1;
                startEngine = 0;
                enableManualFire = 0;
                gunnerForceOptics = 1;
                class Viewoptics
                {
                    initAngleX = 0;
                    initAngleY = 0;
                    initFov = 0.75;
                    maxAngleX = 30;
                    maxAngleY = 100;
                    maxFov = 1.1;
                    maxMoveX = 0;
                    maxMoveY = 0;
                    maxMoveZ = 0;
                    minAngleX = -30;
                    minAngleY = -100;
                    minFov = 0.0125;
                    minMoveX = 0;
                    minMoveY = 0;
                    minMoveZ = 0;
                    opticsZoomInit = 0.75;
                    opticsZoomMax = 0.75;
                    opticsZoomMin = 0.25;
                    thermalMode[] = {5,6};
                    visionMode[] = {"Normal","NVG","Ti"};
                };
                class ViewGunner
                {
                    initAngleX = 0;
                    minAngleX = -45;
                    maxAngleX = 75;
                    initAngleY = 0;
                    minAngleY = -120;
                    maxAngleY = 120;
                    initFov = 0.4;
                    minFov = 0.4;
                    maxFov = 0.4;
                };
            };
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions: ACE_MainActions
            {
                position = "";
                selection = "osaveze";
            };
        };
        class ACE_CSW: ACE_CSW
        {
            enabled = 1;
            proxyWeapon = "Aux501_Weaps_MAR1";
            magazineLocation = "_target selectionPosition 'drum_axis'";
            disassembleWeapon = "Aux501_Weaps_MAR1_carry";
            disassembleTurret = "";
            ammoLoadTime = 1;
            ammoUnloadTime = 1;
            desiredAmmo = 10;
        };
        class AnimationSources
        {
            class Gatling
            {
                source = "revolving";
                weapon = "Aux501_Weaps_MAR1";
            };
            class Gatling_flash
            {
                source = "reload";
                weapon = "Aux501_Weaps_MAR1";
            };
        };
    };
};

class CfgWeapons
{
    class MGun;
    class LMG_RCWS;
    
    class Aux501_Weaps_MAR1: LMG_RCWS
    {
        author = "501st Aux Team";
        displayName = "MAR1 'Driver' Railgun";
        magazineReloadTime = 1;
        maxZeroing = 2000;
        class GunParticles{};
        showAimCursorInternal = 0;
        magazines[]=
        {
            "Aux501_Weapons_Mags_mar1"
        };
        class manual: MGun
        {
            reloadTime = 2;
            dispersion = 0.00001;
            sounds[] = {"StandardSound"};
            class BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\MAR1\data\sounds\MAR1_shot.wss",+5db,1,2200};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
            class StandardSound: BaseSoundModeType
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Weapons\Republic\MAR1\data\sounds\MAR1_shot.wss",+5db,1,2200};
                soundBegin[] = {"begin1",1};
                closure1[] = {};
                closure2[] = {};
                soundClosure[] = {};
            };
        };
        class close: manual
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 8;
            burstRangeMax = 16;
            aiRateOfFire = 0.5;
            aiRateOfFireDispersion = 1.5;
            aiRateOfFireDistance = 50;
            minRange = 0;
            minRangeProbab = 0.7;
            midRange = 100;
            midRangeProbab = 0.7;
            maxRange = 200;
            maxRangeProbab = 0.2;
        };
        class short: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 6;
            burstRangeMax = 16;
            aiRateOfFire = 1;
            aiRateOfFireDispersion = 2;
            aiRateOfFireDistance = 150;
            minRange = 100;
            minRangeProbab = 0.7;
            midRange = 400;
            midRangeProbab = 0.75;
            maxRange = 800;
            maxRangeProbab = 0.2;
        };
        class medium: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 4;
            burstRangeMax = 12;
            aiRateOfFire = 2;
            aiRateOfFireDispersion = 2;
            aiRateOfFireDistance = 400;
            minRange = 400;
            minRangeProbab = 0.75;
            midRange = 800;
            midRangeProbab = 0.7;
            maxRange = 1500;
            maxRangeProbab = 0.1;
        };
        class far: close
        {
            aiBurstTerminable = 1;
            showToPlayer = 0;
            burst = 3;
            burstRangeMax = 12;
            aiRateOfFire = 4;
            aiRateOfFireDispersion = 4;
            aiRateOfFireDistance = 800;
            minRange = 800;
            minRangeProbab = 0.6;
            midRange = 1500;
            midRangeProbab = 0.25;
            maxRange = 2000;
            maxRangeProbab = 0.05;
        };
    };    
};

class ACE_CSW_Groups
{
    class Aux501_Weapons_Mags_mar1_csw
    {
        Aux501_Weapons_Mags_mar1 = 1;
    };
};

class CfgMagazines
{
    class VehicleMagazine;

    class Aux501_Weapons_Mags_mar1: VehicleMagazine
    {
        scope = 1;
        scopeArsenal = 1;
        author = "501st Aux Team";
        displayName = "[501st] 10rnd MAR1 'Driver' Slugs";
        displayNameShort = "10Rnd Slugs";
        descriptionShort = "Heavy Republic Railgun Cell";
        picture = "\Aux501\Weapons\Magazines\data\Aux501_icon_mag_staticrailgun_ca.paa";
        count = 10;
        tracersEvery = 1;
        initSpeed = 1500;
        maxLeadSpeed = 36.1111;
        nameSound = "mgun";
        ammo = "Aux501_Weapons_Ammo_mar1";
        model = "SWLW_clones\launchers\rps6\RPS6_mag.p3d";
        weaponpoolavailable = 1;
        mass = 95;
    };
    class Aux501_Weapons_Mags_mar1_csw: Aux501_Weapons_Mags_mar1
    {
        scope = 2;
        scopeArsenal = 2;
        type = 256;
        ACE_isBelt = 1;
    };
};

class CfgAmmo
{
    class Aux501_Weapons_Ammo_base_blue;

    class Aux501_Weapons_Ammo_mar1: Aux501_Weapons_Ammo_base_blue
    {
        effectFly = "Aux501_particle_effect_heavybolt_fly_blue";
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Blue.p3d";
        hit = 1000;
        thrust = 500;
        typicalSpeed = 3000;
        caliber = 50;
    };
};