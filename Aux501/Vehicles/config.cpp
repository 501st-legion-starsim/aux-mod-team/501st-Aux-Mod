class cfgPatches
{
    class Aux501_Patch_Vehicles
    {
        Name = "501st Aux Mod - Vehicles";
        Author = "501st Aux Team";
        url = "https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod";
        requiredAddons[] = 
        {
            "A3_Weapons_F",
            "A3_Armor_F_Beta"
        };
        units[] = {};
        weapons[] = {};
    };
};

class CfgEditorSubcategories
{
    //Ground Vehicles
    class Aux501_Editor_Subcategory_IFVs
    {
        displayName = "IFVs";
    };
    class Aux501_Editor_Subcategory_Artillery
    {
        displayName = "Artillery";
    };
    class Aux501_Editor_Subcategory_Transports
    {
        displayName = "Transports";
    };
    class Aux501_Editor_Subcategory_Speeders
    {
        displayName = "Speeders";
    };
    class Aux501_Editor_Subcategory_Special
    {
        displayName = "Special";
    };

    //Air Vehicles
    class Aux501_Editor_Subcategory_Starfighters
    {
        displayName = "Starfighters";
    };

    class Aux501_Editor_Subcategory_Gunships
    {
        displayName = "Gunships";
    };

    //Hybrids
    class Aux501_Editor_Subcategory_Heavy_Infantry
    {
        displayName = "Heavy Infantry";
    };
};