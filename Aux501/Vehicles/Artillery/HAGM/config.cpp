class CfgPatches
{
    class Aux501_Patch_Vehicles_Artillery_HAGM
    {
        addonRootClass = "Aux501_Patch_Vehicles";
        requiredAddons[] = 
        {
            "A3_Weapons_F",
            "A3_Armor_F_Beta"
        };
        units[] = 
        {
            "Aux501_Vehicles_Artillery_HAGM_base",
            "Aux501_Vehicles_Artillery_HAGM"
        };
        weapons[] = 
        {
            "Aux501_Vehicles_Weapons_CIS_155mm_howitzer"
        };
        magazines[] = 
        {
            "Aux501_Vehicle_Mags_CIS_32Rnd_155mm_he_mag"
        };
        ammo[] = 
        {
            "Aux501_Vehicles_155mw_HE_ammo"
        };
    };
};

class RCWSOptics;
class Optics_Armored;

class Mode_SemiAuto;
class Mode_Burst;

class Optics_Gunner_MBT_01: Optics_Armored
{
    class Wide;
};

class cfgvehicles
{
    class LandVehicle;

    class Tank: LandVehicle
    {
        class NewTurret;
        class HitPoints;
    };
    class Tank_F: Tank
    {
        class Turrets
        {
            class MainTurret: NewTurret
            {
                class Turrets;
            };
        };
        class ViewOptics;
        class HitPoints: HitPoints
        {
            class HitHull;
            class HitFuel;
            class HitEngine;
            class HitLTrack;
            class HitRTrack;
        };
        class Sounds;
    };

    class Aux501_Vehicles_Artillery_HAGM_base: Tank_F
    {
        Author = "501st Aux Team";
        scope = 1;
        scopecurator = 1;
        side = 0;
        displayName = "HAG-M";
        icon = "\ls_vehicles_ground\_ui\icons\aat_icon.paa";
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_Artillery";
        model = "\kobra\442_g_vehicle\baw_arty\baw_arty_2.p3d";
        hiddenselections[] = {"body","eyes","gun"};
        hiddenSelectionsTextures[] = 
        {
            "ls_vehicles_ground\bawhag\data\body_co.paa",
            "ls_vehicles_ground\bawhag\data\eyes_co.paa",
            "ls_vehicles_ground\bawhag\data\gun_co.paa"
        };
        artilleryScanner = 1;
        availableForSupportTypes[] = {"Artillery"};
        insideSoundCoef = 0.9;
        threat[] = {0.8,1,0.3};
        canFloat = 1;
        waterLeakiness = 2.5;
        waterAngularDampingCoef = 4;
        waterLinearDampingCoefY = 4;
        waterLinearDampingCoefX = 4;
        waterLinearDampingCoefZ = 4;
        engineShiftY = 1.2;
        rudderForceCoef = 100;
        forceHideDriver = 1;
        rudderForceCoefAtMaxSpeed = 100;
        waterPPInVehicle = 0;
        waterResistanceCoef = 0;
        waterSpeedFactor = 3000;
        maxFordingDepth = 0.5;
        waterResistance = 1;
        turnCoef = 0.5;
        engineEffectSpeed = 5;
        simulation = "tankX";
        startEngine = 0;
        enginePower = 3000;
        maxOmega = 276;
        peakTorque = 4832;
        torqueCurve[] = {{0,0},{"(1600/2640)","(2650/2850)"},{"(1800/2640)","(2800/2850)"},{"(1900/2640)","(2850/2850)"},{"(2000/2640)","(2800/2850)"},{"(2200/2640)","(2750/2850)"},{"(2400/2640)","(2600/2850)"},{"(2640/2640)","(2350/2850)"}};
        thrustDelay = 0.1;
        clutchStrength = 180;
        fuelCapacity = 1885;
        brakeIdleSpeed = 1.78;
        latency = 0.1;
        tankTurnForce = 600000;
        idleRpm = 700;
        redRpm = 2640;
        engineLosses = 25;
        transmissionLosses = 15;
        class complexGearbox
        {
            GearboxRatios[] = {"R2",-3.9,"N",0,"D1",4.7,"D2",3.5,"D3",2.6,"D4",2,"D5",1.5,"D6",1.125,"D7",0.85};
            TransmissionRatios[] = {"High",15};
            AmphibiousRatios[] = {"R1",-30,"N",0,"D1",50};
            gearBoxMode = "auto";
            moveOffGear = 1;
            driveString = "D";
            neutralString = "N";
            reverseString = "R";
            transmissionDelay = 0.1;
        };
        class Wheels
        {
            class L2
            {
                boneName = "wheel_podkoloL1";
                center = "wheel_1_2_axis";
                boundary = "wheel_1_2_bound";
                damping = 75;
                steering = 0;
                side = "left";
                weight = 150;
                mass = 150;
                MOI = 25;
                latStiffX = 25;
                latStiffY = 280;
                longitudinalStiffnessPerUnitGravity = 100000;
                maxBrakeTorque = 20000;
                sprungMass = 4000;
                springStrength = 324000;
                springDamperRate = 36000;
                dampingRate = 1;
                dampingRateInAir = 8830;
                dampingRateDamaged = 10;
                dampingRateDestroyed = 10000;
                maxDroop = 0.15;
                maxCompression = 0.15;
            };
            class L3: L2
            {
                boneName = "wheel_podkolol2";
                center = "wheel_1_3_axis";
                boundary = "wheel_1_3_bound";
            };
            class L4: L2
            {
                boneName = "wheel_podkolol3";
                center = "wheel_1_4_axis";
                boundary = "wheel_1_4_bound";
            };
            class L5: L2
            {
                boneName = "wheel_podkolol4";
                center = "wheel_1_5_axis";
                boundary = "wheel_1_5_bound";
            };
            class L6: L2
            {
                boneName = "wheel_podkolol5";
                center = "wheel_1_6_axis";
                boundary = "wheel_1_6_bound";
            };
            class L7: L2
            {
                boneName = "wheel_podkolol6";
                center = "wheel_1_7_axis";
                boundary = "wheel_1_7_bound";
            };
            class R2: L2
            {
                boneName = "wheel_podkolop1";
                center = "wheel_2_2_axis";
                boundary = "wheel_2_2_bound";
                side = "right";
            };
            class R3: R2
            {
                boneName = "wheel_podkolop2";
                center = "wheel_2_3_axis";
                boundary = "wheel_2_3_bound";
            };
            class R4: R2
            {
                boneName = "wheel_podkolop3";
                center = "wheel_2_4_axis";
                boundary = "wheel_2_4_bound";
            };
            class R5: R2
            {
                boneName = "wheel_podkolop4";
                center = "wheel_2_5_axis";
                boundary = "wheel_2_5_bound";
            };
            class R6: R2
            {
                boneName = "wheel_podkolop5";
                center = "wheel_2_6_axis";
                boundary = "wheel_2_6_bound";
            };
            class R7: R2
            {
                boneName = "wheel_podkolop6";
                center = "wheel_2_7_axis";
                boundary = "wheel_2_7_bound";
            };
        };
        cost = 1500000;
        damageResistance = 0.02;
        crewVulnerable = 0;
        armor = 200;
        armorStructural = 3;
        class Hitpoints: Hitpoints
        {
            class HitHull: HitHull
            {
                armor = 0.75;
                material = -1;
                name = "hull_hit";
                visual = "zbytek";
                passthrough = 0.03;
                minimalhit = 0.14;
                explosionshielding = 2;
                radius = 0.25;
            };
            class HitEngine: HitEngine
            {
                armor = 0.75;
                material = -1;
                name = "engine_hit";
                passThrough = 0.08;
                minimalHit = 0.24;
                explosionShielding = 1;
                radius = 0.33;
            };
            class hitammo_l: HitEngine
            {
                name = "ammo_l_hit";
            };
            class hitammo_r: hitammo_l
            {
                name = "ammo_r_hit";
            };
        };
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                class Turrets{};
                gunBeg = "Usti hlavne3";
                gunEnd = "Konec hlavne3";
                memoryPointGunnerOptics = "gunner_view";
                memoryPointGun = "gunner_view_dir";
                discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500};
                discreteDistanceInitIndex = 2;
                proxyIndex = 2;
                weapons[] = {"Aux501_Vehicles_Weapons_CIS_155mm_howitzer"};
                magazines[] = 
                {
                    "Aux501_Vehicle_Mags_CIS_32Rnd_155mm_he_mag",
                    "Aux501_Vehicle_Mags_CIS_32Rnd_155mm_he_mag",
                    "Aux501_Vehicle_Mags_CIS_32Rnd_155mm_he_mag",
                    "Aux501_Vehicle_Mags_CIS_32Rnd_155mm_he_mag",
                    "Aux501_Vehicle_Mags_CIS_32Rnd_155mm_he_mag",
                    "Aux501_Vehicle_Mags_CIS_32Rnd_155mm_he_mag"
                };
                minElev = 0;
                maxElev = 90;
                initElev = 0;
                minTurn = -90;
                maxTurn = 90;
                soundServo[] = {"A3\Sounds_F\vehicles\armor\noises\servo_best",0.01,1,50};
                turretInfoType = "RscWeaponRangeArtilleryAuto";
                gunnerOutOpticsModel = "";
                gunnerOutOpticsEffect[] = {};
                gunnerOpticsEffect[] = {};
                gunnerForceOptics = 1;
                class OpticsIn: Optics_Gunner_MBT_01
                {
                    class Wide: Wide
                    {
                        gunnerOpticsModel = "\A3\Weapons_F\acc\reticle_mortar_01_f.p3d";
                        initFov = 0.174;
                        minFov = 0.0077778;
                        maxFov = 0.14;
                        visionMode[] = {"Normal","NVG","TI"};
                    };
                };
                gunnerAction = "Disabled";
                gunnerInAction = "Disabled";
                forceHideGunner = 1;
                inGunnerMayFire = 1;
                viewGunnerInExternal = 1;
                elevationMode = 3;
                class HitPoints
                {
                    class HitTurret
                    {
                        armor = 0.8;
                        material = -1;
                        name = "main_turret_hit";
                        visual = "vez";
                        passThrough = 0;
                        minimalHit = 0.02;
                        explosionShielding = 0.3;
                        radius = 0.25;
                    };
                    class HitGun
                    {
                        armor = 0.3;
                        material = -1;
                        name = "main_gun_hit";
                        visual = "";
                        passThrough = 0;
                        minimalHit = 0;
                        explosionShielding = 1;
                        radius = 0.25;
                    };
                };
            };
        };
        class RenderTargets{};
        class ViewOptics: ViewOptics
        {
            visionMode[] = {"Normal","NVG","TI"};
            initFov = 0.85;
            minFov = 0.85;
            maxFov = 0.85;
        };
        soundGetIn[] = {"A3\Sounds_F_EPB\Tracked\noises\get_in_out",0.562341,1};
        soundGetOut[] = {"A3\Sounds_F_EPB\Tracked\noises\get_in_out",0.562341,1,20};
        soundTurnIn[] = {"A3\Sounds_F\vehicles\noises\Turn_in_out",1.77828,1,20};
        soundTurnOut[] = {"A3\Sounds_F\vehicles\noises\Turn_in_out",1.77828,1,20};
        soundTurnInInternal[] = {"A3\Sounds_F\vehicles\noises\Turn_in_out",1.77828,1,20};
        soundTurnOutInternal[] = {"A3\Sounds_F\vehicles\noises\Turn_in_out",1.77828,1,20};
        soundDammage[] = {"",0.562341,1};
        soundEngineOnInt[] = {"kobra\442_g_vehicle\aat\sounds\aat_start_int.wss",0.794328,1};
        soundEngineOffInt[] = {"kobra\442_g_vehicle\aat\sounds\aat_stop_int.wss",0.794328,1};
        soundEngineOnExt[] = {"kobra\442_g_vehicle\aat\sounds\aat_start.wss",1.4,1,100};
        soundEngineOffExt[] = {"kobra\442_g_vehicle\aat\sounds\aat_stop.wss",1.4,1,100};
        BushCrash1[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_Collision_Light_Bush_01",0.630957,1,100};
        BushCrash2[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_Collision_Light_Bush_02",0.630957,1,100};
        BushCrash3[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_Collision_Light_Bush_03",0.630957,1,100};
        soundBushCrash[] = {"BushCrash1",0.33,"BushCrash2",0.33,"BushCrash3",0.33};
        buildCrash0[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_01",3.16228,1,200};
        buildCrash1[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_02",3.16228,1,200};
        buildCrash2[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_03",3.16228,1,200};
        buildCrash3[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_04",3.16228,1,200};
        buildCrash4[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_05",3.16228,1,200};
        buildCrash5[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_06",3.16228,1,200};
        soundBuildingCrash[] = {"buildCrash0",0.166,"buildCrash1",0.166,"buildCrash2",0.166,"buildCrash3",0.166,"buildCrash4",0.166,"buildCrash5",0.166};
        woodCrash0[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_01",3.16228,1,200};
        woodCrash1[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_02",3.16228,1,200};
        woodCrash2[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_03",3.16228,1,200};
        woodCrash3[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_04",3.16228,1,200};
        woodCrash4[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_05",3.16228,1,200};
        woodCrash5[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_06",3.16228,1,200};
        soundWoodCrash[] = {"woodCrash0",0.166,"woodCrash1",0.166,"woodCrash2",0.166,"woodCrash3",0.166,"woodCrash4",0.166,"woodCrash5",0.166};
        ArmorCrash0[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_01",3.16228,1,200};
        ArmorCrash1[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_02",3.16228,1,200};
        ArmorCrash2[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_03",3.16228,1,200};
        ArmorCrash3[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_04",3.16228,1,200};
        ArmorCrash4[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_05",3.16228,1,200};
        ArmorCrash5[] = {"A3\Sounds_F\vehicles2\armor\shared\collisions\Vehicle_Armor_General_Collision_06",3.16228,1,200};
        soundArmorCrash[] = {"ArmorCrash0",0.166,"ArmorCrash1",0.166,"ArmorCrash2",0.166,"ArmorCrash3",0.166,"ArmorCrash4",0.166,"ArmorCrash5",0.166};
        class Sounds: Sounds
        {
            class Idle_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",2,1,200};
                frequency = "0.95 + ((rpm/ 2640) factor[(400/ 2640),(900/ 2640)])*0.15";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(100/ 2640),(200/ 2640)]) * ((rpm/ 2640) factor[(900/ 2640),(700/ 2640)]))";
            };
            class Engine
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",2.5,1,240};
                frequency = "0.8 + ((rpm/ 2640) factor[(700/ 2640),(1100/ 2640)])*0.2";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(705/ 2640),(850/ 2640)]) * ((rpm/ 2640) factor[(1100 / 2640),(950/ 2640)]))";
            };
            class Engine1_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",3,1,280};
                frequency = "0.8 + ((rpm/ 2640) factor[(950/ 2640),(1400/ 2640)])*0.2";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(900/ 2640),(1050/ 2640)]) * ((rpm/ 2640) factor[(1400/ 2640),(1200/ 2640)]))";
            };
            class Engine2_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",3.5,1,320};
                frequency = "0.8 + ((rpm/ 2640) factor[(1200/ 2640),(1700/ 2640)])*0.2";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(1170/ 2640),(1380/ 2640)]) * ((rpm/ 2640) factor[(1700/ 2640),(1500/ 2640)]))";
            };
            class Engine3_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",4,1,360};
                frequency = "0.8 + ((rpm/ 2640) factor[(1500/ 2640),(2100/ 2640)])*0.1";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(1500/ 2640),(1670/ 2640)]) * ((rpm/ 2640) factor[(2100/ 2640),(1800/ 2640)]))";
            };
            class Engine4_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",4.5,1,400};
                frequency = "0.8 + ((rpm/ 2640) factor[(1800/ 2640),(2300/ 2640)])*0.1";
                volume = "engineOn*camPos*(((rpm/ 2640) factor[(1780/ 2640),(2060/ 2640)]) * ((rpm/ 2640) factor[(2450/ 2640),(2200/ 2640)]))";
            };
            class Engine5_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",5,1,440};
                frequency = "0.8 + ((rpm/ 2640) factor[(2100/ 2640),(2640/ 2640)])*0.1";
                volume = "engineOn*camPos*((rpm/ 2640) factor[(2150/ 2640),(2500/ 2640)])";
            };
            class IdleThrust
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",4.65016,1,200};
                frequency = "0.8 + ((rpm/ 2640) factor[(400/ 2640),(900/ 2640)])*0.15";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(100/ 2640),(200/ 2640)]) * ((rpm/ 2640) factor[(900/ 2640),(700/ 2640)]))";
            };
            class EngineThrust
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",5,1,200};
                frequency = "0.8 + ((rpm/ 2640) factor[(700/ 2640),(1100/ 2640)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(705/ 2640),(850/ 2640)]) * ((rpm/ 2640) factor[(1100 / 2640),(950/ 2640)]))";
            };
            class Engine1_Thrust_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",2,1,230};
                frequency = "0.8 + ((rpm/ 2640) factor[(950/ 2640),(1400/ 2640)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(900/ 2640),(1050/ 2640)]) * ((rpm/ 2640) factor[(1400/ 2640),(1200/ 2640)]))";
            };
            class Engine2_Thrust_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",2.5,1,290};
                frequency = "0.8 + ((rpm/ 2640) factor[(1200/ 2640),(1700/ 2640)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1170/ 2640),(1380/ 2640)]) * ((rpm/ 2640) factor[(1700/ 2640),(1500/ 2640)]))";
            };
            class Engine3_Thrust_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",3,1,350};
                frequency = "0.8 + ((rpm/ 2640) factor[(1500/ 2640),(2100/ 2640)])*0.1";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1500/ 2640),(1670/ 2640)]) * ((rpm/ 2640) factor[(2100/ 2640),(1800/ 2640)]))";
            };
            class Engine4_Thrust_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",3.5,1,400};
                frequency = "0.8 + ((rpm/ 2640) factor[(1800/ 2640),(2300/ 2640)])*0.1";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1780/ 2640),(2060/ 2640)]) * ((rpm/ 2640) factor[(2450/ 2640),(2200/ 2640)]))";
            };
            class Engine5_Thrust_ext
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",4,1,450};
                frequency = "0.8 + ((rpm/ 2640) factor[(2100/ 2640),(2640/ 2640)])*0.1";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/ 2640) factor[(2150/ 2640),(2500/ 2640)])";
            };
            class Idle_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine.wss",2,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(400/ 2640),(900/ 2640)])*0.15";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(100/ 2640),(200/ 2640)]) * ((rpm/ 2640) factor[(900/ 2640),(700/ 2640)]))";
            };
            class Engine_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",1.41589,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(700/ 2640),(1100/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(705/ 2640),(850/ 2640)]) * ((rpm/ 2640) factor[(1100 / 2640),(950/ 2640)]))";
            };
            class Engine1_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",1.58866,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(950/ 2640),(1400/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(900/ 2640),(1050/ 2640)]) * ((rpm/ 2640) factor[(1400/ 2640),(1200/ 2640)]))";
            };
            class Engine2_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",1.7825,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1200/ 2640),(1700/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(1170/ 2640),(1380/ 2640)]) * ((rpm/ 2640) factor[(1700/ 2640),(1500/ 2640)]))";
            };
            class Engine3_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",2,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1500/ 2640),(2100/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(1500/ 2640),(1670/ 2640)]) * ((rpm/ 2640) factor[(2100/ 2640),(1800/ 2640)]))";
            };
            class Engine4_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",2.24404,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1800/ 2640),(2300/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*(((rpm/ 2640) factor[(1780/ 2640),(2060/ 2640)]) * ((rpm/ 2640) factor[(2450/ 2640),(2200/ 2640)]))";
            };
            class Engine5_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",2.51785,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(2100/ 2640),(2640/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*((rpm/ 2640) factor[(2150/ 2640),(2500/ 2640)])";
            };
            class IdleThrust_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",2.51785,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(400/ 2640),(900/ 2640)])*0.15";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(100/ 2640),(200/ 2640)]) * ((rpm/ 2640) factor[(900/ 2640),(700/ 2640)]))";
            };
            class EngineThrust_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",1.58866,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(700/ 2640),(1100/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(705/ 2640),(850/ 2640)]) * ((rpm/ 2640) factor[(1100 / 2640),(950/ 2640)]))";
            };
            class Engine1_Thrust_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",1.7825,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(950/ 2640),(1400/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(900/ 2640),(1050/ 2640)]) * ((rpm/ 2640) factor[(1400/ 2640),(1200/ 2640)]))";
            };
            class Engine2_Thrust_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",1.7825,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1200/ 2640),(1700/ 2640)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1170/ 2640),(1380/ 2640)]) * ((rpm/ 2640) factor[(1700/ 2640),(1500/ 2640)]))";
            };
            class Engine3_Thrust_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",2,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1500/ 2640),(2100/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1500/ 2640),(1670/ 2640)]) * ((rpm/ 2640) factor[(2100/ 2640),(1800/ 2640)]))";
            };
            class Engine4_Thrust_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",2.24404,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(1800/ 2640),(2300/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 2640) factor[(1780/ 2640),(2060/ 2640)]) * ((rpm/ 2640) factor[(2450/ 2640),(2200/ 2640)]))";
            };
            class Engine5_Thrust_int
            {
                sound[] = {"kobra\442_g_vehicle\aat\sounds\aat_engine_int.wss",2.51785,1};
                frequency = "0.8 + ((rpm/ 2640) factor[(2100/ 2640),(2640/ 2640)])*0.1";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/ 2640) factor[(2150/ 2640),(2500/ 2640)])";
            };
        };
        memoryPointsLeftWaterEffect = "EngineEffectL";
        memoryPointsRightWaterEffect = "EngineEffectR";
        memoryPointTrack1L = "Track LL";
        memoryPointTrack1R = "Track LR";
        memoryPointTrack2L = "Track RL";
        memoryPointTrack2R = "Track RR";
        dustFrontLeftPos = "dustFrontLeft";
        dustFrontRightPos = "dustFrontRight";
        dustBackLeftPos = "dustBackLeft";
        dustBackRightPos = "dustBackRight";
        memoryPointDriverOptics = "driver_view";
        class Exhausts
        {
            class Exhaust_1
            {
                position = "Exhaust_1_pos";
                direction = "Exhaust_1_dir";
                effect = "ExhaustEffectTankSide";
            };
            class Exhaust_2
            {
                position = "Exhaust_2_pos";
                direction = "Exhaust_2_dir";
                effect = "ExhaustEffectTankSide";
            };
        };
        driverAction = "Disabled";
        driverInAction = "Disabled";
        viewDriverInExternal = 1;
        gunBeg = "";
        gunEnd = "";
        memorypointgun = "";
        weapons[] = {};
        magazines[] = {};
        isUav = 1;
        uavCameraDriverPos = "driver_view";
        uavCameraDriverDir = "driver_view_dir";
        class simpleobject
        {
            animate[] = {{"maingun",0},{"mainturret",0},{"damagehide",0}};
        };
        class damage
        {
            tex[] = {};
            mat[] = 
            {
                "kobra\442_g_vehicle\baw_arty\data\body.rvmat",
                "kobra\442_g_vehicle\baw_arty\data\body_damage.rvmat",
                "kobra\442_g_vehicle\baw_arty\data\body_destruction.rvmat",
                "kobra\442_g_vehicle\baw_arty\data\eyes.rvmat",
                "kobra\442_g_vehicle\baw_arty\data\eyes.rvmat",
                "kobra\442_g_vehicle\baw_arty\data\eyes.rvmat",
                "kobra\442_g_vehicle\baw_arty\data\gun.rvmat",
                "kobra\442_g_vehicle\baw_arty\data\gun_damage.rvmat",
                "kobra\442_g_vehicle\baw_arty\data\gun_destruction.rvmat"
            };
        };
    };

    class Aux501_Vehicles_Artillery_HAGM: Aux501_Vehicles_Artillery_HAGM_base
    {
        scope = 2;
        scopecurator = 2;
        crew = "Aux501_Units_CIS_UAV_Crew_Unit";
        typicalCargo[] = {"Aux501_Units_CIS_UAV_Crew_Unit"};
    };
};

class CfgWeapons
{   
    class mortar_155mm_AMOS;

    class Aux501_Vehicles_Weapons_CIS_155mm_howitzer: mortar_155mm_AMOS
    {
        scope = 2;
        displayName = "HAG-M Cannon";
        author = "501st Aux Team";
        fireLightDiffuse[] = {1,0,0};
        fireLightAmbient[] = {1,0,0};
        nameSound = "cannon";
        cursor = "mortar";
        cursorAim = "EmptyCursor";
        sounds[] = {"StandardSound"};
        class StandardSound
        {
            begin1[] = {"\Aux501\Vehicles\Artillery\HAGM\sounds\Weapons\cis_arty_cannon.wss",8,1,2000};
            soundBegin[] = {"begin1",1};
        };
        reloadSound[]=
        {
            "A3\Sounds_F\vehicles\armor\noises\reload_tank_cannon_2",
            31.622778,
            1,
            15
        };
        reloadTime = 7;
        magazineReloadTime = 7;
        autoReload = 1;
        canLock = 0;
        magazines[]=
        {
            "Aux501_Vehicle_Mags_CIS_32Rnd_155mm_he_mag"
        };
        modes[]=
        {
            "Single1",
            "Single2",
            "Single3",
            "Single4",
            "Single5",
            "Burst1",
            "Burst2",
            "Burst3",
            "Burst4",
            "Burst5"
        };
        class Single1: Mode_SemiAuto
        {
            displayName = "$STR_A3_mortar_120mm_AMOS_Single10";
            sounds[] = {"StandardSound"};
            class StandardSound
            {
                begin1[] = {"\Aux501\Vehicles\Artillery\HAGM\sounds\Weapons\cis_arty_cannon.wss",8,1,2000};
                soundBegin[] = {"begin1",1};
            };
            reloadSound[] = {"A3\sounds_f\dummysound",1,1,20};
        };
        class Single2: Single1
        {
            displayName = "$STR_A3_mortar_120mm_AMOS_Single20";
            artilleryCharge = 0.3;
        };
        class Single3: Single1
        {
            displayName = "$STR_A3_mortar_120mm_AMOS_Single30";
            artilleryCharge = 0.48;
        };
        class Burst1: Mode_Burst
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_120mm_AMOS_Burst20";
            dispersion = 0.028;
            burst = 6;
            soundBurst = 0;
            reloadTime = 6;
            minRange = 15;
            minRangeProbab = 0.5;
            midRange = 1500;
            midRangeProbab = 0.7;
            maxRange = 2000;
            maxRangeProbab = 0.5;
            artilleryDispersion = 2.8;
            artilleryCharge = 0.19;
            sounds[] = {"StandardSound"};
            class StandardSound
            {
                begin1[] = {"\Aux501\Vehicles\Artillery\HAGM\sounds\Weapons\cis_arty_cannon.wss",8,1,2000};
                soundBegin[] = {"begin1",1};
            };
            reloadSound[]=
            {
                "A3\sounds_f\dummysound",
                1,
                1,
                20
            };
        };
        class Burst2: Burst1
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_120mm_AMOS_Burst20";
            minRange = 2000;
            minRangeProbab = 0.40000001;
            midRange = 3000;
            midRangeProbab = 0.60000002;
            artilleryDispersion = 10;
            maxRange = 5200;
            maxRangeProbab = 0.40000001;
            artilleryCharge = 0.30000001;
        };
        class Burst3: Burst1
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_120mm_AMOS_Burst30";
            minRange = 5200;
            minRangeProbab = 0.30000001;
            midRange = 8000;
            midRangeProbab = 0.40000001;
            maxRange = 13300;
            maxRangeProbab = 0.30000001;
            artilleryDispersion = 15;
            artilleryCharge = 0.47999999;
        };
        class Burst4: Burst1
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_120mm_AMOS_Burst40";
            minRange = 14600;
            minRangeProbab = 0.2;
            midRange = 25000;
            midRangeProbab = 0.30000001;
            maxRange = 37000;
            artilleryDispersion = 20;
            maxRangeProbab = 0.2;
            artilleryCharge = 0.80000001;
        };
        class Burst5: Burst1
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_120mm_AMOS_Burst50";
            minRange = 25000;
            minRangeProbab = 0.1;
            midRange = 40000;
            midRangeProbab = 0.2;
            maxRange = 58000;
            artilleryDispersion = 25;
            maxRangeProbab = 0.1;
            artilleryCharge = 1;
        };
    };
};

class CfgMagazines
{
    class 32Rnd_155mm_Mo_shells;

    class Aux501_Vehicle_Mags_CIS_32Rnd_155mm_he_mag: 32Rnd_155mm_Mo_shells
    {
        displayNameShort = "HE";
        scope = 2;
        displayName = "32Rnd 125mw HE Shells";
        ammo = "Aux501_Vehicles_155mw_HE_ammo";
        author = "501st Aux Team";
        count = 32;
        nameSound = "heat";
        muzzleImpulseFactor[] = {0.5,0.5};
    };
};

class CfgAmmo
{
    class Sh_155mm_AMOS;

    class Aux501_Vehicles_155mw_HE_ammo: Sh_155mm_AMOS
    {
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Red.p3d";
        effectFly = "Aux501_particle_effect_artillery_fly_red";
        lightColor[] = {1,0,0,0};
        intensity = 10000;
        tracerStartTime = 0.1;
        tracerScale = 10;
        hit = 210;
        indirectHit = 50;
        indirectHitRange = 20;
        dangerRadiusHit = 120;
        suppressionRadiusHit = 90;
        explosive = 0.8;
        cost = 200;
        caliber = 6;
        muzzleEffect = "BIS_Effects_Cannon";
        aiAmmoUsageFlags = "64 + 128 + 512";
        typicalSpeed = 800;
        whistleDist = 2000;
        whistleOnFire = 1;
        soundFly[] = {"\Aux501\Vehicles\Artillery\HAGM\sounds\Explosions\arty_flyby.wss",5,1,2000};
        SoundSetExplosion[] = 
        {
            "Aux501_SoundSet_Arty_Exp",
            "Shell155mm_Tail_SoundSet",
            "Explosion_Debris_SoundSet"
        };
        class CamShakeExplode
        {
            power = 24;
            duration = 2.2;
            frequency = 20;
            distance = 143.636;
        };
        class CamShakeHit
        {
            power = 120;
            duration = 0.6;
            frequency = 20;
            distance = 1;
        };
        class CamShakeFire
        {
            power = 3.30975;
            duration = 2.2;
            frequency = 20;
            distance = 87.6356;
        };
        class CamShakePlayerFire
        {
            power = 0.02;
            duration = 0.1;
            frequency = 20;
            distance = 1;
        };
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_Arty_closeExp
    {
        samples[] = 
        {
            {"\Aux501\Vehicles\Artillery\HAGM\sounds\Explosions\arty_explosion_close1.wss",1},
            {"\Aux501\Vehicles\Artillery\HAGM\sounds\Explosions\arty_explosion_close3.wss",1},
            {"\Aux501\Vehicles\Artillery\HAGM\sounds\Explosions\arty_explosion_close2.wss",1}
        };
        volume = 3.0;
        range = 250;
        rangeCurve = "CannonCloseShotCurve";
    };
    class Aux501_SoundShader_Arty_MidExp
    {
        samples[] = 
        {
            {"\Aux501\Vehicles\Artillery\HAGM\sounds\Explosions\arty_explosion_close1.wss",1},
            {"\Aux501\Vehicles\Artillery\HAGM\sounds\Explosions\arty_explosion_close3.wss",1},
            {"\Aux501\Vehicles\Artillery\HAGM\sounds\Explosions\arty_explosion_close2.wss",1}
        };
        volume = 3.0;
        range = 1000;
        rangeCurve = "CannonMidShotCurve";
    };

    class Aux501_SoundShader_Arty_FarExp
    {
        samples[] = 
        {
            {"\Aux501\Vehicles\Artillery\HAGM\sounds\Explosions\arty_explosion_far1.wss",1},
            {"\Aux501\Vehicles\Artillery\HAGM\sounds\Explosions\arty_explosion_far2.wss",1}
        };
        volume = 3.1220185;
        range = 4000;
        rangeCurve[] = {{0,0},{100,0},{300,1},{4000,1}};
    };
};

class CfgSoundSets
{
    class Shell155mm_Exp_SoundSet;

    class Aux501_SoundSet_Arty_Exp: Shell155mm_Exp_SoundSet
    {
        soundShaders[] = 
        {
            "Aux501_SoundShader_Arty_closeExp",
            "Aux501_SoundShader_Arty_MidExp",
            "Aux501_SoundShader_Arty_FarExp"
        };
    };
};

class CfgLights
{
    class Aux501_light_artillery_blue
    {
        color[] = {0,0,0,0};
        diffuse[] = {0,0,163,1};
        ambient[] = {0,0,0,0.5};
        intensity = 1e6;
        dayLight = 1;
        useFlare = 1;
        flareSize = 7;
        flareMaxDistance = 6000;
    };
    class Aux501_light_artillery_red: Aux501_light_artillery_blue
    {
        diffuse[] = {1,0,0,1};
    };
    class Aux501_light_artillery_yellow: Aux501_light_artillery_blue
    {
        diffuse[] = {255,255,0,1};
    };
    class Aux501_light_artillery_green: Aux501_light_artillery_blue
    {
        diffuse[] = {21,1254,69,1};
    };
    class Aux501_light_artillery_gray: Aux501_light_artillery_blue
    {
        diffuse[] = {45,45,45,0.57};
    };
    class Aux501_light_artillery_orange: Aux501_light_artillery_blue
    {
        diffuse[] = {0.45, 0.21, 0, 1};
    };
};

class Aux501_particle_effect_artillery_fly_blue
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_artillery_blue";
        position[] = {0,0,0};
    };
};
class Aux501_particle_effect_artillery_fly_red
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_artillery_red";
        position[] = {0,0,0};
    };
};
class Aux501_particle_effect_artillery_fly_yellow
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_artillery_yellow";
        position[] = {0,0,0};
    };
};
class Aux501_particle_effect_artillery_fly_green
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_artillery_green";
        position[] = {0,0,0};
    };
};
class Aux501_particle_effect_artillery_fly_gray
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_artillery_gray";
        position[] = {0,0,0};
    };
};
class Aux501_particle_effect_artillery_fly_orange
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_artillery_orange";
        position[] = {0,0,0};
    };
};