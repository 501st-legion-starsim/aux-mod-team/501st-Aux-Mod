class CfgPatches
{
    class Aux501_Patch_Vehicles_Artillery_Hailfire
    {
        addonRootClass = "Aux501_Patch_Vehicles";
        requiredAddons[] = 
        {
            "A3_Weapons_F",
            "A3_Armor_F_Beta"
        };
        units[] = 
        {
            "Aux501_Vehicles_Artillery_IG227_Hailfire_base",
            "Aux501_Vehicles_Artillery_IG227_Hailfire"
        };
        weapons[] = 
        {
            "Aux501_Vehicle_Weapons_Hailfire_Arty"
        };
        magazines[] = 
        {
            "Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets"
        };
        ammo[] = 
        {
            "Aux501_Vehicles_Hailfire_230mm_Ammo",
            "Aux501_Vehicles_Hailfire_230mm_Ammo_Sub"
        };
    };
};

class SensorTemplateIR;
class SensorTemplateVisual;
class SensorTemplatePassiveRadar;
class SensorTemplateActiveRadar;
class SensorTemplateLaser;
class SensorTemplateNV;

class Mode_SemiAuto;
class Mode_Burst;

class DefaultVehicleSystemsDisplayManagerLeft
{
    class components;
};
class DefaultVehicleSystemsDisplayManagerRight
{
    class components;
};

class CfgVehicles
{
    class LandVehicle;

    class Tank: LandVehicle
    {
        class NewTurret;
        class HitPoints;
    };
    class Tank_F: Tank
    {
        class Turrets
        {
            class MainTurret;
        };
        class ViewOptics;
        class HitPoints: HitPoints
        {
            class HitHull;
            class HitFuel;
            class HitEngine;
            class HitLTrack;
            class HitRTrack;
        };
        class Sounds;
    };

    class Aux501_Vehicles_Artillery_IG227_Hailfire_base: Tank_F
    {
        scope = 1;
        scopecurator = 0;
        Author = "501st Aux Team";
        model = "3AS\3AS_hailfire\model\hailfire.p3d";
        hiddenSelections[] = {"camo1","camo2"};
        side = 0;
        crew = "Aux501_Units_CIS_UAV_Crew_Unit";
        forceInGarage = 1;
        armor = 1400;
        ace_cookoff_probability = 0.5;
        destrType = "DestructWreck";
        armorStructural = 10;
        crewExplosionProtection = 0.9999;
        damageResistance = 0.00547;
        cost = 2500000;
        crewVulnerable = 0;
        epeImpulseDamageCoef = 18;
        waterPPInVehicle = 0;
        wheelCircumference = 2.15;
        driverAction = "driver_apcwheeled2_out";
        driverInAction = "driver_apcwheeled2_in";
        tracksSpeed = 1.4;
        memoryPointDriverOptics = "driverview";
        icon = "3AS\3AS_Hailfire\data\ui\Hailfite_top_ca.paa";
        picture = "";
        memoryPointTaskMarker = "TaskMarker_1_pos";
        hideWeaponsDriver = 1;
        hideWeaponsCargo = 1;
        extCameraPosition[] = {0,5,-15};
        simulation = "tankX";
        enginePower = 1000;
		maxOmega = 300;
		maxSpeed = 60;
		peakTorque = 6000;
		torqueCurve[] = {{0.291667,0.540541},{0.416667,0.675676},{0.583333,0.810811},{0.666667,0.891892},{0.75,0.972973},{0.833333,1.02703},{0.916667,1},{1,0.945946}};
		thrustDelay = 0.05;
		clutchStrength = 180.0;
		fuelCapacity = 50;
		brakeIdleSpeed = 1.78;
		latency = 1.5;
		idleRpm = 700;
		redRpm = 2640;
		engineLosses = 25;
		transmissionLosses = 15;
		dampingRateFullThrottle = 0.08;
		dampingRateZeroThrottleClutchEngaged = 20;
		dampingRateZeroThrottleClutchDisengaged = 40;
		changeGearType = "rpmratio";
		changeGearOmegaRatios[] = {1,0.5,0.625,0.458333,0.979167,0.458333,0.958333,0.5625,0.958333,0.583333,1,0.583333};
		class complexGearbox
		{
			GearboxRatios[] = {"R2",-2,"N",0,"D1",5.5,"D2",4.0,"D3",1.8,"D4",1.1};
			TransmissionRatios[] = {"High",7};
			gearBoxMode = "auto";
			moveOffGear = 1;
			driveString = "D";
			neutralString = "N";
			reverseString = "R";
			transmissionDelay = 0.1;
		};
		tankTurnForce = 400000;
		class Wheels
		{
			class L2
			{
				boneName = "wheel_podkoloL1";
				center = "wheel_1_2_axis";
				boundary = "wheel_1_2_bound";
				steering = 0;
				side = "left";
				mass = 150;
				MOI = 22.5;
				latStiffX = 25;
				latStiffY = 180;
				suspTravelDirection[] = {0,-1,0};
				longitudinalStiffnessPerUnitGravity = 10000;
				maxBrakeTorque = 63000;
				sprungMass = 3333;
				springStrength = 163000;
				springDamperRate = 53000;
				dampingRate = 2;
				dampingRateInAir = 8000.0;
				dampingRateDamaged = 10.0;
				dampingRateDestroyed = 10000.0;
				maxDroop = 0.5;
				maxCompression = 0.5;
				frictionVsSlipGraph[] = {{0.0,1},{0.5,1},{1,1}};
			};
			class L3: L2
			{
				boneName = "wheel_podkolol2";
				center = "wheel_1_3_axis";
				boundary = "wheel_1_3_bound";
			};
			class L4: L2
			{
				boneName = "wheel_podkolol3";
				center = "wheel_1_4_axis";
				boundary = "wheel_1_4_bound";
			};
			class L5: L2
			{
				boneName = "wheel_podkolol4";
				center = "wheel_1_5_axis";
				boundary = "wheel_1_5_bound";
			};
			class L6: L2
			{
				boneName = "wheel_podkolol5";
				center = "wheel_1_6_axis";
				boundary = "wheel_1_6_bound";
			};
			class L7: L2
			{
				boneName = "wheel_podkolol6";
				center = "wheel_1_7_axis";
				boundary = "wheel_1_7_bound";
			};
			class R2: L2
			{
				side = "right";
				boneName = "wheel_podkolop1";
				center = "wheel_2_2_axis";
				boundary = "wheel_2_2_bound";
			};
			class R3: R2
			{
				boneName = "wheel_podkolop2";
				center = "wheel_2_3_axis";
				boundary = "wheel_2_3_bound";
			};
			class R4: R2
			{
				boneName = "wheel_podkolop3";
				center = "wheel_2_4_axis";
				boundary = "wheel_2_4_bound";
			};
			class R5: R2
			{
				boneName = "wheel_podkolop4";
				center = "wheel_2_5_axis";
				boundary = "wheel_2_5_bound";
			};
			class R6: R2
			{
				boneName = "wheel_podkolop5";
				center = "wheel_2_6_axis";
				boundary = "wheel_2_6_bound";
			};
			class R7: R2
			{
				boneName = "wheel_podkolop6";
				center = "wheel_2_7_axis";
				boundary = "wheel_2_7_bound";
			};
		};
        turretInfoType = "RscWeaponRangeArtilleryAuto";
        forcehideDriver = 1;
        class HitPoints: HitPoints
        {
            class HitHull: HitHull
            {
                armor = 0.8;
                material = -1;
                name = "telo";
                visual = "zbytek";
                passThrough = 1;
                minimalHit = 0.14;
                explosionShielding = 2.0;
                radius = 0.25;
            };
            class HitEngine: HitEngine
            {
                armor = 1;
                material = -1;
                name = "motor";
                passThrough = 0.8;
                minimalHit = 0.24;
                explosionShielding = 1;
                radius = 0.33;
            };
            class HitLTrack: HitLTrack
            {
                armor = 0.5;
                material = -1;
                name = "pas_L";
                passThrough = 0;
                minimalHit = 0.08;
                explosionShielding = 1.44;
                radius = 0.3;
            };
            class HitRTrack: HitRTrack
            {
                armor = 0.5;
                material = -1;
                name = "pas_P";
                passThrough = 0;
                minimalHit = 0.08;
                explosionShielding = 1.44;
                radius = 0.3;
            };
            class HitFuel: HitFuel
            {
                armor = 0.5;
                material = -1;
                name = "palivo";
                passThrough = 0.1;
                minimalHit = 0.1;
                explosionShielding = 0.6;
                radius = 0.33;
            };
        };
        class AttributeValues
        {
            RadarUsageAI = 1;
        };
        class Eventhandlers
        {
            fired = "[_this select 0,_this select 6,'missile_move','MissileBase'] spawn BIS_fnc_missileLaunchPositionFix;";
        };
        class Reflectors{};
        class Turrets: Turrets
        {
            class Mainturret1: Mainturret
            {
                body = "mainTurret2";
                gunnerCompartments = "Compartment1";
                gun = "maingun2";
                proxyIndex = 1;
                viewGunnerInExternal = 0;
                gunnername = "Gunner";
                animationSourceBody = "mainTurret2";
                animationSourceGun = "maingun2";
                maxHorizontalRotSpeed = 1.8;
                maxVerticalRotSpeed = 1.8;
                stabilizedInAxes = 3;
                soundServo[] = {"A3\Sounds_F\vehicles\armor\noises\servo_best","db-40",1.0,50};
                minElev = -10;
                maxElev = 80;
                initElev = 0;
                minTurn = -15;
                maxTurn = 15;
                initTurn = 0;
                gunnerForceOptics = 1;
                gunnerAction = "crew_tank01_out";
                gunnerInAction = "mbt2_slot2b_in";
                elevationmode = 0;
                forceHideGunner = 1;
                outGunnerMayFire = 0;
                inGunnerMayFire = 1;
                gunnerRightHandAnimName = "";
                gunnerLeftHandAnimName = "";
                soundAttenuationTurret = "HeliAttenuationGunner";
                missileBeg = "pos_missile";
                missileEnd = "pos_missile_end";
                isPersonTurret = 0;
                memoryPointGun[] = {"z_gunL_muzzle","z_gunR_muzzle"};
                weapons[]=
                {
                    "Aux501_Vehicle_Weapons_Hailfire_Arty"
                };
                magazines[]=
                {
                    "Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets",
                    "Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets",
                    "Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets",
                    "Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets",
                    "Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets",
                    "Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets",
                    "Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets",
                    "Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets"
                };
                turretInfoType = "RscWeaponRangeZeroing";
                discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500};
                discreteDistanceInitIndex = 2;
                memoryPointGunnerOptics = "CommanderView";
                memoryPointGunnerOutOptics = "";
                gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Commander_02_F";
                gunnerOutOpticsModel = "";
                gunnerOpticsEffect[] = {};
                gunnerHasFlares = 1;
                turretFollowFreeLook = 0;
                class ViewOptics: ViewOptics
                {
                    initAngleX = 0;
                    minAngleX = -15;
                    maxAngleX = 15;
                    initAngleY = 0;
                    minAngleY = -10;
                    maxAngleY = 60;
                    initFov = 0.155;
                    minFov = 0.034;
                    maxFov = 0.155;
                    visionMode[] = {"Normal","NVG","Ti"};
                    thermalMode[] = {2,3,4};
                };
                gunnerGetInAction = "GetInHigh";
                gunnerGetOutAction = "GetOutHigh";
                startEngine = 0;
                LODTurnedIn = "VIEW_GUNNER";
                class HitPoints
                {
                    class HitTurret
                    {
                        armor = 0.6;
                        material = -1;
                        name = "Commander_Turret";
                        visual = "turret_rocket";
                        passThrough = 0;
                        minimalHit = 0.03;
                        explosionShielding = 0.6;
                        radius = 0.5;
                    };
                    class HitGun
                    {
                        armor = 0.6;
                        material = -1;
                        name = "Commander_Gun";
                        visual = "gun_rocket";
                        passThrough = 0;
                        minimalHit = 0.03;
                        explosionShielding = 0.6;
                        radius = 0.5;
                    };
                };
                class Turrets{};
                class Components
                {
                    class VehicleSystemsDisplayManagerComponentLeft: DefaultVehicleSystemsDisplayManagerLeft
                    {
                        defaultDisplay = "EmptyDisplay";
                        class Components
                        {
                            class EmptyDisplay
                            {
                                componentType = "EmptyDisplayComponent";
                            };
                            class MinimapDisplay
                            {
                                componentType = "MinimapDisplayComponent";
                                resource = "RscCustomInfoMiniMap";
                            };
                            class VehicleDriverDisplay
                            {
                                componentType = "TransportFeedDisplayComponent";
                                source = "Driver";
                            };
                            class VehicleMissileDisplay
                            {
                                componentType = "TransportFeedDisplayComponent";
                                source = "Missile";
                            };
                            class SensorDisplay
                            {
                                componentType = "SensorsDisplayComponent";
                                range[] = {8000,2000,4000,16000,30000};
                                resource = "RscCustomInfoSensors";
                            };
                        };
                    };
                    class VehicleSystemsDisplayManagerComponentRight: DefaultVehicleSystemsDisplayManagerRight
                    {
                        defaultDisplay = "SensorDisplay";
                        class Components
                        {
                            class EmptyDisplay
                            {
                                componentType = "EmptyDisplayComponent";
                            };
                            class MinimapDisplay
                            {
                                componentType = "MinimapDisplayComponent";
                                resource = "RscCustomInfoMiniMap";
                            };
                            class VehicleDriverDisplay
                            {
                                componentType = "TransportFeedDisplayComponent";
                                source = "Driver";
                            };
                            class VehicleMissileDisplay
                            {
                                componentType = "TransportFeedDisplayComponent";
                                source = "Missile";
                            };
                            class SensorDisplay
                            {
                                componentType = "SensorsDisplayComponent";
                                range[] = {8000,2000,4000,16000,30000};
                                resource = "RscCustomInfoSensors";
                            };
                        };
                    };
                };
            };
            class Mainturret2: Mainturret1
            {
                gunnerCompartments = "Compartment1";
                memoryPointGun = "usti hlavne";
                gunBeg = "usti hlavne";
                gunEnd = "konec hlavne";
                weapons[] = {};
                magazines[] = {};
                turretInfoType = "RscWeaponRangeZeroing";
                discreteDistance[] = {100,200,300,400,500,600,700,800,900,1000,1100,1200,1300,1400,1500};
                selectionFireAnim = "zasleh1";
                animationSourceBody = "Mainturret";
                animationSourceGun = "MainGun";
                body = "Mainturret";
                gun = "MainGun";
                discreteDistanceInitIndex = 5;
                memoryPointGunnerOptics = "gunnerview";
                gunnerOutOpticsModel = "";
                gunnerOutOpticsEffect[] = {};
                gunnerOpticsEffect[] = {};
                gunnerForceOptics = 1;
                visionMode[] = {"Normal"};
                thermalMode[] = {};
                class OpticsIn
                {
                    class Wide
                    {
                        initAngleX = 0;
                        minAngleX = -30;
                        maxAngleX = 30;
                        initAngleY = 0;
                        minAngleY = -100;
                        maxAngleY = 100;
                        initFov = 0.155;
                        minFov = 0.155;
                        maxFov = 0.155;
                        visionMode[] = {"Normal","NVG","Ti"};
                        thermalMode[] = {0,1};
                        gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Commander_02_F";
                        gunnerOpticsEffect[] = {};
                    };
                    class Narrow: Wide
                    {
                        gunnerOpticsModel = "\A3\weapons_f\reticle\Optics_Commander_02_F";
                        initFov = 0.047;
                        minFov = 0.047;
                        maxFov = 0.047;
                    };
                };
                gunnerAction = "mbt2_slot2b_in";
                forceHideGunner = 1;
                outGunnerMayFire = 1;
                gunnerInAction = "mbt2_slot2b_in";
                gunnerRightHandAnimName = "";
                gunnerLeftHandAnimName = "";
                gunnerFireAlsoInInternalCamera = 1;
                gunnerOutFireAlsoInInternalCamera = 1;
                proxyIndex = 1;
                viewGunnerInExternal = 0;
                gunnerName = "Commander";
                proxytype = "CPCommander";
                isPersonTurret = 0;
                personTurretAction = "vehicle_turnout_1";
                minOutElev = -10;
                maxOutElev = 15;
                initOutElev = 0;
                minTurn = -30;
                maxTurn = 30;
                initOutTurn = 0;
                soundServo[] = {"A3\Sounds_F\vehicles\armor\noises\servo_best","db-40",1.0,50};
                minElev = -5;
                maxElev = 20;
                initElev = 0;
                inGunnerMayFire = 1;
                class HitPoints
                {
                    class HitTurret
                    {
                        armor = 0.8;
                        material = -1;
                        name = "otocvez";
                        visual = "Otocvez";
                        passThrough = 0;
                        minimalHit = 0.02;
                        explosionShielding = 1;
                        radius = 0.15;
                    };
                    class HitGun
                    {
                        armor = 0.75;
                        material = -1;
                        name = "otocvez";
                        visual = "OtocHlaven";
                        passThrough = 0;
                        minimalHit = 0;
                        explosionShielding = 1;
                        radius = 0.15;
                    };
                };
            };
        };
        class AnimationSources
        {
            class Muzzle_flash
            {
                source = "ammorandom";
                weapon = "RD501_AAT_Repeater";
            };
            class Revolving
            {
                source = "revolving";
                weapon = "Aux501_Vehicle_Weapons_Hailfire_Arty";
            };
        };
        memoryPointLMissile = "pos_missile_l";
        memoryPointRMissile = "pos_missile_r";
        memoryPointLRocket = "pos_missile_l";
        memoryPointRRocket = "pos_missile_r";
        memoryPointMissile[] = {"pos_missile","pos_missile_l"};
        memoryPointMissileDir[] = {"pos_missile_end","pos_missile_l"};
        attenuationEffectType = "TankAttenuation";
        soundGetIn[] = {"A3\sounds_f\vehicles\armor\noises\get_in_out",0.562341,1};
        soundGetOut[] = {"A3\sounds_f\vehicles\armor\noises\get_in_out",0.562341,1,20};
        soundDammage[] = {"",0.562341,1};
        soundEngineOnInt[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\eng_speeder_startup",1,1};
        soundEngineOnExt[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\eng_speeder_startup",1,1};
        soundEngineOffInt[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\eng_speeder_shutdown",1,1,200};
        soundEngineOffExt[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\eng_speeder_shutdown",1,1,200};
        buildCrash0[] = {"A3\sounds_f\Vehicles\crashes\crash_08",1,1,200};
        buildCrash1[] = {"A3\sounds_f\Vehicles\crashes\crash_09",1,1,200};
        buildCrash2[] = {"A3\sounds_f\Vehicles\crashes\crash_10",1,1,200};
        buildCrash3[] = {"A3\sounds_f\Vehicles\crashes\crash_11",1,1,200};
        soundBuildingCrash[] = {"buildCrash0",0.25,"buildCrash1",0.25,"buildCrash2",0.25,"buildCrash3",0.25};
        WoodCrash0[] = {"A3\sounds_f\Vehicles\crashes\crash_08",1,1,200};
        WoodCrash1[] = {"A3\sounds_f\Vehicles\crashes\crash_09",1,1,200};
        WoodCrash2[] = {"A3\sounds_f\Vehicles\crashes\crash_10",1,1,200};
        WoodCrash3[] = {"A3\sounds_f\Vehicles\crashes\crash_11",1,1,200};
        WoodCrash4[] = {"A3\sounds_f\Vehicles\crashes\crash_01",1,1,200};
        WoodCrash5[] = {"A3\sounds_f\Vehicles\crashes\crash_08",1,1,200};
        soundWoodCrash[] = {"woodCrash0",0.166,"woodCrash1",0.166,"woodCrash2",0.166,"woodCrash3",0.166,"woodCrash4",0.166,"woodCrash5",0.166};
        ArmorCrash0[] = {"A3\sounds_f\Vehicles\crashes\crash_08",1,1,200};
        ArmorCrash1[] = {"A3\sounds_f\Vehicles\crashes\crash_09",1,1,200};
        ArmorCrash2[] = {"A3\sounds_f\Vehicles\crashes\crash_10",1,1,200};
        ArmorCrash3[] = {"A3\sounds_f\Vehicles\crashes\crash_11",1,1,200};
        soundArmorCrash[] = {"ArmorCrash0",0.25,"ArmorCrash1",0.25,"ArmorCrash2",0.25,"ArmorCrash3",0.25};
        class Sounds
        {
            class Idle_ext
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_idle_lp",0.31622776,1,100};
                frequency = "0.9 + ((rpm/6900) factor[(400/6900),(1150/6900)])*0.2";
                volume = "engineOn*camPos*(((rpm/6900) factor[(400/6900),(700/6900)]) * ((rpm/6900) factor[(1100/6900),(900/6900)]))";
            };
            class Engine
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_idle_lp",0.35481337,1,200};
                frequency = "0.8 + ((rpm/6900) factor[(900/6900),(2100/6900)])*0.2";
                volume = "engineOn*camPos*(((rpm/6900) factor[(870/6900),(1100/6900)]) * ((rpm/6900) factor[(2100/6900),(1300/6900)]))";
            };
            class Engine1_ext
            {
                sound[] = {"","db-9",1,240};
                frequency = "0.8 + ((rpm/6900) factor[(1300/6900),(3100/6900)])*0.2";
                volume = "engineOn*camPos*(((rpm/6900) factor[(1250/6900),(2050/6900)]) * ((rpm/6900) factor[(3100/6900),(2300/6900)]))";
            };
            class Engine2_ext
            {
                sound[] = {"","db-8",1,280};
                frequency = "0.8 + ((rpm/6900) factor[(2200/6900),(4100/6900)])*0.2";
                volume = "engineOn*camPos*(((rpm/6900) factor[(2250/6900),(3050/6900)]) * ((rpm/6900) factor[(4100/6900),(3300/6900)]))";
            };
            class Engine3_ext
            {
                sound[] = {"","db-7",1,320};
                frequency = "0.8 + ((rpm/6900) factor[(3300/6900),(4900/6900)])*0.2";
                volume = "engineOn*camPos*(((rpm/6900) factor[(3250/6900),(4050/6900)]) * ((rpm/6900) factor[(4870/6900),(4200/6900)]))";
            };
            class Engine4_ext
            {
                sound[] = {"","db-6",1,360};
                frequency = "0.8 + ((rpm/6900) factor[(4200/6900),(6200/6900)])*0.2";
                volume = "engineOn*camPos*(((rpm/6900) factor[(4150/6900),(4800/6900)]) * ((rpm/6900) factor[(6150/6900),(5150/6900)]))";
            };
            class Engine5_ext
            {
                sound[] = {"","db-5",1,420};
                frequency = "0.95 + ((rpm/6900) factor[(5100/6900),(6900/6900)])*0.15";
                volume = "engineOn*camPos*((rpm/6900) factor[(5100/6900),(6100/6900)])";
            };
            class IdleThrust
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_accel",0.56234133,1,150};
                frequency = "0.9 + ((rpm/6900) factor[(400/6900),(1150/6900)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(400/6900),(700/6900)]) * ((rpm/6900) factor[(1100/6900),(900/6900)]))";
            };
            class EngineThrust
            {
                sound[] = {"","db-5",1,250};
                frequency = "0.8 + ((rpm/6900) factor[(900/6900),(2100/6900)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(870/6900),(1100/6900)]) * ((rpm/6900) factor[(2100/6900),(1300/6900)]))";
            };
            class Engine1_Thrust_ext
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_accel",0.70794576,1,250};
                frequency = "0.8 + ((rpm/6900) factor[(1300/6900),(3100/6900)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(1250/6900),(2050/6900)]) * ((rpm/6900) factor[(3100/6900),(2300/6900)]))";
            };
            class Engine2_Thrust_ext
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_accel",0.70794576,1,300};
                frequency = "0.8 + ((rpm/6900) factor[(2200/6900),(4100/6900)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(2250/6900),(3050/6900)]) * ((rpm/6900) factor[(4100/6900),(3300/6900)]))";
            };
            class Engine3_Thrust_ext
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_accel",0.70794576,1,350};
                frequency = "0.8 + ((rpm/6900) factor[(3300/6900),(4900/6900)])*0.2";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(3250/6900),(4050/6900)]) * ((rpm/6900) factor[(4870/6900),(4200/6900)]))";
            };
            class Engine4_Thrust_ext
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_accel",0.70794576,1,400};
                frequency = "0.8 + ((rpm/6900) factor[(4200/6900),(6200/6900)])*0.3";
                volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(4150/6900),(4800/6900)]) * ((rpm/6900) factor[(6150/6900),(5150/6900)]))";
            };
            class Idle_int
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_idle_lp",0.25118864,1};
                frequency = "0.9 + ((rpm/6900) factor[(400/6900),(1150/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/6900) factor[(400/6900),(700/6900)]) * ((rpm/6900) factor[(1100/6900),(900/6900)]))";
            };
            class Engine_int
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_idle_lp",0.2818383,1};
                frequency = "0.8 + ((rpm/6900) factor[(900/6900),(2100/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/6900) factor[(870/6900),(1100/6900)]) * ((rpm/6900) factor[(2100/6900),(1300/6900)]))";
            };
            class Engine1_int
            {
                sound[] = {"","db-12",1};
                frequency = "0.8 + ((rpm/6900) factor[(1300/6900),(3100/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/6900) factor[(1250/6900),(2050/6900)]) * ((rpm/6900) factor[(3100/6900),(2300/6900)]))";
            };
            class Engine2_int
            {
                sound[] = {"","db-11",1};
                frequency = "0.8 + ((rpm/6900) factor[(2200/6900),(4100/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/6900) factor[(2250/6900),(3050/6900)]) * ((rpm/6900) factor[(4100/6900),(3300/6900)]))";
            };
            class Engine3_int
            {
                sound[] = {"","db-10",1};
                frequency = "0.8 + ((rpm/6900) factor[(3300/6900),(4900/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/6900) factor[(3250/6900),(4050/6900)]) * ((rpm/6900) factor[(4870/6900),(4200/6900)]))";
            };
            class Engine4_int
            {
                sound[] = {"","db-9",1};
                frequency = "0.8 + ((rpm/6900) factor[(4200/6900),(6200/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(((rpm/6900) factor[(4150/6900),(4800/6900)]) * ((rpm/6900) factor[(6150/6900),(5150/6900)]))";
            };
            class Engine5_int
            {
                sound[] = {"","db-6",1};
                frequency = "0.95 + ((rpm/6900) factor[(5100/6900),(6900/6900)])*0.15";
                volume = "engineOn*(1-camPos)*((rpm/6900) factor[(5100/6900),(6100/6900)])";
            };
            class IdleThrust_int
            {
                sound[] = {"","db-10",1};
                frequency = "0.9 + ((rpm/6900) factor[(400/6900),(1150/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(400/6900),(700/6900)]) * ((rpm/6900) factor[(1100/6900),(900/6900)]))";
            };
            class EngineThrust_int
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_accel",0.63095737,1};
                frequency = "0.8 + ((rpm/6900) factor[(900/6900),(2100/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(870/6900),(1100/6900)]) * ((rpm/6900) factor[(2100/6900),(1300/6900)]))";
            };
            class Engine1_Thrust_int
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_accel",0.70794576,1};
                frequency = "0.8 + ((rpm/6900) factor[(1300/6900),(3100/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(1250/6900),(2050/6900)]) * ((rpm/6900) factor[(3100/6900),(2300/6900)]))";
            };
            class Engine2_Thrust_int
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_accel",0.7943282,1};
                frequency = "0.8 + ((rpm/6900) factor[(2200/6900),(4100/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(2250/6900),(3050/6900)]) * ((rpm/6900) factor[(4100/6900),(3300/6900)]))";
            };
            class Engine3_Thrust_int
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_accel",0.8912509,1};
                frequency = "0.8 + ((rpm/6900) factor[(3300/6900),(4900/6900)])*0.2";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(3250/6900),(4050/6900)]) * ((rpm/6900) factor[(4870/6900),(4200/6900)]))";
            };
            class Engine4_Thrust_int
            {
                sound[] = {"WarMantle\WM_Imperial_Vehicles\CombatSpeeder\data\sound\veh_av21speeder_accel",1,1};
                frequency = "0.8 + ((rpm/6900) factor[(4200/6900),(6200/6900)])*0.3";
                volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/6900) factor[(4150/6900),(4800/6900)]) * ((rpm/6900) factor[(6150/6900),(5150/6900)]))";
            };
            class Movement
            {
                sound = "soundEnviron";
                frequency = "1";
                volume = "0";
            };
            class TiresRockOut
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "camPos*rock*(speed factor[2, 20])";
            };
            class TiresSandOut
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "camPos*sand*(speed factor[2, 20])";
            };
            class TiresGrassOut
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "camPos*grass*(speed factor[2, 20])";
            };
            class TiresMudOut
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "camPos*mud*(speed factor[2, 20])";
            };
            class TiresGravelOut
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "camPos*gravel*(speed factor[2, 20])";
            };
            class TiresAsphaltOut
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "camPos*asphalt*(speed factor[2, 20])";
            };
            class NoiseOut
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "camPos*(damper0 max 0.02)*(speed factor[0, 8])";
            };
            class TiresRockIn
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "(1-camPos)*rock*(speed factor[2, 20])";
            };
            class TiresSandIn
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "(1-camPos)*sand*(speed factor[2, 20])";
            };
            class TiresGrassIn
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "(1-camPos)*grass*(speed factor[2, 20])";
            };
            class TiresMudIn
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "(1-camPos)*mud*(speed factor[2, 20])";
            };
            class TiresGravelIn
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "(1-camPos)*gravel*(speed factor[2, 20])";
            };
            class TiresAsphaltIn
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "(1-camPos)*asphalt*(speed factor[2, 20])";
            };
            class NoiseIn
            {
                sound[] = {"",0.5011872,1,60};
                frequency = "1";
                volume = "(damper0 max 0.1)*(speed factor[0, 8])*(1-camPos)";
            };
            class breaking_ext_road
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*camPos*asphalt*(LongSlipDrive Factor[-0.1, -0.4])*(Speed Factor[2, 15])";
            };
            class acceleration_ext_road
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*camPos*asphalt*(LongSlipDrive Factor[0.1, 0.4])*(Speed Factor[15, 2])";
            };
            class turn_left_ext_road
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*camPos*asphalt*(latSlipDrive Factor[0.1, 0.4])*(Speed Factor[2, 15])";
            };
            class turn_right_ext_road
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*camPos*asphalt*(latSlipDrive Factor[-0.1, -0.4])*(Speed Factor[2, 15])";
            };
            class breaking_ext_dirt
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*camPos*(1-asphalt)*(LongSlipDrive Factor[-0.1, -0.4])*(Speed Factor[1, 15])";
            };
            class acceleration_ext_dirt
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*camPos*(1-asphalt)*(LongSlipDrive Factor[0.1, 0.4])*(Speed Factor[15, 1])";
            };
            class turn_left_ext_dirt
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*camPos*(1-asphalt)*(latSlipDrive Factor[0.1, 0.4])*(Speed Factor[1, 15])";
            };
            class turn_right_ext_dirt
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*camPos*(1-asphalt)*(latSlipDrive Factor[-0.1, -0.4])*(Speed Factor[1, 15])";
            };
            class breaking_int_road
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*asphalt*(1-camPos)*(LongSlipDrive Factor[-0.1, -0.4])*(Speed Factor[2, 15])";
            };
            class acceleration_int_road
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*asphalt*(1-camPos)*(LongSlipDrive Factor[0.1, 0.4])*(Speed Factor[15, 2])";
            };
            class turn_left_int_road
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*asphalt*(1-camPos)*(latSlipDrive Factor[0.1, 0.4])*(Speed Factor[2, 15])";
            };
            class turn_right_int_road
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*asphalt*(1-camPos)*(latSlipDrive Factor[-0.1, -0.4])*(Speed Factor[2, 15])";
            };
            class breaking_int_dirt
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*(1-asphalt)*(1-camPos)*(LongSlipDrive Factor[-01, -0.4])*(Speed Factor[2, 15])";
            };
            class acceleration_int_dirt
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*(1-asphalt)*(1-camPos)*(LongSlipDrive Factor[0.1, 0.4])*(Speed Factor[15, 2])";
            };
            class turn_left_int_dirt
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*(1-asphalt)*(1-camPos)*(latSlipDrive Factor[0.1, 0.4])*(Speed Factor[2, 15])";
            };
            class turn_right_int_dirt
            {
                sound[] = {"",0.5011872,1,60};
                frequency = 1;
                volume = "engineOn*(1-asphalt)*(1-camPos)*(latSlipDrive Factor[-0.1, -0.4])*(Speed Factor[2, 15])";
            };
        };
        
        class Library
        {
            libTextDesc = "";
        };
        class Components
        {
            class SensorsManagerComponent
            {
                class Components
                {
                    class IRSensorComponent: SensorTemplateIR
                    {
                        class AirTarget
                        {
                            minRange = 30000;
                            maxRange = 30000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = 1;
                        };
                        class GroundTarget
                        {
                            minRange = 30000;
                            maxRange = 30000;
                            objectDistanceLimitCoef = 1;
                            viewDistanceLimitCoef = 1;
                        };
                        angleRangeHorizontal = 360;
                        angleRangeVertical = 360;
                        maxTrackableSpeed = 400;
                        componentType = "IRSensorComponent";
                        typeRecognitionDistance = 2000;
                        maxFogSeeThrough = 0.995;
                        color[] = {1,0,0,1};
                        allowsMarking = 1;
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = -1;
                        minSpeedThreshold = 0;
                        maxSpeedThreshold = 0;
                        animDirection = "";
                        aimDown = 0;
                        minTrackableSpeed = -1e+10;
                        minTrackableATL = -1e+10;
                        maxTrackableATL = 1e+10;
                    };
                    class VisualSensorComponent: SensorTemplateVisual
                    {
                        class AirTarget
                        {
                            minRange = 30000;
                            maxRange = 30000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = 1;
                        };
                        class GroundTarget
                        {
                            minRange = 30000;
                            maxRange = 30000;
                            objectDistanceLimitCoef = 1;
                            viewDistanceLimitCoef = 1;
                        };
                        angleRangeHorizontal = 360;
                        angleRangeVertical = 360;
                        maxTrackableSpeed = 100;
                        aimDown = 1;
                        animDirection = "";
                        componentType = "VisualSensorComponent";
                        nightRangeCoef = 0;
                        maxFogSeeThrough = 0.94;
                        color[] = {1,1,0.5,0.8};
                        typeRecognitionDistance = 2000;
                        allowsMarking = 1;
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = -1;
                        minSpeedThreshold = 0;
                        maxSpeedThreshold = 0;
                        minTrackableSpeed = -1e+10;
                        minTrackableATL = -1e+10;
                        maxTrackableATL = 1e+10;
                    };
                    class PassiveRadarSensorComponent: SensorTemplatePassiveRadar
                    {
                        componentType = "PassiveRadarSensorComponent";
                        class AirTarget
                        {
                            minRange = 45000;
                            maxRange = 45000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget
                        {
                            minRange = 20000;
                            maxRange = 20000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        typeRecognitionDistance = 12000;
                        angleRangeHorizontal = 360;
                        angleRangeVertical = 360;
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = -1;
                        minSpeedThreshold = 0;
                        maxSpeedThreshold = 0;
                        animDirection = "";
                        aimDown = 0;
                        color[] = {0.5,1,0.5,0.5};
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = -1e+10;
                        maxTrackableATL = 1e+10;
                        allowsMarking = 0;
                    };
                    class ActiveRadarSensorComponent: SensorTemplateActiveRadar
                    {
                        class AirTarget
                        {
                            minRange = 30000;
                            maxRange = 30000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget
                        {
                            minRange = 8000;
                            maxRange = 8000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        typeRecognitionDistance = 8000;
                        angleRangeHorizontal = 180;
                        angleRangeVertical = 180;
                        groundNoiseDistanceCoef = 0.2;
                        componentType = "ActiveRadarSensorComponent";
                        maxGroundNoiseDistance = 200;
                        minSpeedThreshold = 30;
                        maxSpeedThreshold = 40;
                        color[] = {0,1,1,1};
                        allowsMarking = 1;
                        animDirection = "";
                        aimDown = 0;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = -1e+10;
                        maxTrackableATL = 1e+10;
                    };
                    class LaserSensorComponent: SensorTemplateLaser
                    {
                        componentType = "LaserSensorComponent";
                        class AirTarget
                        {
                            minRange = 15000;
                            maxRange = 15000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget
                        {
                            minRange = 15000;
                            maxRange = 15000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        angleRangeHorizontal = 180;
                        angleRangeVertical = 180;
                        typeRecognitionDistance = 0;
                        color[] = {1,1,1,0};
                        allowsMarking = 1;
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = -1;
                        minSpeedThreshold = 0;
                        maxSpeedThreshold = 0;
                        animDirection = "";
                        aimDown = 0;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = -1e+10;
                        maxTrackableATL = 1e+10;
                    };
                    class NVSensorComponent: SensorTemplateNV
                    {
                        componentType = "NVSensorComponent";
                        color[] = {1,1,1,0};
                        typeRecognitionDistance = 0;
                        class AirTarget
                        {
                            minRange = 8000;
                            maxRange = 8000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        class GroundTarget
                        {
                            minRange = 8000;
                            maxRange = 8000;
                            objectDistanceLimitCoef = -1;
                            viewDistanceLimitCoef = -1;
                        };
                        angleRangeHorizontal = 90;
                        angleRangeVertical = 90;
                        allowsMarking = 1;
                        groundNoiseDistanceCoef = -1;
                        maxGroundNoiseDistance = -1;
                        minSpeedThreshold = 0;
                        maxSpeedThreshold = 0;
                        animDirection = "";
                        aimDown = 0;
                        minTrackableSpeed = -1e+10;
                        maxTrackableSpeed = 1e+10;
                        minTrackableATL = -1e+10;
                        maxTrackableATL = 1e+10;
                    };
                };
            };
        };
        class Damage
        {
            tex[] = {};
            mat[] = {"3as\3AS_Hailfire\data\Head.rvmat","A3\armor_f_gamma\MBT_01\Data\MBT_01_body_damage.rvmat","A3\armor_f_gamma\MBT_01\Data\MBT_01_body_destruct.rvmat","3as\3AS_Hailfire\data\Wheels.rvmat","A3\armor_f_gamma\MBT_01\Data\MBT_01_body_damage.rvmat","A3\armor_f_gamma\MBT_01\Data\MBT_01_body_destruct.rvmat"};
        };
    };

    class Aux501_Vehicles_Artillery_IG227_Hailfire: Aux501_Vehicles_Artillery_IG227_Hailfire_base
    {
        scope = 2;
        scopeCurator = 2;
        displayname = "IG-227 Artillery Hailfire";
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_Artillery";
        availableForSupportTypes[] = {"Artillery"};
        artilleryScanner = 1;
        ballisticsComputer = 0;
        hiddenSelectionsTextures[] = 
        {
            "3AS\3AS_hailfire\data\headblue_co.paa",
            "3AS\3AS_hailfire\data\wheelsblue_co.paa"
        };
        class Turrets: Turrets
        {
            class Mainturret1: Mainturret1
            {
                minElev = 0;
                initElev = 11.5;
                maxElev = 87.5;
                elevationMode = 3;
                lockWhenVehicleSpeed = 1;
                outGunnerMayFire = 1;
                canHideGunner = 0;
                missileBeg = "pos_missile";
                missileEnd = "pos_missile_end";
                memoryPointGun[] = {"pos_missile"};
            };
            class Mainturret2
            {
                selectionFireAnim = "zasleh1";    
            };
        };
        
    };
};

class CfgWeapons
{
    class RocketPods;

    class Aux501_Vehicle_Weapons_Hailfire_Arty: RocketPods
    {
        scope = 1;
        Author = "501st Aux Team";
        displayName = "IG-227 missile Pods";
        cursor = "EmptyCursor";
        cursorAim = "EmptyCursor";
        canLock = 0;
        autoFire = 1;
        magazineReloadTime = 5;
        magazines[] = {"Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets"};
        ballisticsComputer = 2;
        modes[] = {"Burst1","Burst2","Burst3"};
        class Burst1: RocketPods
        {
            displayName = "Close";
            sounds[] = {"StandardSound"};
            class StandardSound
            {
                weaponSoundEffect = "";
                begin1[] = {"\Aux501\Vehicles\Artillery\Hailfire\sounds\katyusha_firing.wss",+15db,1,2400};
                soundBegin[] = {"begin1",1};
            };
            soundBurst = "false";
            autoFire = 0;
            burst = 30;
            aiBurstTerminable = 1;
            reloadTime = 0.231;
            dispersion = 0.05;
            minRange = 200;
            minRangeProbab = 0.15;
            midRange = 500;
            midRangeProbab = 0.65;
            maxRange = 800;
            maxRangeProbab = 0.05;
            artilleryDispersion = 5;
            artilleryCharge = 0.4;
        };
        class Burst2: Burst1
        {
            displayName = "Medium";
            minRange = 1000;
            minRangeProbab = 0.15;
            midRange = 2300;
            midRangeProbab = 0.55;
            maxRange = 3000;
            maxRangeProbab = 0.05;
            artilleryDispersion = 10;
            artilleryCharge = 0.5;
        };
        class Burst3: Burst1
        {
            displayName = "Far";
            minRange = 2000;
            minRangeProbab = 0.15;
            midRange = 3000;
            midRangeProbab = 0.55;
            maxRange = 4000;
            maxRangeProbab = 0.05;
            artilleryDispersion = 15;
            artilleryCharge = 0.6;
        };
        class GunClouds{};
        class GunParticles{};
    };
};

class CfgMagazines
{
    class VehicleMagazine;

    class Aux501_Vehicle_Mag_30Rnd_Hailfire_Rockets: VehicleMagazine
    {
        scope = 1;
        author = "501st Aux Team";
        displayName = "30rnd IG-227 HE Rockets";
        count = 30;
        nameSound = "rockets";
        ammo = "Aux501_Vehicles_Hailfire_230mm_Ammo";
        initSpeed = 340;
        maxLeadSpeed = 350;
    };
};

class CfgAmmo
{
    class SubmunitionBase;
    class ShellBase;

    class Aux501_Vehicles_Hailfire_230mm_Ammo: SubmunitionBase
    {
        artilleryLock = 1;
        warheadName = "Hailfire Artillery Rocket";
        submunitionAmmo = "Aux501_Vehicles_Hailfire_230mm_Ammo_Sub";
        effectFly = "Aux501_particle_effect_hailfire_missile_effect";
        effectsMissile = "Aux501_particle_effect_hailfire_missile_effect";
        effectsMissileInit = "";
        tracerScale = 4;
        hit = 300;
        cost = 30;
        triggerDistance = 500;
        airFriction = 0;
        muzzleEffect = "";
        allowAgainstInfantry = 1;
        class CamShakeExplode
        {
            power = 46;
            duration = 3;
            frequency = 20;
            distance = 361.326;
        };
        class CamShakeHit
        {
            power = 230;
            duration = 0.8;
            frequency = 20;
            distance = 1;
        };
        class CamShakeFire
        {
            power = 3.89432;
            duration = 3;
            frequency = 20;
            distance = 121.326;
        };
        class CamShakePlayerFire
        {
            power = 5;
            duration = 0.1;
            frequency = 20;
            distance = 1;
        };
        whistleDist = 700;
        soundFly[] = {"\ls_sounds\rockets\hailfire_fly.wss",3,1.9,2000};
    };
    class Aux501_Vehicles_Hailfire_230mm_Ammo_Sub: ShellBase
    {
        artilleryLock = 1;
        warheadName = "Hailfire Artillery Rocket";
        allowAgainstInfantry = 1;
        hit = 100;
        indirectHit = 100;
        indirectHitRange = 3.5;
        explosive = 0.8;
        cost = 30;
        audibleFire = 64;
        deflecting = 0;
        airFriction = 0;
        muzzleEffect = "";
        effectFly = "Aux501_particle_effect_hailfire_missile_effect";
        CraterEffects = "ArtyShellCrater";
        ExplosionEffects = "Aux501_particle_effect_hailfire_missile_Explode";
        effectsMissile = "Aux501_particle_effect_hailfire_missile_effect";
        effectsMissileInit = "Aux501_particle_effect_hailfire_missile_effect";
        effectsFire = "CannonFire";
        tracerScale = 4;
        class CamShakeExplode
        {
            power = 46;
            duration = 3;
            frequency = 20;
            distance = 361.326;
        };
        class CamShakeHit
        {
            power = 230;
            duration = 0.8;
            frequency = 20;
            distance = 1;
        };
        class CamShakeFire
        {
            power = 3.89432;
            duration = 3;
            frequency = 20;
            distance = 121.326;
        };
        class CamShakePlayerFire
        {
            power = 5;
            duration = 0.1;
            frequency = 20;
            distance = 1;
        };
        soundHit1[] = {"A3\Sounds_F\weapons\Explosion\explosion_missile_1.wss",3.51189,1,2100};
        soundHit2[] = {"A3\Sounds_F\weapons\Explosion\explosion_missile_2.wss",3.51189,1,2100};
        soundHit3[] = {"A3\Sounds_F\weapons\Explosion\explosion_missile_3.wss",3.51189,1,2100};
        soundHit4[] = {"A3\Sounds_F\weapons\Explosion\explosion_missile_4.wss",3.51189,1,2100};
        soundHit5[] = {"A3\Sounds_F\weapons\Explosion\explosion_missile_5.wss",3.51189,1,2100};
        multiSoundHit[] = {"soundHit1",0.2,"soundHit2",0.2,"soundHit3",0.2,"soundHit4",0.2,"soundHit5",0.2};
        soundFly[] = {"\ls_sounds\rockets\hailfire_fly.wss",4,1,3000};
        SoundSetExplosion[] = {"Aux501_SoundSet_Hailfire_Exp","RocketsMedium_Tail_SoundSet","Explosion_Debris_SoundSet"};
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_Hailfire_closeExp
    {
        samples[] = 
        {
            {"\SWLB_core\data\sounds\vehicles\mortar\weapon\mortar_hit2.wss",0.5},
            {"\SWLB_core\data\sounds\vehicles\mortar\weapon\mortar_hit3.wss",0.5}
        };
        volume = 1.0;
        range = 250;
        rangeCurve = "CannonCloseShotCurve";
    };

    class Aux501_SoundShader_Hailfire_MidExp
    {
        samples[] = 
        {
            {"\SWLB_core\data\sounds\vehicles\mortar\weapon\mortar_hit2.wss",0.5},
            {"\SWLB_core\data\sounds\vehicles\mortar\weapon\mortar_hit3.wss",0.5}
        };
        volume = 3.0;
        range = 1000;
        rangeCurve = "CannonMidShotCurve";
    };

    class Aux501_SoundShader_Hailfire_FarExp
    {
        samples[] = 
        {
            {"\Aux501\Vehicles\Artillery\Hailfire\sounds\hailfire_explode_far.wss",1}
        };
        volume = 3.1220185;
        range = 4000;
        rangeCurve[] = {{0,0},{100,0},{300,1},{4000,1}};
    };
};

class CfgSoundSets
{
    class RocketsMedium_Exp_SoundSet;

    class Aux501_SoundSet_Hailfire_Exp: RocketsMedium_Exp_SoundSet
    {
        soundShaders[] = 
        {
            "Aux501_SoundShader_Hailfire_closeExp",
            "Aux501_SoundShader_Hailfire_MidExp",
            "Aux501_SoundShader_Hailfire_FarExp"
        };
    };
};

class FX_Missile_AA;

class Aux501_particle_effect_hailfire_missile_effect: FX_Missile_AA
{
    class Aux501_FX_Hailfire_Missile_Light
    {
        simulation = "light";
        type = "Aux501_light_hailfire";
        position[] = {0, 0, 0};
        intensity = 0.01;
        interval = 1;
        lifeTime = 60;
    };
    class Aux501_FX_Hailfire_Missile_Smoke
    {
        simulation = "particles";
        type = "Aux501_cloudlet_hailfire_smoke";
        position[] = {0, 0, 0};
        intensity = 1;
        interval = 1;
        qualityLevel = -1;
        lifeTime = 60;
    };
};

class Aux501_particle_effect_hailfire_missile_Explode
{
    class blast_wave_refract
    {
        simulation = "particles";
        type = "blast_wave_effect_medium";
        position[] = {0, 0, 0};
        intensity = 1;
        interval = 3 * 1;
        lifeTime = 1;
    };
};

class CfgCloudlets
{
    class Missile3;

    class Aux501_cloudlet_hailfire_smoke: Missile3
    {
        color[] = {{0,0,0,0.5},{0.08,0.08,0.08,0.3},{0.08,0.08,0.08,0.3}};
        size[]={3,6,12};
        lifeTime = 40;
        interval = 0.0018;
    };
};

class CfgLights
{
    class RocketLight;

    class Aux501_light_hailfire: RocketLight
    {
        color[] = {0.941,0.443,1};
        diffuse[] = {0.941,0.443,1};
        intensity = 1e8;
        dayLight = 1;
        useFlare = 1;
        flareSize = 1.5;
        flareMaxDistance = 6000;
    };
};