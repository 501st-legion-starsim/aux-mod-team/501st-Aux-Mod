class CfgPatches
{
    class Aux501_Patch_Vehicles_Artillery
    {
        addonRootClass = "Aux501_Patch_Vehicles";
        requiredAddons[] = 
        {
            "A3_Weapons_F",
            "A3_Armor_F_Beta"
        };
        units[] = {};
        weapons[] = {};
        magazines[] = {};
        ammo[] = {};
    };
};

class CfgCloudlets
{
    class Default;

    class Aux501_cloudlet_blast_wave_effect: Default
	{
		angleVar = 1;
		animationName = "";
		animationSpeed[] = {1};
		beforeDestroyScript = "";
		circleRadius = 0;
		circleVelocity[] = {0, 0, 0};
		colorVar[] = {0, 0, 0, 0};
		color[] = {{0.1, 0.1, 0.1, 1*4*2}, {0.25, 0.25, 0.25, 1*4}, {0.5, 0.5, 0.5, 1*4}, {0, 0, 0, 1*4}, {0, 0, 0, 0.5*4}, {0, 0, 0, 0.3*4}};
		interval = 3 * 10;
		lifeTime = 0.4;
		lifeTimeVar = 0;
		MoveVelocityVar[] = {0.2, 0.5, 0.2};
		moveVelocity[] = {0, 0, 0};
		onTimerScript = "";
		particleFSFrameCount = 1;
		particleFSIndex = 0;
		particleFSLoop = 0;
		particleFSNtieth = 1;
		particleShape = "\A3\data_f\ParticleEffects\Universal\refract";
		particleType = "Billboard";
		positionVar[] = {0.40000001, 0.1, 0.40000001};
		randomDirectionIntensity = 0.2;
		randomDirectionIntensityVar = 0;
		randomDirectionPeriod = 0.2;
		randomDirectionPeriodVar = 0;
		rotationVelocity = 0;
		rotationVelocityVar = 90;
		rubbing = 0.1;
		sizeVar = 0.5;
		Size[] = {1, 2, 5, 8, 12, 17};
		timerPeriod = 1;
		volume = 7.9000001;
		weight = 10;
	};
	class Aux501_cloudlet_blast_wave_effect_small: Aux501_cloudlet_blast_wave_effect
	{
		lifeTime = 0.6;
		Size[] = {1*0.25, 2*0.25, 5*0.25, 8*0.25, 12*0.25, 17*0.25};//{1, 2, 5, 8, 12, 17};
	};
	class Aux501_cloudlet_blast_wave_effect_medium: Aux501_cloudlet_blast_wave_effect
	{
		lifeTime = 0.6;
		Size[] = {1*0.25, 8*0.25, 20*0.25, 35*0.25, 50*0.25, 70*0.25};
	};
	class Aux501_cloudlet_blast_wave_effect_large: Aux501_cloudlet_blast_wave_effect
	{
		lifeTime = 0.6;
		Size[] = {1*0.25, 16*0.25, 40*0.25, 70*0.25, 100*0.25, 140*0.25};
	};
	class Aux501_cloudlet_blast_wave_effect_very_large: Aux501_cloudlet_blast_wave_effect
	{
		lifeTime = 1.0;
		Size[] = {1*0.25, 35*0.25, 80*0.25, 140*0.25, 200*0.25, 280*0.25};
	};
};