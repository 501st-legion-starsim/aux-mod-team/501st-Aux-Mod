class CfgPatches
{
    class Aux501_Patch_Vehicles_Mortars_CIS_60mw
    {
        addonRootClass = "Aux501_Patch_Vehicles";
        requiredAddons[] = 
        {
            "A3_Static_F_Mortar_01",
            "Aux501_Patch_Vehicles_Mortars_Republic_81mw"
        };
        units[] = 
        {
            "Aux501_Vehicles_Mortars_CIS_60mw_plasma_mortar_base",
            "Aux501_Vehicles_Mortars_CIS_60mw_plasma_mortar"
        };
        weapons[] = 
        {
            "Aux501_Weaps_60mw_plasma_Mortar"
        };
        magazines[] = 
        {
            "Aux501_Vehicle_Mags_20rnd_60mw_mortar_HE",
            "Aux501_Vehicle_Mags_20rnd_60mw_smoke",
            "Aux501_Vehicle_Mags_20rnd_60mw_illum"
        };
        ammo[] = 
        {
            "Aux501_Vehicle_Ammo_Mortars_60mw_HE",
            "Aux501_Vehicle_Ammo_Mortars_60mw_flare"
        };
    };
};

class Mode_Burst;

class CfgVehicles
{
    class StaticMortar;

    class Mortar_01_base_F: StaticMortar
    {
        class Turrets;
    };
    class B_Mortar_01_F: Mortar_01_base_F
    {
        class Turrets: Turrets
        {
            class MainTurret;
        };
        class assembleInfo;
    };

    class Aux501_Vehicles_Mortars_60mw_plasma_mortar_base: B_Mortar_01_F
    {   
        scope = 1;
        scopeCurator = 0;
        side = 0;
        displayname = "60mw Plasma Mortar";
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "Aux501_Editor_Subcategory_Artillery";
        editorPreview = "\3as\3as_static\images\3as_CIS_Mortar.jpg";
        icon = "3AS\3as_static\Mortar\Data\ui\Mortar_top_ca.paa";
        model = "3AS\3as_static\Mortar\model\republicmortar.p3d";
        hiddenSelections[] = {"Camo_1","Camo_2"};
        hiddenSelectionsTextures[] = {"\3as\3as_static\Mortar\data\cis_base_co.paa","\3as\3as_static\Mortar\data\cis_tube_co.paa"};
        hiddenSelectionsMaterials[] = {"\3as\3as_static\Mortar\data\base_cis.rvmat","\3as\3as_static\Mortar\data\tube_cis.rvmat"};
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                turretInfoType = "RscWeaponRangeArtillery";
                gunnerforceoptics = 1;
                initelev = 0;
                maxelev = 40;
                minelev = -15;
                initturn = 0;
                maxturn = 360;
                minturn = -360;
                weapons[] = {"Aux501_Vehicle_Weapons_Mortars_60mw_Plasma"};
                magazines[] = 
                {
                    "Aux501_Vehicle_Mags_20rnd_60mw_mortar_HE",
                    "Aux501_Vehicle_Mags_20rnd_60mw_mortar_HE",
                    "Aux501_Vehicle_Mags_20rnd_60mw_mortar_HE",
                    "Aux501_Vehicle_Mags_20rnd_60mw_mortar_HE",
                    "Aux501_Vehicle_Mags_20rnd_60mw_mortar_HE",
                    "Aux501_Vehicle_Mags_20rnd_60mw_mortar_HE",
                    "Aux501_Vehicle_Mags_20rnd_60mw_illum",
                    "Aux501_Vehicle_Mags_20rnd_60mw_illum",
                    "Aux501_Vehicle_Mags_20rnd_60mw_smoke",
                    "Aux501_Vehicle_Mags_20rnd_60mw_smoke"
                };
                class Viewoptics
                {
                    initAngleX = 0;
                    initAngleY = 0;
                    initFov = 0.75;
                    maxAngleX = 30;
                    maxAngleY = 100;
                    maxFov = 1.1;
                    maxMoveX = 0;
                    maxMoveY = 0;
                    maxMoveZ = 0;
                    minAngleX = -30;
                    minAngleY = -100;
                    minFov = 0.0125;
                    minMoveX = 0;
                    minMoveY = 0;
                    minMoveZ = 0;
                    opticsZoomInit = 0.75;
                    opticsZoomMax = 0.75;
                    opticsZoomMin = 0.25;
                    thermalMode[] = {6};
                    turretInfoType = "RscWeaponRangeArtillery";
                    visionMode[] = {"Normal","NVG","Ti"};
                };
            };
        };
        class Damage
        {
            tex[] = {};
            mat[] = 
            {
                "3as\3as_static\mortar\data\base_cis.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_01_damage.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_01_destruct.rvmat",
                "3as\3as_static\mortar\data\tube_cis.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_02_damage.rvmat",
                "A3\Static_F_Gamma\data\StaticTurret_02_destruct.rvmat"
            };
        };
        class assembleInfo: assembleInfo
        {
            dissasembleTo[] = {};
        };
    };

    class Aux501_Vehicles_Mortars_60mw_plasma_mortar: Aux501_Vehicles_Mortars_60mw_plasma_mortar_base
    {
        scope = 2;
        scopecurator = 2;
        crew = "Aux501_Units_CIS_B1_Unit";
    };
};

class CfgWeapons
{
    class Aux501_Vehicle_Weapons_Mortars_81mw_Plasma;
    
    class Aux501_Vehicle_Weapons_Mortars_60mw_Plasma: Aux501_Vehicle_Weapons_Mortars_81mw_Plasma
    {
        displayname = "60mw Plasma Mortar";
        magazines[] =
        {
            "Aux501_Vehicle_Mags_20rnd_60mw_mortar_HE",
            "Aux501_Vehicle_Mags_20rnd_60mw_smoke",
            "Aux501_Vehicle_Mags_20rnd_60mw_illum"
        };
        class Burst1: Mode_Burst
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_82mm_Burst10";
            sounds[] = {"StandardSound"};
            class StandardSound
            {
                begin1[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot1.wss",db+5,1,2200};
                begin2[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot2.wss",db+5,1,2200};
                begin3[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot3.wss",db+5,1,2200};
                begin4[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot4.wss",db+5,1,2200};
                begin5[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot5.wss",db+5,1,2200};
                soundBegin[] = {"begin1",0.15,"begin2",0.25,"begin3",0.1,"begin4",0.3,"begin5",0.2};
            };
            reloadSound[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_reload.wss",1,1,20};
            soundServo[] = {"",0.0001,1};
            soundBurst = 0;
            burst = 4;
            aiBurstTerminable = 1;
            reloadTime = 3;
            dispersion = 0.02;
            artilleryDispersion = 2;
            artilleryCharge = 0.01;
            minRange = 15;
            minRangeProbab = 0.5;
            midRange = 290;
            midRangeProbab = 0.7;
            maxRange = 665;
            maxRangeProbab = 0.5;
        };
        class Burst2: Burst1
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_82mm_Burst20";
            artilleryCharge = 0.7;
            minRange = 15;
            minRangeProbab = 0.4;
            midRange = 1175;
            midRangeProbab = 0.6;
            maxRange = 2660;
            maxRangeProbab = 0.4;
        };
        class Burst3: Burst1
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_82mm_Burst30";
            artilleryCharge = 1;
            artilleryDispersion = 15;
            minRange = 540;
            minRangeProbab = 0.3;
            midRange = 2355;
            midRangeProbab = 0.4;
            maxRange = 5500;
            maxRangeProbab = 0.3;
        };
    };
};

class CfgMagazines
{
    class Aux501_Vehicle_Mags_81mw_mortar_HE;
    class Aux501_Vehicle_Mags_81mw_mortar_smoke;
    class Aux501_Vehicle_Mags_81mw_mortar_illum;

    class Aux501_Vehicle_Mags_20rnd_60mw_mortar_HE: Aux501_Vehicle_Mags_81mw_mortar_HE
    {
        displayName = "20rnd 60mw HE";
        displayNameShort = "60mw HE";
        ammo = "Aux501_Vehicle_Ammo_Mortars_60mw_HE";
        count = 20;
    };
    class Aux501_Vehicle_Mags_20rnd_60mw_smoke: Aux501_Vehicle_Mags_81mw_mortar_smoke
    {
        displayName = "20rnd 60mw Smoke";
        displayNameShort = "60mw Smoke";
        count = 20;
    };
    class Aux501_Vehicle_Mags_20rnd_60mw_illum: Aux501_Vehicle_Mags_81mw_mortar_illum
    {
        displayName = "20rnd 60mw Illum";
        displayNameShort = "60mw Illum";
        ammo = "Aux501_Vehicle_Ammo_Mortars_60mw_flare";
        count = 20;
    };
};

class CfgAmmo
{
    class Aux501_Vehicle_Ammo_Mortars_81mw_HE;
    class Aux501_Vehicle_Ammo_Mortars_81mw_flare;
    
    class Aux501_Vehicle_Ammo_Mortars_60mw_HE: Aux501_Vehicle_Ammo_Mortars_81mw_HE
    {
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Red.p3d";
        lightcolor[] = {0.5,0.25,0.25};
        effectFly = "Aux501_particle_effect_artillery_fly_red";
        intensity = 10000;
        tracerScale = 5;
        hit = 100;
        indirectHit = 40;
        indirectHitRange = 15;
        cost = 100;
        dangerRadiusHit = 70;
        suppressionRadiusHit = 50;
        aiAmmoUsageFlags = "64 + 128 + 512";
        soundFly[] = {"\SWLB_core\data\sounds\vehicles\mortar\weapon\mortar_fly.wss",3,1,2000};
    };
    class Aux501_Vehicle_Ammo_Mortars_60mw_flare: Aux501_Vehicle_Ammo_Mortars_81mw_flare
    {
        effectFly = "Aux501_particle_effect_heavybolt_fly_red";
        model = "\Aux501\Weapons\Ammo\data\Aux501_Weapons_Ammo_Tracers_Big_Red.p3d";
        lightcolor[] = {0.5,0.25,0.25};
    };
};