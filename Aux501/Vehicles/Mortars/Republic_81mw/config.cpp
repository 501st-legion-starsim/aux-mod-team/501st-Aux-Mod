class CfgPatches
{
    class Aux501_Patch_Vehicles_Mortars_Republic_81mw
    {
        addonRootClass = "Aux501_Patch_Vehicles";
        requiredAddons[] = 
        {
            "A3_Weapons_F",
            "A3_Data_F",
            "A3_Static_F_Mortar_01",
            "ace_csw",
            "ace_interaction"
        };
        units[] = 
        {
            "Aux501_Vehicles_Mortars_81mw_plasma_mortar"
        };
        weapons[] = 
        {
            "Aux501_Vehicle_Weapons_Mortars_81mw_Plasma"
        };
        magazines[] = 
        {
            "Aux501_Vehicle_Mags_81mw_mortar_HE",
            "Aux501_Vehicle_Mags_81mw_mortar_smoke",
            "Aux501_Vehicle_Mags_81mw_mortar_illum"
        };
        ammo[] = 
        {
            "Aux501_Vehicle_Ammo_Mortars_81mw_HE",
            "Aux501_Vehicle_Ammo_Mortars_81mw_flare",
            "Aux501_Vehicle_Ammo_Mortars_81mw_smoke",
            "Aux501_Vehicle_subAmmo_smokeshell_arty"
        };
    };
};

class Mode_SemiAuto;
class Mode_Burst;

class CfgVehicles
{
    class StaticWeapon;
    class StaticMortar: StaticWeapon
    {
        class Turrets;
        class ACE_Actions;
    };
    class Mortar_01_base_F: StaticMortar
    {
        class Turrets: Turrets
        {
            class MainTurret;
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions;
        };
        class assembleInfo;
    };

    class Aux501_Vehicles_Mortars_81mw_plasma_mortar: Mortar_01_base_F
    {   
        scope = 2;
        scopeCurator = 2;
        author = "501st Aux Team";
        displayname = "81mw Plasma Mortar";
        side = 1;
        faction = "Aux501_FactionClasses_Republic";
        editorCategory = "Aux501_Editor_Category_Republic";
        editorSubcategory = "Aux501_Editor_Subcategory_Artillery";
        crew = "Aux501_Units_Republic_501st_Trooper_Unit";
        model = "\OPTRE_Weapons_Turrets\AU_44_Mortar\AU_44_Mortar.p3d";
        hiddenSelections[] = {"camo","camoScreen"};
        hiddenSelectionsTextures[] = 
        {
            "\Aux501\Vehicles\Mortars\Republic_81mw\textures\Aux501_81mw_Mortar_co.paa",
            "OPTRE_Weapons_Turrets\au_44_mortar\data\computer_screen.paa"
        };
        hiddenSelectionsMaterials[] = 
        {
            "OPTRE_Weapons_Turrets\AU_44_Mortar\data\mortar.rvmat",
            "OPTRE_Weapons_Turrets\AU_44_Mortar\data\mfd.rvmat"
        };
        class SimpleObject
        {
            eden = 1;
            animate[] = {{"mainturret",0},{"maingun",0},{"computerAim",0},{"computerRotate",0},{"barRotate",0}};
            hide[] = {};
            verticalOffset = 0.757;
            verticalOffsetWorld = 0.035;
            init = "''";
        };
        availableForSupportTypes[] = {"Artillery"};
        class Turrets: Turrets
        {
            class MainTurret: MainTurret
            {
                elevationMode = 3;
                weapons[] = {"Aux501_Vehicle_Weapons_Mortars_81mw_Plasma"};
                magazines[] =
                {
                    "Aux501_Vehicle_Mags_81mw_mortar_HE",
                    "Aux501_Vehicle_Mags_81mw_mortar_HE",
                    "Aux501_Vehicle_Mags_81mw_mortar_HE",
                    "Aux501_Vehicle_Mags_81mw_mortar_smoke",
                    "Aux501_Vehicle_Mags_81mw_mortar_smoke",
                    "Aux501_Vehicle_Mags_81mw_mortar_illum"
                };
                minelev = -22.5;
                initelev = 0;
                maxelev = 22.5;
                maxHorizontalRotSpeed = 3;
                maxVerticalRotSpeed = 3;
                disableSoundAttenuation = 0;
                soundElevation[] = {"A3\Sounds_F\vehicles\soft\noises\servo_turret_MRAP03",0.316228,1,10};
                soundServo[] = {"A3\Sounds_F\vehicles\soft\noises\servo_turret_MRAP03",0.177828,1,10};
                soundServoVertical[] = {"A3\Sounds_F\vehicles\soft\noises\servo_turret_MRAP03",0.316228,1,30};
            };
        };
        class AnimationSources
        {
            class muzzle_hide_mortar
            {
                source = "reload";
                weapon = "Aux501_Vehicle_Weapons_Mortars_81mw_Plasma";
            };
        };
        class ACE_Actions: ACE_Actions
        {
            class ACE_MainActions: ACE_MainActions
            {
                position = "";
                selection = "konec hlavne";
            };
        };
        class ACE_CSW
        {
            enabled = 1;
            proxyWeapon = "Aux501_Vehicle_Weapons_Mortars_81mw_Plasma";
            magazineLocation = "_target selectionPosition 'usti hlavne'";
            disassembleWeapon = "Aux501_Weaps_81mm_plasma_Mortar_Carry";
            disassembleTurret = "";
            ammoLoadTime = 1;
            ammoUnloadTime = 1;
            desiredAmmo = 6;
        };
        class MFD
        {
            class MFD_Gunner_Ready_To_Fire
            {
                topLeft = "mfd_rtf_cli_TL";
                topRight = "mfd_rtf_cli_TR";
                bottomLeft = "mfd_rtf_cli_BL";
                borderLeft = 0;
                borderRight = 0;
                borderTop = 0;
                borderBottom = 0;
                color[] = {0,0,0};
                alpha = 0.5;
                enableParallax = 0;
                font = "LCD14";
                class material
                {
                    ambient[] = {1,1,1,1};
                    diffuse[] = {1,1,1,1};
                    emissive[] = {1000,1000,1000,1};
                };
                class Bones{};
                class Draw
                {
                    color[] = {0,0,0};
                    alpha = 1;
                    condition = "1";
                    class Top_text
                    {
                        type = "text";
                        source = "static";
                        text = "READY TO FIRE";
                        sourceScale = 1;
                        sourceLength = 3;
                        scale = 1;
                        align = "center";
                        refreshRate = 0.1;
                        pos[] = {{0.45,0.2},1};
                        right[] = {{0.59,0.2},1};
                        down[] = {{0.45,0.9},1};
                    };
                };
            };
            class MFD_Mag_Name
            {
                topLeft = "mfd_mag_cli_tl";
                topRight = "mfd_mag_cli_tr";
                bottomLeft = "mfd_mag_cli_bl";
                borderLeft = 0;
                borderRight = 0;
                borderTop = 0;
                borderBottom = 0;
                color[] = {0.84,0.86,0.84};
                alpha = 0.5;
                turret[] = {0};
                enableParallax = 0;
                font = "EtelkaMonospaceProBold";
                class material
                {
                    ambient[] = {1,1,1,1};
                    diffuse[] = {1,1,1,1};
                    emissive[] = {1000,1000,1000,1};
                };
                class Bones{};
                class Draw
                {
                    color[] = {1,1,1};
                    alpha = 1;
                    condition = "1";
                    class Gunner_AmmoType
                    {
                        type = "text";
                        source = "ammoFormat";
                        sourceScale = 1;
                        sourceLength = 3;
                        scale = 15;
                        align = "right";
                        refreshRate = 0.1;
                        pos[] = {{0.1,0.15},1};
                        right[] = {{0.25,0.15},1};
                        down[] = {{0.1,0.3},1};
                    };
                    class Gunner_Azimuth
                    {
                        type = "text";
                        source = "static";
                        text = "AZIMUTH";
                        sourceScale = 1;
                        sourceLength = 3;
                        scale = 1;
                        align = "right";
                        refreshRate = 0.1;
                        pos[] = {{0.1,0.3},1};
                        right[] = {{0.25,0.3},1};
                        down[] = {{0.1,0.45},1};
                    };
                    class Gunner_Elevation
                    {
                        type = "text";
                        source = "static";
                        text = "ELEVATION";
                        sourceScale = 1;
                        sourceLength = 3;
                        scale = 1;
                        align = "right";
                        refreshRate = 0.1;
                        pos[] = {{0.1,0.45},1};
                        right[] = {{0.25,0.45},1};
                        down[] = {{0.1,0.6},1};
                    };
                };
            };
            class MFD_Mag_Ammo_Count
            {
                topLeft = "mfd_ammocount_cli_tl";
                topRight = "mfd_ammocount_cli_tr";
                bottomLeft = "mfd_ammocount_cli_bl";
                borderLeft = 0;
                borderRight = 0;
                borderTop = 0;
                borderBottom = 0;
                color[] = {0.84,0.86,0.84};
                alpha = 0.5;
                enableParallax = 0;
                turret[] = {0};
                font = "EtelkaMonospaceProBold";
                class material
                {
                    ambient[] = {1,1,1,1};
                    diffuse[] = {1,1,1,1};
                    emissive[] = {1000,1000,1000,1};
                };
                class Bones{};
                class Draw
                {
                    color[] = {1,1,1};
                    alpha = 1;
                    condition = "1";
                    class Gunner_Ammo
                    {
                        type = "text";
                        source = "ammo";
                        sourceScale = 1;
                        scale = 1;
                        align = "left";
                        refreshRate = 0.1;
                        pos[] = {{0.7,0.15},1};
                        right[] = {{0.85,0.15},1};
                        down[] = {{0.7,0.3},1};
                    };
                };
            };
            class MFD_Azimuth_And_Elevation
            {
                topLeft = "mfd_ammocount_cli_tl";
                topRight = "mfd_ammocount_cli_tr";
                bottomLeft = "mfd_ammocount_cli_bl";
                borderLeft = 0;
                borderRight = 0;
                borderTop = 0;
                borderBottom = 0;
                color[] = {0.84,0.86,0.84};
                alpha = 0.5;
                enableParallax = 0;
                font = "EtelkaMonospaceProBold";
                turret[] = {0};
                class Bones{};
                class material
                {
                    ambient[] = {1,1,1,1};
                    diffuse[] = {1,1,1,1};
                    emissive[] = {1000,1000,1000,1};
                };
                class Draw
                {
                    color[] = {0.61,0.62,0};
                    alpha = 1;
                    condition = "1";
                    class Azimuth
                    {
                        type = "text";
                        source = "[x]turretworld";
                        scale = 1;
                        sourceScale = 1;
                        sourcePrecision = 1;
                        align = "left";
                        pos[] = {{0.7,0.3},1};
                        right[] = {{0.85,0.3},1};
                        down[] = {{0.7,0.45},1};
                    };
                    class AzimuthSymbol: Azimuth
                    {
                        type = "text";
                        source = "static";
                        text = "°";
                        pos[] = {{0.8,0.3},1};
                        right[] = {{0.95,0.3},1};
                        down[] = {{0.8,0.45},1};
                    };
                    class Elevation
                    {
                        type = "text";
                        source = "[y]turretworld";
                        scale = 2;
                        sourceScale = 1;
                        sourcePrecision = 2;
                        align = "left";
                        pos[] = {{0.7,0.45},1};
                        right[] = {{0.85,0.45},1};
                        down[] = {{0.7,0.6},1};
                    };
                    class ElevationSymbol: Elevation
                    {
                        type = "text";
                        source = "static";
                        text = "°";
                        pos[] = {{0.8,0.45},1};
                        right[] = {{0.95,0.45},1};
                        down[] = {{0.8,0.6},1};
                    };
                };
            };
        };
        class assembleInfo: assembleInfo
        {
            dissasembleTo[] = {};
        };
    };
};

class CfgWeapons
{
    class mortar_82mm;
    
    class Aux501_Vehicle_Weapons_Mortars_81mw_Plasma: mortar_82mm
    {
        displayname = "81mw Plasma Mortar";
        author = "501st Aux Team";
        nameSound = "CannonCore";
        cursor = "mortar";
        cursorAim = "EmptyCursor";
        sounds[] = {"StandardSound"};
        ballisticsComputer = 8;
        class StandardSound
        {
            begin1[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot1.wss",db+5,1,2200};
            begin2[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot2.wss",db+5,1,2200};
            begin3[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot3.wss",db+5,1,2200};
            begin4[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot4.wss",db+5,1,2200};
            begin5[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot5.wss",db+5,1,2200};
            soundBegin[] = {"begin1",0.15,"begin2",0.25,"begin3",0.1,"begin4",0.3,"begin5",0.2};
        };
        reloadSound[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_reload.wss",1,1,20};
        reloadMagazineSound[] = {"A3\Sounds_F\arsenal\weapons_static\Mortar\reload_magazine_Mortar",1,1,20};
        soundServo[] = {"",0.0001,1};
        reloadTime = 1.8;
        magazineReloadTime = 2;
        autoReload = 1;
        canLock = 0;
        magazines[] =
        {
            "Aux501_Vehicle_Mags_81mw_mortar_HE",
            "Aux501_Vehicle_Mags_81mw_mortar_smoke",
            "Aux501_Vehicle_Mags_81mw_mortar_illum"
        };
        class GunParticles
        {
            class FirstEffect
            {
                effectName = "MortarFired";
                positionName = "Usti Hlavne";
                directionName = "Konec Hlavne";
            };
        };
        modes[] = {"Single1","Single2","Single3","Burst1","Burst2","Burst3"};
        class Single1: Mode_SemiAuto
        {
            displayName = "$STR_A3_mortar_82mm_Single10";
            sounds[] = {"StandardSound"};
            class StandardSound
            {
                begin1[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot1.wss",db+5,1,2200};
                begin2[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot2.wss",db+5,1,2200};
                begin3[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot3.wss",db+5,1,2200};
                begin4[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot4.wss",db+5,1,2200};
                begin5[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot5.wss",db+5,1,2200};
                soundBegin[] = {"begin1",0.15,"begin2",0.25,"begin3",0.1,"begin4",0.3,"begin5",0.2};
            };
            reloadSound[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_reload.wss",1,1,20};
            reloadTime = 1.8;
            artilleryDispersion = 1.9;
            artilleryCharge = 0.35;
            aiRateOfFire = 1;
            aiRateOfFireDistance = 10;
            minRange = 0;
            minRangeProbab = 0.01;
            midRange = 1;
            midRangeProbab = 0.01;
            maxRange = 2;
            maxRangeProbab = 0.01;
        };
        class Single2: Single1
        {
            displayName = "$STR_A3_mortar_82mm_Single20";
            artilleryCharge = 0.7;
        };
        class Single3: Single1
        {
            displayName = "$STR_A3_mortar_82mm_Single30";
            artilleryCharge = 1;
        };
        class Burst1: Mode_Burst
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_82mm_Burst10";
            sounds[] = {"StandardSound"};
            class StandardSound
            {
                begin1[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot1.wss",db+5,1,2200};
                begin2[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot2.wss",db+5,1,2200};
                begin3[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot3.wss",db+5,1,2200};
                begin4[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot4.wss",db+5,1,2200};
                begin5[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_shot5.wss",db+5,1,2200};
                soundBegin[] = {"begin1",0.15,"begin2",0.25,"begin3",0.1,"begin4",0.3,"begin5",0.2};
            };
            reloadSound[] = {"swlb_core\data\sounds\vehicles\mortar\weapon\mortar_reload.wss",1,1,20};
            soundServo[] = {"",0.0001,1};
            soundBurst = 0;
            burst = 4;
            aiBurstTerminable = 1;
            reloadTime = 1.8;
            artilleryDispersion = 2.2;
            artilleryCharge = 0.01;
            minRange = 60;
            minRangeProbab = 0.5;
            midRange = 290;
            midRangeProbab = 0.7;
            maxRange = 665;
            maxRangeProbab = 0.5;
        };
        class Burst2: Burst1
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_82mm_Burst20";
            artilleryCharge = 0.7;
            minRange = 230;
            minRangeProbab = 0.4;
            midRange = 1175;
            midRangeProbab = 0.6;
            maxRange = 2660;
            maxRangeProbab = 0.4;
        };
        class Burst3: Burst1
        {
            showToPlayer = 0;
            displayName = "$STR_A3_mortar_82mm_Burst30";
            artilleryCharge = 1;
            minRange = 540;
            minRangeProbab = 0.3;
            midRange = 2355;
            midRangeProbab = 0.4;
            maxRange = 5500;
            maxRangeProbab = 0.3;
        };
    };
};

class CfgMagazines
{
    class 8Rnd_82mm_Mo_shells;
    class 8Rnd_82mm_Mo_Smoke_white;
    class 8Rnd_82mm_Mo_Flare_white;

    class Aux501_Vehicle_Mags_81mw_mortar_HE: 8Rnd_82mm_Mo_shells
    {
        author = "501st Aux Team";
        displayName = "6rnd 81mw HE Rounds";
        displayNameShort = "81mw HE";
        displayNameMFDFormat = "HE";
        ammo = "Aux501_Vehicle_Ammo_Mortars_81mw_HE";
        count = 6;
    };
    class Aux501_Vehicle_Mags_81mw_mortar_smoke: 8Rnd_82mm_Mo_Smoke_white
    {
        author = "501st Aux Team";
        displayName = "5rnd 81mw Smoke Rounds";
        displayNameShort = "81mw Smoke";
        displayNameMFDFormat = "Smoke";
        ammo = "Aux501_Vehicle_Ammo_Mortars_81mw_smoke";
        count = 5;
    };
    class Aux501_Vehicle_Mags_81mw_mortar_illum: 8Rnd_82mm_Mo_Flare_white
    {
        author = "501st Aux Team";
        displayName = "3rnd 81mw Illum Rounds";
        displayNameShort = "81mw Illum";
        displayNameMFDFormat = "Illum";
        ammo = "Aux501_Vehicle_Ammo_Mortars_81mw_flare";
        count = 3;
    };
};

class CfgAmmo
{
    class Sh_82mm_AMOS;
    class Flare_82mm_AMOS_White;
    class Smoke_82mm_AMOS_White;
    class SmokeShellArty;
    
    class Aux501_Vehicle_Ammo_Mortars_81mw_HE: Sh_82mm_AMOS
    {
        effectFly = "Aux501_particle_effect_heavybolt_fly_blue";
        model = "\MRC\JLTS\weapons\Core\effects\laser_blue.p3d";
        tracerScale = 3;
        tracerStartTime = 0.1;
        whistleDist = 2000;
        whistleOnFire = 1;
        soundFly[] = {"\Aux501\Vehicles\Mortars\Republic_81mw\sounds\republic_mortar_flyby.wss",3,1,2000};
        SoundSetExplosion[] =  
        {
            "Aux501_SoundSet_Mortar_Exp",
            "Mortar_Tail_SoundSet",
            "Explosion_Debris_SoundSet"
        };
        class CamShakeExplode
        {
            power = 16.4;
            duration = 1.8;
            frequency = 20;
            distance = 216.443;
        };
        class CamShakeHit
        {
            power = 82;
            duration = 0.6;
            frequency = 20;
            distance = 1;
        };
        class CamShakeFire
        {
            power = 3.00922;
            duration = 1.8;
            frequency = 20;
            distance = 72.4431;
        };
        class CamShakePlayerFire
        {
            power = 0.01;
            duration = 0.1;
            frequency = 20;
            distance = 1;
        };
    };

    class Aux501_Vehicle_Ammo_Mortars_81mw_flare: Flare_82mm_AMOS_White
    {
        effectFly = "Aux501_particle_effect_heavybolt_fly_green";
        model = "\A3\Weapons_f\Data\bullettracer\shell_tracer_green";
        lightColor[] = {0,1,0,0};
        tracerScale = 3;
        tracerStartTime = 0.1;
        whistleDist = 2000;
        whistleOnFire = 1;
    };
    
    class Aux501_Vehicle_Ammo_Mortars_81mw_smoke: Smoke_82mm_AMOS_White
    {
        effectFly = "Aux501_particle_effect_heavybolt_fly_gray";
        tracerScale = 1;
        tracerStartTime = 0;
        whistleDist = 200;
        whistleOnFire = 1;
        submunitionAmmo = "Aux501_Vehicle_subAmmo_smokeshell_arty";
        soundFly[] = {"swlw_rework\sounds\launcher\PLX_fly.wss",3,1,2000};
    };
    class Aux501_Vehicle_subAmmo_smokeshell_arty: SmokeShellArty
    {
        smokeColor[] = {1,1,1,1};
        effectsSmoke = "k_SmokeShellwhiteEffect";
    };
};

class CfgSoundShaders
{
    class Aux501_SoundShader_Mortar_closeExp
    {
        samples[] = 
        {
            {"\SWLB_core\data\sounds\vehicles\mortar\weapon\mortar_hit1.wss",1}
        };
        volume = 1.0;
        range = 250;
        rangeCurve = "CannonCloseShotCurve";
    };
    class Aux501_SoundShader_Mortar_MideExp
    {
        samples[] = 
        {
            {"\SWLB_core\data\sounds\vehicles\mortar\weapon\mortar_hit1.wss",1}
        };
        volume = 1.0;
        range = 1000;
        rangeCurve = "CannonMidShotCurve";
    };
    class Aux501_SoundShader_Mortar_FarExp
    {
        samples[] = 
        {
            {"\Aux501\Vehicles\Mortars\Republic_81mw\sounds\mortar_explode_far.wss",1}
        };
        volume = 3.1220185;
        range = 3000;
        rangeCurve[] = {{0,0},{100,0},{300,1},{3000,1}};
    };
};

class CfgSoundSets
{
    class Mortar_Exp_SoundSet;

    class Aux501_SoundSet_Mortar_Exp: Mortar_Exp_SoundSet
    {
        soundShaders[] = 
        {
            "Aux501_SoundShader_Mortar_closeExp",
            "Aux501_SoundShader_Mortar_MideExp",
            "Aux501_SoundShader_Mortar_FarExp"
        };
    };
};

class CfgLights
{
    class Aux501_light_heavybolt_blue;
    class Aux501_light_heavybolt_gray: Aux501_light_heavybolt_blue
    {
        diffuse[] = {45,45,45,0.57};
    };
};

class Aux501_particle_effect_heavybolt_fly_gray
{
    class Light
    {
        simulation = "light";
        type = "Aux501_light_heavybolt_gray";
        position[] = {0,0,0};
    };
};