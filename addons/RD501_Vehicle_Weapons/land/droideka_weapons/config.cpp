

#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_droideka_weapons
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			macro_new_weapon(droideka,sniper),
			macro_new_weapon(droideka,dual)
		};
	};
};
class CfgWeapons
{
	class MGun;
	class LMG_RCWS: MGun
	{
		class manual: MGun{};
	};
	class macro_new_weapon(droideka,sniper): LMG_RCWS
	{
		displayName = "Heavy Sniper";
        fireLightDiffuse[] = {1,0,0};
		fireLightAmbient[] = {1,0,0};	
		class GunParticles{};
		magazines[] = 
        {
            macro_new_mag(CIS_droideka_sniper_cell,10)
		};
		class manual
		{
			reloadTime = 0.5;
			dispersion = 0.00116;
			minRange = 2;
			minRangeProbab = 0.3;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 350;
			maxRangeProbab = 0.1;
			soundContinuous = 0;
			soundBurst = 0;
            sounds[] = {"StandardSound"};
			class StandardSound
			{
				begin1[] = {"\DBA_Core\Addons\DBA_Sounds\cannon.ogg",1,1.65,2100};
				soundBegin[] = {"begin1",1};
			};
		};
		class close: manual
		{
			showToPlayer = 0;
			soundBurst = 0;
			soundContinuous = 0;
			aiRateOfFire = 0.3;
			aiRateOfFireDistance = 500;
			aiRateOfFireDispersion = 1;
			minRange = 0;
			minRangeProbab = 0.05;
			midRange = 30;
			midRangeProbab = 0.58;
			maxRange = 300;
			maxRangeProbab = 0.3;
		};
		class short: close
		{
			minRange = 150;
			midRange = 500;
			maxRange = 1500;
			aiRateOfFireDistance = 500;
			aiRateOfFireDispersion = 1;
		};
		class medium: close
		{
			minRange = 250;
			midRange = 750;
			maxRange = 1000;
			aiRateOfFireDistance = 700;
			aiRateOfFireDispersion = 1.25;
		};
		class far: close
		{
			minRange = 500;
			midRange = 1200;
			maxRange = 2100;
			aiRateOfFireDistance = 1000;
			aiRateOfFireDispersion = 1.5;
		};
    };
	class 3AS_Droideka_Repeater;
	class macro_new_weapon(droideka,dual): 3AS_Droideka_Repeater
	{
		displayName = "Dual Blaster Cannons";
		reloadTime = 0.2;
		reloadmagazinetime = 4;
		magazinereloadtime = 4;
		aiDispersionCoefY = 0.5;
		aiDispersionCoefX = 0.5;
		scope = 1;
		magazines[] = 
		{
			macro_new_mag(droideka_dual_blaster_cell,60)
		};
		class manual: Mgun
		{
			displayName = "Dual Blaster Cannons";
			sounds[] = {"StandardSound"};
			reloadTime = 0.2;
			class StandardSound
			{
				begin1[] = {"kobra\442_turrets\droideka\sounds\droidekafire1.wss",1,1,2000};
				begin2[] = {"kobra\442_turrets\droideka\sounds\droidekafire1.wss",1,1,2000};
				begin3[] = {"kobra\442_turrets\droideka\sounds\droidekafire2.wss",1,1,2000};
				begin4[] = {"kobra\442_turrets\droideka\sounds\droidekafire1.wss",1,1,2000};
				begin5[] = {"kobra\442_turrets\droideka\sounds\droidekafire2.wss",1,1,2000};
				begin6[] = {"kobra\442_turrets\droideka\sounds\droidekafire1.wss",1,1,2000};
				begin7[] = {"kobra\442_turrets\droideka\sounds\droidekafire1.wss",1,1,2000};
				begin8[] = {"kobra\442_turrets\droideka\sounds\droidekafire2.wss",1,1,2000};
				begin9[] = {"kobra\442_turrets\droideka\sounds\droidekafire1.wss",1,1,2000};
				begin10[] = {"kobra\442_turrets\droideka\sounds\droidekafire1.wss",1,1,2000};
				soundBegin[] = {"begin1",0.1,"begin2",0.1,"begin3",0.1,"begin4",0.1,"begin5",0.1,"begin6",0.1,"begin7",0.1,"begin8",0.1,"begin9",0.1,"begin10",0.1};
				class SoundTails
				{
					class TailForest
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_forest",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*forest";
					};
					class TailHouses
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_houses",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*houses";
					};
					class TailInterior
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_interior",1.9952624,1,2200};
						frequency = 1;
						volume = "interior";
					};
					class TailMeadows
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_meadows",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*(meadows/2 max sea/2)";
					};
					class TailTrees
					{
						sound[] = {"A3\Sounds_F\arsenal\weapons\LongRangeRifles\GM6_Lynx\GM6_tail_trees",1,1,2200};
						frequency = 1;
						volume = "(1-interior/1.4)*trees";
					};
				};
			};
			soundContinuous = 0;
			soundBurst = 0;
			autoFire = 1;
			dispersion = 0.0016;
			aiRateOfFire = 1;
			aiRateOfFireDistance = 10;
			minRange = 0;
			minRangeProbab = 0.01;
			midRange = 1;
			midRangeProbab = 0.01;
			maxRange = 2;
			maxRangeProbab = 0.01;
		};
		class close: manual
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 8;
			burstRangeMax = 16;
			aiRateOfFire = 0.5;
			aiRateOfFireDispersion = 2;
			aiRateOfFireDistance = 50;
			minRange = 0;
			minRangeProbab = 0.8;
			midRange = 20;
			midRangeProbab = 0.7;
			maxRange = 50;
			maxRangeProbab = 0.2;
		};
		class short: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 6;
			burstRangeMax = 12;
			aiRateOfFire = 1;
			aiRateOfFireDispersion = 2;
			aiRateOfFireDistance = 150;
			minRange = 0;
			minRangeProbab = 0.7;
			midRange = 150;
			midRangeProbab = 0.7;
			maxRange = 300;
			maxRangeProbab = 0.2;
		};
		class medium: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 3;
			burstRangeMax = 12;
			aiRateOfFire = 2;
			aiRateOfFireDispersion = 2;
			aiRateOfFireDistance = 250;
			minRange = 150;
			minRangeProbab = 0.7;
			midRange = 600;
			midRangeProbab = 0.65;
			maxRange = 800;
			maxRangeProbab = 0.1;
		};
		class far: close
		{
			aiBurstTerminable = 1;
			showToPlayer = 0;
			burst = 3;
			burstRangeMax = 8;
			aiRateOfFire = 4;
			aiRateOfFireDispersion = 4;
			aiRateOfFireDistance = 600;
			minRange = 600;
			minRangeProbab = 0.65;
			midRange = 800;
			midRangeProbab = 0.4;
			maxRange = 1200;
			maxRangeProbab = 0.1;
		};
		drySound[] = {"SWLW_clones\machineguns\z6\sounds\Z6_empty.wss",2,1,20};
	};
};
