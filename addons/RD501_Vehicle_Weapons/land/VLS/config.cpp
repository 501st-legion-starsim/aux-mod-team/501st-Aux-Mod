
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_VLS_weapons
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]={
			macro_new_weapon(MLRS,M80),
			RD501_VLS_Launcher_Weapon
		};
	};
};

class CfgWeapons
{
	//Not Used
	class OPTRE_M79_MLRS;
	class macro_new_weapon(MLRS,M80) : OPTRE_M79_MLRS
	{
		magazines[] = {
			macro_new_mag(MRLS_M80,2)
		};
	};
	
	class MissileLauncher;
	class weapon_VLSBase: MissileLauncher
	{
		class Cruise;
	};
	class RD501_VLS_Launcher_Weapon: weapon_VLSBase
	{
		displayName = "Republic Cruise Missile Weapon";
		magazines[] = {"RD501_Cruise_Missile_Magazine", "RD501_Mash_Missile_Magazine"};
		modes[] = {"Cruise"};

		class Cruise: Cruise
		{
			minRange = 1;
			maxRange = 999999;
		};

		// This copied from BI!
		class EventHandlers
		{
			fired = "_this call (uinamespace getvariable 'BIS_fnc_effectFired');";
		};

		magazineReloadTime = 0.1;
		reloadTime = 0.1;

		scope=2;
		scopeCurator=2;
	};
};
