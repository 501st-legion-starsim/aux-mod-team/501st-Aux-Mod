
#include "../../../RD501_main/config_macros.hpp"
#include "../../_common/common.hpp"


class CfgPatches
{
	class RD501_patch_dsd_weapons
	{
		author=DANKAUTHORS;
		addonRootClass= MACRO_QUOTE(RD501_patch_vehicle_weapons);
		requiredAddons[]=
		{
			RD501_patch_vehicle_weapons,
			"A3_Weapons_F"
		};
		requiredVersion=0.1;
		units[]={};
		weapons[]=
		{
			macro_new_weapon(barc,cannon)
		};
	};
};
class CfgWeapons
{
	class ls_speeder_AP;
	class macro_new_weapon(barc,cannon): ls_speeder_AP
	{
		displayName = "BARC Blaster Cannon";
		magazines[] = 
		{
			macro_new_mag(barc_he,50),
            macro_new_mag(barc_ap,50)

		};
		fireLightDiffuse[] = {1,0,0};
    };
};

