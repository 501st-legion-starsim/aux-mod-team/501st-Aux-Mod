
#include "../RD501_main/config_macros.hpp"
#include "config_macros.cpp"
#define TEXTUREAB \RD501_Helmets\_textures\airborne
class CfgPatches
{
    class RD501_patch_helmets
    {
        author=DANKAUTHORS;
        requiredAddons[]=
        {
            macro_lvl1_req,
            "RD501_patch_main"
        };
        requiredVersion=0.1;
        units[]={};
        weapons[]={};
    };
};

class CfgWeapons
{
    class HeadgearItem;
    class H_HelmetB;
    class SWLB_P2_SpecOps_Helmet;
    class 21st_clone_P2_helmet;
    class 3as_P1_Base;
    class 21st_clone_P2_ENG_helmet;
    class JLTS_CloneHelmetP2;
    class JLTS_CloneHelmetARC;
};