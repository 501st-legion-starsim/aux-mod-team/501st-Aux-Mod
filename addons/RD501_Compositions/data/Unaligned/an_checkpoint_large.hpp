class Object0	{side = 8; vehicle = "land_3AS_Imperial_Tower"; rank = ""; position[] = {0.00646973,0.381243,-4.76837e-007}; dir = 180.964;};
class Object1	{side = 8; vehicle = "k_fence_ray_stand_gate"; rank = ""; position[] = {-7.92712,0.708178,0}; dir = 0.964188;};
class Object2	{side = 8; vehicle = "land_3AS_Imperial_Tower"; rank = ""; position[] = {-15.7817,0.629862,-4.76837e-007}; dir = 180.964;};
class Object3	{side = 8; vehicle = "land_3as_Bunker_Metal"; rank = ""; position[] = {5.09399,-36.8257,3.20015}; dir = 90.9642;};
class Object4	{side = 8; vehicle = "k_fence_ray_stand_gate"; rank = ""; position[] = {-8.26672,-39,0}; dir = -179.036;};
class Object5	{side = 8; vehicle = "k_fence_ray_stand_5m"; rank = ""; position[] = {-1.42114,-39.122,0}; dir = 0.964188;};
class Object6	{side = 8; vehicle = "k_fence_ray_stand_5m"; rank = ""; position[] = {-15.1998,-38.8887,0}; dir = 180.964;};
class Object7	{side = 8; vehicle = "land_3as_Bunker_Metal"; rank = ""; position[] = {-21.6019,-36.3784,3.20488}; dir = 90.9642;};
class Object8	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {11.8777,-41.6024,0}; dir = -179.036;};
class Object9	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {-28.6854,-40.9187,0}; dir = -179.036;};
class Object10	{side = 8; vehicle = "3AS_H_barrier_small_3"; rank = ""; position[] = {-31.7534,-40.9047,0}; dir = -179.036;};
class Object11	{side = 8; vehicle = "3AS_H_barrier_5"; rank = ""; position[] = {-34.7585,-37.465,0}; dir = 90.9642;};
class Object12	{side = 8; vehicle = "3AS_H_barrier_small_3"; rank = ""; position[] = {14.9335,-41.7314,0}; dir = -179.036;};
class Object13	{side = 8; vehicle = "3AS_H_barrier_5"; rank = ""; position[] = {18.0649,-38.395,0}; dir = 90.9642;};
class Object14	{side = 8; vehicle = "3AS_Barricade_2_C_prop"; rank = ""; position[] = {-1.59973,-44.0711,0}; dir = 0.964233;};
class Object15	{side = 8; vehicle = "3AS_Barricade_2_C_prop"; rank = ""; position[] = {-15.1278,-43.8557,0}; dir = 0.964233;};
class Object16	{side = 8; vehicle = "3as_large_crate_stack_2_prop"; rank = ""; position[] = {7.52014,-21.5961,0}; dir = 90.9642;};
class Object17	{side = 8; vehicle = "3as_large_crate_stack_1_prop"; rank = ""; position[] = {-23.2382,-21.0887,0}; dir = 0.964264;};
class Object18	{side = 8; vehicle = "3AS_Barricade_prop"; rank = ""; position[] = {-1.14282,-30.299,0}; dir = -179.036;};
class Object19	{side = 8; vehicle = "3AS_Barricade_prop"; rank = ""; position[] = {-15.3503,-30.0873,0}; dir = 0.964233;};
class Object20	{side = 8; vehicle = "3AS_H_barrier_5"; rank = ""; position[] = {18.257,-26.9834,0}; dir = 90.9642;};
class Object21	{side = 8; vehicle = "3AS_H_barrier_5"; rank = ""; position[] = {18.4502,-15.4995,0}; dir = 90.9642;};
class Object22	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {10.2124,-0.706312,0}; dir = 0.964188;};
class Object23	{side = 8; vehicle = "3AS_H_barrier_5"; rank = ""; position[] = {18.6406,-4.1997,0}; dir = -89.0358;};
class Object24	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {-25.6487,-0.101791,0}; dir = 0.964188;};
class Object25	{side = 8; vehicle = "3AS_H_barrier_5"; rank = ""; position[] = {-34.1827,-3.26965,0}; dir = -89.0358;};
class Object26	{side = 8; vehicle = "3AS_H_barrier_5"; rank = ""; position[] = {-34.567,-26.2316,0}; dir = 90.9642;};
class Object27	{side = 8; vehicle = "3AS_H_barrier_5"; rank = ""; position[] = {-34.3737,-14.7472,0}; dir = 90.9642;};
class Object28	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {4.25647,-0.606562,0}; dir = 0.964218;};
class Object29	{side = 8; vehicle = "3AS_H_barrier_1"; rank = ""; position[] = {-20.0477,-0.197521,0}; dir = 0.964218;};
class Object30	{side = 8; vehicle = "3AS_H_barrier_small_5"; rank = ""; position[] = {14.4441,-0.739923,0}; dir = 0.964218;};
class Object31	{side = 8; vehicle = "3AS_H_barrier_small_5"; rank = ""; position[] = {-29.874,0.0469818,0}; dir = 0.964218;};
class Object32	{side = 8; vehicle = "3AS_Barricade_prop"; rank = ""; position[] = {-0.81958,-11.0324,0}; dir = -179.036;};
class Object33	{side = 8; vehicle = "3AS_Barricade_prop"; rank = ""; position[] = {-15.027,-10.8206,0}; dir = 0.964233;};
class Object34	{side = 8; vehicle = "3AS_Barricade_2_C_prop"; rank = ""; position[] = {-22.8202,3.42632,0}; dir = -179.036;};
class Object35	{side = 8; vehicle = "3AS_Barricade_2_C_prop"; rank = ""; position[] = {7.30115,2.91938,0}; dir = -179.036;};