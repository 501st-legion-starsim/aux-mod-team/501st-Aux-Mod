#include "../../RD501_main/config_macros.hpp"

class cfgPatches
{
    class RD501_ACE_Medical
    {
        requiredAddons[] = {"ace_medical_treatment"};
		units[]={};
		weapons[]=
        {
            "RD501_Painkiller",
            "RD501_phenylephrine_inject"
        };
    };
};
class CfgWeapons
{
    class ACE_ItemCore;
	class CBA_MiscItem_ItemInfo;
    class RD501_Painkiller: ACE_ItemCore
    {
        scope = 2;
		displayName = "Painkiller Autoinjector";
		model = "kobra\442_misc\medical\StimPerigen.p3d";
		picture = "\kobra\442_misc\medical\ui\stim_perigen_ui.paa";
		descriptionShort = "Painkillers";
		descriptionUse = "Inbuilt Clone Armor injector for easy pain relief";
		class ItemInfo: CBA_MiscItem_ItemInfo
		{
			mass = 0.1;
		};
	};
    class RD501_phenylephrine_inject: ACE_ItemCore
	{
		scope=2;
		displayName="[501st] Phenylephrine Autoinjector";
		picture="\RD501_Weapons\Consumables\data\phen.paa";
		model="\A3\Structures_F_EPA\Items\Medical\Painkillers_F.p3d";
		class ItemInfo: CBA_MiscItem_ItemInfo
		{
			mass=0.2;
		};
	};  
    class macro_new_weapon(fortify,nanobots): ACE_ItemCore
    {
        scope = 2;
        displayName = "Nanobot Charge";
        descriptionShort = "No, you can't eat that!";
        picture = "\RD501_Weapons\Consumables\data\nanobot_charge_ico.paa";
        author = "RD501";
		class ItemInfo: CBA_MiscItem_ItemInfo
		{
			mass = 1;
		};
	};
};
class ACE_Medical_Treatment
{
    class Morphine;
    
    class Medication
    {
        class RD501_Painkiller: Morphine
        {
            painReduce = 0.3;
            hrIncreaseLow[] = {-2,-4};
            hrIncreaseNormal[] = {-2,-6};
            hrIncreaseHigh[] = {-2,-7};
            timeInSystem = 600;
            timeTillMaxEffect = 30;
            maxDose = 10;
            incompatibleMedication[] = {};
            viscosityChange = -2;
        };
        class RD501_PhenylephrineInjector
		{
			painReduce=0;
			hrIncreaseLow[]={-5,-10};
			hrIncreaseNormal[]={-5,-10};
			hrIncreaseHigh[]={-10,-15};
			timeInSystem=360;
			timeTillMaxEffect=60;
			maxDose=12;
			incompatibleMedication[]={};
			viscosityChange=25;
            alphaFactor = -0.30;
		};
    };
};
class ACE_Medical_Treatment_Actions
{
    class Morphine;
    class RD501_Painkiller:Morphine
    {
        displayName = "Painkiller Autoinjector";
        displayNameProgress = "Injecting Painkillers";
        allowedSelections[] = {"LeftArm", "RightArm", "LeftLeg", "RightLeg"};
        items[] = {"RD501_Painkiller"};
        treatmentTime = 1;
    };
    class RD501_PhenylephrineInjector: Morphine
	{
		displayName="Inject Pheny Auto";
		displayNameProgress="Injecting Phenylephrine";
        allowedSelections[] = {"LeftArm", "RightArm", "LeftLeg", "RightLeg"};
		items[]= {"RD501_phenylephrine_inject"};
		callbackSuccess="[_medic, _patient, _bodyPart, _className, _itemUser, _usedItem] call ace_medical_treatment_fnc_medication;";
	};
};
