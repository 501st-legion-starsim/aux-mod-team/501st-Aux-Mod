#include "..\RD501_main\config_macros.hpp"

class CfgPatches
{
	class RD501_patch_droids_config
	{
		requiredAddons[] = {
			macro_lvl3_req
		};
		requiredVersion = 0.1;
		units[] = {
		
		};
		weapons[] = {
			
		};
	};
};

#include "droid_inits.hpp"