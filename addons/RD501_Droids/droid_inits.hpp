
class Extended_Init_EventHandlers
{
	class Aux501_Units_CIS_B1_Prototype_Unit
	{
		class apply_skills
		{
			init = [_this select 0]	spawn macro_fnc_name(b1_specop_apply_skill);
		};
	};
	class Aux501_Units_CIS_B1_Jammer_Unit
	{
		class rd501_jammer
		{
			init = "[_this select 0, 300, 160] call rd501_fnc_jammersAdd";
		};
	};
	class Aux501_Units_CIS_BX_Security_Unit
	{
		class rd501_equip_shield
		
		{
			init = "_this call rd501_fnc_jlts_shield_aiToggle";
		};
	};
};