#include "../config_macros.hpp"
#include "config_macros.hpp"
#include "../../RD501_main/config_macros.hpp"
//

#define macro_new_glass(className) CONCAT_3(MODNAME,_,className)

class CfgPatches
{
	class RD501_patch_other_units
	{
		addonRootClass=RD501_patch_units;
		requiredAddons[]=
		{
			MACRO_QUOTE(RD501_patch_units)
		};
		requiredVersion=0.1;
		units[]={
			

		};
		weapons[]=
		{
			macro_new_glass(Diving_Goggles),
			macro_new_glass(phase2_hud),
			macro_new_glass(phase2_hud_snow),
			macro_new_glass(phase1_hud),
			macro_new_glass(acklay_hud),
			macro_new_glass(dark_mask_2)
		};
	};
};

class CfgGlasses
{
	class G_B_Diving;
	class G_Diving;
	class g_balaclava_ti_blk_f;
	class g_balaclava_blk;

};