#include "../../config_macros.hpp"

#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
    class RD501_patch_clones
    {
        addonRootClass=RD501_patch_units;
        requiredAddons[]=
        {
            RD501_patch_units
        };
        requiredVersion=0.1;
        units[]=
        {
        
        };
        weapons[]=
        {
            
        };
    };
};

class cfgWeapons
{
    class Uniform_Base;
    class UniformItem;
    class V_RebreatherB;
    class VestItem;


    //uniforms
    #include "_uniforms.hpp"
    #include "_headwear.hpp"


    class JLTS_Clone_ARC_backpack;
};
class DefaultEventhandlers;
class CfgVehicles
{
    class B_Soldier_base_f;
    class B_Soldier_f: B_Soldier_base_F
    {
        class HitPoints;
    };
};