// Adds an action to an object to allow the use of cruise missile
// supports. Needs to be placed in a mission file to operate correctly.

// The object to register actions to.
params ["_object"];

_object addAction ["Get Cruise Missile Support", {
	params ["_target", "_caller", "_actionId", "_arguments"];
	[_caller, "RD501_CruiseMissileStrike"] call BIS_fnc_addCommMenuItem;
}, [], 1.5, false];

_object addAction ["Get Mash Missile Support", {
	params ["_target", "_caller", "_actionId", "_arguments"];
	[_caller, "RD501_CallMashMissile"] call BIS_fnc_addCommMenuItem;
}, [], 1.5, false];