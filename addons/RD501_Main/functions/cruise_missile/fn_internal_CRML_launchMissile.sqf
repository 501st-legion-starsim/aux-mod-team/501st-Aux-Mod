#include "..\loglevel.hh"

// Launches a cruise missle (server script).
params [
	// The caller of this event.
	"_caller",
	// The target to guide the missile to.
	"_target",
	// The type of missile. It must be a type
	// that is configured in the launchers
	// magazines. If no launcher contains this
	// missile type, no missile will be fired.
	"_missileType",
	// Script to execute on the death of the missile.
	"_onDeath",

	// The guidence settings.
	// 0 - A boolean value that when set to true will spawn
	// 		an asset that can be tracked on map (or with a cTab, etc.)
	// 1 - The side for the object. Currently supports east and west.
	// 2 - The group name for the object.
	"_trackingSettings",
	// True if the target should be deleted
	// after the missile impacts.
	"_deleteTarget",

	// Further Options
	// The start point for the missile. Leave as objNull
	// to get a random start point from the registered
	// launchers or a random position near the target if
	// random missile spawn is enabled.
	// The value for this must follow the following:
	// 0 - Start object
	// 1 - Reload speed (seconds)
	// 2 - Last launch (seconds)
	// 3 - Delete launcher after launch
	"_launcher"
];

["Starting missile launch (server).", LOG_DEBUG, "CRML LAUNCH"] call RD501_fnc_logMessage;

private _randRadius = random [10000, 15000, 20000];

private _launcherSide = civilian;
// _trackingSettings select 1;
// if (not (_launcherSide isEqualType east)) then {
// 	_launcherSide = west;
// };

if ((count _launcher) <= 0) then {
	["No preset values found, requesting next launcher.", LOG_DEBUG, "CRML LAUNCH"] call RD501_fnc_logMessage;

	_launcher = [_missileType] call RD501_fnc_internal_CRML_getNextLauncher;

	if ((count _launcher) <= 0 && RD501_CRML_RandomMissileSpawnAllowed) then {
		["No missile launcher found, using random generation.", LOG_DEBUG, "CRML LAUNCH"] call RD501_fnc_logMessage;

		if (isClass (configFile >> "CfgWeapons" >> RD501_CRML_RandomMissileWeaponClass)) then {
			private _targetPos = _target getPos [_randRadius * sqrt random 1, random 360];
			
			if (isNil "RD501_LaunchHeight") then {
				RD501_LaunchHeight = 10000;
				publicVariable "RD501_LaunchHeight";
			};

			private _node = createVehicle ["Sign_Sphere10cm_F", [_targetPos select 0, _targetPos select 1, RD501_LaunchHeight], [], 0, "CAN_COLLIDE"];
			private _launcherVic = createVehicle ["RD501_VLS_Launcher_Helper_Weapon", _targetPos, [], 0, "CAN_COLLIDE"];
			_launcherVic attachTo [_node, [0, 0, 0]];

			_launcherVic addWeaponTurret [RD501_CRML_RandomMissileWeaponClass, [0]];
			private _magazines = getArray (configFile >> "CfgWeapons" >> RD501_CRML_RandomMissileWeaponClass >> "magazines");
			if (count _magazines > 0) then {
				{
					_launcherVic addMagazineTurret [_x, [0]];
				} forEach _magazines;
			};

			private _unitType = "C_UAV_AI_F";
			// switch (_launcherSide) do {
			// 	case west: {"B_UAV_AI"};
			// 	case east: {"O_UAV_AI"};
			// 	case independent: {"I_UAV_AI"};
			// 	case civilian: {"C_UAV_AI_F"};
			// 	default {"B_UAV_AI"};
			// };

			private _launcherGroup = createGroup [_launcherSide, true];
			private _launcherUnit = _launcherGroup createUnit [_unitType, [0, 0, 0], [], 0, "CAN_COLLIDE"];
			_launcherUnit moveInTurret [_launcherVic, [0]];

			_launcher = [
				_launcherVic,
				false,
				RD501_CRML_RandomMissileSpawnReloadSpeed,
				RD501_CRML_RandomSpawnLastLaunch,
				true,
				[_node]
			];
		} else {
			["Missile weapon is invalid. Configure an actual weapon class in Configure Addons > RD501 > Cruise Missiles > Random Missile Weapon Class.", LOG_ERROR, "CRML LAUNCH"] call RD501_fnc_logMessage;
		};
	} else {
		["Missile launcher was found or random generation is disabled.", LOG_DEBUG, "CRML LAUNCH"] call RD501_fnc_logMessage;
	};
};

if ((count _launcher) <= 0) exitWith {
	["No missile launcher found to launch a missile from.", LOG_WARN, "CRML LAUNCH"] call RD501_fnc_logMessage;

	[_caller, "[MISSILE] Launch Aborted: Invalid missile configuration."] call RD501_fnc_localMessage;
	
	if (_deleteTarget) then {
		deleteVehicle _target;
	};
};

private _mags = [_launcher select 0, _missileType] call RD501_fnc_internal_CRML_getMagazine;

if (not (_mags select 0)) exitWith {
	["No magazines for the provided launcher ammo found to launch a missile with.", LOG_WARN, "CRML LAUNCH"] call RD501_fnc_logMessage;

	[_caller, "[MISSILE]  Launch Aborted: Invalid missile magazine configuration."] call RD501_fnc_localMessage;

	if (_deleteTarget) then {
		deleteVehicle _target;
	};
};

_launcherSide reportRemoteTarget [_target, 16000];
_target confirmSensorTarget [_launcherSide, true];

private _missile = [
	// 0 - Primary target
	_target,
	// 1 - Missile class
	_missileType,
	// 2 - On death script
	_onDeath,
	// 3 - Weapon class name
	_mags select 1,
	// 4 - Magazine class name
	_mags select 2,
	// 5 - delete target on missile death
	_deleteTarget
];

[["Generated missile launch data", _missile] joinString " ", LOG_TRACE, "CRML LAUNCH"] call RD501_fnc_logMessage;

private _launchTime = time;
private _timeSinceLastLaunch = _launchTime - ((_launcher select 3) select 0);
private _reloadSpeed = _launcher select 2;

[["TSLL:", _timeSinceLastLaunch, "Reload Speed:", _reloadSpeed] joinString " ", LOG_TRACE, "CRML LAUNCH HANDLER"] call RD501_fnc_logMessage;

// Lets find the ammo config object.
private _ammoCfgObject = (configFile >> "CfgAmmo" >> _missileType);

if (not (isClass _ammoCfgObject)) exitWith {
	["Invalid ammo class provided.", LOG_WARN, "CRML LAUNCH"] call RD501_fnc_logMessage;

	[_caller, "[MISSILE]  Launch Aborted: Invalid missile ammo configuration."] call RD501_fnc_localMessage;
};

private _alwaysFire = false;

try {
	_alwaysFire = (getNumber (_ammoCfgObject >> "RD501_CRML_alwaysFire")) == 1;
} catch {
	_alwaysFire = false;
};

private _abort = false;

// Find if this missile is ready to fire or not ...
if (_alwaysFire or (_timeSinceLastLaunch > _reloadSpeed)) then {
	// ... if it is, launch it and reset the launch time ...
	(_launcher select 3) set [0, _launchTime];

	[_launcher select 0, "fired", {
		params ["_unit", "_weapon", "_muzzle", "_node", "_ammo", "_magazine", "_projectile"];
		_thisArgs params ["_onDeath", "_target", "_tracking", "_reload", "_deleteAfter", "_deleteTarget", "_additionalDelete"];

		// Remove this event handler before any errors may occour.
		_unit removeEventHandler ["fired", _thisID];

		private _deleteAfterUnit = objNull;
		if (_deleteAfter) then {
			_deleteAfterUnit = _unit;
		};

		// If we have on death code ...
		if (not (_onDeath isEqualTo {})) then {
			// then we create a PFH for this missile.
			private _guidenceHandle = [{
				_this call RD501_fnc_internal_CRML_guideMissilePFH;
			},
			0,
			[
				// missile object
				_projectile,
				// Missile on death
				_onDeath,
				// Missile target
				_target,
				// Missile tracking
				_tracking
			]] call CBA_fnc_addPerFrameHandler;

			[["Launched missile with guidence handle", _guidenceHandle] joinString " ", LOG_INFO, "CRML LAUNCH HANDLER"] call RD501_fnc_logMessage;
		};

		if (_reload) then {
			["RD501_event_internal_CRML_reloadVLSMagazine", [_unit], _unit, [0]] call CBA_fnc_turretEvent;
		};

		// Get the config class of the projectile.
		private _cfgname = typeOf _projectile;
		private _ttl = getNumber (configFile >> "CfgAmmo" >> _cfgname >> "timeToLive");

		// If we have a target to delete ...
		if (_deleteTarget) then {
			[{
				params ["_target"];
				
				// Then wait for the time to live of the ammo and delete the target.
				deleteVehicle _target;
			}, [_target, [_deleteAfterUnit, _additionalDelete]], _ttl] call CBA_fnc_waitAndExecute;

			[["Waiting for", _ttl, "seconds to remove target data."] joinString " ", LOG_DEBUG, "CRML LAUNCH HANDLER"] call RD501_fnc_logMessage;
		};

		// If we have a launcher to delete ...
		if (not (isNull (_deleteLauncher select 0))) then {
			[{
				params ["_deleteLauncher"];

				// Wait for the time to live of the ammo and delete the launcher.
				["RD501_event_internal_CRML_deleteVLSLauncher", [_deleteLauncher select 0], _deleteLauncher select 0] call CBA_fnc_targetEvent;

				{
					deleteVehicle _x;
				} forEach (_deleteLauncher select 1);
			}, [[_deleteAfterUnit, _additionalDelete]], _ttl] call CBA_fnc_waitAndExecute;

			[["Waiting for", _ttl, "seconds to remove launcher data."] joinString " ", LOG_DEBUG, "CRML LAUNCH HANDLER"] call RD501_fnc_logMessage;
		};
	   // on death         target             tracking             reload            delete after       delete target       additional delete
	}, [_missile select 2, _missile select 0, _trackingSettings, _launcher select 1, _launcher select 4, _missile select 5, _launcher select 5]] call CBA_fnc_addBISEventHandler;

	(_launcher select 0) loadMagazine [[0], _missile select 3, _missile select 4];
	
	[["Requested magazine change to: ", _missile select 4, " for missile #", _forEachIndex] joinString "", LOG_DEBUG, "CRML LAUNCH HANDLER"] call RD501_fnc_logMessage;
} else {
	[_caller, ["[MISSILE] Launch Aborted: The launcher is reloading. It will be ready in", _reloadSpeed - _timeSinceLastLaunch, "seconds."] joinString " "] call RD501_fnc_localMessage;

	_abort = true;
};

if (_abort) exitWith {};

private _wepState = weaponState [_launcher select 0, [0]];

// Check magazine switch
if ((_wepState select 3) == (_missile select 4)) then {
	// Check magazine state
	if ((_wepState select 5) != 0 or (_wepState select 6) != 0) then {
		[["Launcher magazine/round still changing, skipped missile #", _forEachIndex] joinString "", LOG_DEBUG, "CRML LAUNCH HANDLER"] call RD501_fnc_logMessage;
		
		if ((_wepState select 5) > 0) then {
			(_launcher select 0) setWeaponReloadingTime [gunner (_launcher select 0), _wepState select 1, 0];
		};

		_wepState = weaponState [_launcher select 0, [0]];
	};

	if ((_wepState select 5) == 0 and (_wepState select 6) == 0) then {
		// Check current missile object
		(_launcher select 0) doWatch (_missile select 0);
		(_launcher select 0) fireAtTarget [_missile select 0];

		[["Launcher magazine changed - requested fire at target: ", _missile select 1, " for missile #", _forEachIndex] joinString "", LOG_DEBUG, "CRML LAUNCH HANDLER"] call RD501_fnc_logMessage;
	} else {	
		[_caller, "[MISSILE] Launch Aborted: The launcher is reloading."] call RD501_fnc_localMessage;
	};
} else {
	[["Launcher failed to change magazines, aborting launch.", _forEachIndex] joinString "", LOG_WARN, "CRML LAUNCH HANDLER"] call RD501_fnc_logMessage;

	if (_launcher select 4) then {
		deleteVehicle (_launcher select 0);
	};
	
	[_caller, "[MISSILE] Launch Aborted: The missile launcher failed to change magazines."] call RD501_fnc_localMessage;
};