// Launches a missile from a mash grenade being thrown
_this spawn {	
	params ["_unit", "_weapon", "_muzzle", "_node", "_ammo", "_magazine", "_projectile"];

	// The explosion time of a smoke grenade.
	sleep 2;

	if (not (isNull _projectile)) then {
		private _target = createVehicle ["HeliHEmpty", (getPosATL _projectile), [], 0, "CAN_COLLIDE"];
		_target attachTo [_projectile, [0, 0, 0]];
		[_unit, _target, side _unit] call RD501_fnc_CRML_launchMashMissile;
		// 										LOG INFO - include not working inside of spawn.
		["Launched mash missile via MASH grenade.", 2, "CRML MASH GREN"] call RD501_fnc_logMessage;
	} else {
		//																		LOG WARN
		["Failed to launch mash missile via MASH grenade - no projectile found.", 3, "CRML MASH GREN"] call RD501_fnc_logMessage;
	};
};
