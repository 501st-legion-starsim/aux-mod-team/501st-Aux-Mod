#include "..\loglevel.hh"

params [
	// The launcher object.
	"_launcher",
	// The vertical offset to spawn the missiles at.
	"_refreshAmmo",
	// The # of seconds between each allowed missile launch.
	"_reloadSpeed"
];

private _res = RD501_CRML_LauncherNodes pushBackUnique [_launcher, _refreshAmmo, _reloadSpeed, [0 - _reloadSpeed], false, []];

if (_res != -1) then {
	[_launcher] call RD501_fnc_CRML_vlsInit;

	[["Added new launcher:", _launcher, "with ammo refresh", _refreshAmmo, "and reload speed of", _reloadSpeed] joinString " ", 
		LOG_INFO, "CRML"] call RD501_fnc_logMessage;
};