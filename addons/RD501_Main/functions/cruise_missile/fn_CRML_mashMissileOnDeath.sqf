#include "..\loglevel.hh";

params [
	// 0
	// Target
	["_target", objNull],
	// 1
	// The last know location of the missile.
	["_lastPos", [0, 0, 0]]
];

try {
	private _pos = _lastPos;

	_spawner = createVehicle [rd501_medical_ccp_building, _pos, [], 0, "CAN_COLLIDE"];
	private _spawnerPos = getPos _spawner;

	// Add the negative z height above the nearest surface to properly reposition
	// the MASH object.
	_spawner setPosATL (_pos vectorAdd [0, 0, 0 - (_spawnerPos select 2)]);

	[["Deployed mash object:", _spawner] joinString " ", LOG_INFO, "CRML"] call RD501_fnc_logMessage;
} catch {
	[["Failed to spawn MASH object:", rd501_medical_ccp_building, "at:", _pos] joinString " ", LOG_ERROR, "CRML"] call RD501_fnc_logMessage;
};