#include "..\loglevel.hh"

params ["_vic"];

private _ai = gunner _vic;

if (not (isNull _ai)) then {
	_ai disableAI "all";
	[["Disabled AI for", _ai, "on", _vic] joinString " ", LOG_DEBUG, "CRML VLS INIT"] call RD501_fnc_logMessage;
} else {
	[["No AI to disable for", _vic] joinString " ", LOG_DEBUG, "CRML VLS INIT"] call RD501_fnc_logMessage;
};
