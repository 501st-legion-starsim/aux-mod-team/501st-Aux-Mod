# Details
Adds cruise missile functionality to scripted components of missions. Supports infinite ammo launchers, custom targeting, firing, and tracking of missiles via scripts, and on death scripts for missiles. Launchers should not be targeted manually.

## Parameters
### Launch Missile
`RD501_fnc_CRML_launchMissile`
```sqf
// Launches a cruise missle.
params [
	// The target to guide the missile to.
	["_target", laserTarget player],
	// The type of missile. It must be a type
	// that is configured in the launchers
	// magazines. If no launcher contains this
	// missile type, no missile will be fired.
	["_missileType", "RD501_Cruise_Missile_Ammo_Base"],
	// Script to execute on the death of the missile.
	["_onDeath", {}],

	// The tracking settings.
	// 0 - A boolean value that when set to true will spawn
	// 		an asset that can be tracked on map (or with a cTab, etc.)
	// 1 - The side for the object. Currently supports east and west.
	// 2 - The group name for the object.
	["_trackingSettings", [false, objNull, objNull]],
	// Further Options
	// The start point for the missile. Leave as objNull
	// to get a random start point from the registered
	// launchers or a random position near the target if
	// random missile spawn is enabled.
	// The value for this must follow the following:
	// 0 - Start object
	// 1 - Reload speed (seconds)
	// 2 - Last launch (seconds)
	// 3 - Delete launcher after launch (bool)
	["_missileStart", []]
];
```

#### Launch Missile - On Death
The on death script is passed the following parameters:
```sqf
params [
	// 0
	// Target
	["_target", objNull],
	// 1
	// The last know location of the missile.
	["_lastPos", [0, 0, 0]]
];
```

### Launch Mash Missile
`RD501_fnc_CRML_launchMashMissile`
```sqf
// launches a mash missile.

params [
	// The target to aim at.
	"_target", 
	// The side of the tracking information. Set to objNull
	// to disable. Defaults to the players side.
	["_side", side player]
];
```

### Register Launcher
`RD501_fnc_CRML_registerLauncher`
```sqf
// Registers a missile launcher
params [
	// The launcher object.
	["_launcher", objNull],
	// True if this launcher should never run out of ammo.
	["_refreshAmmo", true],
	// The # of seconds between each allowed missile launch.
	["_reloadSpeed", 15]
];
```

A version of this script, `RD501_fnc_init_CRML_registerLauncher`, exists to be used in the `init` field within the Eden editor. It takes the same parameters.

### Remove Launcher
`RD501_fnc_CRML_removeLauncher`
```sqf
// Removes a registerd launcher.
params [
	// The launcher object to remove.
	"_launcher"
];
```

### Register Support Actions
`RD501_fnc_CRML_addCruiseMissileSupportRegistrationActions`
```sqf
// Adds an action to an object to allow the use of cruise missile
// supports.

// The object to register actions to.
params ["_object"];
```