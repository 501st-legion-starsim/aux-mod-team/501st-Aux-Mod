// Spawns a UAV Net matching the net name.
params ["_netName"];

// Should only run on server.
if (not isServer) exitWith {};

private _triggerSet = localNamespace getVariable "RD501_UAVNET_triggerObject";
private _trigger = _triggerSet getOrDefault [_netName, objNull];

if (isNull _trigger) then {
	private _triggerDataSet = localNamespace getVariable "RD501_UAVNET_triggerData";
	private _triggerData = _triggerDataSet getOrDefault [_netName, []];

	if ((count _triggerData) == 0) exitWith {
		diag_log "UAVNET Tirgger failed to build - no data was registered.";
	};

	_triggerData params ["_size", "_statements", "_activation", "_interval", "_timeout", "_position"];

	private _trigger = createTrigger ["EmptyDetector", _position];
	_trigger setPosASL _position;

	_trigger setTriggerArea _size;
	_trigger setTriggerStatements _statements;
	_trigger setTriggerActivation _activation;
	_trigger setTriggerInterval _interval;
	_trigger setTriggerTimeout _timeout;

	private _triggers = localNamespace getVariable "RD501_UAVNET_triggerObject";
	_triggers set [_netName, _trigger];

	private _spawnObjectSet = localNamespace getVariable "RD501_UAVNET_additionalSpawns";
	private _spawnObject = _spawnObjectSet getOrDefault [_netName, []];

	private _spawnObjectRaw = [];
	{
		private _obj = createVehicle [_x select 0, _x select 1, [], 0, "CAN_COLLIDE"];
		_obj setDir (_x select 2);
		_spawnObjectRaw pushBack _obj;
	} forEach _spawnObject;

	private _spawns = localNamespace getVariable "RD501_UAVNET_additionalSpawnsRaw";
	_spawns set [_netName, _spawnObjectRaw];
};