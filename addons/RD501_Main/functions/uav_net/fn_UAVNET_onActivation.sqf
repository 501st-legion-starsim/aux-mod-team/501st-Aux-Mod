#include "..\loglevel.hh"

params ["_thisList", "_thisTrigger"];

private _key = objNull;
{
    if (_y == _thisTrigger) exitWith {
        _key = _x;
    };
} forEach (localNamespace getVariable ["RD501_UAVNET_triggerObject", createHashMap]);

if (not (_key isEqualType "")) exitWith {
    [["No registered trigger matching", _thisTrigger] joinString " ", LOG_ERROR, "UAVNET"] call RD501_fnc_logMessage;
};

private _localUavs = _thisList select { unitIsUAV _x };
if(count _localUavs == 0) exitWith {
    ["RD501_event_UAVNET_removeTrigger", [_key]] call CBA_fnc_serverEvent;
};

private _oneTrueUav = _localUavs select 0;
private _triggerPos = getPosASL _thisTrigger;

["RD501_event_internal_UAVNET_handleUAV", [_oneTrueUav, _triggerPos, _key], _oneTrueUav] call CBA_fnc_targetEvent;