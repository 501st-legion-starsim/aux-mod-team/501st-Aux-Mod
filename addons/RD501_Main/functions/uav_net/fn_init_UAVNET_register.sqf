// Registers a UAVNET trigger
params [
	// The trigger, already sized, to save location data for.
	"_thisTrigger",
	// The objects to add spawn net interactions for. If this is not an array,
	// it will be turned into an array.
	["_interactionObjects", []],
	// Any additional objects to save the location of and spawn when the trigger
	// is alive.
	["_additionalSpawns", []],
	// UAV Net name. Registrations of the same name will override any previous
	// registrations, but interactions will stay on all interactable objects.
	["_netName", "default"]
];

// Backwards compatability.
if (not (_interactionObjects isEqualType [])) then {
	_interactionObjects = [_interactionObjects];
};

{
	_x addAction [["Spawn UAV Net (", _netName, ")"] joinString "", {
		["RD501_event_internal_UAVNET_spawnTrigger", [_this select 3]] call CBA_fnc_serverEvent;
	}, _netName, 1.5, false];

	_x addAction [["Remove UAV Net (", _netName, ")"] joinString "", {
		["RD501_event_UAVNET_removeTrigger", [_this select 3]] call CBA_fnc_serverEvent;
	}, _netName, 1.5, false];
} forEach _interactionObjects;

// Only save values on server.
if (isServer) then {
	private _triggerSize = triggerArea _thisTrigger;
	private _triggerStatements = triggerStatements _thisTrigger;
	private _triggerActivation = triggerActivation _thisTrigger;
	private _triggerInterval = triggerInterval _thisTrigger;
	private _triggerTimeout = triggerTimeout _thisTrigger;
	private _triggerPos = getPosASL _thisTrigger;

	private _data = localNamespace getVariable ["RD501_UAVNET_triggerData", objNull];
	
	if (not (_data isEqualType createHashMap)) then {
		// Do first time registration
		_data = createHashMap;
		localNamespace setVariable ["RD501_UAVNET_triggerData", _data];
		localNamespace setVariable ["RD501_UAVNET_triggerObject", createHashMap];
		localNamespace setVariable ["RD501_UAVNET_additionalSpawnsRaw", createHashMap];
		localNamespace setVariable ["RD501_UAVNET_additionalSpawns", createHashMap];
	};

	private _triggers = localNamespace getVariable "RD501_UAVNET_triggerObject";
	private _spawns = localNamespace getVariable "RD501_UAVNET_additionalSpawnsRaw"; 

	_data set [_netName, [_triggerSize, _triggerStatements, _triggerActivation, _triggerInterval, _triggerTimeout, _triggerPos]];
	_triggers set [_netName, _thisTrigger];
	_spawns set [_netName, _additionalSpawns];
};
