#include "..\loglevel.hh"

settingRes = [
	"RD501_MinimumLogLevel",
    "LIST",
    ["Minimum Log Level", "The minimum level of logs to display in the RTP log from RD501."],
    ["RD501", "Logging"],
	[
		[LOG_TRACE, LOG_DEBUG, LOG_INFO, LOG_WARN, LOG_ERROR, LOG_CRIT],
		["Trace", "Debug", "Info", "Warn", "Error", "Critical"],
		LOG_INFO
	],
	false
] call CBA_fnc_addSetting;

settingRes = [
	"RD501_LogGlobally",
    "CHECKBOX",
    ["Log Globally", "If true, will broadcast log messages to all clients."],
    ["RD501", "Logging"],
	true,
	true
] call CBA_fnc_addSetting;

_this call RD501_fnc_CRML_preInit;