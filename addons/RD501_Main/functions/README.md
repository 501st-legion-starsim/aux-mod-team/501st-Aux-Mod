# RD501 Custom UI Control ID Groups Legend

All display IDs are six digits, where the first three are always `501`. The next
three indicate what function group the controls are for.

Individual control definitions can be found in their respective addon folder.

| Code  |  Function Group  |         Path         |
| :---: | :--------------: | :------------------: |
|  001  | Texture Selector | `.\texture_selector` |