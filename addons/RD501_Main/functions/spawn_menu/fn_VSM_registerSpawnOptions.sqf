params ["_vic", "_disp", ["_crew", false]];

if (isNil "RD501_VSM_registered_vics") then {
	RD501_VSM_registered_vics = createHashMap;
	publicVariable "RD501_VSM_registered_vics";
};

item_list = RD501_VSM_registered_vics getOrDefault ["vics", []];
item_list append [[_vic, _disp, _crew]];
RD501_VSM_registered_vics set ["vics", item_list];

diag_log text (["[SVLN]", "[VIC SPAWNER]", "DEBUG:", "Registered Vic", _vic, "(", _disp, ")"] joinString " ");