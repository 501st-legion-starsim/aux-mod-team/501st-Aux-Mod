params ["_unit", "_newVehicle", "_oldVehicle"];

if (_unit == _newVehicle) exitWith {}; // Leaving the vic.

if ([_unit, _newVehicle] call RD501_fnc_internal_TEXSEL_canChangeTextures) then {
	// Player is driver - lets do some auto config for
	// textures.

	private _name = name _unit;
	private _options = "true" configClasses (configFile >> "CfgVehicles" >> typeOf _newVehicle >> "RD501_Texture_Selector");
	
	private _defaultsCheck = [];
	private _defaults = profileNamespace getVariable "RD501_TEXSEL_DefaultTextureConfigurations";
	if (not (isNil '_defaults')) then {
		private _classDefaults = _defaults get (typeOf _newVehicle);
		if (not (isNil '_classDefaults')) then {
			{
				_defaultsCheck pushBack (_x select 1);
			} forEach _classDefaults;
		};
	};

	private _toAutoApply = [];
	private _defaultsToApply = [];
	{
		if (isText (_x >> "title") && isArray (_x >> "decals")) then {
			private _decals = getArray (_x >> "decals");

			private _autoApplyRegex = [_x, "autoApplyNameRegex", ""] call BIS_fnc_returnConfigEntry;
			private _restrictRegex = [_x, "restrictNameRegex", ""] call BIS_fnc_returnConfigEntry;

			private _add = true;
			if (not (_restrictRegex isEqualTo "")) then {
				if (not (_name regexMatch _restrictRegex)) then {
					[["Restrict regex failed to match", _name, "to", _restrictRegex, "for", name player] joinString " ", LOG_DEBUG, "TEXSEL AUTOAPPLY"] call RD501_fnc_logMessage;
					_add = false;
				};
			};

			if (_add) then {
				private _cfgName = configName _x;
				if (_cfgName in _defaultsCheck) then {
					_defaultsToApply pushBack [_cfgName, _decals];
				};
			};

			if (not (_autoApplyRegex isEqualTo "")) then {
				if (not (_name regexMatch _autoApplyRegex)) then {
					[["Auto apply regex failed to match", _name, "to", _autoApplyRegex, "for", name player] joinString " ", LOG_DEBUG, "TEXSEL AUTOAPPLY"] call RD501_fnc_logMessage;
					_add = false;
				};

				if (_add) then {
					_toAutoApply pushBack _decals;
				};	
			} else {
				[["Config", _x, "has no auto apply regex - skipping."] joinString " ", LOG_INFO, "TEXSEL AUTOAPPLY"] call RD501_fnc_logMessage;
			};
		} else {
			[["Config", _x, "is missing a title and decals field - skipping."] joinString " ", LOG_WARN, "TEXSEL AUTOAPPLY"] call RD501_fnc_logMessage;
		};
	} forEach _options;

	{
		{
			if (not (_x isEqualTo "")) then {
				_newVehicle setObjectTextureGlobal [_forEachIndex, _x];
			};
		} forEach _x;
	} forEach _toAutoApply;

	{
		private _check = _x;
		private _index = _defaultsToApply findIf { (_x select 0) == _check };
		if (not (_index == -1)) then {
			{
				if (not (_x isEqualTo "")) then {
					_newVehicle setObjectTextureGlobal [_forEachIndex, _x];
				};
			} forEach ((_defaultsToApply select _index) select 1);
		};
	} forEach _defaultsCheck;
};
