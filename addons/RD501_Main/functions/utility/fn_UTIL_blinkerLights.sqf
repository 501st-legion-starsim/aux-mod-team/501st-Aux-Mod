params [
	// The starting position for the lights.
	"_node",
	// The color of the lights
	["_color", [0, 0, 0]],
	// The directions to move out in. N E S W
	["_dir", [false, false, false, false]],
	// The ammount of lights to spread from the center.
	['_count', 2],
	// The distance in meters each node should be from the previous node.
	["_spread", 5],
	// The time between blinks of each part of the sequence (in seconds)
	["_delay", 0.5],
	// Will the lights blink backwards
	["_inverted", false],
	// Light brightness controls.
	["_brightness", 100],
	["_intensity", 1000],
	["_daylightMultiplier", 10]
];

// Stop if lights are disabled.
if (not (profileNamespace getVariable ["RD501_UTIL_LightsEnabled", true])) exitWith {};

if ((count _color) > 3) then {
	_color deleteRange [3, (count _color) - 3];
};

diag_log text (["[RD501]", "[UTIL]", "TRACE:", "Starting blinker light registration", _node, _color, _dir, _count, _spread, _delay] joinString " ");

_locations = [];
_nodePos = getPosATL _node;
_locations pushBack [_nodePos];

// Build the location data.
private _i = 1;
while { _i <= _count } do {
	_locations pushBack [];
	_arr = _locations select _i;

	_dist = _spread * _i;
	// Moving North (0)
	if (_dir select 0) then {
		_pos = _node getRelPos [_dist, 0];
		_pos set [2, _nodePos select 2];
		_arr pushBack _pos;
	};

	// Moving East (90)
	if (_dir select 1) then {
		_pos = _node getRelPos [_dist, 90];
		_pos set [2, _nodePos select 2];
		_arr pushBack _pos;
	};

	// Moving South (180)
	if (_dir select 2) then {
		_pos = _node getRelPos [_dist, 180];
		_pos set [2, _nodePos select 2];
		_arr pushBack _pos;
	};

	// Moving West (270)
	if (_dir select 3) then {
		_pos = _node getRelPos [_dist, 270];
		_pos set [2, _nodePos select 2];
		_arr pushBack _pos;
	};

	_i = _i + 1;
};

diag_log text (["[RD501]", "[UTIL]", "TRACE:", "Built blinker light location data", _locations] joinString " ");

// Then for each location, create the PFH.
{
	// ... for each spot in the location ...
	_index = _forEachIndex;
	if (_inverted) then {
		_index = ((_count + 1) - _index) - 1;
	};
	
	{
		_handle = [{
			_this call RD501_fnc_internal_UTIL_blinkerLightsPFH;
		}, _delay, [_node, _x, _color, true, objNull, _index, _count + 1, 0, _brightness, _intensity, _daylightMultiplier]] call CBA_fnc_addPerFrameHandler;

		diag_log text (["[RD501]", "[UTIL]", "TRACE:", "Generated PFH", _handle, "for", _x, "with color", _color] joinString " ");
	} forEach _x;
} forEach _locations;