// If this is enabled, and the log comes from the server, execute it globally.
if (not (isNil "RD501_LogGlobally") and RD501_LogGlobally and isServer) then {
	["RD501_event_logMessage", _this] call CBA_fnc_globalEvent;
} else {
	// Otherwise just locally is fine.
	_this call RD501_fnc_logMessageGlobal;
};