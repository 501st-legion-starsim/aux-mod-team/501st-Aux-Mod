# Details
Scripts to operate the catapult launch system.

## Functions
### Open Menu
`RD501_fnc_CAT_openMenu;`

ex: `[] call RD501_fnc_CAT_openMenu;`

```
// Opens the catapult launch menu.
```

### Load Menu Defaults
`RD501_fnc_CAT_loadMenuDefaults`

ex: `[] call RD501_fnc_loadMenuDefaults`

```
// Loads default settings for the catapult.
```

### Register
`RD501_fnc_init_CAT_register`

ex: `[_pad, _displayName] call RD501_fnc_init_CAT_register;`

```
// Registers a catapult pad.
params [
    // The pad object to register. Finds Vics near this pos.
    "_pad",
    // The display name of this pad.
    "_disp"
];
```