/*
 * Author: M3ales
 *
 * Will add a "Change Hud Colour" action to all items of the given type. 
 * It also collects all the colours and produces sub actions, and then finally adds the custom colour option.
 *
 * Arguments:
 * type
 * The classname of the vehicle/object to add this action to.
 *
 * Return Value:
 * Nothing
 *
 * Example:
 * ["rd501_laatmk2"] call rd501_mfd_colours_addActionsToClass
 *
 * Public: No
 */

#include "function_macros.hpp"
#define ICON_PATH(icon_name) ADDON_PATH##\icons\##icon_name##.paa
#define QICON_PATH(icon_name) QUOTE(ICON_PATH(icon_name))

params["_type"];

private _colours = [] call FUNC(availableColours);

private _rootAction = [
	QGVAR(root),
	"Change Hud Colour",
	QICON_PATH(colorWheel),
	{ },
	{ _this call FUNC(canChangeColour) }
] call ACE_interact_menu_fnc_createAction;
[_type, 1, ["ACE_SelfActions"], _rootAction, false] call ACE_interact_menu_fnc_addActionToClass;

{
	_x params["_name", "_iconPath", "_rgba"];
	private _id = format["COLOUR_%1", _foreachIndex];
	private _action = [
		_id,
		_name,
		_iconPath,
		{
			params["_target", "_player", "_params"];
			[_target, _params] call FUNC(setColour);
		},
		{ true },
		{ [] },
		+_rgba,
		"",
		4,
		[false, false, false, true, false]
	] call ACE_interact_menu_fnc_createAction;
	[_type, 1, ["ACE_SelfActions", QGVAR(root)], _action, false] call ACE_interact_menu_fnc_addActionToClass;
} forEach(_colours);

private _customColourAction = [
	QGVAR(custom_colour),
	"Custom",
	QICON_PATH(colorWheel),
	{
		params["_target", "_player", "_params"];
		private _colour = GVAR(custom) + [GVAR(custom_alpha)];
		[_target, _colour] call FUNC(setColour);
	},
	{ true },
	{ [] },
	[],
	"",
	4,
	[false, false, false, true, false]
] call ACE_interact_menu_fnc_createAction;
[_type, 1, ["ACE_SelfActions", QGVAR(root)], _customColourAction, false] call ACE_interact_menu_fnc_addActionToClass;
