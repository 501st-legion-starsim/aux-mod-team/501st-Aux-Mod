#include "function_macros.hpp"
params ["_unit", "_object", "_cost"];
if (GVAR(useAmmo)) exitWith
{
	private _can_fortify = GVAR(currencyItem) in (_unit call ace_common_fnc_uniqueItems); 
	if (!_can_fortify) then
	{
		private _config = configFile >> "CfgWeapons" >> GVAR(currencyItem);
		if!(isText (_config >> "displayName")) exitWith {
			LOG_ERRORF_1("Unable to read displayName on item %1",GVAR(currencyItem));
		};
		private _item_name = getText (_config >> "displayName");
		systemChat format ["You need at least 1 %1 to use the Fortify Tool!", _item_name];
	};
	_can_fortify
};
true
