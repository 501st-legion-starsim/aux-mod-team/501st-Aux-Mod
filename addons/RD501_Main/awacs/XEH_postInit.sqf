#include "functions\function_macros.hpp"

if(hasInterface) then {
	["ace_interact_menu_newControllableObject", {
		params ["_type"]; // string of the object's className
		private _validTypes = GVAR(validTypes);
		if (_validTypes findIf { _type isKindOf (_x) } == -1) exitWith {};
		LOGF_1("Adding AWACS actions to %1", _type);
		[_type] call FUNC(addActionsToClass);
	}] call CBA_fnc_addEventHandler;
};

GVAR(Running) = false;