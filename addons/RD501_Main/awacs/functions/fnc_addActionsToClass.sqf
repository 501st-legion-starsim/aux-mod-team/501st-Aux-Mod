/*
 * Author: M3ales & Hobnob
 *
 * Will add AWACS actions to the given type. Typically expected to be a vehicle.
 *
 * Arguments:
 * type
 * The classname of the vehicle/object to add this action to.
 *
 * Return Value:
 * Nothing
 *
 * Example:
 * ["Car_F"] call rd501_awacs_addActionsToClass
 *
 * Public: No
 */

#include "function_macros.hpp"

params["_type"];

private _toggleAction = [
	QGVAR(toggle),
	"Toggle AWACS Mode",
	"",
	{ _this call FUNC(toggleAWACS) },
	{ visibleMap }
] call ACE_interact_menu_fnc_createAction;
[_type, 1, ["ACE_SelfActions"], _toggleAction, false] call ACE_interact_menu_fnc_addActionToClass;