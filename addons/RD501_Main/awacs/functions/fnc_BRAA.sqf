/*
 * Author: Hobnob
 * Adds the BRAA (Bearing, Range, Altitude, Aspect) line handler. 
 *
 *
 * Arguments:
 *
 * Example:
 * [] call rd501_awacs_fnc_BRAA
 *
 * Public: No
 */

#include "function_macros.hpp"

GVAR(StartPos) = nil;
GVAR(MarkerIndex) = 0;
GVAR(BRAA_EHID) = addMissionEventHandler ["MapSingleClick", {
	params["", "_pos"];
	if (isNil QGVAR(StartPos)) then
	{
		GVAR(StartPos) = _pos;
	} else {
		_bearing = [GVAR(StartPos), _pos] call BIS_fnc_dirTo;
		_distance = [((GVAR(StartPos) distance2D _pos)/1000) , 1] call BIS_fnc_cutDecimals;
		_text = format ["%1° %2km", round _bearing, _distance];
		[GVAR(MarkerIndex), GVAR(StartPos), _pos, _text] spawn {
			params ["_index", "_startPos", "_endPos", "_text"];
			_startPos params ["_startX", "_startY"];
			_endPos params ["_endX", "_endY"];
			_line = createMarkerLocal [format ["BRAA%1", _index], _startPos];
			_line setMarkerShapeLocal "polyline";
			_line setMarkerPolylineLocal [_startX, _startY, _endX, _endY];
			_line SetMarkerColorLocal "ColorBlack";

			_endDot = createMarkerLocal [format ["BRAADOT2%1", _index+1], _endPos];
			_endDot setMarkerShapeLocal "ICON";
			_endDot setMarkerTypeLocal "hd_dot";
			_endDot setMarkerTextLocal _text;
			GVAR(MarkerIndex) = GVAR(MarkerIndex) + 2;
			sleep 1.5;
			deleteMarkerLocal _line;
			deleteMarkerLocal _endDot;
		};
		GVAR(StartPos) = nil;
	};
}];