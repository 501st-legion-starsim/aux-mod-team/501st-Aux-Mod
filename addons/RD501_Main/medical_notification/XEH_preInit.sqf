#include "functions\function_macros.hpp"

LOG("PreInit Begin");
private _version = getArray(configFile >> "CfgPatches" >> ADDON >> "version");
LOG(format["Version: %1", _verison]);

LOG("PREP Begin");
#include "XEH_PREP.sqf"
LOG("PREP Complete");

[QGVAR(enabled),
        "CHECKBOX",
        [
                "Enabled",
                "If disabled you will not send messages to other players when doing medical on them."
        ],
        ["RD501", "Medical Notification"],
		true
] call CBA_fnc_addSetting;

[QGVAR(message),
        "EDITBOX",
        [
                "Message",
                "Message to show to other players, keep it clean. %1 is used as a placeholder for your name."
        ],
        ["RD501", "Medical Notification"],
		"%1 is asking that you kindly hold still."
] call CBA_fnc_addSetting;

LOG("PreInit Complete");