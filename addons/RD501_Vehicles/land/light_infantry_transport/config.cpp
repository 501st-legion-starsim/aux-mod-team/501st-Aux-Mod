//Get this addons macro

//get the macro for the air RD501_patch_vehicles

//get generlized macros
#include "../../../RD501_main/config_macros.hpp"

//General name of the vehicle
#define vehicle_addon light_infantry_transport
#define patch_name MODNAME##vehicle_addon##_Patches
#define vehicle_classname MODNAME##_##vehicle_addon

#define new_lit_class(name) vehicle_classname##_##name

class CfgPatches
{
	class RD501_patch_light_infantry_transport
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			MACRO_QUOTE(RD501_patch_vehicles)
		};
		requiredVersion=0.1;
		units[]=
		{
			MACRO_QUOTE(macro_new_vehicle(light_infantry_transport,CIS_MkII))
		};
		weapons[]=
		{
			
		};
	};
};


#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers ;

class CfgVehicles
{

	#include "inheritance.hpp"
	
	class 442_argon_covered_cis;
	class RD501_argon_transport_covered: 442_argon_covered_cis
	{
		scope = 2;
		scopeCurator = 2;
		forceInGarage = 1;
		displayName = "Argon Transport (Covered)";
		crew = "Aux501_Units_CIS_Humans_Soldier_Unit";
		faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
		editorSubcategory = "Aux501_Editor_Subcategory_Transports";
	};
};
