#include "../../../RD501_main/config_macros.hpp"
class CfgPatches
{
	class RD501_patch_MASH
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles,
			A3_Supplies_F_Heli
		};
		requiredVersion=0.1;
		units[]=
		{,
			RD501_MASH_Tent,
		};
		weapons[]=
		{
			
		};
	};
};

#include "../../common/sensor_templates.hpp"
class DefaultEventhandlers;
class CfgVehicles
{
	class RD501_FX7Droid;
	class RD501_FX7Droid_mash: RD501_FX7Droid
    {
		scope = 1;
        scopeCurator = 1;
        displayName = "FX7 Medical Droid";
        editorPreview = "";
        editorCategory =  MACRO_QUOTE(macro_editor_cat(statics));
		editorSubcategory = MACRO_QUOTE(macro_editor_cat(static_msc));
        icon = "iconObject_5x2";
        model = "RD501_Vehicles\static\FX7Droid\FX7Droid.p3d";
        vehicleClass = "Misc";
		
		class EventHandlers;
    };
	class RD501_MASH_Tent: RD501_FX7Droid_mash
	{
		displayName = "Republic M*A*S*H Droid";
		scope = 2;
		scopeCurator = 2;
		RD501_magclamp_large_offset[] = {0.0, 0.0, -2.0};
		RD501_magclamp_small_offset[] = {0.0, 2.0, -2.0};

		editorCategory = MACRO_QUOTE(macro_editor_cat(statics));
		simulation = "house";
		transportMaxWeapons = 25;
        transportMaxMagazines = 100;
        transportMaxBackpacks = 10;
        maximumLoad = 5000;
        supplyRadius = 5;
        class TransportBackpacks{};
        class TransportMagazines{};
        class TransportWeapons{};
        class TransportItems
		{
			#include "../../common/common_items_medical.hpp"				
		};

		//Add bacta tank functionality to MASH
		class EventHandlers:DefaultEventhandlers {}; 
        class ACE_Actions 
		{
            class ACE_MainActions 
			{
				displayName = "Bacta Tank";
                selection = "";
                distance = 4;
                condition = "true";
				position = "[0,0,1.5]";
                class RD501_Heal_All_Nearby
                {
                    displayName = "Heal Nearby";
                    statement = "[_player, _target, 10] call rd501_fnc_healAllNearby";
                    condition = "true";
                };
				class RD501_Heal_Self
				{
					displayName = "Heal";
					statement = "[_player, _player] call ace_medical_treatment_fnc_fullHeal";
					condition = "true";
				};
            };
        };
	};
};