
#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_jammer_truck
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(cis,jammer_truck),
		};
		weapons[]=
		{
			
		};
	};
};

class CfgVehicles
{
    class 442_argon_repair_cis;

	class macro_new_vehicle(cis,jammer_truck): 442_argon_repair_cis
    {
        displayName = "Argon Enigma Jammer";
        crew = "Aux501_Units_CIS_Humans_Soldier_Unit";
        side = 0;
        faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
		editorSubcategory = "Aux501_Editor_Subcategory_Special";
        scope = 2;
        scopeCurator = 2;
        forceInGarage = 1;
    };
};

class Extended_Init_EventHandlers 
{
	class macro_new_vehicle(cis,jammer_truck)
	{
		class rd501_jammer {
			init = "[_this select 0, 600, 160] call rd501_fnc_jammersAdd";
		};
	};
};