#include "../../../RD501_main/config_macros.hpp"

class CfgPatches
{
	class RD501_patch_n99
	{
		addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

		requiredAddons[]=
		{
			RD501_patch_vehicles
		};
		requiredVersion=0.1;
		units[]=
		{
			macro_new_vehicle(cis,n99)
		};
		weapons[]=
		{
			
		};
	};
};
class CfgVehicles
{
	
    class 3AS_N99_base_F;
	class macro_new_vehicle(cis,n99): 3AS_N99_base_F
	{
        scope = 2;
		scopeCurator = 2;
		displayName = "NR-N99 Snail Tank";
		side = 0;
		faction = "Aux501_FactionClasses_Confederacy";
        editorCategory = "Aux501_Editor_Category_Confederacy";
        editorSubcategory = "EdSubcat_Tanks";
		vehicleClass = "Armored";
		crew = "Aux501_Units_CIS_UAV_Crew_Unit";
        smokeLauncherAngle = 120;
        armor = 250;
    };
};