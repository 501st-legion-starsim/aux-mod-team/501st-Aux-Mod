

class _item_ACE_tourniquet
{
	name = "ACE_tourniquet";
	count = 4;
};
class _item_ACE_splint
{
	name = "ACE_splint";
	count = 4;
};
class _item_ACE_Needle
{
	name = "kat_IV_16";
	count = 4;
};
class _item_ACE_plasmaIV_250
{
	name = "ACE_plasmaIV_250";
	count = 3;
};
class _item_ACE_plasmaIV_500
{
	name = "ACE_plasmaIV_500";
	count = 5;
};
class _item_ACE_plasmaIV_1000
{
	name = "ACE_plasmaIV";
	count = 2;
};
class _item_ACE_elasticBandage
{
	name = "ACE_elasticBandage";
	count = 25;
};				
class _item_ACE_quikclot
{
	name = "ACE_quikclot";
	count = 5;
};					
class _item_ACE_packingBandage
{
	name = "ACE_packingBandage";
	count = 30;
};						
class _item_ACE_painkiller
{
	name = "RD501_Painkiller";
	count = 5;
};								
class _item_ACE_epinephrine
{
	name = "ACE_epinephrine";
	count = 4;
};
class _item_Necro_Enzyme_Refined
{
	name = "dev_enzymeCapsule_refined";
	count = 3;
};
class _item_Necro_Enzyme
{
	name = "dev_enzymeCapsule";
	count = 5;
};
//purple flare
class _item_SmokeShellPurple
{
	name = "SmokeShellPurple";
	count = 3;
};