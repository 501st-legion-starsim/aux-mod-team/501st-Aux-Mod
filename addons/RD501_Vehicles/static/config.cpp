#include "../../RD501_main/config_macros.hpp"
class CfgPatches
{
    class RD501_patch_staticturret
    {
        addonRootClass=MACRO_QUOTE(RD501_patch_vehicles);

        requiredAddons[]=
        {
            "RD501_patch_vehicles"
        };
        requiredVersion=0.1;
        units[]={};
        weapons[]={};
    };
};