
#include "..\RD501_main\config_macros.hpp"

#include "script_macros.hpp"

#define macro_jp_ver 1.1 Endgame
#define name_jumppack_f  JT-21 (Version ##macro_jp_ver##)
class CfgPatches
{
	class RD501_patch_jumppack
	{
		author=DANKAUTHORS;
		requiredAddons[]=
		{
			MACRO_QUOTE(RD501_patch_particle_effects)
		};
		requiredVersion=0.1;
		units[]={

		};
		weapons[]={};
		vehicles[]={
			MACRO_QUOTE(macro_jumppackClass(neutral,base)),
			MACRO_QUOTE(macro_jumppackClass(neutral,rto))
		};
	};
};

class CfgVehicles
{
	class B_AssaultPack_blk;
	class JLTS_Clone_jumppack_mc;
	class JLTS_Clone_jumppack;
	class JLTS_Clone_jumppack_JT12;
};