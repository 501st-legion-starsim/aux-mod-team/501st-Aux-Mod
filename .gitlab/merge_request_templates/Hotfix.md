Please make sure you have read our [Contributing Guidelines](https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod/-/blob/development/CONTRIBUTING.md) and understand the [Testing Guidelines](https://gitlab.com/501st-legion-starsim/aux-mod-team/501st-Aux-Mod/-/blob/development/CONTRIBUTING.md#testing) before creating a merge request.

# Description
The bug that is being hotfixed.

# Details
What issues are being closed. If there are no issues to close, detail what is fixed and why.
- Including
- More
- Detail
- Is
- Better

# Notes
Additional Notes.

/label ~bug