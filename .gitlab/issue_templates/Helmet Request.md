# Details
## Trooper
Your name (or the name of the person you are filing this for).

## Rank
Rank of the person the helmet is for.

## Slot
Where the person is slotted.

## MOS
If this is an MOS helmet, specify it here.

## Design Details
Any design details that are requested.

### Images
Images of your changes/additions.

### Description
A description of the images.

### Source Links
Links to the source files/images for the changes.

/label ~custom-helmet::new-request
